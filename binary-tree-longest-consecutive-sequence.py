class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)

root.right = node3
node3.left = node2
node3.right = node4
node4.right = node5

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {int} the length of the longest consecutive sequence path
    result = None
    path = []
    depth = 0
    def longestConsecutive(self, root):
        # Write your code here
        if root is None:
            return None
        self.result = 1
        currLen = 1
        self.dfs(root, currLen)
        return self.result

    def dfs1(self, root):
        if root is None:
            return

        self.path.append(root.val)

        self.dfs(root.left)
        self.dfs(root.right)

        self.path.pop()

    def compare(self, currVal, path):
        print(currVal, path)
        if currVal == path[-1] + 1:
            self.result = len(self.path)
        else:
            self.result = 1

    def dfs(self, root, currLen):

        if root.left is None and root.right is None:
            self.path.append(root.val)

            self.path.pop()
            return

        self.path.append(root.val)

        self.compare(root.val, self.path)

        if root.left:
            self.dfs(root.left)

        if root.right:
            self.dfs(root.right)

        self.path.pop()
        self.depth -= 1



sol = Solution()
ans = sol.longestConsecutive(root)

print('[ans]',ans)