WALL = 2
ZOMBIE = 1
PEOPLE = 0
DIR_X = [1,0,0,-1]
DIR_Y = [0,1,-1,0]
DIR = [[1,0], [0,1], [0,-1], [-1,0]]
class Solution:

    # @param {int[][]} grid  a 2D integer grid
    # @return {int} an integer
    def zombie(self, grid):
        days = 0
        people = 0
        if grid is None:
            return -1

        m = len(grid)
        n = len(grid[0])
        queue = []

        for row in range(m):
            for col in range(n):
                if grid[row][col] == ZOMBIE:
                    queue.append((row, col))
                elif grid[row][col] == PEOPLE:
                    people += 1

        while queue:
            newQ =[]
            days += 1

            for node in queue:
                for dx, dy in DIR:
                    x = node[0] + dx
                    y = node[1] + dy

                    if self.isHuman(x, y, grid):
                        grid[x][y] = ZOMBIE
                        people -= 1
                        if people == 0:
                            return days

                        newQ.append((x,y))

            queue = newQ


        for x in range(m):
            for y in range(n):
                if grid[x][y] == PEOPLE:
                    return -1

        return days #最後一次在newQ裡面的zombie已經沒有活人可吃了，所以最後一個while q沒有任何isHuman

    def isHuman(self, x, y, grid):
        if x >= 0 and x < len(grid) and y >= 0 and y < len(grid[0]) and grid[x][y] == PEOPLE:
            return True


sol = Solution()
grid = [[0,1,2,0,0],[1,0,0,2,1],[0,1,0,0,0]]
ans = sol.zombie(grid)
print('days:', ans)