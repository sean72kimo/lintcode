from sys import maxsize
class Solution:
    """
    @param: costs: n x 3 cost matrix
    @return: An integer, the minimum cost to paint all houses
    """
    def minCost(self, costs):
        # write your code here

        n = len(costs)
        if n == 0:
            return 0

        f = [[maxsize for i in range(3)]for j in range(n + 1)]
        g = [[maxsize for i in range(3)]for j in range(n + 1)]

        f[0][0] = 0
        f[0][1] = 0
        f[0][2] = 0

        # 前i house:h0, h1, h2...hi-1
        for i in range(1, n + 1):
            # color of house i-1
            for j in range(3):
                # color of house i-2
                for k in range(3):
                    if k == j:
                        continue
                    f[i][j] = min(f[i][j], f[i - 1][k] + costs[i - 1][j])



        return min(f[n][0], f[n][1], f[n][2])

costs = [[14, 2, 11], [11, 14, 5], [14, 3, 10]]
costs = [[1, 2, 3], [1, 4, 6]]
ans = Solution().minCost(costs)
print("ans:", ans)
