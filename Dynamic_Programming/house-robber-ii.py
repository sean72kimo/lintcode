class Solution:
    """
    @param: nums: An array of non-negative integers.
    @return: The maximum amount of money you can rob tonight
    """
    def houseRobber2(self, nums):
        # write your code here
        if nums is None or len(nums) == 0:
            return 0
        if len(nums) == 1:
            return nums[0]

        n = len(nums)

        # case 1, rob the first house
        nums1 = nums[0:n - 1]
        rob = 0
        no_rob = 0
        for val in nums1:
            pre = max(rob, no_rob)
            rob = no_rob + val
            no_rob = pre
        case1 = max(rob, no_rob)

        # case 2, don't rob the first house
        nums2 = nums[1:n]
        rob = 0
        no_rob = 0

        for val in nums2:
            pre = max(rob, no_rob)
            rob = no_rob + val
            no_rob = pre
        case2 = max(rob, no_rob)

        print(nums1, nums2)
        return max(case1, case2)




nums = [3, 6, 4]
nums = [2]
print('ans:', Solution().houseRobber2(nums))
