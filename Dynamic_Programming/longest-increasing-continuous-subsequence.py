class Solution:
    """
    @param: A: An array of Integer
    @return: an integer
    """
    def longestIncreasingContinuousSubsequence(self, A):
        # write your code here
        pass

        n = len(A)
        if n == 0:
            return 0

        ans = 1
        left = 1
        i = 0
        while i < n  :
            if i == n - 1:
                break

            if A[i] < A[i + 1]:
                left = left + 1
            else:
                left = 1
            ans = max(ans, left)
            i = i + 1

        right = 1
        i = n - 1
        while i > -1  :
            if i == 0:
                break

            if A[i] < A[i - 1]:
                right = right + 1
            else:
                right = 1
            ans = max(ans, right)
            i = i - 1


        return ans
A = [5, 2 , 1, 0, 3, 4]
A = [99, 55, 7, 29, 80, 33, 19, 23, 6, 35, 40, 27, 44, 74, 5, 17, 52, 36, 67, 32, 37, 42, 18, 77, 66, 62, 97, 79, 60, 94, 30, 2, 85, 22, 26, 91, 3, 16, 8, 0, 48, 93, 39, 31, 63, 13, 71, 58, 69, 50, 21, 70, 61, 43, 12, 88, 47, 45, 72, 76]

print('ans:', Solution().longestIncreasingContinuousSubsequence(A))
