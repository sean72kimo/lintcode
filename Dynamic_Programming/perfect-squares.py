class Solution_dfs(object):

    def __init__(self):
        self.mem = {}

    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 0

        ans = self.dfs(n)
        return ans

    def dfs(self, n):
        if n in self.mem:
            return self.mem[n]
        if n == 0:
            return 0
        res = n
        j = 1
        while j ** 2 <= n:
            v = n - j ** 2
            rt = self.dfs(v)
            res = min(res, rt)
            j += 1

        self.mem[n] = res + 1
        return self.mem[n]


class Solution(object):

    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """

        f = [i for i in range(n + 1)]

        for i in range(2, n + 1):
            j = 0
            f[i] = i

            while i - j ** 2 >= 0:
                tmp = j * j
                f[i] = min(f[i], f[i - tmp] + 1)
                j += 1

        return f[-1]


n = 12

print('ans:', Solution().numSquares(n))



class Solution3(object):
    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 0
        if n == 1:
            return 1

        f = [float('inf') for _ in range(n + 1)]

        for i in range(n + 1):
            if i ** 2 > n:
                break
            f[i] = 1

        print(f)
        for i in range(n + 1):
            if i == 0:
                f[i] = 0
                continue

            for j in range(1, i + 1):
                if j ** 2 > i:
                    break
                if i - j ** 2 >= 0 and f[i - j ** 2] != float('inf'):

                    f[i] = min(f[i], f[i - j ** 2] + 1)
        print(f)
        return f[-1]



print('ans:', Solution3().numSquares(n))