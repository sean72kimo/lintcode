class Solution_dfs(object):
    def minimumTotal(self, tri):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if len(tri) == 0:
            return 0
        self.mem = {}
        return self.dfs(tri, 0, 0)

    def dfs(self, tri, i, j):
        if (i, j) in self.mem:
            return self.mem[(i, j)]

        dxy = [[1, 0], [1, 1]]

        curr = tri[i][j]
        rt = float('inf')
        for dx, dy in dxy:
            nx = i + dx
            ny = j + dy

            if not (0 <= nx < len(tri) and 0 <= ny < len(tri[nx])):
                continue
            rt = min(self.dfs(tri, nx, ny), rt)

        if rt == float('inf'):
            res = curr
        else:
            res = curr + rt

        self.mem[(i, j)] = res
        return res

class Solution:
    """
    @param: triangle: a list of lists of integers
    @return: An integer, minimum path sum
    """
    def minimumTotal(self, triangle):
        # write your code here
        if triangle is None or len(triangle) == 0 :
            return -1
        if triangle[0] is  None or len(triangle) == 0:
            return -1

        # state: f[i][j] = minimum path value from 0,0 to i,j
        n = len(triangle)
        f = [[0 for i in range(n)] for j in range(n)]

        f[0][0] = triangle[0][0]

        for i in range(1, n):
            f[i][0] = f[i - 1][0] + triangle[i][0]
            f[i][i] = f[i - 1][i - 1] + triangle[i][i]

        for i in f:
            print(i)

        for i in range(1, n):
            for j in range(1, i):
                f[i][j] = min(f[i - 1][j], f[i - 1][j - 1]) + triangle[i][j]

        print('====')
        for i in f:
            print(i)

        ans = f[n - 1][0];
        for i in range(1, n):
            ans = min(ans, f[n - 1][i])

        return ans

triangle = [
     [2],
    [3, 4],
   [6, 5, 7],
  [4, 1, 8, 3]
]
print('ans:', Solution().minimumTotal(triangle))

