# 1,3,4
n = 5
f = [0 for i in range(n + 1)]
f[0] = 1
f[1] = 1
f[2] = 1
f[3] = 2

for i in range(4, n + 1):
    f[i] = f[i - 4] + f[i - 3] + f[i - 1] + 3

print(f)
