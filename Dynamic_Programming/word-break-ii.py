import collections
"""
Time:  O(n * l^2 + n * r), l is the max length of the words,
                           r is the number of the results.
Space: O(n^2)
"""

class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: List[str]
        """
        if len(wordDict) == 0 or len(s) == 0:
            return []
        dic = set(wordDict)
        n = len(s)
        f = [False for _ in range(n + 1)]
        valid = collections.defaultdict(bool)

        for i in range(n + 1):
            if i == 0:
                f[i] = True
                continue

            for j in range(i):
                if s[j:i] in dic and f[j]:
                    f[i] = True
                    valid[(j, i - 1)] = True

        ans = []
        if f[n] == True:
            self.dfs(s, valid, 0, [], ans)
        return ans

    def dfs(self, s, valid, start, path, ans):
        if start == len(s):
            ans.append(" ".join(path))
            return

        for i in range(start, len(s)):
            if valid[(start, i)]:
                sub = s[start: i + 1]
                path.append(sub)
                self.dfs(s, valid, i + 1, path, ans)
                path.pop()


class Solution_dfs(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: List[str]
        """
        if len(wordDict) == 0 or len(s) == 0:
            return []

        dic = set(wordDict)
        self.mem = {}
        ans = self.dfs(s, dic, 0)
        return ans

    def dfs(self, s, dic, start):
        if start in self.mem:
            return self.mem[start]

        if start == len(s):
            return []

        res = []
        for i in range(start, len(s)):
            sub = s[start: i + 1]
            if sub not in dic:
                continue

            ret = self.dfs(s, dic, i + 1)
            for itm in ret:
                if itm:
                    string = sub + " " + itm
                    res.append(string)

        if s[start:] in dic:
            res.append(s[start:])

        self.mem[start] = res
        return res

s = "catsanddog"
wordDict = ["cat", "cats", "and", "sand", "dog"]
# s = "abcd"
# wordDict = ["a", "abc", "b", "cd"]

print('ans:', Solution().wordBreak(s, wordDict))



