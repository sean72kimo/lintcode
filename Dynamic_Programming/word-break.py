"""
time O(L * L * n) L = len(s) , n = len(substring s[j:i])
space O(L)
"""
class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        if len(wordDict) == 0:
            return False

        dic = set(wordDict)
        n = len(s)
        f = [False for _ in range(n + 1)]
        idx = -1
        p = [-1 for _ in range(n + 1)]

        for i in range(n + 1):
            if i == 0:
                f[i] = True
                continue

            for j in range(i):
                if s[j:i] in dic and f[j] is True:

                    f[i] = True
                    p[i] = j
                    idx = i
                    break
        # 打印路徑
        res = []
        while idx != -1:
            res.append(idx)
            idx = p[idx]
        res = res[::-1]

        start = 0
        for i in range(1, len(res)):
            sub = s[res[i-1] : res[i]]
            print(sub)

        return f[n]


class Solution_dfs(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        if len(wordDict) == 0:
            return False

        dic = set(wordDict)
        self.mem = {}
        return self.dfs(s, dic, 0)

    def dfs(self, s, dic, start):
        if start in self.mem:
            return self.mem[start]

        if start == len(s):
            return True

        res = False
        for i in range(start, len(s)):
            sub = s[start: i + 1]

            if sub not in dic:
                continue

            if self.dfs(s, dic, i + 1):
                self.mem[start] = True
                return True

        self.mem[start] = res
        return res





class Solution_dfs_print_path(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        if len(wordDict) == 0:
            return False

        dic = set(wordDict)
        self.mem = {}
        path = []
        res = self.dfs(s, dic, 0, path)
        # 打印路徑
        print(path)
        return res

    def dfs(self, s, dic, start, path):
        if start in self.mem:
            return self.mem[start]

        if start == len(s):
            return True

        res = False
        for i in range(start, len(s)):
            sub = s[start: i + 1]

            if sub not in dic:
                continue
            path.append(sub)
            if self.dfs(s, dic, i + 1, path):
                self.mem[start] = True
                return True
            path.pop()

        self.mem[start] = res
        return res



s = "lintcode"
dict = ["li", "nt", "code", "in", "int"]
a = Solution().wordBreak(s, dict)
print("ans:", a)

