class Solution:
    """
    @param: n: An integer
    @return: A boolean which equals to true if the first player will win
    """
    def firstWillWin(self, n):
        # write your code here
        if n == 0:
            return False

        if n == 1 or n == 2:
            return True

        f = [None for _ in range(n + 1)]

        f[0] = False
        f[1] = f[2] = True

        for i in range(3, n + 1):
            f[i] = not f[f - 1] or  not f[f - 2]

        return f[n]

a = Solution().firstWillWin(10)
print("ans:", a)
