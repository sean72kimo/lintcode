class Solution:
    """
    @param values: a vector of integers
    @return: a boolean which equals to true if the first player will win
    """
    def firstWillWin(self, values):

        if len(values) == 0:
            return False

        n = len(values)
        dp = [[None for _  in range(n+1)] for _  in range(n+1)] # value between index i and index j
        rsum = [0 for _  in range(n+1)]
        # sum i to j = rsum[i][j] = sum[j] - sum[i-1]
        
        for i in range(1, n + 1):
            rsum[i] = rsum[i-1] + values[i-1]
        print("  ",values)
        print(rsum)
        
        
        rsum = [0]
        for v in values:
            rsum.append(rsum[-1] + v)
        print("  ",values)
        print(rsum)
        
        for i in range(0, n + 1): # i: len of range
            for j in range(1, n + 1): # j: starting index of the range
                if j + i > n: # length greater than
                    break
                
                if i == 0: # range = 0
                    dp[j][j] = values[j-1] # from index j to index j
                    continue

                dp[j][j+i] = rsum[j+i] - rsum[j-1] - min(dp[j+1][j+i], dp[j][j+i-1])
        
        print(dp[1][n])
        return dp[1][n] > sum(values) // 2
    
values = [1,2,3,4,5,6,7,8,13,11,10,9]
a = Solution().firstWillWin(values)
print("ans:", a)
