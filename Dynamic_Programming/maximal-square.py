class Solution:
    """
    @param: matrix: a matrix of 0 and 1
    @return: an integer
    """
    def maxSquare(self, matrix):
        # write your code here

        if not any(matrix):
            return 0

        n = len(matrix)
        m = len(matrix[0])
        f = [[ 0 for i in range(m)] for j in range(n)]


        ans = 0

        for i in range(n):
            f[i][0] = matrix[i][0]

        for j in range(m):
            f[0][j] = matrix[0][j]

        for i in range(n):
            for j in range(m):
                if i and j and matrix[i][j]:
                    f[i][j] = min(f[(i - 1)][j - 1], f[(i - 1)][j], f[i][j - 1]) + 1

                ans = max(f[i][j], ans)


        for row in f:
           print(row)

        return ans * ans



matrix = [[1, 0, 1, 0, 0], [1, 0, 1, 1, 1], [1, 1, 1, 1, 1], [1, 0, 0, 1, 0]]
for row in matrix:
    print(row)
print('~~~')
print('ans:', Solution().maxSquare(matrix))
