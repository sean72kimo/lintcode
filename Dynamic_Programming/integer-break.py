class Solution(object):
    def __init__(self):
        self.ans = float('-inf')
        self.mem = []

    def integerBreak(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n <= 1:
            return n

        self.dfs(n, 1, 0)
        return self.ans

    def dfs(self, n, p, cnt):
        if n == 0:
            if cnt < 2:
                return
            self.ans = max(p, self.ans)
            return

        if n < 0:
            return

        for i in range(1, n + 1):
            sofar = p * i
            remain = n - i

            self.dfs(remain, sofar, cnt + 1)




class SolutionMem(object):
    def __init__(self):
        self.ans = float('-inf')
        self.mem = []

    def integerBreak(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n <= 2:
            return 1
        self.mem = [-1 for _ in range(n + 1)]

        return self.dfs(n, 1, 0)


    def dfs(self, n, p, cnt):
        if self.mem[n] != -1:
            return self.mem[n]

        if n == 0:
            if cnt < 2:
                return 0
            return 1


        res = 1
        maxP = float('-inf')
        for i in range(1, n + 1):
            sofar = p * i
            remain = n - i
            rt = self.dfs(remain, sofar, cnt + 1)
            res = sofar * rt
            if res > maxP:
                maxP = res


        self.mem[n] = maxP
        return maxP


n = 3
a = Solution().integerBreak(n)
print("ans:", a)
b = SolutionMem().integerBreak(n)
print("ans:", b, a == b)
