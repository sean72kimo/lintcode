class Solution(object):
    def getMoneyAmount(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 1:
            return 0
        self.mem = {}
        return self.dfs(1, n)

    def dfs(self, left, right):
        if (left, right) in self.mem:
            return self.mem[(left, right)]

        if left >= right:
            return 0

        res = float('inf')
        for i in range(left, right + 1):
            penalty1 = self.dfs(left, i - 1)
            penalty2 = self.dfs(i + 1, right)
            res = min(res, i + max(penalty1, penalty2))

        self.mem[(left, right)] = res
        return res