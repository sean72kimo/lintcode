class Solution1:
    """
    @param maxChoosableInteger: a Integer
    @param desiredTotal: a Integer
    @return: if the first player to move can force a win
    """

    def canIWin(self, maxChoosableInteger, desiredTotal):
        if (1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal:
            return False
        self.memo = {}
        nums = list(range(1, maxChoosableInteger + 1))
        return self.helper(nums, desiredTotal)

    def helper(self, nums, desiredTotal):

        s = str(nums)
        if s in self.memo:
            return self.memo[s]

        # also ok
        # if nums[-1] >= desiredTotal:
        #     return True
        if desiredTotal <= 0:
            return False

        for i in range(len(nums)):
            if not self.helper(nums[:i] + nums[i+1:], desiredTotal - nums[i]):
                self.memo[s] = True
                return True
        self.memo[s] = False
        return False


class Solution(object):
    def canIWin(self, maxChoosableInteger, desiredTotal):
        """
        :type maxChoosableInteger: int
        :type desiredTotal: int
        :rtype: bool
        """
        if desiredTotal <= maxChoosableInteger:
            return True
        if (1 + maxChoosableInteger) * maxChoosableInteger / 2 < desiredTotal:
            return False
        self.memo = {}
        chosen = [False] * (maxChoosableInteger + 1)

        return self.helper(maxChoosableInteger, desiredTotal, chosen)

    def helper(self, maxChoosableInteger, desiredTotal, chosen):
        if desiredTotal <= 0:
            return False

        s = str(chosen)
        if s in self.memo:
            return self.memo[s]

        for i in range(1, maxChoosableInteger + 1):
            if chosen[i]:
                continue

            chosen[i] = True
            if not self.helper(maxChoosableInteger, desiredTotal - i, chosen):
                self.memo[s] = True
                chosen[i] = False
                return True
            chosen[i] = False

        self.memo[s] = False
        return False

maxChoosableInteger, desiredTotal, exp = 10, 11, False
maxChoosableInteger, desiredTotal, exp = 10, 40, False
maxChoosableInteger, desiredTotal, exp = 20, 200, False
maxChoosableInteger, desiredTotal, exp = 4, 6, True
sol = Solution()
a = sol.canIWin(maxChoosableInteger, desiredTotal)
print("ans:", a)