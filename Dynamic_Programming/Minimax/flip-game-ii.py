class Solution(object):
    def canWin(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if len(s) < 2:
            return False
        self.mem = {}
        return self.dfs(s)

    def dfs(self, s):
        if s in self.mem:
            return self.mem[s]

        res = False
        for i in range(len(s) - 1):
            if s[i:i + 2] == '++':
                sub = s[:i] + '--' + s[i + 2:]

                if not self.dfs(sub):
                    res = True
        self.mem[s] = res
        return res

s = '++++'
a = Solution().canWin(s)
print("ans:", a)