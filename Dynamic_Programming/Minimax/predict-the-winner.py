class Solution(object):
    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) == 1:
            return True
        self.memo = {}

        score = self.helper(sum(nums), nums, 0, len(nums) - 1)
        return score * 2 >= sum(nums)

    def helper(self, summ, nums, start, end):
        if (start, end) in self.memo:
            return self.memo[(start, end)]

        if start == end:
            self.memo[(start, end)] = nums[start]
            return nums[start]

        score1 = summ - self.helper(summ - nums[start], nums, start + 1, end)
        score2 = summ - self.helper(summ - nums[end], nums, start, end - 1)

        res = max(score1, score2)
        self.memo[(start, end)] = res

        return res

class Solution2(object):
    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) <= 1:
            return True
        self.memo = {}
        summ = sum(nums)
        score = self.helper(nums, summ)
        return score *2 >= summ

    def helper(self, nums, summ):
        s = str(nums)
        if s in self.memo:
            return self.memo[s]

        if len(nums) == 1:
            return nums[0]

        nxt = nums[1:]
        score1 = summ - self.helper(nxt, summ - nums[0])

        nxt = nums[:-1]
        score2 = summ - self.helper(nxt, summ - nums[-1])

        res = max(score1, score2)
        self.memo[s] = res
        return res

nums, exp = [1], True
nums, exp = [1,5,2], False
# nums, exp = [1,5,233, 2], True
sol = Solution()
a = sol.PredictTheWinner(nums)
print("ans:", a)

print('-----')
sol2 = Solution2()
a = sol2.PredictTheWinner(nums)
print("ans:", a)