class Solution(object):
    def numDistinct(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: int

        A[m-1] == B[n-1]
        f[i][j] =  f[i-1][j-1]

        A[m-1] != B[n-1]
        f[i][j] =  f[i-1][j]
        """
        m = len(s)
        n = len(t)

        if m < n:
            return 0

        f = [[0 for _ in range(n + 1)] for _ in range(m + 1)]

        for i in range(m + 1):
            for j in range(n + 1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0:  # 沒有s字串，那麼只能構成0種t字串
                    continue

                if j == 0:  # 如果沒有t字串，那麼s構建成子字串的方式為一種，就是s什麼都不選取
                    f[i][j] = 1
                    continue

                # s最後一個不選取，那麼就看s[:i-1] 子串裡面能夠見幾個t
                f[i][j] = f[i - 1][j]

                # s最後一個字符相等並且選取，那麼就加上 s[:i-1] 構建成 t[:i-1]的個數
                if i > 0 and j > 0 and s[i - 1] == t[j - 1]:
                    f[i][j] = f[i][j] + f[i - 1][j - 1]

        return f[m][n]

class Solution(object):
    def numDistinct(self, A, B):
        """
        :type s: str
        :type t: str
        :rtype: int
        """

        """
        A[m-1] == B[n-1]
        f[i][j] =  f[i-1][j-1]

        A[m-1] != B[n-1]
        f[i][j] =  f[i-1][j]
        """
        m = len(A)
        n = len(B)

        if m < n:
            return 0

        # f[m][n]
        f = [[None for j in range(n + 1)] for i in range(m + 1)]
        f[0][0] = 1

        for j in range(1, n + 1):
            f[0][j] = 0

        printf(f)
        for i in range(1, m + 1):
            for j in range(n + 1):
                f[i][j] = f[i - 1][j]

                if j > 0 and A[i - 1] == B[j - 1]:
                    f[i][j] = f[i][j] + f[i - 1][j - 1]

        printf(f)
        return f[m][n]

A = "rabbbit"
B = "rabbit"
ans = Solution().numDistinct(A, B)
print('ans:', ans)
