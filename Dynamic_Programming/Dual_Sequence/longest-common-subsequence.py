def printf(f):
    print('=======================================')
    print('[')
    for r in f:
        print(r)
    print(']')
class Solution:
    """
    @param: A: A string
    @param: B: A string
    @return: The length of longest common subsequence of A and B
    """
    def longestCommonSubsequence(self, A, B):
        # write your code here
        # f[i][j] = max{f[i-1][j], f[i][j-1], f[i-1][j-1] but A[i-1]==B[i-1]  }

        m = len(A)
        n = len(B)

        f = [[None for _ in range(n + 1)] for _ in range(m + 1)]
        printf(f)

        for i in range(m + 1):
            for j in range(n + 1):

                # init
                if i == 0 or j == 0:
                    f[i][j] = 0
                    continue

                f[i % 2][j] = max(f[(i - 1) % 2][j], f[i % 2][j - 1])
                if A[i - 1] == B[j - 1]:
                    f[i % 2][j] = max(f[i % 2][j], f[(i - 1) % 2][j - 1] + 1)

        return f[m % 2][n]
