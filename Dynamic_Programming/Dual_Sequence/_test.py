class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
#         if len(s) == 0 and len(p) == 0:
#             return True
#         if len(p) == 0:
#             return False

        m = len(s)
        n = len(p)

        f = [[None for _ in range(m + 1)] for _ in range(n + 1)]



        for i in range(n + 1):
            for j in range(m + 1):
                
                if i == 0 and  j == 0:
                    f[i][j] = True
                    continue
                
                if i == 0:
                    f[i][j] = False
                    continue
                
                f[i][j] = False
                
                print((i,j))
                for r in f:
                    print(r)
                
                if (j - 1 >= 0) and (p[i - 1] == '.' or p[i - 1] == s[j - 1]):
                    f[i][j] |= f[i - 1][j - 1]

                if p[i - 1] == '*':
                    f[i][j] |= f[i - 2][j]

                    if (j - 1 >= 0 and i - 2 >= 0) and (p[i - 2] == '.' or p[i - 2] == s[j - 1]):
                        f[i][j] = f[i - 2][j] | f[i][j - 1]

        return f[n][m]
s = "aa"
p = ".*"



s = "aa"
p = "aa*"


a = Solution().isMatch(s, p)
print("ans:", a)



class Solution2(object):
    def isMatch(self, s, p):
        m = len(s)
        n = len(p)
        # f[m][n]
        f = [[False for _ in range(n + 1)] for _ in range(m + 1)]

        for i in range(m + 1):
            for j in range(n + 1):
                if i == 0 and j == 0:
                    f[i][j] = True
                    continue

                if j == 0:
                    f[i][j] = False
                    continue

                if p[j - 1] == "*":
                    f[i][j] |= f[i][j - 1]
                    # use *
                    if i - 1 >= 0:
                        f[i][j] |= f[i - 1][j]

                if (i - 1 >= 0) and (p[j - 1] == "?" or p[j - 1] == s[i - 1]):
                    f[i][j] |= f[i - 1][j - 1]

        return f[m][n]
# print()
# string = "{0:08b}".format(5)
# print(string)
# 
# stack = ['a', 'b', 'c']
# 
# print(pow(0.25, -2))


