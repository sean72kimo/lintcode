class Solution(object):
    def minDistance(self, A, B):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        m = len(A)
        n = len(B)

        f = [[0 for j in range(n + 1)] for i in range(m + 1)]


        for i in range(m + 1):
            for j in range(n + 1):
                if i == 0:
                    f[i][j] = j
                    continue
                if j == 0:
                    f[i][j] = i
                    continue

                if A[i - 1] == B[j - 1]:
                    f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1, f[i][j])
                else:
                    f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1, f[i - 1][j - 1] + 1)


        return f[m][n]
A = ""
B = "a"
ans = Solution().minDistance(A, B)
print("ans:", ans)
