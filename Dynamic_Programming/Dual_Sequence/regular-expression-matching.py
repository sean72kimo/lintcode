def printf(f):
    print('=======================================')
    print('[')
    for r in f:
        print(r)
    print(']')

class Solution:
    """
    @param s: A string
    @param p: A string includes "." and "*"
    @return: A boolean
    """
    def isMatch(self, s, p):
        # write your code here
        """
        # B[j-1] = '.' or or A[i-1] = B[j-1]
        f[i][j] |= f[i-1][j-1]  , i > 0

        OR

        # B[j-1] = '*'
        f[i][j] = following 3 cases
                # case1: # ignore ".*" or "x*" at the end of B,
                |= f[i][j-2]

                # case2: ".*" at the end of B
                |= f[i-1][j], B[j-2] = '.'

                # case3: "x*" at the end of B
                |= f[i-1][j], B[j-2] = A[i-1]
        """
        m = len(s)
        n = len(p)

        # f[m][n]
        f = [[False for _ in range(n + 1)] for _ in range(m + 1)]
        f[0][0] = True

        for i in range(0, m + 1):
            for j in range(0, n + 1):

                if i == 0 and j == 0:
                    f[i][j] = True
                    continue

                if j == 0:
                    f[i][j] = False
                    continue

                f[i][j] = False

                if p[j - 1] == '*':
                    if j > 1:
                        f[i][j] |= f[i][j - 2]

                        if i > 0 and (p[j - 2] == '.' or p[j - 2] == s[i - 1]):
                            f[i][j] |= f[i - 1][j]

                else:
                    if i > 0 and (s[i - 1] == p[j - 1] or p[j - 1] == '.'):
                        f[i][j] |= f[i - 1][j - 1]


        return f[m][n]


ans = Solution().isMatch("aab", "c*a*b")
ans = Solution().isMatch("", ".*")
print('ans:', ans)
