def printf(f):
    print('=======================================')
    print('[')
    for r in f:
        print(r)
    print(']')

class Solution:
    """
    @param: s1: A string
    @param: s2: A string
    @param: s3: A string
    @return: Determine whether s3 is formed by interleaving of s1 and s2
    """
    def isInterleave(self, s1, s2, s3):
        # write your code here
        m = len(s1)
        n = len(s2)

        if m + n != len(s3):
            return False

        f = [[False for j in range(n + 1)] for i in range(m + 1)]
        f[0][0] = True

        for i in range(m + 1):
            for j in range(n + 1):
                k = i + j  # len of s3


                if i > 0:
                    if s3[k - 1] == s1[i - 1]:
                        f[i][j] |= f[i - 1][j]
                if  j > 0:
                    if s3[k - 1] == s2[j - 1]:
                        f[i][j] |= f[i][j - 1]


        return f[m][n]
s1 = "aba"
s2 = "a"
s3 = "aaba"
ans = Solution().isInterleave(s1, s2, s3)
print('ans:', ans)
