class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """

        m = len(s)
        n = len(p)

        f = [[None for j in range(n + 1)] for i in range(m + 1)]

        for i in range(m + 1):
            for j in range(n + 1):
                if j == 0:
                    f[i][j] = (i == 0)
                    continue

                f[i][j] = False

                if p[j - 1] == '*':
                    # not use *
                    f[i][j] |= f[i][j - 1]

                    # use *
                    if i > 0:
                        f[i][j] |= f[i - 1][j]

                else:
                    if i > 0 and (p[j - 1] == '?' or s[i - 1] == p[j - 1]):
                        f[i][j] |= f[i - 1][j - 1]

        return f[m][n]
