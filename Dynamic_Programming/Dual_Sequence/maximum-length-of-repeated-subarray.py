def printf(f):
    print('=======================================')
    print('[')
    for r in f:
        print(r)
    print(']')

class Solution(object):
    def findLength(self, A, B):
        """
        :type A: List[int]
        :type B: List[int]
        :rtype: int
        """

        m = len(A)
        n = len(B)

        ans = 0
        f = [[0 for j in range(n + 1)] for i in range(m + 1)]

        for i in range(1, m + 1):
            for j in range(1, n + 1):

                if A[i - 1] == B[j - 1]:
                    f[i][j] = f[i - 1][j - 1] + 1

                ans = max(ans, f[i][j])

        return ans

    def findLength_rolling(self, A, B):
        """
        :type A: List[int]
        :type B: List[int]
        :rtype: int
        """

        m = len(A)
        n = len(B)

        ans = 0
        f = [[0 for j in range(n + 1)] for i in range(2)]
        OLD = 0
        CUR = 1

        ans = 0
        for i in range(1, m + 1):
            for j in range(1, n + 1):

                if A[i - 1] == B[j - 1]:
                    f[CUR][j] = f[OLD][j - 1] + 1

                    ans = max(ans, f[CUR][j])

            for j in range(n + 1):
                f[OLD][j] = f[CUR][j]
                f[CUR][j] = 0

        return ans

A = [0, 0, 0, 0, 0]
B = [0, 0, 0, 0, 0]
A = "ja;jfadflakjdfa;djfadfdnvadbfkbH:DADHFLDSHfakldhflakdf;adfasdhfaufhakdbalbgaldbalkdfafhalkdsfhalsdufhakldbakladshfuojfanjoiehflakshf"
B = "adfanbajfasdjfaodjfaldfnasldfjao"
# ans = Solution().findLength(A, B)
ans = Solution().findLength_rolling(A, B)
print('ans', ans)
