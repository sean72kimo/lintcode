def printf(f):
    print('[')
    for r in f:
        print(r)
    print(']')
class Solution:
    """
    @param: A: A string
    @param: B: A string
    @return: the length of the longest common substring.
    """
    def longestCommonSubstring(self, A, B):
        # write your code here

        m = len(A)
        n = len(B)

        f = [[0 for i in range(n + 1)] for j in range(m + 1)]
        ans = 0
        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if A[i - 1] == B[j - 1]:
                    f[i][j] = f[i - 1][j - 1] + 1
        printf(f)
        return max(max(r) for r in f)

A = "ja;jfadflakjdfa;djfadfdnvadbfkbH:DADHFLDSHfakldhflakdf;adfasdhfaufhakdbalbgaldbalkdfafhalkdsfhalsdufhakldbakladshfuojfanjoiehflakshf"
B = "adfanbajfasdjfaodjfaldfnasldfjao"
ans = Solution().longestCommonSubstring(A, B)
print('ans:', ans)
