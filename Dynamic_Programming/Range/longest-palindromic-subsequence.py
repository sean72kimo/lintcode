import collections

"""
similar to longest-palindromic-substring
"""
class Solution_dfs_mem(object):
    def longestPalindromeSubseq(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n <= 1:
            return n

        self.mem = {}
        return self.dfs(s, 0, n - 1)

    def dfs(self, s, i, j):
        if (i, j) in self.mem:
            return self.mem[(i, j)]

        if i == j:
            return 1

        if i + 1 == j and s[i] == s[j]:
            return 2

        res = 0
        if s[i] == s[j]:
            res += self.dfs(s, i + 1, j - 1) + 2
        else:
            res = max(self.dfs(s, i, j - 1), self.dfs(s, i + 1, j))

        self.mem[(i, j)] = res
        return res

class Solution(object):
    def longestPalindromeSubseq(self, s):
        """
        :type s: str
        :rtype: int
        """

        """
        palin string T in S
        n = len(S)
        m = len(T)
        T = S[i...j]
        T[0] == T[m-1]

        k = len(sub_T)
        sub_T = S[i+1...j-1]
        sub_T[0] == sub_T[k-1] --> T[1] == T[m-2]

        f[i][j] =  len of longest palindrome in s[i....j]

        f[i][j] = max(f[i+1][j],f[i][j-1], f[i+1][j-1]+2 if s[i]==s[j])
        """

        n = len(s)
        if n <= 1:
            return n

        f = [[0 for _ in range(n)] for _ in range(n)]
        p = [[0 for _ in range(n)] for _ in range(n)]

        # init
        for i in range(n):
            f[i][i] = 1

            if i + 1 < n:
                f[i][i + 1] = 2 if s[i] == s[i + 1] else 1

        for size in range(3, n + 1):
            for i in range(n):

                j = i + size - 1

                if j >= n :
                    break
                if s[i] == s[j]:
                    f[i][j] = max(f[i][j], f[i + 1][j - 1] + 2)
                else:
                    f[i][j] = max(f[i + 1][j], f[i][j - 1])

        return f[0][n - 1]

    def longestPalindromeSubseq2(self, s):

        n = len(s)
        if n <= 1:
            return n

        f = [[0 for _ in range(n)] for _ in range(n)]


        for i in range(n - 1, -1, -1):
            f[i][i] = 1
            for j in range(i + 1, n):
                if s[i] == s[j]:
                    f[i][j] = f[i + 1][j - 1] + 2
                else:
                    f[i][j] = max(f[i + 1][j], f[i][j - 1])

        return f[0][n - 1]


# 打印路徑
class Solution_print_path(object):
    def longestPalindromeSubseq(self, s):
        n = len(s)
        if n <= 1:
            return n

        f = [[0 for _ in range(n)] for _ in range(n)]
        p = collections.defaultdict(str)
        q = ""

        # init
        for i in range(n):
            f[i][i] = 1
            p[i,i] = s[i]
            if i + 1 < n:
                if s[i] == s[i + 1]:
                    f[i][i + 1] = 2
                    p[i, i+1] = s[i:i+2]
                    # q = s[i:i+2]
        print(p)
        # print(q)
        for size in range(3, n + 1):
            for i in range(n):

                j = i + size - 1

                if j >= n :
                    break

                if s[i] == s[j]:
                    if f[i + 1][j - 1] + 2 > f[i][j]:
                        f[i][j] = f[i + 1][j - 1] + 2
                        p[i,j] = s[i] + p[i+1, j-2] + s[j]


                else:

                    if f[i + 1][j] > f[i][j - 1]:
                        f[i][j] = f[i + 1][j]
                        p[i, j] = p[i + 1, j]

                    else:
                        f[i][j] = f[i][j-1]
                        p[i, j] = p[i, j-1]

        return f[0][n - 1]

s = "bbbab"
a = Solution_print_path().longestPalindromeSubseq(s)
print("ans:", a)

# 打印路徑
class Solution_dfs_print_path(object):
    def longestPalindromeSubseq(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n <= 1:
            return n

        self.mem = {}
        path = ""
        res, palin = self.dfs(s, 0, n - 1)
        return res, palin

    def dfs(self, s, i, j):
        if (i, j) in self.mem:
            return self.mem[(i, j)]

        if i == j:
            return 1, s[i]

        if i + 1 == j and s[i] == s[j]:
            return 2, s[i:i+2]

        res = 0
        if s[i] == s[j]:
            cnt , strs = self.dfs(s, i + 1, j - 1)
            res += cnt + 2
            palin = s[i] + strs + s[j]
        else:
            l_cnt, l_strs = self.dfs(s, i, j - 1)
            r_cnt, r_strs = self.dfs(s, i + 1, j)
            res = max(l_cnt, r_cnt)
            palin = l_strs if l_cnt > r_cnt else r_strs

        self.mem[(i, j)] = res, palin
        return res, palin

s = "bbbab"
a, palin = Solution_dfs_print_path().longestPalindromeSubseq(s)
print("ans:", a, palin)

