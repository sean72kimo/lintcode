"""
倒過來想：假如最後剩下 氣球 nums[i],所獲得的金幣數為 nums[0] * nums[i] * nums[-1]
子問題：
如何求 nums[0]~nums[i]最大金幣數？ 這時候左右的“牆”變為 nums[0], nums[i]
如何求 nums[i+1]~nums[-1]最大金幣數？ 這時候左右的“牆”變為 nums[i], nums[-1]

k 是最後被扎破的氣球
f[i][j] 代表 [i+1 ~ j-1](閉區間)  i, j為牆壁
f[i][j] = max of f[i][k] + f[k][j] + a[i]*a[k]*a[j]
max i~k, + max k~j + cur a[i]*a[k]*a[j]
"""
class Solution(object):
    def maxCoins(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        """
        f[i][j] = max of f[i][k] + f[k][j] + a[i]*a[k]*a[j]
        """
        A = [1] + nums[:] + [1]

        n = len(A)
        f = [[0 for _ in range(n)] for _  in range(n)]

        # for i in range(n):
        #    if i + 1 >= n:
        #        break
        #    f[i][i + 1] = 0

        for size in range(3, n + 1):
            for i in range(n):

                j = i + size - 1
                if j >= n:
                    break

                # f[i][j] = 0 #init, no gold at first

                for k in range(i + 1, j):
                    f[i][j] = max(f[i][j], f[i][k] + f[k][j] + A[i] * A[k] * A[j])

        return f[0][n - 1]


class Solution_dfs(object):
    def maxCoins(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        nums = [1] + nums + [1]
        n = len(nums)
        self.mem = {}
        ans = self.dfs(nums, 0, n - 1)
        return ans

    def dfs(self, nums, i, j):
        if (i, j) in self.mem:
            return self.mem[(i, j)]

        if i + 2 == j:
            return nums[i] * nums[i + 1] * nums[j]

        res = 0
        for k in range(i + 1, j):
            res = max(res, self.dfs(nums, i, k) + self.dfs(nums, k, j) + nums[i] * nums[k] * nums[j])

        self.mem[(i, j)] = res
        return res


nums = [3, 1, 5, 8]
nums = [4, 1, 5, 10]
a = Solution_dfs().maxCoins(nums)
print('ans:', a)

