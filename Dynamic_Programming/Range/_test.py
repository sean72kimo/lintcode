class Solution:
    """
    @param s1: A string
    @param s2: Another string
    @return: whether s2 is a scrambled string of s1
    """
    def isScramble(self, s1, s2):
        # write your code here
        m = len(s1)
        n = len(s2)
        
        if m != n:
            return False
            
        f = [[[False for _ in range(n+1)] for _ in range(n)] for _ in range(n)]

        for i in range(n):
            for j in range(n):
                if s1[i] == s2[j]:
                    f[i][j][1] = True
                else:
                    f[i][j][1] = False
                    
        # k is length
        for k in range(2, n+1):
            for i in range(n - k + 1):
                for j in range(n - k + 1):
                    for w in range(1, k):
                        
                        if f[i][j][w] and f[i + w][j + w][k - w]:
                            f[i][j][k] = True
                            break
                        
                        if f[i][j + k - w][w] and f[i + w][j][k - w]:
                            f[i][j][k] = True
                            break
                        
        
        return f[0][0][n]

s1 = "great"
s2 = "rgtae"

s1 = "aa"
s2 = "ab"
a = Solution().isScramble(s1, s2)
print("ans", a)
