from sys import maxsize
class Solution(object):
    def canWinNim(self, n):
        """
        :type n: int
        :rtype: bool
        """
        print(maxsize)
        if n < 0 :
            return
        if n < 4:
            return True
        f = [False for i in range(n + 1)]
        f[0], f[1], f[2], f[3] = True, True, True, True

        for i in range(4, n + 1):
            f[i] = not (f[i - 1] and f[i - 2] and f[i - 3])

        return f[-1]
a = Solution().canWinNim(1348820612)
print(a)
