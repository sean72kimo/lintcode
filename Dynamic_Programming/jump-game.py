class Solution:
    """
    @param: A: A list of integers
    @return: A boolean
    """
    def canJump(self, A):
        # write your code here
        if A is None or len(A) == 0:
            return False
        n = len(A)
        can = [False for i in range(n)]
        can[0] = True

        for i in range(n):
            for j in range(i):
                # print(A[j], i)
                if j + A[j] >= i and can[j]:
                    can[i] = True
        print(can)
        return can[n - 1]

# A = [2, 3, 1, 1, 4]
A = [0, 8, 2, 0, 9]
print('ans:', Solution().canJump(A))
