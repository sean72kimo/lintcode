from sys import maxsize
class Solution:
    """
    @param nums: The integer array
    @return: The length of LIS (longest increasing subsequence)
    """
    def longestIncreasingSubsequence(self, nums):
        # write your code here
        # f[i] 跳到"第i"個位置的時候，LIS的長度是多少
        # （意思是：這個LIS，是包含第i個位置的）
        # f[i] "前i"個位置的時候，LIS的長度是多少 # 錯誤
        if nums is None or len(nums) == 0:
            return 0

        n = len(nums)
        f = [1 for i in range(n)]

        for i in range(n):
            for j in range(i):
                if nums[j] < nums[i]:
                    f[i] = max(f[i], f[j] + 1)

# 不懂為什麼上課時候要加這兩段
#         ans = maxsize
#         for i in range(n):
#             ans = min(ans, f[i])

#         maxNum = 0
#         for i in range(n):
#             maxNum = max(maxNum, f[i])

        return max(f)

nums = [1, 1, 1, 1, 1, 1, 1]
print('ans:', Solution().longestIncreasingSubsequence(nums))



from sys import maxsize
class Solution2:
    """
    @param nums: The integer array
    @return: The length of LIS (longest increasing subsequence)
    """
    def longestIncreasingSubsequence(self, nums):
        n = len(nums)
        b = [None] * (n + 1)

        # b[i] stores min of a[x] where f[x] = i
        for i in range(len(b)):
            b[i] = maxsize
        b[0] = -maxsize

        print(b)
        for i in range(n):
            idx = self.binarysearch(b, nums[i])
            print(idx)
            b[idx] = nums[i]
            print(b)

        for i in range(len(b) - 1, -1, -1):
            if b[i] != maxsize:

                return i

        return 0

    def binarysearch(self, b, num):
        start = 0
        end = len(b) - 1

        while start + 1 < end:

            mid = (start + end) // 2

            if b[mid] < num:
                start = mid
            else:
                end = mid

        return end




nums = [1, 1, 1, 1, 1, 1, 1]
print('ans:', Solution2().longestIncreasingSubsequence(nums))
