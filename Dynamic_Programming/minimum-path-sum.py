class Solution:
    """
    @param: grid: a list of lists of integers
    @return: An integer, minimizes the sum of all numbers along its path
    """
    def minPathSum(self, grid):
        # write your code here
        if grid is None or len(grid) == 0:
            return -1

        m = len(grid)
        n = len(grid[0])

#         sum = [[0] * n] * m
#         print(sum)
        sum = [[0 for i in range(n)] for j in range(m)]
        print(sum)

        sum[0][0] = grid[0][0]
        print((n, m))
        # init
        for i in range(1, m):
            sum[i][0] = sum[i - 1][0] + grid[i][0]

        for j in range(1, n):
            sum[0][j] = sum[0][j - 1] + grid[0][j]

        for i in range(1, m):
            for j in range(1, n):
                sum[i][j] = min(sum[i - 1][j], sum[i][j - 1]) + grid[i][j]

        return sum[m - 1][n - 1]

grid = [[1, 3, 1], [1, 5, 1], [4, 2, 1]]
print('ans:', Solution().minPathSum(grid))
