"""
dp 背包
summ = sum(nums)
p = 帶有 '+' 的 1 有幾個?
q = 帶有 '-' 的 1 有幾個?
p + q = summ
p - q = target
→ 2p = summ + S
→ p = (summ + S) // 2 → 只要找到p, 必定有q, 問題轉換成，有幾種可能能夠拼成p? (類似背包５)
if S > summ or S < -summ or (S + summ) % 2: return 0
ex [1,1,1,1,1], S = 3 → p = [1,1,1,1], q = [-1]

但這題和背包５不同在於
i !=0 and j == 0 的時候(target為零) f[i][j] 不可跳過，(和單純的背包不同)，因為input nums 有正有負，所以有多種機會可以得到重量0

"""
class Solution(object):
    def findTargetSumWays(self, nums, S):
        """
        :type nums: List[int]
        :type S: int
        :rtype: int
        """
        summ = sum(nums)
        if S > summ or S < -summ or (S + summ) % 2:
            return 0

        target = (S + summ) // 2

        n = len(nums)
        f = [[0 for _ in range(target + 1)] for _ in range(n + 1)]

        for i in range(n + 1):
            for j in range(target + 1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0:
                    f[i][j] = 0
                    continue

                f[i][j] = f[i - 1][j]
                if j - nums[i - 1] >= 0:
                    f[i][j] += f[i - 1][j - nums[i - 1]]

        return f[n][target]

nums = [1, 1, 1, 1, 1]
S = 3
a = Solution().findTargetSumWays(nums, S)
print("ans:", a)


class Solution_dfs_mem:
    def __init__(self):
        self.ans = 0
        self.mem = {}

    def findTargetSumWays(self, nums, S):
        """
        :type nums: List[int]
        :type S: int
        :rtype: int
        """

        if len(nums) == 0:
            return

        self.ans = self.dfs(nums, S, 0)

        return self.ans

    def dfs(self, nums, T, i):
        if (i, T) in self.mem:
            return self.mem[(i, T)]

        if i == len(nums):
            if T == 0:
                return 1
            return 0

        a = self.dfs(nums, T - nums[i], i + 1)
        b = self.dfs(nums, T + nums[i], i + 1)

        res = a + b
        self.mem[(i, T)] = res
        return res
