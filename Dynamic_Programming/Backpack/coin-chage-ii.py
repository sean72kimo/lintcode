# similar to backack vi / combination iv
# this one is to get permutation counts


# 01背包 f[i][j]      = max(f[i - 1][j], f[i - 1][j - nums[i - 1]] + v[i - 1])
# 正常完全背包 f[i][j] = max(f[i - 1][j], f[i][j - nums[i - 1]] + v[i - 1])
# https://blog.csdn.net/qq_37821701/article/details/108466249 01背包和完全背包的差異

# this one is to get combination counts
import collections

# 外加打印路徑
class Solution(object):
    def change(self, target, nums):
        if target == 0:
            return 1
        
        if len(nums) == 0:
            return 0
        
        n = len(nums)
        f = [[None for _ in range(target + 1)] for _ in range(n + 1)]
        p = collections.defaultdict(list)
        p[0] += [""]
        
        for i in range(n+1):
            for j in range(target+1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue
                    
                if i == 0:
                    f[i][j] = 0
                    continue
                    
                if j == 0:
                    f[i][j] = 1
                    continue
                
                f[i][j] = f[i-1][j]
                if j - nums[i-1] >= 0:
                    f[i][j] += f[i][j - nums[i-1]]

                    for path in p[j - nums[i-1]]:
                        p[j]  += [path + '+' + str(nums[i-1])]

        for k, v in p.items():
            print(k, v)
        return f[n][target]
target = 7
nums = [1,2,3,7]

target = 5
nums = [1,2,5]

target = 20
nums = [3,5,10]

target = 13
nums = [3,5,10]
a = Solution().change(target, nums)
print("ans:", a)