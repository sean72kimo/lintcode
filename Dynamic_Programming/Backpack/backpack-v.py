# http://www.lintcode.com/en/problem/backpack-v/
class SolutionSpaceOptimization:
    """
    @param nums: an integer array and all positive numbers
    @param target: An integer
    @return: An integer
    """

    def back_pack_v(self, nums, target):
        # write your code here
        if target == 0:
            return 1

        if len(nums) == 0:
            return 0

        n = len(nums)
        f = [[0 for _ in range(target + 1)] for _ in range(2)]

        for i in range(n + 1):
            for j in range(target + 1):
                if i == 0 and j == 0:
                    f[i % 2][j] = 1
                    continue

                if i == 0:
                    continue

                if j == 0:
                    f[i % 2][j] = 1
                    continue

                f[i % 2][j] = f[(i - 1) % 2][j]
                if j - nums[i - 1] >= 0:
                    f[i % 2][j] += f[(i - 1) % 2][j - nums[i - 1]]
        return f[n % 2][target]

class Solution2DSlower:
    """
    @param nums: an integer array and all positive numbers
    @param target: An integer
    @return: An integer
    """
    def backPackV(self, nums, target):
        # write your code here
        if target == 0:
            return 1
        
        if len(nums) == 0:
            return 0
        
        n = len(nums)
        f = [[None for _ in range(target + 1)] for _ in range(n + 1)]
        
        for i in range(n+1):
            for j in range(target+1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue
                    
                if i == 0:
                    f[i][j] = 0
                    continue
                    
                if j == 0:
                    f[i][j] = 1
                    continue
                
                f[i][j] = f[i-1][j]
                if j - nums[i-1] >= 0:
                    f[i][j] += f[i-1][j - nums[i-1]]
                    
        return f[n][target]

nums = [1,1,1,1]
target = 3
nums = [1,2,3,3,7]
target = 7

a = Solution2DSlower().backPackV(nums, target)
print('ans:', a)

class Solution2D:
    """
    @param: nums: an integer array and all positive numbers
    @param: target: An integer
    @return: An integer
    """
    def backPackV(self, A, m):
        # write your code here
        n = len(A)

        f = [[None for j in range(m + 1)] for i in range(n + 1)]

        for col in range(m + 1):
            f[0][col] = 0

        f[0][0] = 1

        for i in range(1, n + 1):
            for j in range(m + 1):
                f[i][j] = f[i - 1][j]
                if j >= A[i - 1]:
                    f[i][j] = f[i][j] + f[i - 1][ j - A[i - 1] ]

        return f[-1][m]

nums = [1,1,1,1]
target = 3
nums = [1,2,3,3,7]
target = 7

a = Solution2DSlower().backPackV(nums, target)
print('ans:', a)


# http://www.lintcode.com/en/problem/backpack-v/
class Solution1D:
    """
    @param: nums: an integer array and all positive numbers
    @param: target: An integer
    @return: An integer
    """
    def backPackV(self, A, m):
        # write your code here
        n = len(A)

        f = [None for j in range(m + 1)]

        # init
        f[0] = 1
        for j in range(1, m + 1):
            f[j] = 0

        for i in range(1, n + 1):
            for j in range(m, -1, -1):
                if j - A[i - 1] >= 0:
                    f[j] = f[j] + f[j - A[i - 1]]

        return f[m]


a = Solution1D().backPackV(nums, target)
print('ans:', a)



class SolutionMem:
    """
    @param nums: an integer array and all positive numbers
    @param target: An integer
    @return: An integer
    """
    def __init__(self):
        self.mem = []
        
    def backPackV(self, nums, T):
        # write your code here
        if len(nums) == 0:
            return 0
        A = sorted(nums)
        n = len(A)

        self.mem = [[None for _ in range(n)] for _ in range(T+1)]
        for r in self.mem:
            print(r)
        return self.dfs(A, T, 0, [])
        
        
    def dfs(self, A, T, start, path):
        print((T, start))
        if self.mem[T][start] is not None:
            return self.mem[T][start]
                   

        if T == 0:
            return 1

        res = 0
        for i in range(start, len(A)):
            remain = T - A[i]

            if remain < 0:
                break
            res += self.dfs(A, remain, i+1, path)


        self.mem[T][start] = res
        return res

# a = SolutionMem().backPackV(nums, target)
# print("ans:", a)




class SolutionDfs:
    """
    @param nums: an integer array and all positive numbers
    @param target: An integer
    @return: An integer
    """
    def __init__(self):
        self.ans = 0
        
    def backPackV(self, nums, T):
        # write your code here
        if len(nums) == 0:
            return 0
        A = sorted(nums)
        n = len(A)

        self.dfs(A, T, 0, [])
        return self.ans
        
    def dfs(self, A, T, start, path):
        if T == 0:
            print(path)
            self.ans += 1
            return 

        for i in range(start, len(A)):

            remain = T - A[i]
            if remain < 0:
                break

            path.append(A[i])
            self.dfs(A, remain, i+1, path)
            path.pop()
            
# a = SolutionDfs().backPackV(nums, target)
# print("ans:", a)
print(2%3)