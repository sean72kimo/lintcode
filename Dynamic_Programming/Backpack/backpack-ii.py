def printf(f):
    print('=======================================')
    print('[')
    for r in f:
        print(r)
    print(']')

class Solution:
    """
    @param: m: An integer m denotes the size of a backpack
    @param: A: Given n items with size A[i]
    @param: V: Given n items with value V[i]
    @return: The maximum value
    """
    def backPackII(self, m, A, V):
        # write your code here
        n = len(A)
        if n == 0:
            return 0

        # f[n+1][m+1]
        f = [[None for j in range(m + 1)] for i in range(n + 1)]
#         f[0][0] = 0
#
#         # cannot make to weight
#         for w in range(1, m + 1):
#             f[0][w] = -1
#         printf(f)

        for i in range(n + 1):
            for w in range(m + 1):


                if i == 0 and w == 0:
                    f[i][w] = 0
                    continue

                if i == 0:
                    f[i][w] = -1
                    continue

                f[i][w] = f[i - 1][w]

                curWei = A[i - 1]
                if w >= curWei and f[i - 1][w - curWei] != -1:
                    printf(f)
                    f[i][w] = max(f[i][w], f[i - 1][w - curWei] + V[i - 1])

        printf(f)
        ans = 0
        for w in range(m + 1):
            if f[n][w] != -1:
                ans = max(ans, f[n][w])

        return ans



m = 100
A = [77, 22, 29, 50, 99]
V = [92, 22, 87, 46, 90]

ans = Solution().backPackII(m, A, V)
print('ans:', ans)
