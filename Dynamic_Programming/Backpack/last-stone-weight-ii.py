"""
分析：
將array分成兩半，兩半的各自總和left_sum, and right_sum 的差值最小是多少？
用背包的思路，nums總共n個的數字，用這n個數字可以拚出哪些重量？
left_sum = partial_sum
right_sum = summ - partial_sum
if f[partial_sum] and f[summ - partial_sum]，那麼選取最小的 abs(left_sum - right_sum)

基本背包解法：
92. Backpack (lintcode )
nums = stones
target = sum(stones)
建立f矩陣，可否由前n個數字，拼出target?

"""


class Solution(object):
    def lastStoneWeightII(self, stones):
        """
        :type stones: List[int]
        :rtype: int
        """
        n = len(stones)
        target = sum(stones)
        f = [[False for _ in range(target + 1)] for _ in range(n + 1)]

        for i in range(n + 1):
            for j in range(target + 1):
                if i == 0 and j == 0:
                    f[i][j] = True
                    continue

                if i == 0:
                    f[i][j] = False
                    continue

                if j == 0:
                    f[i][j] = True
                    continue

                f[i][j] |= f[i - 1][j]
                if j - stones[i - 1] >= 0:
                    f[i][j] |= f[i - 1][j - stones[i - 1]]

        summ = target
        dp = f[n]

        res = float('inf')
        for partial_sum in range(1, summ + 1):
            if dp[partial_sum] and dp[summ - partial_sum]:
                res = min(res, abs((summ - partial_sum) - partial_sum))

        return res

class Solution(object):
    def lastStoneWeightII(self, stones):
        """
        :type stones: List[int]
        :rtype: int
        """
        summ = sum(stones)
        dp = [0 for _ in range(summ + 1)]
        dp[0] = 1

        for i in range(len(stones)):
            for j in range(len(dp) - 1, -1, -1):
                if j - stones[i] >= 0 and dp[j - stones[i]]:
                    dp[j] = 1
        print(dp)
        res = float('inf')
        for psum in range(1, summ + 1):
            if dp[psum] and dp[summ-psum]:
                res = min(res, abs((summ-psum)-psum))

            # if dp[psum] and 2 * psum - summ >= 0:
            #     res = min(res, 2 * psum - summ)

        return res


stones = [2,7,4,1,8,1]
stones = [53,54,3,61,67]
a = Solution().lastStoneWeightII(stones)
print("ans:", a)
