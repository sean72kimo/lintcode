from typing import (
    List,
)
"""
此作法參考combination-sum-iv, which is backpack Vi in lintcode
https://leetcode.com/problems/combination-sum-iv/
https://www.lintcode.com/problem/564/

因為物品可以無限取用，所以物品原來的順序不是關鍵
重量i可以選擇不拿取當前物品A[j], 
或是選鑿拿取當前物品A[j] -> f[i - a[j]] + v[j]
"""


class Solution:
    """
    @param a: an integer array
    @param v: an integer array
    @param m: An integer
    @return: an array
    """

    def back_pack_i_i_i(self, a: List[int], v: List[int], m: int) -> int:
        # write your code here
        if len(a) == 0:
            return 0

        f = [-1 for _ in range(m + 1)]

        for i in range(m + 1):
            if i == 0:
                f[i] = 0
                continue

            for j in range(len(a)):
                if i - a[j] >= 0 and f[i - a[j]] != -1:
                    f[i] = max(f[i - a[j]] + v[j], f[i])

        res = 0
        for i in range(m + 1):
            if f[i] != -1:
                res = max(f[i], res)

        return res

"""
此方法為九章上課教的方法。
見DP5.webm 18:30
這個方法來自 backpack ii
https://www.lintcode.com/problem/125/
"""
class Solution:
    """
    @param: A: an integer array
    @param: V: an integer array
    @param: m: An integer
    @return: an array
    """
    def backPackIII(self, A, V, m):
        # write your code here

        n = len(A)
        f = [None for _ in range(m + 1)]
        f[0] = 0

        for w in range(1, m + 1):
            f[w] = -1

        for i in range(1, n + 1):
            for w in range(A[i - 1], m + 1):
                if f[w - A[i - 1]] != -1:
                    f[w] = max(f[w], f[w - A[i - 1]] + V[i - 1])

        res = 0
        for w in range(m + 1):
            if f[w] != -1:
                res = max(res, f[w])

        return res
A = [2, 3, 5, 7]
V = [1, 5, 2, 4]
m = 10
ans = Solution().backPackIII(A, V, m)
print('ans:', ans)
