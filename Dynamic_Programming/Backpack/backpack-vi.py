class Solution:
    """
    @param: nums: an integer array and all positive numbers, no duplicates
    @param: target: An integer
    @return: An integer
    """
    def backPackVI(self, A, m):
        # write your code here
        # f[i] = f[i-A0] + f[i-A1] + f[i-A2] + .... +f[i-An-1]

        n = len(A)
        if n == 0:
            return 0

        f = [0 for _ in range(m + 1)]
        f[0] = 1
        for i in range(1, m + 1):  # each weight
            # init
            # f[i] = 0
            for j in range(n):
                if i < A[j]:
                    break
                f[i] = f[i] + f[i - A[j]]


        return f[m]


class Solution2: # 還未想通這題的2D解法
    #https://www.jiuzhang.com/solution/backpack-vi/#tag-other

    def backPackVI(self, nums, target):
        # write your code here
        if len(nums) == 0:
            return 0
            
        n = len(nums)
        f = [[None for _ in range(n + 1)] for _ in range( target + 1)]
        
        for i in range(target + 1):
            for j in range(n + 1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue
                
                if i == 0:
                    f[i][j] = 1
                    continue
                
                if j == 0:
                    f[i][j] = 0
                    continue
                
                f[i][j] = f[i][j-1]
                if i - nums[j-1] >= 0:
                    f[i][j] = f[i][j] + f[i - nums[j-1]][n]
                    
        return f[target][n]

nums = [1,2,4]
target = 4

# a = Solution().backPackV(nums, target)
# print("ans:", a)