"""
92. Backpack
https://www.lintcode.com/problem/backpack/my-submissions
"""
class Solution:
    def backPack(self, m, A):
        n = len(A)
        if n == 0:
            return 0

        f = [[None for _ in range(m + 1)] for _ in range(n + 1)]

        # f[0][0] = True

        # for j in range(1,m+1):
        #     f[0][j] = False

        for i in range(n + 1):
            for j in range(m + 1):

                if i == 0 and j == 0:
                    f[i][j] = True
                    continue

                if i == 0:
                    f[i][j] = False
                    continue

                if j == 0:
                    f[i][j] = True
                    continue

                f[i][j] = f[i - 1][j]
                if j >= A[i - 1]:
                    f[i][j] = f[i][j] or f[i - 1][j - A[i - 1]]

        res = 0
        for j in range(m, -1, -1):
            if f[n][j]:
                res = j
                break
        return res

class Solution:
    # @param m: An integer m denotes the size of a backpack
    # @param A: Given n items with size A[i]
    # @return: The maximum size
    def backPack(self, m, A):
        n = len(A)

        if n == 0:
            return 0

        f = [[None for i in range(m + 1)] for j in range(n + 1)]
        pi = [[0 for i in range(m + 1)] for j in range(n + 1)]
        f[0][0] = True


        for j in range(1, m + 1):
            f[0][j] = False

        for i in range(1, n + 1):
            for j in range(m + 1):
                f[i][j] = f[i - 1][j]

                if j >= A[i - 1]:
                    print('w', j - A[i - 1])
                    f[i][j] = f[i][j] | f[i - 1][j - A[i - 1]]
                    pi[i][j] = 1

        res = 0
        for j in range(m, -1, -1):
            if f[n][j]:
                res = j
                break


        j = res
        for i in range(n, 0, -1):
            if pi[i][j] == 1:
                print("item ", A[i - 1])
                j -= A[i - 1]
        return res

m = 11
A = [2, 3, 5, 7]
a = Solution().backPack(m, A)
print('ans:', a)
