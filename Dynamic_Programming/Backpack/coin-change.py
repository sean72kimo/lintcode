from typing import List

class Solution_dfs(object):
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        n = len(coins)
        f = [float('inf') for _ in range(amount + 1)]
        coins.sort()
        self.mem = {}
        rt = self.dfs(coins, amount)

        return rt if rt != float('inf') else -1

    def dfs(self, coins, remain):
        if remain in self.mem:
            return self.mem[remain]

        if remain == 0:
            return 0

        res = float('inf')
        for i in range(len(coins)):
            if remain - coins[i] < 0:
                break

            rt = self.dfs(coins, remain - coins[i])
            res = min(res, rt + 1)

        self.mem[remain] = res
        return res


class Solution_DP:
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        if amount == 0:
            return 0
        if len(coins) == 0:
            return -1

        f = [None for _ in range(amount+1)]
        
        myCoins = set(coins)
        
        for i in range(amount+1):
            if i == 0:
                f[i] = 0
                continue
            
            if i in myCoins:
                f[i] = 1
                continue
            
            for c in coins:
                if i-c >= 0 and f[i-c] is not None:
                    if f[i] is None:
                        f[i] = f[i-c] + 1
                    else:
                        f[i] = min(f[i-c] + 1, f[i])
                    
                    
        return f[-1] if f[-1] is not None else -1
    
    
class Solution_Dfs:
    def __init__(self):
        self.ans = float('inf')
        self.mem = {}
        
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        if amount == 0:
            return 0
        
        if len(coins) == 0:
            return -1
            
        coins.sort()
        
        self.dfs(coins, amount, 0, 0)
        if self.ans == float('inf'):
            return -1
        return self.ans
        
    def dfs(self, nums, target, start, cnt):
        if target == 0:
            self.ans = min(self.ans, cnt)
            return   
        
        res = float('inf')
        for i in range(len(nums)):
            if target - nums[i] < 0:
                break
                
            self.dfs(nums, target-nums[i], i, cnt + 1)

# 打印路徑
class Solution_print_path:
    def coinChange(self, coins: List[int], amount: int) -> int:
        if len(coins) == 0 or amount == 0:
            return 0

        n = len(coins)
        f = [float('inf') for _ in range(amount + 1)]
        p = [[] for _ in range(amount + 1)]

        for c in coins:
            if c <= amount:
                f[c] = 1
                p[c].append(c)

        for i in range(amount + 1):
            for c in coins:
                if i - c >= 0 and f[i - c] != float('inf'):
                    if f[i - c] + 1 < f[i]:
                        f[i] = f[i - c] + 1
                        p[i] = p[i - c] + [c]

        if f[amount] == float('inf'):
            print(p[amount])
            return -1
        print(p)
        return f[amount]

coins = [1,2,5]
amount = 11
# coins = [2]
# amount = 3
a = Solution_print_path().coinChange(coins, amount)
print("ans:", a)


