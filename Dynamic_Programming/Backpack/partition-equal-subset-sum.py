class Solution(object):
    def canPartition(self, A):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if A is None or len(A) == 0:
            return False
        if sum(A) % 2:
            return False

        m = sum(A) // 2

        # A.sort()
        n = len(A)

        f = [[False for _ in range(m + 1)] for _ in range(n + 1)]
        f[0][0] = True

        for i in range(1, n + 1):
            for j in range(m + 1):
                f[i][j] = f[i - 1][j]
                if j - A[i - 1] >= 0:
                    f[i][j] = f[i][j] | f[i - 1][ j - A[i - 1]]
        return f[n][m]



class Solution2(object):
    def canPartition(self, A):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if A is None or len(A) == 0:
            return False
        if sum(A) % 2:
            return False

        m = sum(A) // 2

        # A.sort()
        n = len(A)

        f = [[False for _ in range(m + 1)] for _ in range(2)]
        f[0][0] = True

        for i in range(1, n + 1):
            for j in range(m + 1):
                f[i & 1][j] = f[(i - 1) & 1][j]
                if j - A[i - 1] >= 0:
                    f[i & 1][j] = f[i & 1][j] | f[(i - 1) & 1][ j - A[i - 1]]
        return f[n & 1][m]
A = [1, 5, 11, 5]
A = [1, 2, 3, 4, 5, 6, 7]
a = Solution2().canPartition(A)
print("ans:", a)
