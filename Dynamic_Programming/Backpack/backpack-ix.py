"""
Lintcode 800. Backpack IX
https://www.lintcode.com/problem/backpack-ix/description
"""


class Solution:
    """
    @param n: Your money
    @param prices: Cost of each university application
    @param probability: Probability of getting the University's offer
    @return: the  highest probability
    """

    def backpackIX(self, m, prices, probability):
        if m == 0:
            return 0 * 1.0

        n = len(prices)
        p = []
        for prob in probability:  # 每間學校失敗的機率
            p.append(1 - prob)

        f = [[1.00 for _ in range(m + 1)] for _ in range(n + 1)]

        for i in range(n + 1):  # cost for each school
            for j in range(m + 1):  # total money
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0:
                    continue

                f[i][j] = f[i - 1][j]  # 不申請當前學校
                if j - prices[i - 1] >= 0:  # 申請當前學校
                    f[i][j] = min(f[i - 1][j - prices[i - 1]] * p[i - 1], f[i][j])  # 失敗率越低越好

        return (1 - f[n][m]) * 1.00