class Solution:

    def knightProbability(self, N, K, r, c):
        """
        :type N: int
        :type K: int
        :type r: int
        :type c: int
        :rtype: float
        """
        dp = [[0] * N for _ in range(N)]

        dp[r][c] = 1
        dxy = ((2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (1, -2), (-1, 2), (-1, -2))
        for _ in range(K):

            dp2 = [[0] * N for _ in range(N)]

            for i in range(N):
                for j in range(N):

                    for dx, dy in dxy:
                        nx = i + dx
                        ny = j + dy
                        if not (0 <= nx < N and  0 <= ny < N):
                            continue

                        dp2[nx][ny] += dp[i][j] / 8.0

            dp = dp2

        return sum(map(sum, dp))
