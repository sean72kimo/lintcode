class Solution:
    """
    @param: word1: A string
    @param: word2: A string
    @return: The minimum number of steps.
    """
    def minDistance(self, word1, word2):
        # write your code here
        n = len(word1)
        m = len(word2)

        dp = [[0 for i in range(m + 1)] for j in range(n + 1)]
        step = 0
        for i in range(n + 1):
            dp[i][0] = i
        for j in range(m + 1):
            dp[0][j] = j

        for i in range(1, n + 1):
            for j in range(1, m + 1):
                if word1[i - 1] != word2[j - 1]:
                    dp[i][j] = min(dp[i - 1][j - 1], dp[i][j - 1], dp[i - 1][j]) + 1
                else:
                    dp[i][j] = dp[i - 1][j - 1]

        # for row in dp:
        #     print(row)
        return dp[-1][-1]

word1 = "mart"
word2 = "karma"
print('ans:', Solution().minDistance(word1, word2))
