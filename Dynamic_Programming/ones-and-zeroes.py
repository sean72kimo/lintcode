class Solution(object):
    def findMaxForm(self, A, m, n):
        """
        :type strs: List[str]
        :type m: int
        :type n: int
        :rtype: int
        """

        """
        f[i][j][k] = max of
                    1.f[i-1][j][k]

                    2. f[i-1][j-a][k-b] + 1, use a of 0 and b of 1 to create last "01" string
                    m > a and n > b
        """
        T = len(A)

        cnts = []
        c0 = []
        c1 = []
        for s in A:
            cnt0 = 0
            cnt1 = 0
            for bit in s:
                if bit == '0':
                    cnt0 += 1
                else:
                    cnt1 += 1
            c0.append(cnt0)
            c1.append(cnt1)

        # f[T+1][m+1][n+1]
        f = [[[0 for _ in range(n + 1)] for _ in range(m + 1)] for _ in range(T + 1)]


        for j in range(m + 1):
            for k in range(n + 1):
                f[0][j][k] = 0

        for i in range(1, T + 1):
            for j in range(m + 1):
                for k in range(n + 1):
                    f[i][j][k] = f[i - 1][j][k]  # don't form the last 01string

                    if j >= c0[i - 1] and k >= c1[i - 1]:
                        f[i][j][k] = max(f[i][j][k], f[i - 1][j - c0[i - 1]][k - c1[i - 1]] + 1)

        return f[T][m][n]

A = ["10", "0001", "111001", "1", "0"]
m = 5
n = 3
Solution().findMaxForm(A, m, n)
