"""
time: n^2

"""
class Solution2(object):

    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0
        m = len(grid)
        n = len(grid[0])
        up = [[0 for _ in range(n)] for _ in range(m)]
        down = [[0 for _ in range(n)] for _ in range(m)]
        right = [[0 for _ in range(n)] for _ in range(m)]
        left = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(0, m, 1):
            for j in range(0, n, 1):
                if grid[i][j] == 'W':
                    continue

                if grid[i][j] == 'E':
                    up[i][j] += 1

                if i - 1 >= 0:
                    up[i][j] += up[i - 1][j]

        for i in range(m - 1, -1, -1):
            for j in range(0, n, 1):
                if grid[i][j] == 'W':
                    continue

                if grid[i][j] == 'E':
                    down[i][j] += 1

                if i + 1 <= m - 1:
                    down[i][j] += down[i + 1][j]

        for i in range(0, m, 1):
            for j in range(0, n, 1):
                if grid[i][j] == 'W':
                    continue

                if grid[i][j] == 'E':
                    right[i][j] += 1

                if j - 1 >= 0:
                    right[i][j] += right[i][j - 1]

        for i in range(0, m, 1):
            for j in range(n - 1, -1, -1):
                if grid[i][j] == 'W':
                    continue

                if grid[i][j] == 'E':
                    left[i][j] += 1

                if j + 1 <= n - 1:
                    left[i][j] += left[i][j + 1]

        ans = 0
        for i in range(0, m, 1):
            for j in range(0, n, 1):
                if grid[i][j] == '0':
                    t = up[i][j] + down[i][j] + left[i][j] + right[i][j]
                    ans = max(ans, t)
        return ans

"""
time: n^3

"""

class Solution(object):

    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0

        n = len(grid)
        m = len(grid[0])

        rows = 0
        cols = [0 for i in range(m)]
        kill = 0

        def rowkill(i, j):
            cnt = 0
            for y in range(j, m):
                if grid[i][y] == 'E':
                    cnt += 1
                if grid[i][y] == 'W':
                    break
            return cnt

        def colkill(i, j):
            cnt = 0
            for x in range(i, n):
                if grid[x][j] == 'E':
                    cnt += 1
                if grid[x][j] == 'W':
                    break
            return cnt

        for i in range(n):
            for j in range(m):
                if j == 0 or grid[i][j - 1] == 'W':
                    rows = rowkill(i, j)
                if i == 0 or grid[i - 1][j] == 'W':
                    cols[j] = colkill(i, j)

                if grid[i][j] == '0':
                    kill = max(kill, rows + cols[j])

        return kill


grid = [["0", "E", "0", "0"], ["E", "0", "W", "E"], ["0", "E", "0", "0"]]
ans = Solution2().maxKilledEnemies(grid)
print('ans:', ans)
