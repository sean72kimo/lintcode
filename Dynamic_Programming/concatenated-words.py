class Solution(object):
    def findAllConcatenatedWordsInADict(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        if len(words) == 0:
            return []
        
        words.sort(key = len)
        preWords = set()
        res = []
        for i in range(len(words)):
            if self.canForm(words[i], preWords):
                res.append(words[i])
            preWords.add(words[i])
        
        return res
            
        
    def canForm(self, word, dic):
        if len(dic) == 0:
            return False
        
        dp = [None for _ in range(len(word)+1)]
        dp [0] = True
        
        for i in range(len(word)+1):
            for j in range(i):
                if dp[j] is True: #word[0:j]
                    if word[j:i] in dic:
                        dp[i] = True
                        break
        return dp[len(word)]
        
        
        
words = ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]
a = Solution().findAllConcatenatedWordsInADict(words)
print("ans:", a)