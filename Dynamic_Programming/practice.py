class Solution:
    """
    @param nums: an integer array and all positive numbers
    @param target: An integer
    @return: An integer
    """
    def backPackV(self, A, T):
        # write your code here
        if len(A) == 0:
            return 0
        n = len(A)
        f = [[None for _ in range(T+1)] for _ in range(n+1)]
        
        #f[i][w] = f[i-1][w] + f[i-1][w-A[i-1]]
        
        for i in range(n+1):
            for w in range(T+1):
                
                if i == 0 and w == 0:
                    f[i][w] = 1
                    continue
                
                if i == 0:
                    f[i][w] = 0
                    continue
                    
                f[i][w] = f[i-1][w] 
                
                
                if w-A[i-1] >= 0:
                    
                    f[i][w] += f[i-1][w-A[i-1]]
                    
                    
        for r in f:
            print(r)
        return f[n][T]
                    
A = [1,2,3,3,7] 
T = 7
a = Solution().backPackV(A, T)
print("ans:", a)