class Solution(object):
    def maxVacationDays(self, flights, days):
        """
        :type flights: List[List[int]]
        :type days: List[List[int]]
        :rtype: int
        """
        if len(flights) == 0 or len(days) == 0:
            return 0
        self.mem = {}
        return self.dfs(flights, days, 0, 0)

    def dfs(self, flights, days, city, week):
        if (week, city) in self.mem:
            return self.mem[(week, city)]

        if week == len(days[0]):
            return 0
        res = 0
        for c in range(len(days)):
            if flights[city][c] or c == city:
                tmp = self.dfs(flights, days, c, week + 1) + days[c][week]
                res = max(res, tmp)

        self.mem[(week, city)] = res
        return res

flights = [[0,1,1],[1,0,1],[1,1,0]]
days = [[1,3,1],[6,0,3],[3,3,3]]
exp = 12

flights = [[0,0,0],[0,0,0],[0,0,0]]
days = [[1,1,1],[7,7,7],[7,7,7]]
exp = 3
sol = Solution()
a = sol.maxVacationDays(flights, days)
print('ans:', a)
