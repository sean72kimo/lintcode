class Solution:
    """
    @param n: An integer
    @return: An integer
    """
    def __init__(self):
        self.ans = []
    def climbStairs(self, n):
        # write your code here
        if n == 0:
            return 1
        if n <= 2:
            return n


        sum = [0 for i in range(n)]
        sum[0] = 1
        sum[1] = 2


        for i in range(2, n):
            sum[i] = sum[i - 1] + sum[i - 2]

        return sum[n - 1]



class Solution_dfs:
    """
    @param n: An integer
    @return: An integer
    """

    def climbStairs(self, n):
        self.ans = []
        self.mem = {}
        res = self.helper(n)
        return res

    def helper(self, n):

        if n in self.mem:
            return self.mem[n]

        if n == 0:
            return 1

        if n < 0:
            return 0

        res = 0
        for i in range(1,3):
            res += self.helper(n - i)

        self.mem[n] = res
        return res

# 打印路徑
class Solution_print_path:
    """
    @param n: An integer
    @return: An integer
    """

    def climbStairs(self, n):
        self.ans = []
        self.mem = {}
        res = self.helper(n, [])
        print(res)
        return len(res)

    def helper(self, n, path):
        if n == 0:
            return ['']

        if n in self.mem:
            return self.mem[n]

        if n < 0:
            return ['']

        res = []
        for i in range(1,3):
            path = self.helper(n - i, path)

            for p in path:
                now = str(i) + ',' + p
                res.append(now)

        self.mem[n] = res
        return res


n = 4
print('ans', Solution_print_path().climbStairs(n))
print('ans', Solution_dfs().climbStairs(n))
print('ans', Solution().climbStairs(n))
# print('ans', Solution().climbStairs_dfs(n))
