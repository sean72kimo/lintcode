from sys import maxsize
class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        if len(nums) == 1:
            return nums[0]
        n = len(nums)
        f = [None] * (n+1)
        g = [None] * (n+1)
        f[0] = 1
        g[0] = 1
        
        for i in range(1, n+1):
            f[i] = max(nums[i-1], f[i-1] * nums[i-1], g[i-1] * nums[i-1])
            g[i] = min(nums[i-1], g[i-1] * nums[i-1], f[i-1] * nums[i-1])
            print(f)
        return max(f[1:])
nums = [2,3,-2,4]
# nums = [3,-2]
# nums = [-2,0,-1]
a = Solution().maxProduct(nums)
print("ans:", a)



from sys import maxsize
class Solution2(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        if len(nums) == 1:
            return nums[0]
        n = len(nums)
        f = [None] * (2)
        g = [None] * (2)
        f[0] = 1
        g[0] = 1
        res = float('-inf')
        for i in range(1, n+1):
            f[i%2] = max(nums[i-1], f[(i-1)%2] * nums[i-1], g[(i-1)%2] * nums[i-1])
            g[i%2] = min(nums[i-1], g[(i-1)%2] * nums[i-1], f[(i-1)%2] * nums[i-1])
            res = max(res, f[i%2])
            print(f)
        
        return res
    
a = Solution2().maxProduct(nums)
print("ans:", a)