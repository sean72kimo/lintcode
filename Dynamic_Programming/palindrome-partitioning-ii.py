class Solution(object):
    def minCut(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0
        n = len(s)
        # f[i]：長度為i的字串，能有多少段回文串？（n段，那麼min cut是n-1刀）
        # 長度為i的子串中，最差情況是每個字符都是回文，所以有i段回文串
        f = [i for i in range(n + 1)]
        is_palin = self.calc_palinrome(s)

        for i in range(n + 1):
            if i == 0:
                f[0] = 0
                continue
            f[i] = i  # 長度為i的子串中，最差情況是每個字符都是回文，所以有i段回文串
            for j in range(i):
                if is_palin[j][i - 1]:  # s[j:i]為回文, s[j:i] == s[j:i][::-1]
                    # f[j] 長度為j的字符串中，能有幾段回文串。
                    # f[j]+1, +1的部分代表s[j:i]的部分是回文串, i長度的子串有 f[j]+1
                    f[i] = min(f[i], f[j] + 1)

        return f[n] - 1

    def calc_palinrome(self, s):
        n = len(s)

        # is_palin[i][j]：字串下標i～下標j的子字串s[i:j+1]是否為回文？
        is_palin = [[False for _ in range(n)] for _ in range(n)]

        # m 為切分點，i, j從中點(m)往兩端點移動，判斷是否為palindrome
        for m in range(n):
            # 奇數個字符，所以s[i]為中點
            i = j = m
            while i >= 0 and j < n and s[i] == s[j]:
                is_palin[i][j] = True
                i -= 1
                j += 1

            # 偶數個字符，所以s[i:i+2]為中點
            i = m
            j = m + 1
            while i >= 0 and j < n and s[i] == s[j]:
                is_palin[i][j] = True
                i -= 1
                j += 1

        return is_palin


s = "aab"
# s = "ab"
a = Solution().minCut(s)
print('ans:', a)





from sys import maxsize
class Solution2:
    # @param s, a string
    # @return an integer

    def calcPalin(self, s):

        n = len(s)
        isPalin = [[False for _ in range(n)] for _ in range(n)]

        for m in range(n):
            i = j = m

            # s[m] is the symmetrix axis
            while i >= 0 and j < n and s[i] == s[j]:
                isPalin[i][j] = True
                i -= 1
                j += 1

            # s[m] | s[m+1]
            i = m
            j = m + 1
            while i >= 0 and j < n and s[i] == s[j]:
                isPalin[i][j] = True
                i -= 1
                j += 1

        return isPalin

    def minCut(self, s):
        # write your code here
        n = len(s)
        if n == 0:
            return 0

        isPalin = self.calcPalin(s)

        f = [None for _ in range(n + 1)]
        f[0] = 0

        for i in range(1, n + 1):
            f[i] = i
            for j in range(i, -1, -1):  # s[j:i] == s[j:i][::-1]
                print(j, i, s[j:i])

                if s[j:i] == s[j:i][::-1]:
                   f[i] = min(f[i], f[j] + 1)


        return  f[n] - 1
# s = "aab"
# a = Solution2().minCut(s)
# print('ans:', a)
