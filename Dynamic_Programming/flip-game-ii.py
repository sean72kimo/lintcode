class Solution(object):
    def canWin(self, s):
        """
        :type s: str
        :rtype: bool
        """
        n = len(s)
        lst = []

        for i in range(n - 1):
            if s[i] == '+' and s[i + 1] == '+':
                tmp = s[:i] + "--" + s[i + 2:]
                lst.append(tmp)

        for l in lst:
            if not self.canWin(l):
                return True

        return False

s = "+++++"
Solution().canWin(s)
