class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        prime = [1] * n
        prime[0] = 0
        prime[1] = 0
        count = 0

        for i in range(2, int(n ** 0.5) + 1):
            if prime[i]:
                for j in range(2, (n - 1) // i + 1):
                    print(i, j)
                    prime[i * j] = 0

        print(prime)
        return prime.count(1)




class Solution2(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        prime = [1] * (n + 1)
        prime[0] = 0
        prime[1] = 0
        prime[n] = 0
        count = 0

        for i in range(2, n + 1):
            if prime[i]:
                for j in range(2, n+1):
                    print(i, j, i * j)
                    if i * j > n:
                        break

                    prime[i * j] = 0

        print(prime)
        return prime.count(1)

a = Solution2().countPrimes(10)
print(a)

"""
埃拉托斯特尼篩法
https://zh.wikipedia.org/wiki/%E5%9F%83%E6%8B%89%E6%89%98%E6%96%AF%E7%89%B9%E5%B0%BC%E7%AD%9B%E6%B3%95
"""
class Solution_math(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n <= 2:
            return 0
        isPrime = [True] * (n)
        isPrime[0] = False
        isPrime[1] = False

        for i in range(2, int(n ** 0.5) + 1):
            if not isPrime[i]:
                continue

            for j in range(i * i, n, i):  # for j = i^2, i^2+i, i^2+2i, i^2+3i
                print(i,j)
                isPrime[j] = False

        return sum(isPrime)


# a = Solution_math().countPrimes(20)
# print(a)