import collections


class Solution(object):
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        f = [[0 for _ in range(m)] for _ in range(n)]
        p = collections.defaultdict(list)
        p[0] = [""]

        for i in range(n):
            for j in range(m):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0 or j == 0:
                    f[i][j] = 1
                    continue

                f[i][j] = f[i - 1][j] + f[i][j - 1]

        return f[n - 1][m - 1]