class Solution(object):
    def longestLine(self, M):
        """
        :type M: List[List[int]]
        :rtype: int
        """
        if len(M) == 0:
            return 0
        m = len(M)
        n = len(M[0])
        f = [[[0,0,0,0] for _ in range(n)] for _ in range(m)]
        ans = 0
        for i in range(m):
            for j in range(n):
                if i == 0 and j == 0:
                    f[i][j] = [1,1,1,1] if M[i][j] else [0,0,0,0]
                    ans = max(ans, max(f[i][j]))
                    continue

                if j == 0:
                    if M[i][j]:
                        f[i][j][0] = max(f[i][j][0], f[i-1][j][0] + 1)
                        f[i][j][1] = 1
                        f[i][j][2] = 1

                        if i - 1 >= 0 and j + 1 < n:
                            f[i][j][3] = max(f[i][j][3], f[i - 1][j + 1][3] + 1)

                        ans = max(ans, max(f[i][j]))
                    continue

                if i == 0:
                    if M[i][j]:
                        f[i][j][0] = 1
                        f[i][j][1] = max(f[i][j][1], f[i][j-1][1] + 1)
                        f[i][j][2] = 1
                        f[i][j][3] = 1
                        ans = max(ans, max(f[i][j]))
                    continue

                if M[i][j]:
                    f[i][j][0] = max(f[i][j][0], f[i-1][j][0] + 1)
                    f[i][j][1] = max(f[i][j][1], f[i][j-1][1] + 1)
                    f[i][j][2] = max(f[i][j][2], f[i-1][j-1][2] + 1)
                    if i-1 >= 0 and j+1 < n:
                        f[i][j][3] = max(f[i][j][3], f[i-1][j+1][3] + 1)

                ans = max(ans, max(f[i][j]))

        for row in f:
            print(row)
        return ans

M = [
    [0,1,1,0],
    [0,1,1,0],
    [0,0,1,1]
]

M = [
    [1,1,0,0,1,0,0,1,1,0],
    [1,0,0,1,0,1,1,1,1,1],
    [1,1,1,0,0,1,1,1,1,0],
    [0,1,1,1,0,1,1,1,1,1],
    [0,0,1,1,1,1,1,1,1,0],
    [1,1,1,1,1,1,0,1,1,1],
    [0,1,1,1,1,1,1,0,0,1],
    [1,1,1,1,1,0,0,1,1,1],
    [0,1,0,1,1,0,1,1,1,1],
    [1,1,1,0,1,0,1,1,1,1]
] #9


# M = [
#     [1,1,1,1],
#     [0,1,1,0],
#     [0,0,0,1]
# ] #4

sol = Solution()
a = sol.longestLine(M)
print("ans:", a)
