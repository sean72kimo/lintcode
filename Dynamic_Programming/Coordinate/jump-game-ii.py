from sys import maxsize
class Solution(object):
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        size = len(nums)

        f = [maxsize for _ in range(size)]
        f[0] = 0

        for i in range(1, size):
            for j in range(i):
                if j + nums[j] >= i and f[j] != maxsize:
                    f[i] = min(f[i], f[j] + 1)


        return f[-1]

nums = [2, 3, 1, 1, 4]
a = Solution().jump(nums)
print("ans:", a)
