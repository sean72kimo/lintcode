# 這雖然是for loop, 俗稱bottom up, 但是在計算的時候，卻是從上往下計算
class SolutionBottomUp(object):
    def minimumTotal(self, tri):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if len(tri) == 0:
            return 0

        n = len(tri)
        m = len(tri[-1])
        f = [[float('inf') for _ in range(m)] for _ in range(n)]

        for i in range(n):
            for j in range(i + 1):
                if i == 0 and j == 0:
                    f[i][j] = tri[i][j]
                    continue


                f[i][j] = f[i - 1][j] + tri[i][j]
                if j - 1 >= 0:
                    f[i][j] = min(f[i - 1][j - 1], f[i - 1][j]) + tri[i][j]

        return min(f[n - 1])


# 雖然這是dfs, 俗稱top-down, 但是在記錄self.mem的時候，其實是從最底層往上紀錄
# 最底層的每一個(i,j)最小值就是自己triangle[i][j]
# 倒數第二層則是看看當前格子正下方，和右下方哪一個比較小
class SolutionTopDown(object):
    def minimumTotal(self, triangle):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if len(triangle) == 0:
            return 0

        self.mem = {}
        self.dfs(triangle, 0, 0, 0)
        print(self.mem)
        return self.mem[(0,0)]

    def dfs(self, nums, i, j, summ):

        if (i, j) in self.mem:
            return self.mem[(i, j)]

        if i == len(nums):
            return

        if j < 0 or j >= len(nums[i]):
            return

        self.dfs(nums, i + 1, j + 1, summ)
        self.dfs(nums, i + 1, j, summ)


        a = b = float('inf')
        if (i+1, j+1) in self.mem:
            a = nums[i][j] + self.mem[(i+1, j+1)]
        if (i+1, j) in self.mem:
            b = nums[i][j] + self.mem[(i+1, j)]
        print((i,j), a,b )
        if a == b == float('inf'):
            self.mem[(i, j)] = nums[i][j]
        else:
            self.mem[(i, j)] = min(a, b)


tri = [[2],[3,4],[6,5,7],[4,1,8,3]]
tri = [[-1],[3,2],[-3,1,-1]]
a = SolutionBottomUp().minimumTotal(tri)
b = SolutionTopDown_Wrong().minimumTotal(tri)
print("ans:", b)