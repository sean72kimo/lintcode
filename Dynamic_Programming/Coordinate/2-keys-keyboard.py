from sys import maxsize
class Solution(object):
    def minSteps(self, n):
        """
        :type n: int
        :rtype: int
        """

        if n <= 1:
            return 0

        f = [maxsize for _ in range(n + 1)]
        f[0] = 0
        f[1] = 0
        f[2] = 2

        for i in range(3, n + 1):
            f[i] = i
            for j in range(1, i):
                if i % j == 0:
                    print()
                    f[i] = f[j] + i // j
        return f[n]

a = Solution().minSteps(3)
print("ans:", a)
