import collections


class Solution:
    """
    @param: obstacleGrid: A list of lists of integers
    @return: An integer
    """
    def uniquePathsWithObstacles(self, obstacleGrid):
        # write your code here
        if obstacleGrid is None or len(obstacleGrid) == 0:
            return 0

        n = len(obstacleGrid)
        m = len(obstacleGrid[0])

        sum = [[0 for i in range(m)] for j in range(n)]


        for i in range(n):
            if obstacleGrid[i][0] != 1:
                sum[i][0] = 1
            else:
                break

        for j in range(m):
            if obstacleGrid[0][j] != 1:
                sum[0][j] = 1
            else:
                break

        for i in range(1, n):
            for j in range(1, m):
                if obstacleGrid[i][j] != 1:
                    sum[i][j] = sum[i - 1][j] + sum[i][j - 1]

                else:
                    sum[i][j] = 0

        return sum[n - 1][m - 1]


class Solution(object):
    def uniquePathsWithObstacles(self, grid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        if grid[0][0] == 1:
            return 0

        m = len(grid)
        n = len(grid[0])
        f = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(m):
            for j in range(n):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0:
                    if grid[i][j - 1] == 0:
                        f[i][j] = f[i][j - 1]
                    continue

                if j == 0:
                    if grid[i - 1][j] == 0:
                        f[i][j] = f[i-1][j]
                    continue

                if grid[i][j] == 1:
                    f[i][j] = 0
                    continue

                if grid[i][j - 1] == 0:
                    f[i][j] += f[i][j - 1]

                if grid[i - 1][j] == 0:
                    f[i][j] += f[i - 1][j]

        return f[m - 1][n - 1]


class Solution(object):
    def uniquePathsWithObstacles(self, grid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        if grid[0][0] == 1 or grid[-1][-1] == 1:
            return 0

        m = len(grid)
        n = len(grid[0])
        f = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(m):
            for j in range(n):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                if i == 0:
                    if grid[i][j] == 0:
                        f[i][j] = f[i][j - 1]
                    continue

                if j == 0:
                    if grid[i][j] == 0:
                        f[i][j] = f[i - 1][j]
                    continue

                if grid[i][j] == 0:
                    f[i][j] = f[i][j - 1] + f[i - 1][j]
                elif grid[i][j] == 1:
                    f[i][j] = 0

        for row in f:
            print(row)
        return f[m - 1][n - 1]

obstacleGrid = [
    [0, 0, 0],
    [0, 1, 0],
    [0, 0, 0],
] #exp 2


obstacleGrid = [
    [0,0],
    [1,0]
] #exp = 1

print('ans:', Solution().uniquePathsWithObstacles(obstacleGrid))
