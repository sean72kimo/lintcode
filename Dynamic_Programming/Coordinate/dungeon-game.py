"""
similar to unique path i, ii
但是是從終點往起點走
所以初始化last row and last col
last row要從右往左走
last col要從下往上走
和unique path那題的行進方向正好相反
"""

class Solution:
    def calculateMinimumHP(self, grid):
        if len(grid) == 0:
            return 0

        m = len(grid)
        n = len(grid[0])
        f = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(m - 1, -1, -1):
            for j in range(n - 1, -1, -1):
                if i == m - 1 and j == n - 1:
                    f[i][j] = max(1, 1 - grid[i][j])
                    continue

                if i == m - 1:
                    f[i][j] = max(1, f[i][j + 1] - grid[i][j])
                    continue

                if j == n - 1:
                    f[i][j] = max(1, f[i + 1][j] - grid[i][j])
                    continue

                f[i][j] = max(1, min(f[i][j + 1] - grid[i][j], f[i + 1][j] - grid[i][j]))

        return f[0][0]