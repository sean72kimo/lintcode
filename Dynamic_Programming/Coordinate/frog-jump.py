import collections


class Solution(object):
    def canCross(self, stones):
        """
        :type stones: List[int]
        :rtype: bool
        """
        dst = stones[-1]
        start = stones[1]
        return self.dfs(start, set(stones), 1, dst)

    def dfs(self, start, stones, jump, dst):
        print(start)
        if start == dst:
            return True

        res = False
        for step in [jump - 1, jump, jump + 1]:
            nxt = start + step
            if nxt <= 0 or nxt == start:
                continue

            if nxt not in stones:
                continue

            if self.dfs(nxt, stones, step, dst):
                return True

        return res

stones = [0,1,3,5,6,8,12,17]
a = Solution().canCross(stones)
print("ans:", a)

class Solution:

    def canCross(self, stones):
        """
        :type stones: List[int]
        :rtype: bool
        """
        if len(stones) <= 1:
            return True
        if stones[0] + 1 != stones[1]:
            return False
        s = set(stones)
        que = collections.deque([  [stones[0], 1 ] ])
        vst = set((stones[0], 1))
        end = stones[-1]

        while que:
            curr, j = que.popleft()

            for i in range(-1, 2):
                step = j + i
                if step <= 0:
                    continue

                nxt = curr + step

                if nxt == end:
                    return True

                if nxt in s and (nxt, step) not in vst:
                    que.append([nxt, j + i])
                    vst.add((nxt, step))
        return False


stones = [0, 1, 3, 5, 6, 8, 12, 17]
# stones = [0, 1, 2, 3, 4, 8, 9, 11]
stones = [0, 1, 3, 6, 10, 13, 15, 18]
# stones = [0, 2]
# a = Solution().canCross(stones)
# print("ans:", a)

class Solution_dfs(object):
    def canCross(self, stones):
        self.mem = {}
        stones_set = set(stones)
        self.last = stones[-1]

        return self.dfs(stones[0], 1, stones_set, True)

    def dfs(self, pos, jump, stones, first=False):
        if (pos, jump) in self.mem:
            return self.mem[(pos, jump)]

        if pos == self.last:
            return True

        if jump <= 0 or pos not in stones:
            return False

        if first:
            steps = [jump]
        else:
            steps = [jump-1, jump, jump+1]

        res = False
        for step in steps:
            if step <= 0:
                continue
            res |= self.dfs(pos + step, step, stones)

        self.mem[(pos, jump)] = res
        return res



# stones = [0, 1, 3, 6, 10, 13, 15, 18] #True
# stones = [0,1,3,5,6,8,12,17] # True
# stones = [0,1,2,3,4,8,9,11] # False
# stones = [0, 2] #False
# # stones = [0, 3, 4] #False
# a = Solution_dfs().canCross(stones)
# print("ans:", a)

class Solution_dfs3(object):
    def canCross(self, stones):
        self.mem = {}
        self.stones = stones
        unit2stone = {stones[i]: i for i in range(len(stones))}
        return self.dfs(stones[0], 1, unit2stone, True)

    def dfs(self, i, step, unit2stone, first=False):
        if (i, step) in self.mem:
            return self.mem[(i, step)]

        if step <= 0 or i >= len(self.stones):
            return False

        if i == len(self.stones) - 1:
            return True

        if first:
            steps = [step]
        else:
            steps = [step - 1, step, step + 1]

        res = False
        for s in steps:
            unit = self.stones[i] + s
            if unit in unit2stone:
                res |= self.dfs(unit2stone[unit], s, unit2stone)

        self.mem[(i, step)] = res
        return res

stones = [0, 1, 3, 6, 10, 13, 15, 18] #True
# stones = [0,1,3,4] # True
# stones = [0,1,3,5,6,8,12,17] # True
stones = [0,1,2,3,4,8,9,11] # False
# stones = [0, 2] #False
# stones = [0, 3, 4] #False
a = Solution_dfs3().canCross(stones)
print("ans:", a)