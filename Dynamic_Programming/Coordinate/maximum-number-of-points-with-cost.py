import collections
from typing import List

# m * n * n
class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:
        m = len(points)
        n = len(points[0])

        f = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(m):
            for j in range(n):
                if i == 0:
                    f[i][j] = points[i][j]
                    continue

                for k in range(n):
                    f[i][j] = max(f[i - 1][k] + points[i][j] - abs(k - j), f[i][j])

        return max(f[-1])


points = [[1,3,2],[1,5,1],[3,1,1]]
a = Solution().maxPoints(points)
print("ans:", a)


# m * n
class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:
        m = len(points)
        n = len(points[0])

        f = [[0 for _ in range(n)] for _ in range(m)]
        g = [[0 for _ in range(n)] for _ in range(m)]

        for i in range(1, m):
            for j in range(n):

                b = 0
                a = points[i - 1][j]
                if j - 1 >= 0:
                    b = f[i][j - 1] - 1
                f[i][j] = max(a, b)

            for j in range(n - 1, -1, -1):
                b = 0
                a = points[i - 1][j]
                if j + 1 < n:
                    b = g[i][j + 1] - 1
                g[i][j] = max(a, b)

            for j in range(n):
                points[i][j] += max(f[i][j], g[i][j])

        return max(points[-1])

# points = [[1,2,3],[1,5,1],[3,1,1]]
points = [[1,3,2],[1,5,1],[3,1,1]]

a = Solution().maxPoints(points)
print("ans:", a)


class Solution:
    def lengthOfLongestSubstringKDistinct(self, s: str, k: int) -> int:
        mp = collections.Counter()
        j = 0
        ans = 0
        dup = 0
        for i in range(len(s)):
            mp[s[i]] += 1

            while mp and len(mp) > k:
                mp[s[j]] -= 1
                if mp[s[j]] == 0:
                    mp.pop(s[j])
                j += 1

            ans = max(i - j + 1, ans)

        return ans
s = "eceba"
k = 2
a = Solution().lengthOfLongestSubstringKDistinct(s, k)
print("ans:", a)