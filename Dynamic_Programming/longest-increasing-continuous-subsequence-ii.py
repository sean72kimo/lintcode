class Solution:
    """
    @param: A: An integer matrix
    @return: an integer
    """
    def __init__(self):
        self.ans = 1
        self.dp = []
        self.flago = []
    def longestIncreasingContinuousSubsequenceII(self, A):
        # write your code here
        if not any(A):
            return 0
        n = len(A)
        m = len(A[0])
        res = 1
        self.dp = [[1 for i in range(m)] for j in range(n)]
        self.flag = [[0 for i in range(m)] for j in range(n)]

        for i in range(n):
            for j in range(m):
                self.dp[i][j] = self.dfs(i, j, A)
                res = max(res, self.dp[i][j])

        for row in self.dp:
            print(row)
        return res

    def dfs(self, x, y, A):
#         if self.flag[x][y]:
#             return self.dp[x][y]

        length = 1
        dxy = [[0, 1], [0, -1], [1, 0], [-1, 0]]
        for i in range(len(dxy)):

            nx = x + dxy[i][0]
            ny = y + dxy[i][1]

            if 0 <= nx < len(A) and 0 <= ny < len(A[0]):
                if A[x][y] > A[nx][ny]:
                    length = max(length, self.dfs(nx, ny, A) + 1)

        self.flag[x][y] = 1
        self.dp[x][y] = length
        return length



A = [[1, 2, 3, 4, 5], [16, 17, 24, 23, 6], [15, 18, 25, 22, 7], [14, 19, 20, 21, 8], [13, 12, 11, 10, 9]]
# A = [[1, 5, 3], [4, 10, 9], [2, 8, 7]]
for row in A:
    print(row)
print('~~~~~~~~')
print('ans:', Solution().longestIncreasingContinuousSubsequenceII(A))
