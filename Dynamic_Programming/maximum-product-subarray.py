from sys import maxsize
class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        curr_max = nums[0]
        curr_min = nums[0]
        ans = nums[0]

        for i in range(1, len(nums)):
            n = nums[i]
            a = curr_max * n
            b = curr_min * n

            curr_max = max(a, b, n)
            curr_min = min(a, b, n)
            ans = max(curr_max, ans)

        return ans

    def maxProduct2(self, nums):
        # write your code here
        """
        :type nums: List[int]
        :rtype: int
        """

        if len(nums) == 0:
            return 0

        n = len(nums)
        f = [None] * n  # 以下標i結尾的subarray最大乘積
        g = [None] * n  # 以下標i結尾的subarray最小乘積

        for i in range(n):
            if i == 0:
                f[i] = nums[i]
                g[i] = nums[i]
                continue

            f[i] = max(g[i - 1] * nums[i], f[i - 1] * nums[i], nums[i])
            g[i] = min(g[i - 1] * nums[i], f[i - 1] * nums[i], nums[i])

        return max(f)


    def maxProduct3(self, nums):
        # write your code here
        n = len(nums)

        f = [0] * n
        g = [0] * n
        fstart = [0] * n
        gstart = [0] * n
        res = -maxsize
        stop = -1

        for i in range(n):
            f[i] = nums[i]
            g[i] = nums[i]
            fstart[i] = i
            gstart[i] = i

        for i in range(n):
            if i > 0:
                f[i] = max(f[i], nums[i] * f[i - 1], nums[i] * g[i - 1])
                if f[i] == nums[i] * f[i - 1]:
                    fstart[i] = fstart[i - 1]
                elif f[i] == nums[i] * g[i - 1]:
                    fstart[i] = gstart[i - 1]
                else:
                    fstart[i] = i

                g[i] = min(g[i], nums[i] * f[i - 1], nums[i] * g[i - 1])
                if g[i] == nums[i] * f[i - 1]:
                    gstart[i] = fstart[i - 1]
                elif g[i] == nums[i] * g[i - 1]:
                    gstart[i] = gstart[i - 1]
                else:
                    gstart[i] = i

            res = max(res, f[i])

            if res == f[i]:
                stop = i

        # so the max subarray range is A[fstart[stop]:stop+1]
        print(fstart[stop], stop, nums[fstart[stop]:stop + 1])

        return res

nums = [0, 0, 3, 3, 0, -2, 3, -2, -1, -1, 2, 1]

a = Solution().maxProduct(nums)
print(a)
