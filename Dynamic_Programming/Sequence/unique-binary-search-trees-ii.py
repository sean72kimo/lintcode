from lib.binarytree import TreeNode


class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        if n == 0:
            return []

        if n == 1:
            return [TreeNode(1)]
        self.mem = {}
        return self.dfs(1, n)

    def dfs(self, start, end):
        if (start, end) in self.mem:
            return self.mem[(start, end)]

        if start == end:
            return [TreeNode(start)]

        if start > end:
            return [None]

        res = []
        for i in range(start, end + 1):
            left = self.dfs(start, i - 1)
            right = self.dfs(i + 1, end)

            for l in left:
                for r in right:
                    root = TreeNode(i)
                    root.left = l
                    root.right = r
                    res.append(root)

        self.mem[(start, end)] = res
        return res

class Solution2(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        if n == 0:
            return []

        if n == 1:
            return [TreeNode(1)]

        self.mem = {}
        return self.dfs(n, n, set())

    def dfs(self, n, k, vst):
        print(k)

        if k in self.mem:
            return self.mem[n]

        if k <= 0 or k > n:
            return [None]

        if k == 1:
            return [TreeNode(1)]

        res = []
        for i in range(1, k+1):

            vst.add(i)
            left = self.dfs(n, k - 1, vst)
            right = self.dfs(n, k + 1, vst)
            vst.remove(i)

            for l in left:
                for r in right:
                    root = TreeNode(i)
                    root.left = l
                    root.right = r
                    res.append(root)

        self.mem[k] = res
        return res





a = Solution2().generateTrees(3)
print(a)