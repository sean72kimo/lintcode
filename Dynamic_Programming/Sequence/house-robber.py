class Solution:
    """
    @param: A: An array of non-negative integers
    @return: The maximum amount of money you can rob tonight
    """
    def houseRobber_coor(self, A):
        # write your code here
        rob = 0
        no_rob = 0

        for n in A:
            pre_max = max(rob, no_rob)
            rob = no_rob + n
            no_rob = pre_max

        return max(rob, no_rob)


    def houseRobber(self, A):
        # write your code here
        if not A or len(A) == 0:
            return 0

        n = len(A)

        if n == 1:
            return A[0]

        f = [None for i in range(2)]
        f[0] = 0
        f[1] = A[0]

        for i in range(2, n + 1):
            f[i % 2] = max(f[(i - 1) % 2], f[(i - 2) % 2] + A[i - 1])

        print(f)
        return f[-1]

A = [3, 8, 4]
a = Solution().houseRobber(A)
print('ans:', a)
