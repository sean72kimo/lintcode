class Solution(object):

    def maxSumOfThreeSubarrays(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        res = [None] * 3
        if len(nums) == 0:
            return res
        n = len(nums)
        sum = [0] * n
        sum[0] = nums[0]

        for i in range(1, n):
            sum[i] = sum[i - 1] + nums[i]
            if i >= k:
                sum[i] = sum[i] - nums[i - k]
        print(sum)

        left = [0] * n
        l_mx_idx = k - 1
        for i in range(k - 1, n):
            if sum[i] > sum[l_mx_idx]:
                left[i] = i
                l_mx_idx = i
            else:
                left[i] = l_mx_idx

        print(left)

        right = [0] * n
        r_mx_idx = n - 1
        for i in range(n - 1, k, -1):
            if sum[i] > sum[r_mx_idx]:
                right[i] = i
                r_mx_idx = i
            else:
                right[i] = r_mx_idx

        print(right)

        mx = float('-inf')
        for i in range(n):
            if i - k < 0 or i + k > n - 1:
                continue
            sum3 = sum[left[i - k]] + sum[i] + sum[right[i + k]]
            if sum3 > mx:
                mx = sum3
                res[0] = left[i - k] - k + 1
                res[1] = i - k + 1
                res[2] = right[i + k] - k + 1
        return res


nums, k = [1, 2, 1, 2, 6, 7, 5, 1], 2
a = Solution().maxSumOfThreeSubarrays(nums, k)
print("ans:", a)


class Solution2(object):

    def maxSumOfThreeSubarrays(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        bestSeq = 0
        bestTwoSeq = [0, k]
        bestThreeSeq = [0, k, 2 * k]

        seqSum = sum(nums[:k])
        seqTwoSum = sum(nums[k:2 * k])
        seqThreeSum = sum(nums[2 * k:3 * k])

        bestSeqSum = seqSum
        bestTwoSum = seqSum + seqTwoSum
        bestThreeSum = seqSum + seqTwoSum + seqThreeSum


nums = [1, 2, 1, 2, 6, 7, 5, 1]
k = 2
a = Solution2().maxSumOfThreeSubarrays(nums, k)
# print("ans:", a)
