class Solution:

    def numSubarrayProductLessThanK(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        f = [1]

        for num in nums:
            f.append(f[-1] * num)

        print(f)
        ans = 0
        j = 0
        for i in range(len(nums)):
            for j in range(j, i + 1):
                if f[i + 1] // f[j] < k:
                    ans += i - j + 1
                    break
        return ans


nums = [10 , 1, 1, 1, 1, 1, 5, 2, 6, 2, 3, 4, 5, 6, 7, 89, 10, 11]
k = 100
a = Solution().numSubarrayProductLessThanK(nums, k)
print("ans:", a)

