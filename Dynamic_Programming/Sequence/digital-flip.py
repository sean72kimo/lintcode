class Solution:
    """
    @param nums: the array
    @return: the minimum times to flip digit
    """
    def flipDigit(self, nums):
        # Write your code here
        if nums is None or len(nums) == 0:
            return 0
        
        n = len(nums)
        f = [[None for _ in range(2)] for _ in range(n)]
        
        if nums[0] == 0:
            f[0][0] = 0
            f[0][1] = 1
        else:
            f[0][0] = 1
            f[0][1] = 0
            
        for i in range(1, n):
            if nums[i-1] == 1:
                f[i][1] = min(f[i-1][0], f[i-1][1])
                f[i][0] = min(f[i-1][0], f[i-1][1])
            else:
                f[i][1] = min(f[i-1][0], f[i-1][1] + 1)
                f[i][0] = min(f[i-1][0], f[i-1][1])
            print("nums[{}] = {}".format( i, nums[i]))
            print(f[i])
        return min(f[n-1][0], f[n-1][1])
                
    
nums = [1,0,0,1,1,1]
a = Solution().flipDigit(nums)
print("ans:", a)