from collections import defaultdict


class Solution:

    def numberOfArithmeticSlices(self, A):
        """
        :type A: List[int]
        :rtype: int
        """

        total = 0
        n = len(A)
        dp = [defaultdict(int) for _ in range(n)]
        for r in dp:
            print(r)
        for i in range(n):
            for j in range(i):
                diff = A[i] - A[j]
                dp[i][diff] += 1
                if diff in dp[j]:
                    dp[i][diff] += dp[j][diff]
                    total += dp[j][diff]
        for r in dp:
            print(r)

        return total


A = [2, 4, 6, 8, 10]
a = Solution().numberOfArithmeticSlices(A)
print("ans:", a)
