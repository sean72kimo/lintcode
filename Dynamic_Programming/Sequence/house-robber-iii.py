"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        sef.val = val
        self.left, self.right = None, None
"""
#  similar to 24. Binary Tree Maximum Path Sum有點類似的味道。在當前點，都要仔細考慮左右兒子回傳答案的各種組合，並且取最大值。


class Solution:
    def rob(self, root) -> int:
        if not root:
            return 0

        rob, no = self.dfs(root)
        return max(rob, no)

    def dfs(self, root):
        if not root:
            return 0, 0

        rob_left, no_left = self.dfs(root.left)
        rob_right, no_right = self.dfs(root.right)

        rob_this = root.val + no_left + no_right
        no_this = max(rob_left + rob_right, no_left + no_right, no_left + rob_right, no_right + rob_left)

        return rob_this, no_this

class Solution:
    """
    @param: root: The root of binary tree.
    @return: The maximum amount of money you can rob tonight
    """
    def houseRobber3(self, root):
        # write your code here
        rob, no_rob = self.visit(root)
        return max(rob, no_rob)

    def visit(self, root):
        if root is None:
            return 0, 0

        left_rob, left_no_rob = self.visit(root.left)
        right_rob, right_no_rob = self.visit(root.right)

        rob = root.val + left_no_rob + right_no_rob
        no_rob = max(left_rob, left_no_rob) + max(right_rob, right_no_rob)
        return rob, no_rob
