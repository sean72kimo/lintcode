class Solution(object):
    def longestStrChain(self, words):
        """
        :type words: List[str]
        :rtype: int
        """
        if len(words) == 0:
            return 0

        n = len(words)
        f = [1 for _ in range(n)]
        words.sort(key=lambda w: len(w))
        print(words)
        for i in range(n):
            for j in range(i):
                if self.valid(words[j], words[i]):
                    f[i] = max(f[j] + 1, f[i])

        return max(f)

    def valid(self, s, t):
        if len(s) >= len(t):
            return False

        for i in range(len(s)):
            if s[i] == t[i]:
                continue
            return s[i:] == t[i + 1:]

        return True
words = ["a","b","ba","bca","bda","bdca"]
a = Solution().longestStrChain(words)
print("ans:",a )
