class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n == 0:
            return 0
        
        f = [0 for _ in range(n+1)]
        f[0] = 1
        for i in range(1, n+1):
            
            if s[i-1] != '0':
                f[i] += f[i-1]
                
            if i - 2 >= 0 and 10 <= int(s[i-2:i]) <= 26:
                print(i, s[i-2:i])
                f[i] += f[i-2]
            
        return f[n]
        
s = "12"
s = "226"
s = "712"
s = "01"
a = Solution().numDecodings(s)
print("ans:", a)


class Solution2_dfs_TLE(object):
    def __init__(self):
        self.ans = 0
        self.mem = None
        
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n == 0:
            return 0
        
        self.dfs(s, 0)
        return self.ans
    
    def dfs(self, s, i):

        if i == len(s):
            self.ans += 1
            
        for j in range(1,3):
            if i+j > len(s):
                return
            
            sub = s[i:i+j]
            if sub == '0':
                break
            
            num = int(sub)
            if 1 <= num <= 26:
                self.dfs(s, i+j)

# s = "12"
# a = Solution2_dfs_TLE().numDecodings(s)
# print("ans:", a)




class Solution_dfs_mem(object):
    def __init__(self):
        self.mem = None
        
    def __init__(self):
        self.mem = None
        
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n == 0:
            return 0
        self.mem = [None for _ in range(n+1)]

        return self.dfs(s, 0)
    
    def dfs(self, s, i):
        
        if self.mem[i] is not None:
            return self.mem[i]
        
        res = 0
        if i == len(s):
            return 1
        
        for j in range(1,3):
            if i+j > len(s):
                break

            sub = s[i:i+j]
            if sub == '0':
                self.mem[i] = 0
                return 0
            
            num = int(sub)
            
            if 1 <= num <= 26:
                res += self.dfs(s, i+j)

        self.mem[i] = res
        return res

s = "12"
# a = Solution_dfs_mem().numDecodings(s)
# print("ans:",a )


class Solution3(object):
    def __init__(self):
        self.ans = []
        
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        if n == 0:
            return 0
        self.dfs2(s, 0, [])
        return self.ans
    
    def dfs(self, s, i, path):
        if i == len(s):
            self.ans.append(''.join(path[:]))
            print(path)
            return
        
        if s[i] == '0':
            return
        num1 = s[i:i+1]
        path.append( chr(int(num1) + ord('A') - 1 ))
        self.dfs(s, i+1, path)
        path.pop()


        if i+2 <= len(s):
            num2 = s[i:i+2]
            if 10 <= int(num2) <= 26: 
                path.append( chr(int(num2) + ord('A') - 1 ))
                self.dfs(s, i+2, path)
                path.pop()
                
    def dfs2(self, s, i, path):
        if i == len(s):
            self.ans.append(''.join(path[:]))
            return
        
        for j in range(1,3):
            if i + j > len(s):
                return
            
            sub = s[i:i+j]
            if sub == '0':
                continue
            
            num = int(sub)
            if 1 <= num <= 26:
                path.append( chr(int(num) + ord('A') - 1 ))
                self.dfs(s, i+j, path)
                path.pop()



s = "12"
# a = Solution3().numDecodings(s)
# print("ans:", a)