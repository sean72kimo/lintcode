class Solution:
    def numTilings(self, N: int) -> int:
        f = [[0 for _ in range(2)] for _ in range(N)]
        for i in range(N):
            if i == 0:
                f[i][0] = 0
                f[i][1] = 0
                continue

            if i == 1:
                f[i][0] = 1
                f[i][1] = 0
                continue

            f[i][0] = f[i - 1][0] + f[i - 2][0] + 2 * f[i - 1][1]
            f[i][1] = f[i - 2][0] + f[i - 1][1]

        print(f)
        return f[N - 1][0] + f[N - 1][1]

sol = Solution()
a = sol.numTilings(3)
print("ans:", a)


