from sys import maxsize
class Solution(object):
    def maxProfit(self, k, P):
        """
        :type prices: List[int]
        :rtype: int
        """
        n = len(P)

        if n == 0:
            return 0

        res = 0
        if k > n // 2 :
            for i in range(n - 1):
                res += max(P[i + 1] - P[i], 0)
            return res


        f = [[-maxsize for _ in range(2 * k + 1)] for _ in range(2)]
        f[0][0] = 0

        for i in range(1, n + 1):
            for j in range(0, 2 * k + 1, 2):
                f[i % 2][j] = f[(i - 1) % 2][j]
                if i - 2 >= 0 and j - 1 >= 0:
                    profit = P[i - 1] - P[i - 2]
                    f[i % 2][j] = max(f[i % 2][j], f[(i - 1) % 2][j - 1] + profit)

            for j in range(1, 2 * k + 1, 2):
                f[i % 2][j] = f[(i - 1) % 2][j - 1]
                if i - 2 >= 0:
                    profit = P[i - 1] - P[i - 2]
                    f[i % 2][j] = max(f[(i - 1) % 2][j] + profit, f[i % 2][j])

        res = 0
        for i in range(0, 2 * k + 1, 2):
            res = max(res, f[n % 2][i])
        return res

k = 2
prices = [4, 4, 6, 1, 1, 4, 2, 5]

k = 2
prices = [2, 1, 4, 5, 2, 9, 7]
a = Solution().maxProfit(k, prices)
print("ans:", a)
