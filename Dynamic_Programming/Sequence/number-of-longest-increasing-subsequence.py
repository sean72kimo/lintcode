class Solution:

    def findNumberOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        n = len(nums)
        f = [1] * n
        g = [1] * n

        for i in range(n):
            if i == 0:
                continue
            for j in range(i):
                if nums[j] < nums[i]:
                    if f[j] >= f[i]:
                        f[i] = f[j] + 1
                        g[i] = g[j]
                    elif f[i] == f[j] + 1:
                        g[i] += g[j]
        print(f)
        print(g)
        mx = max(f)
        cnt = 0
        for i in range(n):
            if f[i] == mx:
                cnt += g[i]
        return cnt


nums = [1, 3, 5, 4, 7, 2, 1, 3, 5, 4, 7]
a = Solution().findNumberOfLIS(nums)
print("ans:", a)
