class Solution:

    def numberOfArithmeticSlices(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        n = len(A)
        f = [0] * n
        ans = 0
        for i in range(2, n):
            if A[i] - A[i - 1] == A[i - 1] - A[i - 2]:
                f[i] = 1 + f[i - 1]
        print(f)
        return sum(f)


class Solution2:

    def numberOfArithmeticSlices(self, A):
        n = len(A)
        f = [0] * 2
        ans = 0
        for i in range(2, n):
            if A[i] - A[i - 1] == A[i - 1] - A[i - 2]:
                f[i % 2] = 1 + f[(i - 1) % 2]
                ans += f[i % 2]
        return ans


A = [1, 2, 3, 4, 5]
A = [1, 2, 3, 8, 9, 10]  # 2
a = Solution().numberOfArithmeticSlices(A)
print("ans:", a)
b = Solution2().numberOfArithmeticSlices(A)
print("ans:", b)

