"""
f[i]代表有i個節點時，可構成幾種bst
f[0] init 為 1

i 迴圈枚舉有幾個節點
j 迴圈枚舉左樹中的節點個數，i-j-1代表右樹節點個數(並扣掉1個根節點)
https://www.youtube.com/watch?v=HWJEMKWzy-Q
"""
class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n < 1:
            return 0
        f = [0 for i in range(n + 1)]


        f[0] = 1
        f[1] = 1

        for i in range(2, n + 1):
            for j in range(i):
                cur = f[j] * f[i - j - 1]
                f[i] += cur

        return f[n]

print('ans:', Solution().numTrees(5))
