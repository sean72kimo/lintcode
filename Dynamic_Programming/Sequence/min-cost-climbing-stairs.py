import collections


class Solution(object):
    def minCostClimbingStairs(self, cost):
        """
        :type cost: List[int]
        :rtype: int
        """
        if len(cost) <= 1:
            return 0
        n = len(cost)
        f = [0 for _ in range(n+1)]

        for i in range(2, n+1):
            f[i] = min(f[i-1]+cost[i-1], f[i-2]+cost[i-2])

        return f[n]

# 打印路徑
class Solution_print_path(object):
    def minCostClimbingStairs(self, cost):
        """
        :type cost: List[int]
        :rtype: int
        """
        if len(cost) <= 1:
            return 0
        n = len(cost)
        f = [0 for _ in range(n+1)]
        p = collections.defaultdict(list)
        pi = [-1 for _ in range(n + 1)]

        idx = -1
        for i in range(2, n+1):

            if f[i-1]+cost[i-1] < f[i-2]+cost[i-2]:

                f[i] = f[i - 1] + cost[i - 1]
                p[i] = p[i-1] + [cost[i-1]]
                pi[i] = i-1
                idx = i-1



            else:
                f[i] = f[i - 2] + cost[i - 2]
                p[i] = p[i - 2] + [cost[i - 2]]
                pi[i] = i - 2
                idx = i - 2


        while idx != -1:
            print(cost[idx], end=',')
            idx = pi[idx]
        print()
        print(p[n])
        print(pi)
        return f[n]
cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
a = Solution_print_path().minCostClimbingStairs(cost)
print("ans:", a)

class Solution_dfs(object):
    def minCostClimbingStairs(self, cost):
        """
        :type cost: List[int]
        :rtype: int
        """
        if len(cost) <= 1:
            return 0
        n = len(cost)
        f = [0 for _ in range(n+1)]

        for i in range(2, n+1):
            f[i] = min(f[i-1]+cost[i-1], f[i-2]+cost[i-2])

        return f[n]
    def dfs(self, n, cost):
        if len(cost) <= 1:
            return 0
