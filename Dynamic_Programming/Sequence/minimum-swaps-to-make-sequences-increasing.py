class Solution(object):

    def minCostII(self, costs):
        """
        :type costs: List[List[int]]
        :rtype: int
        """

        if len(costs) == 0:
            return 0
        MAX = float('inf')
        m = len(costs)
        n = len(costs[0])

        f = [[MAX for i in range(n)] for i in range(m)]
        preMn1C = None
        preMn2C = None
        for i in range(m):
            curMn1C = None
            curMn2C = None

            for j in range(n):
                if i == 0:
                    f[i][j] = costs[i][j]
                    if curMn1C is None or f[i][j] < f[i][curMn1C]:
                        curMn2C = curMn1C
                        curMn1C = j
                    elif curMn2C is None or f[i][j] < f[i][curMn2C]:
                        curMn2C = j
                    continue

                if j == preMn1C:
                    f[i][j] = f[i - 1][preMn2C] + costs[i][j]
                else:
                    f[i][j] = f[i - 1][preMn1C] + costs[i][j]

                if curMn1C is None or f[i][j] < f[i][curMn1C]:
                    curMn2c = curMn1C
                    curMn1c = j
                elif curMn2C is None or f[i][j] < f[i][curMn2C]:
                    curMn2c = j

            preMn1C = curMn1C
            preMn2C = curMn2C
            print(i, preMn1C, preMn2C, curMn1C, curMn2C)

        return min(f[-1])


costs = [[20, 19, 11, 13, 12, 16, 16, 17, 15, 9, 5, 18], [3, 8, 15, 17, 19, 8, 18, 3, 11, 6, 7, 12], [15, 4, 11, 1, 18, 2, 10, 9, 3, 6, 4, 15]]
a = Solution().minCostII(costs)
print("ans:", a)

