class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0

        if len(nums) == 1:
            return nums[0]

        size = len(nums)


        # rob first house:
        rob = 0
        norob = 0
        mx = max(rob, norob)
        nums1 = nums[0:size - 1]
        print(nums1)

        for n in nums1:
            mx = max(rob, norob)
            rob = norob + n
            norob = mx

        mx1 = max(rob, norob)


        # rob last house:
        rob = 0
        norob = 0
        mx = max(rob, norob)

        nums2 = nums[1:size]
        print(nums, nums[n])

        for n in nums2:
            mx = max(rob, norob)
            rob = norob + n
            norob = mx

        mx2 = max(rob, norob)

        return max(mx1, mx2)

A = [1, 2]
print("ans:", Solution().rob(A))


