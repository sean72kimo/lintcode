class Solution(object):
    def maxProfit(self, prices, fee):
        """
        :type prices: List[int]
        :type fee: int
        :rtype: int
        """
        if len(prices) == 0:
            return 0
        n = len(prices)
        # f[i][0] 代表前i天結束後，沒有股票
        # f[i][1] 代表前i天結束後，擁有股票
        f = [[0 for _ in range(2)] for _ in range(n + 1)]

        for i in range(n + 1):
            if i == 0:
                f[i][0] = 0
                f[i][1] = 0
                continue
            f[i][0] = f[i - 1][0]  # f[i][0] 代表前i天結束後，沒有股票。此時最大獲利可能為 1.前一天沒有股票的狀態，或者
            f[i][1] = f[i - 1][0]  # f[i][1] 代表前i天結束後，擁有股票。此時最大獲利可能為 1.前一天沒有股票的狀態，或者

            if i - 2 >= 0:
                profit = prices[i - 1] - prices[i - 2]
                f[i][0] = max(f[i - 1][0], f[i - 1][1] + profit - fee)  # 2. 前一天擁有股票，今天賣出(因此加上這天的profit，並扣掉手續費)
                f[i][1] = max(f[i - 1][1] + profit, f[i - 1][0])  # 2. 前一天擁有股票，並持續擁有(因此加上這天的profit)

        return f[n][0]

prices = [1, 3, 2, 8, 4, 9]
fee = 2
a = Solution().maxProfit(prices, fee)
print("ans:", a)
