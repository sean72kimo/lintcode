class Solution(object):

    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        if len(prices) == 0:
            return 0

        n = len(prices)
        f = [[None for _ in range(2)] for _ in range(n + 1)]
        # 0 no share
        # 1 with share

        for i in range(n + 1):
            if i == 0:  # 前0天，不論狀態為有股票或者沒股票，前0天的獲利一定是0
                f[i][0] = 0
                f[i][1] = 0
                continue

            # 前1天，
            # 今天無股票：則獲利依然為0。
            # 今天有股票：此情形不可能發生。所以獲利依然是0
            # 因為今天買股票，需要額外cooldown一天，也就是i-1為cooldpwn，i-2為sell
            if i == 1:
                f[i][0] = 0
                f[i][1] = 0
                continue

            if i - 2 >= 0:
                profit = prices[i - 1] - prices[i - 2]
                f[i][0] = max(f[i - 1][0], f[i - 1][1] + profit)
                f[i][1] = max(f[i - 1][1] + profit, f[i - 2][0])

        return f[n][0]


P = [1, 2, 3, 0, 2]
# P = [2, 1, 2, 0, 1]
a = Solution().maxProfit(P)
print("ans:", a)
