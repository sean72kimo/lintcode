class Solution(object):

    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s == None:
            return 0
        mod = 1000000007
        n = len(s)
        f = [0] * (n + 1)
        f[0] = 1

        for i in range(1, n + 1):
            t = self.calc1(s[i - 1])

            f[i] = t * f[i - 1]

            if i > 1:
                t = self.calc2(s[i - 2], s[i - 1])
                f[i] += t * f[i - 2]

            f[i] %= mod

        return f[n]

    def calc1(self, c):
        if c == '0': return 0
        if c == '*': return 9  # 1~9
        return 1

    def calc2(self, c2, c1):
        if c2 == '0': return 0

        if c2 >= '3' and c2 <= '9': return 0

        if c2 == '1':
            if c1 == '*': return 9
            return 1

        if c2 == '2':
            if c1 >= '0' and c1 <= '6': return 1
            if c1 != '*' and c1 >= '7': return 0
            return 6

        if c2 == '*':
            if c1 == '*': return 15
            if c1 <= '6': return 2
            return 1  # 7 <= c1 <= 9


s = '1*'
a = Solution().numDecodings(s)
print("ans:", a)
