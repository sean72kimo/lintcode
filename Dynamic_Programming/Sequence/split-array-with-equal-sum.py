class Solution(object):
    def splitArray(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) == 0:
            return 0

        pre = [0]
        for n in nums:
            pre.append(pre[-1] + n)

        n = len(nums)
        for j in range(3, n - 3):
            hsset = set()
            for i in range(1, j - 1):
                sum1 = pre[i]
                sum2 = pre[j] - pre[i + 1]
                if sum1 == sum2:
                    hsset.add(sum1)

            for k in range(j + 1, n - 1):
                sum3 = pre[k] - pre[j + 1]
                sum4 = pre[n] - pre[k + 1]
                if sum3 == sum4 and sum3 in hsset:
                    return True
        return False

class Solution:
    def splitArray(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        pre = [0]
        for n in nums:
            pre.append(pre[-1] + n)
        n = len(nums)
        for i in range(1, n):
            sum1 = pre[i]
            for j in range(i + 2, n - 3):
                sum2 = pre[j] - pre[i + 1]
                for k in range(j + 2, n - 1):
                    sum3 = pre[k] - pre[j + 1]
                    sum4 = pre[n] - pre[k + 1]
                    print(sum1, sum2, sum3, sum4)
                    if sum1 == sum2 == sum3 == sum4:
                        return True
        return False
nums = [1, 2, 1, 2, 1, 2, 1]
a = Solution().splitArray(nums)
print("ans:", a)
