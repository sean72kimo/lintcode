from sys import maxsize
class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        if len(prices) == 0:
            return 0
        n = len(prices)
        f = [[0 for _ in range(5)] for _ in range(n + 1)]


        for i in range(n + 1):
            for j in range(5):
                if j in [0, 2, 4]:  # 如果今天處於沒有股票的階段(j)
                    f[i][j] = f[i - 1][j]  # 今天的最大利潤，1. 有可能等於昨天處於同樣階段(沒有股票)
                    if i - 2 >= 0 and j - 1 >= 0:  # 或者，2. 有可能昨天處在擁有股票(j-1)的階段，今天有獲利p[i-1] - p[i-2]
                        profit = prices[i - 1] - prices[i - 2]
                        f[i][j] = max(f[i - 1][j], f[i - 1][j - 1] + profit)

                if j in [1, 3]:  # 如果今天處於擁有股票的階段(j)
                    f[i][j] = f[i - 1][j - 1]  # 今天的最大利潤，有可能1.前一天沒有股票，今天剛買，f[i-1(前一天)][j-1(前一個階段，沒股票)]
                    if i - 2 >= 0:  # 或者，2.前一天處於同階段(擁有股票)，今天擁有獲利p[i-1] - p[i-2]
                        profit = prices[i - 1] - prices[i - 2]
                        f[i][j] = max(f[i - 1][j - 1], f[i - 1][j] + profit)
        return max(f[n][0], f[n][2], f[n][4])

prices = [4, 4, 6, 1, 1, 4, 2, 5]
prices = [2, 1, 4, 5, 2, 9, 7]
a = Solution().maxProfit(prices)
print("ans:", a)
