class Solution_DP_n2(object):
    def wiggleMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) < 2:
            return len(nums)
        n = len(nums)
        u = [1 for _ in range(n)]
        d = [1 for _ in range(n)]

        for i in range(1, n):
            for j in range(i):
                if nums[i] > nums[j]:  # up
                    u[i] = max(d[j] + 1, u[i])
                elif nums[i] < nums[j]:  # down
                    d[i] = max(u[j] + 1, d[i])

        return max(max(u), max(d))


class Solution_DP_n(object):
    def wiggleMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        n = len(nums)
        u = [0 for _ in range(n)]  # 最後是上升的最長wiggle seq
        d = [0 for _ in range(n)]  # 最後是下降的最長wiggle seq

        for i in range(n):
            if i == 0:
                u[i] = 1
                d[i] = 1
                continue

            if nums[i] > nums[i - 1]:  # 上升
                u[i] = d[i - 1] + 1
                d[i] = d[i - 1]

            elif nums[i] < nums[i - 1]:  # 下降
                u[i] = u[i - 1]
                d[i] = u[i - 1] + 1

            else:
                u[i] = u[i - 1]
                d[i] = d[i - 1]

        return max(u[n - 1], d[n - 1])

nums = [1,7,4,9,2,5]
a = Solution().wiggleMaxLength(nums)
print("ans:", a)