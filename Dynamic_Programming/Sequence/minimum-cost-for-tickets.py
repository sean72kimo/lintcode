class Solution(object):
    def mincostTickets(self, days, costs):
        """
        :type days: List[int]
        :type costs: List[int]
        :rtype: int
        """
        if len(days) == 0 or len(costs) == 0:
            return 0
        mx = max(days)
        dayset = set(days)
        f = [float('inf') for _ in range(mx+1)]  # 0~365

        for i in range(mx+1):
            if i == 0:
                f[0] = 0
                continue

            if i not in dayset:
                f[i] = f[i - 1]

                continue

            if i - 1 >= 0:
                f[i] = min(f[i - 1] + costs[0], f[i - 1] + costs[1], f[i - 1] + costs[2],f[i])

            if i - 7 >= 0:
                f[i] = min(f[i - 7] + costs[1], f[i - 7] + costs[2],f[i])

            if i - 30 >= 0:
                f[i] = min(f[i - 30] + costs[2], f[i])

        return f[mx]

days = [1,4,6,7,8,20]
costs = [2,7,15]
exp = 11

# days = [1,4,6,7,8,20]
# costs = [7,2,15]
# exp = 6

sol = Solution()
a = sol.mincostTickets(days, costs)
print("ans:", a)