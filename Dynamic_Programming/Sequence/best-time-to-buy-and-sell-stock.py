from sys import maxsize
class Solution(object):
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        if not any(prices):
            return 0

        n = len(prices)


        minP = maxsize
        profit = 0
        for i in range(n):
            minP = prices[i] if prices[i] < minP else minP

            if prices[i] - minP > profit:
                profit = prices[i] - minP


        print(profit)
        return profit

prices = [7, 1, 5, 3, 6, 4]
Solution().maxProfit(prices)
