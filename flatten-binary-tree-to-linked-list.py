class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
    def __str__(self):
        return "Node{0}".format(self.val)


root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(5)

node4 = TreeNode(3)
node5 = TreeNode(4)
node6 = TreeNode(6)

root.left = node2
root.right = node3

# node2.left = node4
# node2.right = node5

# node3.right = node6



class Solution:
    # @param root: a TreeNode, the root of the binary tree
    # @return: nothing
    prevNode = None

    def flatten(self, root):
        if root is None:
            return
        else:
            print('now at', root)

        if self.prevNode is not None:
            self.prevNode.left = None
            self.prevNode.right = root

        self.prevNode = root
        right = root.right

        self.flatten(root.left)
        self.flatten(right)

    def flatten2(self, root):
        if root is None:
            return

        left = root.left
        right = root.right

        self.flatten2(left)
        self.flatten2(right)


        root.right = left
        curr = root

        while curr.right is not None:
            curr = curr.right
        curr.right = right


    def printNodes(self, root):
        if root is None:
            return
        print(root.val, '-> ', end='', flush=True)
        self.printNodes(root.right)





sol = Solution()
sol.flatten(root)
sol.printNodes(root)
print("")


