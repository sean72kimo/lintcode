class UndirectedGraphNode(object):
    def __init__(self, x):
        self.label = x
        self.neighbors = []
    def __str__(self):
        return "<Node %s>" % (self.label)



def printQ (queue):
    print('Q:[', end='')
    for node in queue:
        print(node.label, end='')
    print(']')

class Solution(object):

    def myDFS(self, node):
        print("~ myDFS ~")
        stack = []
        visited = {}
        result = []

        stack.append(node)
        visited[node.label] = True
        result.append(node.label)


        while stack:
            current = stack[-1]
            visited[current.label] = True

            for nei in current.neighbors:
                if nei.label not in visited:
                    stack.append(nei)
                    result.append(nei.label)
                    break

                if nei == current.neighbors[-1]:
                    popNode = stack.pop()

        return result


    def myBFS(self, node):
        print("~ myBFS ~")
        queue = []
        visited = {}
        result = []

        queue.append(node)
        visited[node.label] = True
        result.append(node.label)

        while queue:
            current = queue.pop(0)

            for nei in current.neighbors:
                if nei.label not in visited:
                    queue.append(nei)
                    visited[nei.label] = True
                    result.append(nei.label)


        return result

A = UndirectedGraphNode('A')
B = UndirectedGraphNode('B')
C = UndirectedGraphNode('C')
D = UndirectedGraphNode('D')
E = UndirectedGraphNode('E')
F = UndirectedGraphNode('F')
G = UndirectedGraphNode('G')
H = UndirectedGraphNode('H')

A.neighbors = [B,D,G]
B.neighbors = [A,E,F]
C.neighbors = [F,H]
D.neighbors = [A,F]
E.neighbors = [B,G]
F.neighbors = [B,C,D]
G.neighbors = [A,E]
H.neighbors = [C]


sol = Solution()
res = sol.myBFS(A)
print(res)
# res = sol.myDFS(A)
# print(res)