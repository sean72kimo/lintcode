import collections
from typing import List


class Solution(object):
    def wallsAndGates(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: None Do not return anything, modify rooms in-place instead.
        此題仍需要用vst去避免重複計算！
        本以為可以利用rooms[i][j]是否為inf當作去除重複
        反例：
        rooms = [
            [2147483647,        -1,         0, -1],
            [2147483647,2147483647,2147483647, -1],
            [2147483647,        -1,        -1, -1],
            [         0,        -1,        -1, -1]]
        exp = [
            [3, -1, 0, -1],
            [2, 2, 1, -1],
            [1, -1, -1, -1],
            [0, -1, -1, -1]
        ]
        """
        if len(rooms) == 0:
            return

        MAX_INT = 2 ** 31 - 1
        m = len(rooms)
        n = len(rooms[0])
        que = []
        vst = set()
        for i in range(m):
            for j in range(n):
                if rooms[i][j] == 0:
                    que.append([i, j])
                    vst.add((i, j))

        step = 0
        dxy = [1, 0], [0, 1], [-1, 0], [0, -1],
        print(que)
        while que:
            new_q = []
            for x, y in que:
                rooms[x][y] = step

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if not (0 <= nx < m and 0 <= ny < n):
                        continue

                    if (nx, ny) in vst:
                        continue

                    if rooms[nx][ny] == MAX_INT:
                        print("at({},{}), add({},{})".format(x, y, nx, ny))
                        new_q.append([nx, ny])
                        vst.add((nx, ny))

            step += 1
            que = new_q


class Solution2:
    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        """
        Do not return anything, modify rooms in-place instead.
        """
        if len(rooms) == 0:
            return
        INF = 2147483647
        dxy = [0, 1], [-1, 0], [0, -1], [1, 0],
        vst = set()
        que = []
        n = len(rooms)
        m = len(rooms[0])
        step = 0

        for i in range(n):
            for j in range(m):
                if rooms[i][j] == 0:
                    que.append([i, j])
                    vst.add((i, j))

        while que:
            new_q = []

            for x, y in que:
                rooms[x][y] = step
                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if not (0 <= nx < n and 0 <= ny < m) or (nx, ny) in vst:
                        continue

                    if rooms[nx][ny] == INF:

                        new_q.append([nx, ny])
                        vst.add((nx, ny))

            que = new_q
            step += 1

rooms = [[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]
exp = [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]

rooms = [
    [2147483647,        -1,         0, -1],
    [2147483647,2147483647,2147483647, -1],
    [2147483647,        -1,        -1, -1],
    [         0,        -1,        -1, -1]]
exp = [
    [3, -1, 0, -1],
    [2, 2, 1, -1],
    [1, -1, -1, -1],
    [0, -1, -1, -1]
]
for r in rooms:
    print(r)

Solution2().wallsAndGates(rooms)
print("ans:", rooms == exp)
for r in rooms:
    print(r)


if rooms != exp:
    print("=== err ===")
    for r in exp:
        print(r)






























