import collections
class Solution(object):
    def possibleBipartition(self, N, dislikes):
        graph = collections.defaultdict(list)
        for u, v in dislikes:
            graph[u].append(v)
            graph[v].append(u)

        color = collections.defaultdict(int)
        que = collections.deque([])

        for i in range(1, N+1):
            if color[i] != 0:
                continue
            
            que.append(i)
            color[i] = 1

            while que:
                node = que.popleft()
                curr_color = color[node]
                
                for nei in graph[node]:
                    if color[nei] == 0:
                        color[nei] = -curr_color

                    else:
                        if nei in color: 
                            if color[nei] == color[node]:
                                return False
                            else:
                                continue

                    que.append(nei)
        print(color)
        return True

# 
# N = 4
# dislikes = [[1,2],[1,3],[2,3]]
# a = Solution().possibleBipartition(N, dislikes)
# print("test pass?:", a is False)
# 
# N = 4
# dislikes = [[1,2],[1,3],[2,4]]
# a = Solution().possibleBipartition(N, dislikes)
# print("test pass?:", a is True)
# 
# N = 5
# dislikes = [[1,2],[2,3],[3,4],[4,5],[1,5]]
# a = Solution().possibleBipartition(N, dislikes)
# print("test pass?:", a is False)


N = 5
dislikes = [[1,2],[3,4],[4,5],[3,5]]
a = Solution().possibleBipartition(N, dislikes)
print("test pass?:", a is False)

