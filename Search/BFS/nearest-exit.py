INF = 2147483647
class Solution:
    """
    @param: rooms: m x n 2D grid
    @return: nothing
    """

    def wallsAndGates(self, rooms):
        # write your code here
        if not any(rooms):
            return

        n = len(rooms)
        m = len(rooms[0])
        q = []

        for i in range(n):
            for j in range(m):
                if rooms[i][j] == 0:
                    q.append([i, j])

        dxy = [[0, 1], [0, -1], [1, 0], [-1, 0]]

        # bfs
        while q:
            cx, cy = q.pop(0)

            for i in range(len(dxy)):
                nx = cx + dxy[i][0]
                ny = cy + dxy[i][1]
                if 0 <= nx < n and 0 <= ny < m and rooms[nx][ny] == INF:
                    q.append([nx, ny])
                    rooms[nx][ny] = rooms[cx][cy] + 1
