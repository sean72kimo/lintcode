import copy

"""
要用deepcopy複製整個board, 不然每次交換之後board的順序會亂掉，
grid = board[:][:] 是shallow copy
"""
class Solution2(object):
    def slidingPuzzle(self, board):
        """
        :type board: List[List[int]]
        :rtype: int
        """
        m = len(board)
        n = len(board[0])
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        que = []
        vst = set()
        for i in range(m):
            for j in range(n):
                if board[i][j] == 0:
                    grid = copy.deepcopy(board)
                    s = self.toString(grid)
                    itm = (s, grid, i, j)
                    que.append(itm)
                    vst.add(s)
                    break

        step = 0
        while que:
            new_q = []
            for curr, grid, x, y in que:

                if curr == "123450":
                    return step

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy

                    if not (0 <= nx < m and 0 <= ny < n):
                        continue

                    nxt_grid = self.swap(grid, x, y, nx, ny)
                    s = self.toString(nxt_grid)

                    if s in vst:
                        continue

                    itm = (s, nxt_grid, nx, ny)
                    new_q.append(itm)
                    vst.add(s)


            que = new_q
            step += 1

        return -1

    def swap(self, grid, x, y, nx, ny):
        nxt = copy.deepcopy(grid)
        nxt[x][y], nxt[nx][ny] = nxt[nx][ny], nxt[x][y]
        return nxt

    def toString(self, board):
        s = ""
        for row in board:
            for col in row:
                s += str(col)
        return s

class Solution(object):
    def slidingPuzzle(self, board):
        """
        :type board: List[List[int]]
        :rtype: int
        """
        end = [[1,2,3],[4,5,0]]
        end = self.board2Str(end)
        start = self.board2Str(board)
        
        que = [start]
        vst = {start}
        step = 0
        while que:
            newQ = []
            for curr in que:
                if curr == end:
                    return step
                for nxt in self.getNext(curr):
                    if nxt in vst:
                        continue
                    newQ.append(nxt)
                    vst.add(nxt)
            que = newQ
            step += 1
        
        return -1
        
    def getNext(self, curr):
        
        arr = []
        dxy = [[0,1],[0,-1],[1,0],[-1,0]]
        idx = curr.find('0')

        x = idx // 3
        y = idx % 3

        for dx, dy in dxy:
            nx = x + dx
            ny = y + dy
            
            if not (0 <= nx < 2 and 0 <= ny < 3):
                continue
            
            j = nx * 3 + ny
            lst = list(curr)
            lst[idx], lst[j] = lst[j], lst[idx]
            arr.append(''.join(lst))
        
        return arr
        
    def board2Str(self, board):
        tmp = ""
        for i in range(len(board)):
            for j in range(len(board[0])):
                tmp = tmp + str(board[i][j])
        return tmp


sol = Solution2()
# board = [[1,2,3],[4,0,5]]
# print(sol.slidingPuzzle(board) == 1)

board = [[4,1,2],[5,0,3]]
print(sol.slidingPuzzle(board) == 5)

board =  [[3,2,4],[1,5,0]]
print(sol.slidingPuzzle(board) == 14)

board = [[1,2,3],[5,4,0]]
print(sol.slidingPuzzle(board) == -1)

