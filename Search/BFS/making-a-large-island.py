import collections


class Solution(object):
    def largestIsland(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0

        color = 1
        n = len(grid)
        mp = {0:0}
        ans = 0
        vst = set()
        for i in range(n):
            for j in range(n):
                if (i, j) in vst:
                    continue
                if grid[i][j] == 1:
                    color += 1
                    area = self.bfs(grid, i, j, vst, color)
                    mp[color] = area
                    ans = max(area, ans)

        for lst in grid:
            print(lst)

        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        for i in range(n):
            for j in range(n):
                if grid[i][j] != 0:
                    continue
                area = 1
                ans = max(ans, area)
                calculated = set()
                for dx, dy in dxy:
                    nx = i + dx
                    ny = j + dy

                    if not (0 <= nx < len(grid) and 0 <= ny < len(grid[0])):
                        continue

                    color_code = grid[nx][ny]

                    if color_code in calculated:
                        continue
                    calculated.add(color_code)
                    area += mp[color_code]
                    ans = max(area, ans)
        return ans

    def bfs(self, grid, i, j, vst, color):

        vst.add((i, j))
        grid[i][j] = color
        que = collections.deque([(i, j)])
        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        n = len(grid)
        m = len(grid[0])
        area = 1

        while que:
            x, y = que.popleft()
            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy

                if not (0 <= nx < n and 0 <= ny < m):
                    continue
                if (nx, ny) in vst:
                    continue
                if grid[nx][ny] != 1:
                    continue

                que.append((nx, ny))
                vst.add((nx, ny))
                grid[nx][ny] = color
                area += 1
        return area


grid = [[1,1],[1,0]]
grid = [[1,1],[1,1]]
grid = [
    [1,0,1,1,1,1],
    [1,0,1,0,0,0],
    [1,1,0,1,1,1],
    [1,0,1,0,0,0],
    [0,1,1,1,1,1]
]
grid = [[0]]
a = Solution().largestIsland(grid)
print("ans:", a)
