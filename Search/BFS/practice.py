import string
class Solution:
    """
    @param: start: a string
    @param: end: a string
    @param: dict: a set of string
    @return: An integer
    """
    def ladderLength(self, start, end, dict):
        # write your code here
        dict.add(start)
        dict.add(end)
        vst = set()
        que = [start]
        vst = {start}
        step = 0
        while que:
            new_q = []
            step += 1
            for word in que:
                lst = self.getNext(word, vst, dict)

                for nextWord in lst:
                    if nextWord in vst:
                        continue
                    if nextWord == end:
                        step += 1
                        return step
                    if nextWord in dict:
                        new_q.append(nextWord)
                        vst.add(nextWord)
            que = new_q
        return -1
            
    def getNext(self, word, vst, dict):
        lst = []
        for i in range(len(word)):
            for letter in string.ascii_lowercase:
                if letter == word[i]:
                    continue
                newWord = word[:i] + letter + word[i+1:]
                if newWord not in vst and newWord in dict:
                    lst.append(newWord)
        return lst
                
start = "hit"
end = "cog"
dict = {"hot","dot","dog","lot","log"}
# start = "hit"
# end = "hot"
# dict = {"hot"}
# start = "a"
# end = "a"
# dixt = {"b"}
a = Solution().ladderLength(start, end, dict)
print("ans:", a)