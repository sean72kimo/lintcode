import collections


class Solution(object):
    def floodFill(self, image, sr, sc, newColor):
        """
        :type image: List[List[int]]
        :type sr: int
        :type sc: int
        :type newColor: int
        :rtype: List[List[int]]

        一般來說，若是塗新顏色，可用顏色的不同避免重複訪問而死循環

        若是edge case: original color == new color,
        1. 可用vst去除重複訪問造成死循環，或者
        2. original color == new color 直接return image, 當作edge case, 不用塗色
        """
        if len(image) == 0:
            return

        original = image[sr][sc]
        if original == newColor:
            return image

        que = collections.deque([[sr, sc]])
        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]
        m = len(image)
        n = len(image[0])

        while que:
            i, j = que.popleft()

            image[i][j] = newColor

            for dx, dy in dxy:
                nx = i + dx
                ny = j + dy

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if image[nx][ny] == original:
                    que.append([nx, ny])

        return image