class Solution(object):
    def openLock(self, deadends, target):
        """
        :type deadends: List[str]
        :type target: str
        :rtype: int
        """
        if target in deadends or "0000" in deadends:
            return -1
        dead_ends = set(deadends)
        que = ["0000"]
        vst = {"0000"}
        step = 0
        while que:
            new_q = []
            for curr in que:

                if curr == target:
                    return step

                res = self.get_next(curr)
                for nxt in res:
                    if nxt in vst:
                        continue
                    if nxt in dead_ends:
                        continue
                    new_q.append(nxt)
                    vst.add(nxt)

            que = new_q
            step += 1
        return -1

    def get_next(self, s):
        res = []

        for i in range(len(s)):
            num = int(s[i])
            nxt_ch = (num + 1) % 10
            pre_ch = num - 1 if num > 0 else 9
            nxt = s[:i] + str(nxt_ch) + s[i + 1:]
            res.append(nxt)

            pre = s[:i] + str(pre_ch) + s[i + 1:]
            res.append(pre)

        return res



class Solution:
    """
    @param deadends: the list of deadends
    @param target: the value of the wheels that will unlock the lock
    @return: the minimum total number of turns 
    """
    def openLock(self, deadends, target):
        # Write your code here
        
        que = ['0000']
        vst = {'0000'}
        
        deadends = set(deadends)
        
        if '0000' in deadends or target in deadends:
            return -1
        
        step = 0
        while que:
            newQ = []

            for curr in que:
                if curr == target:
                    return step

                for i in range(4):
                    nxt = self.getNext(curr, i, vst, deadends)

                    for itm in nxt:
                        if itm in vst or itm in deadends:
                            continue
                        newQ.append(itm)
                        vst.add(itm)
                
            que = newQ
            step += 1
        
        return -1
    
    def getNext(self, string, i, vst, deadends):
        rt = []

        lst = list(string)

        a = int(lst[i]) + 1
        if a == 10:
            a = 0
            
        b = int(lst[i]) - 1
        if b == -1:
            b = 9

        slots = [str(a), str(b)]
        
        for s in slots:
            lst[i] = s
            cand = ''.join(lst)
            if cand in vst or cand in deadends:
                continue
            rt.append(cand)
        return rt
    
deadends = ["0201","0101","0102","1212","2002"]
target = "0202"
a = Solution().openLock(deadends, target)
print("ans:", a)

