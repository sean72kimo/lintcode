class Solution:
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if board is None or len(board) == 0:
            return

        n = len(board)
        m = len(board[0])

        def inbound(x, y):
            return 0 <= x < n and 0 <= y < m

        def bfs(i, j):
            if board[i][j] != 'O':
                return

            d = [[0, 1], [0, -1], [1, 0], [-1, 0]]
            q = [[i, j]]

            while q:
                x, y = q.pop(0)
                board[x][y] = 'W'
                for dx, dy in d:
                    nx = x + dx
                    ny = y + dy
                    if inbound(nx, ny) and board[nx][ny] == 'O':
                        q.append([nx, ny])

        # find 'O' from edge
        for i in range(n):
            bfs(i, 0)
            bfs(i, m - 1)


        # find 'O' from edge
        for j in range(m):
            bfs(0, j)
            bfs(n - 1, j)

        for i in range(n):
            for j in range(m):
                if board[i][j] == 'W':
                    board[i][j] = 'O'
                else:
                    board[i][j] = 'X'



import collections
class Solution2(object):
    def bfs(self, board, i, j):
        if board[i][j] != 'O':
            return
        vst = {(i, j)}
        m = len(board)
        n = len(board[0])

        que = collections.deque([(i, j)])
        dxy = [[0, 1], [1, 0], [-1, 0], [0, -1]]

        while que:
            x, y = que.popleft()
            print('1', (x, y), board[x][y], que)
            board[x][y] = 'W'
            print('1', (x, y), board[x][y])

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if board[nx][ny] == 'X' or board[nx][ny] == 'W':
                    continue

                if (nx, ny) in vst:
                    print((nx, ny), board[nx][ny])
                    input('pause')

                if board[nx][ny] == 'O':
                    que.append((nx, ny))
                    vst.add((nx, ny))

    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if not any(board):
            return

        m = len(board)
        n = len(board[0])

        for i in range(m):
            for j in range(n):
                if i == 0 or i == m - 1:
                    if board[i][j] == 'X' or board[i][j] == 'W':
                        continue
                    self.bfs(board, i, j)  # first and last row
                    continue

                if j == 0 or j == n - 1:
                    if board[i][j] == 'X' or board[i][j] == 'W':
                        continue
                    self.bfs(board, i, j)  # first and last col
                    continue


        for i in range(m):
            for j in range(n):
                if board[i][j] == 'W':
                    board[i][j] = 'O'
                elif board[i][j] == 'O':
                    board[i][j] = 'X'
board = [["X", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"], ["O", "X", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "X", "X"], ["O", "O", "O", "O", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "X"], ["O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "X", "O"], ["O", "O", "O", "O", "O", "X", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "X", "O", "O", "X"], ["X", "O", "O", "O", "X", "O", "O", "O", "O", "O", "X", "O", "X", "O", "X", "O", "X", "O", "X", "O"], ["O", "O", "O", "O", "X", "O", "O", "X", "O", "O", "O", "O", "O", "X", "O", "O", "X", "O", "O", "O"], ["X", "O", "O", "O", "X", "X", "X", "O", "X", "O", "O", "O", "O", "X", "X", "O", "X", "O", "O", "O"], ["O", "O", "O", "O", "O", "X", "X", "X", "X", "O", "O", "O", "O", "X", "O", "O", "X", "O", "O", "O"], ["X", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "O", "X", "X", "O", "O", "X", "O", "O", "X"], ["O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "X", "O", "O", "X", "O", "O", "O", "X", "O", "X"], ["O", "O", "O", "O", "X", "O", "X", "O", "O", "X", "X", "O", "O", "O", "O", "O", "X", "O", "O", "O"], ["X", "X", "O", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"], ["O", "X", "O", "X", "O", "O", "O", "X", "O", "X", "O", "O", "O", "X", "O", "X", "O", "X", "O", "O"], ["O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "X", "O", "O", "O", "O", "O", "X", "O", "X", "O"], ["X", "X", "O", "O", "O", "O", "O", "O", "O", "O", "X", "O", "X", "X", "O", "O", "O", "X", "O", "O"], ["O", "O", "X", "O", "O", "O", "O", "O", "O", "O", "X", "O", "O", "X", "O", "X", "O", "X", "O", "O"], ["O", "O", "O", "X", "O", "O", "O", "O", "O", "X", "X", "X", "O", "O", "X", "O", "O", "O", "X", "O"], ["O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"], ["X", "O", "O", "O", "O", "X", "O", "O", "O", "X", "X", "O", "O", "X", "O", "X", "O", "X", "O", "O"]]
# for r in board:
#     print(r)
# Solution().solve(board)
Solution2().solve(board)

