
import heapq


class Solution_heap(object):
    def swimInWater(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """

        m = len(grid)
        n = len(grid[0])
        que = [(grid[0][0], 0, 0)]
        vst = {(0, 0)}
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        ans = 0
        while que:
            h, x, y = heapq.heappop(que)
            ans = max(h, ans)
            if (x, y) == (m - 1, n - 1):
                return ans

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) in vst:
                    continue

                itm = (grid[nx][ny], nx, ny)
                heapq.heappush(que, itm)
                vst.add((nx, ny))


grid = [[0,23,2,3,4],[24,1,22,21,5],[12,13,14,15,16],[11,17,18,19,20],[10,9,8,7,6]]
exp = 23
# grid = [[0,2],[1,3]]
# exp = 3

grid = [[3,2],[0,1]]
exp = 3
grid = [[0,1,2,3,4],[24,23,22,21,5],[12,13,14,15,16],[11,17,18,19,20],[10,9,8,7,6]]
exp = 16
grid = [[3,2],[0,1]]
exp = 3
a = Solution_heap().swimInWater(grid)
print("ans:", a)
