import collections
import heapq

class Solution(object):
    def findCheapestPrice(self, n, flights, src, dst, K):
        """
        :type n: int
        :type flights: List[List[int]]
        :type src: int
        :type dst: int
        :type K: int
        :rtype: int
        """
        if src == dst:
            return 0
        graph = collections.defaultdict(dict)

        for u, v, w in flights:
            graph[u][v] = w
        print(graph)
        que = [(0, 0, src)]
        vst = {(0, src): 0}
        ans = float('inf')
        K = K+1
        while que:
            cost, dist, curr = heapq.heappop(que)

            if curr == dst:
                ans = min(ans, cost)

            for nei in graph[curr]:
                w = graph[curr][nei]
                d = dist + 1
                new_cost = cost + w
                itm = (new_cost, d, nei)
                print(itm)

                if d > K:
                    continue

                if (d, nei) in vst and new_cost >= vst[(d, nei)]:
                    continue

                heapq.heappush(que, itm)
                vst[(d, nei)] = new_cost

        if ans != float('inf'):
            return ans

        return -1




class Solution(object):
    def findCheapestPrice(self, n, flights, src, dst, K):
        graph = collections.defaultdict(dict)
        for u, v, w in flights:
            graph[u][v] = w
        best = {}
        pq = [(0, 0, src)]

        while pq:
            cost, k, place = heapq.heappop(pq)

            if k > K + 1 or cost > best.get((k, place), float('inf')):
                continue

            if place == dst:
                return cost

            for nei, wt in graph[place].iteritems():
                newcost = cost + wt
                if newcost < best.get((k + 1, nei), float('inf')):
                    heapq.heappush(pq, (newcost, k + 1, nei))
                    best[k + 1, nei] = newcost

        return -1


class Solution(object):
    def findCheapestPrice(self, n, flights, src, dst, K):
        """
        :type n: int
        :type flights: List[List[int]]
        :type src: int
        :type dst: int
        :type K: int
        :rtype: int
        """
        graph = collections.defaultdict(dict)
        for u, v, w in flights:
            graph[u][v] = w

        que = [[0, 0, src]]
        vst = {}
        K += 1
        while que:
            cost, step, curr = heapq.heappop(que)
            print(cost, curr, curr == dst)
            if step > K:
                continue

            if curr == dst:
                return cost

            if (cost, curr) in vst and step >= vst[(cost, curr)]:
                continue

            vst[(cost, curr)] = step

            for nei in graph[curr]:
                c = cost + graph[curr][nei]
                s = step + 1
                itm = (c, s, nei)
                heapq.heappush(que, itm)
        return -1

n, edges, src, dst, k = 3,[[0,1,100],[1,2,100],[0,2,500]], 0, 2, 1
exp = 200


n, edges, src, dst, k = 3, [[0,1,100],[1,2,100],[0,2,500]],0,2,0
exp = 500

# n, edges, src, dst, k = 3,[[0,1,2],[1,2,1],[2,0,10]],1,2,1
# exp = 1
sol = Solution()
a = sol.findCheapestPrice(n, edges, src, dst, k)
print("ans:", a)