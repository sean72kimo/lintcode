from typing import List
import collections


class Solution:
    def shortestPath(self, grid: List[List[int]], k: int) -> int:
        m = len(grid)
        n = len(grid[0])
        target = (m - 1, n - 1)

        # if we have sufficient quotas to eliminate the obstacles in the worst case,
        # then the shortest distance is the Manhattan distance
        if k >= m + n - 2:
            return m + n - 2

        # (row, col, remaining quota to eliminate obstacles)
        state = (0, 0, k)
        # (steps, state)
        queue = collections.deque([(0, state)])
        seen = {state}

        while queue:
            steps, (i, j, k) = queue.popleft()

            # we reach the target here
            if (i, j) == target:
                return steps

            # explore the four directions in the next step
            for nx, ny in [(i, j + 1), (i + 1, j), (i, j - 1), (i - 1, j)]:
                # if (new_row, new_col) is within the grid boundaries
                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                new_k = k - grid[nx][ny]
                new_state = (nx, ny, new_k)

                if new_k < 0:
                    continue
                if new_state in seen:
                    continue

                seen.add(new_state)
                queue.append((steps + 1, new_state))

        # did not reach the target
        return -1