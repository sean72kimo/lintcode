from sys import maxsize
import collections
class Solution(object):
    def cutOffTree(self, forest):
        """
        :type forest: List[List[int]]
        :rtype: int
        """
        ans = maxsize
        if not forest or len(forest) == 0:
            return -1

        def bfs(forest, sr, sc, tr, tc):
            R, C = len(forest), len(forest[0])
            # queue = collections.deque([(sr, sc, 0)])
            queue = [(sr, sc, 0)]
            seen = {(sr, sc)}
            while queue:

                # r, c, d = queue.popleft()
                r, c, d = queue.pop(0)
                if r == tr and c == tc:
                    return d
                for nr, nc in ((r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)):
                    if (0 <= nr < R and 0 <= nc < C and
                            (nr, nc) not in seen and forest[nr][nc]):
                        seen.add((nr, nc))
                        queue.append((nr, nc, d + 1))
            return -1


        trees = []
        for r, row in enumerate(forest):
            for c, v in enumerate(row):
                if v > 1:
                    tree = (v, r, c)
                    trees.append(tree)
        trees.sort()

#         trees = sorted((v, r, c) for r, row in enumerate(forest)
#                        for c, v in enumerate(row) if v > 1)

        sr = sc = ans = 0
        for _, tr, tc in trees:
            d = bfs(forest, sr, sc, tr, tc)

            if d < 0:
                return -1
            ans += d
            sr, sc = tr, tc
        return ans


forest = [
 [1, 2, 3],
 [0, 0, 4],
 [7, 6, 5]
]
a = Solution().cutOffTree(forest)
print("ans:", a)
