class Solution(object):
    def updateBoard(self, board, click):
        """
        :type board: List[List[str]]
        :type click: List[int]
        :rtype: List[List[str]]
        """
        x, y = click[0], click[1]

        m = len(board)
        n = len(board[0])
        dxy = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
        que = [click]
        vst = {tuple(click)}

        while que:
            new_q = []
            for x, y in que:

                if board[x][y] == 'M':
                    board[x][y] = 'X'
                    return board

                if board[x][y] == 'E':
                    mine = self.find_mind(board, x, y)

                    if mine:
                        board[x][y] = str(mine)
                        continue
                    else:
                        board[x][y] = 'B'

                    for dx, dy in dxy:

                        nx = x + dx
                        ny = y + dy

                        if not (0 <= nx < m and 0 <= ny < n):
                            continue

                        if (nx, ny) in vst:
                            continue

                        if board[nx][ny] == 'M':
                            continue

                        new_q.append((nx, ny))
                        vst.add((nx, ny))

            que = new_q

        return board

    def find_mind(self, board, x, y):
        m = len(board)
        n = len(board[0])
        dxy = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
        mine = 0
        for dx, dy in dxy:

            nx = x + dx
            ny = y + dy

            if not (0 <= nx < m and 0 <= ny < n):
                continue
            if board[nx][ny] == 'M':
                mine += 1
        return mine


board = [
 ['E', 'E', 'E', 'E', 'E'],
 ['E', 'E', 'M', 'E', 'E'],
 ['E', 'E', 'E', 'E', 'E'],
 ['E', 'E', 'E', 'E', 'E']
]
click = [3,0]
exp = [
 ['B', '1', 'E', '1', 'B'],
 ['B', '1', 'M', '1', 'B'],
 ['B', '1', '1', '1', 'B'],
 ['B', 'B', 'B', 'B', 'B']
]
sol = Solution()
a = sol.updateBoard(board, click)
print("ans:", a==exp)
for r in a:
    print(r)


class Solution(object):
    def updateBoard(self, board, click):
        oriX, oriY = click[1], click[0]
        if board[oriY][oriX] == 'M':
            board[oriY][oriX] = 'X'
            return board

        queue = [click]
        detected = set()
        width,height = len(board[0]),len(board)

        def next(y,x):
            return [(y-1,x),(y-1,x+1),(y,x+1),(y+1,x+1),(y+1,x),(y+1,x-1),(y,x-1),(y-1,x-1)]

        def count(surrounding):
            c = 0
            cnt = 0
            for p in surrounding:
                c += 1 if board[p[0]][p[1]] == 'M' else 0
            cnt += c
            return cnt

        def valid(point,w,h):
            y,x = point[0], point[1]
            return x > -1 and y > -1 and x < w and y < h

        while queue:
            nextLevel = []
            for y,x in queue:
                surrounding = [point for point in next(y,x) if valid(point,width,height)]

                num = count(surrounding)
                board[y][x] = str(num or 'B')
                detected.add((y,x))
                if num == 0:
                    for posi in surrounding:
                        if (posi[0],posi[1]) not in detected and posi not in nextLevel:
                            nextLevel.append(posi)
            queue = nextLevel

        return board
