class Solution(object):
    def orangesRotting(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int

        O(N) time
        corner case很多
        如果一開始計算發現fresh == 0, 直接就 return 0
        
        注意如何判斷結束？fresh ==0。何時判斷？進入while loop時判斷。
        但如果一開始計算發現fresh == 0 且 len(que) == 0, 不會進入while loop。
        或者，我們可以說，若是一開始fresh ==0, 就可以直接 return 0
        """
        if not any(grid):
            return 0

        m = len(grid)
        n = len(grid[0])

        fresh = 0
        que = []
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    fresh += 1
                elif grid[i][j] == 2:
                    que.append([i, j])

        if fresh == 0 and len(que) == 0:
            return 0

        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        step = 0
        while que:

            if fresh == 0:
                return step

            new_q = []
            for x, y in que:

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if not (0 <= nx < m and 0 <= ny < n):
                        continue
                    if grid[nx][ny] == 0 or grid[nx][ny] == 2:
                        continue
                    grid[nx][ny] = 2
                    fresh -= 1
                    new_q.append([nx, ny])

            step += 1
            que = new_q

        return -1

case0 = {
    'inp' : [[0]],
    'exp' : 0
}
case1 = {
    'inp' : [[1],[2],[2]],
    'exp' : 1
}
case2 = {
    'inp' : [[2,1,1],[0,1,1],[1,0,1]],
    'exp' : -1
}
case3 = {
    'inp' : [[2,1,1],[1,1,0],[0,1,1]],
    'exp' : 4
}
case4 = {
    'inp' : [[1]],
    'exp' : -1
}
tests = [
    case0,
    case1,
    case2,
    case3,
    case4
]
DEBUG = False
sol = Solution()

for i, case in enumerate(tests):

    a = sol.orangesRotting(case['inp'])
    if a != case['exp']:
        print("testing case#{} fail".format(i))
        DEBUG = True
        break

# a = sol.orangesRotting(tests[0]['inp'])
# print(a, tests[0]['exp'])


class Solution(object):
    def orangesRotting(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not any(grid):
            return 0

        m = len(grid)
        n = len(grid[0])

        fresh = 0
        que = []
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    fresh += 1
                elif grid[i][j] == 2:
                    que.append([i, j])

        if fresh == 0 and len(que) == 0:
            return 0

        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        step = 0
        while que:

            if fresh == 0:
                return step

            new_q = []
            for x, y in que:

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if not (0 <= nx < m and 0 <= ny < n):
                        continue
                    if grid[nx][ny] == 0 or grid[nx][ny] == 2:
                        continue
                    grid[nx][ny] = 2
                    fresh -= 1
                    new_q.append([nx, ny])

            step += 1
            que = new_q

        return -1