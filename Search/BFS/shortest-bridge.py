import collections
from typing import List

"""
1.  用bfs or dfs(stack)找出兩個島 source and target
2.  從 source出發，bfs碰到target 即為最短距離
2.1 bfs時候有點小技巧，利用((x,y),d)去計算距離
2.2 1裡面有包含所有曾經訪問過的點，扣掉target所有點，剩下的就是source所有點，這些點不需要再訪問
"""
class Solution(object):
    def shortestBridge(self, A):
        """
        :type A: List[List[int]]
        :rtype: int
        """

        island = set()
        source, target = self.get_island(A, island)

        ans = self.find_path(A, source, target, island)
        return ans

    def find_path(self, A, source, target, island):
        m, n = len(A), len(A[0])
        dest = set(target)

        vst = island - dest
        que = collections.deque([])
        for node in source:
            que.append((node, 0))

        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]

        while que:
            curr, d = que.popleft()

            if curr in dest:
                return d - 1
            for dx, dy in dxy:
                nx = curr[0] + dx
                ny = curr[1] + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue
                if (nx, ny) in vst:
                    continue

                nxt = ((nx, ny), d + 1)
                que.append(nxt)
                vst.add((nx, ny))

    def get_island(self, A, island):
        m, n = len(A), len(A[0])
        source = target = None
        for i in range(m):
            for j in range(n):
                if (i, j) in island or A[i][j] == 0:
                    continue
                area = self.mark_island(A, i, j, island)
                if source is None:
                    source = area
                elif target is None:
                    target = area

                if source and target:
                    return source, target

    def mark_island(self, A, i, j, island):
        stack = [(i, j)]
        island.add((i, j))
        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]
        m = len(A)
        n = len(A[0])
        area = []

        while stack:
            x, y = stack.pop()
            area.append((x, y))
            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue
                if (nx, ny) in island:
                    continue
                if A[nx][ny] == 0:
                    continue
                stack.append((nx, ny))
                island.add((nx, ny))
        return area


class Solution2:
    def shortestBridge(self, grid: List[List[int]]) -> int:
        island = set()  # vst
        source, target = self.get_island(grid, island)

        ans = self.find_path(grid, source, set(target), island)
        return ans

    def find_path(self, grid, source, target, island):

        m = len(grid)
        n = len(grid[0])
        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]
        vst = island - target

        step = 0
        while source:
            new_q = []
            for i, j in source:
                if (i, j) in target:
                    return step - 1

                for dx, dy in dxy:
                    nx = i + dx
                    ny = j + dy

                    if not (0 <= nx < m and 0 <= ny < n) or (nx, ny) in vst:
                        continue

                    if grid[nx][ny] == 0 or (nx, ny) in target:
                        new_q.append((nx, ny))
                        vst.add((nx, ny))
            print(new_q)
            source = new_q
            step += 1

    def get_island(self, grid, island):
        m = len(grid)
        n = len(grid[0])
        source = None
        target = None
        for i in range(m):
            for j in range(n):
                if (i, j) in island or grid[i][j] == 0:
                    continue

                area = self.mark_island(grid, i, j, island)
                if source is None:
                    source = area
                elif target is None:
                    target = area

                if source and target:
                    return source, target

    def mark_island(self, grid, i, j, island):
        stack = [(i, j)]
        stack = collections.deque(stack)
        island.add((i, j))
        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]
        m = len(grid)
        n = len(grid[0])
        area = []

        while stack:
            x, y = stack.pop()
            area.append((x, y))

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy

                if not (0 <= nx < m and 0 <= ny < n):
                    continue
                if (nx, ny) in island:
                    continue
                if grid[nx][ny]:
                    stack.append((nx, ny))
                    island.add((nx, ny))

        return area


A = [[0,1],[1,0]]
# A = [[0,1,0],[0,0,0],[0,0,1]]
# A = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
sol = Solution2()
a = sol.shortestBridge(A)
print("ans:", a)
