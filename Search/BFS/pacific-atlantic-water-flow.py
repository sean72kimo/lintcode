import collections


class Solution(object):
    def pacificAtlantic(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        if len(matrix) == 0:
            return []

        aQ = collections.deque([])
        pQ = collections.deque([])
        avst = set()
        pvst = set()

        m = len(matrix)
        n = len(matrix[0])

        for i in range(m):
            pQ.append((i, 0))
            pvst.add((i, 0))

            aQ.append((i, n - 1))
            avst.add((i, n - 1))

        for j in range(n):
            pQ.append((0, j))
            pvst.add((0, j))

            aQ.append((m - 1, j))
            avst.add((m - 1, j))

        self.bfs(matrix, pQ, pvst)
        self.bfs(matrix, aQ, avst)
        ans = []
        for i in range(m):
            for j in range(n):
                if (i, j) in pvst and (i, j) in avst:
                    ans.append([i, j])
        return ans

    def bfs(self, matrix, que, vst):
        dxy = [1, 0], [0, 1], [-1, 0], [0, -1],
        m = len(matrix)
        n = len(matrix[0])

        while que:
            x, y = que.popleft()

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) in vst:
                    continue

                if matrix[nx][ny] >= matrix[x][y]:
                    que.append((nx, ny))
                    vst.add((nx, ny))



class Solution(object):

    def pacificAtlantic(self, M):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        if not M or len(M) == 0:
            return []

        m = len(M)
        n = len(M[0])

        pas = [[False for _ in range(n)] for _ in range(m)]
        atl = [[False for _ in range(n)] for _ in range(m)]
        pQ = []
        aQ = []

        for j in range(n):
            pas[0][j] = True
            pQ.append((0, j))

            atl[m - 1][j] = True
            aQ.append((m - 1, j))


        for i in range(m):
            pas[i][0] = True
            pQ.append((i, 0))

            atl[i][n - 1] = True
            aQ.append((i, n - 1))

        print("----- pacific ------")
        print(pQ)
        self.bfs(M, pQ, pas)
        print("----- atlantic ------")
        print(aQ)
        self.bfs(M, aQ, atl)

        res = []
        for i in range(m):
            for j in range(n):
                if pas[i][j] and atl[i][j]:
                    res.append([i, j])

        return res

    def bfs(self, M, que, vst):
        dxy = [[1, 0], [-1, 0], [0, 1], [0, -1]]

        while que:
            i, j = que.pop(0)

            for dx, dy in dxy:
                ni = i + dx
                nj = j + dy
                if not self.inbound(M, ni, nj):
                    continue

                if vst[ni][nj]:
                    continue

                if M[ni][nj] >= M[i][j]:
                    vst[ni][nj] = True
                    que.append((ni, nj))

    def inbound(self, M, i, j):
        m = len(M)
        n = len(M[0])
        return 0 <= i < m and 0 <= j < n


class Solution(object):
    def pacificAtlantic(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        if len(matrix) == 0:
            return []

        m = len(matrix)
        n = len(matrix[0])

        pacific = [[False for _ in range(n)] for _ in range(m)]
        atlantic = [[False for _ in range(n)] for _ in range(m)]

        for i in range(m):
            for j in range(n):
                if i == 0 or j == 0:
                    self.bfs(matrix, pacific, i, j, set())
                if i == m - 1 or j == n - 1:
                    self.bfs(matrix, atlantic, i, j, set())

        ans = []
        for i in range(m):
            for j in range(n):
                if pacific[i][j] and atlantic[i][j]:
                    ans.append([i, j])
        return ans

    def bfs(self, matrix, mp, i, j, vst):
        if mp[i][j]:
            return
        m = len(matrix)
        n = len(matrix[0])
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]

        que = collections.deque([(i, j)])
        vst = {(i, j)}
        mp[i][j] = True

        while que:
            x, y = que.popleft()

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) in vst:
                    continue

                if mp[nx][ny]:
                    continue

                if matrix[nx][ny] >= matrix[x][y]:
                    mp[nx][ny] = True
                    que.append((nx, ny))
                    vst.add((nx, ny))


def main():
    matrix = [
         [1, 2, 2, 3, 5],
         [3, 2, 3, 4, 4],
         [2, 4, 5, 3, 1],
         [6, 7, 1, 4, 5],
         [5, 1, 1, 2, 4]
    ]
    # matrix = [[1]]

    a = Solution().pacificAtlantic(matrix)
    print("ans:", a)

if __name__ == "__main__":
    main()
