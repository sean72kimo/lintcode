from typing import List


class Solution:
    def numsSameConsecDiff(self, n: int, k: int) -> List[int]:

        que = [digit for digit in range(1, 10)]
        vst = set(que)
        ans = []
        print(que)
        print(vst)
        while que:
            new_q = []

            for num in que:
                if len(str(num)) == n:
                    ans.append(num)

                res = self.generate(n, k, num, vst)
                for itm in res:
                    new_q.append(itm)
            que = new_q
        return ans

    def generate(self, n, k, num, vst):
        ops = [1, -1]
        res = []
        for op in ops:
            d = num % 10 + k * op
            if not (0 <= d <= 9):
                continue
            new_num = num * 10 + d

            if new_num in vst:
                continue

            if len(str(new_num)) > n:
                continue
            res.append(new_num)
            vst.add(new_num)

        return res
n = 3
k = 7
a = Solution().numsSameConsecDiff(n,k)
print("ans:", a)
