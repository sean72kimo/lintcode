class Solution:
    """
    @param init_state: the initial state of chessboard
    @param final_state: the final state of chessboard
    @return: return an integer, denote the number of minimum moving
    """
    def toString(self, grid):
        string = ""
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                string += str(grid[i][j])
        return string
    
    def minMoveStep(self, init_state, final_state):
        # # write your code here

        source = self.toString(init_state)
        target = self.toString(final_state)


        que = [source]
        vst = {source}
        step = 0
        
        while que:
            newQ = []
            
            for string in que:
                # swap with nei
                if string == target:
                    return step
                for newS in self.getNext(string):
                    if newS in vst:
                        continue
                    newQ.append(newS)
                    vst.add(newS)
            que = newQ
            step += 1

    def getNext(self, source):
        
        nextS = []
        dxy = [[0,1],[0,-1],[1,0],[-1,0]]
        idx = source.find('0')
        x = idx // 3
        y = idx % 3
        
        for dx, dy in dxy:
            nx = x + dx
            ny = y + dy
            if not (0 <= nx < 3 and 0 <= ny < 3):
                continue
            j = nx * 3 + ny
            lst = list(source)
            lst[idx], lst[j] = lst[j], lst[idx]

            nextS.append(''.join(lst))
            
        return nextS



init = [
 [2,8,3],
 [1,0,4],
 [7,6,5]
]

final = [
 [1,2,3],
 [8,0,4],
 [7,6,5]
]
a = Solution().minMoveStep(init, final)
print("ans:",a)

