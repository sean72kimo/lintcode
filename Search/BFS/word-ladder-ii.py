import string
import collections
class Solution:
    """
    @param: start: a string
    @param: end: a string
    @param: dict: a set of string
    @return: a list of lists of string
    """
    def __init__(self):
        self.ans = []

    def findLadders(self, start, end, dict):
        dictionary = set(dict)
        dictionary.add(start)
        dictionary.add(end)
        dist_map = {}
        neighbor = collections.defaultdict(set)

        self.bfs(end, start, dictionary, dist_map, neighbor)

        path = [start]
        self.dfs(start, end, dist_map, neighbor, path)
        return self.ans

    def dfs(self, curr, end, dist_map, neighbor, path):
        if curr == end:
            self.ans.append(path[:])
            return

        for nxt in neighbor[curr]:
            if dist_map[curr] - 1 == dist_map[nxt]:
                path.append(nxt)
                self.dfs(nxt, end, dist_map, neighbor, path)
                path.pop()

    def bfs(self, start, end, dictionary, dist_map, neighbor):
        que = [start]

        step = 0

        while que:
            new_q = []

            for curr in que:
                if curr in dist_map:
                    continue

                dist_map[curr] = step
                next_list = self.getNext(curr, dictionary)

                for w in next_list:
                    new_q.append(w)
                    neighbor[curr].add(w)

            que = new_q
            step += 1


    def getNext(self, curr, dictionary):
        ret = []

        for i in range(len(curr)):
            for ch in string.ascii_lowercase:
                newS = curr[:i] + ch + curr[i + 1:]
                if newS not in dictionary:
                    continue
                ret.append(newS)

        return ret

start = "hit"
end = "cog"
dict = ["hot", "dot", "dog", "lot", "log"]
a = Solution().findLadders(start, end, dict)
print("ans:", a)

