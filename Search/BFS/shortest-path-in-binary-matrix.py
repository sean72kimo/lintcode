class Solution:
    def shortestPathBinaryMatrix(self, grid):
        if len(grid) == 0:
            return -1
        if grid[0][0] or grid[-1][-1]:
            return -1

        que = [(0, 0)]
        vst = {(0, 0)}
        dxy = [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1],
        m = len(grid)

        step = 1
        while que:

            new_q = []
            for x, y in que:
                if (x, y) == (m-1, m-1):
                    return step
                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy

                    if not (0 <= nx < m and 0 <= ny < m):
                        continue
                    if (nx, ny) in vst:
                        continue
                    if grid[nx][ny] == 1:
                        continue
                    new_q.append((nx, ny))
                    vst.add((nx, ny))

            que = new_q
            step += 1

        return -1

grid = [[0,1],[1,0]]
a = Solution().shortestPathBinaryMatrix(grid)
print("ans:", a)