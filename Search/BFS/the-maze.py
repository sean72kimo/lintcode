import collections
from typing import List


class Solution(object):
    def hasPath(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: bool
        """
        dxy = ((0,1), (0,-1), (1,0), (-1,0))
        que = collections.deque([(start[0], start[1])])
        vst = {(start[0], start[1])}
        
        while que:
            x, y = que.popleft()

            if x == destination[0] and y == destination[1]:
                return True
            
            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                while 0 <= nx < len(maze) and 0 <= ny < len(maze[0]) and maze[nx][ny] != 1:
                    nx += dx
                    ny += dy
                nx -= dx
                ny -= dy
                
                if maze[nx][ny] == 0 and (nx, ny) not in vst:
                    que.append((nx, ny))
                    vst.add((nx, ny))
                    
        return False


class Solution2:
    def hasPath(self, maze: List[List[int]], start: List[int], destination: List[int]) -> bool:
        que = collections.deque([start])
        vst = {(start[0], start[1])}
        m = len(maze)
        n = len(maze[0])
        dxy = ((0, 1), (0, -1), (1, 0), (-1, 0))
        while que:
            i, j = que.popleft()
            print((i,j))

            if [i, j] == destination:
                return True

            for dx, dy in dxy:

                nx = i + dx
                ny = j + dy

                while 0 <= nx < m and 0 <= ny < n and maze[nx][ny] == 0:
                    nx += dx
                    ny += dy

                nx -= dx
                ny -= dy

                if (nx, ny) not in vst:
                    que.append((nx, ny))
                    vst.add((nx, ny))
        return False

M = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
start = [0,4]
destination = [3,2]

M = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
start = [0,4]
destination = [4,4]

for r in M:
    print(r)
a = Solution2().hasPath(M, start, destination)
print("ans:", a)