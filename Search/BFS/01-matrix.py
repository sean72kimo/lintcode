class Solution(object):
    def updateMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        if len(matrix) == 0:
            return

        m = len(matrix)
        n = len(matrix[0])
        one = []
        for i in range(m):
            for j in range(n):
                if matrix[i][j] == 1:
                    one.append([i, j])

        for x, y in one:
            step = self.bfs(x, y, matrix)
            matrix[x][y] = step

        return matrix

    def bfs(self, x, y, matrix):
        m = len(matrix)
        n = len(matrix[0])
        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        que = [(x, y)]
        vst = {(x, y)}
        step = 0

        while que:
            new_q = []
            for x, y in que:
                if matrix[x][y] == 0:
                    return step

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if (nx, ny) in vst:
                        continue
                    if not (0 <= nx < m and 0 <= ny < n):
                        continue
                    new_q.append([nx, ny])
                    vst.add((nx, ny))

            step += 1
            que = new_q
        return None