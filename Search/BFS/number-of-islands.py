class Solution(object):
    def isInBound(self, i, j, grid):
        n = len(grid)
        m = len(grid[0])
        return 0 <= i < n and 0 <= j < m

    def newLand(self, i, j, dxy, visited):
        for i in range(4):
            ni = i + dxy[i][0]
            nj = j + dxy[i][1]

            if visited[ni][nj] and grid[ni][nj] == "1":
                return False

        return True

    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """

        if not any(grid):
            return 0
        count = 0
        n = len(grid)
        m = len(grid[0])
        dxy = [[1, 0], [-1, 0], [0, 1], [0, -1]]
        queue = [[0, 0]]

        visited = [[False for i in range(m)]for j in range(n)]

        new_q = []

        while queue:
            i, j = queue.pop(0)

            if not visited[i][j] and self.isInBound(i, j, grid) and grid[i][j] == "1":
                if self.newLand(i, j, dxy, visited):
                    count += 1
                visited[i][j] == True

                # add adjacent
                for i in range(4):
                    ni = i + dxy[i][0]
                    nj = j + dxy[i][1]
                    new_q.append([ni, nj])
            else:
                continue

            queue = new_q

        print("count", count)
        return count
