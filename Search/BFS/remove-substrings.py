class Solution:
    """
    @param: s: a string
    @param: dict: a set of n substrings
    @return: the minimum length
    """
    def minLength(self, s, dic):
        # write your code here
        hashset = set()
        hashset.add(s)

        que = []
        que.append(s)
        ans = len(s)
        ss = s


        while que:
            string = que.pop(0)

            for sub in dic:
                i = string.find(sub)

                while i != -1:
                    new_s = string[:i] + string[i + len(sub):]

                    if new_s not in hashset:
                        if len(new_s) < ans:
                            ans = len(new_s)
                            ss = new_s
                        hashset.add(new_s)
                        que.append(new_s)

                    i = string.find(sub, i + len(sub))



        return ans, ss






s = 'ccdaabcdbb'
dict = ["ab", "cd"]
s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
dict = ["ab", "b"]

a = Solution().minLength(s, dict)
print("ans:", a)
