import collections
from typing import List
"""
similar to 1020. Number of Enclaves
用bfs將靠外圍的陸地改成海。剩下的陸地就是沒靠牆，而且全部被水包圍，再次用bfs計算有多少陸地。
"""

class Solution:
    def closedIsland(self, grid: List[List[int]]) -> int:
        if len(grid) == 0:
            return 0

        n = len(grid)
        m = len(grid[0])
        vst = set()

        for i in range(n):
            for j in [0, m - 1]:
                if grid[i][j] == 0:
                    self.mark_wall(i, j, grid)

        for j in range(m):
            for i in [0, n - 1]:
                if grid[i][j] == 0:
                    self.mark_wall(i, j, grid)

        ans = 0
        for i in range(n):
            for j in range(m):
                if (i, j) in vst:
                    continue

                if grid[i][j] == 0:
                    self.bfs(i, j, grid, vst)
                    ans += 1
        return ans

    def mark_wall(self, i, j, grid):
        n = len(grid)
        m = len(grid[0])

        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        que = collections.deque([(i, j)])

        while que:
            i, j = que.popleft()
            grid[i][j] = 1

            for dx, dy in dxy:
                nx = i + dx
                ny = j + dy
                if not (0 <= nx < n and 0 <= ny < m):
                    continue
                if grid[nx][ny] == 0:
                    que.append((nx, ny))

    def bfs(self, i, j, grid, vst):
        n = len(grid)
        m = len(grid[0])

        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        que = collections.deque([(i, j)])
        vst.add((i, j))

        while que:
            i, j = que.popleft()

            for dx, dy in dxy:
                nx = i + dx
                ny = j + dy
                if not (0 <= nx < n and 0 <= ny < m):
                    continue
                if (nx, ny) in vst:
                    continue
                if grid[nx][ny] == 0:
                    que.append((nx, ny))
                    vst.add((nx, ny))


grid = [[0,0,1,1,0,1,0,0,1,0],
        [1,1,0,1,1,0,1,1,1,0],
        [1,0,1,1,1,0,0,1,1,0],
        [0,1,1,0,0,0,0,1,0,1],
        [0,0,0,0,0,0,1,1,1,0],
        [0,1,0,1,0,1,0,1,1,1],
        [1,0,1,0,1,1,0,0,0,1],
        [1,1,1,1,1,1,0,0,0,0],
        [1,1,1,0,0,1,0,1,0,1],
        [1,1,1,0,1,1,0,1,1,0]
        ]
exp = 5

grid = [[1,1,1,1,1,1,1,0],[1,0,0,0,0,1,1,0],[1,0,1,0,1,1,1,0],[1,0,0,0,0,1,0,1],[1,1,1,1,1,1,1,0]]
exp = 2
# for row in grid:
#     print(row)

sol = Solution()
a = sol.closedIsland(grid)
print("ans:", a)

