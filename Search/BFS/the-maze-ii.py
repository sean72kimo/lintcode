import collections


class Cell:

    def __init__(self, x, y, l):
        self.x = x
        self.y = y
        self.l = l

    def __repr__(self):
        return "<Cell>({},{}({}))".format(self.x, self.y, self.l)


class Solution:
    """
    @param maze: the maze
    @param start: the start
    @param destination: the destination
    @return: the shortest distance for the ball to stop at the destination
    """

    def shortestDistance(self, M, start, destination):
        # write your code here
        if M is None or len(M) == 0:
            return False

        m = len(M)
        n = len(M[0])

        if start == destination:
            return 0

        que = collections.deque([])
        res = [[float('inf') for _ in range(n)] for _ in range(m)]
        dirs = [[0, 1], [1, 0], [-1, 0], [0, -1]]
        p = Cell(start[0], start[1], 0)
        que.append(p)

        while que:
            print(que)
            curr = que.popleft()

            if  curr.l >= res[curr.x][curr.y]:
                continue

            res[curr.x][curr.y] = curr.l

            for dx, dy in dirs:
                x, y, l = curr.x, curr.y, curr.l
                nx = x + dx
                ny = y + dy
                l += 1

                while 0 <= nx < m and 0 <= ny < n and M[nx][ny] != 1:
                    nx += dx
                    ny += dy
                    l += 1

                nx -= dx
                ny -= dy
                l -= 1

                que.append(Cell(nx, ny, l))
        for r in res:
            print(r)
        if res[destination[0]][destination[1]] == float('inf'):
            return -1

        return res[destination[0]][destination[1]]


import heapq


class Solution2:

    def shortestDistance(self, maze, start, dest):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int
        """
        m = len(maze)
        n = len(maze[0])
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        itm = (0, start[0], start[1])
        pq = [itm]
        res = [[float('inf') for _ in range(n)] for _ in range(m)]

        while pq:
            curr_l, x, y = heapq.heappop(pq)

            if curr_l >= res[x][y]:
                continue

            res[x][y] = curr_l

            for dx, dy in dxy:
                l = curr_l
                nx = x + dx
                ny = y + dy
                l += 1

                while 0 <= nx < m and 0 <= ny < n and maze[nx][ny] != 1:
                    nx += dx
                    ny += dy
                    l += 1

                nx -= dx
                ny -= dy
                l -= 1

                nxt = (l, nx, ny)
                heapq.heappush(pq, nxt)

        for r in res:
            print(r)

        if res[dest[0]][dest[1]] == float('inf'):
            return -1
        return res[dest[0]][dest[1]]


class Solution(object):
    def shortestDistance(self, maze, start, destination):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int
        """

        dxy = [[1, 0], [0, 1], [0, -1], [-1, 0]]
        que = collections.deque([])
        que.append((start[0], start[1], 0))
        m = len(maze)
        n = len(maze[0])
        mp = [[float('inf') for _ in range(n)] for _ in range(m)]

        while que:
            x, y, d = que.popleft()
            print(x, y, d)

            if mp[x][y] != float('inf') and d >= mp[x][y]:
                continue

            mp[x][y] = d

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                nd = d + 1
                while 0 <= nx < m and 0 <= ny < n and maze[nx][ny] == 0:
                    nx += dx
                    ny += dy
                    nd += 1
                nx -= dx
                ny -= dy
                nd -= 1

                itm = (nx, ny, nd)
                que.append(itm)

        i, j = destination[0], destination[1]
        for r in mp:
            row = []
            for n in r:
                if n != float('inf'):
                    s = "{:03d}".format(int(n))
                    row.append(s)
                else:
                    row.append('inf')
            print(row)


        if mp[i][j] == float('inf'):
            return -1

        return mp[i][j]


M = [[0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 1, 0], [1, 1, 0, 1, 1], [0, 0, 0, 0, 0]]
start = [0, 4]
destination = [3, 2]

# M = [[0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 1, 0], [1, 1, 0, 1, 1], [0, 0, 0, 0, 0]]
# start = [0, 4]
# destination = [4, 4]

print("---------")
a = Solution().shortestDistance(M, start, destination)
print("ans:", a)
