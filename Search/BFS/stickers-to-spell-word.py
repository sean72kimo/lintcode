import collections
from typing import List

"""
bfs就是状态数+边数
那状态数这个题就是O2^n，n是原字符串长度，因为他算的就是从原来的字符串里删掉对应的字符
那边数，简单的算就是每个点都可以通过删每一种单词得到新的点
那估算就是2^n *m
"""
class Solution:
    def minStickers(self, stickers: List[str], target: str) -> int:
        availables = []
        for sticker in stickers:
            counter = collections.Counter()
            for ch in sticker:
                if ch in target:
                    counter[ch] += 1
            availables.append(counter)

        queue = collections.deque([(target, 0)])
        vst = {target}
        while queue:
            cur, step = queue.popleft()
            if not cur:
                return step
            for avl in availables:
                if cur[0] in avl:
                    nxt = cur
                    for k, v in avl.items():
                        nxt = nxt.replace(k, '', v)
                    if nxt not in vst:
                        vst.add(nxt)
                        queue.append((nxt, step + 1))
        return -1
stickers = ["with","example","science"]
target = "thehat"
exp = 3
a = Solution().minStickers(stickers, target)
print("ans:", a)