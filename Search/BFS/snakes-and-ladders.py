"""
nxt = curr + i
注意，要先判斷這個nxt是否可以爬樓梯(或者蛇)，才能更新為真正的nxt值
更新正確的nxt之後，才能判斷這個nxt是否拜訪過。
若沒有拜訪過，那麼加入vst and new_q
"""

import heapq
from typing import List


class SolutionBFS:
    def snakesAndLadders(self, board: List[List[int]]) -> int:
        if len(board) == 0:
            return 0
        n = len(board)
        nn = n * n
        arr = [0]
        for i, row in enumerate(board[::-1]):
            if i % 2 == 0:
                arr += row
            else:
                arr += row[::-1]

        que = [1]
        vst = {1}
        moves = 0

        while que:
            new_q = []

            for curr in que:

                if curr == nn:
                    return moves

                for i in range(1, 7):
                    nxt = curr + i

                    if not (1 <= nxt <= nn):
                        continue

                    if arr[nxt] != -1:
                        nxt = arr[nxt]

                    if nxt in vst:
                        continue

                    vst.add(nxt)
                    new_q.append(nxt)

            que = new_q
            moves += 1

        return -1

class SolutionDijkstra(object):

    def snakesAndLadders(self, board):
        """
        :type board: List[List[int]]
        :rtype: int
        """
        if len(board) == 0:
            return 0
        n = len(board)
        nn = n * n
        arr = [0]
        for i, row in enumerate(board[::-1]):
            arr += row[::-1] if i % 2 else row

        que = [(0,1)]
        vst = {}

        while que:
            dist, curr = heapq.heappop(que)

            if curr in vst and dist >= vst[curr]:
                continue

            if curr == nn:
                return dist

            vst[curr] = dist

            for i in range(1, 7):
                nx = curr + i

                if not (1 <= nx <= nn):
                    continue

                if arr[nx] == -1:
                    heapq.heappush(que, (dist + 1, nx))

                else:
                    ladder = arr[nx]
                    heapq.heappush(que, (dist + 1, ladder))

        return -1

board = [[-1]]
exp = 0
board = [[-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1],[-1,35,-1,-1,13,-1],[-1,-1,-1,-1,-1,-1],[-1,15,-1,-1,-1,-1]]
exp = 4
board = [[-1,7,-1],[-1,6,9],[-1,-1,2]]
exp = 1
board = [[1,1,-1],[1,1,1],[-1,1,1]]
exp = -1
# board = [[-1,-1,-1,46,47,-1,-1,-1],[51,-1,-1,63,-1,31,21,-1],[-1,-1,26,-1,-1,38,-1,-1],[-1,-1,11,-1,14,23,56,57],[11,-1,-1,-1,49,36,-1,48],[-1,-1,-1,33,56,-1,57,21],[-1,-1,-1,-1,-1,-1,2,-1],[-1,-1,-1,8,3,-1,6,56]]
# exp = 4
sol = Solution_bfs()
a = sol.snakesAndLadders(board)
print("ans:", a == exp, a)

