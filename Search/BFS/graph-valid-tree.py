import collections
from typing import List

"""
Solution0 
單純地用遍歷圖的方式，看看是否重複拜訪而造成環
1, 將該點的nei全部放入que進行下一輪的bfs(popleft) or dfs(pop), 如果nei點已經被訪問過，無需再放入
2, 該點完事, 那麼將該點放入vst
3, pop得到下一個要處理的點，如果該點已經在vst中，代表該點曾經被兩個不同的prev點當作nei放入，推得有環
4, while結束，如果沒有環，還要判斷所有vst點的個數是否等於 n (樹上每一個點都會被遍歷過)

Solution1
利用樹的性質。如果共有n個點，一棵樹則有n-1條邊
1. bfs or dfs遍歷整棵樹。vst點個數必須等於n

"""
class Solution0:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:

        graph = collections.defaultdict(list)

        for s, e in edges:
            graph[s].append(e)
            graph[e].append(s)

        que = collections.deque([])
        que.append(0)
        vst = set()

        while que:
            curr = que.pop()

            if curr in vst:
                return False

            for nei in graph[curr]:
                if nei in vst:
                    continue
                que.append(nei)

            vst.add(curr)

        return len(vst) == n

class Solution1:
    def validTree(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        if len(edges) != n - 1:
            return False

        graph = collections.defaultdict(set)

        for s, e in edges:
            graph[s].add(e)
            graph[e].add(s)

        vst = {0}
        que = collections.deque([0])

        while que:
            curr = que.popleft()
            for nei in graph[curr]:
                if nei in vst:
                    continue
                vst.add(nei)
                que.append(nei)

        return len(vst) == n

    def validTree_dfs(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        if n - 1 != len(edges):
            return False
        if len(edges) == 0:
            return True

        graph = collections.defaultdict(list)

        for s, e in edges:
            graph[s].append(e)
            graph[e].append(s)

        que = collections.deque([0])
        vst = {0}
        self.dfs(graph, 0, vst)
        return len(vst) == n

    def dfs(self, graph, start, vst):
        # print(start, graph[start])
        for nei in graph[start]:
            if nei in vst:
                continue
            vst.add(nei)
            self.dfs(graph, nei, vst)
