"""
bfs or dfs 搜索皆可
用deque, deque.popleft()就是bfs
用stack, stack.pop()就是dfs
"""
class Solution(object):
    def canVisitAllRooms(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: bool
        """
        que = collections.deque([0])
        vst = set()

        while que:
            k = que.popleft()
            if k in vst:
                continue

            vst.add(k)
            if len(vst) == len(rooms):
                return True

            keys = rooms[k]
            que.extend(keys)

        return False


class Solution:
    def canVisitAllRooms(self, rooms: List[List[int]]) -> bool:
        if len(rooms) == 0:
            return True

        que = collections.deque([rooms[0]])
        vst = {0}

        while que:
            keys = que.popleft()

            for k in keys:
                if k in vst:
                    continue
                que.append(rooms[k])
                vst.add(k)

        return len(vst) == len(rooms)