import collections


class Solution2(object):

    def bfs(self, x, y, grid):
        dxy = [[1, 0], [0, 1], [-1, 0], [0, -1]]
        que = [(x, y)]
        vst = {(x, y)}
        dist = 0

        while que:
            new_q = []
            dist += 1
            for x, y in que:
                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy
                    if (0 <= nx < self.m and 0 <= ny < self.n) and (nx, ny) not in vst and grid[nx][ny] == 0:

                        self.visited_time[nx][ny] += 1
                        self.distance_sum[nx][ny] += dist

                        vst.add((nx, ny))
                        new_q.append((nx, ny))
            que = new_q

    def shortestDistance(self, grid):
        self.m = len(grid)
        self.n = len(grid[0])

        self.visited_time = [[0] * self.n for _ in range(self.m)]
        self.distance_sum = [[0] * self.n for _ in range(self.m)]

        house_count = 0
        for x in range(self.m):
            for y in range(self.n):
                if grid[x][y] == 1:
                    house_count += 1
                    self.bfs(x, y, grid)

        min_distance = float('inf')
        for x in range(self.m):
            for y in range(self.n):
                if grid[x][y] == 0 and self.visited_time[x][y] == house_count and self.distance_sum[x][y] < min_distance:
                    min_distance = self.distance_sum[x][y]

        if min_distance == float('inf'):
            return -1
        return min_distance


class Solution(object):
    def shortestDistance(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        dis_map = [[0 for _ in range(n)] for _ in range(m)]
        cnt_map = [[0 for _ in range(n)] for _ in range(m)]
        house = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    house += 1
                    self.bfs(grid, i, j, dis_map, cnt_map)

        dis = float('inf')
        for i in range(m):
            for j in range(n):
                if cnt_map[i][j] == house and dis_map[i][j] < dis:
                    dis = dis_map[i][j]


        if dis == float('inf'):
            return -1
        return dis

    def bfs(self, grid, i, j, dis_map, cnt_map):
        m = len(grid)
        n = len(grid[0])
        dxy = [[0, 1], [1, 0], [-1, 0], [0, -1]]
        que = [[i, j]]
        vst = {(i, j)}
        step = 0
        while que:
            new_q = []

            for x, y in que:
                dis_map[x][y] += step
                cnt_map[x][y] += 1

                for dx, dy in dxy:
                    nx = x + dx
                    ny = y + dy

                    if not (0 <= nx < m and 0 <= ny < n):
                        continue
                    if (nx, ny) in vst:
                        continue
                    if grid[nx][ny] == 0:
                        new_q.append([nx, ny])
                        vst.add((nx, ny))
            que = new_q
            step += 1
grid = [[1, 0, 2, 0, 1], [0, 0, 0, 0, 0], [0, 0, 1, 0, 0]]
grid = [[1]]
a = Solution().shortestDistance(grid)
print("ans:", a)
