import collections
import heapq
"""
time 2^n * n
space 2^n * n
如何記錄狀態？
"""

class Solution(object):
    def shortestPathLength(self, graph):
        N = len(graph)
        goal = (1 << N) - 1
        queue = []
        vst = set()
        for x in range(N):
            itm = (0, 1 << x, x)
            vst.add(itm)
            queue.append(itm)

        heapq.heapify(queue)

        while queue:
            step, cover, head = heapq.heappop(queue)

            if cover == goal:
                return step

            for child in graph[head]:
                cover2 = cover | (1 << child)
                itm = (step + 1, cover2, child)
                if itm in vst:
                    continue
                heapq.heappush(queue, itm)
                vst.add(itm)


class Solution(object):
    def shortestPathLength(self, graph):
        N = len(graph)
        goal = (1 << N) - 1
        queue = collections.deque([])

        for x in range(N):
            itm = (1 << x, x)
            queue.append(itm)

        dist = collections.defaultdict(lambda: N * N)
        for x in range(N):
            dist[1 << x, x] = 0

        while queue:
            cover, head = queue.popleft()
            d = dist[cover, head]
            if cover == goal:
                return d

            for child in graph[head]:
                cover2 = cover | (1 << child)
                if d + 1 < dist[cover2, child]:
                    dist[cover2, child] = d + 1
                    queue.append((cover2, child))

graph = [[1,2,3],[0],[0],[0]]
a = Solution().shortestPathLength(graph)
print("ans:", a)