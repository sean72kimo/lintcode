"""
# Employee info
class Employee(object):
    def __init__(self, id, importance, subordinates):
        # It's the unique id of each node.
        # unique id of this employee
        self.id = id
        # the importance value of this employee
        self.importance = importance
        # the id of direct subordinates
        self.subordinates = subordinates
"""


class Solution(object):
    def getImportance(self, employees, idx):
        """
        :type employees: Employee
        :type id: int
        :rtype: int
        """
        mp = {}
        for e in employees:
            mp[e.id] = e

        e = mp[idx]
        deque = collections.deque([e])

        ans = 0
        while deque:
            employee = deque.popleft()
            ans += employee.importance

            for sub_id in employee.subordinates:
                deque.append(mp[sub_id])
        return ans


