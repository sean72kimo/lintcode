import collections


class Solution(object):
    def killProcess(self, pid, ppid, kill):
        """
        :type pid: List[int]
        :type ppid: List[int]
        :type kill: int
        :rtype: List[int]
        """
        mp = collections.defaultdict(set)
        for i in range(len(pid)):
            mp[ppid[i]].add(pid[i])

        print(mp)
        que = [kill]
        ans = [kill]
        while que:
            new_q = []
            for curr in que:
                for child in mp[curr]:
                    ans.append(child)
                    new_q.append(child)

            que = new_q

        return ans

pid = [1, 3, 10, 5,11,101]
ppid = [3, 0, 5, 3,5,11]
kill = 5
a = Solution().killProcess(pid, ppid, kill)
print("ans:", a)
