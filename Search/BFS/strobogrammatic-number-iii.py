import collections

"""
此題判斷是否加入que, 要從字串長度，而非數字大小
例如 "00" 不能被忽略，因為他會產生 1001, 8008
例如 (low, high) = ("9695", "9697) ，產生 "69"時，不能因為 69 < low 而跳過，因為 "69" -> "9696"
"""
class Solution(object):

    def strobogrammaticInRange(self, low, high):
        """
        :type low: str
        :type high: str
        :rtype: int
        """
        l = len(low)
        h = len(high)

        length = set(range(l, h + 1))
        mn = l
        mx = h

        rotate = {
            "0" : "0",
            "1" : "1",
            "6" : "9",
            "8" : "8",
            "9" : "6"
        }

        que = collections.deque(["", "0", "1", "8"])
        tmp = []
        ans = 0
        while que:
            curr = que.popleft()
            if len(curr) > h:
                break

            if len(curr) in length:
                #            判斷第一位數是否為零也可以寫成 and str(int(x)) == x:
                if int(low) <= int(curr) <= int(high) and not (curr[0] == "0" and len(curr) != 1):
                    ans += 1
                    tmp.append(curr)

            for k, v in rotate.items():
                nxt = k + curr + v
                que.append(nxt)
        return ans, tmp


low, high = "50", "100"  # 3
low, high = "0", "0"  # 1
low, high = "0", "1680"  # 21
# low, high = "11", "69"  # 2
low, high = "9695", "9697"  # 1
b, tmp1 = Solution().strobogrammaticInRange(low, high)
print(sorted(tmp1))

print("ans:", a)
