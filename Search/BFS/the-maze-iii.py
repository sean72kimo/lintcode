import collections 
class Cell:
    def __init__(self, x, y, l, p):
        self.x = x
        self.y = y
        self.l = l
        self.p = p

from sys import maxsize
class Solution:
    """
    @param maze: the maze
    @param ball: the ball position
    @param hole: the hole position
    @return: the lexicographically smallest way
    """
    def xGreaterEqualY(self, x, y):
        if x.l != y.l:
            return x.l >= y.l
        else:
            return x.p >= y.p

    def findShortestWay(self, M, B, H):
        # write your code here
        if M is None or len(M) == 0:
            return False
            
        m = len(M)
        n = len(M[0])
        
        if B == H:
            return ""
            
        que = collections.deque([])
        res = [[None for _ in range(n)] for _ in range(m)]
        
        for i in range(m):
            for j in range(n):
                res[i][j] = Cell(i, j, maxsize, "")
        
        dirs = [[0,1],[1,0],[-1,0],[0,-1]]
        dirStr = ["r", "d", "u", "l"]
        p = Cell(B[0], B[1], 0, "")
        que.append(p)
        
        while que:
            curr = que.popleft() #Cell
            tmp = res[curr.x][curr.y]
            print(curr.x, curr.y, curr.l, curr.p)

            if self.xGreaterEqualY(curr, tmp):
                continue

            res[curr.x][curr.y] = curr
            
            if curr.x == H[0] and curr.y == H[1]:
                continue

            for i, (dx, dy) in enumerate(dirs):
                x, y, l, p = curr.x, curr.y, curr.l, curr.p
                print("directions", (i,dirStr[i]), dx, dy)
                while 0 <= x < m and 0 <= y < n and M[x][y] == 0 and (x != H[0] or y != H[1]):
                    x += dx
                    y += dy
                    l += 1

                if x != H[0] or y != H[1]:
                    x -= dx
                    y -= dy
                    l -= 1
                print("adding Cell", (x, y, l, curr.p + dirStr[i]))
                que.append(Cell(x, y, l, curr.p + dirStr[i]))
                
        if res[H[0]][H[1]].l == maxsize:
            return "impossible"
        
        return res[H[0]][H[1]].p
        
M = [[0,0,0,0,0],[1,1,0,0,1],[0,0,0,0,0],[0,1,0,0,1],[0,1,0,0,0]]
B = [4,3]
H = [0,1]

xx = Cell(0,0,5,"a")
yy = Cell(0,0,5,"b")
# print(Solution().xGreaterEqualY(xx, yy))

a = Solution().findShortestWay(M, B, H)
print("ans:", a)
