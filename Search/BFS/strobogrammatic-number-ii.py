import collections


class Solution(object):

    def findStrobogrammatic(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        if n == 0:
            return []
        rotate = {
            "0" : "0",
            "1" : "1",
            "6" : "9",
            "8" : "8",
            "9" : "6"
        }

        que = collections.deque([])
        if n % 2 == 0:
            que.append("")
        else:
            que.append("0")
            que.append("1")
            que.append("8")

        ans = []
        while que:
            num = que.popleft()
            if len(num) == n:
                ans.append(num)
#                 if num[0] != "0" :
#                     ans.append(num)

            else:
                for k, v in rotate.items():
                    que.append(k + num + v)

        return ans


class Solution2(object):
    def findStrobogrammatic(self, n):
        """
        :type low: str
        :type high: str
        :rtype: int
        """
        if n == 0:
            return []
        if n == 1:
            return ["0", "1", "8"]

        rotate = {
            "0": "0",
            "1": "1",
            "6": "9",
            "8": "8",
            "9": "6",
        }

        ans = []
        que = collections.deque([])
        if n % 2:
            que.extend(['0', '8', '1'])
            ans.extend(['0', '8', '1'])
        else:
            que.append("")

        while que:
            x = que.popleft()

            for p, q in rotate.items():
                nxt = p + x + q
                if len(nxt) == n and nxt[0] != "0":
                    ans.append(nxt)
                if len(nxt) <= n:
                    que.append(nxt)

        return ans


a = Solution2().findStrobogrammatic(4)
print("ans:", a)

x = "00"
print(str(int(x)) == x)