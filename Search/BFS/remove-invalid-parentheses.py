"""
time C(n, c), n = len(s), c = number of del char
"""
class Solution(object):
    def removeInvalidParentheses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        if len(s) == 0:
            return [""]
        que = [s]
        vst = {s}
        ans = []
        found = False
        while que:
            new_q = []

            for curr in que:
                if self.valid(curr):
                    ans.append(curr)
                    found = True

                res = self.get_next(curr)
                for itm in res:
                    if itm in vst:
                        continue
                    new_q.append(itm)
                    vst.add(itm)

            que = new_q
            if found:
                break
        if len(ans) == 0:
            return [""]
        return ans

    def get_next(self, s):
        res = []
        for i in range(len(s)):
            if s[i] not in ['(', ')']:
                continue
            nxt = s[:i] + s[i + 1:]
            res.append(nxt)
        return res

    def valid(self, s):
        bal = 0
        for ch in s:
            if ch == '(':
                bal += 1
            elif ch == ')':
                bal -= 1
            if bal < 0:
                return False
        return bal == 0

s = '()(a))()'
# s = ")((())))))()(((l(((("
# s = '(a)())()'  # ans: ['(a())()', '(a)()()']
# s = ')('  # ans: ['']
# s = 'n'  # ans: ['n']
a = Solution().removeInvalidParentheses(s)
print("ans:", a)
