import collections
from typing import List


class Solution:
    def isBipartite(self, graph: List[List[int]]) -> bool:
        n = len(graph)
        colors = collections.defaultdict(lambda: 0)

        for i in range(n):
            if colors[i]:
                continue

            que = [i]
            vst = {i}
            colors[i] = 1

            while que:
                print(i, colors)
                new_q = []

                for node in que:
                    c = colors[node]
                    for nei in graph[node]:

                        if colors[nei] == c:
                            return False
                        elif colors[nei] == -c:
                            continue
                        else:
                            colors[nei] = -c
                            new_q.append(nei)
                            vst.add(nei)
                que = new_q

            return True

graph = [[1,3], [0,2], [1,3], [0,2]] # True
graph = [[1,2,3], [0,2], [0,1,3], [0,2]] # False
sol = Solution()
a = sol.isBipartite(graph)
print("ans:", a)