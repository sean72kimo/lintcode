import collections
"""
1. 建立bus to bus的graph
    1. 因為bus to bus可以透過各自路線中的重複站點，所以建構過程比較麻煩
2. 從routes和bus關係圖中，找到起點至終點，需要搭乘哪兩輛公車
    1. corner case, 起點和終點在同一輛公車路線中。
    2.雖然題目中說，每輛公車的route不會有重複的站點，但是若在題目一開始就將每個list of route轉換成set，那麼在判斷起點終點是否在route裡面速度比較快
3. bfs求兩輛公車之間的轉乘次數（步數）
4. 用deque也可以做。看似更簡潔
"""

class Solution(object):
    def numBusesToDestination(self, routes, S, T):
        """
        :type routes: List[List[int]]
        :type S: int
        :type T: int
        :rtype: int
        """
        if S == T:
            return 0

        routes = list(map(set, routes))
        graph = collections.defaultdict(set)
        for i in range(len(routes)):
            for j in range(len(routes)):
                if i == j:
                    continue
                r1 = routes[i]
                r2 = routes[j]

                for r in r1:
                    if r in r2:
                        graph[i].add(j)
                        graph[j].add(i)

        target = set()
        seen = set()
        que = collections.deque([])
        for i, r in enumerate(routes):
            if S in r:
                seen.add(i)
                que.append((i, 1))
            if T in r:
                target.add(i)

        while que:
            node, depth = que.popleft()
            if node in target:
                return depth
            for nei in graph[node]:
                if nei not in seen:
                    que.append((nei, depth + 1))
                    seen.add(nei)

        return -1




class Solution2(object):
    def numBusesToDestination(self, routes, S, T):
        """
        :type routes: List[List[int]]
        :type S: int
        :type T: int
        :rtype: int
        """
        if S == T:
            return 0

        routes = list(map(set, routes))
        graph = collections.defaultdict(set)
        n = len(routes)
        for i in range(n):
            for j in range(n):
                if i == j:
                    continue

                r1 = routes[i]
                r2 = routes[j]

                for node in r1:
                    if node in r2:
                        graph[i].add(j)
                        graph[j].add(i)
                        break

        print(graph)
        journey = self.find(routes, S, T)

        if -1 in journey:
            return -1
        if journey[0] == journey[1]:
            return 1

        que = [journey[0]]
        vst = {journey[0]}
        step = 1

        while que:
            new_q = []
            for curr in que:
                if curr == journey[1]:
                    return step

                for nei in graph[curr]:
                    if nei in vst:
                        continue

                    new_q.append(nei)
                    vst.add(nei)

            que = new_q
            step += 1
        return -1

    def find(self, routes, s, t):
        ret = [-1, -1]
        for i, route in enumerate(routes):
            if s in route and t in route:
                return [i, i]

            if s in route:
                ret[0] = i
            elif t in route:
                ret[1] = i

        return ret


routes, S, T = [[1, 2, 7], [3, 6, 7]], 1, 1
exp = 0
routes, S, T = [[1, 2, 7], [3, 6, 7]], 1, 6
exp = 2

# routes = [[1,9,12,20,23,24,35,38],[10,21,24,31,32,34,37,38,43],[10,19,28,37],[8],[14,19],[11,17,23,31,41,43,44],[21,26,29,33],[5,11,33,41],[4,5,8,9,24,44]]
# S = 37
# T = 37
# exp = 0

# routes = [[7,12],[4,5,15],[6],[15,19],[9,12,13]]
# S = 15
# T = 12
# exp = -1


# routes = [[2],[2,8]]
# S = 8
# T = 2
# exp = 1
a = Solution2().numBusesToDestination(routes, S, T)
print("ans:", a==exp, a)