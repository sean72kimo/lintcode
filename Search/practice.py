from string import ascii_lowercase
import collections
class Solution:
    """
    @param: start: a string
    @param: end: a string
    @param: dict: a set of string
    @return: a list of lists of string
    """
    def __init__(self):
        self.ans = []
    def getNext(self, word, dic):
        lst = []
        for i in range(len(word)):
            for letter in ascii_lowercase:
                newWord = word[:i] + letter + word[i+1:]
                if newWord in dic:
                    lst.append(newWord)
        return lst
        
        
    def findLadders(self, start, end, dic):
        # write your code here
        if start == end:
            return [start]
            
        if len(dic) == 0:
            return [""]
            
        dic = set(dic)
        dic.add(end)
        dic.add(start)
        distance_map = {}
        
        neighbor = collections.defaultdict(list)
        
        # bfs to find min steps to end
        vst = set()
        que = [end]
        step = 1
        distance_map[end] = 1
        
        while que:
            new_q = []
            step += 1
            for word in que:

                nextWords = self.getNext(word, dic)

                if word == 'lot':
                    print(nextWords)

                for nextWord in nextWords:
                    distance_map[nextWord] = step
                    neighbor[word].append(nextWord)
                    if nextWord in distance_map:
                        continue
                    new_q.append(nextWord)
 
            que = new_q
        #print(distance_map)
        print(neighbor)
#         self.dfs(start, end, neighbor, distance_map, [])
#         
#         return self.ans
#         
#     # dfs from start to find end
#     def dfs(self, start, end, neighbor, distance_map, path):
#         if start == end:
#             self.ans.append(path[:])
#             return
#         
#         path.append(start)
#         for nei in neighbor[start]:
#             if distance_map[start] + 1 == distance_map[nei]:
#                 
#                 self.dfs(nei, end, neighbor, distance_map, path)
#         path.pop()
            
            
start = "hit"
end = "cog"
dic = ["hot","dot","dog","lot","log"]

a = Solution().findLadders(start, end, dic)
print("ans:", a)

def bit_not(n, numbits=8):
    return (1 << numbits) - 1 - n

print(bit_not(127, 8))

