class Solution_bit(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        if n == 0:
            return [0]
        res = [0]
        for i in range(1, 2**n):
            print(res[-1], i)
            print(bin(res[-1]), bin(i))
            print('==')
            res.append(res[-1] ^ (i & -i))
        return res

class Solution_DFS_TLE(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        if n == 0:
            return [0]
        path = [0]
        self.ans = []
        self.dfs(n, path, set())
        return self.ans

    def dfs(self, n, path, vst):
        if len(path) == 2 ** n:
            self.ans = path
            return True

        for i in range(1, 2 ** n):
            if i in vst:
                continue

            if not self.valid(path[-1], i):
                continue

            path.append(i)
            vst.add(i)
            if self.dfs(n, path, vst):
                return True
            vst.remove(i)
            path.pop()

        return False

    def valid(self, a, b):
        c = a ^ b
        cnt = 0
        while c:
            if c & 1:
                cnt += 1
            c = c >> 1
        return cnt == 1


a = Solution_bit().grayCode(4)
print("ans:", a)