class Solution(object):
    def __init__(self):
        self.ans = []
    def generatePossibleNextMoves(self, s):
        """
        :type s: str
        :rtype: List[str]
        """

        n = len(s)
        if n < 2:
            return self.ans

        path = list(s)
        self.dfs(s, 0, path)

        return self.ans
    def dfs(self, s, start, path):
        if start == len(s):
            return
        print(start, path[start:start + 2])

        if path[start:start + 2] == ['+', '+']:
            path[start:start + 2] = ['-', '-']

            self.ans.append(''.join(path))
            path[start:start + 2] = ['+', '+']

        self.dfs(s, start + 1, path)


s = "++--+"
a = Solution().generatePossibleNextMoves(s)
print('ans:', a)


S = list(s)
