class Solution(object):
    def __init__(self):
        self.dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]

    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """

        m = len(grid)
        n = len(grid[0])
        islands = set()
        for i in range(m):
            for j in range(n):
                island = []
                if self.dfs(i, j, i, j, grid, m, n, island):
                    islands.add(tuple(island))

        return len(islands)

    def inbound(self, x, y, m, n):
        return 0 <= x < m and 0 <= y < n

    def dfs(self, i0, j0, i, j, grid, m, n, island):
        if not self.inbound(i, j, m, n):
            return False
        if grid[i][j] <= 0:
            return False

        island.append((i - i0, j - j0))
        grid[i][j] = -1

        for dx, dy in self.dxy:
            self.dfs(i0, j0, i + dx, j + dy, grid, m, n, island)

        return True
grid = [[1,1,0,1,1],[1,0,0,0,0],[0,0,0,0,1],[1,1,0,1,1]]
a = Solution().numDistinctIslands(grid)
print("ans:", a)





class Solution2(object):
    def __init__(self):
        self.dxy = [[0,1],[1,0],[0,-1],[-1,0]]
        
    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0
        
        n = len(grid)
        m = len(grid[0])
        ans = set()
        vst = [[False for _ in range(m)] for _ in range(n)]
        for i in range(n):
            for j in range(m):
                if grid[i][j] == 1:
                    if vst[i][j]:
                        continue
                    path=[]
                    self.dfs(i, j, i, j, grid, path, vst)
                    ans.add(tuple(path))
                    print(path)
        return len(ans)
                    
    def dfs(self, i0, j0, i, j, grid, path, vst):
        
        n = len(grid)
        m = len(grid[0])
        
        if not (0 <= i < n and 0 <= j < m):
            return
        
        if vst[i][j]:
            return
        
        if grid[i][j] == 0:
            return
        
        #grid[i][j] = -1
        vst[i][j] = True
        path.append((i-i0, j-j0))
        
        for dx, dy in self.dxy:
            ni = i + dx
            nj = j + dy
            self.dfs(i0, j0, ni, nj, grid, path, vst)







grid=[[1,1,0,1,1],[1,0,0,0,0],[0,0,0,0,1],[1,1,0,1,1]]
a = Solution2().numDistinctIslands(grid)
print("ans:", a)


