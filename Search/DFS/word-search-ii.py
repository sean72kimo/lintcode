class TrieNode:
    def __init__(self):
        self.isWord = False
        self.child = {}
        self.word = ""
        self.curr = ""


class Trie:
    def __init__(self):
        self._root = TrieNode()

    @property
    def root(self):
        return self._root

    def insert(self, word):
        node = self._root
        for letter in word:
            child = node.child.get(letter)
            if not child:
                child = TrieNode()
                node.child[letter] = child
            node = child
            node.curr = letter

        node.isWord = True
        node.word = word


class Solution(object):
    def __init__(self):
        self.ans = set()

    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """

        if board is None or len(board) == 0 or words is None or len(words) == 0:
            return []


        n = len(board)
        m = len(board[0])

        vst = [[False for _ in range(m)] for _ in range(n)]
        dxy = [1, 0], [-1, 0], [0, 1], [0, -1],
        trie = Trie()

        def inbound(i, j):
            return 0 <= i < n and 0 <= j < m

        def search(i, j, node):
            if node.isWord:
                self.ans.add(node.word)

            if not inbound(i, j):
                return

            letter = board[i][j]
            next_node = node.child.get(letter)

            if next_node and not vst[i][j]:
                vst[i][j] = True
                for dx, dy in dxy:
                    ni = i + dx
                    nj = j + dy
                    search(ni, nj, next_node)
                vst[i][j] = False

        for word in words:
            trie.insert(word)

        for i in range(n):
            for j in range(m):
                search(i, j, trie.root)



        return list(self.ans)


board = [
  "doaf",
  "agai",
  "dcan"
]

board = ["aaa", "abb", "abb", "bbb", "bbb", "aaa", "bbb", "abb", "aab", "aba"]
word = {"dog", "dad", "dgdg", "can", "again"}

board = [["o", "a", "a", "n"], ["e", "t", "a", "e"], ["i", "h", "k", "r"], ["i", "f", "l", "v"]]
word = ["oath", "pea", "eat", "rain", "oaan"]

board = [["a"]]
word = ["a"]

print('ans:', Solution().findWords(board, word))
