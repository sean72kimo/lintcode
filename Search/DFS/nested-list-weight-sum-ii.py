"""
三種dfs方法
1 先走到最下層，知道總層數，回到上一層(以及上面每一層)，利用總層數進行權重計算
2 走到最下層，一邊紀錄每一層總和，並用一個map紀錄 depth:summ_at_each_depth, 完事之後再從map裡面計算答案
3 一路往下走，每往下一層走的時候，順便把 carry往下一層傳遞
"""

class Solution(object):
    def depthSumInverse(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        if len(nestedList) == 0:
            return 0
        self.ans = 0
        self.depth = 0
        self.dfs(nestedList, 1)

        return self.ans

    def dfs(self, nestedlist, d):
        if len(nestedlist) == 0:
            return

        lst = []
        this = 0
        for itm in nestedlist:
            if itm.isInteger():
                this += itm.getInteger()
            else:
                lst.extend(itm.getList())

        self.dfs(lst, d + 1)

        self.depth = max(self.depth, d)
        w = self.depth - d + 1
        self.ans += this * w


class Solution(object):
    def depthSumInverse(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        if len(nestedList) == 0:
            return 0
        self.ans = 0
        self.depth = [0]
        self.mp = {}
        self.dfs(nestedList, 1)
        max_d = max(self.mp.keys())

        for d, v in self.mp.items():
            w = max_d - d + 1
            self.ans += w * v

        return self.ans

    def dfs(self, nestedlist, d):
        if len(nestedlist) == 0:
            return

        lst = []
        this = 0
        for itm in nestedlist:
            if itm.isInteger():
                this += itm.getInteger()
            else:
                lst.extend(itm.getList())

        self.mp[d] = this
        self.dfs(lst, d + 1)


class Solution(object):
    def depthSumInverse(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        if len(nestedList) == 0:
            return 0
        self.ans = 0
        self.dfs(nestedList, 1, 0)
        return self.ans

    def dfs(self, nestedlist, d, prev):
        if len(nestedlist) == 0:
            return

        this = prev
        lst = []

        for itm in nestedlist:
            if itm.isInteger():
                this += itm.getInteger()
            else:
                lst.extend(itm.getList())

        self.ans += this
        self.dfs(lst, d + 1, this)