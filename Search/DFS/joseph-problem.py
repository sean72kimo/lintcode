class Solution:
    """
    @param n: an integer
    @param k: an integer
    @return: the last person's number
    """
    def josephProblem(self, n, k):
        if n <= 1:
            return n

        lst = list(range(1, n+1))
        vst = set()
        self.helper(lst, k, vst, 0)
        last = vst ^ set(lst)
        return list(last)[0]
        
    def helper(self, lst, k, vst, start):
        if len(vst) == len(lst) - 1:
            return
        
        i = start
        cnt = 0
        while cnt < k:
            if lst[i] not in vst:
                cnt += 1
                if cnt == k:
                    vst.add(lst[i])
            i += 1
            if i >= len(lst):
                i = i % 5
            
        return self.helper(lst, k, vst, i)
n = 5
k = 3
a = Solution().josephProblem(n, k)
print("ans:",a)