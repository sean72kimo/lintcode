class Solution(object):
    def countArrangement(self, N):
        """
        :type N: int
        :rtype: int
        time O(n!)
        """
        if N <= 1:
            return N

        self.ans = []
        self.cnt = 0
        self.dfs(N, [], set())

        return self.cnt

    def dfs(self, N,  path, vst):
        if len(path) == N:
            self.cnt += 1
            return

        for i in range(1, N + 1):
            if i in vst:
                continue

            path.append(i)
            vst.add(i)
            #pos = len(path)
            if len(path) % i == 0 or i % len(path) == 0:
                self.dfs(N,  path, vst)

            path.pop()
            vst.remove(i)
N = 10
sol = Solution()
a = sol.countArrangement(N)
print("ans:", a , sol.ans)
