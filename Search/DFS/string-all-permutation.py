import collections


class Solution:
    def allPermutation(self, s):
        mp = collections.Counter(s)
        s = list(s)
        path = []
        self.ans = []
        self.dfs(s, mp, path)
        return self.ans

    def dfs(self, s, mp, path):
        if len(path) == len(s):
            self.ans.append(''.join(path))
            return

        for ch in mp:
            if mp[ch] == 0:
                continue
            path.append(ch)
            mp[ch] -= 1
            self.dfs(s, mp, path)
            path.pop()
            mp[ch] += 1

s = "AABC"
sol = Solution()
a = sol.allPermutation(s)
print("ans:", a, len(a))