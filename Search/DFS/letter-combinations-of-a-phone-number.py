class Solution(object):

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if len(digits) == 0:
            return []
        mp = {
            '1': [],
            '2': ['a', 'b', 'c'],
            '3': ['d', 'e', 'f'],
            '4': ['g', 'h', 'i'],
            '5': ['j', 'k', 'l'],
            '6': ['m', 'n', 'o'],
            '7': ['p', 'q', 'r', 's'],
            '8': ['t', 'u', 'v'],
            '9': ['w', 'x', 'y', 'z'],
            '0': []
        }
        self.ans = []
        self.mem = {}
        self.ans = self.dfs(digits, 0, [], mp)
        return self.ans

    def dfs(self, digits, start, path, mp):
        if start in self.mem:
            return self.mem[start]

        if start == len(digits) - 1:
            d = digits[start]
            ret = mp[d][:]
            return ret

        res = []
        d = digits[start]
        ret = self.dfs(digits, start + 1, path, mp)
        for letter in mp[d]:
            for itm in ret:
                sub = letter + itm
                res.append(sub)

        self.mem[start] = res
        return res


class Solution_dfs_mem(object):

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if len(digits) == 0:
            return []
        mp = {
            '1': [],
            '2': ['a', 'b', 'c'],
            '3': ['d', 'e', 'f'],
            '4': ['g', 'h', 'i'],
            '5': ['j', 'k', 'l'],
            '6': ['m', 'n', 'o'],
            '7': ['p', 'q', 'r', 's'],
            '8': ['t', 'u', 'v'],
            '9': ['w', 'x', 'y', 'z'],
            '0': []
        }
        self.ans = []
        self.mem = {}
        self.ans = self.dfs(digits, 0, [], mp)
        return self.ans

    def dfs(self, digits, start, path, mp):
        if start in self.mem:
            return self.mem[start]

        if start == len(digits) - 1:
            d = digits[start]
            ret = mp[d][:]
            return ret

        res = []
        d = digits[start]
        ret = self.dfs(digits, start + 1, path, mp)
        for letter in mp[d]:
            for itm in ret:
                sub = letter + itm
                res.append(sub)

        self.mem[start] = res
        return res

digits="77776627"
a = Solution().letterCombinations(digits)
print("ans:", a)