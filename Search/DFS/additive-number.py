"""
time n^3
"""


class Solution(object):
    def isAdditiveNumber(self, s):
        """
        :type num: str
        :rtype: bool
        """
        if len(s) < 3:
            return False

        for i in range(len(s)-2):
            first = int(s[:i+1])
            if str(first) != s[:i+1]:
                continue

            for j in range(i+1, len(s)-1):
                second = int(s[i+1 : j+1])


                if str(second) != s[i+1 : j+1]:
                    continue

                if self.dfs(s[j+1:], first, second):
                    return True


        return False

    def dfs(self, s, first, second):
        if len(s) == 0:
            return  True

        third = first + second

        summ = str(third)
        print(s, first, second, summ)
        if not s.startswith(summ):
            return False

        j = len(summ)
        if self.dfs(s[j:], second, third):
            return True

        return False

s = "112359"
s = "199100199"
sol = Solution()
a = sol.isAdditiveNumber(s)
print("ans:", a)