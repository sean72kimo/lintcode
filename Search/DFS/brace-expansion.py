class Solution(object):
    def expand(self, S):
        self.ans = []
        self.helper(S, "")
        self.ans.sort()
        return self.ans

    def helper(self, s, path):
        if len(s) == 0:
            self.ans.append(path)
            return

        if s[0] == '{':
            i = s.find('}')
            sub = s[1:i].split(',')
            print(s[i+1:])
            for letter in sub:
                self.helper(s[i+1:], path + letter)
        else:
            print(s[1:])
            self.helper(s[1:], path + s[0])

class Solution2(object):
    def expand(self, S):
        self.ans = []
        self.dfs(S, 0, len(S),"")
        self.ans.sort()
        return self.ans

    def dfs(self, s, i, j, path):
        print(i, j)
        if i == j:
            self.ans.append(path)
            return

        if s[i] == '{':
            #k = s[i:].find('}')
            for k in range(i, len(s)):
                if s[k] == '}':
                    break
            sub = s[i+1:k].split(',')
            print(s[k+1:j])
            for ch in sub:
                self.dfs(s, k+1,j, path+ch)

        else:
            self.dfs(s, i+1, j, path+ s[i])


S = '{a,b,c}d{e,f}'
# S = 'abcd'
a = Solution().expand(S)
print("ans:", a)