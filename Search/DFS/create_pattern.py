import string


class Solution:
    def create_pattern(self, n):

        vst = [False] * 3
        self.ans = []
        self.dfs(n, [], vst)
        return self.ans

    def dfs(self, n, path, vst):
        if len(path) == n:
            self.ans.append(''.join(path))
            return

        for i in range(3):
            if vst[i]:
                c = chr(ord('A') + i)
                path.append(c)
                self.dfs(n, path, vst)
                path.pop()
            else:
                c = chr(ord('A') + i)
                path.append(c)
                vst[i] = True
                self.dfs(n, path, vst)
                vst[i] = False
                path.pop()
                break
        return


a = Solution().create_pattern(3)
print("ans:", a)