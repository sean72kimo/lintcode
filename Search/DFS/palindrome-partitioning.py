import collections

# time n * 2^n (枚舉切分點)
# space
class SolutionMem(object):
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """
        if len(s) == 0:
            return []
        self.mem = {}
        self.ans = []
        self.ans = self.dfs(s, 0, [])
        return self.ans

    def dfs(self, s, start, path):
        if start in self.mem:
            return self.mem[start]

        if start == len(s):
            return [[]]

        res = []
        for i in range(start, len(s)):
            sub = s[start: i + 1]

            if sub == sub[::-1]:
                rt = self.dfs(s, i + 1, path)
                for itm in rt:
                    if itm:
                        res.append([sub] + itm)


        if s[start:] == s[start:][::-1]:
            sub = s[start:]
            print('res1:', res)
            # res = [[sub]]
            res.append([sub])
            print('res2:', res)

        self.mem[start] = res
        return res


class SolutionMem(object):
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """
        if len(s) == 0:
            return []
        self.mem = {}
        self.ans = []
        self.ans = self.dfs(s, 0, [])
        return self.ans

    def dfs(self, s, start, path):
        if start in self.mem:
            return self.mem[start]

        if start == len(s):
            return [[]]

        res = []
        for i in range(start, len(s)):
            sub = s[start: i + 1]

            if sub == sub[::-1]:
                rt = self.dfs(s, i + 1, path)
                for itm in rt:
                    res.append([sub] + itm)

        self.mem[start] = res
        return res

s="aab"
s="ab"
print("ans:", SolutionMem().partition(s))
class Solution(object):
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """
        if len(s) == 0:
            return []

        self.ans = []
        self.dfs(s, 0, [])
        return self.ans

    def dfs(self, s, start, path):
        if start == len(s):
            self.ans.append(path[:])
            return

        for i in range(start, len(s)):
            sub = s[start: i + 1]
            if sub == sub[::-1]:
                path.append(sub)
                self.dfs(s, i + 1, path)
                path.pop()


s="aab"
# s="lintcode"
# print("ans:", Solution().partition(s))
