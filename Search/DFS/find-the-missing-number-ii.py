class Solution:
    """
    @param n: An integer
    @param str: a string with number from 1-n in random order and miss one number
    @return: An integer
    """
    def findMissing2(self, n, str):
        # Write your code here
        vst = set()
        return self.find(n, str, 0, vst)

    def find(self, n, str, index, vst):
        if index == len(str):
            for i in range(1, n + 1):
                if i not in vst:
                    return i
            return -1

        if str[index] == '0':
            return -1

        for l in range(1, 3):
            num = int(str[index : index + l])
            print(index, num)
            if num >= 1 and num <= n and num not in vst:

                vst.add(num)
                target = self.find(n, str, index + l, vst)
                vst.remove(num)
                if target != -1:
                    return target


        return -1


n = 20
string = '19201234567891011121314151618'
a = Solution().findMissing2(n, string)
print("ans:", a)
