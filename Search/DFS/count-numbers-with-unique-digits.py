"""
number doesn't have leading 0, so first dfs, which means first digits, starts with range(1, 10)
"""
class Solution(object):
    def countNumbersWithUniqueDigits(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 1
        if n > 10:
            n = 10

        used = [False for _ in range(10)]
        count = self.search(used, 0, n)

        return count

    def search(self, used, d, n):
        if d == n:
            return 1

        count = 1
        if d == 0:
            rnge = range(1, 10)
        else:
            rnge = range(0, 10)

        for i in rnge:
            if used[i]:
                continue
            used[i] = True
            count += self.search(used, d + 1, n)
            used[i] = False

        return count