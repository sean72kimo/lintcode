"""
"123", 6 -> ["1+2+3", "1*2*3"]
"232", 8 -> ["2*3+2", "2+3*2"]
"105", 5 -> ["1*0+5","10-5"]
"00", 0 -> ["0+0", "0-0", "0*0"]
"3456237490", 9191 -> []
"""
class Solution:
    def __init__(self):
        self.ans = []

    def addOperators(self, num, target):
        """
        :type num: str
        :type target: int
        :rtype: List[str]
        """
        if len(num) == 0:
            return self.ans

        self.dfs(num, target, "", 0, 0, 0)
        return self.ans

    def dfs(self, S, target, string, start, summ, last):
        if start == len(S):
            if summ == target:
                self.ans.append(string)
            return

        for i in range(start, len(S)):
            sub = S[start : i + 1]

            if len(sub) == 0:
                print(i)
                return

            x = int(sub)

            if len(sub) != 1 and sub[0] == '0':
                continue

            if start == 0:
                self.dfs(S, target, "" + sub, i + 1, x, x)
                continue

            newS = string + '+' + sub
            self.dfs(S, target, newS, i + 1, summ + x, x)

            newS = string + '-' + sub
            self.dfs(S, target, newS, i + 1, summ - x, -x)

            newS = string + '*' + sub
            self.dfs(S, target, newS, i + 1, summ - last + x * last, x * last)
s = "123"
t = 6
a = Solution().addOperators(s, t)
print("ans", a)
