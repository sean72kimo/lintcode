class Solution(object):
    def __init__(self):
        self.ans = ""
    def helper(self, num):
        belowTen = ["", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]
        belowTwenty = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", 
                       "Seventeen", "Eighteen", "Nineteen"]
        belowHundred = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"]
        
        
        if num < 10:
            return belowTen[num]

        if num < 20:
            return belowTwenty[num%10]
        
        if num < 100:
            return belowHundred[num//10] + " " + self.helper(num%10)
        
        if num < 1000:
            return belowTen[num//100] + " Hundred" + " " + self.helper(num%100)
        
        if num < 1000000:
            th = num//1000
            return self.helper(th) + " Thousand" + " " + self.helper(num%1000)

        if num < 1000000000:
            th = num//1000000
            return self.helper(th) + " Million" + " " + self.helper(num%1000000)

        if num < 1000000000000:
            th = num//1000000000
            return self.helper(th) + " Billion" + " " + self.helper(num%1000000000)

    def numberToWords(self, num):
        """
        :type num: int
        :rtype: str
        """
        if num == 0 :
            return "Zero"
        
        return self.helper(num)
num = 2120000000
# num = 119912000
a = Solution().numberToWords(num)
print("ans:", a)



