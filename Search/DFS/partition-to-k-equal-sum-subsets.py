class Solution(object):
    def canPartitionKSubsets(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """

        if sum(nums) % k:
            return False

        target = sum(nums) // k
        vst = set()
        return self.dfs(nums, k, target, target, 0, vst)

    def dfs(self, nums, k, target, remain, start, vst):
        if k == 0:
            return True

        if remain < 0:
            return False

        if remain == 0:
            return self.dfs(nums, k - 1, target, target, 0, vst)

        for i in range(start, len(nums)):
            if i in vst:
                continue
            # sub = nums[start : i+1]
            # summ = sum(sub)
            vst.add(i)
            if self.dfs(nums, k, target, remain - nums[i], i + 1, vst):
                return True
            vst.remove(i)
        return False

nums = [4, 3, 2, 3, 5, 2, 1]
k = 4
nums = [2, 2, 2, 2, 3, 4, 5]
k = 4
a = Solution().canPartitionKSubsets(nums, k)
print("ans:", a)


def toInt(ch):
    return ord(ch) - ord('a')

def toCh(i):
    return chr(i + ord('a'))

