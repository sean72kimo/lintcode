class Solution:
    """
    @param: s1: A string
    @param: s2: Another string
    @return: whether s2 is a scrambled string of s1
    """
    def isScramble(self, s1, s2):
        # write your code here
        if len(s1) != len(s2): return False

        if s1 == s2 : return True

        l1 = list(s1)
        l2 = list(s2)

        l1.sort()
        l2.sort()
        if l1 != l2:
            return False

        length = len(s1)

        for i in range(1, length):
            if self.isScramble(s1[:i], s2[:i]) and self.isScramble(s1[i:], s2[i:]):
                return True

            a = s1[:i]
            b = s2[length - i:]
            c = s1[i:]
            d = s2[:length - i]
            print("ab:", a, b)
            print("cd:", c, d)
            if self.isScramble(a, b) and self.isScramble(c, d):
                return True

        return False
s1 = "great"
s2 = "rgtae"
Solution().isScramble(s1, s2)
