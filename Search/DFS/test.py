class Solution(object):
    def __init__(self):
        self.mem = []
        
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        if s is None or len(s) == 0:
            return False
        self.mem = [None for _ in range(len(s))]
        wordDict = set(wordDict)
        a = self.check(s, wordDict, 0)
        print(self.mem)
        return a 
        
    def check(self, s, wordDict, start):
        if self.mem[start] is not None:
            return self.mem[start]
        
        if start == len(s):
            return True
                
        res = False
        for i in range(start, len(s)):
            
            left = s[start : i+1] #leet 
            
            if left in wordDict:
                tmp = self.check(s, wordDict, i+1)
                res = res | tmp

        self.mem[start] = res
        return res
s = "aaaaaaa"
wordDict = ["aaaa","aaa"]
s = "catsandog"
wordDict = ["cats","dog","sand","and","cat"]
a = Solution().wordBreak(s, wordDict)
print("ans:", a)