class Solution(object):
    def __init__(self):
        self.ans = []
        self.used = []
    def combinationSum2(self, C, T):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if not C or len(C) == 0 or T == 0:
            return []

        C.sort()
        self.used = [0 for _ in range(len(C))]

        self.dfs(C, T, [], 0)
        return self.ans

    def dfs(self, C, T, path, start):
        # print(path)
        if T == 0:
            self.ans.append(path[:])
            return

        for i in range(start, len(C)):
            if i != start and C[i] == C[i - 1]:
                continue

            remain = T - C[i]
            if remain < 0:
                return

            if self.used[i]:
                continue

            path.append(C[i])
            self.used[i] = 1
            self.dfs(C, remain, path, i + 1)
            path.pop()
            self.used[i] = 0

candidates = [10, 1, 2, 2, 7, 6, 1, 5]
target = 8

a = Solution().combinationSum2(candidates, target)
print('ans:', a)
