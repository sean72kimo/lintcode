class Solution(object):
    def __init__(self):
        self.row = [set() for _ in range(9)]
        self.col = [set() for _ in range(9)]
        self.grid = [set() for _ in range(9)]
        self.ans = []

    def getK(self, r, c):
        rr = r // 3
        cc = c // 3
        k = rr * 3 + cc
        return k

    def solveSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == '.':
                    continue
                self.row[i].add(board[i][j])
                self.col[j].add(board[i][j])
                k = self.getK(i, j)
                self.grid[k].add(board[i][j])

        self.dfs(board, 0, 0)

    def dfs(self, board, r, c):
        if r == 9:
            self.ans = board[:]
            return True
        
        if c == 9:
            return self.dfs(board, r+1,0)

        if board[r][c] != ".":
            return self.dfs(board, r, c+1)

        k = self.getK(r, c)
        for n in range(1, 10):
            ch = str(n)

            if ch in self.row[r] or ch in self.col[c] or ch in self.grid[k]:
                continue

            board[r][c] = ch
            self.row[r].add(ch)
            self.col[c].add(ch)
            self.grid[k].add(ch)

            if self.dfs(board, r, c+1):
                return True

            board[r][c] = '.'
            self.row[r].remove(ch)
            self.col[c].remove(ch)
            self.grid[k].remove(ch)
            
        return False



board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]
sol = Solution()
sol.solveSudoku(board)
print("ans:",)

for r in sol.ans:
    print(r)



class Solution2(object):
    def __init__(self):
        self.row = [set() for _ in range(9)]
        self.col = [set() for _ in range(9)]
        self.grid = [set() for _ in range(9)]
        self.ans = []

    def getK(self, r, c):
        rr = r // 3
        cc = c // 3
        k = rr * 3 + cc
        return k

    def solveSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == '.':
                    continue
                self.row[i].add(board[i][j])
                self.col[j].add(board[i][j])
                k = self.getK(i, j)
                self.grid[k].add(board[i][j])

        self.dfs(board, 0, 0)

    def dfs(self, board, r, c):
        print(r, c, board[r])
        if r == 9:
            print('.............')
            self.ans = board[:]
            return 
        
        if c == 9:
            self.dfs(board, r+1,0)
            return

        if board[r][c] != ".":
            self.dfs(board, r, c+1)

        k = self.getK(r, c)
        for n in range(1, 10):
            ch = str(n)

            if ch in self.row[r] or ch in self.col[c] or ch in self.grid[k]:
                continue

            board[r][c] = ch
            self.row[r].add(ch)
            self.col[c].add(ch)
            self.grid[k].add(ch)

            self.dfs(board, r, c+1)

            board[r][c] = '.'
            self.row[r].remove(ch)
            self.col[c].remove(ch)
            self.grid[k].remove(ch)


# board = [[".", ".", "9", "7", "4", "8", ".", ".", "."], ["7", ".", ".", ".", ".", ".", ".", ".", "."], [".", "2", ".", "1", ".", "9", ".", ".", "."], [".", ".", "7", ".", ".", ".", "2", "4", "."], [".", "6", "4", ".", "1", ".", "5", "9", "."], [".", "9", "8", ".", ".", ".", "3", ".", "."], [".", ".", ".", "8", ".", "3", ".", "2", "."], [".", ".", ".", ".", ".", ".", ".", ".", "6"], [".", ".", ".", "2", "7", "5", "9", ".", "."]]
# Solution2().solveSudoku(board)
