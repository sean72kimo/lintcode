class Solution(object):
    def __init__(self):
        self.ans = []
    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """

        if candidates is None or len(candidates) == 0:
            return []
        candidates.sort()

        self.dfs(candidates, target, [], 0)
        # self.dfs2(candidates, target, [], 0)
        print(self.ans)
        return self.ans


#     def dfs(self, candidates, target, comb, index):


    def dfs(self, candidates, target, comb, index):
        if sum(comb) > target:
            return

        if sum(comb) == target:
            self.ans.append(comb[:])
            return

        size = len(candidates)
        for i in range(index, size):

            comb.append(candidates[i])
            print(index, i, comb)
            self.dfs(candidates, target, comb, i)
            comb.pop()

candidates = [2, 3, 6, 7]
target = 7
Solution().combinationSum(candidates, target)

