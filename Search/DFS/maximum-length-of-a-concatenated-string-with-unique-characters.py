from typing import List

"""
time 2^n
"""
class Solution:
    def __init__(self):
        self.ans = 0

    def maxLength(self, arr: List[str]) -> int:
        return self.dfs(arr, 0, "")

    def dfs(self, arr, start, path):
        if len(path) != len(set(path)):
            return 0

        best = len(path)

        for i in range(start, len(arr)):
            size = self.dfs(arr, i + 1, path + arr[i])
            best = max(size, best)
        return best

class Solution:
    def maxLength(self, arr: List[str]) -> int:
        # Initialize results with an empty string
        # from which to build all future results
        results = [""]
        best = 0
        for word in arr:
            # We only want to iterate through results
            # that existed prior to this loop
            for i in range(len(results)):
                # Form a new result combination and
                # use a set to check for duplicate characters
                new_res = results[i] + word
                if len(new_res) != len(set(new_res)):
                    continue

                # Add valid options to results and
                # keep track of the longest so far
                results.append(new_res)
                best = max(best, len(new_res))
        return best
arr = ["jnfbyktlrqumowxd","mvhgcpxnjzrdei"]
a = Solution().maxLength(arr)
print("ans:", a)



