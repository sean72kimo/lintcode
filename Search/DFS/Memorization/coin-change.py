class Solution(object):
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        n = len(coins)
        f = [float('inf') for _ in range(amount + 1)]
        coins.sort()
        self.mem = {}
        rt = self.dfs(coins, amount)

        return rt if rt != float('inf') else -1

    def dfs(self, coins, remain):
        if remain in self.mem:
            return self.mem[remain]

        if remain == 0:
            return 0

        res = float('inf')
        for i in range(len(coins)):
            if remain - coins[i] < 0:
                break

            rt = self.dfs(coins, remain - coins[i])
            res = min(res, rt + 1)

        self.mem[remain] = res
        return res
