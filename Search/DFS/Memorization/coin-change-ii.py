class Solution(object):
    def __init__(self):
        self.mem = {}
        
    def change(self, amount, coins):
        coins.sort(reverse = True)
    
        res = self.dfs(coins, amount, 0, "")
        return res

    def dfs(self, coins, remain, start, path):
        if (start, remain) in self.mem:
            return self.mem[(start, remain)]
        
        if remain == 0:
            return 1
        
        if remain < 0:
            return -1
        
        res = 0
        for i in range(start, len(coins)):
            ret = self.dfs(coins, remain-coins[i], i, path + str(coins[i]) + "," )
            if ret > 0:
                res += ret
                
        self.mem[(start, remain)] = res
        return res

    
amount = 5
coins = [1, 2, 5]
a = Solution().change(amount, coins)
print("ans:", a)