


class Solution(object):
    def __init__(self):
        self.mem = {}
        self.ans = 1

    def longestIncreasingPath(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        if len(matrix) == 0:
            return 0
        m = len(matrix)
        n = len(matrix[0])
        ans = 1
        for i in range(m):
            for j in range(n):
                rt = self.dfs(matrix, i, j, set())
                ans = max(ans, rt)

        return ans

    def dfs(self, matrix, i, j, vst):
        if (i, j) in self.mem:
            return self.mem[(i, j)]

        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]

        m = len(matrix)
        n = len(matrix[0])


        res = 1
        for dx, dy in dxy:
            nx = i + dx
            ny = j + dy

            if not (0 <= nx < m and 0 <= ny < n):
                continue
            if (nx, ny) in vst:
                continue
            if matrix[nx][ny] <= matrix[i][j]:
                continue

            vst.add((nx, ny))
            res = max(res, self.dfs(matrix, nx, ny, vst) + 1)
            vst.remove((nx, ny))

        self.mem[(i, j)] = res
        return res


class Solution:
    def __init__(self):
        self.ans = 0

    def longestIncreasingPath(self, matrix) -> int:
        m = len(matrix)
        n = len(matrix[0])

        for i in range(m):
            for j in range(n):
                path = [matrix[i][j]]
                vst = {(i, j)}
                if (i, j) == (2,1):
                    self.dfs(matrix, i, j, path, vst)

        return self.ans

    def dfs(self, matrix, i, j, path, vst):
        m = len(matrix)
        n = len(matrix[0])


        self.ans = max(self.ans, len(path))
        nxy = [i + 1, j], [i, j + 1], [i - 1, j], [i, j - 1],
        for nx, ny in nxy:
            if (nx, ny) in vst:
                continue

            if not (0 <= nx < m and 0 <= ny < n):
                continue

            if matrix[nx][ny] <= matrix[i][j]:
                continue

            path.append(matrix[nx][ny])
            vst.add((nx, ny))

            self.dfs(matrix, nx, ny, path, vst)

            path.pop()
            vst.remove((nx, ny))

matrix = [[9,9,4],[6,6,8],[2,1,1]]
e = 4
a = Solution().longestIncreasingPath(matrix)
print("ans:", a==e, a)