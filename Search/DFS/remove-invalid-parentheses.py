class Solution:
    """
    @param s: The input string
    @return: Return all possible results
    """
    def removeInvalidParentheses(self, s):
        # Write your code here
        l = 0
        r = 0
        for i in range(len(s)):
            if s[i] == '(':
                l += 1
            if s[i] == ')':
                if l > 0:
                    l -= 1
                else:
                    r += 1

        res = []
        self.dfs(res, s, 0, l, r)

        return res

    def dfs(self, res, ss, start, l, r):
        print(ss)
        if l == 0 and r == 0 and self.isValid(ss):
            res.append(ss)
            return

        for i in range(start, len(ss)):
            if i != start and ss[i] == ss[i - 1]:
                continue

            if l > 0 and ss[i] == '(':
                print("remove left, nxt:", ss[0:i] + ss[i + 1:])
                self.dfs(res, ss[0:i] + ss[i + 1:], i, l - 1, r)

            if r > 0 and ss[i] == ')':
                print("remove right, nxt:", ss[0:i] + ss[i + 1:])
                self.dfs(res, ss[0:i] + ss[i + 1:], i, l, r - 1)

    def isValid(self, ss):
        l, r = 0, 0
        for i in range(len(ss)):
            if ss[i] == '(':
                l += 1
            if ss[i] == ')':
                if l - 1 < 0:
                    return False
                l -= 1
        return l == 0

s = "()())()"
s = ")("
print("input", s)
a = Solution().removeInvalidParentheses(s)
print("ans:", a)
