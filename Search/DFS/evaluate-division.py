import collections


class Solution_dfs_top_down(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        if len(queries) == 0:
            return []

        graph = collections.defaultdict(dict)
        for (x, y), v in zip(equations, values):
            graph[x][y] = v
            graph[y][x] = 1 / v

        ans = []
        for start, end in queries:
            if start == end and start in graph:
                ans.append(1.0)
                continue
            if start not in graph or end not in graph:
                ans.append(-1.0)
                continue

            val = self.dfs(start, end, 1.0, graph, set())
            ans.append(val)

        return ans

    def dfs(self, start, end, val, graph, vst):
        if start not in graph or end not in graph:
            return -1.0

        if start == end:
            return val

        for nei in graph[start]:
            if nei in vst:
                continue
            v = graph[start][nei]
            vst.add(nei)
            rt = self.dfs(nei, end, val * v, graph, vst)
            if rt != -1.0:
                return rt
            vst.remove(nei)

        return -1.0


class Solution_bfs(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        if len(queries) == 0:
            return []

        graph = collections.defaultdict(dict)
        for (x, y), v in zip(equations, values):
            graph[x][y] = v
            graph[y][x] = 1 / v

        ans = []
        for start, end in queries:
            if start not in graph or end not in graph:
                ans.append(-1.0)
                continue

            if start == end:
                ans.append(1.0)
                continue
            res = self.bfs(graph, start, end)
            ans.append(res)

        return ans

    def bfs(self, graph, start, end):
        que = [(start, 1.0)]
        vst = {start}

        while que:
            new_q = []

            for curr, val in que:
                if curr == end:
                    return val

                for nei in graph[curr]:
                    if nei in vst:
                        continue
                    v = graph[curr][nei]
                    new_q.append((nei, val * v))
                    vst.add(nei)

            que = new_q

        return -1.0


class Solution_bfs:

    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """

        g = collections.defaultdict(dict)
        for (x, y), val in zip(equations, values):
            g[x][y] = val
            g[y][x] = 1 / val

        ans = []
        for start, end in queries:
            if start == end and start in g:
                ans.append(1.0)
                continue

            queue, res, vst = collections.deque(), -1.0, set()
            queue.append((start, 1.0))

            while queue and res == -1.0:
                curr, val = queue.popleft()
                vst.add(curr)
                for nei in g[curr]:
                    if nei in vst:
                        continue
                    new_val = val * g[curr][nei]
                    if nei == end:
                        res = new_val

                    queue.append((nei, new_val))
            ans.append(res)
        return ans


equations = [ ["a", "b"], ["b", "c"] ]
values = [2.0, 3.0]
queries = [ ["a", "c"], ["b", "c"], ["a", "e"], ["a", "a"], ["x", "x"] ]

# equations = [["a", "b"], ["c", "d"]]
# values = [1.0, 1.0]
# queries = [["a", "c"], ["b", "d"], ["b", "a"], ["d", "c"]]
# [-1.0, -1.0, 1.0, 1.0]
a = Solution_bfs().calcEquation(equations, values, queries)
print("ans:", a)


class Solution_dfs_bottom_up:

    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        g = collections.defaultdict(dict)
        for (x, y), val in zip(equations, values):
            g[x][y] = val
            g[y][x] = 1 / val

        ans = []
        for s, e in queries:
            if s not in g or e not in g:
                ans.append(-1.0)
                continue

            v = self.dfs(s, e, g, {s})
            ans.append(v)

        return ans

    def dfs(self, s, e, g, vst):
        if s == e:
            return 1.0

        for nei in g[s]:
            if nei in vst:
                continue
            vst.add(nei)
            d = self.dfs(nei, e, g, vst)
            if d > 0:
                return d * g[s][nei]
            vst.remove(nei)

        return -1.0

# equations = [ ["a", "b"], ["b", "c"] ]
# values = [2.0, 3.0]
# queries = [ ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]
#
# equations = [["a", "b"], ["c", "d"]]
# values = [1.0, 1.0]
# queries = [["a", "c"], ["b", "d"], ["b", "a"], ["d", "c"]]
# # [-1.0, -1.0, 1.0, 1.0]
# a = Solution_dfs().calcEquation(equations, values, queries)
# print("ans:", a)
