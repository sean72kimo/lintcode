import collections
from typing import List


class Solution:
    def __init__(self):
        self.res = float('inf')

    def minTransfers(self, transactions: List[List[int]]) -> int:
        person = collections.defaultdict(int)

        for x, y, amount in transactions:
            person[x] -= amount
            person[y] += amount

        accounts = list(person.values())

        self.dfs(accounts, 0, 0, )
        return self.res

    def dfs(self, accounts, i, cnt, ):

        while i < len(accounts) and accounts[i] == 0:
            i += 1

        if i == len(accounts):
            self.res = min(self.res, cnt)
            return

        if cnt >= self.res:
            return

        for j in range(i + 1, len(accounts)):
            amount = accounts[i]

            if accounts[i] * accounts[j] < 0:
                accounts[j] += amount
                accounts[i] -= amount
                self.dfs(accounts, i + 1, cnt + 1)
                accounts[i] += amount
                accounts[j] -= amount


transactions = [[0,1,10],[2,0,5]]
a = Solution().minTransfers(transactions)
print("ans:", a)