from sys import maxsize

class Solution2(object):
    def solveNQueens(self, n):
        res = []
        self.dfs([-1] * n, 0, [], res)
        return res

    # nums is a one-dimension array, like [1, 3, 0, 2] means
    # first queen is placed in column 1, second queen is placed
    # in column 3, etc.
    def dfs(self, nums, index, path, res):
        if index == len(nums):
            res.append(path)
            return  # backtracking
        for i in range(len(nums)):
            nums[index] = i
            if self.valid(nums, index):  # pruning
                tmp = "."*len(nums)
                self.dfs(nums, index + 1, path + [tmp[:i] + "Q" + tmp[i + 1:]], res)

    # check whether nth queen can be placed in that column
    def valid(self, nums, n):
        for i in range(n):
            if abs(nums[i] - nums[n]) == n - i or nums[i] == nums[n]:
                return False
        return True


b = Solution2().solveNQueens(0)
print('ans:', b)

class Solution(object):
    def __init__(self):
        self.ans = []
        self.size = 0

    def solveNQueens(self, n):
        self.size = n
        res = []
        arr = [None] * self.size
        self.dfs(arr, 0, [])
        return self.ans


    def dfs(self, arr, row, path):

        if row == self.size:
            # print('...', path)
            self.ans.append(path[:])
            return  # backtracking

        for col in range(self.size):
            # print("[{0},{1}]".format(row, col), arr)
            if self.valid(arr, row, col):
                arr[row] = col

                tmp = ["."] * self.size
                tmp[col] = "Q"
                this_row = ''.join(tmp)
                path.append(this_row)
                print(path)
                self.dfs(arr, row + 1, path)
                path.pop()
                arr[row] = None
                # update_path = path + this_row
                # print(update_path)
                # self.dfs(arr, row + 1, update_path)

            # arr[row] = None
        return

    # check whether nth queen can be placed in that column
    def valid(self, arr, row, col):

        for r , c in enumerate(arr):
            if c is None or row == r:
                continue

            if arr[r] == col:  # except current row, col cannot be in other row
                return False

            if row - col == r - c or row + col == r + c:
                return False

        return True

a = Solution().solveNQueens(4)
print('ans:', a)

print(a == b)


x = {"a":1, "b":5}
y = {"a":1, "x":5}
print(ord('a'))
