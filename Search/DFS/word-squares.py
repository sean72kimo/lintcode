import collections


class Solution(object):
    def wordSquares(self, words):
        """
        :type words: List[str]
        :rtype: List[List[str]]
        """
        n = len(words)
        if n == 0:
            return []

        self.ans = []
        self.hs = collections.defaultdict(list)

        self.size = len(words[0])
        self.initPrefix(words)
        self.dfs(0, [])
        return self.ans

    def dfs(self, l, path):
        if l == self.size:
            self.ans.append(path[:])
            return

        prefix = ""
        for i in range(l):
            prefix += path[i][l]

        candidates = self.hs[prefix]  # list of strings

        for w in candidates:
            if l + 1 < self.size and not self.checkprefix(l, w, path):
                continue

            path.append(w)
            self.dfs(l + 1, path)
            path.pop()

    def checkprefix(self, l, nxt_word, path):
        prefix = ""
        for i in range(l):
            prefix = prefix + path[i][l + 1]
        prefix += nxt_word[l+1]
        return prefix in self.hs

    def initPrefix(self, words):
        for word in words:
            for i in range(len(word)):
                sub = word[:i]
                self.hs[sub].append(word)



words = ["ball", "area", "lady", "leap", "lead", "wall", ]
a = Solution().wordSquares(words)
print('ans:', a)


