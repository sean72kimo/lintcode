from copy import deepcopy
class Solution:
    """
    @param: n: Given the range of numbers
    @param: k: Given the numbers of combinations
    @return: All the combinations of k numbers out of 1..n
    """
    def __init__(self):
        self.ans = []
    def combine(self, n, k):
        # write your code here
        if n == 0 or k == 0:
            return []

        self.dfs([], n, k, 1)
        return self.ans

    def dfs(self, path, n, k, start):
        if len(path) == k:
            self.ans.append(deepcopy(path))
            return

        for i in range(start, n + 1):
            path.append(i)
            self.dfs(path, n, k , i + 1)
            path.pop()

print('ans:', Solution().combine(4, 2))
