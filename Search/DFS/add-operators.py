DEBUG = True
class Solution:
    """
    @param: num: a string contains only digits 0-9
    @param: target: An integer
    @return: return all possibilities
    """
    num = None
    target = None
    ans = []
    def addOperators(self, num, target):
        # write your code here
        self.num = num
        self.target = target
        self.dfs(0, "", 0, 0)
        return self.ans


    def dfs(self, pos, string, sum, last_val):
        # print(pos, string, sum, last_val)
        if pos == len(self.num):
            if sum == self.target:
                self.ans.append(string)
                return
            return

        for i in range(pos, len(self.num)):
            num_str = self.num[pos : i + 1]
            if len(num_str) > 1 and num_str[0] == '0':
                continue

            cur_val = int(num_str)

            if pos == 0:
                self.dfs(i + 1, "" + str(cur_val), cur_val, cur_val)
            else:
                self.dfs(i + 1, string + "+" + str(cur_val), sum + cur_val, cur_val)
                self.dfs(i + 1, string + "-" + str(cur_val), sum - cur_val, -cur_val)
                self.dfs(i + 1, string + "*" + str(cur_val), sum - last_val + cur_val * last_val, cur_val * last_val)



# num = "123"
# target = 6
# num = "105"
# target = 5
# num = "00"
# target = 0
# num = "232"
# target = 8
# num = "123456789"
# target = 45
num = "3456237490"
target = 9191
print('ans:', Solution().addOperators(num, target))
