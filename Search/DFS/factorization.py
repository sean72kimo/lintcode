import copy
class Solution:
    """
    @param: n: An integer
    @return: a list of combination
    """
    def __init__(self):
        self.ans = []
    def getFactors(self, n):
        # write your code here
        if n is None or 0:
            return []

        self.dfs(2, n, [])
        return self.ans

    def dfs(self, i, remain, combination):

        if remain == 1 and len(combination) > 1:
            self.ans.append(copy.deepcopy(combination))
            return


        for i in range(i, remain + 1):
            if i > remain / i:
                print(i, remain)
                break

            if remain % i == 0:
                combination.append(i)
                self.dfs(i, int(remain / i), combination)
                combination.pop()

#         if remain == i:
#             print('  ', i, remain)
#             combination.append(remain)
#             self.dfs(remain, 1, combination)
#             combination.pop()


print('ans:', Solution().getFactors(8))
