from typing import List


class Solution:
    def __init__(self):
        self.ans = float('inf')

    def closestCost(self, baseCosts: List[int], toppingCosts: List[int], target: int) -> int:

        summ = 0
        for i in range(len(baseCosts)):
            summ = baseCosts[i]
            self.dfs(toppingCosts, summ, target, 0)

        return self.ans

    def dfs(self, topping_cost, summ, target, idx):
        if summ == target:
            self.ans = summ
            return

        if abs(target - summ) < abs(target - self.ans):
            self.ans = summ
        elif abs(target - summ) == abs(target - self.ans) and summ < self.ans:
            self.ans = summ

        if idx >= len(topping_cost):
            return

        cost = topping_cost[idx]
        self.dfs(topping_cost, summ, target, idx + 1)
        self.dfs(topping_cost, summ + cost, target, idx + 1)
        self.dfs(topping_cost, summ + cost * 2, target, idx + 1)