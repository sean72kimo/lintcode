class Solution(object):
    def splitIntoFibonacci(self, s):
        """
        :type S: str
        :rtype: List[int]
        len(str(MAX)) = 10
        each level will for loop to 10, n level
        time: 10^n , n = len(s)
        """
        if len(s) == 0:
            return []

        self.ans = []
        self.dfs(s, 0, [])
        return self.ans[0] if len(self.ans) else []

    def dfs(self, s, start, path):
        if start == len(s):
            if len(path) > 2:
                self.ans.append(path[:])
            return

        for i in range(start, len(s)):
            sub = s[start: i + 1]
            val = int(sub)
            if val > 2**31-1:
                return

            if str(val) != sub:
                return

            if len(path) < 2:
                path.append(val)
                self.dfs(s, i + 1, path)
                path.pop()

            else:

                if val == path[-1] + path[-2]:
                    path.append(val)
                    self.dfs(s, i + 1, path)
                    path.pop()
                elif val > path[-1] + path[-2]:
                    return
                else:
                    continue

s = "123456579"

s = "11235813" # [1,1,2,3,5,8,13]
s = "0123" # []
s = "1101111" # [110, 1, 111]
s = "112358130"
s = "539834657215398346785398346991079669377161950407626991734534318677529701785098211336528511"
a = Solution().splitIntoFibonacci(s)
print("ans:", a)
n = 2**31-1
print(len(str(MAX)))