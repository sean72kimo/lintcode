class Solution:
    """
    @param str: A string
    @return: all permutations
    """
    def __init__(self):
        self.ans = []

    def stringPermutation2(self, S):
        # write your code here
        if len(S) == 0:
            return []

        S = list(S)
        S.sort()
        vst = [False for _ in range(len(S))]
        self.helper(S, [], vst)

        return self.ans

    def helper(self, S, path, vst):
        if len(path) == len(S):
            # join to make string
            string = ''.join(path[:])
            self.ans.append(string)
            return

        for i in range(len(S)):
            if i - 1 >= 0 and S[i] == S[i - 1]:
                if vst[i - 1] == False:
                    continue

            if vst[i]:
                continue

            path.append(S[i])
            vst[i] = True
            self.helper(S, path, vst)
            path.pop()
            vst[i] = False
S = "abb"
a = Solution().stringPermutation2(S)
print("ans:", a)
