import collections


class Solution_DFS(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        self.mem = {}
        self.ans = []
        cnt = self.dfs(nums, target, [])
        print(self.ans)
        return cnt

    def dfs(self, nums, target, path):
        if target in self.mem:
            return self.mem[target]

        if target == 0:
            self.ans.append(tuple(path))
            return 1

        if target < 0:
            return 0

        res = 0
        for i in range(len(nums)):
            path.append(nums[i])
            res += self.dfs(nums, target - nums[i], path)
            path.pop()

        self.mem[target] = res
        return res

class Solution_DP(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int

        lintcode backpack VI


        """
        if len(nums) == 0:
            return 0
        f = [0 for _ in range(target + 1)]

        for i in range(target + 1):
            if i == 0:
                f[i] = 1
                continue

            for j in range(len(nums)):
                if i - nums[j] >= 0:
                    f[i] += f[i - nums[j]]

        return f[target]



# 打印路徑
class Solution_print_path(object):
    def combinationSum4(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int

        lintcode backpack VI


        """
        if len(nums) == 0:
            return 0
        f = [0 for _ in range(target + 1)]
        p = collections.defaultdict(list)
        p[0] = [""]


        print(p)
        for i in range(target + 1):
            if i == 0:
                f[i] = 1
                continue

            for j in range(len(nums)):
                if i - nums[j] >= 0:
                    f[i] += f[i - nums[j]]

                    for path in p[i - nums[j]]:
                        p[i].append(path + '+' + str(nums[j]))


        print(p[target])
        return f[target]

A = [1, 2]
T = 2

A = [1, 2, 3]
T = 4
a = Solution_print_path().combinationSum4(A, T)
print("ans:", a)