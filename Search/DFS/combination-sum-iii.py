class Solution(object):
    def __init__(self):
        self.ans = []
        self.visited = []
    def combinationSum3(self, k, n):
        """
        :type k: int
        :type n: int
        :rtype: List[List[int]]
        """
        if k == 0:
            return []

        self.dfs(k, n, [], 1)
        return self.ans

    def dfs(self, k, n, path, start):
        if len(path) == k:
            if n == 0:
                self.ans.append(path[:])
            return


        for i in range(start, 10):

            remain = n - i
            if remain < 0:
                return
            path.append(i)
            self.dfs(k, remain, path, i + 1)
            path.pop()

a = Solution().combinationSum3(2, 18)
print('ans:', a)

print(pow(10, 8) < 0x7fffffff)
print(0x7fffffff)
