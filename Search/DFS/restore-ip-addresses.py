class Solution(object):
    def __init__(self):
        self.ans = []
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]

        """
        if s is None or len(s) == 0:
            return []
        self.dfs(s, [])
        return self.ans

    def dfs(self, s, path):
        if len(path) == 4 and len(s) != 0:
            return

        if len(s) == 0:
            if len(path) != 4:
                return

            addr = '.'.join(path)
            self.ans.append(addr)
            return

        for i in range(1, len(s) + 1):
            sub = s[:i]
            remain = s[i:]

            if self.validIP(sub):
                path.append(sub)
                self.dfs(remain, path)
                path.pop()

    def validIP(self, string):
        if int(string) > 255 or int(string) < 0:
            return False
        if string[0] == '0' and len(string) != 1:  # 010, 001
            return False
        return True
s = "25525511135"
s = "0000"
print('ans:', Solution().restoreIpAddresses(s))





class Solution2(object):
    def __init__(self):
        self.ans = []
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]

        """
        if len(s) == 0:
            return self.ans

        self.dfs(s, 0, [])
        return self.ans

    def dfs(self, s, start, path):
        if len(path) == 4:
            if start == len(s):
                ip = ""
                for i, p in enumerate(path):
                    ip = ip + str(p)
                    if i == 3:
                        break
                    ip += '.'
                self.ans.append(ip)
                return
            return

        for i in range(1, 4):
            if start + i > len(s):
                return
            sub = s[start : start + i]
            print(start, start + i, sub)
            # sub = int(sub)
            if self.validIP(sub):
                path.append(sub)
                self.dfs(s, start + i, path)
                path.pop()

    def validIP(self, string):

        if int(string) > 255 or int(string) < 0:
            return False
        if string[0] == '0' and len(string) != 1:  # 010, 001
            return False
        return True



s = "25525511135"
s = "0000"
s = "1111"
ans2 = Solution2().restoreIpAddresses(s)
print('ans2:', ans2)
