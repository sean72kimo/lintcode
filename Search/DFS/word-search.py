class Solution:
    # @param board, a list of lists of 1 length string
    # @param word, a string
    # @return a boolean

    def exist(self, board, word):
        # write your code here

        if word is None or len(word) == 0:
            return True

        if board is None or len(board) == 0:
            return False

        n = len(board)
        m = len(board[0])


        self.visited = [[False for i in range(m)] for j in range(n)]
        self.direction = [[1, 0], [-1, 0], [0, 1], [0, -1]]
        for i in range(n):
            for j in range(m):
                if board[i][j] == word[0]:
                    if self.dfs(board, i, j, word, 0):
                        return True

        return False

    def dfs(self, board, i, j, word, idx):
        n = len(board)
        m = len(board[0])

        # at the end of word, does it match
        if idx == len(word):
            return True

        # bound check
        if i >= n or i < 0  or j >= m or j < 0:
            return False

        # if the char doesn't match
        if board[i][j] != word[idx]:
            return False
        # if char matches, keep dfs searching
        else:
            if not self.visited[i][j]:
                self.visited[i][j] = True
                for x, y in self.direction:
                    if self.dfs(board, i + x, j + y, word, idx + 1):
                        return True

                self.visited[i][j] = False
#
#                 if self.dfs(board, i - 1, j, word, idx + 1):
#                     return True
#                 elif self.dfs(board, i + 1, j, word, idx + 1):
#                     return True
#                 elif self.dfs(board, i, j - 1, word, idx + 1):
#                     return True
#                 elif self.dfs(board, i, j + 1, word, idx + 1):
#                     return True
#                 else:
#                     self.visited[i][j] = False

        return False










board = [
  "ABCE",
  "SFCS",
  "ADEE"
]

word = "ABCCED"
word = "ABCB"

# print('ans:', Solution().exist(board, word))











# 第二次練習
class Solution2(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        if word is None or len(word) == 0:
            return True

        if board is None or len(board) == 0:
            return False

        n = len(board)
        m = len(board[0])

        def inbound(i, j):
            return 0 <= i < n and 0 <= j < m

        def dfs(i, j, word, path):
            if word == "":
                # print(board[i][j] , word, len(word), path)
                print(path)
                return True

            if not inbound(i, j):
                return False

            if board[i][j] == word[0] and not visited[i][j]:
                visited[i][j] = True
                for dx, dy in direction:
                    ni = i + dx
                    nj = j + dy

                    path.append(board[i][j])
                    if dfs(ni, nj, word[1:], path):
                        return True
                    path.pop()
                visited[i][j] = False
            else:
                return False

            return False

        direction = [1, 0], [-1, 0], [0, 1], [0, -1],
        visited = [[False for _ in range(m)] for _ in range(n)]
        for i in range(n):
            for j in range(m):
                if dfs(i, j, word, []):
                    return True
        return False

board = [["a"]]
word = "a"



# print('ans:', Solution2().exist(board, word))


class Solution3(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        if len(board) == 0:
            return False

        m = len(board)
        n = len(board[0])
        for i in range(m):
            for j in range(n):
                path = []
                if self.dfs(board, word, i, j, set(), 0, path):
                    # 打印路徑 
                    print(path)
                    print([board[i][j] for i, j in path])
                    return True
        return False

    def dfs(self, board, word, i, j, vst, start, path):
        m = len(board)
        n = len(board[0])
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]

        if start == len(word):
            return True

        if not (0 <= i < m and 0 <= j < n):
            return False

        if (i, j) in vst:
            return False

        if board[i][j] != word[start]:
            return False

        vst.add((i, j))
        path.append((i, j))

        for dx, dy in dxy:
            nx = dx + i
            ny = dy + j
            if self.dfs(board, word, nx, ny, vst, start + 1, path):
                return True
        vst.remove((i, j))
        path.pop()
        return False

board = [["A","B","C","E"],["S","F","E","S"],["A","D","E","E"]]
word = "ABCEFSADEESE"
print('ans:', Solution3().exist(board, word))



