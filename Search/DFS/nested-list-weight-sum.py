class Solution(object):
    def depthSum(self, nestedList):
        """
        :type nestedList: List[NestedInteger]
        :rtype: int
        """
        self.ans = 0
        self.dfs(nestedList, 1)
        return self.ans

    def dfs(self, nestedlist, d):
        if len(nestedlist) == 0:
            return
        lst = []
        for i, itm in enumerate(nestedlist):
            # print(itm)
            if itm.isInteger():
                self.ans += d * itm.getInteger()
            else:

                lst.extend(itm.getList())
        self.dfs(lst, d + 1)