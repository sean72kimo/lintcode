class Solution:
    def allCombinations(self, s):
        lst = self.preprocess(s, 0, len(s)-1)
        print('parsed',lst)
        path = []
        res = self.build_str(lst, path)

        return  res

    def build_str(self, lst, path):
        if len(lst) == 0:
            return []
        all_str = True
        for itm in lst:
            if not isinstance(itm, str):
                all_str = False
                break
        if all_str:
            print('all str:',lst)
            return lst

        res = []
        for i, itm in enumerate(lst):
            if isinstance(itm, str):
                if len(res):
                    new = []
                    for sub in res:
                        ii = sub + itm
                        new.append(ii)
                    res = new[:]
                else:
                    res.append(itm)

            elif isinstance(itm, list):
                rt = self.build_str(itm, path)
                if len(rt):
                    if len(res):
                        new = []
                        for sub in res:
                            for ii in rt:
                                bb = sub + ii
                                new.append(bb)
                        res = new[:]
                    else:
                        res = rt[:]

        return res


    def preprocess(self, s, start, end):

        if start > end:
            return []
        if start == end:
            return [s[start]]

        if s[start] != '{' and s[end] != '}':
            path = []
            for i in range(start, end+1):
                if s[i].isalpha():
                    path.append(s[i])
            return path

        bal = 0
        lo = float('inf')
        hi = float('-inf')
        path = []

        for i in range(start, end+1):
            if s[i] == '{':
                bal += 1
                lo = min(lo, i)
            elif s[i] == '}':
                bal -= 1
                hi = max(hi, i)

            if bal == 0:
                if s[i].isalpha():
                    path.append(s[i])
                else:
                    rt = self.preprocess(s, lo+1, hi-1)

                    lo = float('inf')
                    hi = float('-inf')
                    if rt:
                        path.append(rt)

        return path[:]



# nestedlist = ['a','b',['c',['d'],'e',['f',['g','h'],'i','j'],'k']]
s = "{a,b}c{d,e,{f,g}}i"
s = "{a,b},c,{{d,e},{f,g}}"
# s = "c{{d,e},{g,h}}"
s = "{a,b,{c, d}}"

# s = "{a,b},c,{d,e,f,g}"
# s = "{a},c,{d}"
# s = "{a,{b,x}}c{d,e}"
# s = "{a,{b, c}}"
# s = "a,{b, c}"
# s = "{b, c}a"
a = Solution().allCombinations(s)
print('ans:',a)