class Solution:
    """
    @param: s: A string
    @param: wordDict: A set of words.
    @return: All possible sentences.
    """
    def __init__(self):
        self.ans = []
        self.mem = {}

    def wordBreak(self, s, wordDict):
        # write your code here

        # res = self.dfs(s, wordDict, 0, {})
        res = self.helper(s, wordDict)

        return res


    def helper(self, s, wordDict):


        if s in self.mem:
            print(s, self.mem)
            return self.mem[s]

        res = []
        for i in range(1, len(s) + 1):
            left = s[:i]
            right = s[i:]
            partRes = []

            if left in wordDict:
                partRes = self.helper(right, wordDict)

                if partRes:
                    for itm in partRes:
                        res.append(left + " " + itm)
                else:
                    if i == len(s):
                        res.append(left)

        self.mem[s] = res
        return res


s = "lintcode"
wordDict = ["de", "ding", "co", "code", "lint"]

s = "catsanddog"
wordDict = ["cat", "cats", "and", "sand", "dog"]
sol = Solution()
a = sol.wordBreak(s, wordDict)
print("ans:", a)
print(sol.mem)

import collections
class Solution2(object):
    def __init__(self):
        self.mem = {}
        self.ans = []
        
        
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: List[str]
        """
        if s is None or len(s) == 0:
            return []
        wordDict = set(wordDict)
        return self.dfs(s, wordDict, 0, [])
        return self.ans
    
    
    def dfs(self, s, wordDict, start, path):
        if start in self.mem:
            return self.mem[start]
        
        if start == len(s):
            self.mem[start] = [' '.join(path)]

            return [' '.join(path)]
        
        
        for i in range(start, len(s)):
            sub = s[start : i+1]
            res = []
            if sub in wordDict:
                rt = self.dfs(s, wordDict, i+1, path)
                if rt:
                    res.append(rt)
                
        self.mem[start] = res
        return res
                
s = "lintcode"
wordDict = ["de", "ding", "co", "code", "lint"]
sol = Solution2()
a = sol.wordBreak(s, wordDict)
print("ans:", a)
print(sol.mem)




