class Solution:
    def findSubsequences(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        res = []
        n = len(nums)
        def dfs(i, path):
            if len(path) > 1:
                res.append(path)
            seen = set()
            for j in range(i + 1, n):
                if nums[j] not in seen and (path == [] or nums[j] >= nums[i]):
                    seen.add(nums[j])
                    dfs(j, path + [nums[j]])

        dfs(-1, [])

        return res

class Solution2(object):
    def __init__(self):
        self.ans = []

    def findSubsequences(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if not nums or len(nums) == 0:
            return []
 
        self.helper(nums, 0, [])
        return self.ans

    def helper(self, nums, start, holder):
        seen = set()
        if len(holder) >= 2:
            self.ans.append(holder[:])

        if start == len(nums):
            return

        for i in range(start , len(nums)):

            if nums[i] not in seen and (len(holder) == 0 or nums[i] >= holder[-1]):
                seen.add(nums[i])
                holder.append(nums[i])
                self.helper(nums, i + 1, holder)
                holder.pop()


nums = [6, 4, 7, 7]
nums = [4, 6, 7, 7]
a = Solution2().findSubsequences(nums)
print("ans:", a)












class Solution3(object):
    def __init__(self):
        self.ans = set()
        
    def findSubsequences(self, nums):
        if nums is None or len(nums) == 0:
            return self.ans

        seen = set()
        self.dfs(nums, 0, [], seen)
        return self.ans
        
    def dfs(self, nums, start, path, seen):
        
        if len(path) >= 2:
            self.ans.add(tuple(path[:]))


        for i in range(start, len(nums)):
            if len(path) == 0 or (nums[i] >= path[-1]):
                path.append(nums[i])
                
                self.dfs(nums, i+1, path, seen)
                
                path.pop()
                
nums = [4, 6, 7, 7]
a = Solution3().findSubsequences(nums)
print("ans:", a)