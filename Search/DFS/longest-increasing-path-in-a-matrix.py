class Solution(object):
    def __init__(self):
        self.memo = None
        self.vst = None
        self.ans = 1
        self.dxy = [[1, 0], [-1, 0], [0, 1], [0, -1]]

    def longestIncreasingPath(self, M):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        if not M or len(M) == 0 :
            return 0

        m = len(M)
        n = len(M[0])
        self.memo = [[0 for _ in range(n)] for _ in range(m)]
        self.vst = [[0 for _ in range(n)] for _ in range(m)]


        for i in range(m):
            for j in range(n):
                self.vst[i][j] = True
                l = self.dfs(M, i , j)
                self.ans = max(self.ans, l)
                self.vst[i][j] = False

        for r in self.memo:
            print (r)
        return self.ans

    def dfs(self, M, i, j):
        if self.memo[i][j] != 0:
            return self.memo[i][j]

        m = len(M)
        n = len(M[0])
        maxL = 1

        for dx, dy in self.dxy:
            ni = i + dx
            nj = j + dy
            if not (0 <= ni < m and 0 <= nj < n):
                continue

            if self.vst[ni][nj] or M[ni][nj] <= M[i][j]:
                continue

            self.vst[ni][nj] = True
            length = 1 + self.dfs(M, ni, nj)
            maxL = max(maxL, length)
            self.vst[ni][nj] = False

        self.memo[i][j] = maxL
        return maxL


def main():

    nums = [
        [9, 9, 4],
        [6, 6, 8],
        [2, 1, 1]
    ]

    nums = [
        [3, 4, 5],
        [3, 2, 6],
        [2, 2, 1]
    ]



    a = Solution().longestIncreasingPath(nums)
    print("ans:", a)

if __name__ == "__main__":
    main()
