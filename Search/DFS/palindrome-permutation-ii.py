import collections
class Solution(object):
    def __init__(self):
        self.ans = []
        
    def generatePalindromes(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        if len(s) == 0:
            return self.ans
        
        count = collections.Counter(s)
        
        odd = 0
        # if can't create palin, then return []
        for ch, cnt in count.items():
            if cnt % 2 == 1:
                odd += 1
                if odd > 1:
                    return self.ans

        single = [] # is there a single ch in the mid of palin?

        size = 0 # for total len of valid palin
        for ch, cnt in count.items():
            size += cnt
            if cnt % 2 == 1:
                single.append(ch)
                
                if count[ch] == 1:
                    del count[ch] # case 'acbca
                else:
                    count[ch] -= 1 # case 'aaa'
                
        self.dfs(list(count.keys()), count, [], single, size)
        return self.ans
    
    def dfs(self, arr, count, path, single, size):
        palin = ''.join(path + single + path[::-1])
        if len(palin) == size:
            self.ans.append(palin)
            
        
        for i in range(len(arr)):
            if count[arr[i]] == 0:
                continue
                
            path.append(arr[i])
            count[arr[i]] -= 2
            self.dfs(arr, count, path, single, size)
            path.pop()
            count[arr[i]] += 2

s = "aaxbb"
s = "abbcc"
s = "aaa"
s = "aaaaaa"
s = "aabbccc"
# s = "aabb"
# a = Solution().generatePalindromes(s)
# print("ans:",a)

class Solution2:
    def generatePalindromes(self, s):
        if len(s) <= 1:
            return [s]

        mp = collections.Counter(s)

        flag = 0
        single = ""
        size = 0
        for ch, cnt in mp.items():
            size += cnt
            if cnt % 2:
                flag += 1
                single = ch
                if flag > 1:
                    return []
        self.ans = []
        if single in mp:
            mp[single] -= 1
        self.dfs(mp.keys(), mp, single, "", size)
        return self.ans

    def dfs(self, arr, mp, single, path, size):
        string = path + single + path[::-1]
        if len(string) == size:
            self.ans.append(string)
            return

        for ch in arr:
            if mp[ch] == 0:
                continue

            mp[ch] -= 2
            self.dfs(arr, mp, single, path + ch, size)
            mp[ch] += 2

s = "aabb"
s = "aab"
a = Solution2().generatePalindromes(s)
print("ans:",a)