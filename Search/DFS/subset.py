class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if len(nums) == 0:
            return []

        nums.sort()
        self.ans = []
        self.dfs(nums, 0, [])
        return self.ans

    def dfs(self, nums, start, path):

        self.ans.append(path[:])

        if start == len(nums):
            return

        for i in range(start, len(nums)):
            path.append(nums[i])
            self.dfs(nums, i + 1, path)
            path.pop()




S = [1, 2, 3]
print('ans:', Solution().subsets(S))
