import collections


class Solution:
    def findItinerary(self, tickets):
        """
        :type tickets: List[List[str]]
        :rtype: List[str]

        建立graph[start] = [[dest, True], [dest, True]]
        將每個value list按照字典序排序。
        從起點JFK開始dfs JFK可以飛抵的dest,
        設定path & valid of the ticket，backtracking
        如果ticket_cnt == 0代表可以飛完整趟
        time: O(n!) 遍歷所有排列
        """

        graph = collections.defaultdict(list)
        for ticket in tickets:
            graph[ticket[0]].append([ticket[1], True])
        for k in graph.keys():
            graph[k].sort()

        start = "JFK"
        path = [start]
        self.dfs(start, len(tickets), graph, path)
        return path

    def dfs(self, start, ticket_cnt, graph, path):
        if ticket_cnt == 0:
            return True

        for ticket in graph[start]:
            if not ticket[1]:
                continue

            dest = ticket[0]
            ticket[1] = False
            path.append(dest)
            if self.dfs(dest, ticket_cnt - 1, graph, path):
                return True
            path.pop()
            ticket[1] = True
        return False


tickets = [["JFK", "SFO"], ["JFK", "ATL"], ["SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
ans = ["JFK", "ATL", "JFK", "SFO", "ATL", "SFO"]

tickets = [["JFK", "KUL"], ["JFK", "NRT"], ["NRT", "JFK"]]
ans = ["JFK", "NRT", "JFK", "KUL"]

tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
ans = ["JFK", "MUC", "LHR", "SFO", "SJC"]


# a = Solution().findItinerary(tickets)
# print("ans:", a == ans)
