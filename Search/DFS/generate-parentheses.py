class Solution(object):
    def __init__(self):
        self.ans = []

    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        if n == 0:
            return []

        string = ""

        self.dfs(n, string, n, n)
        return self.ans

    def dfs(self, n, string, left, right):

        if left > right or left < 0 or right < 0:
            return

        if left == 0 and right == 0:
            self.ans.append(string)
            return

        for i in range(2):
            if i == 0:
                self.dfs(n, string + '(', left - 1, right)
            else:
                self.dfs(n, string + ')', left, right - 1)


a = Solution().generateParenthesis(3)
print('ans:', a)
