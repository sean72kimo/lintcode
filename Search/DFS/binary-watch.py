class Solution(object):
    def readBinaryWatch(self, num):
        """
        :type num: int
        :rtype: List[str]
        """
        if num == 0:
            return ["0:00"]

        ans = []
        total = num
        for i in range(12):
            h = self.count_bits(i)
            num -= h

            for j in range(60):

                m = self.count_bits(j)

                if num - m == 0:
                    hh = str(i)
                    if len(str(j)) == 1:
                        mm = '0' + str(j)
                    else:
                        mm = str(j)
                    time = hh + ':' + mm

                    ans.append(time)

            num += h

        return ans

    def count_bits(self, n):
        c = 0
        while n:
            if n & 0b01:
                c += 1
            n = n >> 1
        return c


class Solution(object):
    def readBinaryWatch(self, num):
        """
        :type num: int
        :rtype: List[str]
        """
        if num == 0:
            return ["0:00"]

        ans = []
        for h in range(12):
            c = self.count_bits(h)
            num -= c
            self.dfs(num, h, ans)
            num += c

        return ans

    def dfs(self, num, h, ans):

        for m in range(60):

            c = self.count_bits(m)

            if num - c == 0:
                hh = str(h)
                if len(str(m)) == 1:
                    mm = '0' + str(m)
                else:
                    mm = str(m)
                time = hh + ':' + mm

                ans.append(time)
        return ans


    def count_bits(self, n):
        c = 0
        while n:
            if n & 0b01:
                c += 1
            n = n >> 1
        return c

num = 1
a = Solution().readBinaryWatch(num)
print("ans:", a)