class Solution:
    def __init__(self):
        self.ans = []

    def findSubseq(self, s):
        vst = set()
        self.dfs(s, 0, [], vst)
        print("ans:", self.ans)

        self.ans = []
        vst = set()
        self.dfs2(s, 0, [], vst)
        print("ans:", self.ans)

    def dfs(self, s, start, path, vst):

        self.ans.append(''.join(path))

        for i in range(start, len(s)):
            if i != start and s[i] == s[i - 1]:
                continue

            path.append(s[i])
            vst.add(i)
            self.dfs(s, i + 1 , path, vst)
            path.pop()
            vst.remove(i)

    def dfs2(self, s, start, path, vst):
        if start == len(s):
            self.ans.append(''.join(path))
            return

        if start - 1 >= 0 and s[start] == s[start - 1] and start - 1 not in vst:
            return

        path.append(s[start])
        vst.add(start)
        self.dfs(s, start + 1, path, vst)
        path.pop()
        vst.remove(start)

        self.dfs(s, start + 1, path, vst)




s = "122"
a = Solution().findSubseq(s)

