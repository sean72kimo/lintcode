class Solution(object):
    def numberOfPatterns(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        skip = [[0 for i in range(10)] for j in range(10)]
        skip[1][3] = skip[3][1] = 2
        skip[1][7] = skip[7][1] = 4
        skip[3][9] = skip[9][3] = 6
        skip[7][9] = skip[9][7] = 8
        skip[1][9] = skip[9][1] = 5
        skip[2][8] = skip[8][2] = 5
        skip[3][7] = skip[7][3] = 5
        skip[4][6] = skip[6][4] = 5

        res = 0
        for i in range(m, n + 1):
            res += self.dfs({1}, skip, 1, i - 1, {}) * 4
            res += self.dfs({2}, skip, 2, i - 1, {}) * 4
            res += self.dfs({5}, skip, 5, i - 1, {})
        return res

    def dfs(self, vis, skip, cur, remain, mem):
        s = str(sorted(vis))
        if (s, cur) in mem:
            return mem[(s, cur)]

        if remain == 0:
            return 1
        if remain < 0:
            return 0

        rst = 0
        for i in range(1, 10):
            if i in vis:
                continue
            if skip[cur][i] == 0 or skip[cur][i] in vis:
                vis.add(i)
                rst += self.dfs(vis, skip, i, remain - 1, mem)
                vis.remove(i)

        mem[(s, cur)] = rst
        return rst

m = 1
n = 3
s = Solution()
a = s.numberOfPatterns(m, n)
# print(s.all_path)
print("ans:", a)


class Solution2(object):
    class Solution(object):
        def numberOfPatterns(self, m, n):
            """
            :type m: int
            :type n: int
            :rtype: int
            """
            invalid = {
                (1, 3): 2,
                (1, 7): 4,
                (1, 9): 5,
                (2, 8): 5,
                (3, 1): 2,
                (3, 7): 5,
                (3, 9): 6,
                (4, 6): 5,
                (6, 4): 5,
                (7, 1): 4,
                (7, 3): 5,
                (7, 9): 8,
                (8, 2): 5,
                (9, 1): 5,
                (9, 3): 6,
                (9, 7): 8,
            }

            self.ans = 0

            for i in range(1, 10):
                vst = set()
                self.dfs(m, n, invalid, {i}, i)
            return self.ans

        def dfs(self, m, n, invalid, vst, start):

            if len(vst) == n:
                self.ans += 1
                return

            if len(vst) >= m:
                self.ans += 1

            for i in range(1, 10):
                if i == start:
                    continue

                nxt = (start, i)
                skip = invalid.get(nxt, 0)
                if i not in vst and (nxt not in invalid or skip in vst):
                    vst.add(i)
                    self.dfs(m, n, invalid, vst, i)
                    vst.remove(i)

s = Solution2()
b = s.numberOfPatterns(m, n)

print("ans:", b ,b == a)











