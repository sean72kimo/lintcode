"""
https://www.1point3acres.com/bbs/thread-532114-1-1.html

AC
https://paste.ubuntu.com/p/C9Dk886YcQ/
sort() at line 9

WA
https://paste.ubuntu.com/p/3ZwRZSM5bS/
沒有sort 並且使用 題目給的條件 nums % nums[j] == 0 or nums[j] % nums == 0
fail在一個larget test case
大家能指點迷津嗎？
謝謝


题目说任何一对数都要满足这个关系。只有sort之后这个关系才具有传递性。
如果不sort，比如2，6，3这个case，2和6，6和3虽然都满足，但2和3不满足。
"""
class Solution_TLE(object):
    def __init__(self):
        self.ans = []

    def largestDivisibleSubset(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) < 1:
            return []


        nums.sort()
        self.dfs(nums, 0, [])
        return self.ans

    def dfs(self, nums, start, path):
        if len(path) and len(path) > len(self.ans):
            self.ans = path[:]


        for i in range(start, len(nums)):
            if len(path) == 0 or nums[i] % path[-1] == 0:
                path.append(nums[i])
                self.dfs(nums, i + 1, path)
                path.pop()


class Solution_DFS_MEM(object):
    def __init__(self):
        self.ans = []
        self.mem = {}

    def largestDivisibleSubset(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) < 1:
            return []

        nums.sort()

        rt = self.dfs(nums, 0)
        return rt

    def dfs(self, nums, start):
        if start in self.mem:
            return self.mem[start]

        res = []
        div = 1 if start == 0 else nums[start-1]

        for i in range(start, len(nums)):

            if nums[i] % div == 0:
                print(start, i)
                lst = self.dfs(nums, i + 1)[:]

                lst.append(nums[i])
                if len(lst) > len(res):
                    res = lst

        self.mem[start] = res

        return res


class Solution_DP(object):
    def largestDivisibleSubset(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) == 0:
            return []

        n = len(nums)
        f = [1 for _ in range(n)]
        p = [-1 for _ in range(n)]
        mx = 0
        index = -1
        nums.sort()
        for i in range(n):
            f[i] = 1
            p[i] = -1

            for j in range(i):
                if nums[i] % nums[j] == 0:
                    if f[j] + 1 > f[i]:
                        f[i] = f[j] + 1
                        p[i] = j

            if f[i] > mx:
                mx = f[i]
                index = i

        res = []
        while index != -1:
            res.append(nums[index])
            index = p[index]

        return res[::-1]



class Solution_WA_THINK(object):
    def __init__(self):
        self.ans = []
        self.mem = {}

    def largestDivisibleSubset(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) < 1:
            return []

        nums.sort(reverse=True)

        rt = self.dfs(nums, 0)
        return rt

    def dfs(self, nums, start):
        if start in self.mem:
            return self.mem[start]
        if start >= len(nums):
            return []

        res = []

        for i in range(start, len(nums)):

            lst = self.dfs(nums, i + 1)
            print(lst, nums[start], i)

            for itm in lst:
                if itm and nums[start] % itm[-1] == 0:
                    itm.append(nums[start])
                    res.append(itm)

        res.append( [nums[start]] )

        self.mem[start] = res

        return res


nums = [1,2,3] # [1,2] or [1,3]
nums = [1,2,4,8]
nums = [2,3,4,8]
nums = [2,3,4,8]

nums = [8,4,2]
nums =  [3,4,16,8]
nums = [2,6,3]
nums = [2, 3]

a = Solution_DP().largestDivisibleSubset(nums)
print("ans:",a)