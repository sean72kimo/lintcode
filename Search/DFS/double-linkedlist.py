from lib.dlinkedlist import *
class Soution:
    def __init__(self):
        self.ans = 0
    def findBlock(self, nums, root):
        if not root or len(nums) == 0:
            return 0
        
        myset = set(nums)

        for n in nums:
            if n not in myset:
                continue
            
            # node = self.findNode(n, root)
            # if node is None:
            #    continue
            
            self.ans += 1
            self.findNext(node, myset)
            self.findPrev(node, myset)
        
        return self.ans
    
    def findNode(self, val, root):
        while root:
            if root.val == val:
                return root
            root = root.next
        return None
    
    def findNext(self, root, myset):
        while root:
            if root.val in myset:
                myset.remove(root.val)
            else:
                return
            root = root.next

    def findPrev(self, root, myset):
        while root:
            if root.val in myset:
                myset.remove(root.val)
            else:
                return
            root = root.prev
    
dlst = [1,2,3,4,5,6]
nums = [1,2,3,5]
root = list2doublyLinkedList(dlst)
a = Soution().findBlock(nums, root)
print("ans:", a)

class Soution:
    def __init__(self):
        self.ans = 0
    def findBlock(self, nums):
        if not root or len(nums) == 0:
            return 0
        
        myset = set(nums)

        for node in nums:
            if node not in myset:
                continue
            
            if node is None:
                continue
            
            self.ans += 1
            self.findNext(node, myset)
            self.findPrev(node, myset)
        
        return self.ans
    
    def findNext(self, node, myset):
        while node:
            if node in myset:
                myset.remove(node)
            else:
                return
            node = node.next

    def findPrev(self, node, myset):
        while node:
            if node in myset:
                myset.remove(node)
            else:
                return
            node = node.prev