class Solution:
    """
    @param s: A string
    @param p: A string includes "." and "*"
    @return: A boolean
    """

    def isMatch(self, s, p):
        # write your code here
        f = [[False for i in range(len(s) + 1)] for j in range(len(p)) + 1]
        f[0][0] = True
        
        for i in range(len(s)):
            for j in range(len(p)):
                if p[j] == s[i]:
                    
                if p[j] == '.':
        return




Solution().isMatch("aa", "a")  # false
Solution().isMatch("aa", "aa")  # true
Solution().isMatch("aaa", "aa")  # false
Solution().isMatch("aa", "a*")  # true
Solution().isMatch("aa", ".*")  # true
Solution().isMatch("ab", ".*")  # true
Solution().isMatch("aab", "c*a*b")  # true


