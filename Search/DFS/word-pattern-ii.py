class Solution(object):
    def wordPatternMatch(self, pattern, s):
        """
        :type pattern: str
        :type s: str
        :rtype: bool
        """
        w2p = {}
        p2w = {}
        return self.match(pattern, s, 0, 0, w2p, p2w)

    def match(self, pattern, string, i, j, w2p, p2w):
        is_match = False

        if i == len(pattern) and j == len(string):
            return True
        if i == len(pattern) or j == len(string):
            return False

        p = pattern[i]

        if p in p2w:
            w = p2w[p]
            if w == string[j: j + len(w)]:
                is_match = self.match(pattern, string, i + 1, j + len(w), w2p, p2w)

        else:
            for k in range(j, len(string)):  # 藉由枚舉右端點的方式，取出所有substring
                w = string[j: k + 1]
                if w not in w2p:
                    w2p[w] = p
                    p2w[p] = w
                    is_match = self.match(pattern, string, i + 1, k + 1, w2p, p2w)
                    if is_match:
                        return True
                    w2p.pop(w)
                    p2w.pop(p)

                else:  # w in p2w,
                    if p in p2w and w2p[w] == p2w[p]:
                        is_match = True
                    else:
                        is_match = False  # 枚舉下一個substring，有機會修正w2p and p2w, 所以不能直接返回False

        return is_match

class Solution:
    """
    @param pattern: a string,denote pattern string
    @param str: a string, denote matching string
    @return: a boolean
    """
    def wordPatternMatch(self, pattern, string):
        # write your code here
        w2p, p2w = {}, {}
        return self.match(pattern, string, 0, 0, w2p, p2w)

    def match(self, pattern, string, i, j, w2p, p2w):
        is_match = False

        if i == len(pattern) and j == len(string):
            return True
        if i == len(pattern) or j == len(string):
            return False


        p = pattern[i]

        if p in p2w:
            w = p2w[p]
            if w == string[j : j + len(w)]:
                is_match = self.match(pattern, string, i + 1, j + len(w), w2p, p2w)

        else:
            for k in range(j, len(string)):  # enumerate all possible sub-string
                w = string[j : k + 1]
                if w not in w2p:
                    w2p[w] = p
                    p2w[p] = w
                    is_match = self.match(pattern, string, i + 1, k + 1, w2p, p2w)
                    if is_match:
                        return True
                    del w2p[w]
                    del p2w[p]


        return is_match
pattern = "abab"
string = "redblueredblue"
print(Solution().wordPatternMatch(pattern, string))
