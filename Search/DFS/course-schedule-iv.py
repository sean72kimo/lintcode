import collections
class Solution:
    """
    @param n: an integer, denote the number of courses
    @param p: a list of prerequisite pairs
    @return: return an integer,denote the number of topologicalsort
    """
    def __init__(self):
        self.ans = 0
        
    def topologicalSortNumber(self, n, p):
        # Write your code here
        ind = [0 for _ in range(n)]
        done = [False for _ in range(n)]
        graph = collections.defaultdict(set)
        
        
        for end, start in p:
            graph[start].add(end)
            ind[end] += 1

        self.dfs(0, graph, ind, done, n)
        return self.ans
        
    def dfs(self, level, graph, ind, done, n):
        if level == n:
            self.ans += 1
            return
         
        for course, pre in enumerate(ind):
            if not done[course] and pre == 0:
                done[course] = True
                for nei in graph[course]:
                    ind[nei] -= 1
                
                self.dfs(level + 1, graph, ind, done, n)
                
                done[course] = False
                for nei in graph[course]:
                    ind[nei] += 1
n = 4
p = [[1,0],[2,0],[3,1],[3,2]]
a = Solution().topologicalSortNumber(n, p)
print("ans:", a)