"""
time O(nlogn), 每層有n個factors, num每次都被除以factor(所以呈現log遞減),
space O(logn), ???
"""
import math


class Solution(object):
    def __init__(self):
        self.ans = []
        self.fac = []

    def getFactors(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        if n <= 1:
            return self.ans
        self.cla_fac(n)
        print(self.fac)

        self.dfs(n, [], 0)
        return self.ans

    def dfs(self, n, path, start):

        if n == 1:
            if len(path) > 1:
                self.ans.append(path[:])
            return


        for i in range(start, len(self.fac)):
            f = self.fac[i]
            if n % f == 0:
                path.append(f)
                self.dfs(n // f, path, i)
                path.pop()


    def cla_fac(self, n):
        for i in range(2, n):

            if n % i == 0:
                self.fac.append(i)

# a = Solution().getFactors(12)
# print("ans:", a)



class Solution3(object):
    def getFactors(self, n):
        ans, stack, x = [], [], 2
        while True:
            if x > n // x:
                print(n, x, n // x)
                if not stack:
                    return ans
                ans.append(stack + [n])
                x = stack.pop()
                n *= x
                x += 1
            elif n % x == 0:
                stack.append(x)
                n //= x
            else:
                x += 1

# a = Solution3().getFactors(12)
# print("ans:", a)


class Solution(object):
    def getFactors(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        if n < 4:
            return []

        self.ans = []
        self.dfs(n, 2, [])

        return self.ans

    def dfs(self, n, start, path):
        if n == 1:
            if len(path) > 1:
                self.ans.append(path[:])
            return

        for i in range(start, n+1):
            if n % i == 0:
                path.append(i)
                self.dfs(n // i, i, path)
                path.pop()



a = Solution().getFactors(12)
print("ans:", a)