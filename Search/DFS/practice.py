class Solution:
    """
    @param board: A list of lists of character
    @param word: A string
    @return: A boolean
    """
    def __init__(self):
        self.dxy = [[0,1],[1,0],[-1,0],[0,-1]]
        
    def exist(self, board, word):
        # write your code here
        
        n = len(board)
        m = len(board[0])
        
        for i in range(n):
            for j in range(m):
                if board[i][j] == word[0]:
                    if self.dfs(board, word, i, j, 0):
                        return True
        return False
    
    def dfs(self, board, word, i, j, idx):
        if idx == len(word):
            return True

        if i < 0 or i >= len(board) or j < 0 or j >= len(board[0]):
            return False

        if word[idx] != board[i][j]:
            return False
        
        board[i][j] = '*'
        for dx, dy in self.dxy:
            ni = i + dx
            nj = j + dy

            if self.dfs(board, word, ni, nj, idx+1):
                board[i][j] = word[idx]
                return True
        board[i][j] = word[idx]
        return False
        
        
board = [
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
word = "ABCCED"
# word = "ABCB"

board = [["a"]]
word = "a"
a = Solution().exist(board, word)
print("ans:", a)