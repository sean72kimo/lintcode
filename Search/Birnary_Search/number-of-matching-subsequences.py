import bisect
import collections
from typing import List


class Solution:
    def numMatchingSubseq(self, s: str, words: List[str]) -> int:
        mp = collections.defaultdict(list)

        for i, ch in enumerate(s):
            mp[ch].append(i)

        cnt = 0
        for word in words:
            if self.is_subseq(word, mp):
                print(word)
                cnt += 1

        return cnt

    def is_subseq(self, word, mp):
        start = 0

        for ch in word:
            if ch not in mp:
                return False

            lst = mp[ch]
            pos = bisect.bisect_left(lst, start)
            if pos == len(lst):
                return False
            start = lst[pos] + 1

        return True

s = "dsahjpjauf"
words = ["ahjpjau"]
a = Solution().numMatchingSubseq(s, words)
print("ans:", a)