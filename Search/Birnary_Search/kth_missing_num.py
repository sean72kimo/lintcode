class Solution(object):
    def kth_missing_num(self, nums, k):

        missing = self.num_missing(nums)
        print(missing)
        if missing < k:
            raise ValueError("aren't that many missing numbers")
        return self.kth_missing_in_range(nums, 0, len(nums) - 1, k)

    def kth_missing_in_range(self, nums, start, end, k):
        if start + 1 == end:
            return nums[start] + k

        mid = (start + end) // 2
        missing_on_the_left = self.num_missing(nums, start, mid)
        if missing_on_the_left >= k:
            return self.kth_missing_in_range(nums, start, mid, k)
        else:
            return self.kth_missing_in_range(nums, mid, end, k - missing_on_the_left)

    def num_missing(self, nums, start=0, end=None):
        if end is None:
            end = len(nums) - 1

        return nums[end] - nums[start] - (end - start)
        
class Solution:
    def kth_missing_num(self, nums, k):

        missed = self.num_missing(nums, 0, len(nums)-1)
        if missed < k:
            raise ValueError("not that many missing numbers")

        start, end = 0, len(nums) - 1
        while start + 1 != end:
            mid = (start + end) // 2
            missed = self.num_missing(nums, start, mid)
            if missed >= k:
                end = mid
            else:
                start = mid
                k = k - missed
        return nums[start] + k

    def num_missing(self, nums, start=0, end=None):
        return nums[end] - nums[start] - (end - start)

nums = [2,4,7,8,9,15] # missing 3,5,6,10,11,12,13,14
k = 2
a = Solution().kth_missing_num(nums, 4)
print("ans:", a)