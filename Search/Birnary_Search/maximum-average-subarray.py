class Solution:
    # @param {int[]} nums an array with positive and negative numbers
    # @param {int} k an integer
    # @return {double} the maximum average
    def maxAverage(self, nums, k):
        # Write your code here
        l, r = min(nums), max(nums)
        n = len(nums)

        while r - l >= 1e-6:
            mid = (l + r) / 2.0

            if self.check(nums, mid, k):
                l = mid
            else:
                r = mid
        return l


    def check(self, nums, mid, k):
        n = len(nums)
        prefix = [0] * (n + 1)
        min_avg = mid
        for i in range(1, n + 1):
            prefix[i] = prefix[i - 1] + nums[i - 1];
            avg = prefix[i] / i

            if i >= k and  avg >= min_avg:
                return True

            if i >= k:
                min_avg = min(avg, min_avg)
        return False

nums = [1, 12, -5, -6, 50, 3]
k = 3
ans = Solution().maxAverage(nums, k)
print('ans:', ans)
