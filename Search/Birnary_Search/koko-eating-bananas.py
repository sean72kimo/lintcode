from typing import List


class Solution:
    def minEatingSpeed(self, piles: List[int], h: int) -> int:
        if not piles:
            return -1
        if h == 0:
            return -1
        s = 1
        e = 100

        while s + 1 < e:
            m = (s + e) // 2

            hours = self.need_hours(piles, m)

            if hours <= h:
                e = m
            else:
                s = m

        need = self.need_hours(piles, s)

        if need <= h:
            return s
        return e

    def need_hours(self, piles, m):
        hours = 0
        for p in piles:
            if p % m == 0:
                hours += p // m
            else:
                hours += p // m + 1
        return hours

piles = [3,6,7,11]
h = 8
exp = 4
a = Solution().minEatingSpeed(piles, h)
print("ans:", a)