from typing import List

"""
对于左边界而言，由于我们不能「拆分」一个包裹，因此船的运载能力不能小于所有包裹中最重的那个的重量，
对于右边界而言，船的运载能力也不会大于所有包裹的重量之和
"""
class Solution:
    def shipWithinDays(self, weights: List[int], days: int) -> int:

        if not weights:
            return -1
        if days == 0:
            return -1
        s = max(weights)
        e = sum(weights)

        while s + 1 < e:
            m = (s + e) // 2
            # print(s, m, e)
            if self.need_days(weights, m) <= days:
                e = m
            else:
                s = m

        need = self.need_days(weights, s)
        # print(s, e)
        if need <= days:
            return s
        return e

    def need_days(self, weights, m):
        days = 1
        curr_weight = 0
        for w in weights:
            if curr_weight + w <= m:
                curr_weight += w
            else:
                days += 1
                curr_weight = w
        # print(days)
        return days

weights = [1,2,3,4,5,6,7,8,9,10]
days = 5
exp = 15

weights = [1,2,3,1,1]
days = 4
exp = 3
a = Solution().shipWithinDays(weights, days)
print("ans:", a)
