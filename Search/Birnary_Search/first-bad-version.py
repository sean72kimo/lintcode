class Solution:
    def firstBadVersion(self, n: int) -> int:
        start = 0
        end = n

        while start + 1 < end:
            mid = (start + end) // 2

            if isBadVersion(mid):
                end = mid
            else:
                start = mid

        if isBadVersion(start):
            return start
        return end