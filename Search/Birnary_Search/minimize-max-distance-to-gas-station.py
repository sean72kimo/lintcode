class Solution(object):
    def minmaxGasDist(self, stations, K):
        """
        :type stations: List[int]
        :type K: int
        :rtype: float
        """
        start = 0
        end = 10**8
        eps = 1e-6
        while end - start > eps:
            mid = (start + end) / 2.0
            if self.possible(stations, K, mid):
                end = mid
            else:
                start = mid

        return start

    def possible(self, stations, k, mid):
        print(mid)
        cnt = 0
        for i in range(len(stations)- 1):
            c = int((stations[i + 1] - stations[i]) / mid)
            cnt += c

        return cnt <= k


stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
K = 9
a = Solution().minmaxGasDist(stations, K)
print("ans:", a)

from fractions import Fraction
print(Fraction(3,5))