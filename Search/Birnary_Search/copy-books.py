
"""
437. Copy Books
https://www.lintcode.com/problem/copy-books/description
二分答案，猜測總共最少需要多少時間
檢查猜出來的時間是否能夠完成抄寫
"""


class Solution:
    """
    @param pages: an array of integers
    @param k: An integer
    @return: an integer
    """

    def copyBooks(self, pages, k):
        # write your code here
        if len(pages) == 0:
            return 0
        s = max(pages)
        e = sum(pages)

        while s + 1 < e:
            m = (s + e) // 2
            if self.checkAns(pages, k, m):
                e = m
            else:
                s = m
        if self.checkAns(pages, k, s):
            return s
        return e

    def checkAns(self, pages, k, m):
        count = 0
        time_cost = 0
        for page in pages:
            if page > m:
                return False

            if time_cost + page > m:# 當前和之前所有頁數正好等於一個抄寫員的工作量
                count += 1          # 加一個人
                time_cost = 0       # 新人的工作量從0開始計算

            time_cost += page

        if time_cost > 0: # 有頁數，需要一個抄寫員
            count += 1

        # 抄寫員的數量若是少於k, 代表可以更快抄完
        return count <= k

class Solution:
    """
    @param: pages: an array of integers
    @param: k: An integer
    @return: an integer
    """
    def copyBooks(self, pages, k):
        # write your code here
        n = len(pages)

        # best case, n books, n copier, each one copies one book,
        # the longest one is the one copying the thinckest book
        s = max(pages)
        # worst case, one copier has to copy all books
        e = sum(pages)

        while s + 1 < e:
            m = s + (e - s) // 2
            cnt = self.countCopiers(pages, m)
            print('num of copier', cnt)
            if  cnt > k:
                s = m
            else:
                e = m

        if self.countCopiers(pages, s) <= k:
            return s
        return e

    def countCopiers(self, pages, limit):
        copier = 1
        time = pages[0]
        if len(pages) == 0:
            return 0

        for i in range(1, len(pages)):
            if time + pages[i] > limit:
                copier += 1
                time = 0
            time = time + pages[i]

        return copier

# pages = [1, 2]
# copiers = 5
# a = Solution().copyBooks(pages, copiers)
# print('ans:', a)




pages = [1, 2]
copiers = 5

pages = [3,2,4]
copier = 2
exp = 5
a = Solution().copyBooks(pages, copiers)
print('ans:', a)




