class Solution2(object):
    def singleNonDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        數個數
        維持右半邊為對對出現
        tricky! 如果已經知道當前 nums[m] == nums[m-1] 為一個對子，那麼將 m += 1, 去看看 m 到 len(nums)是否為對對出現
        """
        if len(nums) == 0:
            return

        start = 0
        end = len(nums) - 1

        while start + 1 < end:
            mid = (start + end) // 2
            if nums[mid] == nums[mid - 1]:
                mid += 1

            right = len(nums) - mid
            if right % 2 == 0:
                end = mid
            else:
                start = mid

        if start == 0:
            return nums[start]

        if end == len(nums) - 1:
            return nums[end]

        if nums[start] == nums[start - 1]:
            return nums[end]

        return nums[start]


nums = [1, 1, 2, 2, 3, 3, 4, 8,8]

# print('ans:', Solution2().singleNonDuplicate(nums))
