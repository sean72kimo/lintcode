class Solution:
    """
    @param: A: an integer sorted array
    @param: target: an integer to be inserted
    @return: a list of length 2, [index1, index2]
    """
    def searchRange(self, A, target):
        # write your code here
        if len(A) == 0 or target is None:
            return [-1, -1]

        start = 0
        end = len(A) - 1
        bound = [None, None]

        while start + 1 < end:
            mid = int((start + end) / 2)
            if A[mid] == target:
                end = mid
            elif A[mid] < target:
                start = mid
            else:
                end = mid

        if A[start] == target:
            left = start
        elif A[end] == target:
            left = end
        else:
            left = -1


        start = 0
        end = len(A) - 1
        while start + 1 < end:
            mid = int((start + end) / 2)
            if A[mid] == target:
                start = mid
            elif A[mid] < target:
                start = mid
            else:
                end = mid

        if A[end] == target:
            right = end
        elif A[start] == target:
            right = start
        else:
            right = -1

        return [left, right]




A = [5, 7, 7, 8, 8, 10]
target = 8
A = []
target = 9
print('and:', Solution().searchRange(A, target))
