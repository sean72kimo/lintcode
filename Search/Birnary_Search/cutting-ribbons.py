import math


class Solution(object):
    def maxLength(self, ribbons, k):
        """
        :type ribbons: List[int]
        :type k: int
        :rtype: int
        """
        # write your code here
        if len(ribbons) == 0:
            return 0

        e = max(ribbons)
        s = 1
        while s + 1 < e:
            m = (s + e) // 2
            valid = self.is_valid(ribbons, k, m)
            if valid:
                s = m
            else:
                e = m

        if self.is_valid(ribbons, k, e):
            return e

        if self.is_valid(ribbons, k, s):
            return s

        return 0

    def is_valid(self, ribbons, k, m):
        total_pieces = 0
        for ribbon in ribbons:
            pieces = ribbon // m
            total_pieces += pieces
            if total_pieces >= k:
                return True
        return False

r = [7,5,9]
k = 4
e = 4

# r = [5, 7, 9]
# k = 22
# e = 0

# r = [9,7,5]
# k = 3
# e = 5

# r = [100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,1,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000]
# k = 49
# e = 100000


r = [100000,1,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000,100000]
k = 50
e = 50000
a = Solution().maxLength(r, k)
print("ans:", a, a == e)



import collections


class TrieNode:
    def __init__(self, word=""):
        self.is_word = False
        self.children = {}
        self.word = word


class Trie:
    def __init__(self):
        self.root = TrieNode()

    def add(self, word):
        node = self.root

        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt
        node.is_word = True
        node.word = word

    def search(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                return False
            node = nxt
        return node.is_word


class Solution:
    def addBoldTag(self, s, words) -> str:
        trie = Trie()

        for word in words:
            trie.add(word)

        n = len(s)
        mark = [False for _ in range(n)]
        for i in range(n):
            for j in range(i, n):
                sub = s[i:j + 1]
                bold = trie.search(sub)
                if not bold:
                    continue
                for k in range(i, j + 1):
                    mark[k] = True


        interval = []
        print(mark)
        mark.append(False)
        que = collections.deque([])
        j = 0
        while j < len(mark):
            try:
                i = mark.index(True, j)
                j = i
            except:
                break
            while mark[j]:
                j += 1

            que.append([i, j-1])

        print(que)
        ans = []
        for i in range(n):

            if len(que) and i == que[0][0]:
                if len(ans) and ans[-1] == "</b>":
                    continue
                ans.append("<b>")

            ans.append(s[i])

            if len(que) and i == que[0][1]:
                ans.append("</b>")
                que.popleft()
        return ''.join(ans)



class Solution:
    def addBoldTag(self, s, dict):
        """
        :type s: str
        :type dict: List[str]
        :rtype: str
        """
        status = [False]*len(s)
        final = ""
        for word in dict:
            start = s.find(word)
            last = len(word)
            while start != -1:
                for i in range(start, last+start):
                    status[i] = True
                start = s.find(word,start+1)
        print(status)
        i = 0
        while i < len(s):
            if status[i]:
                final += "<b>"
                while i < len(s) and status[i]:
                    final += s[i]
                    i += 1
                final += "</b>"
            else:
                final += s[i]
                i += 1
        return final

s = "abcxyz123"
words = ["abc","123"]
e = "<b>abc</b>xyz<b>123</b>"

# s = "aaabbcc"
# words = ["aaa","aab","bc"]
# e ="<b>aaabbc</b>c"

# s ="abcdef"
# words = ["a","c","e","g"]
# e = "<b>a</b>b<b>c</b>d<b>e</b>f"

a = Solution().addBoldTag(s, words)
print("ans: ", a, a == e)
print(math.sqrt(4))