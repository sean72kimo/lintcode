"""
mlogn
"""
class Solution:
    def leftMostColumnWithOne(self, binaryMatrix: 'BinaryMatrix') -> int:

        m, n = binaryMatrix.dimensions()
        ans = float('inf')
        for i in range(m):
            j = self.binary_search(binaryMatrix, i)
            ans = min(ans, j)
        if ans == float('inf'):
            return -1
        return ans

    def binary_search(self, binaryMatrix, i):
        m, n = binaryMatrix.dimensions()

        s = 0
        e = n - 1

        while s + 1 < e:
            m = (s + e) // 2
            v = binaryMatrix.get(i, m)
            if v:
                e = m
            else:
                s = m

        v = binaryMatrix.get(i, s)
        if v:
            return s
        v = binaryMatrix.get(i, e)
        if v:
            return e
        return float('inf')

"""
m+n
"""
class Solution2:
    def leftMostColumnWithOne(self, binaryMatrix: 'BinaryMatrix') -> int:

        m, n = binaryMatrix.dimensions()
        i = m - 1
        j = n - 1
        ans = -1
        while i >= 0 and j >= 0:
            v = binaryMatrix.get(i, j)
            if v == 0:
                i -= 1
            else:
                ans = j
                j -= 1

        return ans