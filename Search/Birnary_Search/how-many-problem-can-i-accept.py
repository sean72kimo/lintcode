import math
class Solution:
    """
    @param n: an integer
    @param k: an integer
    @return: how many problem can you accept
    """

    def canAccept(self, n, k):
        # Write your code here

        left = 1
        right = math.sqrt(2*n)
        print(left,  right)
        while left + 1 < right:
            mid = (right - left) // 2 + left
            print(left, '/', mid, '/',right)
            if self.canFinish(n, k, mid):
                left = mid
            else:
                right = mid
         
        if self.canFinish(n, k, right):
            return right
        return left
         
    def canFinish(self, n, k, p):
        
        total = (((1 + p)*p) // 2) * k
        if total <= n:
            return True
        else:
            return False
n = 30
k = 1
Solution().canAccept(n, k)