from typing import (
    List,
)


class Solution:
    """
    @param l: Given n pieces of wood with length L[i]
    @param k: An integer
    @return: The maximum length of the small pieces
    """

    def wood_cut(self, l: List[int], k: int) -> int:
        # write your code here
        if not l:
            return 0

        start = 1
        end = max(l)

        while start + 1 < end:
            mid = (start + end) // 2

            if self.more_than_k(l, k, mid):
                start = mid
            else:
                end = mid

        if self.more_than_k(l, k, end):
            return end
        if self.more_than_k(l, k, start):
            return start
        return 0

    def more_than_k(self, l, k, mid):
        total = 0
        for log in l:
            pieces = log // mid
            total += pieces
        return total >= k


l = [232, 124, 456]
k = 7
exp = 114


l = [1, 2, 3]
k = 7
exp = 0

sol = Solution()
ans = sol.wood_cut(l, k)
print("ans= ", ans)

