from typing import List


class Solution:
    def findKthPositive(self, nums: List[int], k: int) -> int:

        s = 0
        e = len(nums) - 1

        while s + 1 < e:
            m = (s + e) // 2
            if self.missing(nums, m) < k:
                s = m
            else:
                e = m

        missing_count_at_index_0 = (nums[0] - 1 + 1) - (0 + 1)
        print(self.missing(nums, e))
        print(self.missing(nums, s))
        if self.missing(nums, e) < k:
            tmp = nums[e] + k - self.missing(nums, e)
            return tmp - missing_count_at_index_0
        else:
            tmp = nums[s] + k - self.missing(nums, s)
            return tmp - missing_count_at_index_0

    def missing(self, nums, idx):
        return nums[idx] - nums[0] + 1 - (idx + 1)

nums = [2]
k = 1

a = Solution().findKthPositive(nums, k)
print("ans:", a)