import bisect
import random



class Solution:

    def __init__(self, rects):
        self.rects, self.ranges, sm = rects, [], 0
        for x1, y1, x2, y2 in rects:
            sm += (x2 - x1 + 1) * (y2 - y1 + 1)
            self.ranges.append(sm)

    def pick(self):
        r = random.randint(1, self.ranges[-1])
        i = bisect.bisect_left(self.ranges, r)
        x1, y1, x2, y2 = self.rects[i]
        return [random.randint(x1, x2), random.randint(y1, y2)]
# Your Solution object will be instantiated and called as such:
# obj = Solution(rects)
# param_1 = obj.pick()


class Solution2(object):

    def __init__(self, rects):
        """
        :type rects: List[List[int]]
        """
        self.rects = rects
        self.ranges =[0]
        sm = 0
        for x1, y1, x2, y2 in rects:
            sm += (x2 - x1 + 1) * (y2 - y1 + 1)
            self.ranges.append(sm)
        print(self.ranges)

    def pick(self):
        """
        :rtype: List[int]
        """
        n = random.randint(0, self.ranges[-1] - 1)
        i = bisect.bisect_right(self.ranges, n)
        x1, y1, x2, y2 = self.rects[i - 1]
        n -= self.ranges[i - 1]
        return [x1 + n % (x2 - x1 + 1), y1 + n // (x2 - x1 + 1)]


ops = ["Solution","pick","pick","pick","pick","pick"]
val = [[[[-2,-2,-1,-1],[1,0,3,0]]],[],[],[],[],[]]
# exp = [None,0,9,4,2,None,5]
ans = []
for i, op in enumerate(ops):
    if op == "Solution":
        obj = Solution(val[i][0])
        ans.append(None)

    elif op == "pick":
        rt = obj.pick()
        ans.append(rt)

print(ans)