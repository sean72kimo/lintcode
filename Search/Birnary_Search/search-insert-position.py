class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        s = 0
        e = len(nums) - 1

        while s + 1 < e:
            # m = (e - s) // 2 + s
            m = (s + e) // 2
            v = nums[m]
            print(s, e)
            if v == target or v > target:
                e = m
            else:
                s = m

        if nums[s] == target:
            return s

        if nums[s] > target:
            return s

        if nums[e] == target:
            return e

        if nums[e] < target:
            return e + 1

        if nums[e] > target:
            return e
nums = [1,3,5,6]
target = 5
a = Solution().searchInsert(nums, target)
print("ans:", a)

