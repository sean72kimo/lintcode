class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """

        if len(matrix) == 0 or len(matrix[0]) == 0:
            return False

        n = len(matrix)
        m = len(matrix[0])

        s = 0
        e = n * m - 1

        while s + 1 < e :
            mid = (s + e) / 2
            x = mid // m
            y = mid % m

            if matrix[x][y] == target:
                return True

            if matrix[x][y] < target:
                s = mid
            else:
                e = mid

        x = s // m
        y = s % m
        if matrix[x][y] == target:
            return True

        x = e // m
        y = e % m
        if matrix[x][y] == target:
            return True

        return False
nums = [[]]
target = 1
Solution().searchMatrix(nums, target)
