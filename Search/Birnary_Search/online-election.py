import bisect
import collections


class TopVotedCandidate(object):

    def __init__(self, persons, times):
        """
        :type persons: List[int]
        :type times: List[int]
        維持一個arr按照時間升序排列。每個element為 (t, person with most_vote)
        因為arr是按照時間升序，所以可以快速二分查找某個時間點並得到那個時間點的最高得票
        i = bisect.bisect_right(self.A, (t, float('inf'))
        float('inf')代表person, 若時間相同，則查找較晚被投票的那位
        return self.A[i-1][1]

        zip and loop persons, times array
        用counter 統計當前時間t 某個人p得幾票
        如果票數大於目前最高票，代表當前時間t，這個人p得票最高, 將此(t, person)加入self.A

        """
        self.A = []
        counter = collections.Counter()
        leader = None
        most_vote = 0

        for p, t in zip(persons, times):
            counter[p] += 1
            vote = counter[p]

            if vote >= most_vote:  # >= , 因為後得票者算領先
                if p != leader:
                    leader = p
                    self.A.append((t, leader))

                most_vote = vote

    def q(self, t):
        """
        :type t: int
        :rtype: int
        """
        i = bisect.bisect_right(self.A, (t, float('inf')))
        return self.A[i - 1][1]

# Your TopVotedCandidate object will be instantiated and called as such:
# obj = TopVotedCandidate(persons, times)
# param_1 = obj.q(t)

ops = ["TopVotedCandidate","q","q","q","q","q","q"]
val = [[[0,1,1,0,0,1,0],[0,5,10,15,20,25,30]],[3],[12],[25],[15],[24],[8]]
exp = [None,0,1,1,0,0,1]

ans = []
for i, op in enumerate(ops):

    if op == "TopVotedCandidate":
        obj = TopVotedCandidate(val[0][0], val[0][1])
        ans.append(None)
    elif op == "q":
        ans.append(obj.q(val[i][0]))

print("ans:", ans == exp, ans)
