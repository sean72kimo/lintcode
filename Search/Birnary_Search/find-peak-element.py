class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0 :
            return
        n = len(nums)
        s = 0
        e = n - 1

        while s + 1 < e:
            m = (s + e) // 2


            # peak
            if nums[m - 1] < nums[m] and nums[m] > nums[m + 1]:
                return m

            # bottom
            if nums[m - 1] > nums[m] and nums[m] < nums[m + 1]:
                e = m
            elif nums[m] < nums[m + 1]:
                s = m
            elif nums[m] > nums[m + 1]:
                e = m

        print(s, m, e)
        if nums[e] > nums[s]:
            return e
        else:
            return s

nums = [2, 1, 2]
# Solution().findPeakElement(nums)



class Solution2:
    """
    @param: A: An integers array.
    @return: return any of peek positions.
    """
    def findPeakElement(self, A):
        # write your code here

        n = len(A)
        if n == 0:
            return

        s = 0
        e = n - 1

        while s + 1 < e:
            m = s + (e - s) // 2
            if A[m - 1] < A[m] > A[m + 1]:
                return m

            if  A[m] < A[m - 1] :
                e = m
            elif  A[m] > A[m - 1] :
                s = m
            elif A[m - 1] > A[m] < A[m + 1]:
                e = m

        if A[s] < A[e]:
            return e
        else:
            return s


nums = [1, 2, 1, 3, 4, 5, 7, 6]
a = Solution2().findPeakElement(nums)
print("ans:", a)
