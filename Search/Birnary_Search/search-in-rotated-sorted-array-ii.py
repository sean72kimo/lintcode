class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: bool
        """

        if nums is None or len(nums) == 0:
            return False

        n = len(nums)

        start = 0
        end = n - 1

        while start + 1 < end:
            mid = start + (end - start) // 2
            print(start, mid, end)
            if nums[mid] == target:
                return True

            if nums[start] < nums[mid]:
                if nums[start] <= target <= nums[mid]:
                    end = mid
                else:
                    start = mid
            elif nums[start] > nums[mid]:
                if nums[mid] <= target <= nums[end]:
                    start = mid
                else:
                    end = mid
            else:
                start += 1

        if nums[start] == target:
            return True

        if nums[end] == target:
            return True

        return False

nums = [1, 1, 3, 1]
target = 3
a = Solution().search(nums, target)
print("ans:", a)
