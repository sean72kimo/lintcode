

# https://www.youtube.com/watch?v=G2UlzuEQu5E
# https://www.youtube.com/watch?v=fWS0TCcr-lE
class Solution(object):

    def __init__(self, w):
        """
        :type w: List[int]
        """

        self.n = len(w)
        self.s = [w[0]]

        for i in range(1, len(w)):
            self.s.append(self.s[-1] + w[i])

    def pickIndex(self):
        """
        :rtype: int
        """
        r = random.randint(1, self.s[-1])
        start = 0
        end = len(self.s) - 1

        while start + 1 < end:
            mid = (start + end) // 2
            if self.s[mid] > r:
                end = mid
            elif self.s[mid] < r:
                start = mid
            else:
                end = mid

        if self.s[start] < r:
            return end
        return start


w = [1, 3, 2]
obj = Solution(w)
# param_1 = obj.pickIndex()
import random
print(random.random())
