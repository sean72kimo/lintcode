
"""
1. 找到最左邊開頭的target
1. 找到最右邊結尾的target
"""
class Solution(object):
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        if len(nums) == 0:
            return [-1, -1]

        ans = []

        s = 0
        e = len(nums) - 1

        while s + 1 < e:
            m = (s + e) // 2

            if nums[m] >= target:
                e = m
            elif nums[m] < target:
                s = m

        if nums[s] == target:
            ans.append(s)
        elif nums[e] == target:
            ans.append(e)
        else:
            return [-1, -1]

        s = 0
        e = len(nums) - 1

        while s + 1 < e:
            m = (s + e) // 2

            if nums[m] > target:
                e = m
            elif nums[m] <= target:
                s = m

        if nums[e] == target:
            ans.append(e)
        elif nums[s] == target:
            ans.append(s)

        return ans