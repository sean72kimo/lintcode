from typing import List
# https://xingxingpark.com/Leetcode-1060-Missing-Element-in-Sorted-Array/

class Solution:
    def missingElement(self, nums: List[int], k: int) -> int:
        s = 0
        e = len(nums) - 1

        while s + 1 < e:
            m = (s + e) // 2
            if self.missing(nums, m) < k:
                s = m

            else:
                e = m

        if self.missing(nums, e) < k:
            return nums[e] + k - self.missing(nums, e)
        else:
            return nums[s] + k - self.missing(nums, s)

    def missing(self, nums, idx):
        # 1. nums[idx] - nums[0] + 1 閉區間 [nums[idx] , nums[0]] 之間，完整的情況，有幾個數字？
        # 2. [0, idx] 有幾個數字？ 代表實際上 nums 只有能裝得下(idx+1)個數字
        # 所以1和2中間消失了missing nums[idx] - nums[0] + 1 - (idx + 1)這麼多數字
        return nums[idx] - nums[0] + 1 - (idx + 1)