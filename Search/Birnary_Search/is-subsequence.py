import collections
import bisect
"""
curr 代表走到 t 的哪個位置
bisect_left, 在當前這些idx裡面找，看看curr會走到哪
如果curr 要走到 bisect_left最末 == len(lst)
代表curr 走的比所有當前這些idx還要遠，
s = abbb
t = abbxy
b = [1,2]
curr 走到 3了，但是想要找找有沒有letter b -> 找不到了 
return False

curr = lst[j] + 1
代表curr 移動到 t 字串的  lst[j] + 1這個位置
"""

class SolutionBisect:
    def isSubsequence(self, s: str, t: str) -> bool:

        mp = collections.defaultdict(list)
        for i, ch in enumerate(t):
            mp[ch].append(i)

        curr = 0
        for i, ch in enumerate(s):
            lst = mp[ch]

            j = bisect.bisect_left(lst, curr)

            if j == len(lst):
                return False
            curr = lst[j] + 1

        return True


class SolutionBinarySearch(object):
    def isSubsequence(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        positions = collections.defaultdict(list)
        for i, ch in enumerate(t):
            positions[ch].append(i)
        print(positions)
        i = 0
        for ch in s:
            j = bisect.bisect_left(positions[ch], i)
            print("===", ch, j)
            k = self.find_pos(positions[ch], i)
            if j != k:
                print("!!!!", j, k)
            if j == len(positions[ch]):
                return False
            i = positions[ch][j] + 1
        return True

    def find_pos(self, nums, i):
        if len(nums) == 0:
            return 0
        if len(nums) == 1:
            return 0 if nums[0] >= i else 1
        print(nums, i)
        s = 0
        e = len(nums) - 1
        while s + 1 < e:
            m = (s + e) // 2

            if nums[m] > i:
                e = m
            elif nums[m] < i:
                s = m
            else:
                return m - 1

        if nums[s] >= i:
            return s
        return e

s = "acb"
t = "ahbgdc"

# s = "axc"
# t = "ahbgdc"
#
s = "acb"
t = "ahbgdc"
# a = SolutionBisect().isSubsequence(s, t)
b = SolutionBinarySearch().isSubsequence(s, t)
# print("ans:", a, b)

j = bisect.bisect_left([2], 6)
print(j)


class Solution_linear(object):
    def isSubsequence(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if len(t) == 0 and len(s) == 0:
            return True
        if len(t) == 0:
            return False

        i = j = 0
        while i < len(t) and j < len(s):
            if t[i] == s[j]:
                i += 1
                j += 1
            else:
                i += 1

        return j == len(s)