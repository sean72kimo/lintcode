class Solution(object):
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return -1

        s = 1
        e = len(nums) - 1

        while s + 1 < e:
            m = (s + e) // 2
            cnt = self.cnt_of_le_target(nums, m)
            if cnt <= m:
                s = m
            else:
                e = m

        if self.cnt_of_le_target(nums, s) <= s:
            return e
        return s

    def cnt_of_le_target(self, nums, m):
        cnt = 0
        for n in nums:
            if n <= m:
                cnt += 1
        return cnt