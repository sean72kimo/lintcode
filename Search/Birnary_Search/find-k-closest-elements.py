from functools import cmp_to_key


class Solution:
    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        # 將arr切成兩半，右半都是大於等於x，左半都是小於x
        right = bisect.bisect_left(arr, x)
        left = right - 1

        # 求K個。left, right不包含，中間夾著k個數，就是答案 arr[left + 1 : right]
        for _ in range(k):
            if left < 0:
                right += 1
            elif right >= len(arr):
                left -= 1
            elif x - arr[left] <= arr[right] - x:
                left -= 1
            else:
                right += 1

        return arr[left + 1:right]


class Solution:
    """
    Note: 2022之後過不了了。此法離開while loop之後的最後一次比較有問題。

    目標：尋找subarray, size k, subarry 每個元素最靠近x
    用二分法尋找target arr 左端點

    s....m...x....m+k...n-k
    為一個可能的左端點
    另外一個可能的左端點 m+k
    m 和 m+k

    尋找subarray arr[m:m+k]左端點。每次比較 target與arr[m] and arr[m+k] 兩個左端點哪個較近。
    關鍵在：當diff(arr[m], target) == diff(arr[m+k], target)應該是end往mid移動，
    若是我們將start往mid移動，代表我們想取的是arr[m+k: m+2k], 但我們的算法是漸進式的從左往右推移subarry, 因此跳過了正確答案

    離開 while loop之後的剩下arr[s], arr[e]選擇
    我們執行同樣於loop裡面的判斷方法，用abs(x-arr[s]) vs abs(x-arr[s+k])去比較，
    題目說，若是兩個相同，取偏小的那邊

    corner case: k >= len(arr) ->取整個array

    """

    def findClosestElements_logn(self, arr, k, x):
        """
        :type arr: List[int]
        :type k: int
        :type x: int
        :rtype: List[int]
        """
        if k >= len(arr):
            return arr

        s = 0
        e = len(arr) - k

        while s + 1 < e:
            m = (s + e) // 2

            if abs(x - arr[m]) > abs(x - arr[m + k]):
                s = m
            else:
                e = m

        if abs(x - arr[s]) > abs(x - arr[s + k]):
            return arr[e : e + k]
        return arr[s : s + k]

    def findClosestElements_linear(self, arr, k, x):
        if arr is None or len(arr) == 0:
            return arr

        while len(arr) > k:
            if abs(arr[0] - x) > abs(arr[-1] - x):
                arr.pop(0)
            else:
                arr.pop(-1)

        return arr

    def findClosestElements_nlogn(self, arr, k, x):
        """
        :type arr: List[int]
        :type k: int
        :type x: int
        :rtype: List[int]
        """

        def mykey(a):
            print(a, abs(a - x))
            return abs(a - x)

        arr.sort(key=mykey)
        print(arr[:k])
        return sorted(arr[:k])


arr = [1, 2, 3, 4, 5]
k = 4
x = 3

arr = [0, 0, 1, 2, 3, 3, 4, 7, 7, 8]
k = 3
x = 5

arr = [1, 2, 3, 4, 5, 6, 7]
k = 3
x = 3
a = Solution().findClosestElements_logn(arr, k, x)
print("ans:", a)

