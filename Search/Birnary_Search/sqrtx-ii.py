class Solution:
    """
    @param: x: a double
    @return: the square root of x
    用二分法求值
    eps為誤差
    下界為 0，上界為 x
    如果 0 < target < 1，則上界取 1.0
    e = max(x, 1.0)
    """
    def sqrt(self, x):
        # write your code here

        s = 0.0
        e = max(x, 1.0)
        eps = 1e-12

        while s + eps < e:
            m = (e + s) / 2
            if m * m < x:
                s = m
            elif m * m > x:
                e = m
            else:
                return m

        if e ** 2 < x:
            return e
        return s
