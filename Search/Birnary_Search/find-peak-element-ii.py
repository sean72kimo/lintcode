class Solution:
    """
    @param: A: An integer matrix
    @return: The index of the peak
    """
    def findPeakII(self, A):
        # write your code here
        if len(A) == 0:
            return [-1, -1]

        n = len(A)
        m = len(A[0])

        start = 0
        end = n - 1


        def find(row):
            col = 0
            for j in range(m):
                if A[row][col] < A[row][j]:
                    col = j
            return col


        ans = []
        while start + 1 < end:
            mid = start + (end - start) // 2

            # c = find(mid)
            col = A[mid].index(max(A[mid]))

            if A[mid - 1][col] < A[mid][col] > A[mid + 1][col]:
                ans.extend([mid, col])
                return ans

            if A[mid][col] < A[mid - 1][col]:
                end = mid
            elif A[mid][col] > A[mid - 1][col]:
                start = mid
            elif A[mid - 1][col] > A[mid][col] < A[mid + 1][col]:
                end = mid

        if A[start][col] > A[end][col]:
            ans.extend([start, col])
        else:
            ans.extend([end, col])

        return ans


A = [
  [1 , 2 , 3 , 6 , 5],
  [16, 41, 23, 22, 6],
  [15, 17, 24, 21, 7],
  [14, 18, 19, 20, 10],
  [13, 14, 11, 10, 9]
]



ans = Solution().findPeakII(A)
print('ans:', ans)

