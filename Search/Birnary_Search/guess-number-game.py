# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# you can call Guess.guess(num)

class Solution:
    # @param {int} n an integer
    # @return {int} the number you guess
    def guessNumber(self, n):
        # Write your code here

        # nums = [i for i in range(1, n+1)]
        start = 1
        end = n


        while start + 1 < end:
            mid = int((start + end) / 2)
            compare = Guess.guess(mid)

            if compare == 0:
                end = mid
            elif compare == -1:  # target < mid
                end = mid
            else:  # target > mid
                start = mid


        if Guess.guess(start) == 0:
            return start
        elif Guess.guess(end) == 0:
            return end
        else:
            return -1
