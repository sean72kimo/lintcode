class Solution_logn(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        time O(logn)
        space O(1)
        1.比O(n) time更好的解法只有 O(logn)，那麼就是將 T(n) = T(n/2) + O(1)， 在O(1) time將規模由n降為 n/2
        2.找第k個數，變成找第 k/2個數，那就得扔掉k/2個數
        3.half = k/2 準備扔掉 k/2個數，index = half-1
        4.nums1[index] or nums2[index]哪個較小? 則從較小的那個nums數列中numsX中扔掉k/2個數，
        .可以保證第k個數仍然保留在nums1 or nums2裡
        """
        m = len(nums1)
        n = len(nums2)
        t = m + n
        ans = None

        if t % 2 == 1:
            k = t // 2 + 1
            return self.helper(nums1, nums2, k) / 1.0
        else:
            k = t // 2
            small = self.helper(nums1, nums2, k)
            big = self.helper(nums1, nums2, k + 1)
            return (small + big) / 2.0

    def helper(self, nums1, nums2, k):
        if len(nums1) == 0:
            return nums2[k - 1]
        if len(nums2) == 0:
            return nums1[k - 1]
        if k == 1:
            return min(nums1[0], nums2[0])

        half = k // 2
        # print(half)

        a, b = None, None
        if len(nums1) >= half:
            a = nums1[half - 1]
        if len(nums2) >= half:
            b = nums2[half - 1]

        if (a and a < b) or b is None:
            return self.helper(nums1[half:], nums2, k - half)
        else:
            return self.helper(nums1, nums2[half:], k - half)


nums1 = [1, 2, 3]
nums2 = [4, 5, 6, 7]
a = Solution_logn().findMedianSortedArrays(nums1, nums2)
print("ans:", a)
class Solution_n(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float

        O(n) time
        O(1) space
        1.因為 nums1 & nums2 已經排好序了，直接比較 nums1 nums2最後一個數字，較大的扔出
        2.不斷的扔出nums1 or nums2最後一個數字，直到第k個數

        """
        m = len(nums1)
        n = len(nums2)
        t = m + n
        ans = None
        if t % 2 == 1:
            k = t // 2 + 1
            for i in range(k):
                ans = self.helper(nums1, nums2) * 1.0
        else:
            k = t // 2
            for i in range(k):
                big = self.helper(nums1, nums2)
            small = self.helper(nums1, nums2)
            ans = (big + small) / 2.0

        return ans

    def helper(self, nums1, nums2):
        v = None

        if nums1 and nums2:
            if nums1[-1] > nums2[-1]:
                return nums1.pop()
            else:
                return nums2.pop()

        if nums1:
            return nums1.pop()

        if nums2:
            return nums2.pop()

        return v

# nums1 = [1]
# nums2 = []
# a = Solution_n().findMedianSortedArrays(nums1, nums2)
# print("ans:", a)
