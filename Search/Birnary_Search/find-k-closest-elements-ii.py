"""
lintcode 460. Find K Closest Elements
https://www.lintcode.com/problem/find-k-closest-elements/
用二分找到距離target最靠近的數
利用s, e兩個pointer依次比較，向外擴展，找到k的最接近target的數
技巧: 如果s, e pointer超過邊界，設diff 為 None (不可設為0, diff = 0代表該數等於target)
l_diff = abs(A[s] - target) if s >= 0 else None
r_diff = abs(A[e] - target) if e <= len(A)-1 else None
判斷diff是否存在要用 if diff is not None ，不可以用 if not diff
"""


class Solution:
    """
    @param A: an integer array
    @param target: An integer
    @param k: An integer
    @return: an integer array
    """

    def kClosestNumbers(self, A, target, k):
        # write your code here
        if k > len(A):
            return A
        s, e = 0, len(A) - 1
        while s + 1 < e:
            mid = (s + e) // 2
            if A[mid] < target:
                s = mid
            else:
                e = mid

        ans = []
        while len(ans) != k:
            l_diff = abs(A[s] - target) if s >= 0 else None
            r_diff = abs(A[e] - target) if e <= len(A)-1 else None

            if r_diff is not None and l_diff is not None:
                if r_diff < l_diff:
                    ans.append(A[e])
                    e += 1
                else:
                    ans.append(A[s])
                    s -= 1
            elif r_diff is not None:
                ans.append(A[e])
                e += 1
            elif l_diff is not None:
                ans.append(A[s])
                s -= 1
            else:
                break

        return ans

A, target, k, exp = [1,4,6,8], 3, 3, [4,1,6]
a = Solution().kClosestNumbers(A, target, k)
print(a)