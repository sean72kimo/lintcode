from copy import deepcopy
class Solution:
    # @param s, a string
    # @return a list of lists of string
    res = []
    def isPalindrome(self, string):
        return string == string[::-1]
        # length = len(string)
        # for i in range(0, length):
        #    if string[i] == string[length - 1 - i]:
        #        pass
        #        # print(string[i] , string[length - 1 - i])
        #    else:
        #        return False
        # return True

    def partition(self, s):
        # write your code here
        if not s:
            self.res.append([])
            return self.res
        self.dfs(s, 0, [])
        print('ans:', self.res)
        return self.res

    def dfs(self, s, startIndex, strList):

        if startIndex == len(s):
            self.res.append(deepcopy(strList))
            return

        for i in range(startIndex, len(s)):
            substring = s[startIndex : i + 1]
            if startIndex == 1:
                print(startIndex, substring,)

            if self.isPalindrome(substring):

                strList.append(substring)

                self.dfs(s, i + 1, strList)

                strList.pop()



s = "aab"
s = "sean72"
sol = Solution()
ans = sol.partition(s)
# print('ans',*ans, sep='\n')




