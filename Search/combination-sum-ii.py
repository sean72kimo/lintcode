from copy import deepcopy
class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers...
    ret = []
    def combinationSum2(self, candidates, target):
        # write your code here
        candidates = list(candidates)
        candidates.sort()
        self.ret = []
        self.DFS(candidates, 0, [], target)
        return self.ret

    def DFS(self, candidates, startIndex, combination, target):
        length = len(candidates)

        if target == 0:
            tmp = deepcopy(combination)
            print(tmp)
            self.ret.append(tmp)
            return

        for i in range(startIndex, length):
            if i != startIndex and candidates[i] == candidates[i-1]:
                print("idx{0}, val:{1}, prev_val:{2}".format(i, candidates[i], candidates[i-1]))
                continue

            if target < candidates[i]:
                return

            combination.append(candidates[i])
            remainTarget = target - candidates[i]
            self.DFS(candidates, i+1, combination, remainTarget)
            combination.pop()




sol = Solution()
candidates = [10,1,6,7,1,2,5]
target = 8
ans = sol.combinationSum2(candidates, target)
print('ans:', ans)