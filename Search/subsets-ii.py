from copy import deepcopy
class Solution:
    """
    @param S: A set of numbers.
    @return: A list of lists. All valid subsets.
    """
    res = []
    def subsetsWithDup(self, S):
        if not S:
            self.res.append([])
            return self.res
        S.sort()
        subset = []
        startIndex = 0
        self.dfs(S, startIndex, subset)
        return self.res

    def dfs(self, S, startIndex, subset):

        self.res.append(deepcopy(subset))

        for i in range(startIndex, len(S)):
            # if i != startIndex and S[i] == S[i-1]:
            #     continue
            subset.append(S[i])
            self.dfs(S, i+1, subset)
            subset.pop()



S = []
sol = Solution()
ans = sol.subsetsWithDup(S)
print('ans:', ans)