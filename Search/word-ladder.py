import string


class Solution:
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        if len(wordList) == 0:
            return 0

        wordSet = set(wordList)
        que = [(beginWord, [beginWord])]
        vst = {beginWord}
        step = 1
        while que:
            new_q = []
            for curr, path in que:
                if curr == endWord:
                    print(path) #打印路徑
                    return step

                candidate = self.getNext(curr)
                for nxt in candidate:
                    if nxt in vst or nxt not in wordSet:
                        continue
                    p = path[:]
                    p.append(nxt)
                    itm = (nxt, p)
                    new_q.append(itm)
                    vst.add(nxt)
            que = new_q
            step += 1
        return 0

    def getNext(self, curr):
        candidate = []
        for i, ch in enumerate(curr):
            for letter in string.ascii_lowercase:
                nxt = curr[:i] + letter + curr[i + 1:]
                candidate.append(nxt)
        return candidate

start = "hit"
end = "cog"
dict = {"hot","dot","dog","lot","log","cog"}
sol = Solution()
a = sol.ladderLength(start, end, dict)
print("ans:", a)


class Solution:
    """
    @param: start: a string
    @param: end: a string
    @param: dict: a set of string
    @return: An integer
    """
    def ladderLength(self, start, end, dict):
        # write your code here
        if start is None or end is None:
            return 0
        if dict is None or len(dict) == 0:
            return 0
        if start == end:
            return 1


        dict.add(start)
        dict.add(end)

        step = 0
        queue = [start]
        visited = []


        while queue:
            new_q = []
            step += 1
            for word in queue:
                if word == end:
                    return step
                if word in visited:
                    continue
                visited.append(word)
                next_words = self.getNextWords(word, dict)

                for w in next_words:
                    if w in visited:
                        continue
                    if w in new_q:
                        continue
                    new_q.append(w)

            queue = new_q

        return 0

    def getNextWords(self, word, dict):
        tmp = []
        for i in range(len(word)):
            for letter in string.ascii_lowercase:
                if letter == word[i]:
                    continue
                new_word = word[:i] + letter + word[i + 1:]
                if new_word in dict:
                    tmp.append(new_word)

        return tmp





from collections import deque
class Solution2(object):
    def ladderLength(self, beginWord, endWord, wordList):

        def construct_dict(word_list):
            d = {}
            for word in word_list:
                for i in range(len(word)):
                    s = word[:i] + "_" + word[i + 1:]
                    d[s] = d.get(s, []) + [word]
            return d

        def bfs_words(begin, end, dict_words):
            queue, visited = deque([(begin, 1)]), set()
            while queue:
                word, steps = queue.popleft()
                if word not in visited:
                    visited.add(word)
                    if word == end:
                        return steps
                    for i in range(len(word)):
                        s = word[:i] + "_" + word[i + 1:]
                        neigh_words = dict_words.get(s, [])
                        for neigh in neigh_words:
                            if neigh not in visited:
                                queue.append((neigh, steps + 1))
            return 0


        d = construct_dict(wordList | set([beginWord, endWord]))
        return bfs_words(beginWord, endWord, d)

# start = "hit"
# end = "cog"
# dict = {"hot", "dot", "dog", "lot", "log"}
# sol = Solution2().ladderLength(start, end, dict)
# print("ans:", sol)
