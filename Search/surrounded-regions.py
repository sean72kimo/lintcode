class Solution(object):
    def solve(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if board is None or len(board) == 0:
            return

        n = len(board)
        m = len(board[0])

        def inbound(x, y):
            return 0 <= x < n and 0 <= y < m

        def bfs(i, j):
            if board[i][j] != 'O':
                return

            d = [[0, 1], [0, -1], [1, 0], [-1, 0]]
            q = [[i, j]]

            while q:
                x, y = q.pop(0)
                board[x][y] = 'W'
                for dx, dy in d:
                    nx = x + dx
                    ny = y + dy
                    if inbound(nx, ny) and board[nx][ny] == 'O':
                        q.append([nx, ny])

        # find 'O' from edge
        for i in range(n):
            bfs(i, 0)
            bfs(i, m - 1)


        # find 'O' from edge
        for j in range(m):
            bfs(0, j)
            bfs(n - 1, j)

        for i in range(m):
            for j in range(n):
                if board[i][j] == 'O':
                    board[i][j] = 'X'

        for i in range(m):
            for j in range(n):
                if board[i][j] == 'W':
                    board[i][j] = 'O'


board = [
    ["X", "X", "X", "X"],
    ["X", "O", "O", "X"],
    ["X", "X", "O", "X"],
    ["X", "O", "X", "X"]
]
Solution().solve(board)

for r in board:
    print(r)


