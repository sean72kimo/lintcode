from copy import deepcopy
class Solution:
    """
    @param nums: A list of Integers.
    @return: A list of permutations.
    """
    rst = []
    def permute(self, nums):
        if not nums:
            self.rst.append([])
            return self.rst

        self.helper(nums, [])
        return self.rst

    def helper(self, nums, numList):
        if len(numList) == len(nums):
            self.rst.append(deepcopy(numList))
            return

        for i in range(0, len(nums)):
            if nums[i] in numList:
                continue
            numList.append(nums[i])
            print(numList)
            self.helper(nums, numList)
            numList.pop()


nums = [1, 2, 3]
sol = Solution()
ans = sol.permute(nums)
print('ans', ans)
