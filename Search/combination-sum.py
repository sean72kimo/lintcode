from copy import deepcopy
class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def combinationSum(self, candidates, target):
        # write your code here
        candidates = list(set(candidates))
        candidates.sort()
        Solution.ret = []
        self.DFS(candidates, target, 0, [])
        return Solution.ret

    def DFS(self, candidates, target, start, valuelist):
        length = len(candidates)

        if target == 0:

            tmp = deepcopy(valuelist)
            print(tmp)
            Solution.ret.append(tmp)
            return
        for i in range(start, length):
            if target < candidates[i]:
                return
            # valuelist2 = valuelist + [candidates[i]]...
            # self.DFS(candidates, target - candidates[i], i, valuelist2)

            valuelist.append(candidates[i])
            self.DFS(candidates, target - candidates[i], i, valuelist)
            valuelist.pop()




sol = Solution()
candidates = [2,3,6,7]
target = 7
ans = sol.combinationSum(candidates, target)
print('ans:', ans)