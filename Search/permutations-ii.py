from copy import deepcopy
VISITED = 1
UNVISITED = 0
class Solution:
    """
    @param nums: A list of Integers.
    @return: A list of permutations.
    """
    rst = []
    visited = []
    def permuteUnique(self, nums):
        if not nums:
            self.rst.append([])
            return self.rst
        self.visited = [UNVISITED] * len(nums)
        self.helper(sorted(nums), [])
        return self.rst

    def helper(self, nums, numList):
        if len(numList) == len(nums):
            self.rst.append(deepcopy(numList))
            return

        for i in range(0, len(nums)):
            # if self.visited[i] == VISITED or nums[i] == nums[i-1]:
            if self.visited[i] == VISITED:
                continue

            if i != 0 and nums[i] == nums[i - 1]:
                if self.visited[i - 1] == UNVISITED:
                    print('pass', nums[i], numList)
                    continue

            self.visited[i] = VISITED
            numList.append(nums[i])
            print(numList)
            self.helper(nums, numList)
            numList.pop()
            self.visited[i] = UNVISITED



class Solution2:
    """
    @param nums: A list of Integers.
    @return: A list of permutations.
    """
    rst = []
    visited = []

    def permuteUnique(self, nums):
        if nums is None and len(nums) == 0:
            return self.rst

        self.dfs(nums, [])
        return self.rst

    def dfs(self, nums, numList):
        if len(numList) == len(nums):
            if numList not in self.rst:
                self.rst.append(deepcopy(numList))
            return

        for i in range(len(nums)):

            numList.append(nums[i])

            self.dfs(nums, numList)
            numList.pop()

nums = [1, 2, 2]
sol = Solution()
ans = sol.permuteUnique(nums)
print('ans', *ans, sep = '\n')
