EMPTY = 0
BARRIER = 1
class Solution:
    # @param {boolean[][]} grid a chessboard included 0 (False) and 1 (True)
    # @param {Point} source a point
    # @param {Point} destination a point
    # @return {int} the shortest path
    def shortestPath(self, grid, source, destination):
        # Write your code her
        if grid is None or source is None or destination is None:
            return -1

        dirX = [1, 1,-1,-1, 2, 2,-2,-2]
        dirY = [2,-2, 2,-2, 1,-1, 1,-1]
        direction = [(-2, -1), (-2, 1), (-1, 2), (1, 2), (2, 1), (2, -1), (1, -2), (-1, -2)]

        n = len(grid)
        m = len(grid[0])

        steps = 0
        queue = list()
        queue.append(source)
        grid[source.x][source.y] = BARRIER

        while queue:
            newQ = []
            for node in queue:

                if node.x == destination.x and node.y == destination.y:
                    return steps

                for dx, dy in direction:
                    x = node.x + dx
                    y = node.y + dy
                    nextPoint = Point(x,y)
                    if not self.inBound(nextPoint, grid):
                        continue

                    newQ.append(nextPoint)
                    grid[x][y] = BARRIER

            steps += 1
            queue = newQ

        return -1

    def inBound(self, point, grid):

        if point.x < 0 or point.x >= len(grid):
            return False
        if point.y < 0 or point.y >= len(grid[0]):
            return False
        if grid[point.x][point.y] == BARRIER:
            return False

        return True



class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b
sol = Solution()
source = Point(0,0)
destination = Point(7,0)
param = {
    'grid' : [[0,0,0,0,1,1],[1,0,1,0,0,1],[0,0,1,0,0,1],[0,0,1,1,0,1],[1,0,1,0,0,1],[0,0,1,0,0,1],[0,0,1,0,0,1],[0,0,1,0,0,1]],
    'source':source,
    'destination':destination
}
ans = sol.shortestPath(**param)
print(ans)
