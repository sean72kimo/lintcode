import collections


class Solution(object):
    def maxAreaOfIsland(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        print('maxAreaOfIsland')
        if grid is None or len(grid) == 0 :
            return 0

        m = len(grid)
        n = len(grid[0])
        visited = [[0 for _ in range(n)] for _ in range(m)]
        maxsize = 0

        for i in range(m):
            for j in range(n):
                if grid[i][j] and not visited[i][j]:
                    size = self.bfs(grid, i, j, visited)
                    maxsize = max(size, maxsize)

        return maxsize

    def bfs(self, grid, i, j, visited):

        def inbound(x, y):
            return 0 <= x < len(grid) and 0 <= y < len(grid[0])

        d = [[1, 0], [0, 1], [-1, 0], [0, -1]]
        q = [(i, j)]
        visited[i][j] = 1
        size = 0
        while q:
            x, y = q.pop(0)


            size += 1
            for dx, dy in d:
                nx = x + dx
                ny = y + dy
                if inbound(nx, ny) and not visited[nx][ny] and grid[nx][ny]:
                    visited[nx][ny] = 1
                    q.append((nx, ny))

        return size




grid = [
 [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
 [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
 [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
 [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
 [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]]
a = Solution().maxAreaOfIsland(grid)
print('ans:', a)


class Solution2(object):
    def maxAreaOfIsland(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0:
            return

        m = len(grid)
        n = len(grid[0])
        self.dxy = [[1, 0], [-1, 0], [0, 1], [0, -1]]

        vst = set()
        ans = 0
        for i in range(m):
            for j in range(n):
                if (i, j) not in vst and grid[i][j] == 1:
                    size = self.bfs(grid, i, j, vst)
                    print((i,j), size)

                    ans = max(size, ans)
        return ans

    def bfs(self, grid, i, j, vst):
        m = len(grid)
        n = len(grid[0])

        que = collections.deque([])
        que.append((i, j))
        vst.add((i, j))

        size = 0
        while que:
            x, y = que.popleft()
            size += 1

            for dx, dy in self.dxy:
                nx = x + dx
                ny = y + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) in vst:
                    continue

                if grid[nx][ny] == 1:
                    que.append((nx, ny))
                    vst.add((nx, ny))

        return size

grid = [
    [1,1,0,1,1],
    [1,0,0,0,0],
    [0,0,0,0,1],
    [1,1,0,1,1]
]
a = Solution2().maxAreaOfIsland(grid)
print('ans:', a)
