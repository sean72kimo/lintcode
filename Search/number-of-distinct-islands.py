import collections


class Solution_dfs(object):
    def __init__(self):
        self.dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]

    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """

        m = len(grid)
        n = len(grid[0])
        islands = set()
        for i in range(m):
            for j in range(n):
                island = []
                if self.dfs(i, j, i, j, grid, m, n, island):
                    islands.add(tuple(island))

        return len(islands)

    def inbound(self, x, y, m, n):
        return 0 <= x < m and 0 <= y < n

    def dfs(self, i0, j0, i, j, grid, m, n, island):
        if not self.inbound(i, j, m, n):
            return False
        if grid[i][j] <= 0:
            return False

        island.append((i - i0, j - j0))
        grid[i][j] = -1

        for dx, dy in self.dxy:
            self.dfs(i0, j0, i + dx, j + dy, grid, m, n, island)

        return True


class Solution_bfs(object):
    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        vst = set()
        shapes = set()
        self.dxy = [[1, 0], [-1, 0], [0, 1], [0, -1]]

        def mykey(x):
            return x[0], x[1]

        for i in range(m):
            for j in range(n):
                if (i, j) not in vst and grid[i][j] == 1:
                    path = []
                    self.bfs(grid, i, j, i, j, vst, path)
                    path.sort(key=mykey)
                    shapes.add(tuple(path))
        return len(shapes)

    def bfs(self, grid, orig_i, orig_j, i, j, vst, path):
        m = len(grid)
        n = len(grid[0])

        que = collections.deque([(i, j)])
        vst.add((i, j))
        while que:
            i, j = que.popleft()

            di = i - orig_i
            dj = j - orig_j
            path.append((di, dj))

            for dx, dy in self.dxy:
                nx = i + dx
                ny = j + dy
                if (nx, ny) in vst:
                    continue

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if grid[nx][ny] == 0:
                    continue

                que.append((nx, ny))
                vst.add((nx, ny))


grid = [[1, 1, 0, 0, 0], [1, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 1]]
grid = [
    [1, 1, 0, 1, 1],
    [1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1],
    [1, 1, 0, 1, 1]
]
a = Solution().numDistinctIslands(grid)
print("ans:" , a)
