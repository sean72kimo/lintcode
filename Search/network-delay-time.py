import collections
import heapq


class Solution(object):
    def networkDelayTime(self, times, N, K):
        """
        :type times: List[List[int]]
        :type N: int
        :type K: int
        :rtype: int
        """
        graph = self.build_graph(times)

        que = [(0, K)]
        vst = set()
        dis = {}

        while que:
            print(que)
            t, curr = heapq.heappop(que)

            if curr in dis and t >= dis[curr]:
                continue

            dis[curr] = t

            if len(dis) == N:
                return t

            for nei in graph[curr]:
                w = graph[curr][nei]
                itm = (w + t, nei)
                heapq.heappush(que, itm)

        return -1

    def build_graph(self, times):
        graph = collections.defaultdict(dict)
        for u, v, w in times:
            graph[u][v] = w
        return graph



class Solution_dfs(object):
    def networkDelayTime(self, times, N, K):
        """
        :type times: List[List[int]]
        :type N: int
        :type K: int
        :rtype: int
        """
        if K < 1 or K > N:
            return -1
        if len(times) == 0:
            return -1

        graph = collections.defaultdict(list)
        for s, e, t in times:
            graph[s].append((e, t))

        if K not in graph:
            return -1

        mp = {}
        for i in range(1, N + 1):
            mp[i] = float('inf')  # assume not reachable to every node

        self.dfs(K, 0, mp, graph)
        ans = max(mp.values())

        if ans == float('inf'):
            return -1
        return ans

    def dfs(self, node, time, mp, graph):
        if time >= mp[node]:
            return

        mp[node] = time

        def mykey(x):
            return x[1], x[0]
        graph[node].sort(key=mykey)

        for nei, t in graph[node]:
            self.dfs(nei, time + t, mp, graph)



class Solution_heap(object):
    def networkDelayTime(self, times, N, K):
        """
        :type times: List[List[int]]
        :type N: int
        :type K: int
        :rtype: int
        """
        if K < 1 or K > N:
            return -1
        if len(times) == 0:
            return -1

        graph = collections.defaultdict(list)
        for s, e, t in times:
            graph[s].append((e, t))
        if K not in graph:
            return -1

        heap = [(0, K)]
        dist = {}
        while len(heap):
            t, node = heapq.heappop(heap)
            if node in dist:
                continue
            dist[node] = t
            for nei, t2 in graph[node]:
                heapq.heappush(heap, (t + t2, nei))
        if len(dist) != N:
            return -1
        return max(dist.values())


class Solution_bfs_dist_mp(object):
    def networkDelayTime(self, times, N, K):
        """
        :type times: List[List[int]]
        :type N: int
        :type K: int
        :rtype: int
        """
        graph = collections.defaultdict(list)

        for u, v, w in times:
            graph[u].append((v, w))

        mp = collections.defaultdict(lambda: float('inf'))
        que = collections.deque([])
        mp[K] = 0
        que.append([K, 0])

        while que:
            curr, time = que.popleft()

            for nei, w in graph[curr]:
                t = time + w
                if t >= mp[nei]:
                    continue

                mp[nei] = t
                que.append([nei, t])


        if len(mp) == N:
            return max(mp.values())

        return -1
times, N, K = [[1,2,1],[2,1,3]], 2, 2
exp = 3

times, N, K = [[1,2,1],[2,3,2],[1,3,4]], 3, 1
exp = 3

# times, N, K = [[1,2,1],[2,3,2],[1,3,2]], 3, 1
# exp = 2
sol = Solution_bfs_dist_mp()
a = sol.networkDelayTime(times, N, K)
print("ans:", a)
