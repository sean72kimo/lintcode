import collections
import string


class Solution(object):
    def findLadders(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: List[List[str]]
        """
        if len(wordList) == 0:
            return []

        dist_map = {}
        neighbor = collections.defaultdict(set)
        wordList.append(beginWord)
        self.bfs(endWord, beginWord, set(wordList), dist_map, neighbor)
        print(neighbor)
        print(dist_map)
        path = [beginWord]
        ans = []
        self.dfs(beginWord, endWord, dist_map, neighbor, path, ans)

        return ans

    def dfs(self, curr, end, dist_map, neighbor, path, ans):
        if curr == end:
            ans.append(path[:])
            return

        for nxt in neighbor[curr]:
            if dist_map[curr] - 1 == dist_map[nxt]:
                path.append(nxt)
                self.dfs(nxt, end, dist_map, neighbor, path, ans)
                path.pop()

    def bfs(self, start, end, dictionary, dist_map, neighbor):
        que = [start]
        step = 0
        while que:
            new_q = []
            for curr in que:
                if curr in dist_map:
                    continue

                dist_map[curr] = step
                candidate = self.getNext(curr)

                for nxt in candidate:
                    if nxt not in dictionary or nxt == curr:
                        continue
                    neighbor[curr].add(nxt)
                    new_q.append(nxt)

            step += 1
            que = new_q

    def getNext(self, curr):
        candidate = []
        for i, ch in enumerate(curr):
            for letter in string.ascii_lowercase:
                nxt = curr[:i] + letter + curr[i + 1:]
                candidate.append(nxt)
        return candidate


class Solution2(object):
    def findLadders(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: List[List[str]]
        """
        if len(wordList) == 0:
            return []

        dist_map = {}
        neighbor = collections.defaultdict(set)
        wordList.append(beginWord)
        self.bfs(endWord, beginWord, set(wordList), dist_map, neighbor)
        print(neighbor)
        print(dist_map)

        path = [beginWord]
        ans = []
        self.dfs(beginWord, endWord, dist_map, neighbor, path, ans)

        return ans

    def dfs(self, curr, end, dist_map, neighbor, path, ans):
        if curr == end:
            ans.append(path[:])
            return

        for nxt in neighbor[curr]:
            if dist_map[curr] - 1 == dist_map[nxt]:
                path.append(nxt)
                self.dfs(nxt, end, dist_map, neighbor, path, ans)
                path.pop()

    def bfs(self, start, end, dictionary, dist_map, neighbor):
        que = [start]
        dist_map[start] = 0
        step = 1

        while que:
            new_q = []
            for curr in que:

                ret = self.getNext(curr)

                for itm in ret:
                    if itm not in dictionary:
                        continue

                    if curr in dictionary and itm in dictionary:
                        neighbor[curr].add(itm)
                        neighbor[itm].add(curr)


                    if itm in dist_map:
                        continue

                    new_q.append(itm)
                    dist_map[itm] = step

            que = new_q
            step += 1

    def getNext(self, curr):
        candidate = []
        for i, ch in enumerate(curr):
            for letter in string.ascii_lowercase:
                if letter == ch:
                    continue
                nxt = curr[:i] + letter + curr[i + 1:]
                candidate.append(nxt)
        return candidate
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log","cog"]

beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]
sol = Solution2()
ans = sol.findLadders(beginWord, endWord, wordList)
print('ans:', ans)
