class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """


        tmp = 0
        while dividend:
            t = divisor

            while dividend // t:
                t = t << 1

            t = t >> 1
            dividend = dividend // t
            tmp += t

        return tmp

dividend = 10
divisor = 3
print('ans:', Solution().divide(dividend, divisor))


class Solution2(object):

    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        MAX_VAL = 2147483647
        MIN_VAL = -2147483648


        if divisor == 0:
            return MAX_VAL

        if dividend == MIN_VAL:
            if divisor == 1 :
                return MIN_VAL
            if divisor == -1 :
                return MAX_VAL

        positive = True
        if dividend < 0 and divisor > 0:
            positive = False
            dividend = -dividend

        if dividend > 0 and divisor < 0:
            positive = False
            divisor = -divisor

        if dividend < 0 and divisor < 0:
            divisor = -divisor
            dividend = -dividend

        res = 0
        while dividend >= divisor:
            shift = 0
            temp = divisor

            while dividend >= temp:
                shift += 1
                temp = divisor << shift

            cnt = shift - 1
            res = res + (1 << cnt)
            dividend = dividend - (divisor << cnt)

        if not positive:
            return -res
        else:
            return res





# dividend = -1
# divisor = 1
# print('ans:', Solution().divide(dividend, divisor))
