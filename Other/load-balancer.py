class LoadBalancer:

    def __init__(self):
        # Initialize your data structure here.
        self.cluster = {}


    # @param {int} server_id add a new server to the cluster
    # @return nothing
    def add(self, server_id):
        # Write your code here
        if server_id in self.cluster:
            self.cluster[server_id] = self.cluster[server_id] + 1
        else:
             self.cluster[server_id] = 1


    # @param {int} server_id remove a bad server from the cluster
    # @return nothing
    def remove(self, server_id):
        # Write your code here
        if server_id not in self.cluster:
            return
        else:
            del self.cluster[server_id]
            # self.cluster[server_id] -= 1


    # @return {int} pick a server in the cluster randomly with equal probability
    def pick(self):
        import random
        index = random.randint(0, len(self.cluster) - 1)
        cluster = list(self.cluster.items())

        return cluster[index][0]

l = LoadBalancer()
l.add(1)
l.add(2)
l.add(3)

l.pick()
l.pick()
l.pick()
l.remove(1)
l.pick()

