# The knows API is already defined for you.
# @param a, person a
# @param b, person b
# @return a boolean, whether a knows b
# you can call Celebrity.knows(a, b)

class Solution:
    # @param {int} n a party with n people
    # @return {int} the celebrity's label or -1
    def findCelebrity(self, n):
        # Write your code here
        celebrity = 0
        for person in range(1, n):
            if Celebrity.knows(celebrity, person):
                celebrity = person


        for person in range(n):
            if Celebrity.knows(celebrity, person) and celebrity != person:
                return -1
            if not Celebrity.knows(person, celebrity) :
                return -1

        return celebrity
