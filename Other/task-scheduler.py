import collections
import heapq


class Solution(object):
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        if not tasks or len(tasks) == 0:
            return 0
        
        if n == 0:
            return len(tasks)
        
        tasks_counts = collections.Counter(tasks)

        most_task = max(tasks_counts.values())
        print(most_task)
        Mct = 0
        for t, c in tasks_counts.items():
            if c == most_task:
                Mct += 1
        
        slots = (most_task - 1) * (n + 1) + Mct
        # len(tasks) > slots: we can schedule most frequent tasks without idling
        return max(slots, len(tasks))

class Solution_heap(object):
    def leastInterval(self, S, k):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        if k == 0:
            return len(S)
        counter = collections.Counter(S)
        que = []
        for key, cnt in counter.items():
            itm = (-cnt, key)
            heapq.heappush(que, itm)
        k += 1
        ans = ""
        while que:
            new_q = []
            for i in range(k):

                if len(que) == 0:
                    ans += " " * (k-i)
                    break

                cnt, ch = heapq.heappop(que)
                ans += ch

                if cnt + 1 < 0:
                    prev_cnt = cnt + 1
                    prev_ch = ch
                    new_q.append((prev_cnt, prev_ch))

            for itm in new_q:
                heapq.heappush(que, itm)

        ans = ans.rstrip()
        return len(ans)
all_tasks = ["A","A","A","B","B","B"]
n = 2
# all_tasks = ["A","A","A","B","B","B"]
# n = 0
a = Solution2().leastInterval(all_tasks, n)
print("ans:", a)


