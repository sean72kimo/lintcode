class Solution:
    """
    @param: word: a non-empty string
    @param: abbr: an abbreviation
    @return: true if string matches with the given abbr or false
    """
    def validWordAbbreviation(self, word, abbr):
        # write your code here
        i, j = 0, 0
        while i < len(word) and j < len(abbr):
            if abbr[j] == '0':
                return False

            if abbr[j].isalpha():
                if abbr[j] != word[i]:
                    return False
                j += 1
                i += 1
            else:
                val = 0

                while j < len(abbr) and abbr[j].isdigit():
                    val = val * 10 + int(abbr[j])
                    j += 1

                i += val
        if i == len(word) and j == len(abbr):
            return True
        else:
            return False


word = "internationalization"
abbr = "i12iz4n"
word = "a"
abbr = "1"
print('ans:', Solution().validWordAbbreviation(word, abbr))


