class Solution(object):
    def __init__(self):
        self.ans = []
    def lexicalOrder(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        for i in range(1, 10):
            self.dfs(i, n)
        return self.ans

    def dfs(self, cur, n):
        if cur > n:
            return

        self.ans.append(cur)
        for i in range(10):
            ne = cur * 10 + i

            if ne > n:
                return
            self.dfs(ne, n)

ans = Solution().lexicalOrder(13)
print('ans:', ans)
