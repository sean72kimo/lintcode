class Solution(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        if len(nums) == 0:
            return 0

        n = len(nums)

        myset = set(nums)
        ans = 1

        for n in nums:

            if n in myset:
                myset.remove(n)
                nxt = n + 1
                prv = n - 1
                print(nxt, prv)

            while nxt in myset:
                myset.remove(nxt)
                nxt += 1

            while prv in myset:
                myset.remove(prv)
                prv -= 1

            leng = nxt - prv - 1
            ans = max(leng, ans)

        return ans





num = [100, 4, 200, 1, 3, 2]
num = [3,4,2,100]
print('ans:', Solution().longestConsecutive(num))
