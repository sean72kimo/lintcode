class ZigzagIterator(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self.frm = "v1"
        self.p1 = 0
        self.p2 = 0
        self.v1 = v1
        self.v2 = v2
        if len(self.v1) == 0:
            self.frm = "v2"

    def next(self):
        """
        :rtype: int
        """
        if not self.hasNext():
            return
        
        if self.frm == "v1":
            val = self.v1[self.p1]
            self.p1 += 1
            
            if self.p2 < len(self.v2):
                self.frm = "v2"
            else:
                self.frm = "v1"
            
        elif self.frm == "v2":
            val = self.v2[self.p2]
            self.p2 += 1
            
            if self.p1 < len(self.v1):
                self.frm = "v1"
            else:
                self.frm = "v2"

        return val
        
        

    def hasNext(self):
        """
        :rtype: bool
        """
        if self.p1 == len(self.v1) and self.p2 == len(self.v2):
            return False
        
        return True
        
        
        
class ZigzagIterator2(object):

    def __init__(self, v1, v2):
        self.data = [(len(v), iter(v)) for v in (v1, v2) if v]
        print(self.data)
    def next(self):
        len, iter = self.data.pop(0)
        if len > 1:
            self.data.append((len-1, iter))
        return next(iter)

    def hasNext(self):
        return bool(self.data)
# Your ZigzagIterator object will be instantiated and called as such:
v1 = [1,2]
v2 = [3,4,5,6]
# v1 = [1]
# v2 = []
i, v = ZigzagIterator2(v1, v2), []
while i.hasNext(): 
    v.append(i.next())
print(v)

