
# Definition for an Interval.
class Interval:
    def __init__(self, start: int = None, end: int = None):
        self.start = start


"""
https://leetcode.com/problems/employee-free-time/
similar to merger interval and related questions
"""
class Solution: # leetcode uses Interval object instead of list
    def employeeFreeTime(self, schedule: '[[Interval]]') -> '[Interval]':

        events = []
        start = 0
        end = 1
        for employee in schedule:
            for inter in employee:
                event_s = (inter.start, start)
                event_e = (inter.end, end)
                events.append(event_s)
                events.append(event_e)

        def mykey(x):
            return x[0], x[1]

        events.sort(key=mykey)

        working_cnt = 0

        prev = None
        ans = []
        for time, status in events:
            if working_cnt == 0 and prev:
                ans.append(Interval(prev, time))
            if status == start:
                working_cnt += 1
            elif status == end:
                working_cnt -= 1

            prev = time
        return ans


class Solution(object): # convert Interval to list
    def employeeFreeTime(self, schedule):
        """
        :type schedule: List[List[List[int]]]
        :rtype: List[List[int]]
        """
        events = []
        for employee in schedule:
            for s, e in employee:
                events.append((s, 0))
                events.append((e, 1))
        def mykey(x):
            return x[0], x[1]
        events.sort(key=mykey)


        ans = []
        prev = None
        bal = 0
        for t, cmd in events:
            if bal == 0 and prev is not None:
                ans.append([prev, t])

            if cmd == 0:
                bal += 1
            elif cmd == 1:
                bal -= 1
            prev = t
        return ans





schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
exp = [[3,4]]
a = Solution().employeeFreeTime(schedule)
print("ans:", a==exp, a)