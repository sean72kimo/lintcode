from functools import cmp_to_key
class Solution(object):
    def canAttendMeetings(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: bool
        """
        n = len(intervals)

        if n <= 1:
            return True


        def sorter(a, b):
            return a.start - b.start


        inter = sorted(intervals, key = cmp_to_key(sorter))

        for i in range(1, n):
            if inter[i].start < inter[i - 1].end:
                return False
        return True

class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e
class Solution2(object):
    def canAttendMeetings(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: bool
        """
        if len(intervals) == 0:
            return True
        def mykey(x):
            return x.start
        intervals.sort(key=mykey)
        for i in range(len(intervals)):
            print(intervals[i].start, intervals[i].end)
            
        for i in range(len(intervals)):
            if i == 0:
                continue
            if intervals[i].start < intervals[i-1].end:
                return False
        return True
    
A = [[7,10],[2,4]]
intervals = []
for s, e in A:
    intervals.append(Interval(s,e))
    
print("ans:",Solution2().canAttendMeetings(intervals))
