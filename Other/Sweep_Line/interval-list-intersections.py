class Solution(object):
    def intervalIntersection(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        ans = []
        i = j = 0
        end = float('-inf')
        while i < len(A) and j < len(B):
            lo = max(A[i][0], B[j][0])
            hi = min(A[i][1], B[j][1])

            if lo <= hi:
                ans.append([lo, hi])

            if A[i][1] < B[j][1]:
                i += 1
            else:
                j += 1
        return ans


class Solution_sweep_line_sort(object):
    def intervalIntersection(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        events = []
        for s, e in A:
            events.append((s, 0))
            events.append((e, 1))
        for s, e in B:
            events.append((s, 0))
            events.append((e, 1))

        def mykey(x):
            return x[0], x[1]
        events.sort(key=mykey)


        ans = []
        prev = None
        bal = 0
        for t, cmd in events:
            if cmd == 0:
                bal += 1
            elif cmd == 1:
                bal -= 1
                if bal == 1 and prev is not None:
                    ans.append([prev, t])
            prev = t

        return ans


class Node(object):
    def __init__(self, time, status):
        self.time = time
        self.status = status
    def __repr__(self):
        string = 'right' if self.status else 'left'
        return "<{}, {}>".format(string, self.time)


class Solution(object):
    def intervalIntersection(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        events = []
        for s, e in A:
            events.append(Node(s, 0))  # open
            events.append(Node(e, 1))  # close
        for s, e in B:
            events.append(Node(s, 0))
            events.append(Node(e, 1))

        def mykey(x):
            return x.time, x.status

        events.sort(key=mykey)

        stack = []
        ans = []
        for i, node in enumerate(events):
            if len(stack) == 0 or node.status == 0:
                stack.append(node)
            elif node.status == 1:
                prev = stack.pop()
                if len(stack):
                    overlap = [prev.time, node.time]
                    ans.append(overlap)
        return ans


class Solution(object):
    def intervalIntersection(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        i = j = 0

        events = []

        def mykey(x):
            return x[0], x[1]
        while i < len(A) and j < len(B):
            a1 = [A[i][0], 0]
            a2 = [A[i][1], 1]

            b1 = [B[i][0], 0]
            b2 = [B[i][1], 1]
            event = [a1, a2, b1, b2]

            event.sort(key=mykey)
            events.extend(event)
            i += 1
            j += 1

        print(events)
        stack = []
        ans = []
        for t, e in events:
            if len(stack) == 0 or e == 0:
                stack.append([t, e])
            elif e == 1:
                prev = stack.pop()
                if len(stack):
                    overlap = [prev[0], t]
                    ans.append(overlap)
        return ans




A = [[0,2],[5,10],[13,23],[24,25]]
B = [[1,5],[8,12],[15,24],[25,26]]
exp = [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]

# A = [[0,2]]
# B = [[1,5]]
# exp = [[1,2]]


sol = Solution()
a = sol.intervalIntersection(A, B)
print("ans:", a==exp, a)