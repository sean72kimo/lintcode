"""
Definition of Interval.
"""
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

class Solution:
    """
    @param: intervals: interval list.
    @return: A new interval list.
    1. 注意 input intervals並非有序，需先排序!!
    """
    def merge(self, intervals):
        if len(intervals) == 0:
            return [[]]
        if len(intervals) == 1:
            return intervals
        ans = []

        intervals.sort(key = lambda x: x.start)


        i = 0
        for interval in intervals:
            if len(ans) == 0:
                ans.append(interval)

            if ans[-1].end < interval.start:
                ans.append(interval)
            else:
                ans[-1].end = max(ans[-1].end, interval.end)

        return ans
a = [
  [8, 10],
  [2, 6],
  [15, 18],
  [1, 3],
]


intervals = []
for i in a:
    intervals.append(Interval(i[0], i[1]))
ans = Solution().merge(intervals)

for i in ans:
    print(i.start, i.end)



