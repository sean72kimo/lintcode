# Definition for an interval.
class Interval(object):
    def __init__(self, s = 0, e = 0):
        self.start = s
        self.end = e

class Point():
    def __init__(self, time = 0, flag = 0):
        self.time = time
        self.flag = flag

from functools import cmp_to_key
class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        n = len(intervals)
        if n == 0:
            return 0


        def sorter(a, b):
            if a.time == b.time:
                return a.flag - b.flag
            else:
                return a.time - b.time

        meetings = []
        for i in intervals:
            meetings.append(Point(i.start, 1))
            meetings.append(Point(i.end, 0))

        inter = sorted(meetings, key = cmp_to_key(sorter))

        room = 0
        ans = 0
        for i in inter:
            print("[{},{}]".format(i.time, i.flag))
            if i.flag:
                room += 1
            else:
                room -= 1


            ans = max(ans, room)

        return ans

intervals = []
inputs = [[9, 10], [4, 9], [4, 17]]
for s, e in inputs:
    intervals.append(Interval(s, e))
# ans = Solution().minMeetingRooms(intervals)
# print('ans:', ans)



class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        n = len(intervals)
        if n == 0:
            return 0

        def mycmp(a, b):
            if a.start == b.start:
                return a.end - b.end
            return a.start - b.start
        inter = sorted(intervals, key = cmp_to_key(mycmp))

        for item in inter:
            print("[{},{}]".format(item.start, item.end), end = "")
        print("")

        end = inter[0].end
        cnt = 1

        for i in range(1, n):
            if inter[i].start < end:
                cnt += 1
                end = max(inter[i].end, end)

                if inter[i].start >= inter[i - 1].end:
                    cnt -= 1

            else:
                end = inter[i].end



        return cnt


intervals = []
inputs = [[2, 15], [36, 45], [9, 29], [16, 23], [4, 9]]
for s, e in inputs:
    intervals.append(Interval(s, e))
ans = Solution().minMeetingRooms(intervals)
print('ans:', ans)
