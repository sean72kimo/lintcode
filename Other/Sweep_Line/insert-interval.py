"""
Definition of Interval.
"""
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
    def __repr__(self):
        return "[{},{}]".format(self.start, self.end)



class Solution2:
    """
    @param: intervals: Sorted interval list.
    @param: newInterval: new interval.
    @return: A new interval list.
    """
    def insert(self, intervals, newInterval):
        # write your code here
        i = 0
        while i < len(intervals) and intervals[i].start < newInterval.start:
                i += 1

        intervals.insert(i, newInterval)
        print_interval(intervals)


        ans = []
        for interval in intervals:
            if len(ans) == 0:
                ans.append(interval)

            if interval.start > ans[-1].end:
                ans.append(interval)
            else:
                ans[-1].end = max(ans[-1].end, interval.end)

        return ans

class Solution:
    def insert(self, intervals, newInter):
        """
        :type intervals: List[Interval]
        :type newInterval: Interval
        :rtype: List[Interval]
        """
        if len(intervals) == 0:
            return [newInter]
        
        print(len(intervals), intervals)
        for idx, inter in enumerate(intervals):
            if newInter.start > inter.start:
                intervals.insert(idx+1, newInter)
                break


        ans = []
        for inter in intervals:
            if len(ans) == 0:
                ans.append(inter)
                print('one elem',(ans[-1].start, ans[-1].end))
                continue
            if inter.start <= ans[-1].end:
                pre = ans.pop()
                item = Interval(pre.start, max(pre.end, inter.end))
                ans.append(item)
            else:
                ans.append(inter)
        
        return ans

def print_interval(intervals):

    for interval in intervals:
        print('[', end = '')
        print(interval.start, end = ',')
        print(interval.end, end = ']')
    print('\n')

intervals = []
newInterval = Interval(3, 4)
old = [[1, 2], [5, 9]]

newInterval = Interval(9, 9)
old = [[1, 5], [7, 8], [10, 13]]


newInterval = Interval(2, 3)
old = [[1, 5]]


newInterval = Interval(2,5)
old = [[1,3],[6,9]]

for o in old:
    intervals.append(Interval(o[0], o[1]))

ans = Solution().insert(intervals, newInterval)
print('ans:', end = '')
print_interval(ans)


P = [7,1,5,3,6,4]
for i, p in enumerate(P,1):
    print(i,p)
