class MovingAverage(object):

    def __init__(self, size):
        # Initialize your data structure here.
        self.size = size
        self.temp = []
        self.sum = 0.0



    # @param {int} val an teger
    def next(self, val):

        if len(self.temp) < self.size:
            self.temp.append(val)
            self.sum = sum(self.temp)
        else:
            remove = self.temp.pop(0)
            self.temp.append(val)
            self.sum = self.sum + val - remove

        avg = self.sum / len(self.temp)

        return avg

        # Write your code here


m = MovingAverage(3)
m.next(1)
m.next(10)
m.next(3)
m.next(5)


# Your MovingAverage object will be instantiated and called as such:
# obj = MovingAverage(size)
# param = obj.next(val)
