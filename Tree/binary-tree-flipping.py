"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: root: the root of binary tree
    @return: new root
    """
    new_root = None
    def upsideDownBinaryTree(self, root):
        if root is None:
            return

        if root.left is None:
            return root

        newRoot = self.upsideDownBinaryTree(root.left)
        root.left.right = root
        root.left.left = root.right
        root.left = None
        root.right = None

        return newRoot

    def upsideDownBinaryTree2(self, root):
        if root is None:
            return

        self.dfs(root)
        return self.new_root

    def dfs(self, curr):
        print(curr.val)
        if curr.left is None:
            self.new_root = curr
            return

        self.dfs(curr.left)
        curr.left.right = curr
        curr.left.left = curr.right
        curr.left = None
        curr.right = None


from lib.binarytree import BinaryTree, TreeNode

data = '{1,2,3,4,5}'
tree = BinaryTree()
root = tree.deserialize(data)

node = Solution().upsideDownBinaryTree(root)

print(node.right.val, node.left.val)
print(node.right.right.val)
print(node.right.left.val)

