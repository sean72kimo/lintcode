# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
class Solution(object):
    def __init__(self):
        self.longest = 0

    def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None:
            return 0
        self.helper(root)
        return self.longest

    def helper(self, root):
        if root == None:
            return 0

        left = self.helper(root.left)
        right = self.helper(root.right)

        subLongest = 1
        if root.left and root.val + 1 == root.left.val:
            subLongest = max(subLongest, left + 1)
        if root.right and root.val + 1 == root.right.val:
            subLongest = max(subLongest, right + 1)

        self.longest = max(self.longest, subLongest)

        return subLongest

def stringToTreeNode(input):
    input = input.strip()
    input = input[1:-1]
    if not input:
        return None

    inputValues = [s.strip() for s in input.split(',')]
    root = TreeNode(int(inputValues[0]))
    nodeQueue = [root]
    front = 0
    index = 1
    while index < len(inputValues):
        node = nodeQueue[front]
        front = front + 1

        item = inputValues[index]
        index = index + 1
        if item != "null":
            leftNumber = int(item)
            node.left = TreeNode(leftNumber)
            nodeQueue.append(node.left)

        if index >= len(inputValues):
            break

        item = inputValues[index]
        index = index + 1
        if item != "null":
            rightNumber = int(item)
            node.right = TreeNode(rightNumber)
            nodeQueue.append(node.right)
    return root

def intToString(input):
    if input is None:
        input = 0
    return str(input)

def main():
    import sys
    def readlines():
        myinput = ["[1,null,3,2,4,null,null,null,5]"]
        myinput = ["[1,2,3]"]
        for line in myinput:
            yield line.strip('\n')
    lines = readlines()
    while True:
        try:
            line = next(lines)
            root = stringToTreeNode(line)

            ret = Solution().longestConsecutive(root)

            out = intToString(ret)
            print (out)

        except StopIteration:
            break

if __name__ == '__main__':
    main()
