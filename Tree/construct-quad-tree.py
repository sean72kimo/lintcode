"""
# Definition for a QuadTree node.
class Node(object):
    def __init__(self, val, isLeaf, topLeft, topRight, bottomLeft, bottomRight):
        self.val = val
        self.isLeaf = isLeaf
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
        self.bottomRight = bottomRight
"""

"""
dfs重點：起始位置 (0, 0) 縮小範圍 邊長 l
def dfs(self, x, y, l)
"""

class Solution(object):
    def construct(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: Node
        """
        if len(grid) == 0:
            return
        return self.build(grid, 0, 0, len(grid))

    def build(self, grid, x, y, l):
        if l == 1:
            return Node(grid[x][y] == 1, True, None, None, None, None)

        d = l // 2
        top_left = self.build(grid, x, y, d)
        top_right = self.build(grid, x, y + d, d)
        bottom_left = self.build(grid, x + d, y, d)
        bottom_right = self.build(grid, x + d, y + d, d)

        if top_left.isLeaf and top_right.isLeaf and bottom_left.isLeaf and bottom_right.isLeaf and \
                top_left.val == top_right.val == bottom_left.val == bottom_right.val:
            node = Node(top_left.val, True, None, None, None, None)
            return node
        else:
            val = top_left.val or top_right.val or bottom_left.val or bottom_right.val
            node = Node("*", False, top_left, top_right, bottom_left, bottom_right)
            return node