from lib.binarytree import BinaryTree
from lib.binarytree import TreeLinkNode as Node


class Solution_dfs:

    # @param root, a tree link node
    # @return nothing
    def connect(self, root):
        """
        :type root: Node
        :rtype: Node
        """
        if not root:
            return

        dummy = Node(0, None, None, None)
        curr = dummy

        if root.left:
            curr.next = root.left
            curr = curr.next

        if root.right:
            curr.next = root.right
            curr = curr.next

        node = root.next
        while node:
            if node.left:
                curr.next = node.left
                curr = curr.next
            if node.right:
                curr.next = node.right
                curr = curr.next
            node = node.next

        self.connect(root.left)
        self.connect(root.right)


class Solution_while:
    def connect(self, root):
        if not root:
            return

        while root:
            dummy = Node(0, None, None, None)
            curr = dummy
            head = root
            while head:
                if head.left:
                    curr.next = head.left
                    curr = curr.next
                if head.right:
                    curr.next = head.right
                    curr = curr.next
                head = head.next
            root = dummy.next
        return root

data = '[0,1,2,3,4,#,6]'
data = '[0,1,2,3,#,#,6]'
data = '[0,2,4,1,#,3,-1,5,9,#,6,#,8]'
data = '[1,2,3,4,5,#,6,7,#,#,#,#,8]'
tree = BinaryTree(Node)
root = tree.deserialize(data)
tree.drawtree(root)
Solution().connect(root)
