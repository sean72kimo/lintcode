from lib.binarytree import BinaryTree, TreeNode
"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param: root: The root of the binary search tree.
    @param: A: A TreeNode in a Binary.
    @param: B: A TreeNode in a Binary.
    @return: Return the least common ancestor(LCA) of the two nodes.
    """
    def lowestCommonAncestor(self, root, A, B):
        # write your code her
        print(root.val)
        if root is None:
            return root

        if root is A or root is B:
            return root

        left = self.lowestCommonAncestor(root.left, A, B)
        right = self.lowestCommonAncestor(root.right, A, B)

        if left and right:
            return root
        if left:
            return left
        if right:
            return right

        return None

a = TreeNode(4)
b = TreeNode(5)
data = "{4,3,#,#,7,5,6}"

r = BinaryTree().deserialize(data)
ans = Solution().lowestCommonAncestor(r, a, b)
print("ans:", ans)
