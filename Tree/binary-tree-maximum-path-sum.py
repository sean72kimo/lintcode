

class Solution(object):

    def __init__(self):
        self.ans = float('-inf')

    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        self.dfs(root)

        return self.ans

    def dfs(self, root):
        # 注意：此時應該回傳float('-inf')
        # 若parent為負數，例如-1 , 右子樹為空，卻回傳0, 那麼會造成parent以為最大的path sum為0
        if not root:
            return float('-inf')

        left_sum = self.dfs(root.left)
        right_sum = self.dfs(root.right)

        self.ans = max(self.ans, left_sum + right_sum + root.val, root.val, right_sum + root.val, left_sum + root.val)

        return max(root.val, left_sum + root.val, right_sum + root.val)

    # to make dfs looks nicer
    def dfs_nicer(self, root):
        # 注意：此時應該回傳float('-inf')
        # 若parent為負數，例如-1 , 右子樹為空，卻回傳0, 那麼會造成parent以為最大的path sum為0
        if not root:
            return float('-inf') 

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        path_summ = max(root.val + left, root.val + right, root.val)
        self.ans = max(path_summ, self.ans, root.val + left + right)

        return path_summ
