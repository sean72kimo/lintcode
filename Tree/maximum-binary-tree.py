from lib.binarytree import TreeNode, BinaryTree


class Solution(object):

    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if nums is None or len(nums) == 0:
            return None

        return self.helper(nums)

    def helper(self, nums):
        if nums is None or len(nums) == 0:
            return None

        i = nums.index(max(nums))
        root_val = nums[i]
        left_nums = nums[:i]
        right_nums = nums[i + 1:]

        root = TreeNode(root_val)
        root.left = self.helper(left_nums)
        root.right = self.helper(right_nums)

        return root


class Solution_const_space(object):

    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if len(nums) == 0:
            return

        return self.dfs(nums, 0, len(nums) - 1)

    def dfs(self, nums, lo, hi):
        print((lo, hi))
        if lo == hi:
            return TreeNode(nums[lo])

        if lo > hi:
            return
        mx = float('-inf')
        idx = None
        for i in range(lo, hi + 1):
            if nums[i] > mx:
                mx = nums[i]
                idx = i

        root = TreeNode(nums[idx])

        root.left = self.dfs(nums, lo, idx - 1)
        root.right = self.dfs(nums, idx + 1, hi)

        return root


nums = [3, 2, 1, 6, 0, 5]
a = Solution().constructMaximumBinaryTree(nums)

b = Solution_const_space().constructMaximumBinaryTree(nums)

tree = BinaryTree()
print(tree.same_tree(a, b))