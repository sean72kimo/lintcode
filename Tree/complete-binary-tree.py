"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from collections import deque
class Solution:
    """
    @param root: the root of binary tree.
    @return: true if it is a complete binary tree, or false.
    """
    def isComplete(self, root):
        # write your code here
        if not root:
            return True
            
        que = deque([root])

        lst = [root.val]
        while que:
            node = que.popleft()
            
            if node.left:
                que.append(node.left)
                lst.append(node.left.val)
            else:
                lst.append('#')

            if node.right:
                que.append(node.right)
                lst.append(node.right.val)
            else:
                lst.append('#')
        
        while lst[-1] == '#':
            lst.pop()
        
        for i in lst:
            if i == '#':
                return False
        return True
from lib.binarytree import BinaryTree
data = '{1,2,3,4,5,6,7,#,8}'
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution().isComplete(root)
print("ans:", a)



print(chr(ord('a') ^ (1<<5)))
print(chr(ord('A') ^ (1<<5)))
print(ord('a'))
print(ord('A'))

class Coord:
    def __init__(self, x,y):
        self.x = x
        self.y = y
    def __repr__(self):
        return "<Coord> ({},{})".format(self.x, self.y) 
    

# itm = Coord(101,102)
# print(itm, type(itm))
itm = (123,456)
que = deque([itm])
print(len(que))
print(que.popleft())
vst = {itm}
print(vst)
