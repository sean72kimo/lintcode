from lib.binarytree import TreeNode, BinaryTree
class Solution(object):
    def maxAncestorDiff(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.diff = 0
        self.dfs(root, root.val, root.val)
        return self.diff

    def dfs(self, root, max_parent, min_parent):
        if not root:
            return

        d1 = abs(root.val - max_parent)
        d2 = abs(root.val - min_parent)
        self.diff = max(self.diff, d1, d2)
        max_parent = max(root.val, max_parent)
        min_parent = min(root.val, min_parent)
        self.dfs(root.left, max_parent, min_parent)
        self.dfs(root.right, max_parent, min_parent)

class Solution2(object):
    def maxAncestorDiff(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        return self.dfs(root, root.val, root.val)

    def dfs(self, root, mx, mn):
        if not root:
            return mx - mn

        mx = max(root.val, mx)
        mn = min(root.val, mn)
        print(root, mx, mn)
        left = self.dfs(root.left, mx, mn)
        right = self.dfs(root.right, mx, mn)

        return max(left, right)

data = '[8,3,10,1,6,null,14,null,null,4,7,13]' #7
data = '[0,null,1]' #1
tree = BinaryTree()
root = tree.deserialize(data)
sol = Solution2()
a = sol.maxAncestorDiff(root)
print("ans:", a)