# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import collections

from lib.binarytree import BinaryTree, TreeNode


class Solution:

    def __init__(self):
        self.nodes = []
        self.left = []
        self.right = []
        self.bottom = []

    def leftBoundary(self, root):
        if not root.left and not root.right:
            return
        self.left.append(root.val)

        if root.left:
            self.leftBoundary(root.left)
        elif root.right:
            self.leftBoundary(root.right)

    def rightBoundary(self, root):
        if not root.left and not root.right:
            return
        self.right.append(root.val)

        if root.right:
            self.rightBoundary(root.right)
        elif root.left:
            self.rightBoundary(root.left)

    def leaves(self, root):
        if not root:
            return

        if root.left is None and root.right is None:
            self.bottom.append(root.val)

        self.leaves(root.left)
        self.leaves(root.right)

    def boundaryOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if not root:
            return []

        self.nodes.append(root.val)

        if root.left:
            self.leftBoundary(root.left)

        if root.right:
            self.rightBoundary(root.right)

        if root.left or root.right:
            self.leaves(root)

        self.nodes = self.nodes + self.left + self.bottom + self.right[::-1]

        return self.nodes

data = '[1,2,3,4,5,6,#,#,#,7,8,9,10]'
data = '[1,#,2,3,4]'
data = '[1,null,2,3,4,5,6,7,8]'
data = '[1,2,3,4,5,6,null,null,null,7,8,9,10]'
data = '[1,2,3,null,5,6,null,7,8,9,10]'
data = '[1,1]'
tree = BinaryTree()
root = tree.deserialize(data)
# tree.drawtree(root)
a = Solution().boundaryOfBinaryTree(root)
print("ans:", a)
