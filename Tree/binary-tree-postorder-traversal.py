from lib.binarytree import BinaryTree


class Solution_one_stack(object):
    def postorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.ans = []
        if not root:
            return

        stack = [root]
        s2 = []
        ans = []

        while len(stack):
            root = stack.pop()
            ans.append(root.val)
            if root.left:
                stack.append(root.left)
            if root.right:
                stack.append(root.right)

        return ans[::-1]

class Solution_two_stack:
    """
    @param: root: A Tree
    @return: Postorder in ArrayList which contains node values.
    """

    def __init__(self):
        self.ans = []

    def postorderTraversal(self, root):
        # write your code here

        if not root:
            return self.ans

        s1 = [root]
        s2 = []

        while s1:
            node = s1.pop()
            s2.append(node.val)

            if node.left:
                s1.append(node.left)
            if node.right:
                s1.append(node.right)

        while s2:
            self.ans.append(s2.pop())

        return self.ans


data = '[3,9,20,1,2,15,7]'
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution().postorderTraversal(root)
print("ans:", a)
