from lib.binarytree import BinaryTree, TreeNode


class Solution(object):

    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        try:
            v = int(s)
            return TreeNode(v)
        except:
            pass

        if len(s) == 0:
            return

        i = s.find('(')

        sub = s[:i]
        root = TreeNode(int(sub))

        left = 0
        for j in range(i, len(s)):
            if s[j] == '(':
                left += 1
            elif s[j] == ')':
                left -= 1
            if left == 0:
                break

        root.left = self.str2tree(s[i + 1 : j ])
        root.right = self.str2tree(s[j + 2:-1])

        return root


class Solution_divide_conquer(object):
    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        if len(s) == 0:
            return
        return self.dfs(s, 0, len(s) - 1)

    def dfs(self, s, start, end):
        if start > end:
            return

        n_str = ""
        for j in range(start, end + 1):
            if s[j] == '-' or s[j].isdigit():
                n_str += s[j]
                continue
            break

        n = int(n_str)
        root = TreeNode(n)

        bal = 0
        l_s = float('inf')
        l_e = float('-inf')
        for i in range(j, end+1, 1):
            if s[i] == '(':
                l_s = min(i, l_s)
                bal += 1
            elif s[i] == ')':
                l_e = max(i, l_e)
                bal -= 1

            if bal == 0:
                break

        bal = 0
        r_s = float('inf')
        r_e = float('-inf')
        for i in range(end, start-1, -1):
            if s[i] == '(':
                r_s = min(i, r_s)
                bal += 1
            elif s[i] == ')':
                r_e = max(i, r_e)
                bal -= 1
            if bal == 0:
                break

        if (r_s, r_e) == (l_s, l_e):
            r_s, r_e = float('inf'), float('-inf')

        root.left = self.dfs(s, l_s + 1, l_e - 1)
        root.right = self.dfs(s, r_s + 1, r_e - 1)

        return root


class Solution2(object):
    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        if len(s) == 0:
            return
        return self.dfs(s, 0, len(s) - 1)

    def dfs(self, s, start, end):
        if start > end:
            return

        j = s.find('(', start, end+1)
        if j < 0:
            sub = s[start:end+1]
            return TreeNode(int(sub))

        sub = s[start:j]
        n = int(sub)
        root = TreeNode(n)

        bal = 0
        i = 0
        for i in range(j, end + 1, 1):
            if s[i] == '(':
                bal += 1
            elif s[i] == ')':
                bal -= 1
            if bal == 0:
                break

        l_s = j + 1
        l_e = i - 1

        r_s = i + 2
        r_e = end - 1

        root.left = self.dfs(s, l_s , l_e )
        root.right = self.dfs(s, r_s , r_e )

        return root

s = "4(2(3)(1))(6(5))"
# s = "4(2(3)(1)))"
# s = "6(5)"
# s = "2(3)(1)"
# s = "-1"


a = Solution2().str2tree(s)
tree = BinaryTree()
tree.drawtree(a)

