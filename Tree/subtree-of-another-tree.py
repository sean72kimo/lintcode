import collections


class Solution(object):
    def isSubtree(self, s, t):
        """
        :type s: TreeNode (size of n)
        :type t: TreeNode (size of m)
        :rtype: bool

        nested dfs,
        注意！subtree的條件是包含葉子節點，每一個點都需一樣，並不是中間一坨一樣就可以了，
        所以dfs裡面判斷的條件是 return left and right, 而不是 left or right

        time: m * n
        space: n
        """
        if not s and not t:
            return True

        if (s and not t) or (not s and t):
            return False

        a = self.dfs(s, t)
        b = self.isSubtree(s.left, t)
        c = self.isSubtree(s.right, t)
        return a or b or c

    def dfs(self, p, q):
        if not p and not q:
            return True

        if (p and not q) or (not p and q):
            return False

        if p.val != q.val:
            return False

        left = self.dfs(p.left, q.left)
        right = self.dfs(p.right, q.right)

        return left and right


class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        mps = collections.defaultdict(int)
        mpt = collections.defaultdict(int)
        encode = self.dfs(t, "", mpt)
        self.dfs(s, "", mps)

        return mps[encode] > 0

    def dfs(self, root, path, mp):
        if not root:
            return '#'

        left = self.dfs(root.left, path, mp)
        right = self.dfs(root.right, path, mp)
        path += left + ',' + right + ',' + str(root.val)
        mp[path] += 1

        return path


class Solution(object):
    def isSubtree(self, s, t):
        """
        :type s: TreeNode
        :type t: TreeNode
        :rtype: bool
        """
        self.mp = set()
        self.ans = False
        serial_t = self.encode(t)
        self.mp.add(serial_t)

        serial_s = self.encode(s)

        return self.ans

    def encode(self, root):
        if not root:
            return "#"
        left = self.encode(root.left)
        right = self.encode(root.right)
        serial = "{},{},{}".format(root.val, left, right)
        if serial in self.mp:
            self.ans = True
        return serial




