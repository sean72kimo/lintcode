# https://www.lintcode.com/problem/474/
# time O(h)
# space O(h)
class Solution:
    """
    @param: root: The root of the tree
    @param: A: node in the tree
    @param: B: node in the tree
    @return: The lowest common ancestor of A and B
    """
    def lowestCommonAncestorII(self, root, A, B):
        if not root or A.val == root.val or B.val == root.val:
            return root
        
        hs = set()
        while B:
            hs.add(B)
            B = B.parent
        
        while A:
            if A in hs:
                return A
            A = A.parent