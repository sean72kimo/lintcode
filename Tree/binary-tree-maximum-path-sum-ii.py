"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from lib.binarytree import BinaryTree
class Solution:
    """
    @param root: the root of binary tree.
    @return: An integer
    """
    def __init__(self):
        self.ans = float('-inf')
    def maxPathSum2(self, root):
        # write your code here
        if not root:
            return
        self.helper(root, [])
        return self.ans
        
    def helper(self, root, path):
        if not root:
            self.ans = max(sum(path), self.ans)
            return
        
        path.append(root.val)
        self.ans = max(sum(path), self.ans)
        self.helper(root.left, path)
        self.helper(root.right, path)
        path.pop()
data = '{-19,-3,-4,#,#,-5,-6,-7,-11}'        
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution().maxPathSum2(root)
print("ans:", a)

from sys import maxsize
class Solution2:
    """
    @param root: the root of binary tree.
    @return: An integer
    """
    def __init__(self):
        self.ans = -maxsize
    
    def maxPathSum2(self, root):
        # write your code here
        if not root:
            return -maxsize

        return self.helper(root)
        
    def helper(self, root):
        if not root:
            return -maxsize
            
        left = self.helper(root.left)
        right = self.helper(root.right)
        print(root.val, left, right)
        return root.val + max(0, left, right)

data = '{-19,-3,-4,#,#,-5,-6,-7,-11}'
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution2().maxPathSum2(root)
print("ans:", a)
