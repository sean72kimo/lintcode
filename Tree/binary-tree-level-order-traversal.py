"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        if root is None:
            return []

        res = []
        q = [root]
        while q:
            tmp = []
            new_q = []

            for node in q:
                tmp.append(node.val)

                if node.left:
                    new_q.append(node.left)
                if node.right:
                    new_q.append(node.right)

            res.append(tmp)
            q = new_q

        return res
data = "{3,9,20,#,#,15,7}"
from lib.binarytree import BinaryTree
tree = BinaryTree()
root = tree.deserialize(data)


print('ans:', Solution().levelOrder(root))

