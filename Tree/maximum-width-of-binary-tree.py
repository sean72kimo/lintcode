class Solution(object):
    def widthOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        que = [(root, 1)]

        ans = 0
        while que:
            new_q = []
            mx = float('-inf')
            mn = float('inf')

            for curr, i in que:
                mx = max(mx, i)
                mn = min(mn, i)
                ans = max(ans, mx - mn + 1)

                if curr.left:
                    new_q.append((curr.left, i * 2))
                if curr.right:
                    new_q.append((curr.right, i * 2 + 1))

            que = new_q

        return ans