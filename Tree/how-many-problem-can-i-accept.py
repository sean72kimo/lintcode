class Solution:
    """
    @param n: an integer
    @param k: an integer
    @return: how many problem can you accept
    """
    def canAccept(self, n, k):
        # Write your code here

        left = 1
        right = n
 
        rsum = [0] #running sum
        for i in range(1, n + 1):
            rsum.append(rsum[-1] + i)
        print(rsum)
 
        while left + 1 < right:
            mid = (right - left) // 2 + left
             
            if self.canFinish(n, k, mid, rsum):
                left = mid
            else:
                right = mid
         
        if self.canFinish(n, k, right, rsum):
            return right
        return left
         
    def canFinish(self, n, k, p, rsum):
        print(p, rsum[p])
        total = rsum[p] * k

        if total <= n:
            return True
        else:
            return False
n = 30
k = 1
a = Solution().canAccept(n, k)
print("ans:", a)