# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from typing import Optional

from lib.binarytree import TreeNode, BinaryTree

"""
simulate multiple children rather than just left and right child
"""
class Solution:
    def __init__(self):
        self.ans = float('-inf')

    def maximumAverageSubtree(self, root: Optional[TreeNode]) -> float:
        if not root:
            return 0
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return 0, 0.0

        total_cnt = 0
        total_val = 0
        for sub in [root.left, root.right]:
            cnt, val = self.dfs(sub)
            total_cnt += cnt
            total_val += val

        total_cnt += 1
        total_val += root.val

        self.ans = max(self.ans, total_val / total_cnt)

        return total_cnt, total_val

"""
regular tree traversal
"""
class Solution:
    def __init__(self):
        self.ans = float('-inf')

    def maximumAverageSubtree(self, root: Optional[TreeNode]) -> float:
        if not root:
            return 0
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return 0, 0.0

        left_cnt, left = self.dfs(root.left)
        right_cnt, right = self.dfs(root.right)

        cnt = left_cnt + right_cnt + 1
        v = left + right + root.val

        self.ans = max(self.ans, v / cnt)

        return cnt, v


arr = "[2,6,3,null,5,12,7,null,null,0,4,null,10,null,null,11,null,null,9,null,1,null,8]"
tree = BinaryTree()
root = tree.deserialize(arr)
tree.drawtree(root)

