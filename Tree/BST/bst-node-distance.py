"""
1561. BST Node Distance
https://www.lintcode.com/problem/bst-node-distance/
"""
from lib.binarytree import TreeNode, BinaryTree



class Solution:
    """
    @param numbers: the given list
    @param node1: the given node1
    @param node2: the given node2
    @return: the distance between two nodes
    """

    def bstDistance(self, nums, node1, node2):
        if node1 not in nums or node2 not in nums:
            return -1
        if len(nums) < 2:
            return -1

        root = None
        for n in nums:
            root = self.dfs(n, root)
        print(root)
        lca = self.find_lca(root, node1, node2)
        dist1 = self.find_dist(lca, node1)
        dist2 = self.find_dist(lca, node2)
        print(dist1, dist2)
        return dist1 + dist2

    def find_dist(self, root, val):
        print(root, val)
        if root.val == val:
            return 0

        left = right = 0
        if val > root.val:
            left = self.find_dist(root.right, val)
        elif val < root.val:
            right = self.find_dist(root.left, val)

        if left:
            return left + 1
        return right + 1

    def find_lca(self, root, p, q):

        if root.val == p or root.val == q:
            return root

        if p > root.val > q or p < root.val < q:
            return root
        elif p < root.val and q < root.val:
            return self.find_lca(root.left, p, q)
        elif p > root.val and q > root.val:
            return self.find_lca(root.right, p, q)

    def dfs(self, val, root):
        if not root:
            return TreeNode(val)

        if val > root.val:
            root.right = self.dfs(val, root.right)
        elif val < root.val:
            root.left = self.dfs(val, root.left)

        return root



nums = [7,4,3,5,8]
node1 = 8
node2 = 4
tree = BinaryTree()
sol = Solution()
a = sol.bstDistance(nums, node1, node2)



print("ans:", a)