class Solution:
    def inorderSuccessor(self, root: 'TreeNode', p: 'TreeNode') -> 'TreeNode':
        if not root:
            return

        curr = root
        succ = None
        while curr:
            if curr.val > p.val:
                succ = curr
                curr = curr.left
            elif curr.val <= p.val:
                curr = curr.right
        return succ

from lib.binarytree import BinaryTree, TreeNode

data = '{4,2,6,1,3,5,8,#,#,#,#,#,#,7,9}'
tree = BinaryTree()
root = tree.deserialize(data)
p = TreeNode(8)

node = Solution().inorderSuccessor(root, p)
print('ans', node)