# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def __init__(self):
        self.num = []

    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        self.preorder_helper(root)
        for i in range(1, len(self.num)):
            if self.num[i] <= self.num[i - 1]:
                return False

        return True


    def preorder_helper(self, root):
        if not root:
            return

        self.preorder_helper(root.left)
        self.num.append(root.val)
        self.preorder_helper(root.right)

# preorder traversal
class Solution(object):
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        return self.dfs(root, float('inf'), float('-inf'))

    def dfs(self, root, max_v, min_v):
        if not root:
            return True

        if root.left:
            if root.val <= root.left.val:
                return False
            if root.left.val <= min_v:
                return False

        if root.right:
            if root.val >= root.right.val:
                return False
            if root.right.val >= max_v:
                return False

        l = self.dfs(root.left, root.val, min_v)
        r = self.dfs(root.right, max_v, root.val)

        return l and r


nums = [1,2,3,4,5,6,7]
print(nums[:-1])
print(nums[:-1][:2])