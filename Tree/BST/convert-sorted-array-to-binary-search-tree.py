# Definition for a binary tree node.
from lib.binarytree import BinaryTree, TreeNode

class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if len(nums) == 0:
            return None
        return self.dfs(nums, 0, len(nums) - 1)

    def dfs(self, nums, start, end):
        if start == end:
            return TreeNode(nums[start])
        if start > end:
            return None

        mid = (start + end) // 2
        v = nums[mid]
        root = TreeNode(v)
        root.left = self.dfs(nums, start, mid - 1)
        root.right = self.dfs(nums, mid + 1, end)

        return root

class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if nums is None or len(nums) == 0:
            return

        mid = len(nums) // 2
        root = TreeNode(nums[mid])
        left = self.sortedArrayToBST(nums[:mid])
        right = self.sortedArrayToBST(nums[mid + 1:])
        root.left = left
        root.right = right

        return root


nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
p = TreeNode(1)
q = TreeNode(4)
root = Solution().sortedArrayToBST(nums)



