# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def getMinimumDifference(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        self.ans = float('inf')
        self.prev = float('-inf')
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return

        self.dfs(root.left)
        d = root.val - self.prev
        self.prev = root.val
        self.ans = min(self.ans, d)
        self.dfs(root.right)