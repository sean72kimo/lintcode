from lib.binarytree import BinaryTree, TreeNode
from lib.dlinkedlist import DoubleLinkedListUtil

class Solution(object):
    def __init__(self):
        self.prev = None

    def treeToDoublyList(self, root):
        """
        :type root: Node
        :rtype: Node
        """
        if not root:
            return
        if not root.left and not root.right:
            root.right = root
            root.left = root
            return root

        dummy = Node(0, None, None)
        self.prev = dummy
        self.helper(root)
        self.prev.right = dummy.right
        dummy.right.left = self.prev
        return dummy.right

    def helper(self, root):
        if not root:
            return

        self.helper(root.left)
        self.prev.right = root
        root.left = self.prev
        self.prev = root
        self.helper(root.right)

nums = [1, 2, 3, 4, 5]
a = list(enumerate(nums))
print(a)
