# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from lib.binarytree import BinaryTree, TreeNode
class Solution(object):
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return None

        if p.val == root.val or q.val == root.val:
            return root
        
        if (p.val < root.val < q.val) or (q.val < root.val < p.val):
            return root
        
        if p.val < root.val and q.val < root.val:
            return self.lowestCommonAncestor(root.left, p, q)
            
        if p.val > root.val and q.val > root.val:
            return self.lowestCommonAncestor(root.right, p, q)
        
tree = BinaryTree()
data = '[6,2,8,0,4,7,9,#,#,3,5]'
root = tree.deserialize(data)
# tree.drawtree(root)
sol = Solution()
p = TreeNode(3)
q = TreeNode(5)
a = sol.lowestCommonAncestor(root, p, q)
print(a.val)
