from lib.binarytree import BinaryTree
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

class BST:
    def __init__(self):
        self.root = None

    def contains(self, val):
        return self.containsHelper(self.root, val)

    def containsHelper(self, root, val):
        if not root:
            return False

        if val > root.val:
            return self.containsHelper(root.right, val)
        elif val < root.val:
            return self.containsHelper(root.left, val)
        else:
            return True

        return False

    def insert(self, val):
        self.root = self.insertHelper(self.root, val)

    def insertHelper(self, root, val):
        if root is None:
            return TreeNode(val)

        if val > root.val:
            root.right = self.insertHelper(root.right, val)
        elif val < root.val:
            root.left = self.insertHelper(root.left, val)

        return root

    def delete(self, val):
        self.root = self.deleteHelper(self.root, val)

    def deleteHelper(self, root, val):
        if not root:
            return None

        if val > root.val:
            root.right = self.deleteHelper(root.right, val)
        elif val < root.val:
            root.left = self.deleteHelper(root.left, val)
        else:
            if not root.left and not root.right:
                return None
            if not root.left:
                return root.right
            if not root.right:
                return root.left

            node = self.findNext(root.right)
            root.val = node.val
            root.right = self.deleteHelper(root.right, root.val)

        return root

    def findNext(self, root):
        if not root:
            return None

        while root.left:
            root = root.left
        return root

    def valid(self):
        return self.validHelper(self.root, float('-inf'), float('inf'))
    
    def validHelper(self, root, mn, mx):
        if not root:
            return True

        if root.val <= mn or root.val >= mx:
            return False

        left = self.validHelper(root.left, mn, root.val)
        right = self.validHelper(root.right, root.val, mx)
        
        return left and right
        
bt = BinaryTree()
tree = BST()

tree.insert(5)
tree.insert(2)
tree.insert(1)
tree.insert(6)
tree.insert(8)
tree.insert(4)
tree.insert(7)
tree.insert(3)

bt.drawtree(tree.root)
print(tree.valid() == True)
tree.delete(4)
print(tree.valid() == True)
bt.drawtree(tree.root)
tree.delete(7)
print(tree.valid() == True)
bt.drawtree(tree.root)

print(tree.contains(4) == False)
print(tree.contains(7) == False)
#
print(tree.contains(8) == True)
print(tree.contains(5) == True)






data = '{8,3,10,1,6,#,14,#,#,4,7,13}'
tree = BinaryTree()
root = tree.deserialize(data)

bst = BST()
bst.insert(8)
bst.insert(10)
bst.insert(2)
bst.insert(3)
res = bst.inorderTraversal_dfs(bst.root)
print(res)
print(bst.contains(5))
print(bst.contains(8))
print(bst.contains(10))
print(bst.contains(3))




