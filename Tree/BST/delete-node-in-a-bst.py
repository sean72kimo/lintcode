class Solution:
    def deleteNode(self, root, key):
        """
        :type root: TreeNode
        :type key: int
        :rtype: TreeNode
        """
        if not root:
            return None
        
        if key > root.val:
            root.right = self.deleteNode(root.right, key)
        elif key < root.val:
            root.left = self.deleteNode(root.left, key)
        else:
            if not root.left and not root.right:
                return None
            if not root.left:
                return root.right
            if not root.right:
                return root.left
            
            node = self.findNext(root.right)
            root.val = node.val
            root.right = self.deleteNode(root.right, node.val)
        
        return root
    
    def findNext(self, root):
        if not root:
            return 
        
        while root.left:
            root = root.left
        return root
