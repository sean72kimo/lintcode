# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from lib.binarytree import BinaryTree, TreeNode
class Solution:
    """
    @param A: an array
    @return: is it a complete binary search tree
    """
    # return boolean
    def isCompleteBinarySearchTree(self, A):
        # Write your code here
        if len(A) == 0:
            return True
        root = None
        for val in A:
            root = self.insert(root, val)
        print(BinaryTree().serialize(root))
        if not self.isComplete(root):
            return False
        mainRoot = root
        
        left = True
        while left and root:
            left = self.isComplete(root)
            if left:
                root = root.left
            
        if left is False:
            return False
        
        root = mainRoot
        right = True
        while right and root:
            right = self.isComplete(root)
            if right:
                root = root.right
                
        if right is False:
            return False
        

        return True
            
    # return TreeNode    
    def insert(self, root, val):
        if root is None:
            return TreeNode(val)
            
        if val < root.val:
            root.left = self.insert(root.left, val)
        else:
            root.right = self.insert(root.right, val)
        
        return root
    
    # return boolean
    def isComplete(self, root):
        if not root:
            return True

        if not root.left and not root.right:
            return True

        if root.left and root.right:
            return True
            
        if root.left and not root.right:
            return True
        
        if not root.left and root.right:
            return False


A = [10,5,20,3,6,15,1]
sol = Solution()
ans = sol.isCompleteBinarySearchTree(A)
print("ans:", ans)
