from lib.binarytree import TreeNode, BinaryTree


class Solution(object):
    def splitBST(self, root, target):
        """
        :type root: TreeNode
        :type V: int
        :rtype: List[TreeNode]

        每一層回傳 [smaller_than_V, bigger_than_V]
        if root >  V , 往左邊(小邊)找，此層的bigger_than_V  就是root, 此層的smaller_than_V則看小邊傳回來的smaller是什麼
        題目要求大於V的全部放在一棵樹，所以將小邊(root.left)接上bigger, 保證此root下面全部都大於V

        if root <= V , 往右邊(大邊)找，此層的smaller_than_V 就是root, 此層的bigger_than_V 則看大邊傳回來的bigger 是什麼
        題目要求小於V的全部放在一棵樹，所以將大邊(root.right)接上smaller, 保證此root下面全部都小於V
        """
        if not root:
            return [None, None]

        if root.val > V:
            smaller, bigger = self.splitBST(root.left, V)
            root.left = bigger
            return [smaller, root]
        elif root.val <= V:
            smaller, bigger = self.splitBST(root.right, V)
            root.right = smaller
            return [root, bigger]

data = '[4,2,6,1,3,5,7]'
V = 2.5
tree = BinaryTree()


root = tree.deserialize(data)

a = Solution().splitBST(root, V)
print(a)
# tree.drawtree(a[0])
# tree.drawtree(a[1])
