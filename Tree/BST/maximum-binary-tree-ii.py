from lib.binarytree import TreeNode

class Solution(object):
    def insertIntoMaxTree(self, root, val):
        """
        :type root: TreeNode
        :type val: int
        :rtype: TreeNode
        """
        if not root:
            return TreeNode(val)

        if root.val > val:
            root.right = self.insertIntoMaxTree(root.right, val)
            return root
        else:
            node = TreeNode(val)
            node.left = root
            return node
