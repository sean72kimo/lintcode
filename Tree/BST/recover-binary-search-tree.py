# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def recoverTree(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        if not root:
            return

        self.i = None
        self.j = None
        self.prev = None
        self.dfs(root)
        self.i.val, self.j.val = self.j.val, self.i.val

    def dfs(self, root):  # inorder
        if not root:
            return

        self.dfs(root.left)

        if not self.prev:
            self.prev = root

        elif root.val < self.prev.val:
            if self.i is None:
                self.i = self.prev
            self.j = root

        self.prev = root

        self.dfs(root.right)