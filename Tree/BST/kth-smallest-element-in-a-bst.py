from lib.binarytree import TreeNode, BinaryTree
import collections

class Solution_dfs_binary_search(object):
    def kthSmallest(self, root, k):
        count = self.countNode(root.left)
        print(root, count)
        if k <= count:
            return self.kthSmallest(root.left, k)
        elif k > count + 1:
            return self.kthSmallest(root.right, k - count - 1)

        return root.val  # k == count + 1

    def countNode(self, root):
        if root is None:
            return 0
        return 1 + self.countNode(root.left) + self.countNode(root.right)


class Solution_dfs_inorder_traversal(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int

        dfs_inorder_traversal & inorder_traversal_with_stack
        兩種方法time complexity 皆是 O(h + k), space O(h)
        要先走到最左邊(最小值)耗時O(h)，接著看k為多少，走k步
        """
        if not root:
            return

        self.ans = 0
        self.k = k
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return

        self.dfs(root.left)
        self.k -= 1
        if self.k == 0:
            self.ans = root.val

        self.dfs(root.right)

class Solution_inorder_stack(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        if not root:
            return


        stack = []
        curr = root

        while stack or curr:
            if curr:
                stack.append(curr)
                curr = curr.left
            else:
                curr = stack.pop()
                k -= 1
                if k == 0:
                    return curr.val
                # inorder process
                curr = curr.right
        return -1

data = ('[3,1,4,#,2]', 1)
data = ('[5,3,6,2,4,#,#,1]', 3)
data = ('[5,3,6,2,4,#,#,1]', 4)
data = ('[5,3,6,2,4,#,#,1]', 5)
data = ('[5,3,6,2,4,#,#,1]', 6)
# data = ('[41,37,44,24,39,42,48,1,35,38,40,#,43,46,49,0,2,30,36,#,#,#,#,#,#,45,47,#,#,#,#,#,4,29,32,#,#,#,#,#,#,3,9,26,#,31,34,#,#,7,11,25,27,#,#,33,#,6,8,10,16,#,#,#,28,#,#,5,#,#,#,#,#,15,19,#,#,#,#,12,#,18,20,#,13,17,#,#,22,#,14,#,#,21,23]', 25)
string = data[0]
k = data[1]
tree = BinaryTree()
root = tree.deserialize(string)
# tree.drawtree(root)
a = Solution_dfs_binary_search().kthSmallest(root, k)
print("ans:", a)
