class Solution:
    """
    @param: root: The root of the BST.
    @param: p: You need find the successor node of p.
    @return: Successor of p.
    """
    def inorderSuccessor(self, root, p):
        # write your code here

        if root is None or p is None:
            return None

        if root.val <= p.val:
            node = self.inorderSuccessor(root.right, p)

        else:
            node = self.inorderSuccessor(root.left, p)
            if node:
                print(' ', node.val)
            else:
                node = root

        return node


from lib.binarytree import BinaryTree, TreeNode

data = '{4,2,6,1,3,5,8,#,#,#,#,#,#,7,9}'
tree = BinaryTree()
root = tree.deserialize(data)
p = TreeNode(8)

node = Solution().inorderSuccessor(root, p)
print('ans', node)
