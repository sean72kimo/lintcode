"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: root: the root of binary tree
    @return: the new root
    """
    def convertBST(self, root):
        # write your code here
        self.dfs(root)
        return root

    def dfs(self, curr):
        if curr is None:
            return

        self.dfs(curr.right)
        self.sum += curr.val
        curr.val = self.sum
        self.dfs(curr.left)
