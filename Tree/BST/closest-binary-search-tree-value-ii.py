# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import heapq




class Solution(object):
    def closestKValues(self, root, target, k):
        """
        :type root: TreeNode
        :type target: float
        :type k: int
        :rtype: List[int]
        272: time: O(H), space O(H),
        建立一個smaller arr存著小於target的點，
        建立一bigger arr存著小於target的點，
        (從root往樹底，左右左右分別將大於和小於target的點，存於smaller and bigger arr)

        smaller arr最後一個元素，是所有小於target中，最大最接近target，
        bigger  arr最後一個元素，是所有大於target中，最小最接近target，
        while len(ans) < k, 找k個點
        每次都比較一下，是smaller[-1]比較靠近，還是bigger[-1]比較靠近？
        取較靠近的那個。

        並且更新smaller arr / bigger arr
        取了smaller / bigger node之後
        以smaller node為例，
        node.left (必定更小於node), (node.right 必定大於node, 甚至可能大於 taget)
        node.left裡面，往右邊走，路上每一個點必定都小於target,且越來越接近target
        將一路上的點存回smaller, 結束後，smaller[-1]又是所有小於target中，最大最接近target，

        """

        bigger = []
        smaller = []
        node = root

        ret = []
        while node:
            if node.val < target:
                smaller.append(node)
                node = node.right
            else:
                bigger.append(node)
                node = node.left

        while len(ret) < k:
            dist_smaller = float('inf')
            dist_bigger = float('inf')

            if len(smaller):
                dist_smaller = target - smaller[-1].val

            if len(bigger):
                dist_bigger = bigger[-1].val - target

            if len(bigger) == 0 or (len(smaller) != 0 and dist_smaller < dist_bigger):
                ret.append(smaller[-1].val)
                self.get_next_smaller(smaller)
            else:
                ret.append(bigger[-1].val)
                self.get_next_bigger(bigger)

        return ret

    def get_next_bigger(self, bigger):
        node = bigger.pop().right
        while node:
            bigger.append(node)
            node = node.left

    def get_next_smaller(self, smaller):
        node = smaller.pop().left
        while node:
            smaller.append(node)
            node = node.right


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class SolutionHeap(object):
    """
    Time O(NlogK)
    Space O(K)
    """
    def closestKValues(self, root, target, k):
        """
        :type root: TreeNode
        :type target: float
        :type k: int
        :rtype: List[int]
        """

        heap = []
        self.dfs(root, target, k, heap)
        ans = []
        for diff, val in heap:
            ans.append(val)
        return ans

    def dfs(self, root, target, k, heap):
        if not root:
            return

        self.dfs(root.left, target, k, heap)

        item = (-abs(target - root.val), root.val)
        heapq.heappush(heap, item)
        if len(heap) > k:
            heapq.heappop(heap)

        self.dfs(root.right, target, k, heap)
from lib.binarytree import BinaryTree
tree = BinaryTree()
data = '{4,2,5,1,3}'
data = '{2,1}'
# data = '{0}'
root = tree.deserialize(data)
target = 4.14285
# target = 3.714
# target = 2147483648.0
k = 2
# k = 1
a = Solution().closestKValues(root, target, k)
print("ans:", a)
