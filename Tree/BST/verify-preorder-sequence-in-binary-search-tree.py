class Solution(object):

    def verifyPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        if len(preorder) <= 1:
            return True

        root = preorder[0]
        right_start = 0
        for i, v in enumerate(preorder):
            if v > root:
                right_start = i
                break
        if right_start == 0:
            left = preorder[:]
            right = []
        else:
            left = preorder[1:right_start]
            right = preorder[right_start:]

        print(root, '/', left, '/', right)
        for v in left:
            if v > root:
                return False

        for v in right:
            if v < root:
                return False

        ll = self.verifyPreorder(left)
        rr = self.verifyPreorder(right)

        return ll and rr


class SolutionStack:

    def verifyPreorder(self, pre):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        stack = []
        low = float('-inf')
        for p in pre:
            if p < low:
                return False

            while stack and p > stack[-1]:
                    low = stack.pop()

            stack.append(p)
        return True


class Solution3(object):

    def verifyPreorder(self, pre):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        if len(pre) == 0:
            return True

        lb = float('-inf')
        i = 0
        for n in pre:
            if n < lb:
                return False

            while i > 0 and n > preorder[i - 1]:
                lb = preorder[i - 1]
                i -= 1

            preorder[i] = n
            i += 1

        return True


preorder = [8, 3, 1, 6, 4, 7, 10, 14, 13]
preorder = [1]
preorder = [5, 3]
preorder = [3, 5]
preorder = [27, 14, 10, 19, 16, 21, 35, 31, 42]
preorder = [5, 2, 1, 3, 6]

a = SolutionStack().verifyPreorder(preorder)
print("ans:", a)
