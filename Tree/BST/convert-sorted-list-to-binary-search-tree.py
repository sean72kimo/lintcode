from lib.linkedlist import list2LinkedList, printLinkedList
from lib.binarytree import BinaryTree
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):

    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """
        if not head:
            return None

        if not head.next:
            return TreeNode(head.val)

        slow = head
        fast = head.next
        prev = None

        while fast and fast.next:
            prev = slow
            slow = slow.next
            fast = fast.next.next

        if prev:
            prev.next = None
        else:
            head = None

        mid = slow

        root = TreeNode(mid.val)
        root.left = self.sortedListToBST(head)
        root.right = self.sortedListToBST(mid.next)

        return root


    def sortedListToBST2(self, head):
        if head:
            print("[head]", head.val)
        else:
            print("[head]", "None")

        if not head:
            return

        if not head.next:

            return TreeNode(head.val)


        slow, fast = head, head.next.next
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        # print (head.val, slow.val)
        printLinkedList(head)

        # tmp points to root
        tmp = slow.next
        # cut down the left child
        slow.next = None
        root = TreeNode(tmp.val)
        # print("send head {} to recursive".format(head.val))
        root.left = self.sortedListToBST2(head)
        root.right = self.sortedListToBST2(tmp.next)
        return root

linkedlist = [-10, -3, 0, 5, 9]
# linkedlist = [-10, -3]



head = list2LinkedList(linkedlist)
root = Solution().sortedListToBST(head)
# root = Solution().sortedListToBST2(head)
print(BinaryTree().serialize(root))
