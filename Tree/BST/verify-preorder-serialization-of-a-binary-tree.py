"""
一開始完全沒有data, return True
若是空#, return True
得到左子，但是data卻用完了，無法產生右子，return False
整棵樹接完，data必須也用光 self.i == len(data)
"""
import collections


class Solution(object):
    def isValidSerialization(self, preorder):
        """
        :type preorder: str
        :rtype: bool
        """
        data = preorder.strip().split(',')
        self.i = 0
        return self.dfs(data) and self.i == len(data)

    def dfs(self, data):
        if self.i == len(data):
            return True

        node = data[self.i]
        self.i += 1

        if node == '#':
            return True

        l, r = True, True
        l = self.dfs(data)
        if self.i == len(data):
            return False
        r = self.dfs(data)

        return l and r

class Solution(object):
    def isValidSerialization(self, preorder):
        """
        :type preorder: str
        :rtype: bool
        """
        pre = preorder.strip().split(',')
        data = collections.deque(pre)
        return self.dfs(data) and len(data) == 0

    def dfs(self, data):
        if len(data) == 0:
            return True

        node = data.popleft()

        if node == '#':
            return True

        l = self.dfs(data)
        if len(data) == 0:
            return False
        r = self.dfs(data)

        return l and r

