# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """

        ans = root
        minD = float('inf')
        while root:
            diff = abs(target - root.val)
            
            if diff < minD:
                minD = diff
                ans = root
            
            if target < root.val:
                root = root.left

            elif target >= root.val:
                root = root.right
            
            else:
                return root.val
            
        return ans.val


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """
        if not root:
            return 0
        self.ans = None
        self.diff = float('inf')
        self.dfs(root, target)
        return self.ans

    def dfs(self, root, target):
        if not root:
            return

        if root.val > target:
            if root.val - target < self.diff:
                self.ans = root.val
                self.diff = root.val - target
            self.dfs(root.left, target)

        elif root.val < target:
            if target - root.val < self.diff:
                self.ans = root.val
                self.diff = target - root.val
            self.dfs(root.right, target)
        else:
            self.ans = root.val
            return
        return
from lib.binarytree import BinaryTree
tree = BinaryTree()
data = '{2147483647}'
target = 0.0
data = '[4,2,5,1,3]'
target = 3.714286
root = tree.deserialize(data)

a = Solution().closestValue(root, target)
print("ans:", a)