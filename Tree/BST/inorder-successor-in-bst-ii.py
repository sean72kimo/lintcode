class Node(object):
    def __init__(self, val, left, right, parent):
        self.val = val
        self.left = left
        self.right = right
        self.parent = parent
    def __repr__(self):
        return "<Node {}>".format(self.val)


class Solution:
    """
    透過node.parent一路找到root, 然後重複285的動作
    從root，往下走一路上紀錄比target大的點，最後一個被記錄的點就是successor
    """
    def inorderSuccessor(self, node):
        curr = node
        while curr and curr.parent:
            curr = curr.parent

        p = node
        succ = None
        while curr:
            if curr.val > p.val:
                succ = curr
                curr = curr.left
            elif curr.val <= p.val:
                curr = curr.right
        return succ


class Solution(object):
    def inorderSuccessor(self, node):
        """
        :type node: Node
        :rtype: Node
        """

        ans = None

        curr = node.parent
        while curr:
            if not curr:
                break
            if curr.val > node.val and (not ans or curr.val < ans.val):
                ans = curr
            curr = curr.parent

        curr = node.right
        while curr:
            if not curr:
                break
            if curr.val > node.val and (not ans or curr.val < ans.val):
                ans = curr
            curr = curr.left

        return ans

n1 = Node(1, None, None, None)
n2 = Node(2, None, None, None)
n3 = Node(3, None, None, None)
n1.parent = n2
n2.left = n1
n2.right = n3
n3.parent = n2
a = Solution().inorderSuccessor(n1)
print("ans:", a)