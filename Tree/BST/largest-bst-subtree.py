from lib.binarytree import TreeNode, BinaryTree

# solution 1
class SubTree(object):
    def __init__(self, n, mn, mx):
        self.max = mx
        self.min = mn
        self.n = n


class Solution(object):
    def largestBSTSubtree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        self.ans = 0
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return SubTree(0, float('inf'), float('-inf'))

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        if left.max < root.val < right.min:
            n = left.n + right.n + 1
            node = SubTree(n, min(left.min, root.val), max(right.max, root.val))
        else:
            node = SubTree(float('-inf'), float('inf'), float('-inf'))

        self.ans = max(node.n, left.n, right.n, self.ans)
        return node

# solution 2
class SubTree(object):
    def __init__(self, largest, n, min, max):
        self.largest = largest  # largest BST
        self.n = n              # number of nodes in this ST
        self.min = min          # min val in this ST
        self.max = max          # max val in this ST

class Solution(object):
    def largestBSTSubtree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        res = self.dfs(root)

        return res.largest

    def dfs(self, root):

        if not root:
            return SubTree(0,0, float('inf'), float('-inf'))

        left = self.dfs(root.left)
        right = self.dfs(root.right)

        if left.max < root.val < right.min:
            n = left.n + right.n + 1
        else:
            n = float('-inf')

        print('at', root, n, left.largest, right.largest)
        largest = max(n, left.largest, right.largest)
        return SubTree(largest, n, min(left.min, root.val), max(right.max, root.val))



data = '[10,5,15,1,8,null,7]'
exp = 3
# data = '[3,2,4,null,null,1]'
# exp = 2
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution().largestBSTSubtree(root)

print("ans:", a==exp, a)
