# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
import collections

from lib.binarytree import BinaryTree, TreeNode


class Codec1:
    """
    O(n) serialize
    O(n) deserialize
    """
    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        if not root:
            return ""

        path = []
        self.build_str(root, path)
        ans = ','.join(path)
        print(ans)
        return ans

    def build_str(self, root, path):
        if not root:
            return

        path.append(str(root.val))
        self.build_str(root.left, path)
        self.build_str(root.right, path)

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if len(data) == 0:
            return
        data = data.split(',')
        nodes = list(map(int, data))

        return self.build_tree(nodes, [0], float('inf'))

    def build_tree(self, nodes, i, MAX):

        if i[0] >= len(nodes) or nodes[i[0]] >= MAX:
            return

        root = TreeNode(nodes[i[0]])
        i[0] += 1
        root.left = self.build_tree(nodes, i, root.val)
        root.right = self.build_tree(nodes, i, MAX)

        return root


class Codec:

    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        if not root:
            return ""
        path = []
        self.build_str(root, path)

        return ','.join(path)

    def build_str(self, root, path):
        if not root:
            return

        path.append(str(root.val))
        self.build_str(root.left, path)
        self.build_str(root.right, path)

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """
        if len(data) == 0:
            return

        tmp = data.strip().split(',')

        return self.build_tree(tmp, 0, len(tmp) - 1)

    def build_tree(self, path, start, end):
        if len(path) == 0:
            return

        if start == end:
            return TreeNode(int(path[start]))

        if start > end:
            return

        v = int(path[start])
        root = TreeNode(v)

        left = [start + 1, end]
        right = [0, -1]
        for i in range(start + 1, len(path)):
            if int(path[i]) > v:
                left = [start + 1, i - 1]
                right = [i, end]
                break

        root.left = self.build_tree(path, left[0], left[1])
        root.right = self.build_tree(path, right[0], right[1])

        return root


class Codec:
    """
    O(n) serialize
    O(nlogn) to O(n^2) deserialize
    """
    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        if not root:
            return ""
        path = []
        self.build_str(root, path)
        return ','.join(path[:])

    def build_str(self, root, path):
        if not root:
            return
        path.append(str(root.val))
        self.build_str(root.left, path)
        self.build_str(root.right, path)

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """
        if len(data) == 0:
            return

        data = data.strip().split(',')

        data = list(map(int, data))

        return self.build_tree(data)

    def build_tree(self, que):
        if len(que) == 0:
            return

        v = que.pop(0)
        root = TreeNode(v)

        left = que
        right = []

        for i in range(len(que)):
            if que[i] > v:
                left = que[:i]
                right = que[i:]
                break


        root.left = self.build_tree(left)
        root.right = self.build_tree(right)

        return root

data = '[8,3,10,1,6,#,14,#,#,4,7,13]'
# data = '[3,1]'
tree = BinaryTree()
root = tree.deserialize(data)

codec = Codec1()
string = codec.serialize(root)
print(string)
r = codec.deserialize(string)
tree.drawtree(r)