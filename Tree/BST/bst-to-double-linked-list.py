from lib.binarytree import BinaryTree, TreeNode
from lib.dlinkedlist import DoubleLinkedListUtil
class Solution:
    def __init__(self):
        self.head = None
        self.prev = None
    def bst2dll(self, root):
        if not root:
            return
        self.helper(root)
        return self.head

    def helper(self, root):
        if not root:
            return
        self.helper(root.left)
        if self.prev is None:
            self.head = root
            self.prev = root
        else:
            root.left = self.prev
            self.prev.right = root
            self.prev = root
        self.helper(root.right)

data = '[27, 14, 35, 10, 19, 31, 42]'
tree = BinaryTree()
root = tree.deserialize(data)
root = Solution().bst2dll(root)

prev = None
fwd = []
rev = []
def printLinkedList(head):
    global prev
    while head:
        fwd.append(head.val)
        print(head.val, end = ' <-> ')
        prev = head
        head = head.right
    print('None')
print(prev)

def printLinkedListReverse(head):
    while head:
        rev.append(head.val)
        print(head.val, end = ' <-> ')
        head = head.left
    print('None')
printLinkedList(root)
printLinkedListReverse(prev)

for i , n in enumerate(fwd):
    if i == 0:
        continue
    if n < fwd[i-1]:
        print(False)
print(fwd == rev[::-1])
