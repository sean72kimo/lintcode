from lib.binarytree import TreeNode, BinaryTree

class Solution(object):
    def bstFromPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: TreeNode
        """
        if len(preorder) == 0:
            return None

        if len(preorder) == 1:
            return TreeNode(preorder[0])
        self.idx = 0

        return self.dfs(preorder, float('-inf'), float('inf'))

    def dfs(self, preorder, lower, upper):
        if self.idx == len(preorder):
            return

        v = preorder[self.idx]
        if v < lower or v > upper:
            return

        self.idx += 1
        root = TreeNode(v)
        root.left = self.dfs(preorder, lower, v)
        root.right = self.dfs(preorder, v, upper)
        return root

class Solution2(object):
    def bstFromPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: TreeNode
        """
        if len(preorder) == 1:
            return TreeNode(preorder[0])
        if len(preorder) == 0:
            return None

        v = preorder.pop(0)
        root = TreeNode(v)

        mx  = max(preorder)
        if mx < v:
            left = preorder
            right = []
        else:
            for i in range(len(preorder)):
                if preorder[i] > v:
                    break

            left = preorder[:i]
            right = preorder[i:]

        root.left = self.bstFromPreorder(left)
        root.right = self.bstFromPreorder(right)

        return root

preorder = [8,5,1,7,10,12]
# preorder = [4,2]
# preorder = [3,1,2]
sol = Solution2()
a = sol.bstFromPreorder(preorder)

tree = BinaryTree()
tree.drawtree(a)