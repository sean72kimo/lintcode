# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from lib.binarytree import BinaryTree


class Solution_bfs(object):
    def findTarget(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: bool
        O(n) time, O(n) space
        """
        vst = set()
        que = [root]

        while que:
            new_q = []
            for curr in que:
                # curr.val + x = k
                x = k - curr.val
                if x in vst:
                    return True
                vst.add(curr.val)

                if curr.left:
                    new_q.append(curr.left)
                if curr.right:
                    new_q.append(curr.right)
            que = new_q
        return False

class Solution_stack_as_iter(object):
    def findTarget(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: bool
        """
        # BST iterator
        left = []
        right = []

        curr = root
        while curr:
            left.append(curr)
            curr = curr.left

        curr = root
        while curr:
            right.append(curr)
            curr = curr.right

        while len(left) and len(right) and left[-1].val != right[-1].val:
            summ = left[-1].val + right[-1].val
            if summ == k:
                return True
            if summ < k:
                l = left.pop()
                node = l.right
                while node:
                    left.append(node)
                    node = node.left
            elif summ > k:
                r = right.pop()
                node = r.left
                while node:
                    right.append(node)
                    node = node.right
        return False


class Solution(object):
    def findTarget(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: bool
        """
        left = []
        node = root
        while node:
            left.append(node)
            node = node.left
        i = left[-1]

        right = []
        node = root
        while node:
            right.append(node)
            node = node.right
        j = right[-1]

        while i and j and i.val < j.val:
            val = i.val + j.val
            if val == k:
                return True

            elif val < k:
                if len(left) == 0:
                    i = None
                    continue

                node = left.pop().right
                while node:
                    left.append(node)
                    node = node.left
                i = left[-1] if len(left) else None

            elif val > k:
                if len(right) == 0:
                    j = None
                    continue

                node = right.pop().left
                while node:
                    right.append(node)
                    node = node.right
                j = right[-1] if len(right) else None

        return False

class BSTIterSm:
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.ele = None
        self.path = []
        self.push(root)

    def push(self, curr):
        if not curr:
            return

        while curr:
            self.path.append(curr)
            curr = curr.left

    def next(self):
        """
        @return the next smallest number
        :rtype: int
        """
        if self.ele is None:
            self.hasNext()

        v = self.ele
        self.ele = None
        return v

    def hasNext(self):
        """
        @return whether we have a next smallest number
        :rtype: bool
        """
        if self.ele:
            return True
        if len(self.path) == 0:
            return False
        curr = self.path.pop()
        self.ele = curr.val
        self.push(curr.right)

        return True

class BSTIterBg:
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.ele = None
        self.path = []
        self.push(root)

    def push(self, curr):
        if not curr:
            return

        while curr:
            self.path.append(curr)
            curr = curr.right

    def next(self):
        """
        @return the next smallest number
        :rtype: int
        """
        if self.ele is None:
            self.hasNext()

        v = self.ele
        self.ele = None
        return v

    def hasNext(self):
        """
        @return whether we have a next smallest number
        :rtype: bool
        """
        if self.ele:
            return True
        if len(self.path) == 0:
            return False
        curr = self.path.pop()
        self.ele = curr.val
        self.push(curr.left)

        return True

class Solution_with_iter_class(object):
    def findTarget(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: bool
        O(n) time, O(h) space
        """
        # BST iterator
        left = BSTIterSm(root)
        right = BSTIterBg(root)

        i = left.next()
        j = right.next()
        if i is None or j is None:
            return False

        while i < j:
            v = i + j

            if v == k:
                return True
            if v < k:
                i = left.next()
            elif v > k:
                j = right.next()

        return False


data = '[5,3,6,2,4,#,7]'
target = 11

data = '[1]'
target = 2
tree = BinaryTree()
root = tree.deserialize(data)

a = Solution().findTarget(root, target)
print("ans:", a)







