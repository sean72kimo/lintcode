
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(-5)
node3 = TreeNode(10)

node4 = TreeNode(3)
node5 = TreeNode(4)

node6 = TreeNode(5)
node7 = TreeNode(6)


root.left = node2
root.right = node3


node2.left = node4
node2.right = node5

node3.left = node6
node3.left = node7

import sys
class Solution2:
    # @param {TreeNode} root the root of binary tree
    # @return {int} the minimun weight node
    
    minumun_weight = sys.maxsize
    result = None

    def findMinimunWeight(self, root):
        # Write your code here
        self.helper(root)

        return self.result

    def helper(self, root):
        if root is None:
            return 0

        left_weight = self.helper(root.left)
        right_weight = self.helper(root.right)
        
        if left_weight + right_weight + root.val <= self.minumun_weight:
            self.minumun_weight = left_weight + right_weight + root.val
            self.result = root

        return left_weight + right_weight + root.val



import sys
class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the minimum subtree
    minWeight = sys.maxsize
    result = None
    
    # def __init__(self):
    #     self.result = None
    #     self.minWeight = sys.maxint
        
    def findSubtree(self, root):
        if root is None:
            return -1
        
        self.dfs(root)
        return self.result
    
    def dfs(self, node):
        if node is None:
            return 0
        
        leftWeight = self.dfs(node.left)
        rightWeight = self.dfs(node.right)
    
        if leftWeight + rightWeight + node.val <= self.minWeight:
            self.minWeight = leftWeight + rightWeight + node.val 
            self.reslt = node
        
        return leftWeight + rightWeight + node.val


sol = Solution()
ans = sol.findSubtree(root)
print(ans.val)
