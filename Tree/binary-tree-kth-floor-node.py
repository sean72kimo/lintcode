"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

class Solution:
    """
    @param root: the root node
    @param k: an integer
    @return: the number of nodes in the k-th layer of the binary tree
    """
    def kthfloorNode_dfs(self, root, k):
        if root is None:
            return 0
        
        if k == 1:
            return 1
        
        left = self.kthfloorNode_dfs(root.left, k-1)
        right = self.kthfloorNode_dfs(root.right, k-1)
        print(root.val, left, right)
        return left + right
    
    def kthfloorNode_bfs(self, root, k):
        # Write your code here
        if not root:
            return 0
        if k == 1:
            return 1

        que = [root]
        k = k - 1
        while que:
            newQ = []
            for node in que:
                if node.left:
                    newQ.append(node.left)
                if node.right:
                    newQ.append(node.right)
            k = k - 1
            if k == 0:
                return len(newQ)
            que = newQ

from lib.binarytree import BinaryTree
data = '{1,2,3,4,5,12,11,#,#,#,#,#,#,#,#}'
tree = BinaryTree()
root = tree.deserialize(data)
k = 3
a = Solution().kthfloorNode_dfs(root, k)
print("ans:",a)



