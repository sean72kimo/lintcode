from lib.binarytree import BinaryTree, TreeLinkNode


class Solution_dfs:

    # @param root, a tree link node
    # @return nothing
    def connect(self, root):
        if not root:
            return

        self.dfs(root)

    def dfs(self, root):
        if not root:
            return

        if not root.left and not root.right:
            return

        if root.left:
            root.left.next = root.right

        if root.next and root.right:
            root.right.next = root.next.left

        self.dfs(root.left)
        self.dfs(root.right)


class Solution_bfs(object):
    def connect(self, root):
        """
        :type root: Node
        :rtype: Node
        """
        if not root:
            return

        que = [root]

        while que:
            new_q = []
            for i in range(len(que)):
                if i + 1 <= len(que) - 1:
                    que[i].next = que[i + 1]

                if que[i].left:
                    new_q.append(que[i].left)
                if que[i].right:
                    new_q.append(que[i].right)
            que = new_q
        return root

# time O(n)
# space O(1)
class Solution_while_loop:
    def connect(self, root: 'Node') -> 'Node':
        if root is None:
            return
        tmp = root
        while root:
            dummy = TreeLinkNode(0, None, None, None)
            curr = dummy
            head = root
            while head:
                if head.left:
                    curr.next = head.left
                    curr = curr.next
                if head.right:
                    curr.next = head.right
                    curr = curr.next
                head = head.next
            root = dummy.next

        return tmp
data = '[1,2,3,4,5,6]'
tree = BinaryTree(TreeLinkNode)
root = tree.deserialize(data)
Solution_while_loop().connect(root)

