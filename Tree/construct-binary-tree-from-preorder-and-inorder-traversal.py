import collections
from typing import List, Optional

from lib.binarytree import TreeNode, BinaryTree

"""
O(n) by building a index map
"""
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        inorder_index_map = {}
        for index, value in enumerate(inorder):
            inorder_index_map[value] = index

        return self.dfs(collections.deque(preorder), inorder_index_map, 0, len(inorder) - 1)

    def dfs(self, preorder, inorder_index_map, start, end):
        if not preorder or start > end:
            return None
        v = preorder.popleft()

        mid = inorder_index_map[v]

        root = TreeNode(v)
        root.left = self.dfs(preorder, inorder_index_map, start, mid - 1)
        root.right = self.dfs(preorder, inorder_index_map, mid + 1, end)

        return root

"""
O(n^2) by indexing inorder in each node build
"""
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        return self.dfs(collections.deque(preorder), inorder)

    def dfs(self, preorder, inorder):
        if not preorder or not inorder:
            return None
        root_v = preorder.popleft()

        mid = inorder.index(root_v)

        root = TreeNode(root_v)
        root.left = self.dfs(preorder, inorder[:mid])
        root.right = self.dfs(preorder, inorder[mid + 1:])

        return root
preorder = [3,9,20,15,7]
inorder = [9,3,15,20,7]

# preorder = [1,2]
# inorder = [1,2]

preorder = [3,1,2,4]
inorder = [1,2,3,4]

sol = Solution()
a = sol.buildTree(preorder, inorder)
print(a, type(a))
tree = BinaryTree()


# data = '[3,1,4,null,2]'
# a = tree.deserialize(data)
tree.drawtree(a)
