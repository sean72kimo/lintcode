# time O((logn)**2)
# space O(1)
from lib.binarytree import BinaryTree
class Solution(object):

    def countNodes(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        left = self.getLH(root, 0)
        right = self.getRH(root, 0)
        if left == right:
            return (1 << left) - 1
        else:
            return self.countNodes(root.left) + self.countNodes(root.right) + 1 

    def getLH(self, root, h):
        if root is None:
            return h
        return self.getLH(root.left, h+1)
        
    def getRH(self, root, h):
        if root is None:
            return h
        return self.getRH(root.right, h+1)


class Solution2(object):
    def countNodes(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        h = self.get_h(root)

        self.cnt = 0
        self.count_last_row(root, 1, h)
        full = 2 ** (h - 1) - 1
        ans = full + self.cnt
        return ans

    def count_last_row(self, root, lv, h):
        if not root:
            return

        self.count_last_row(root.left, lv+1, h)
        self.count_last_row(root.right, lv+1, h)

        if not root.left and not root.right and lv == h:
            self.cnt += 1

    def get_h(self, root):
        if not root:
            return 0

        left = self.get_h(root.left)
        right = self.get_h(root.right)

        return max(left, right) + 1
data = '{1,2,3,4,5,6}'
# data = '{1,2,3,4}'
tree = BinaryTree()
root = tree.deserialize(data)

a = Solution().countNodes(root)
print("ans:", a)