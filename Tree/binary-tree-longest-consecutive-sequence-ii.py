from lib.binarytree import BinaryTree, TreeNode


class Solution(object):

    def __init__(self):
        self.longest = 0

    def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if root is None:
            return 0
        self.helper(root)
        return self.longest

    def helper(self, root):
        if root == None:
            return (0, 0)

        left_down, left_up = self.helper(root.left)
        right_down, right_up = self.helper(root.right)

        down, up = 0 , 0
        if root.left:
            if root.left.val + 1 == root.val:
                down = max(down, left_down + 1)

            if root.left.val - 1 == root.val:
                up = max(up, left_up + 1)

        if root.right:
            if root.right.val + 1 == root.val:
                down = max(down, right_down + 1)

            if root.right.val - 1 == root.val:
                up = max(up, right_up + 1)

        lenn = down + 1 + up
        self.longest = max(self.longest, lenn)

        return down, up


data = '[1, 2, 3]'
data = '[2,1,3]'
data = '[1,2,3,4]'
data = '[3,2,4,null,null,1]'
data = '[2,1,3,null,5,null,4]'
tree = BinaryTree()
root = tree.deserialize(data)

a = Solution().longestConsecutive(root)
print("ans:", a)
