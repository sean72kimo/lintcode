from lib.binarytree import BinaryTree, TreeNode

class Solution(object):
    def __init__(self):
        self.ans = []

    def printTree(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[str]]
        """
        self.dfs(root, 0, 0)

        return self.ans

    def dfs(self, root, indent, lv):
        if root is None:
            return indent

        left = self.dfs(root.left, indent, lv + 1)

        left_including_me = left + 1

        if len(self.ans) <= lv:
            how_many = lv + 1 - len(self.ans)
            for i in range(how_many):
                self.ans.append([])

        if len(self.ans[lv]) < left_including_me:
            space = left_including_me - len(self.ans[lv]) - 1
            self.ans[lv].extend([' '] * space)
            self.ans[lv].append(str(root.val))

        right = self.dfs(root.right, left_including_me, lv + 1)

        print(left_including_me, right)
        return max(left_including_me, right)


data = '[1,#,2,3,4,6,7]'
tree = BinaryTree()
root = tree.deserialize(data)
# tree.drawtree(root)
a = Solution().printTree(root)
print("ans:")
for r in a:
    for itm in r:
        print(itm, end = ',')
    print()
