from lib.binarytree import TreeNode, BinaryTree


class Solution(object):
    def flatten(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        1.目標:將root.right連接到root.left
        1.最後的linked list是由每一個root.right連結起來，然後將原本的root.left切斷
        2.暫時將root.right記錄在tmp
        3.如果有root.left(node), 先將root.right連接至此，並且尋找此node往右的最末點 (理當連成root.right.right.right...)
        4.最末點和tmp相連
        """
        if not root:
            return
        self.helper(root)

    def helper(self, root):
        if not root:
            return

        self.helper(root.left)

        l_node = root.left
        r_node = root.right

        end = None
        if root.left:
            end = root.left
            while end.right:
                end = end.right

        if l_node:
            root.right = l_node
        if end:
            end.right = r_node

        root.left = None

class Solution(object):
    def flatten(self, root):

        if not root:
            return

        self.helper(root)

    def helper(self, root):
        if not root:
            return

        tmp = root.right

        self.helper(root.left)

        if root.left:
            root.right = root.left
            node = root.left
            while node.right:
                node = node.right
            node.right = tmp

        root.left = None

        self.helper(tmp)
        return


tree = BinaryTree()
data = '[1,2,5,3,4,6,7]'
data = '[1,2,5,3,4,#,6]'
data = '[1,2,5,3,4,null,6]'

root = tree.deserialize(data)
tree.drawtree(root)
Solution().flatten(root)
print(tree.serialize(root))
