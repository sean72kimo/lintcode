from lib.binarytree import TreeNode, BinaryTree
class Solution:
    def sumRootToLeaf(self, root):
        if not root:
            return 0

        self.ans = 0
        self.dfs(root, 0)
        return self.ans

    def dfs(self, root, path):

        if not root.left and not root.right:
            path = (path << 1) + root.val
            self.ans += path
            return

        if not root:
            return

        path = (path << 1) + root.val
        if root.left:
            self.dfs(root.left, path)

        if root.right:
            self.dfs(root.right, path)


data = '[1,0,1,0,1,0,1]'
tree = BinaryTree()
root = tree.deserialize(data)
# tree.drawtree(root)
sol = Solution()
a = sol.sumRootToLeaf(root)
print("ans:", a)