import collections

from lib.binarytree import TreeNode, BinaryTree


class Solution(object):
    def distanceK(self, root, target, K):
        """
        :type root: TreeNode
        :type target: TreeNode
        :type K: int
        :rtype: List[int]
        1 用dfs將每個node標記 parent (prev), 然後用bfs方式找到第K步，返回所有點。
        找的時候要注意vst不要重複找(因為不是單純的樹往下 curr.left , curr.right 而是變成圖了)

        2 無相圖找距離target k步的點。bfs圖的遍歷。我們可以先用O(n)時間建立graph。再從target開始bfs找到第K層，返回答案
        """
        if not root:
            return []

        self.dfs(root, None)
        que = collections.deque([target])
        vst = {target}
        step = 0
        while que:
            new_q = []
            if step == K:
                return [node.val for node in que]

            for curr in que:

                for nei in [curr.left, curr.right, curr.prev]:
                    if nei and nei not in vst:
                        vst.add(nei)
                        new_q.append(nei)
            step += 1
            que = new_q
        return []

    def dfs(self, root, prev):
        if not root:
            return

        root.prev = prev
        self.dfs(root.left, root)
        self.dfs(root.right, root)


class Solution2(object):
    def distanceK(self, root, target, K):
        """
        :type root: TreeNode
        :type target: TreeNode
        :type K: int
        :rtype: List[int]
        2 bfs圖的遍歷。若題目不允許更改原輸入，我們可以先用O(n)時間建立graph。再從target開始bfs找到第K層，返回答案
        """
        if not root:
            return []
        graph = self.build_graph(root)
        que = [target]
        vst = {target}
        step = 0
        ans = []
        while que:
            new_q = []
            if step == K:
                ans = [node.val for node in que]
                return ans

            for curr in que:
                for nei in graph[curr]:
                    if nei in vst:
                        continue
                    new_q.append(nei)
                    vst.add(nei)

            que = new_q
            step += 1
        return ans

    def build_graph(self, root):
        graph = collections.defaultdict(set)

        que = [root]
        while que:
            new_q = []
            for curr in que:
                left = curr.left
                right = curr.right
                if left:
                    graph[curr].add(left)
                    graph[left].add(curr)
                    new_q.append(left)
                if right:
                    graph[curr].add(right)
                    graph[right].add(curr)
                    new_q.append(right)

            que = new_q
        return graph


data = '[3,5,1,6,2,0,8,null,null,7,4]'
K = 2
tree = BinaryTree()
root = tree.deserialize(data)
target_val = 5
def get_target(root):
    if not root: return
    if root.val == target_val: return root
    left = get_target(root.left)
    right = get_target(root.right)
    if left: return left
    if right: return right
    return None
target = get_target(root)
print(target)
sol = Solution2()
a = sol.distanceK(root, target, K)
print("ans:", a)

# left = [1,2,None,3]
# res = [n for n in left if n]
#
# print(list(res))
