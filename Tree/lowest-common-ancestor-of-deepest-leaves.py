# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
"""
similar to 865
1. 深度計算從葉子往root計算
1. 如果左右子樹最深深度一樣，那麼當前root為lca
1. 如果左樹較深，那麼最深的葉子(們)必在左樹 -> lca也在左樹
1. 右樹道理同左樹

"""


class Solution(object):
    def lcaDeepestLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return

        return self.find_lca(root, 0)[0]

    def find_lca(self, root, d):
        if not root:
            return None, 0

        left, left_d = self.find_lca(root.left, d)
        right, right_d = self.find_lca(root.right, d)

        if left_d == right_d:
            return root, left_d + 1
        if left_d > right_d:
            return left, left_d + 1
        if right_d > left_d:
            return right, right_d + 1