from typing import List, Optional

from lib.binarytree import BinaryTree, TreeNode

"""
O(n)
"""
class Solution:
    def buildTree(self, inorder: List[int], postorder: List[int]) -> Optional[TreeNode]:
        idx_mp = {}

        for i, v in enumerate(inorder):
            idx_mp[v] = i

        return self.dfs(postorder, idx_mp, 0, len(inorder) - 1)

    def dfs(self, postorder, idx_mp, start, end):
        if start > end or len(postorder) == 0:
            return None

        v = postorder.pop()
        idx = idx_mp[v]

        root = TreeNode(v)
        root.right = self.dfs(postorder, idx_mp, idx + 1, end)
        root.left = self.dfs(postorder, idx_mp, start, idx - 1)

        return root

class Solution2(object):

    def buildTree(self, inorder, postorder):
        """
        :type inorder: List[int]
        :type postorder: List[int]
        :rtype: TreeNode
        """
        if len(inorder) == 0 or len(postorder) == 0:
            return
        return self.dfs(inorder, postorder, 0, len(inorder) - 1, 0, len(postorder) - 1)

    def dfs(self, inorder, postorder, instart, inend, poststart, postend):
        if instart > inend:
            return

        v = postorder[postend]
        root = TreeNode(v)
        i = inorder.index(v)

        root.left = self.dfs(inorder, postorder, instart, i - 1, poststart, poststart + i - instart - 1)

        root.right = self.dfs(inorder, postorder, i + 1, inend, poststart + i - instart, postend - 1)

        return root


inorder = [9, 3, 15, 20, 7]
postorder = [9, 15, 7, 20, 3]
a = Solution().buildTree(inorder, postorder)
tree = BinaryTree()
tree.drawtree(a)
