from lib.binarytree import TreeNode, BinaryTree


class Solution:
    def goodNodes(self, root: TreeNode) -> int:
        if not root:
            return 0
        self.ans = 0
        self.dfs(root, float('-inf'))
        return self.ans

    def dfs(self, root, max_so_far):
        if not root:
            return

        if root.val >= max_so_far:
            self.ans += 1

        self.dfs(root.left, max(max_so_far, root.val))
        self.dfs(root.right, max(max_so_far, root.val))


arr = "[3,1,4,3,null,1,5,null,null,1,2,3,4 ]"
tree = BinaryTree()
root = tree.deserialize(arr)
tree.drawtree(root)