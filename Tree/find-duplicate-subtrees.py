import collections

"""
time O(n^2), O(n) time to visit each node, but at each node to form "serail" is o(n) time
space O(n^2), each node holds O(n) of tree representation, total n nodes
"""
class Solution(object):
    def __init__(self):
        self.ans = []
    
    def findDuplicateSubtrees(self, root):
        count = collections.Counter()
        self.preorder(root, count)

        return self.ans
        
    def preorder(self, root, count):
        if not root:
            return "#"

        left = self.preorder(root.left, count)
        right = self.preorder(root.right, count)
        serial = "{},{},{}".format(str(root.val), left, right)
        
        count[serial] += 1
        
        if count[serial] == 2:
            self.ans.append(root)
            
        return serial
        

from lib.binarytree import BinaryTree
tree = BinaryTree()
data = '[1,2,3,4,#,2,4,#,#,4]'
# data = '[1,2,3,#,#,4,5]'
root = tree.deserialize(data)
# tree.drawtree(root)
a = Solution().findDuplicateSubtrees(root)
print(a)
