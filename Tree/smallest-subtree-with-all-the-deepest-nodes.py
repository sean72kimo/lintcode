from lib.binarytree import BinaryTree, TreeNode


class Solution:

    def __init__(self):
        self.ans = None

    def subtreeWithAllDeepest(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return

        mp = {}
        self.dfs(root, 1, mp)
        max_d = max(mp.values())
        nodes = set()
        for node, d in mp.items():
            if d == max_d:
                nodes.add(node)
        print(mp, max_d, nodes)
        self.ans = self.lca(root, nodes)
        return self.ans

    def lca(self, root, nodes):
        if not root:
            return
        if root in nodes:
            return root

        left = self.lca(root.left, nodes)
        right = self.lca(root.right, nodes)
        if left and right:
            return root
        if left:
            return left
        if right:
            return right
        return None

    def dfs(self, root, d, mp):
        if not root:
            return

        mp[root] = d

        self.dfs(root.left, d + 1, mp)
        self.dfs(root.right, d + 1, mp)


data = '[3,5,1,6,2,0,8,null,null,7,4]'
data = '[0,1,3,null,2]'
data = '[1]'
tree = BinaryTree()
root = tree.deserialize(data)

a = Solution().subtreeWithAllDeepest(root)
print("ans:", a)

