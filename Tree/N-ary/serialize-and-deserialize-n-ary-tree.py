
# Definition for a Node.
class Node(object):
    def __init__(self, val, children=[]):
        self.val = val
        self.children = children
    def __repr__(self):
        return "<N{}>".format(str(self.val))

class Codec:

    def serialize(self, root):
        if not root:
            return ""

        path = []
        self.build_str(root, path)
        data = ','.join(path)
        return data

    def build_str(self, root, path):
        if not root:
            return
        path.append(str(root.val))
        for child in root.children:
            self.build_str(child, path)
        path.append('#')

    def deserialize(self, data):
        if len(data) == 0:
            return
        path = data.strip().split(',')
        self.i = 0
        val = int(path[self.i])
        self.i += 1
        root = Node(val, [])
        self.build_tree(root, path)
        return root

    def build_tree(self, root, path):
        if self.i >= len(path):
            return

        if path[self.i] == '#':
            self.i += 1
            return

        while path[self.i] != '#':
            val = int(path[self.i])
            self.i += 1
            node = Node(val, [])
            self.build_tree(node, path)
            root.children.append(node)
        self.i += 1
        return

n1 = Node(1)
n2 = Node(2)
n3 = Node(3)
n4 = Node(4)
n5 = Node(5)
n6 = Node(6)

n3.children = [n5, n6]
n1.children = [n3, n2, n4]


codec = Codec()
path = codec.serialize(n1)
print(path)
node = codec.deserialize(path)
print(node, node.children)
