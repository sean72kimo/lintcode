"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
import collections


class Solution(object):
    def __init__(self):
        self.ans = 0

    def maxDepth(self, root):
        """
        :type root: Node
        :rtype: int
        """
        if not root:
            return self.ans

        self.dfs(root, 1)
        return self.ans

    def dfs(self, root, d):
        if not root:
            return

        self.ans = max(d, self.ans)
        for child in root.children:
            self.dfs(child, d + 1)

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: Node
        :rtype: int
        """
        if not root:
            return 0
        que = [(root, 1)]
        deque = collections.deque(que)
        ans = float('-inf')
        while deque:
            node, d = deque.popleft()
            ans = max(ans, d)
            for child in node.children:
                itm = (child, d+1)
                deque.append(itm)
        return ans
    