"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""


class Solution(object):
    def levelOrder(self, root):
        """
        :type root: Node
        :rtype: List[List[int]]
        """
        if not root:
            return []

        que = [root]
        ans = []
        while que:
            new_q = []
            tmp = []
            for curr in que:
                tmp.append(curr.val)
                for child in curr.children:
                    new_q.append(child)

            que = new_q
            ans.append(tmp)
        return ans