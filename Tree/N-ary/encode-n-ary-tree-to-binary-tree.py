"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
"""
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
"""
"""
children第一個node, children[0]放到treeNode.left
children[1:]剩下的node,  接到treeNode.right
"""

class Codec:

    def encode(self, root):
        """Encodes an n-ary tree to a binary tree.

        :type root: Node
        :rtype: TreeNode
        """
        if not root:
            return
        if len(root.children) == 0:
            return TreeNode(root.val)

        binary = TreeNode(root.val)

        if len(root.children):
            node = root.children[0]
            binary.left = self.encode(node)

            curr = binary.left
            for n in root.children[1:]:
                curr.right = self.encode(n)
                curr = curr.right

        return binary

    def decode(self, root):
        """Decodes your binary tree to an n-ary tree.

        :type data: TreeNode
        :rtype: Node
        """
        if not root:
            return None
        if not root.left and not root.right:
            return Node(root.val, [])

        res = Node(root.val, [])
        cur = root.left
        while cur:
            res.children.append(self.decode(cur))
            cur = cur.right
        return res

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(root))