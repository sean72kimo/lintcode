import collections

from lib.binarytree import TreeNode, BinaryTree

class Solution_dfs_prefixSum_cache(object):
    """time O(n), space O(n)"""

    def __init__(self):
        self.ans = 0

    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: int
        """
        cache = collections.defaultdict(int)
        cache[0] = 1

        self.dfs(root, sum, 0, cache)
        return self.ans

    def dfs(self, root, target, currPathSum, cache):
        if not root:
            return

        currPathSum += root.val
        prefixSum = currPathSum - target
        self.ans += cache[prefixSum]

        cache[currPathSum] += 1
        self.dfs(root.left, target, currPathSum, cache)
        self.dfs(root.right, target, currPathSum, cache)
        cache[currPathSum] -= 1


data = "[7,5,-3,3,2,null,11,3,-2,null,1]"
s = 8
exp = 3

data = '[7,5]'
s = 5
exp = 1

data = '[7,5,null,-3,null,3,null,2,null,-2,null,1]'
s = 5
exp = 4

data = '[10,5,-3,3,2,null,11,3,-2,null,1]'
s = 8
exp = 3
tree = BinaryTree()
root = tree.deserialize(data)
# tree.drawtree(root)
sol = Solution_dfs_prefixSum_cache()
a = sol.pathSum(root, s)
print("ans:", a==exp, a)


class Solution_dfs_top_dwon(object):
    """ time O(n^2), space O(h)"""
    def __init__(self):
        self.ans = 0

    def pathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: int
        """
        if not root:
            return 0

        self.dfs(root, sum)
        self.pathSum(root.left, sum)
        self.pathSum(root.right, sum)
        return self.ans

    def dfs(self, root, summ):
        if not root:
            return

        t = summ - root.val
        if t == 0:
            self.ans += 1

        self.dfs(root.left, t)
        self.dfs(root.right, t)


class Solution_dfs_bottom_up(object):
    """ time O(n^2), space O(h)"""

    class Solution(object):
        def pathSum(self, root, sum):
            """
            :type root: TreeNode
            :type sum: int
            :rtype: int
            """
            if not root:
                return 0
            a = self.dfs(root, sum)
            b = self.pathSum(root.left, sum)
            c = self.pathSum(root.right, sum)
            return a + b + c

        def dfs(self, root, sum):
            if not root:
                return 0

            t = sum - root.val
            l = self.dfs(root.left, t)
            r = self.dfs(root.right, t)

            if t == 0:
                return 1 + l + r
            else:
                return l + r


