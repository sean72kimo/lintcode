from lib.binarytree import TreeNode
"""
此題的重點在postorder裡面找 "左樹的根"，並以此點切分
注意！在循環時候，postorder要後面退一格，preorder前面退一格
"""
class Solution(object):
    def constructFromPrePost(self, pre, post):
        """
        :type pre: List[int]
        :type post: List[int]
        :rtype: TreeNode
        """
        if not pre:
            return

        if len(pre) == 1:
            return TreeNode(pre[0])

        n = len(pre)
        return self.dfs(pre, post, 0, 0, n)

    def dfs(self, pre, post, i0, i1, n):
        if n == 0:
            return None
        if n == 1:
            return TreeNode(pre[i0])

        root = TreeNode(pre[i0])
        for L in range(n):
            if post[i1 + L - 1] == pre[i0 + 1]:
                break
        root.left = self.dfs(pre, post, i0 + 1, i1, L)
        root.right = self.dfs(pre, post, i0 + L + 1, i1 + L, n - 1 - L)

        return root


class Solution(object):
    def constructFromPrePost(self, pre, post):
        """
        :type pre: List[int]
        :type post: List[int]
        :rtype: TreeNode
        """
        if not pre:
            return

        if len(pre) == 1:
            return TreeNode(pre[0])

        root = TreeNode(pre[0])
        i = post.index(pre[1]) + 1

        root.left = self.constructFromPrePost(pre[1:i + 1], post[:i])
        root.right = self.constructFromPrePost(pre[i + 1:], post[i:-1])

        return root