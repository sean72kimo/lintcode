# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
from typing import Optional, List

from lib.binarytree import TreeNode


class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        if not root:
            return []

        que = [root]
        ans = []

        while que:
            new_q = []
            level = []

            for node in que:
                level.append(node.val)

                if node.left:
                    new_q.append(node.left)
                if node.right:
                    new_q.append(node.right)

            que = new_q
            ans.append(level)

        return ans[::-1]

class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        if not root:
            return []

        self.ans = []

        self.dfs(root, 0)
        return self.ans[::-1]

    def dfs(self, node, lv):
        if not node:
            return

        if len(self.ans) == lv:
            self.ans.append([])

        self.ans[lv].append(node.val)
        self.dfs(node.left, lv + 1)
        self.dfs(node.right, lv + 1)

