"""
此題關鍵為“葉子節點”
在not node.left and not node.right (葉子)的時候進行判斷
而非null

bfs解法亦可(vs 102 Binary Tree Level Order Traversal)

dfs解法(vs 104 Maximum Depth of Binary Tree)
"""


class Solution_BFS(object):

    def minDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        que = [root]
        lv = 0
        ans = float('inf')
        while que:
            new_q = []
            lv += 1
            for node in que:

                if not node.left and not node.right:
                    ans = min(ans, lv)

                if node.left:
                    new_q.append(node.left)

                if node.right:
                    new_q.append(node.right)
            que = new_q

        return ans


class Solution_DFS_TopDown(object):

    def __init__(self):
        self.ans = float('inf')

    def minDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        self.dfs(root, 1)
        return self.ans

    def dfs(self, root, lv):
        if not root.left and not root.right:
            self.ans = min(self.ans, lv)
            return

        if root.left:
            self.dfs(root.left, lv + 1)
        if root.right:
            self.dfs(root.right, lv + 1)


class Solution_DFS_BottomUp(object):

    def __init__(self):
        self.ans = float('inf')

    def minDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        self.ans = self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root.left and not root.right:
            return 1

        left = right = None
        if root.left:
            left = self.dfs(root.left)

        if root.right:
            right = self.dfs(root.right)

        if not left:
            return right + 1

        if not right:
            return left + 1

        return min(left, right) + 1
