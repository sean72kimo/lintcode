"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """

    def maxDepth(self, root):
        # write your code here
        max_depth = self.helper2(root)
        return max_depth

    def helper(self, root):
        if root is None:
            return 0
        ldepth = self.helper(root.left)
        rdepth = self.helper(root.right)

        return max(ldepth, rdepth) + 1

    def helper2(self, root):
        if root is None:
            return 0

        if root.left is None and root.right is None:
            return 1

        ldepth = self.helper(root.left)
        rdepth = self.helper(root.right)

        return max(ldepth, rdepth) + 1

from lib.binarytree import BinaryTree
data = '{1,#,2,3}'
root = BinaryTree().deserialize(data)
BinaryTree().drawtree(root)
print('ans:', Solution().maxDepth(root))
