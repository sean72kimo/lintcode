# Definition for a binary tree node.

from typing import List

from lib.binarytree import TreeNode, BinaryTree
class Solution:
    def delNodes(self, root: TreeNode, to_delete: List[int]) -> List[TreeNode]:
        delete = set(to_delete)
        self.ans = []
        self.dfs(root, delete)
        if root.val not in delete:
            self.ans.append(root)
        return self.ans

    def dfs(self, root, delete):
        if not root:
            return False

        ld = self.dfs(root.left, delete)
        rd = self.dfs(root.right, delete)

        if root.val in delete:
            if root.left and root.left.val not in delete:
                self.ans.append(root.left)
            if root.right and root.right.val not in delete:
                self.ans.append(root.right)

        if ld:
            root.left = None
        if rd:
            root.right = None

        return root.val in delete

data = '[1,2,3,4,5,6,7]'
to_delete = [3,5]
tree = BinaryTree()
root = tree.deserialize(data)
sol = Solution()
ans = sol.delNodes(root, to_delete)
print(ans)


