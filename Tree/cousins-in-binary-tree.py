import collections


class Solution(object):
    def isCousins(self, root, x, y):
        """
        :type root: TreeNode
        :type x: int
        :type y: int
        :rtype: bool
        
        hashmap記錄每個點的深度和父親
        """
        parent = {}
        depth = collections.defaultdict(int)
        self.dfs(root, None, 0, parent, depth)

        return depth[x] == depth[y] and parent[x] != parent[y]

    def dfs(self, root, pre, d, parent, depth):
        if not root:
            return

        depth[root.val] = d
        parent[root.val] = pre.val if pre else 0
        self.dfs(root.left, root, d + 1, parent, depth)
        self.dfs(root.right, root, d + 1, parent, depth)