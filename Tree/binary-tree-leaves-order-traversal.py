"""
Definition of TreeNode:
        self.left, self.right = None, None
"""
class TreeNode:
    def __init__(self, val):
        self.val = val



class Solution:
    """
    @param: root: the root of binary tree
    @return: collect and remove all leaves
    """
    nodes = {}

    def findLeaves(self, root):
        if root is None:
            return []

        max_d = self.dfs(root)
        ans = []
        for k, v in self.nodes.items():
            ans.append(v)
        return ans


    def dfs(self, curr):
        if curr is None:
            return 0

        l_depth = self.dfs(curr.left)
        r_depth = self.dfs(curr.right)
        depth = max(l_depth , r_depth) + 1

        if depth in self.nodes:
            self.nodes[depth].append(curr.val)
        else:
            self.nodes[depth] = [curr.val]
        return depth


from lib.binarytree import BinaryTree, TreeNode

data = '{1,2,3,4,5}'
tree = BinaryTree()
root = tree.deserialize(data)

print(Solution().findLeaves(root))



