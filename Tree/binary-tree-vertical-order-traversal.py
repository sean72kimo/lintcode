"""
此題不能用dfs，下面這個輸入會出錯
root = [3,9,8,4,0,1,7,null,null,null,2,5]
使用dfs的時候，會一路往左走到最底，優先處理根節點的所有左子樹
所以就算pos的位置正確，但是順序卻會出錯，實際把上面那棵樹畫出來走一遍即可知

"""

class Solution:
    """
    @param: root: the root of tree
    @return: the vertical order traversal
    """
    def verticalOrder(self, root):
        # write your code here
        result = {}
        ans = []
        queue = [(root, 0)]  # (node, col)

        while queue:
            node, col = queue.pop(0)
            if node:
                if col in result:
                    result[col].append(node.val)
                else:
                    result[col] = [node.val]
                queue.append((node.left, col - 1))
                queue.append((node.right, col + 1))

        for key in sorted(result):
            ans.append(result[key])
        return ans

    def verticalOrder2(self, root):
        # write your code here
        if root is None:
            return []
        result = {}
        ans = []
        queue = [(root, 0)]  # (node, col)

        while queue:
            new_q = []

            for item in queue:
                node = item[0]
                col = item[1]

                if col in result:
                    result[col].append(node.val)
                else:
                    result[col] = [node.val]

                if node.left:
                    new_q.append((node.left, col - 1))
                if node.right:
                    new_q.append((node.right, col + 1))

            queue = new_q

        for key in sorted(result):
            ans.append(result[key])

        return ans

from lib.binarytree import BinaryTree, TreeNode

data = '{3,9,20,#,#,15,7}'
tree = BinaryTree()
root = tree.deserialize(data)

print(Solution().verticalOrder2(root))


# somedict = {}
# print(somedict[3])  # KeyError
#
# someddict = collections.defaultdict(list)
# print(someddict[3])  #
