from lib.binarytree import BinaryTree

"""
similar to https://leetcode.com/problems/binary-search-tree-iterator/submissions/
binary tree inorder traversal, stack 
"""
class Solution:
    def inorderTraversal(self, root):
        stack = []
        while root:
            stack.append(root)
            root = root.left

        ans = []
        while stack:
            node = stack.pop()
            ans.append(node.val)

            curr = node.right

            while curr:
                stack.append(curr)
                curr = curr.left

        return ans

class Solution:
    """
    @param root: The root of binary tree.
    @return: Inorder in ArrayList which contains node values.
    """
    result = []
    def inorderTraversal_stack(self, root):
        inorder = []
        if root is None:
            return inorder
        
        current = root
        stack = []
        while current or stack:
            print (stack)
            if current:
                stack.append(current)
                current = current.left
            else:
                current = stack.pop()
                inorder.append(current.val)
                current = current.right

        return inorder
    def inorderTraversal_dfs(self, root):
        # write your code here
        if root is None:
            return self.result
            
        self.helper(root)
        return self.result
        
    def helper(self, root):
        if root is None:
            return
        self.helper(root.left)
        self.result.append(root.val)
        self.helper(root.right)

data = '{1,#,2,3}'
data = '[41,37,44,24,39,42,48,1,35,38,40,#,43,46,49,0,2,30,36,#,#,#,#,#,#,45,47,#,#,#,#,#,4,29,32,#,#,#,#,#,#,3,9,26,#,31,34,#,#,7,11,25,27,#,#,33,#,6,8,10,16,#,#,#,28,#,#,5,#,#,#,#,#,15,19,#,#,#,#,12,#,18,20,#,13,17,#,#,22,#,14,#,#,21,23]'
# data = '[15,10,20,8,12,16,25]'
tree = BinaryTree()
root = tree.deserialize(data)
a = Solution().inorderTraversal_stack(root)
print("ans:", a)
