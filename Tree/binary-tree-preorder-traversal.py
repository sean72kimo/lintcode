"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


"""
stack
"""
class SolutionStack:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
    def preorderTraversal(self, root):
        if root is None:
            return []
        print ('root', root.val, root.left.left, root.left.right)
        stack = [root]
        preorder = []
         
        while stack:
            node = stack.pop()
            preorder.append(node.val)
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
        return preorder
    

"""
traverse
"""
class SolutionTraverse:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
    
    
    def preorderTraversal(self, root):
        self.result = []
        
        if root is None:
            self.result
        
        self.traverse(root)
        return self.result
        
        
    def traverse(self, node):
        if node is None:
            return
        self.result.append(node.val)
        self.traverse(node.left)
        self.traverse(node.right)
        

            
"""
divide and conquer
"""
class SolutionDivideConquer(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        res = []
        if root is None:
            return res
        
        left_res = self.preorderTraversal(root.left)
        right_res = self.preorderTraversal(root.right)
        
        res.append(root.val)

        res += left_res
        res += right_res

        return res 
    
    
class SolutionStr(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        res = []
        if root is None:
            return '#'
        
        left = self.preorderTraversal(root.left)
        right = self.preorderTraversal(root.right)
        
        ser = str(root.val) + ',' + left + ',' + right
        return ser
from lib.binarytree import BinaryTree, TreeNode
data = '[1,2,3,4,5,6]'
tree = BinaryTree()
root = tree.deserialize(data)
a = SolutionTraverse().preorderTraversal(root)
print("ans", a)