
import collections
from lib.binarytree import BinaryTree, TreeNode

class Solution(object):
    def verticalTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return []

        que = [(root, 0, 0)]
        h = collections.defaultdict(list)
        v = collections.defaultdict(int)
        while que:
            new_q = []
            for curr, x, y in que:
                h[x].append((curr, x, y))
                v[curr] = y

                if curr.left:
                    new_q.append((curr.left, x - 1, y - 1))
                if curr.right:
                    new_q.append((curr.right, x + 1, y - 1))

            que = new_q

        ans = []
        mn = min(list(h.keys()))
        mx = max(list(h.keys()))

        def mykey(t):
            return t[2], t[1], t[0].val

        for i in range(mn, mx + 1):
            lst = h[i]
            tmp = sorted(lst, key=mykey)
            ans.append([v[0].val for v in tmp])

        return ans

data = '[1,2,3,4,5,6,7]'
exp = [[4],[2],[1,5,6],[3],[7]]

tree = BinaryTree()
root = tree.deserialize(data)
sol = Solution()
a = sol.verticalTraversal(root)
print("ans:", a==exp, a)
