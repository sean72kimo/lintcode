
import collections
from lib.binarytree import BinaryTree, TreeNode

class Solution_bfs(object):
    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return
        parent = collections.defaultdict(set)
        out = collections.defaultdict(int)
        allN = set()
        que = [root]

        while que:
            new_q = []
            for curr in que:
                allN.add(curr)
                if curr.left:
                    out[curr] += 1
                    parent[curr.left].add(curr)
                    new_q.append(curr.left)

                if curr.right:
                    out[curr] += 1
                    parent[curr.right].add(curr)
                    new_q.append(curr.right)

            que = new_q

        print('out', out)
        print('parent:', parent)
        que = []
        ans = []
        path = []
        for node in allN:
            if node not in out:
                path.append(node.val)
                que.append(node)
        ans.append(path)

        while que:
            new_q = []
            path = []
            for curr in que:
                for p in parent[curr]:
                    out[p] -= 1
                    if out[p] == 0:
                        new_q.append(p)
                        path.append(p.val)

            que = new_q
            if len(path):
                ans.append(path)
        return ans


class Solution(object):
    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return []
        self.mp = collections.defaultdict(list)
        d = self.dfs(root)
        ans = []
        for i in range(d):
            if i in self.mp:
                ans.append(self.mp[i])

        return ans

    def dfs(self, root):
        if not root:
            return 0

        l = self.dfs(root.left)
        r = self.dfs(root.right)

        d = max(l, r)
        self.mp[d].append(root.val)


class Solution(object):
    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if not root:
            return
        parent = {}
        out = collections.defaultdict(int)
        allN = set()
        que = [root]

        while que:
            new_q = []
            for curr in que:
                allN.add(curr)
                if curr.left:
                    out[curr] += 1
                    parent[curr.left] = curr
                    new_q.append(curr.left)

                if curr.right:
                    out[curr] += 1
                    parent[curr.right] = curr
                    new_q.append(curr.right)

            que = new_q
        parent[root] = TreeNode(0)
        print(parent)

        que = []
        ans = []
        path = []
        for node in allN:
            if node not in out:
                path.append(node.val)
                que.append(node)
        ans.append(path)

        while que:
            new_q = []
            path = []
            for curr in que:
                p = parent[curr]
                out[p] -= 1
                if out[p] == 0:
                    new_q.append(p)
                    path.append(p.val)

            que = new_q
            if len(path):
                ans.append(path)
        return ans


data = '[1,2,3,4,5]'
exp = [[4,5,3],[2],[1]]
tree = BinaryTree()
root = tree.deserialize(data)
sol = Solution()
a = sol.findLeaves(root)
print("ans:", a==exp, a)