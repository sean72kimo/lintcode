from sys import maxsize
from lib.binarytree import BinaryTree
class Solution:
    """
    @param: root: the root of binary tree
    @return: the maximum weight node
    """
    maximum_weight = -maxsize
    result = None

    def findSubtree(self, root):
        if root is None:
            return

        self.helper(root)
        return self.result

    def helper(self, root):
        if root is None:
            return 0

        lval = self.helper(root.left)
        rval = self.helper(root.right)
        sum = lval + rval + root.val

        if self.result is None:
            self.result = root

        if sum >= self.maximum_weight:
            self.maximum_weight = sum
            self.result = root

        return sum

data = '{1,-5,2,0,3,-4,-5}'
root = BinaryTree().deserialize(data)
a = Solution().findSubtree(root)
print(a.val)
