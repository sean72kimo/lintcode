import collections

from lib.binarytree import BinaryTree, TreeNode


class Codec:
    """
    目前最喜歡的做法，
    如果preorder, 則split data into deque and deque.popleft() deserialize
    如果postorder,則split data into list and list.pop() deserialize
    
    """

    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        if not root:
            return ""
        path = []
        self.preorder(root, path)
        return ','.join(path)

    def preorder(self, root, path):
        if not root:
            path.append('#')

        else:
            path.append(str(root.val))
            self.preorder(root.left, path)
            self.preorder(root.right, path)

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if len(data) == 0:
            return None

        nums = data.split(',')
        path = collections.deque(nums)
        return self.helper(path)

    def helper(self, path):
        if path[0] == '#':
            path.popleft()
            return None

        curr = TreeNode(path.popleft())
        curr.left = self.helper(path)
        curr.right = self.helper(path)
        return curr


class Codec_pointer:

    def __init__(self):
        self.spliter = ','
        self.null = '#'
        self.string = ""

        self.idx = 0

    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        path = []
        self.buildstr(root, path)

        return ','.join(path)

    def buildstr(self, root, path):
        if not root:
            path.append('#')

        else:
            path.append(str(root.val))

            self.buildstr(root.left, path)
            self.buildstr(root.right, path)

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """

        normalized = data.split(',')

        if normalized[self.idx] == '#':
            self.idx += 1
            return None

        val = normalized[self.idx]
        self.idx += 1

        root = TreeNode(val)
        left = self.deserialize(data)
        right = self.deserialize(data)

        root.left = left
        root.right = right
        return root


class Codec_level_order_traversal:

    def __init__(self):
        self.spliter = ','
        self.none = '#'
        self.string = ""

    def serialize(self, root):
        """Encodes a tree to a single string.

        :type root: TreeNode
        :rtype: str
        """
        res = [str(root.val)]
        que = [root]

        while que:
            new_q = []

            for root in que:
                if root.left:
                    new_q.append(root.left)
                    res.append(str(root.left.val))
                else:
                    res.append(self.none)

                if root.right:
                    new_q.append(root.right)
                    res.append(str(root.right.val))
                else:
                    res.append(self.none)

            que = new_q

        while res[-1] == '#':
            res.pop()

        return '{' + ','.join(res) + '}'

    def deserialize(self, data):
        """Decodes your encoded data to tree.

        :type data: str
        :rtype: TreeNode
        """
        if data == '{}' or data is None:
            return None

        vals = data[1:-1].split(',')

        root = TreeNode(int(vals[0]))
        queue = [root]
        isLeft = True
        idx = 0

        for val in vals[1:]:
            if val != '#':
                node = TreeNode(int(val))
                if isLeft:
                    queue[idx].left = node
                else:
                    queue[idx].right = node
                queue.append(node)

            if not isLeft:
                idx += 1

            isLeft = not isLeft
        return root


def main():
    tree = BinaryTree()
    root = tree.deserialize(tree.bst)

    data = Codec().serialize(root)
    print("========= ans =========")
    print(data)
    node = Codec().deserialize(data)
    # data2 = Codec().serialize(root)
    # print(data2)
#     data = Codec2().serialize(root)
#     print(data)
#     node = Codec2().deserialize(data)
#     data = Codec2().serialize(node)
#     print(data)


if __name__ == '__main__':
    main()

