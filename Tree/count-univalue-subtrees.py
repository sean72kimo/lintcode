# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from lib.binarytree import TreeNode, BinaryTree
class Solution(object):
    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        self.ans = 0
        self.dfs(root)
        return self.ans

    def dfs(self, root):
        if not root:
            return True

        if not root.left and not root.right:
            self.ans += 1
            return True

        left = self.dfs(root.left)
        right = self.dfs(root.right)
        print(root, root.left, root.right)

        if root.left and root.right and root.val == root.left.val == root.right.val and left and right:
            self.ans += 1
            return True

        if root.left and not root.right and root.val == root.left.val and left:

            self.ans += 1
            return True

        if root.right and not root.left and root.val == root.right.val and right:
            self.ans += 1
            return True

        return False


class Solution(object):
    def __init__(self):
        self.ans = 0

    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0

        self.helper(root)
        return self.ans

    def helper(self, root):
        if not root:
            return True

        left = self.helper(root.left)
        right = self.helper(root.right)

        if left and right:
            if root.left and root.val != root.left.val:
                return False

            if root.right and root.val != root.right.val:
                return False

            self.ans += 1
            return True
        return False

tree = BinaryTree()
data = '[5,1,5,5,5,null,5]'
root = tree.deserialize(data)
sol = Solution()
a = sol.countUnivalSubtrees(root)
print("ans:", a)