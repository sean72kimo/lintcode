"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the maximum average of subtree
    node = None
    average = 0
    def findSubtree2(self, root):
        # Write your code here
        self.helper(root)
        return self.node

    def helper(self, root):
        if root is None:
            return 0, 0

        lsum, lsize = self.helper(root.left)
        rsum, rsize = self.helper(root.right)

        sum = root.val + lsum + rsum
        size = lsize + rsize + 1
        avg = sum * 1.0 / size

        if self.node is None or avg > self.average:
            self.node = root
            self.average = avg

        return sum, size




