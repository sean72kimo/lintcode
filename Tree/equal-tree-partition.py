from lib.binarytree import BinaryTree, TreeNode
import collections


class Solution(object):

    def checkEqualTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        mp = collections.defaultdict(int)
        summ = self.calc(root, mp)
        print(mp)
        if summ % 2:
            return False

        if summ == 0:
            return summ in mp and mp[summ] > 1

        return summ // 2 in mp

    def calc(self, root, mp):
        if not root:
            return 0

        curr = root.val + self.calc(root.left, mp) + self.calc(root.right, mp)
        mp[curr] += 1

        return curr


data = '[5,10,10,null,null,2,3]'
# data = '[1,2,3,4,5,6,7,8,9,null,null,29,25,10,11]'
# data = '[1,2,10,null,null,2,20]'
# data = '[1,2,3,4,5,6,3,8,9,null,null,29,25,10,11]'
# data = '[0,-1,1]'
tree = BinaryTree()
root = tree.deserialize(data)

a = Solution().checkEqualTree(root)
print("ans:", a)

