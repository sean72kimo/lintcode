# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from lib.binarytree import TreeNode, BinaryTree
class Solution(object):
    def longestUnivaluePath(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        self.ans = 0
        self.dfs(root)
        return self.ans - 1

    def dfs(self, root):
        if not root:
            return 0, 0

        ll, lr = self.dfs(root.left)
        rl, rr = self.dfs(root.right)

        path_left = 1
        path_right = 1

        if root.left and root.val == root.left.val:
            path_left = max(ll, lr) + 1
        if root.right and root.val == root.right.val:
            path_right = max(rl, rr) + 1

        if root.left and root.right and root.val == root.left.val == root.right.val:
            print(root, ll, lr, rl, rr)

            self.ans = max(max(ll, lr) + max(rl, rr) + 1, self.ans)
        else:
            self.ans = max(path_left, path_right, self.ans)

        return path_left, path_right

data = '[1,4,5,4,4,5]'
data = '[5,4,5,1,1,5]'
tree = BinaryTree()
root = tree.deserialize(data)
sol = Solution()
a = sol.longestUnivaluePath(root)
print("ans:", a)
