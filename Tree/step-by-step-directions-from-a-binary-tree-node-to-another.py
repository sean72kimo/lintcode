class Solution:
    def __init__(self):
        self.path = []

    def getDirections(self, root: Optional[TreeNode], startValue: int, destValue: int) -> str:

        lca = self.lca(root, startValue, destValue)
        ans = self.helper(lca, startValue, destValue)

        return ans

    def lca(self, root, p, q):
        if not root:
            return None
        if root.val == p or root.val == q:
            return root

        left = self.lca(root.left, p, q)
        right = self.lca(root.right, p, q)

        if left and right:
            return root
        if left:
            return left
        if right:
            return right
        return None

    def helper(self, root: Optional[TreeNode], startValue: int, destValue: int) -> str:
        if not root:
            return ""

        self.dfs(root, startValue, [])
        first_half = 'U' * len(self.path)

        self.path = []
        self.dfs(root, destValue, [])
        second_half = ''.join(self.path)

        return first_half + second_half

    def dfs(self, root, target, path):
        if root.val == target:
            self.path = path[:]
            return

        if root.left:
            path.append("L")
            self.dfs(root.left, target, path)
            path.pop()

        if root.right:
            path.append("R")
            self.dfs(root.right, target, path)
            path.pop()