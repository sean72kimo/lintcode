"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(4)
node4 = TreeNode(2)
node5 = TreeNode(3)


root.left = node2
root.right = node3

node2.left = node4
node2.right= node5

import copy
class Solution:
    # @param {TreeNode} root the root of binary tree
    # @param {int} target an integer
    # @return {int[][]} all valid paths

    def binaryTreePathSum(self, root, target):
        # Write your code here
        if root is None:
            return []
        result = []


        total_paths = self.binaryTreePath(root)

        for p in total_paths:
            val = sum(p)
            if val == target:
                result.append(p)

        return result

    def binaryTreePath(self, root):
        paths = []

        if root is None:
            return []
        else:
            pass

        leftPath = self.binaryTreePath(root.left)
        rightPath = self.binaryTreePath(root.right)

        for p in leftPath:
            p.insert(0, root.val)
            paths.append(p)

        for p in rightPath:
            p.insert(0, root.val)
            paths.append(p)

        if not paths:
            paths.append([root.val])

        return paths






sol = Solution()
ans = sol.binaryTreePathSum(root, 5)
print('ans',ans)