
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)
node6 = TreeNode(6)
node7 = TreeNode(7)
node8 = TreeNode(8)

root.left = node2
root.right = node3
node3.left = node4
node3.right = node5
node2.left = node6
node6.left = node7
node7.left = node8

class Solution:
    # @param {TreeNode} root the root of the binary tree
    # @return {List[str]} all root-to-leaf paths
    res = []

    def binaryTreePaths(self, root):
        if root is None:
            return
        self.dfs(root, [])
        return self.res

    def dfs(self, node, tmp):
        tmp.append(str(node.val))
        print(tmp)

        if node.left is None and node.right is None:
            string = '->'.join(tmp)
            self.res.append(string)
            tmp.pop()
            return

        if node.left:
            self.dfs(node.left, tmp)
        if node.right:
            self.dfs(node.right, tmp)

        tmp.pop()

    def binaryTreePaths1(self, root):
        if root is None:
            return -1

        result = []
        self.dfs1(root, result, [])
        return result

    def dfs1(self, node, result, tmp):
        tmp.append(str(node.val))

        if node.left is None and node.right is None:
            string = '->'.join(tmp)
            print(string)
            result.append(string)
            tmp.pop()
            return

        if node.left:
            self.dfs1(node.left, result, tmp)

        if node.right:
            self.dfs1(node.right, result, tmp)

        print('tmp',tmp)
        tmp.pop()

    # divide and conquer
    def binaryTreePaths2(self, root):

        paths = []
        if root is None:
            return paths
        else:
            print('now at',root.val)

        leftPath = self.binaryTreePaths2(root.left)

        rightPath = self.binaryTreePaths2(root.right)
        print(rightPath)


        # if leftPath:
        #     paths.append(str(root.val) + '->' + leftPath[0])
        # if rightPath:
        #     paths.append(str(root.val) + '->' + rightPath[0])

        for p in leftPath:
            paths.append(str(root.val) + '->' + p)

        for p in rightPath:
            paths.append(str(root.val) + '->' + p)

        if not paths:
            paths.append(str(root.val))

        return paths





sol = Solution()
ans = sol.binaryTreePaths2(root)
print('ans',ans)
