class Solution(object):
    def removeDuplicates(self, s):
        """
        :type s: str
        :rtype: str
        """
        a = list(s)
        if len(a) == 0:
            return ""
        if len(a) == 1:
            return ''.join(a)
        if len(a) == 2:
            if a[0] == a[1]:
                return ""
            return ''.join(a)

        i = 0
        tmp = []
        while i < len(a):
            while i < len(a) and i+1 < len(a) and a[i] == a[i+1]:
                if len(tmp) and tmp[-1] == a[i]:
                    tmp.pop()
                i += 1
            tmp.append(a[i])
            i += 1

        print(s, tmp)
        string = self.removeDuplicates(''.join(tmp))

        return string

s = "abbaca"
a = Solution().removeDuplicates(s)
print("ans:", a)