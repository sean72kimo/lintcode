import bisect
import collections


class Solution(object):
    def shortestWay(self, s, t):
        """
        :type source: str
        :type target: str
        :rtype: int
        """
        src = set(s)
        i = 0
        j = 0
        res = 1
        while i < len(t):
            if t[i] not in src:
                return -1

            if j == len(s):
                j = 0
                res += 1

            if s[j] == t[i]:
                j += 1
                i += 1
            else:
                j += 1

        return res


class Solution2(object):
    def shortestWay(self, s, t):
        """
        :type source: str
        :type target: str
        :rtype: int
        """
        pos = collections.defaultdict(list)

        for i, ch in enumerate(s):
            pos[ch].append(i)

        i = j = 0
        n = len(t)
        res = 1
        while i < n:
            if t[i] not in pos:
                return -1

            lst = pos[t[i]]
            j = bisect.bisect_left(lst, i)
            if j == len(lst):
                j = 0
                res += 1
            else:
                i = lst[j] + 1



        return res

sol = Solution2()
s = "abc"
t = "abcbc"

# s = "abc"
# t = "acdbc"
#
# s = "aaaaa"
# t = "aaaaaaaaaaaaa"  #3

# s = "xyz"
# t = "xzyxz" #3

a = sol.shortestWay(s, t)
print("ans:", a)
