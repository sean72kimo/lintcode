import collections
class Solution:
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        if len(s) == 0:
            return ""
        if len(t) == 0:
            return s[:1]
        
        ans = ""
        left = 0
        right = 0
        summ = 0

        mp = collections.Counter(t)
        count = len(mp)
        

        while right < len(s):
            if s[right] in mp:
                mp[s[right]] -= 1
                
                if mp[s[right]] == 0:
                    count -= 1
            print(mp)
            while count == 0:
                if ans == "":
                    ans = s[left : right + 1]
                
                if right - left + 1 < len(ans):
                    ans = s[left : right + 1]

                if s[left] in mp:
                    mp[s[left]] += 1

                    if mp[s[left]] > 0:
                        count += 1

                left += 1

            right += 1
        return ans
s = "ADOBECODEBANC"
t = "ABCC"
a = Solution().minWindow(s, t)
print("ans:",a)