class Solution(object):

    def solveEquation(self, equation):
        """
        :type equation: str
        :rtype: str
        """
        left, right = equation.split('=')
        lx, lc = self.solve(left)
        rx, rc = self.solve(right)

        x = lx - rx
        c = rc - lc

        if x:
            return 'x=' + str(c / x)
        elif c:
            return 'No solution'
        return 'Infinite solutions'

    def solve(self, expr):
        x = c = 0
        num, sign = '', 1

        for ch in expr + '#':
            if '0' <= ch <= '9':
                num += ch

            elif ch == 'x':
                x += int(num or '1') * sign
                num, sign = '', 1

            else:
                c += int(num or '0') * sign
                num, sign = '', 1
                if ch == '-':
                    sign = -1

        return x, c


a = Solution().solveEquation("-1+5x-3+x=6+x-2")
print("ans:", a)
