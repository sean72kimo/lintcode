import heapq


class Solution:
    def strWithout3a3b(self, a: int, b: int) -> str:
        ans = []
        write_a = False
        while a or b:
            n = len(ans)
            if len(ans) >= 2 and ans[-1] == ans[-2]:
                write_a = ans[-1] == 'b'
            else:
                write_a = a >= b

            if write_a:
                a -= 1
                ans.append('a')
            else:
                b -= 1
                ans.append('b')

        return "".join(ans)
a = 4
b = 1
a = Solution().strWithout3a3b(a, b)
print("ans:", a)