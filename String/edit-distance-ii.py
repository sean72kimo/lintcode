class Solution:
    """
    @param: s: a string
    @param: t: a string
    @return: true if they are both one edit distance apart or false
    """
    def isOneEditDistance(self, s, t):
        # write your code here
        hash_s = {}
        hash_t = {}

        len_s = len(s)
        len_t = len(t)

        if len_s > len_t:
            return self.isOneEditDistance(t, s)

        diff = abs(len_s - len_t)
        if diff > 1:
            return False

        if diff == 0:
            cnt = 0
            for i in range(len_s):
                if s[i] != t[i]:
                    cnt += 1
            return cnt == 1

        if diff == 1:
            for i in range(len_s):
                print(i)
                if s[i] != t[i]:
                    return s[i:] == t[i + 1:]

        return True

s = "ab"
t = "a"
print('ans:', Solution().isOneEditDistance(s, t))
