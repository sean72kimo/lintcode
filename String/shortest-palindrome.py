class Solution(object):
    def shortestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        j = 0
        i = len(s) - 1
        
        #"adcba"
        for i in range(len(s)-1,-1,-1):
            if s[i] == s[j]:
                j+=1
        
        if j == len(s):
            return s

        print(j)
        
#         suffix = s[j:]
#         print(suffix)
#         
#         prefix = suffix[::-1]
#         print(prefix)
#         
#         mid = self.shortestPalindrome(s[:j])
#         
#         return prefix + s[:j] + suffix
        


s = "aacecaaa"
# s = "abcd"
s = "adcba"
a = Solution().shortestPalindrome(s)
print(a, a==a[::-1])