import collections


class TrieNode:
    def __init__(self, word=""):
        self.is_word = False
        self.children = {}
        self.word = word


class Trie:
    def __init__(self):
        self.root = TrieNode()

    def add(self, word):
        node = self.root

        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt
        node.is_word = True
        node.word = word

    def search(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                return False
            node = nxt
        return node.is_word


class Solution:
    def addBoldTag(self, s, words) -> str:
        trie = Trie()

        for word in words:
            trie.add(word)

        n = len(s)
        mark = [False for _ in range(n)]
        for i in range(n):
            for j in range(i, n):
                sub = s[i:j + 1]
                print(sub)
                bold = trie.search(sub)
                if not bold:
                    continue
                for k in range(i, j + 1):
                    mark[k] = True

        que = collections.deque([])
        tmp = []
        print(mark)
        mark.append(False)
        first, last = -1, -1
        for i in range(len(mark)):
            if mark[i] and first == -1:
                first = i
                continue
            if mark[i]:
                last = i

            if not mark[i] and first != -1 and last != -1:
                que.append([first, last])
                first, last = -1, -1



        print(que)
        ans = []
        for i in range(n):

            if len(que) and i == que[0][0]:
                if len(ans) and ans[-1] == "</b>":
                    continue
                ans.append("<b>")

            ans.append(s[i])

            if len(que) and i == que[0][1]:
                ans.append("</b>")
                que.popleft()
        return ''.join(ans)





s = "abcxyz123"
words = ["abc","123"]
e = "<b>abc</b>xyz<b>123</b>"

s = "aaabbcc"
words = ["aaa","aab","bc"]
e ="<b>aaabbc</b>c"

s ="abcdef"
words = ["a","c","e","g"]
e = "<b>a</b>b<b>c</b>d<b>e</b>f"

a = Solution().addBoldTag(s, words)
print("ans: ", a, a == e)


