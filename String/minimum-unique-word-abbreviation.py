"""
320. Generalized Abbreviation
408. Valid Word Abbreviation
"""
class Solution(object):
    def minAbbreviation(self, target, dictionary):
        """
        :type target: str
        :type dictionary: List[str]
        :rtype: str
        """
        if len(dictionary) == 0:
            return str(len(target))

        abbrs = []
        self.abbr_target(target, 0, 0, [], abbrs)

        def mykey(x):
            return len(x), x

        abbrs.sort(key=mykey)

        for abbr in abbrs:
            conflict = False

            for word in dictionary:
                if self.valid_abbr(word, abbr):
                    conflict = True
                    break

            if not conflict:
                return abbr

    def valid_abbr(self, word, abbr):
        i = j = 0
        while i < len(word) and j < len(abbr):
            if abbr[j].isdigit():
                if abbr[j] == '0':
                    return False
                leng, j = self.get_len(abbr, j)
                i = i + leng

            elif word[i] == abbr[j]:
                i += 1
                j += 1

            else:
                return False

        return i == len(word) and j == len(abbr)

    def get_len(self, abbr, j):

        num = 0
        while j < len(abbr) and abbr[j].isdigit():
            num = num * 10 + int(abbr[j])
            j += 1
        return num, j

    def abbr_target(self, word, i, k, path, abbr):
        if i == len(word):
            abbr.append(''.join(path))
            return

        path.append(word[i])
        self.abbr_target(word, i + 1, 0, path, abbr)
        path.pop()

        create = False
        if len(path) and path[-1].isdigit():
            path[-1] = str(k + 1)
        else:
            create = True
            path.append(str(k + 1))
        self.abbr_target(word, i + 1, k + 1, path, abbr)
        if create:
            path.pop()
target = "apple"
dictionary = ["blade"]
exp = "a4"

target = "apple"
dictionary = ["plain", "amber", "blade"]
exp = "1p3"

target = "aaaaaxaaaaa"
dictionary = ["bbbbbxbbbbb"]
exp = "a10"
a = Solution().minAbbreviation(target, dictionary)
print("ans:", a)

