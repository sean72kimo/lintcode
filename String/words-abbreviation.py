
import collections

"""
hashmap記錄有多少重複的prefix, prefix[i] array記錄對應的words[i] prefix應該多長才不會產生重複
預設每個單詞都沒有重複的prefix, 每個單詞都取prefix len = 1 即可壓縮，存放在ans[i]
while loop一遍又一遍的loop over ans[i]透過hashmap檢查每個壓縮的單詞都是唯一，不然就增加prefix[i] +=1  並且重新壓縮
"""

class Solution(object):
    def wordsAbbreviation(self, dic):
        """
        :type dict: List[str]
        :rtype: List[str]
        """
        mp = collections.defaultdict(int)  # count for each prefix
        n = len(dic)
        prefix = [1 for _ in range(n)]  # assume each word has its prefix and no overlap
        ans = []

        for word in dic:  # assume there is no conflict
            abbr = self.shorten(word, 1)
            ans.append(abbr)
            mp[abbr] += 1

        unique = False
        while not unique:
            unique = True
            for i in range(n):
                if mp[ans[i]] > 1:
                    prefix[i] += 1
                    abbr = self.shorten(dic[i], prefix[i])
                    ans[i] = abbr

                    mp[abbr] += 1
                    if mp[abbr] > 1:
                        unique = False

        return ans

    def shorten(self, word, i):
        n = len(word)
        size = n - i - 1
        abbr = word[:i] + str(size) + word[-1]
        if len(abbr) >= len(word):
            return word
        return abbr




dict = ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]

print('ans:', Solution().wordsAbbreviation(dict))
