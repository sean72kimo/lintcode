class Solution:
    """
    @param: word1: A string
    @param: word2: A string
    @return: The minimum number of steps.
    """
    def minDistance(self, word1, word2):
        # write your code here
        hash1 = {}
        hash2 = {}

        for c in word1:
            if hash1.get(c) == None:
                hash1[c] = 1
            else:
                hash1[c] += 1

        for c in word2:
            if hash2.get(c) == None:
                hash2[c] = 1
            else:
                hash2[c] += 1

        step = 0
        for key, val in hash1.items():

            if hash2.get(key):
                step += abs(hash1[key] - hash2[key])
            else:
                step += hash1[key]


        for key in hash2:
            if hash1.get(key):
                step += hash2[key]

        return step

word1 = "mart"
word2 = "karma"
print('ans:', Solution().minDistance(word1, word2))
