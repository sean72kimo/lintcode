class Solution(object):
    def repeatedStringMatch(self, A, B):
        """
        :type A: str
        :type B: str
        :rtype: int
        """
        nA = len(A)
        nB = len(B)

        C = ""
        cnt = 0

        while len(C) < nB:
            C = C + A
            cnt += 1

        if B in C:
            return cnt

        C += A
        if B in C:
            return cnt + 1

        return -1

A = "abcd"
B = "cdabcdab"
a = Solution().repeatedStringMatch(A, B)
print("ans:", a)
