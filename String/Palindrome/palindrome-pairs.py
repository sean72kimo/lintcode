class Solution(object):

    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        res = []
        if len(words) == 0:
            return []

        mp = {}
        for i in range(len(words)):
            mp[words[i]] = i

        for i in range(len(words)):
            word = words[i]

            for j in range(len(word) + 1):
                left = word[:j]
                right = word[j:]

                if self.isPalin(right):
                    x = mp.get(left[::-1])
                    if x is not None and x != i:
                        res.append([i, x])

                if not left:  # to avoid duplicate
                    continue

                if self.isPalin(left):
                    y = mp.get(right[::-1])
                    if y is not None and y != i:
                        res.append([y, i])

        return res

    def isPalin(self, word):
        return word == word[::-1]
