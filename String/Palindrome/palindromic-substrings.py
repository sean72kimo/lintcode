"""647. Palindromic Substrings
Input: "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa"
"""
class Solution(object):
    def __init__(self):
        self.count = 0
        
    def countSubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        for i in range(len(s)):
            self.countPalin(s, i, i)
            self.countPalin(s, i, i+1)
        
        return self.count
                
            
    
    def countPalin(self, s, left, right):
        while left >= 0 and right <= len(s)-1 and s[left] == s[right]:
            self.count += 1
            left -= 1
            right += 1

"""
DP, 區間
similar to 5. Longest Palindromic Substring
"""
class Solution(object):
    def countSubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0

        n = len(s)
        f = [[False for _ in range(n)] for _ in range(n)]

        ans = 0
        for i in range(n):
            f[i][i] = True
            ans += 1

        for i in range(n - 1):
            if s[i] == s[i + 1]:
                f[i][i + 1] = True
                ans += 1

        for size in range(3, n + 1):
            for i in range(n):
                j = i + size - 1
                if j >= n:
                    break

                if s[i] == s[j]:
                    f[i][j] |= f[i + 1][j - 1]

                if f[i][j]:
                    ans += 1

        return ans

