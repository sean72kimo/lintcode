"""214. Shortest Palindrome"""


class Solution(object):

    def shortestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        prefix = s[0]
        r = 0
        for i in range(len(s)):
            sub1, r1 = self.findPrefix(s, i, i)
            sub2, r2 = self.findPrefix(s, i, i + 1)

            if len(sub1) > len(sub2):
                sub = sub1

            else:
                sub = sub2

            r = max(r1, r2, r)

            if len(sub) > len(prefix):
                prefix = sub

        suffix = s[r:]

        return suffix[::-1] + prefix + suffix

    def findPrefix(self, s, left, right):
        while left >= 0 and right < len(s) and s[left] == s[right]:
            left -= 1
            right += 1

        if left + 1 == 0:
            return s[left + 1 : right], right
        return "", 0


class Solution_bruteforce(object):

    def shortestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) == 0:
            return ""

        n = len(s)

        for i in range(n, -1, -1):
            if s[:i] == s[:i][::-1]:

                ans = s[i:][::-1] + s
                print(i, s[:i], ans)
                return ans


s = "aacecaaa"
s = "abcd"
a = Solution_bruteforce().shortestPalindrome(s)
print("ans:", a)
