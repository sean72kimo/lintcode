"""5. Longest Palindromic Substring

DP solution is similar to longest-palindromic-subsequence

"""
class Solution(object):
    def longestPalindrome(self, s):
        if len(s) == 0:
            return 0
        
        ans = s[0]
        for i in range(len(s)):
            sub1 = self.count(s, i, i)
            sub2 = self.count(s, i, i+1)
            
            if len(sub1) > len(sub2):
                sub = sub1
            else:
                sub = sub2
            
            if len(sub) > len(ans):
                ans = sub
            
        return ans
    
    def count(self, s, left, right):
        while left >=0 and right < len(s) and s[left] == s[right]:
            left -= 1
            right += 1
        return s[left+1 : right]

"""
DP, 區間
同647. Palindromic Substrings
"""
class Solution_dp(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) == 0:
            return ""
        n = len(s)
        f = [[False for _ in range(n)] for _ in range(n)]

        # f[i][j] = f[i+1][j-1] if s[i] == [j]

        maxL = 0
        ans = s[0]
        for i in range(n - 1):
            f[i][i] = True

            if s[i] == s[i + 1]:
                f[i][i + 1] = True
                ans = s[i:i + 2]
                maxL = 2

        for size in range(3, n + 1):
            for i in range(n):
                j = i + size - 1
                if j >= n:
                    break

                if s[i] == s[j]:
                    f[i][j] |= f[i + 1][j - 1]

                if f[i][j] and j - i + 1 > maxL:
                    ans = s[i:j + 1]

        return ans
s = "ccc"
a = Solution().longestPalindrome(s)
print(a)
