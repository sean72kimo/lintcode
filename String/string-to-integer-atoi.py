class Solution(object):
    def myAtoi(self, strings):
        """
        :type str: str
        :rtype: int
        1. max_int & min_int, if java 要宣告val 為long避免越界，但是若超過max int & min int範圍，則要返回max_int, or min_int
        2. string strip 空白後，可能開頭為 '+' or '-'，要記得處理。用sign = 1 or sign = -1
        3. 累加過程中，如果val > max_int就停止累加，並離開迴圈

        try this one 65. Valid Number
        """
        MAX_INT = 2 ** 31 - 1
        MIN_INT = -2 ** 31

        s = strings.strip()
        if len(s) == 0:
            return 0

        sign = 1
        val = 0
        for i in range(len(s)):
            if i == 0:
                if s[i] == '-':
                    sign = -1
                    continue
                elif s[i] == '+':
                    sign = 1
                    continue

            if not s[i].isdigit():
                break

            val = val * 10 + int(s[i])
            if val > MAX_INT:
                break

        if val * sign >= MAX_INT:
            return MAX_INT

        if val * sign <= MIN_INT:
            return MIN_INT

        return val * sign
