class Solution:

    def backspaceCompare(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: bool
        """
        i = len(S) - 1
        j = len(T) - 1
        backS = 0
        backT = 0

        while True:
            while i >= 0 and (backS or S[i] == '#'):
                backS += 1 if S[i] == '#' else -1
                i -= 1

            while j >= 0 and (backT or T[j] == '#'):
                backT += 1 if T[j] == '#' else -1
                j -= 1

            if not(i >= 0 and j >= 0 and S[i] == T[j]):
                return i == j == -1

            i -= 1
            j -= 1


S, T = "a##c", "#a#c"
S, T = "ab##", "c#d#"
S, T = "a#c", "b"
a = Solution().backspaceCompare(S, T)
print("ans:", a)


class Solution_stack:

    def backspaceCompare(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: bool
        """
        stack1 = []
        stack2 = []

        i = 0
        j = 0
        for i in range(len(S)):
            if S[i] == '#':
                if stack1: stack1.pop()
            else:
                stack1.append(S[i])

        for i in range(len(T)):
            if T[i] == '#':
                if stack2: stack2.pop()
            else:
                stack2.append(T[i])

        return stack1 == stack2

