# import itertools
# n = 5
# for x in itertools.combinations(range(1, n), 2):
#     print (x)

# http://bookshadow.com/weblog/2015/11/18/leetcode-additive-number/

class Solution2(object):
    def isAdditiveNumber(self, num):
        """
        :type num: str
        :rtype: bool
        """

        n = len(num)
        for i in range(1, n):
            for j in range(i + 1, n):
                a = num[:i]
                b = num[i:j]

                # check if string with leading 0, such as 02
                if a != str(int(a)) or b != str(int(b)):
                    continue

                while j < n:
                    c = str(int(a) + int(b))
                    if not num.startswith(c, j):
                        break
                    j = j + len(c)
                    a, b = b, a

                if j == n:
                    return True
                return False

class Solution(object):
    def isAdditiveNumber(self, num):
        """
        :type num: str
        :rtype: bool
        """

        n = len(num)

        for i in range(1, n):
            for j in range(i + 1, n):
                a = num[:i]
                b = num[i:j]
                r = num[j:]
                # check if string with leading 0, such as 02
                if a != str(int(a)) or b != str(int(b)):
                    continue

                while j < n:
                    c = str(int(a) + int(b))
                    if not r.startswith(c):
                        break
                    j = j + len(c)
                    a = b
                    b = c
                    r = num[j:]

                if j == n:
                    return True
        return False

num = "112358"
ans = Solution().isAdditiveNumber(num)
print('ans', ans)

