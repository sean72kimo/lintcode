class Solution:

    def tokenize(self, num):
        token = ""
        while num:
            d = num % 26
            token = self.toCh(d) + token
            num = num // 26
        return token

    def recover(self, s):
        num = 0
        for ch in s:
            n = self.toInt(ch)
            num = num * 26 + n
        return  num

    def toInt(self, ch):
        return ord(ch) - ord('a')

    def toCh(self, n):
        dist = ord('a') + n
        ch = chr(dist)
        return ch

sol = Solution()
num = 7843
token = sol.tokenize(num)
print(token)
n = sol.recover(token)
print(n, n==num)

