# max depth is the max count of (
class Solution:
    def maxDepth(self, s: str) -> int:
        cnt = 0
        ans = 0

        for ch in s:
            if ch == "(":
                cnt += 1
                ans = max(cnt, ans)
            elif ch == ")":
                cnt -= 1

        return ans