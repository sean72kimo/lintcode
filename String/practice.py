class Solution:
    def __init__(self):
        self.diff = float('inf')
        self.ans = None
    """
    @param time: the given time
    @return: the next closest time
    """
    def nextClosestTime(self, time):
        lst = []
        for c in time:
            if c == ':':
                continue
            lst.append(int(c))
        mm = self.getMin(lst)
        self.dfs(lst, [], mm)
        print(self.ans)

        hh = self.ans[0] * 10 + self.ans[1]
        mm = self.ans[2] * 10 + self.ans[3]
        ans_str = "{0:02d}:{1:02d}".format(hh, mm)
        return ans_str
        
    def dfs(self, lst, path, mm):
        if len(path) == 4:
            
            if self.isValid(path):
                diff = self.getDiff(path, mm)
                if diff < self.diff:
                    self.diff = diff
                    self.ans = path[:]
            return
        
        for v in lst:
            path.append(v)
            self.dfs(lst, path, mm)
            path.pop()
    
    def isValid(self, path):
        hh = path[0] * 10 + path[1]
        if hh > 23:
            return False
        mm = path[2] * 10 + path[3]
        if mm > 59:
            return False
        return True
            
    def getMin(self, lst):
        hh = lst[0] * 10 + lst[1]
        mm = lst[2] * 10 + lst[3]
        return hh * 60 + mm
    
    def getDiff(self, path, mm):
        now = self.getMin(path)
        if now > mm:
            diff =  now - mm
        else:
            diff = 1440 - mm + now
        return diff

time = "19:34"
a = Solution().nextClosestTime(time)
print("ans:", a)

