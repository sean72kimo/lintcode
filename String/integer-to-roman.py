class Solution:
    """
    @param: n: The integer
    @return: Roman representation
    """
    def intToRoman(self, n):
        # write your code here
        I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        M = ["", "M", "MM", "MMM"]
        print((n // 100) % 10)
        thousand = (n // 1000) % 10
        hundred = (n // 100) % 10
        ten = (n // 10) % 10
        digit = n % 10
        ans = M[thousand] + C[hundred] + X[ten] + I[digit]
        return ans



a = Solution().intToRoman(3999)
print("ans:", a)
