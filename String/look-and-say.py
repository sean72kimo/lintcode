









# Enter your code here. Read input from STDIN. Print output to STDOUT
"""
1st iteration
input: 112222
output: 2142

2nd iteration
input: 2142
output: 12 11 14 12

11 12 31 14 11 12
"""

#12
def lookAndSay(seed, iteration=1):

    data = seed
    for iter in range(iteration):
        cnt = 1
        temp = ""

        for i, ch in enumerate(data):
            if i > 0 and data[i] == data[i-1]:
                cnt += 1
            elif i > 0 and data[i] != data[i-1]:
                temp = temp + str(cnt) + data[i-1]
                cnt = 1
     
        temp = temp + str(cnt) + data[i]
        data = temp

     
    return data

            

seed = "112222"

iteration = 2
a = lookAndSay(seed, iteration)
print("ans:", a)