class Solution:

    def isAlienSorted(self, words, order):
        """
        :type words: List[str]
        :type order: str
        :rtype: bool
        """
        myorder = [None] * 26

        for i, ch in enumerate(order):
            myorder[ord(ch) - ord('a')] = i

        for i in range(len(words) - 1):
            word1 = words[i]
            word2 = words[i + 1]
            mx = max(len(word1), len(word2))

            for j in range(mx):
                if j > len(word2) - 1:
                    return False

                if myorder[ord(word1[j]) - ord('a')] < myorder[ord(word2[j]) - ord('a')]:
                    break
                elif myorder[ord(word1[j]) - ord('a')] == myorder[ord(word2[j]) - ord('a')]:
                    continue
                else:
                    return False

        return True


words = ["hello", "leetcode"]
order = "hlabcdefgijkmnopqrstuvwxyz"

# words = ["word", "world", "row"]
# order = "worldabcefghijkmnpqstuvxyz"

# words = ["apple", "app"]
# order = "abcdefghijklmnopqrstuvwxyz"
a = Solution().isAlienSorted(words, order)
print("ans:", a)
