"""
N = len(indexes), S = len(S)
time NlogN + S (all ch in S will only be vistited once and rebuild in ans string)
"""
class Solution(object):
    def findReplaceString(self, S, indexes, sources, targets):
        """
        :type S: str
        :type indexes: List[int]
        :type sources: List[str]
        :type targets: List[str]
        :rtype: str
        """

        start = 0
        n = len(S)
        ans = ""
        rules = list(zip(indexes, sources, targets))

        rules.sort()

        for j in range(len(rules)):
            idx = rules[j][0]

            if 0 <= idx < n:
                ans += S[start: idx]

            sub = rules[j][1]

            if S[idx: idx + len(sub)] == sub:
                ans += rules[j][2]
            else:
                ans += S[idx: idx + len(sub)]

            start = idx + len(sub)
        ans += S[start:]
        return ans

sol = Solution()
S = "abcd"
indexes = [0,2]
sources = ["a","cd"]
targets = ["eee","ffff"]
exp = "eeebffff"

S = "abcd"
indexes = [0, 2]
sources = ["ab", "ec"]
targets = ["eee", "ffff"]
exp = "eeecd"

S = "vmokgggqzp"
indexes = [3,5,1]
sources = ["kg","ggq","mo"]
targets = ["s","so","bfr"]
exp = "vbfrssozp"
a = sol.findReplaceString(S, indexes, sources, targets)
print("ans:", a)