class Solution:
    """
    @param s: a string
    @return: a string after capitalizes the first letter
    """
    def capitalizesFirst(self, s):
        # Write your code here
        if len(s) == 0:
            return ""
        diff = ord('a') - ord('A')
        first = True
        ans = ""
        for ch in s:
            if ch.isspace():
                ans += ch
                first = True
            
            if ch.isalpha():
                if first:
                    capitalied = chr(ord(ch) - diff)
                    ans += capitalied
                    first = False
                else:
                    ans += ch
        return ans
s = "i want to get an accepted"
s = "   i   jkdls    lakjdifs   mdsljidja midsals"
a = Solution().capitalizesFirst(s)
print("ans:", a)