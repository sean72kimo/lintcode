class Solution(object):
    def reverseWords(self, s):
        """
        :type s: str
        :rtype: str
        """
        split_s = s.split(sep=" ")
        new_s = list(reversed(split_s))
        res = ' '.join(new_s)
        return res
s = "the sky is blue"
print('ans:', Solution().reverseWords(s))
