class Solution(object):

    def parseTernary(self, s):
        """
        :type expression: str
        :rtype: str
        """

        stack = []
        sub = ''
        for ch in s + '#':
            if '0' <= ch <= '9':
                sub += ch
            elif ch == "?":
                stack.append(sub)
                sub = ""
            elif ch == ':' or ch == '#':
                if len(sub) and len(stack) > 1:
                    r = sub
                    l = stack.pop()
                    t = stack.pop()
                    res = l if t == 'T' else r
                    stack.append(res)
                    sub = ""
                    continue

                stack.append(sub)
                sub = ""

            else:
                sub = ch

        print(stack)
        return stack[0]

    def get_res(self, expr):
        res = ''
        left, right = expr.split('?')
        a, b = right.split(':')

        return a if left == 'T' else b


s = "T?2:3"
s = "F?1:T?4:5"
# s = "T?T?F:5:3"
a = Solution().parseTernary(s)
print("ans:", a)
