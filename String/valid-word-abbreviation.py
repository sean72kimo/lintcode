class Solution:

    def validWordAbbreviation(self, word, abbr):
        """
        :type word: str
        :type abbr: str
        :rtype: bool
        """

        i = j = 0
        while i < len(word) and j < len(abbr):

            if abbr[j].isdigit():
                if abbr[j] == '0':  # word:"a", abbr:"01"
                    return False
                print(i, j)
                num = 0
                while j < len(abbr) and abbr[j].isdigit():
                    num = num * 10 + int(abbr[j])
                    j += 1

                i += num
                print((i, word[i]), (j, abbr[j]))

            else:
                if abbr[j] != word[i]:
                    return False
                i += 1
                j += 1

        return i == len(word) and j == len(abbr)


word = "internationalization"
abbr = "i5a11o1"
e = True

word = "internationalization"
abbr = "i12iz4n"

word = "a"
abbr = "01"
e = False

word = "internat"
abbr = "i5at"
e = True
a = Solution().validWordAbbreviation(word, abbr)
print("AC" if a == e else "WA")

