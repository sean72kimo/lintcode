class Solution:
    """
    @param: nums: a sorted integer array
    @param: lower: An integer
    @param: upper: An integer
    @return: a list of its missing ranges
    """
    ans = []
    def findMissingRanges2(self, nums, lower, upper):
        # write your code here
        if nums is None or len(nums) == 0:
            self.get_range(lower, upper)
            return self.ans

        if len(nums) == 0 and lower == upper:
            self.ans.append(lower)
            return self.ans

        if lower != nums[0]:
            self.get_range(lower, nums[0] - 1)

        for i in range(len(nums)):
            if i == len(nums) - 1 :
                break
            curr = nums[i]
            next = nums[i + 1]
            if next == curr + 1:
                continue
            if next == curr:
                continue

            self.get_range(curr + 1, next - 1)

        if upper != nums[-1]:
            self.get_range(nums[-1] + 1, upper)

        return self.ans

    def findMissingRanges(self, nums, lower, upper):
        if len(nums) == 0:
            self.get_range(lower, upper)
            return self.ans

        self.get_range(lower, nums[0] - 1)

        for i in range(1, len(nums)):
            self.get_range(nums[i - 1] + 1, nums[i] - 1, i)

        self.get_range(nums[-1] + 1, upper)
        return self.ans

    def get_range(self, start, end, i = None):
        if start > end:
            print(start, end, i)
            return
        range = str(start) + '->' + str(end) if start != end else str(start)
        self.ans.append(range)

nums = [0, 1, 3, 50, 75]
lower = 0
upper = 99

print('ans:', Solution().findMissingRanges(nums, lower, upper))
