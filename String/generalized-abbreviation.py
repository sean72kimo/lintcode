"""
backtracking: dfs時候每一個char可能縮寫或者可能不縮寫。
如果縮寫，有可能是append to path, 有可能是修改path[-1],
所以每次backtrack的時候, 有可能是pop, 也有可能是改回原值
"""
class Solution(object):
    def generateAbbreviations(self, word):
        """
        :type word: str
        :rtype: List[str]
        """
        self.ans = []
        self.dfs(word, 0, 0, [])
        return self.ans

    def dfs(self, word, i, k, path):
        if i == len(word):
            self.ans.append(''.join(path))
            return

        create = False
        if len(path) == 0 or path[-1].isalpha():
            create = True
            path.append(str(k + 1))
        else:
            path[-1] = str(k + 1)
        self.dfs(word, i + 1, k + 1, path)
        if create:
            path.pop()
        else:
            path[-1] = str(int(path[-1]) - 1)

        path.append(word[i])
        self.dfs(word, i + 1, 0, path)
        path.pop()


word = "googleqwerqwerqwer"
a = Solution().generateAbbreviations(word)
print("ans:", len(a))


class Solution_get_count(object):
    def generateAbbreviations(self, word):
        """
        :type word: str
        :rtype: List[str]
        """
        if len(word) == 0:
            return []
        self.ans = []
        self.mem = {}
        res = self.dfs(word, 0, 0, [])
        return res

    def dfs(self, word, i, k, path):
        if i in self.mem:
            print('i, k', i, k)
            return self.mem[i]

        if i == len(word):
            return 1

        res = 0
        res += self.dfs(word, i+1, 0, path)
        res += self.dfs(word, i+1, k+1, path)

        self.mem[i] = res
        return res

a = Solution_get_count().generateAbbreviations(word)
print("ans:", a)



class Solution2(object):
    def generateAbbreviations(self, word):
        """
        :type word: str
        :rtype: List[str]
        """

        self.ans = []

        self.dfs(word, 0, "", 0)
        return self.ans

    def dfs(self, word, pos, cur, cnt):
        if pos == len(word):
            if cnt:
                cur = cur + str(cnt)
            self.ans.append(cur)
            return

        self.dfs(word, pos + 1, cur, cnt + 1)

        if cnt:
            cur = cur + str(cnt) + word[pos]
        else:
            cur = cur + word[pos]

        self.dfs(word, pos + 1, cur, 0)

# word = "word"
# a = Solution2().generateAbbreviations(word)
# print("ans:", a)


class Solution3(object):
    def generateAbbreviations(self, word):
        """
        :type word: str
        :rtype: List[str]
        """

        self.ans = []

        self.dfs(word, 0, "", False)
        return self.ans

    def dfs(self, word, i, path, abbr):
        if i == len(word):
            self.ans.append(path)
            return

        self.dfs(word, i+1, path + word[i], False)

        if not abbr:
            for j in range(1, len(word)-i+1):
                self.dfs(word, i+j, path+str(j), True)
# word = "word"
# a = Solution3().generateAbbreviations(word)
# print("ans:", a)


