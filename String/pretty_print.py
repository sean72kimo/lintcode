class PrettyPrint:
    def print(self, s, space = 2):
        if len(s) == 0:
            return

        tmp = ""
        bal = 0
        stack = []
        tab = " " * space

        for ch in s:
            if ch.isalpha():
                tmp += ch

            elif ch == '(':
                if len(tmp):
                    text = tab * bal + tmp
                    stack.append(text)
                    tmp = ""

                text = tab * bal + '('
                stack.append(text)
                bal += 1

            elif ch == ')':
                if len(tmp):
                    text = tab * bal + tmp
                    stack.append(text)
                    tmp = ""
                bal -= 1
                text = tab * bal + ')'
                stack.append(text)
        if tmp:
            stack.append(tmp)

        for itm in stack:
            print(itm)





s = "(ab)"
s = "(ab)(cd(ef))"
s = "ab"
s = "xy((ab(pq)))mn(cd)"
a = PrettyPrint().print(s, 4)
print("ans:", a)



class Print:
    def print_nested(self, lst):
        if len(lst) == 0:
            return
        path  = []
        self.dfs(nested, path)
        return path


    def dfs(self, nested, path):
        if len(nested) == 0:
            return

        for i in range(len(nested)):
            print(nested[i])
            if isinstance(nested[i], int):
                path.append(nested[i])
            else:
                self.dfs(nested[i], path)

nested = [1,[2, 3],[[],[4, [5,6]],7],8]
p = Print()
ret = p.print_nested(nested)
print(ret)