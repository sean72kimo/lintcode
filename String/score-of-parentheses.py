class Solution(object):
    def scoreOfParentheses(self, S):
        return self.dfs(S, 0, len(S))

    def dfs(self, S, i, j):
        if i >= j - 1:
            return 0

        ans = bal = 0

        for k in range(i, j):
            bal += 1 if S[k] == '(' else -1
            if bal == 0:
                if k - i == 1:
                    ans += 1
                else:
                    ans += 2 * self.dfs(S, i + 1, k)
                i = k + 1

        return ans


s = "(()(()))"
# s = "()()"
a = Solution().scoreOfParentheses(s)
print("ans:", a)