class Solution(object):
    def compareVersion(self, version1, version2):
        """
        :type version1: str
        :type version2: str
        :rtype: int
        """

        versions1 = [int(v) for v in version1.split(".")]
        versions2 = [int(v) for v in version2.split(".")]


        print(versions1)
        print(versions2)

        n = max(len(versions1), len(versions2))
        for i in range(n):
            v1 = versions1[i] if i < len(versions1) else 0
            v2 = versions2[i] if i < len(versions2) else 0
            if v1 > v2:
                return 1
            elif v1 < v2:
                return -1;
        return 0;

v1 = "1.0"
v2 = "1"
a = Solution().compareVersion(v1, v2)
print('ans', a)
