class Solution:
    """
    @param: num: a string
    @return: true if a number is strobogrammatic or false
    """
    def isStrobogrammatic(self, num):
        # write your code here
        if num is None or len(num) == 0:
            return True


        hashMap = {
            '0':'0',
            '1':'1',
            '6':'9',
            '8':'8',
            '9':'6',
        }


        j = len(num) - 1
        for i in range(len(num)):
            if hashMap.get(num[i]) != num[j]:
                return False
            j -= 1


        return True

num = '69'
print('ans:', Solution().isStrobogrammatic(num))

