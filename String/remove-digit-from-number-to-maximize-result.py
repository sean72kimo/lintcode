class Solution:
    def removeDigit(self, nums: str, digit: str) -> str:
        for i in range(len(nums) - 1):
            if nums[i] == digit and nums[i + 1] > nums[i]:
                return nums[:i] + nums[i + 1:]

        for i in range(len(nums) - 1, -1, -1):
            if nums[i] == digit:
                return nums[:i] + nums[i + 1:]


number = "123"
digit = "3"
e = "12"

number = "7795478535679443616467964135298543"
digit = "5"
Expected = "779547853679443616467964135298543"

a = Solution().removeDigit(number, digit)
print("ans:", a==Expected, a)

print(a)
print(Expected)