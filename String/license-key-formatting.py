class Solution(object):
    def licenseKeyFormatting(self, S, K):
        """
        :type S: str
        :type K: int
        :rtype: str
        """

        s = S.replace("-", "").upper()
        print(s)
        n = len(s)
        ans = ""
        for i in range(n - 1, -1, -K):
            print("starting at", i)

            if i - K + 1 >= 0:
                tmp = s[i - K + 1 : i + 1 ]
            else:
                tmp = s[:i + 1]

            ans = tmp + "-" + ans


        return ans.rstrip("-")
S = "5F3Z-2e-9-w"
K = 4
S = "2-5g-3-J"
K = 2
a = Solution().licenseKeyFormatting(S, K)
print("ans:", a)
