class Solution:
    """
    @param: num1: a non-negative integers
    @param: num2: a non-negative integers
    @return: return sum of num1 and num2
    """
    def addStrings(self, num1, num2):
        # write your code here


        if len(num1) < len(num2):
            num1, num2 = num2, num1

        while len(num2) < len(num1):
            num2 = "0" + num2

        n = len(num1)
        carry = 0
        ans = ""

        for i in range(n - 1, -1, -1):
            sm = carry
            sm += int(num1[i]) + int(num2[i])
            carry = sm // 10
            sm %= 10

            ans = str(sm) + ans

        if carry != 0:
            ans = str(carry) + ans
        print (ans)
        return ans
num1 = "9999999999981"
num2 = "19"
Solution().addStrings(num1, num2)
