import collections
from sys import maxsize
class Solution(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s is None or len(s) == 0:
            return 0

        start = 0
        end = 0
        cnt = 0
        map = collections.defaultdict(int)
        d = -maxsize

        while end < len(s):
            c = s[end]

            map[c] += 1
            if map[c] == 1:
                cnt += 1
            end += 1

            while cnt > 2:
                print(s[start:end], start, end)
                tmp = s[start]
                map[tmp] -= 1
                if map[tmp] == 0:
                    cnt -= 1
                start += 1

            print('[dis]:', s[start:end], start, end)
            d = max(d, end - start)
        return d


# s = "eceba"
# a = Solution().lengthOfLongestSubstringTwoDistinct(s)
# print('ans', a)


import collections
from sys import maxsize
class Solution2(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s is None or len(s) == 0:
            return 0
        if len(s) <= 2:
            return len(s)

        start = 0
        end = 0
        mp = collections.defaultdict(int)
        length = 0

        for i in range(len(s)):
            mp[s[i]] += 1

            while len(mp) > 2:
                length = max(length, end - start)

                mp[s[start]] -= 1
                if mp[s[start]] == 0:
                    del mp[s[start]]
                start += 1

            end += 1
            length = max(length, end - start)

        return length

s = "eeceeba"
# s = "aac"
a = Solution2().lengthOfLongestSubstringTwoDistinct(s)
print('ans', a)
