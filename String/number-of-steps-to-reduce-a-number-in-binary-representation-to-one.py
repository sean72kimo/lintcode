
class Solution:
    def numSteps(self, s: str) -> int:

        num = 0
        d = 0
        n = len(s)

        for i in range(n - 1, -1, -1):
            num += int(s[i]) * 2 ** d
            d += 1
        step = 0
        while num > 1:
            if num % 2:
                num += 1
                step += 1
            num = num >> 1
            step += 1

        return step


"""""
https://leetcode.com/problems/number-of-steps-to-reduce-a-number-in-binary-representation-to-one/discuss/1184352/JavaPython-Clean-and-Concise-Clear-Explanation-O(N)
"""
class Solution:
    def numSteps(self, s: str) -> int:
        carry = 0
        ans = 0
        n = len(s)
        # for loop exclude first digit
        # cuz first digit has been shift right
        for i in range(n - 1, 0, -1):
            if int(s[i]) + carry == 1:
                carry = 1
                ans += 2
            else:
                ans += 1

        return ans + carry


