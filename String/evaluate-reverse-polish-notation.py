class Solution:
    """
    @param tokens: The Reverse Polish Notation
    @return: the value
    """
    def evalRPN(self, tokens):
        # write your code here
        if len(tokens) == 0:
            return 0
        
        oper = {'+', '-', '*', '/'}
        stack = []
        
        for ch in tokens:
            if ch not in oper:
                stack.append(int(ch))
                continue
            print(ch, stack)
            a = stack.pop()
            b = stack.pop()
            
            if ch == '+':
                stack.append(a + b)
            elif ch == '-':
                stack.append(b - a)
            elif ch == '*':
                stack.append(b * a)
            else:
                stack.append(int(b / a))
            
        return stack[0]

tokens = ["18"]
tokens = ["4","13","5","/","+"]
tokens = ["0","3","/"]
tokens = ["4","-2","/","2","-3","-","-"]
a = Solution().evalRPN(tokens)
print("ans:", a)




