import collections
from typing import List


class Solution:
    def getFolderNames(self, names: List[str]) -> List[str]:
        versions = collections.defaultdict(int)

        ans = []
        for name in names:
            if name not in versions:
                versions[name] = 0
                ans.append(name)
                continue

            ver = versions[name]
            modified = name

            while modified in versions:
                ver += 1
                modified = f'{name}({ver})'

            ans.append(modified)
            versions[modified] = 0
            versions[name] = ver

        return ans

class Solution:
    def getFolderNames(self, names: List[str]) -> List[str]:
        mp = {}
        exists = set()
        last = collections.defaultdict(int)

        res = []

        for name in names:
            ver = last[name]
            modified = name

            while modified in exists:
                ver += 1
                modified = f'{name}({ver})'

            last[name] = ver
            res.append(modified)
            exists.add(modified)
        return res

