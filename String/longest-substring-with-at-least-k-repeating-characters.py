import collections
class Solution(object):
    def longestSubstring(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        if len(s) < k or len(s) == 0:
            return 0
        if k == 0:
            return len(s)

        ch_map = collections.Counter(s)

#         i = 0
#         while i < len(s) and ch_map[s[i]] >= k:
#             i += 1
#         print("i",i)
        
        i = 0
        for i in range(len(s)):
            if ch_map[s[i]] < k:
                break
        if i == len(s)-1:
            i+=1


        if i == len(s):
            return len(s)

        left = self.longestSubstring(s[:i], k)
        right = self.longestSubstring(s[i + 1:], k)

        return max(left, right)

s = "aaxbbcc"
k = 2
a = Solution().longestSubstring(s, k)
print("ans:",a)
