class Solution:
    """
    @param: character: a character
    @return: a character
    """
    def lowercaseToUppercase(self, ch):
        # write your code here
        diff = ord('a') - ord('A')

        ans = chr(ord(ch) - diff)

        return str(ans)


ans = Solution().lowercaseToUppercase('a')
print('ans', ans)
