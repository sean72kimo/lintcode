class Solution(object):


    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """

        if n == 0:
            return ''

        def countIdx(s):
            c = s[0]
            cnt = 1
            rt = ""
            for i in range(1, len(s)):
                if s[i] == c:
                    cnt += 1
                else:
                    rt = rt + str(cnt) + c
                    c = s[i]
                    cnt = 1
            rt = rt + str(cnt) + c

            return rt

        s = "1"

        for i in range(1, n):
            s = countIdx(s)

        return s
n= 5
a = Solution().countAndSay(n)
print('ans:', a)


class Solution2(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        if n == 0:
            return ""

        if n == 1:
            return '1'

        seed = '1'

        for _ in range(n - 1):
            seed = self.getStr(seed)

        return seed

    def getStr(self, s):
        i = 1
        cnt = 1
        t = s[0]
        rt = ""
        while i < len(s):
            if s[i] == s[i - 1]:
                cnt += 1
            else:
                rt += str(cnt) + t
                t = s[i]
                cnt = 1
            i += 1

        rt += str(cnt) + t
        return rt

sol = Solution2()
a = sol.countAndSay(1)
print(a)