class Solution:
    """
    @param s1: a string
    @param s2: a string
    @return: is s1 and s2 are equivalent
    """
    def isEquivalentStrings(self, s1, s2):
        # Write your code here
        if s1 == s2:
            return True
        return self.equivalent(s1, s2)
    
    def equivalent(self, s1, s2):
        if len(s1) == 0 or len(s2) == 0:
            return False
        #print(s1, s2)
        if s1 == s2:
            return True
        
        mid = len(s1) // 2
        
        a1 = s1[:mid]
        a2 = s1[mid:]
        
        b1 = s2[:mid]
        b2 = s2[mid:]
        
        if (self.equivalent(a1, b1) and self.equivalent(a2, b2)) or (self.equivalent(a1, b2) and self.equivalent(a2, b1)):
            return True
        
        return False
sol = Solution()
s1 = "aaba"
s2 = "abaa"
print("ans",sol.isEquivalentStrings(s1, s2))


s1 = "aabb"
s2 = "abab"
print("ans",sol.isEquivalentStrings(s1, s2))


s1 = "uwzwdxfmosmqatyv"
s2 = "dxfmzwwusomqvyta"
print("ans",sol.isEquivalentStrings(s1, s2))