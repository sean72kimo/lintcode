class Solution:
    """
    @param: strs: a list of strings
    @return: encodes a list of strings to a single string.
    """
    def encode(self, strs):
        # write your code here
        string = ""
        for each_str in strs:
            for c in each_str:
                if c == ':':
                    c = '::'
                string += c
            string += ':;'
        return string


    """
    @param: str: A string
    @return: dcodes a single string to a list of strings
    """
    def decode(self, str):
        # write your code here
        strs = []
        temp = ""
        i = 0
        while i < len(str):
            if str[i] == ':' and str[i + 1] == ':':
                temp += ':'
                i += 1
            elif str[i] == ':' and str[i + 1] == ';':
                strs.append(temp)
                temp = ""
                i += 1
            else:
                temp += str[i]

            i += 1
        return strs

strs = ["lint", "code", "love", "you"]
str = "li::nt:;code:;love:;you:;"
sol = Solution()
print(sol.encode(strs))
print(sol.decode(str))

