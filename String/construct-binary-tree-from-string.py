# Definition for a binary tree node.

from lib.binarytree import BinaryTree
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def str2tree(self, s):
        """
        :type s: str
        :rtype: TreeNode
        """
        if len(s) == 0:
            return
        s = '(' + s + ')'
        return self.helper(s)

    def getNode(self, s):
        # print("getNode:", s)
        sign = 1
        if s[0] == '-':
            sign = -1
            num = int(s[1:]) * sign
        else:
            num = int(s)

        return TreeNode(num)

    def helper(self, s):
        print("helper:", s , end = " pos: ")
#         if len(s) == 0:
#             return

        if s.count('(') == 1 and s.count(')') == 1:
            size = len(s)
            s = s[1:size - 1]
            return self.getNode(s)

        size = len(s)
        s = s[1:size - 1]
        pos = s.find('(')
        num_str = s[:pos]
        print(pos, num_str)

        root = self.getNode(num_str)
        rest = s[pos:]
        print("rest:", rest)

        if len(rest) == 0:
            return

        l = 0
        cnt = 0
        for i, ch in enumerate(rest):
            if ch == '(':
                cnt += 1
            elif ch == ')':
                cnt -= 1

            if cnt == 0:
                break
        lPart = rest[:i + 1]
        rPart = rest[i + 1:]
        left = right = None
        if lPart:
            left = self.helper(lPart)
        if rPart:
            right = self.helper(rPart)

        if left:
            root.left = left
        if right:
            root.right = right

        return root

s = "4(2(3)(1))(6(5))"
root = Solution().str2tree(s)
tree = BinaryTree()
ans = tree.serialize(root)
print("ans", ans)
