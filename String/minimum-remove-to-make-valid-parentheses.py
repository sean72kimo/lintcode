class Solution:
    def minRemoveToMakeValid(self, s):
        print("input:", s)
        s = self.delete_invalid_closing(s, "(", ")")
        print(s)
        s = self.delete_invalid_closing(s[::-1], ")", "(")
        return s[::-1]

    def delete_invalid_closing(self, string, open_symbol, close_symbol):
        res = []
        balance = 0

        for c in string:
            if c == open_symbol:
                balance += 1
            elif c == close_symbol:
                if balance == 0:
                    continue
                balance -= 1
            res.append(c)
        return "".join((res))




s = "lee(t(c)o)de)("
# s = [1,2,3]
ans = Solution().minRemoveToMakeValid(s)
print("ans:", ans)
