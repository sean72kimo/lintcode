class Solution(object):
    def fullJustify(self, words, maxWidth):
        """
        :type words: List[str]
        :type maxWidth: int
        :rtype: List[str]
        """
        line = []
        ans = []
        length = 0

        for word in words:
            if length + len(line) + len(word) <= maxWidth:
                line.append(word)
                length += len(word)
            else:
                ret = self._format(line, maxWidth)
                ans.append(ret)
                line = [word]
                length = len(word)

        ret = self._format_last(line, maxWidth)
        ans.append(ret)
        return ans

    def _format(self, line, width):
        if len(line) == 1:
            string = line[0] + " " * (width - len(line[0]))
            return string

        gaps = len(line) - 1
        t = sum(map(len, line))
        width = width - t
        spaces = width // gaps
        left = width % gaps
        ret = ""

        for i, word in enumerate(line):
            if i == len(line) - 1:
                ret = ret + word
                continue

            ret = ret + word + " " * spaces
            if left:
                ret += " "
                left -= 1

        return ret

    def _format_last(self, line, width):
        ret = ""
        for i, word in enumerate(line):
            if i == len(line) - 1:
                ret = ret + word
                continue
            ret = ret + word + " "

        left = width - len(ret)
        ret = ret + " " * left
        return ret


words = ["This", "is", "an", "example", "of", "text", "justification."]
# words = ["This", "is", "an", "examp", "of", "text"]
words = ["What","must","be","acknowledgment","shall","be"]
maxWidth = 16
a = Solution2().fullJustify(words, maxWidth)
for r in a:
    print("[" + r + "]")


