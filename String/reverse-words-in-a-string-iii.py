class Solution(object):
    def reverseWords(self, s):
        """
        :type s: str
        :rtype: str
        """
        if not s or s == " ":
            return s
        s = str(s)
        l = 0
        r = 0
        ans = ""
        for i, c in enumerate(s):
            if c == " ":
                r = i
                ans += s[l:r][::-1] + " "
                l = i + 1

        ans += s[l:][::-1]

        print(ans)
        return ans

s = "Let's take LeetCode contest"
Solution().reverseWords(s)
