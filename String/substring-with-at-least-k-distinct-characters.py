import collections
class Solution:
    """
    @param s: a string
    @param k: an integer
    @return: the number of substrings there are that contain at least k distinct characters
    """
    def __init__(self):
        self.ans = 0
    
    
    def kDistinctCharacters(self, s, k):
        # Write your code here
        
        start = 0
        for left in range(len(s)):
            if left + k > len(s):
                break
            mp = collections.Counter()
            for right in range(max(left, start), len(s)):
                mp[s[right]] += 1
                while len(mp) == k:
                    self.ans = self.ans + len(s) - right
                    mp[s[left]] -= 1
                    if mp[s[left]] == 0:
                        del mp[s[left]]
                    start = right+1
                    
                
        return self.ans

s = "abcabcabcabc" 
k = 3
# s = "abcabcabca"
# k = 4



a = Solution().kDistinctCharacters(s, k)
print("ans:", a)

mp = collections.Counter()