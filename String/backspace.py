class Solution:
    def backspace(self, A, B):
        i = len(A) - 1
        j = len(B) - 1
        
        while i >= 0 and j >= 0:
            cnt = 0
            while A[i] == '\\b':
                i -= 1
                cnt += 1

            if cnt:
                i -= cnt

            if A[i] != B[j]:
                return False

            i -= 1
            j -= 1 

        return True



sol = Solution()
input = ['a','b','\\b','d','c']
output = ['a','d','c']
print(sol.backspace(input, output) == True) 

input = ['a','b','\\b','\\b','\\b','d','c']
output = ['d','c']
print(sol.backspace(input, output) == True)
 
input = ['a','b','d','\\b']
output = ['a','d']
print(sol.backspace(input, output) == False)
