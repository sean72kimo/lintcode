#  我的靈感來自 score-of-parentheses 計算bal
import collections

# time n
class Solution:
    def removeOuterParentheses(self, s: str) -> str:
        ans = []
        opened = 0

        for ch in s:
            if ch == "(" and opened > 0:
                ans.append(ch)

            elif ch == ")" and opened > 1:
                ans.append(ch)

            if ch == "(":
                opened += 1
            else:
                opened -= 1

        return "".join(ans)

# time n^2
class Solution2:

    def removeOuterParentheses(self, s: str) -> str:

        bal = 0
        j = 0
        ans = ""
        for i in range(len(s)):

            if s[i] == '(':
                bal += 1
            else:
                bal -= 1

            if bal == 0:
                sub = s[j + 1:i]
                ans += sub
                j = i + 1
        return ans
s = "abc"
print("    ".isspace())
print('d' not in s)

