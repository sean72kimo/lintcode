class Solution:
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        paths = path.split('/')

        tmp = []
        for p in paths:
            if p == '.' or p == '':
                continue
            tmp.append(p)

        ans = []
        for p in tmp:
            if p == '..':
                if  len(ans):
                    ans.pop()
                continue
            ans.append(p)

        return '/' + '/'.join(ans)



# path = "/home//foo/"
# path = "/a/./b/../../c/"
# path = "/home/"
# path = "/home//foo/"
path = "/../"
a = Solution().simplifyPath(path)
print("ans:", a)


string = 'axcb'
print(sorted(string))