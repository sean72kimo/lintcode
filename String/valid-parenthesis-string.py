
"""
case left > 0: 也許是多餘的( or maybe they are just *
所以我們從右到左再掃一遍，把所有的 * 當作右括號
如果多餘的left數量是 ( 造成的，那麼從右往左掃，應該會抵銷這些左括號
如果還是有多餘的數量right, 表示是*所造成的，那麼便可以反回True
從右往左掃的情況，會有右括號多餘的現象嗎？此情況在左從右掃的時候已經排除了
"""


class Solution_greedy(object):
    def checkValidString(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if len(s) == 0:
            return True

        left = 0
        right = 0
        n = len(s)

        # treat all * as (
        for ch in s:
            if ch == '(' or ch == '*':
                left += 1
            else:
                left -= 1
            if left < 0:
                return False

        if left == 0:
            return True  # we used all * as ( and got balanced 左括號＋星 的數量 = 右括號的數量

        # case left > 0: 也許是多餘的( or maybe they are just *
        # 所以我們從右到左再掃一遍，把所有的 * 當作右括號
        # 如果多餘的left數量是 ( 造成的，那麼從右往左掃，應該會抵銷這些左括號
        # 如果還是有多餘的數量right, 表示是*所造成的，那麼便可以反回True
        # 從右往左掃的情況，會有右括號多餘的現象嗎？此情況在左從右掃的時候已經排除了

        for ch in s[::-1]:
            if ch == ')' or ch == '*':
                right += 1
            else:
                right -= 1
            if right < 0:
                return False

        return True
"""
DP, 區間

"""
class Solution_dp_WA(object):
    def checkValidString(self, s):
        if len(s) == 0:
            return True
        n = len(s)
        f = [[False for _ in range(n)] for _ in range(n)]
        LEFT = '(*'
        RIGHT = ')*'
        for i in range(n):
            if s[i] == '*':
                f[i][i] = True
            if i+1 > n-1:
                break
            if s[i] in LEFT and s[i+1] in RIGHT:
                f[i][i+1] = True

        for size in range(3, n+1):
            for i in range(n):
                j = i + size - 1
                if j >= n:
                    break


                if s[i] in LEFT:
                    for k in range(i+1, j+1):
                        if (s[k] in RIGHT) and ((k==i+1 or f[i+1][k-1]) and (k==j or f[k+1][j+1])):
                            f[i][j+1] = True
                elif s[i] == '*' and f[i+1][j-1]:
                    f[i][j] = True

        return f[0][n-1]
s = ''
a = Solution_dp().checkValidString(s)



