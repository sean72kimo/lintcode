class Solution(object):
    def convertToBase7(self, num):
        """
        :type num: int
        :rtype: str
        """
        ans = []
        neg = False
        if num < 0:
            num = -num
            neg = True
        while num:
            rem = num % 7
            ans.append(str(rem))
            num = num // 7

        if neg:
            return '-' + ''.join(ans[::-1])
        else:
            ''.join(ans[::-1])

a = Solution().convertToBase7(-7)
print(a)
