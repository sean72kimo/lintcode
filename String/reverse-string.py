class Solution(object):
    def reverseString(self, s):
        """
        :type s: str
        :rtype: str
        """
        if s is None or len(s) == 0:
            return ""

        n = len(s)

        i = 0
        j = n - 1

        while i < n:
            s[i], s[j] = s[j], s[i]

        return s

a = Solution().reverseString(['h', 'e', 'l'])
print(a)
