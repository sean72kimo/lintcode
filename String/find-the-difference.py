class Solution(object):
    def findTheDifference(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        n = len(s)
        m = len(t)
        c = 0
        for i in range(n):
            c = c ^ ord(s[i])

        for i in range(m):
            c = c ^ ord(t[i])

        return chr(c)
s = 'abcd'
t = 'abcde'
a = Solution().findTheDifference(s, t)
print('ans:', a)
