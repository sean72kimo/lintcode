import collections
class Solution(object):
    def mostCommonWord(self, paragraph, banned):
        """
        :type paragraph: str
        :type banned: List[str]
        :rtype: str
        """
        banned = set(banned)
        lst = paragraph.split(" ")
        temp = []
        for itm in lst:
            w = itm.strip("!?',;.").lower()
            if w not in banned:
                temp.append(w)
        
        counter = collections.Counter(temp)
        print(counter)
        max_cnt = float('-inf')
        ans = ""
        for word, cnt in counter.items():
            if cnt > max_cnt:
                ans = word
                max_cnt = cnt
        return ans
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
banned = ["hit"]
a = Solution().mostCommonWord(paragraph, banned)
print("ans:", a)