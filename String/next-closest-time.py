class Solution(object):
    def nextClosestTime(self, time):
        """
        :type time: str
        :rtype: str
        """

        tmp = []
        for c in time:
            if c.isdigit():
               tmp.append(c)

        ti = time.split(":")
        hh = int(ti[0])
        mm = int(ti[1])
        print(hh, mm)

        while True:
            mm += 1


            if mm == 60:
                mm = 0
                hh += 1

            if hh == 24:
                hh = 0

            h_str = "{0:02d}".format(hh)
            m_str = "{0:02d}".format(mm)

            now = []
            now.extend(h_str)
            now.extend(m_str)

            def reuse(now, tmp):
                for i in now:
                    if i not in tmp:
                        return False
                return True

            if reuse(now, tmp):
                rt = now[0] + now[1] + ":" + now[2] + now[3]
                return rt



if __name__ == "__main__":
    time = "23:59"
    a = Solution().nextClosestTime(time)
    print("ans:", a)

    a = 11
    aa = "{0:02d}".format(a)





