class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        s_to_t = {}
        for i, ch in enumerate(s):
            if ch in s_to_t and s_to_t[ch] != t[i]:
                print(".")
                return False
            s_to_t[ch] = t[i]

        t_to_s = {}
        for i, ch in enumerate(t):
            if ch in t_to_s and t_to_s[ch] != s[i]:
                print("/")
                return False

            t_to_s[ch] = s[i]

        return True

class Solution2(object):
    def isIsomorphic1(self, s, t):
        d1, d2 = {}, {}
        for i, val in enumerate(s):
            d1[val] = d1.get(val, []) + [i]
        for i, val in enumerate(t):
            d2[val] = d2.get(val, []) + [i]


        return sorted(d1.values()) == sorted(d2.values())

s = "egg"

t = "add"
a = Solution().isIsomorphic(s, t)
print("ans:", a)
