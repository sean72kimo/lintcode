class Solution:
    def lengthLongestPath(self, paths: str) -> int:
        maxlen = 0
        pathlen = {0: 0}

        for line in paths.splitlines():
            name = line.strip('\t') # len of \t is 1
            depth = len(line) - len(name)

            if '.' in name:
                maxlen = max(maxlen, pathlen[depth] + len(name))
            else:
                pathlen[depth + 1] = pathlen[depth] + len(name) + 1
                # 最後的+1代表每個folder結束時，最後面trailing /
        return maxlen

paths = "dir\n\tsubdir2\n\t\tfile.ext"
Solution().lengthLongestPath(paths)
"""
monotonic stack
"""
class Solution:
    def lengthLongestPath(self, paths: str) -> int:
        paths = paths.split('\n')
        ans = 0
        stack = []
        for itm in paths:
            lv = itm.count('\t')

            while stack and lv <= stack[-1][1]:
                stack.pop()

            stack.append((itm, lv))

            if '.' in itm:
                size = self.get_len(stack)
                ans = max(size, ans)
        return ans

    def get_len(self, stack):
        tmp = []
        for path, _ in stack:
            p = path.lstrip('\t')
            tmp.append(p)

        res = '/'.join(tmp)
        return len(res)