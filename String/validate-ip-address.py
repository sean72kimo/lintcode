class Solution(object):

    def isIPv4(self, string):
        lst = string.split('.')

        try:
            for ip in lst:
                if (ip[0] == '0' and len(ip) > 1) or ip[0] == '-':
                    return False
                if not (0 <= int(ip) < 256):
                    return False
            return True
        except:
            return False

    def isIPv6(self, string):
        lst = string.split(':')
        try:
            for ip in lst:
                if len(ip) > 4 or len(ip) == 0 or ip[0] == '-':
                    return False
                if not(int(ip, 16) >= 0):
                    return False
            return True
        except:
            return False

    def validIPAddress(self, IP):
        """
        :type IP: str
        :rtype: str
        """

        if IP.count('.') == 3 and self.isIPv4(IP):
            return "IPv4"

        if IP.count(':') == 7 and self.isIPv6(IP):
            return "IPv6"

        return "Neither"


string = "0.0.0.-0"
a = Solution().validIPAddress(string)
print("ans:", a)


class Solution2(object):

    def validIPAddress(self, IP):
        """
        :type IP: str
        :rtype: str
        """
        if len(IP) == 0:
            return "Neither"
        ipv4 = False
        hex_str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'}
        num_str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
        tmp = IP.split('.')
        if len(tmp) > 1:
            ipv4 = True

        if ipv4:

            for itm in tmp:
                if itm == '':
                    return 'Neither'
                if itm[0] == 0 and len(itm) > 1:  # leading zero
                    return 'Neither'

                for ch in itm:
                    if ch not in num_str:
                        return 'Neither'

                if int(itm) > 255 or int(itm) < 0:
                    return 'Neither'
            return 'IPv4'

        else:
            tmp = IP.split(':')
            for itm in tmp:
                if itm == '':
                    return 'Neither'
                if len(itm) > 4:
                    return 'Neither'
                for ch in itm:
                    if ch not in hex_str:
                        return 'Neither'

            return 'IPv6'
