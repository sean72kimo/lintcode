class Solution(object):
    def findRepeatedDnaSequences(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        if len(s) == 0:
            return s
        seen = set()
        ans = []

        for i in range(0, len(s)):
            if i + 10 > len(s):
                break

            ten = s[i:i + 10]
            if ten in seen:
                ans.append(ten)

            seen.add(ten)

        return ans


class Solution2:
    def findRepeatedDnaSequences(self, s: str):
        if len(s) == 0:
            return []

        seen = set()
        ans = []

        for i in range(0, len(s)):
            if i + 10 > len(s):
                break
            sub = s[i:i + 10]
            if sub in seen:
                ans.append(sub)
                print(ans)

            seen.add(sub)

        return ans


s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
answer = Solution2().findRepeatedDnaSequences(s)
print('ans:', answer)
