class Codec:
    """
    encode部分，若是碰到 ':' 則用 '::' 代替，若是string結尾，則用':;'
    """
    def encode(self, strs):
        """Encodes a list of strings to a single string.
        
        :type strs: List[str]
        :rtype: str
        """
        if strs is None or len(strs) == 0:
            return ""

        string = ""
        for each in strs:
            for ch in each:

                string += ch
                
            string += ":;"
            
        return string
            

    def decode(self, s):
        """Decodes a single string to a list of strings.
        
        :type s: str
        :rtype: List[str]
        """
        if s is None or len(s) == 0:
            return []
        
        path = ""
        ans = []
        i = 0
        while i < len(s):
            if s[i : i+2] == ":;":
                ans.append(path)
                path = ""
                i += 2
                continue

            path += s[i]
            i += 1
        return ans
# Your Codec object will be instantiated and called as such:
codec = Codec()
strs = ["lint","code","love","you"]
strs = ["(}", "{:y5"]
strs = ["tP", "8f_f@^", "w{=dT(0@:", ""]
string = codec.encode(strs) 
decoded = codec.decode(string)
print(string, decoded == strs)
print(decoded)


