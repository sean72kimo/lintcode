def isPali(s):
    return s[:] == s[::-1]

class Solution(object):
    def longestPalindrome(self, s):
        if not any(s):
            return ""

        res = s[0]
        for i in range(len(s)):
            # case aba
            tmp = self.helper(s, i, i)
            if len(tmp) >= len(res):
                res = tmp

            # case bb
            tmp = self.helper(s, i, i + 1)
            if len(tmp) > len(res):
                res = tmp
        return res

    def helper(self, s, l, r):

        while l >= 0 and r < len(s) and s[l] == s[r]:
            l -= 1
            r += 1

        return s[l + 1:r]



# Time Limit Exceeded
class Solution2(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        i = 0
        self.n = len(s)
        j = len(s) - 1
        longest = 0
        self.ans = None

        self.dfs3(s, 0, 0, longest)
        return self.ans


    def dfs3(self, s, startIndex, level, longest):

        if startIndex == len(s):
            return

        for i in range(startIndex, len(s)):
            substring = s[startIndex : i + 1]

            if isPali(substring):
                if not self.ans:
                    self.ans = substring
                else:
                    if len(substring) > len(self.ans):
                        self.ans = substring
                        longest = len(substring)

            self.dfs3(s, i + 1, level + 1, longest)




s = "abcd"
s = "sean72"
s = "bb"
# s = "babaddtattarrattatddetartrateedredividerb"

print('ans:', Solution().longestPalindrome(s))

