class Solution(object):
    def __init__(self):
        self.count = 0
        self.sub = [] #record all possible substring
        
    def countSubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        for i in range(len(s)):
            self.countPalin(s, i, i)
            self.countPalin(s, i, i+1)
        print(self.sub)
        return self.count


    def countPalin(self, s, left, right):
        while left >= 0 and right <= len(s)-1 and s[left] == s[right]:
            self.count += 1
            self.sub.append(s[left : right+1])
            left -= 1
            right += 1
            
s = "abc"
s = "aaa"
a = Solution().countSubstrings(s)
print("ans:", a)