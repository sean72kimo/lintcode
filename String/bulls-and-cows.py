import collections


class Solution(object):
    def getHint(self, secret, guess):
        """
        :type secret: str
        :type guess: str
        :rtype: str
        """
        mp1 = collections.defaultdict(set)
        mp2 = collections.defaultdict(set)
        for i, ch in enumerate(secret):
            mp1[ch].add(i)

        for i, ch in enumerate(guess):
            mp2[ch].add(i)

        a = 0
        b = 0

        for i, ch in enumerate(guess):
            if ch in mp1 and i in mp1[ch]:
                a += 1
                continue

            if ch in mp1:
                b += 1
                continue

        return str(a) + "A" + str(b) + "B"

secret = "1807"
guess = "7810"

secret = "11"
guess = "10"
expexted = "1A0B"
a = Solution().getHint(secret, guess)
print("ans:", a==expexted, a)