import string
class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        idx = []
        for c in string.ascii_lowercase:
            if s.count(c) == 1:
                idx.append(s.find(c))
        if len(idx):
            return min(idx)
        else:
            return -1
s = 'leetcode'
ans = Solution().firstUniqChar(s)
print('ans:', ans)

a = 8
b = format(a, 'b')
print(b)
