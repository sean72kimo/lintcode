class Solution(object):
    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """
        res = []
        if input.isdigit():
            return [int(input)]


        for i in range(len(input)):
            if input[i] in "+-*":
                left = self.diffWaysToCompute(input[:i])
                right = self.diffWaysToCompute(input[i + 1:])
                print(left, right)
                res.append(self.operate(left[0], right[0], input[i]))
        return res

    def operate(self, x, y, op):
        if op == "+":
            return x + y
        elif op == "-":
            return x - y
        else:
            return x * y

# similar to above, but use pointers as boundary
class Solution(object):
    def diffWaysToCompute(self, s):
        """
        :type input: str
        :rtype: List[int]
        """
        self.oper = ['+', '-', '*']
        self.mem = {}
        return self.dfs(s, 0, len(s) - 1)

    def dfs(self, s, i, j):
        print(i, j)
        if i > j:
            return []

        if s[i:j + 1].isdigit():
            return [int(s[i:j + 1])]
        res = []
        for k in range(i, j + 1):
            if s[k] in ['+', '-', '*']:
                left = self.dfs(s, i, k-1)
                right = self.dfs(s, k + 1, j)

                for l in left:
                    for r in right:
                        val = self.calc(l, r, s[k])
                        res.append(val)
        return res

    def calc(self, x, y, op):
        if op == '+':
            return x + y
        elif op == '-':
            return x - y
        else:
            return x * y


input = "2*3-4*5"
print('ans:', Solution().diffWaysToCompute(input))

