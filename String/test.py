import collections
class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """

        if len(s)==0 or len(t)==0:
            return ""

        left=0
        right=0

        mp = collections.Counter(t)
        setT = set(t)
        count = 0
        for w, c in mp.items():
            count += c

        ans=""
        # S = "ADOBECODEBANC"
        # T = "ABC"
        for right, ch in enumerate(s):
            if ch in mp:
                mp[ch] -= 1
                if mp[ch] == 0:
                    mp.pop(ch)
                
            while len(mp) == 0:
                if ans == "":
                    ans = s[left : right + 1]
                else:
                    if right - left + 1 < len(ans):
                        ans = s[left : right + 1]
                    
                if s[left] in t:
                    mp[s[left]] += 1
                    if mp[s[left]] > 0:
                        count += 1
                print(s[left:right+1])
                left += 1
        return ans
        
        
        
        
s = "ADOBECODEBANC"
t = "ABC"
a = Solution().minWindow(s, t)
print("ans:", a)
