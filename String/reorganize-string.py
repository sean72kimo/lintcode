import collections, heapq


class Solution:

    def reorganizeString(self, s):
        """
        :type S: str
        :rtype: str
        """
        res = ""
        pq = []  # maxheap
        c = collections.Counter(s)

        h = [(-freq, ch) for (ch, freq) in collections.Counter(s).items()]
        heapq.heapify(h)
        res = []

        while h:
            q = []

            for _ in range(2):
                if len(res) == len(s):
                    return ''.join(res)
                if not h:
                    return ''

                freq, ch = heapq.heappop(h)
                res.append(ch)
                if freq + 1 < 0:
                    q.append((freq + 1, ch))

            while q:
                heapq.heappush(h, q.pop())

        return ''.join(res)


s = "aaab"
s = "aaabbbcccdde"
a = Solution().reorganizeString(s)
print("ans:", a)
