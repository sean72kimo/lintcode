class Solution:
    """
    @param: s: Roman representation
    @return: an integer
    """
    def romanToInt(self, s):
        # write your code here
        ROMAN = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000
        }

        if s is None or len(s) == 0:
            return 0

        ans = ROMAN[s[0]]


        for i in range(1, len(s)):
            integer = ROMAN[s[i]]
            if ROMAN[s[i - 1]] >= ROMAN[s[i]] :
                ans += integer
            else:

                ans = ans - ROMAN[s[i - 1]]
                ans = ans + ROMAN[s[i]] - ROMAN[s[i - 1]]

        return ans

print('ans', Solution().romanToInt('CDXXI'))
