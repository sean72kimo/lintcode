class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        maxint = (2 ** 31) - 1
        minint = -(maxint + 1)

        neg = False
        if x < 0:
            neg = True

        val = int(str(abs(x))[::-1]) if not neg else -int(str(abs(x))[::-1])

        if val > maxint or val < minint:
            return 0

        return val


    def reverse2(self, x):
        maxint = (2 ** 31) - 1
        minint = -(maxint + 1)


        m = -1 if x < 0 else 1
        x = x * m

        n = 0
        while x > 0:
            print(n, x)
            n = (n * 10) + (x % 10)
            print(n, x)
            print("....")
            x = x // 10

        if n > maxint:
            return 0

        return n * m

a = Solution().reverse2(123)
print(a)
