import collections
from typing import List


class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        if len(chars) == 0:
            return 0
        char_cnt = collections.Counter(chars)
        ans = 0
        for word in words:

            word_cnt = collections.Counter(word)

            for i, ch in enumerate(word):
                if ch not in char_cnt:
                    break
                if word_cnt[ch] > char_cnt[ch]:
                    break

                # 在最後一個字符的時候計算長度
                if i == len(word) - 1:
                    ans += len(word)
            # 如果在for 外面計算長度，那麼for loop裡面的那些不符條件的break，都會被計算至答案，這是不正確的。

        return ans
