class Solution:
    """
    @param: s: a string
    @param: p: a string
    @return: a list of index
    """
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """
        if not s or not p:
            return []

        m = len(p)
        n = len(s)
        hashp = {}
        hashs = {}
        ans = []

        if m > n:
            return []

        for i in range(m):
            hashp[p[i]] = hashp.get(p[i], 0) + 1

        for i in range(m):
            hashs[s[i]] = hashs.get(s[i], 0) + 1

        if hashp == hashs:
            ans.append(0)

        for i in range(1, n):
            if i + m - 1 > n - 1:
                break

            hashs[s[i + m - 1]] = hashs.get(s[i + m - 1], 0) + 1
            hashs[s[i - 1]] = hashs[s[i - 1]] - 1

            if hashs[s[i - 1]] == 0:
                del hashs[s[i - 1]]

            if hashs == hashp:
                ans.append(i)


        return ans

    def findAnagrams2(self, s, p):
        if len(s) < len(p):
            return []
        p_len = len(p)
        p_hash = {}
        t_hash = {}
        ans = []
        for c in p:
            p_hash[c] = 1 if c not in p_hash else p_hash[c] + 1

        for i in range(len(p)):
            c = s[i]
            t_hash[c] = 1 if c not in t_hash else t_hash[c] + 1

        if t_hash == p_hash:
            ans.append(0)

        # print('[init]', t_hash)
        for i in range(1, len(s)):
            end = i + len(p)
            if end > len(s):
                break


            add_c = s[i + len(p) - 1]
            remove_c = s[i - 1]
            # print(i, add_c, remove_c)

            t_hash[remove_c] -= 1
            if t_hash[remove_c] == 0:
                del t_hash[remove_c]
            t_hash[add_c] = 1 if add_c not in t_hash else t_hash[add_c] + 1

            # print('[curr]', t_hash)

            if t_hash == p_hash:
                ans.append(i)
                continue

            if t_hash == p_hash:
                ans.append(i)

        return ans



s = "abab"
p = "ab"
####
# s = "cbaebabacd"
# p = "abc"

print('ans:', Solution().findAnagrams(s, p))

