class Solution:
    def minInsertions(self, s: str) -> int:

        s = s.replace('))', ']')
        open_bracket_count = 0
        count = 0
        for ch in s:
            if ch == '(':
                open_bracket_count += 1
                continue

            else:  # ch == ')' or ']':

                if ch == ')':
                    count += 1

                if open_bracket_count:
                    open_bracket_count -= 1
                else:
                    count += 1
        return count + open_bracket_count * 2