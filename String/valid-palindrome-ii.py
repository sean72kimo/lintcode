class Solution:
    def validPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        i = 0
        j = len(s) - 1

        skip = False

        while i < j:
            if s[i] == s[j]:
                i += 1
                j -= 1
                continue
            else:
                return s[i+1:j+1] == s[i+1:j+1][::-1] or s[i:j] == s[i:j][::-1] 

        return True

# s = "lcuucul"
# a = Solution().validPalindrome(s)
# print("ans:", a)


class Solution_Wrong_Answer:
    def validPalindrome(self, s: str) -> bool:
        i = 0
        j = len(s) - 1
        c = 0
        while i < j:
            if s[i] == s[j]:
                i += 1
                j -= 1
                continue

            if s[i] != s[j] and c > 0:
                return False

            if s[i] != s[j]:
                print(i,j)
                if s[i + 1] == s[j]:

                    i += 1
                    c += 1
                    print("i move", s[i] == s[j])
                    continue
                elif s[i] == s[j - 1]:

                    j -= 1
                    c += 1
                    print("j move", s[i] == s[j])
                    continue
                else:
                    print("false")
                    return False

        return True

s = "lcuucul"
#    0123456
a = Solution2().validPalindrome(s)
print("ans:", a)



