class Solution(object):
    def longestCommonPrefix(self, strs):
            """
            :type strs: List[str]
            :rtype: str
            """
            if not strs:
                return ""
            shortest = min(strs, key = len)
            print(shortest)

            for i, ch in enumerate(shortest):
                for other in strs:
                    if other[i] != ch:
                        return shortest[:i]
            return shortest


strs = ["ABCDEFG", "ABCEFG" , "ABCEFA"]
a = Solution().longestCommonPrefix(strs)
print('ans:', a)
