from typing import List


class Solution:
    def findMissingRanges(self, nums: List[int], lower: int, upper: int) -> List[str]:
        if len(nums) == 0:
            return [self.create_range(lower - 1, upper + 1)]

        ans = []

        if nums[0] != lower:
            rt = self.create_range(lower - 1, nums[0])
            if len(rt):
                ans.append(rt)

        for i in range(len(nums) - 1):
            rt = self.create_range(nums[i], nums[i + 1])
            if len(rt):
                ans.append(rt)

        if nums[-1] != upper:
            rt = self.create_range(nums[-1], upper + 1)
            if len(rt):
                ans.append(rt)

        return ans

    def create_range(self, lo, hi):
        diff = hi - lo
        if diff <= 1:
            return ""

        if diff == 2:
            return str(lo + 1)

        return str(lo + 1) + "->" + str(hi - 1)

nums = [0,1,2,3,50,75,76]
lower = 0
upper = 99
a = Solution().findMissingRanges(nums, lower, upper)
print("ans:", a)