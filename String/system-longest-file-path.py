class Solution:
    """
    /system-longest-file-path
    @param: input: an abstract file system
    @return: return the length of the longest absolute path to file
    """
    def lengthLongestPath(self, input):
        # write your code here
        if input is None or len(input) == 0:
            return 0

        len_for_each_level = [0 for i in range(len(input))]
        ans = 0
        tab_len = len('\t')
        for line in input.split('\n'):
            tab_count = line.rfind('\t') + 1
            level = tab_count
            if tab_count == 0:
                line = line + '/'
            else:
                line = line[tab_count:] + '/'


            len_for_each_level[level] = len(line)

            if '.' in line:

                # remove trailing slash length
                total_path_len = sum(len_for_each_level[:level + 1]) - 1
                ans = max(ans, total_path_len)

        return ans



input = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"
input = "skd\n\talskjv\n\t\tlskjf\n\t\t\tklsj.slkj\n\t\tsdlfkj.sdlkjf\n\t\tslkdjf.sdfkj\n\tsldkjf\n\t\tlskdjf\n\t\t\tslkdjf.sldkjf\n\t\t\tslkjf\n\t\t\tsfdklj\n\t\t\tlskjdflk.sdkflj\n\t\t\tsdlkjfl\n\t\t\t\tlskdjf\n\t\t\t\t\tlskdjf.sdlkfj\n\t\t\t\t\tlsdkjf\n\t\t\t\t\t\tsldkfjl.sdlfkj\n\t\t\t\tsldfjlkjd\n\t\t\tsdlfjlk\n\t\t\tlsdkjf\n\t\tlsdkjfl\n\tskdjfl\n\t\tsladkfjlj\n\t\tlskjdflkjsdlfjsldjfljslkjlkjslkjslfjlskjgldfjlkfdjbljdbkjdlkjkasljfklasjdfkljaklwejrkljewkljfslkjflksjfvsafjlgjfljgklsdf.a"
input = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
print('ans:', Solution().lengthLongestPath(input))

"""
dir         3
    subdir1
    subdir2 8
        file.ext 10
"""


