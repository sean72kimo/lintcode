class Solution(object):

    def findLongestWord(self, s, d):
        """
        :type s: str
        :type d: List[str]
        :rtype: str
        排序。長的往前放，若長度一樣，按照字典序 注意sort key
        窮舉每一個word in d, 每次都和s單字做比較，i從word的第一個字符開始。
        如果word[i] == ch in s, 代表匹配, 指針向前，看看下一個word[i]是否匹配下一個ch in s
        如果word[i] != ch, 則繼續嘗試下一個ch (意味著當前的ch被刪除了)
        """

        def mykey(x):
            return (-len(x), x)

        d.sort(key=mykey)
        for word in d:
            i = 0
            for ch in s:
                if i < len(word) and word[i] == ch:
                    i += 1
                else:
                    continue  # this is like del the ch in s
            if i == len(word):
                return word
        return ""

