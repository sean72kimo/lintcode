class Solution(object):
    def reverseWords(self, str):
        """
        :type str: List[str]
        :rtype: void Do not return anything, modify str in-place instead.
        """
        def myreverse(string, s, e):
            while s < e:
                string[s], string[e] = string[e], string[s]
                s += 1
                e -= 1

        str.reverse()

        start = 0
        for i, c in enumerate(str):
            if str[i] == " ":
                myreverse(str, start, i - 1)
                start = i + 1
            if i == len(str) - 1:
                myreverse(str, start, i)



str = ["t", "h", "e", " ", "s", "k", "y", " ", "i", "s", " ", "b", "l", "u", "e"]
Solution().reverseWords(str)
