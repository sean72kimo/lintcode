class Solution_test(object):
    def isNumber(self, s):
        """
        :type s: str
        :rtype: bool
        """
        s = s.strip(" ") + " "
        i = 0
        size = len(s) - 1

        if s[i] == '+' or s[i] == '-':
            i += 1

        if i == size:
            # "+"
            return False

        # "+1"
        digit = 0
        point = 0
        while s[i].isdigit() or s[i] == '.':
            if s[i].isdigit():
                digit += 1

            if s[i] == '.':
                point += 1
            i += 1

        if digit <= 0 or point > 1:
            return False

        if s[i] == 'e':
            i += 1
            if s[i] == "+" or s[i] == "-":
                i += 1
            # "+123e- "
            if i == size:
                return False
            # "+123e-678 "
            while s[i].isdigit():
                i += 1
        return i == size

s = "3"
s = "1 "
s = "2e0"
s = "+12a"
s = "..2"
a = Solution_test().isNumber(s)
print("ans:", a)

class Solution_correct(object):
    def isNumber(self, s):
        """
        :type s: str
        :rtype: bool
        """

        i = 0

        # remove all white spaces at front and tail
        s = s.strip(" ") + " "
        size = len(s) - 1
        print("{}.".format(s))
        if len(s) == 0:
            return False

        # skip sign
        if s[i] == '+' or s[i] == '-':
            i += 1

        nDigit = 0
        nPoint = 0
        while s[i].isdigit() or s[i] == '.':
            if s[i].isdigit():
                nDigit += 1
            if s[i] == '.':
                nPoint += 1

            i += 1

        if nDigit <= 0 or nPoint > 1:
            return False
        print("idx:", i, size)

        if s[i] == 'e':
            i += 1
            if s[i] == '+' or s[i] == '-':
                i += 1
            if i == size:
                return False

            while i < size:
                if not s[i].isdigit():
                    return False
                i += 1

        return i == size

# s = "3"
# s = "1 "
# s = "2e0"
# s = "+12a"
# a = Solution_correct().isNumber(s)
# print("ans:", a)






