class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        l1 = len(num1)
        l2 = len(num2)
        if l1 == 0 or l2 == 0:
            return

        num1 = num1[::-1]
        num2 = num2[::-1]
        ans = [0 for i in range(l1 + l2 + 1)]

        def toint(s):
            return ord(s) - ord('0')


        for i in range(l1):
            for j in range(l2):
                ans[i + j] += toint(num1[i]) * toint(num2[j])

        print(ans)
        carry = 0
        for i in range(len(ans)):
            ans[i] = carry + ans[i]
            carry = ans[i] // 10
            ans[i] = str(ans[i] % 10)

        string = ""
        for i in range(len(ans)):
            string = str(ans[i]) + string

        if string == '0' * len(string):
            return '0'

        return string.lstrip('0')
num1 = '123'
num2 = '456'
ans = Solution().multiply(num1, num2)
print('ans:', ans)
