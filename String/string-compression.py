from typing import List


class Solution:
    def compress(self, chars: List[str]) -> int:
        """
        https://leetcode.com/problems/string-compression/submissions/
        """
        cur = None
        cnt = 0
        idx = 0
        i = 0
        while i < len(chars):
            if i == 0:
                cur = chars[i]
                cnt = 1
                i += 1
                continue

            if cur == chars[i]:
                cnt += 1
                i += 1
                continue

            if cnt == 1:
                chars[idx] = cur
                idx += 1
                cur = chars[i]
                i += 1
            else:
                nxt = chars[i]
                idx = self.fill(chars, cur, cnt, idx)
                cur = nxt
                cnt = 1
                i += 1

        if cnt != 1:
            idx = self.fill(chars, cur, cnt, idx)
        else:
            chars[idx] = cur
            idx += 1

        return idx

    def fill(self, chars, cur, cnt, idx):
        print(cur, cnt, idx, len(chars), chars)
        cnt_str = str(cnt)
        string = cur + cnt_str

        for i in range(len(string)):
            chars[idx] = string[i]
            idx += 1
        return idx

string = "aaabbbccc"
# string = "aaaaaaaaaaaa"
# string = "aa"
# string = "a"
# string = "xaaaaaaaaaaaa"
# string = "aaaaab"
# string = "abc"
# string = "aaaaab"
# string = "aaaaba"
chars = list(string)

idx = Solution().compress(chars)
print(chars[:idx])

class Solution(object): #不知道這是哪一題, 貼到LC 443錯誤
    def compress(self, s):
        """
        :type chars: List[str]
        :rtype: int
        """
        if len(s) == 0:
            return 0
        s.append(' ')
        i = j = 0
        cnt = 0
        curr = s[0]

        while i < len(s):
            if s[i] != curr or s[i] == '_':
                prev = curr
                curr = s[i]
                j = self.fill(s, cnt, j, prev)
                cnt = 0

            cnt += 1
            i += 1
        return j

    def fill(self, s, cnt, j, ch):
        s[j] = ch
        if cnt == 1:
            return j+1

        j += 1
        lo = j
        while j < len(s) and cnt:
            print(cnt)
            d = cnt % 10
            s[j] = str(d)
            cnt = cnt // 10
            j += 1
        hi = j - 1

        while lo < hi:
            s[lo], s[hi] = s[hi], s[lo]
            lo += 1
            hi -= 1
        return j



string = "aaabbbccc"
string = "aaaaaaaaaaaa"
# string = "aa"
# string = "a"
string = "xaaaaaaaaaaaa"
string = "aaaaab"
# string = "abc"
# string = "aaaaab"
# string = "aaaaba"
chars = list(string)
chars = ["$","$","$","#","#","#","#","#","$","$"]
# idx = Solution().compress(chars)

# print(chars[:idx])
class Solution_leetcode:
    def fill(self, chars, curr, cnt, idx):
        cnt_str = str(cnt)
        chars[idx] = curr
        idx += 1
        for j in range(len(cnt_str)):
            chars[idx] = cnt_str[j]
            idx += 1
        return idx
    def compress(self, chars):
        """
        :type chars: List[str]
        :rtype: int
        """

        if len(chars) <= 1:
            return

        cnt = 0
        idx = 0
        i = 0

        while i < len(chars):
            if i == 0:
                cnt = 1
                curr = chars[0]
                i += 1
                continue

            if chars[i] == curr:
                cnt += 1
            else :
                if cnt == 1:
                    chars[idx] = curr
                    idx += 1
                    curr = chars[i]
                else:
                    nxt = chars[i]

                    idx = self.fill(chars, curr, cnt, idx)

                    curr = nxt
                    cnt = 1
                    print(curr, idx, i)
            i += 1

        # last
        if cnt != 1:
            print(curr, cnt)
            cnt_str = str(cnt)
            chars[idx] = curr
            idx += 1
            for j in range(len(cnt_str)):
                chars[idx] = cnt_str[j]
                idx += 1
        else:
            chars[idx] = curr
            idx += 1
        return idx



string = "aaabbbccc"
# string = "aa"
# string = "a"
# string = "abbbbbbbbbbbb"
# string = "abc"
# string = "aaaaab"
# string = "aaaaba"
chars = list(string)

# idx = Solution_leetcode().compress(chars)
# print(chars[:idx])

class Solution_lintcode:
    """
    @param str: a string
    @return: a compressed string

    1.注意input為空字串
    2.for loop結束後，注意最後一組ch要記得加入ans字串中
    3.如果壓縮後的字串沒有更短，返回原字串
    """
    def compress(self, string):
        # write your code here
        if len(string) == 0:
            return string
        ans = ""
        cnt = 0
        for i in range(len(string)):
            if i == 0:
                ch = string[i]
                cnt += 1
                continue
            
            if string[i] == string[i-1]:
                cnt += 1
                continue
            
            if string[i] != string[i-1]:
                ans += ch + str(cnt)
                ch = string[i]
                cnt = 1

        ans += ch + str(cnt)  # for loop結束後，注意最後一組ch要記得加入ans字串中

        if len(ans) >= len(string):
            return string

        return ans
    
chars = "aaabbbccc"
#chars = "aabbccd"
# chars = "a"
# a = Solution_lintcode().compress(chars)
# print("ans:",a)
