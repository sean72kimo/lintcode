class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        d1 = {}
        d2 = {}

        if s is None or len(s) == 0:
            if t is None or len(t) == 0:
                return True
            return False

        if len(s) != len(t):
            return False

        for c in t:
            cnt = d1.get(c, 0) + 1
            d1[c] = cnt

        for c in s:
            cnt = d2.get(c, 0) + 1
            d2[c] = cnt

        for c in d1:
            if c not in d2:
                return False

            if d1[c] != d2[c]:
                return False

        return True


s = "rat"
t = "cat"
a = Solution().isAnagram(s, t)
print('ans:', a)
