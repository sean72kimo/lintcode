class Solution(object):
    def addStrings(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        i = len(num1) - 1
        j = len(num2) - 1
        carry = 0
        ans = ""
        f = 0
        while i >= 0 or j >= 0:
            n1 = int(num1[i]) if i >= 0 else 0
            n2 = int(num2[j]) if j >= 0 else 0

            n = n1 + n2 + carry
            d = n % 10
            carry = n // 10

            ans = str(d) + ans

            i -= 1
            j -= 1
            f += 1

        if carry:
            ans = str(carry) + ans

        return ans