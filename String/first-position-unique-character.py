class Solution:
    """
    @param: s: a string
    @return: it's index
    """
    def firstUniqChar(self, s):
        # write your code here
        if len(s) == 0:
            return -1

        hash = {}
        for char in s:
            if char not in hash:
                hash[char] = 1
            else:
                hash[char] += 1

        for i in range(len(s)):
            if hash[s[i]] == 1:
                return i
        return -1

s = 'lovelintcode'
print('ans:', Solution().firstUniqChar(s))
