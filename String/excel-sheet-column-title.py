#https://leetcode.com/problems/excel-sheet-column-title/submissions/
class Solution:
    def convertToTitle(self, num: int) -> str:

        while num:
            if num <=26:
                ch = (chr(ord('A') + num - 1))
                ans += ch
                break

            ans = ""
            quotient = num // 26
            ch = (chr(ord('A') + quotient - 1))
            ans += ch
            num = num % 26

        return ans


a = Solution().convertToTitle(701)
print("ans:", a)