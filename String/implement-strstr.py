class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """

        if haystack is None:
            return -1

        if needle is None or len(needle) == 0:
            return 0

        i = 0
        while i < len(haystack) - len(needle) + 1:
            j = 0
            while j < len(needle):
                if haystack[i + j] != needle[j]:
                    break

                j += 1

            if j == len(needle):
                return i

            i += 1
        return -1

haystack = ""
needle = ""

a = Solution().strStr(haystack, needle)
print(a)
