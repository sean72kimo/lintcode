class Solution(object):
    def isOneEditDistance(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if len(s) == 0 and len(t) == 0:
            return False

        if len(t) < len(s):
            return self.isOneEditDistance(t, s)

        if len(t) - len(s) > 1:
            return False

        if len(s) == 0 and len(t) == 1:
            return True

        n = len(s)
        i = j = 0

        while i < len(s):
            if s[i] == t[i]:
                i += 1
                continue
            else:
                return s[i:] == t[i + 1:] or s[i + 1:] == t[i + 1:]

        if i == len(t):
            return False
        return True


