class Solution:
    """
    @param: source: A source string
    @param: target: A target string
    @return: An integer as index
    """


    def strStr2(self, source, target):
        # write your code here
        if source is None or target is None:
            return -1

        if len(target) == 0:
            return 0

        BASE = 1000000
        m = len(target)

        power = 1
        for i in range(m):
            power = (power * 31) % BASE

        targetCode = 0
        for i in range(m):
            targetCode = (targetCode * 31 + ord(target[i])) % BASE
        print("[targetCode]", targetCode)
        hashCode = 0
        for i in range(len(source)):
            hashCode = (hashCode * 31 + ord(source[i])) % BASE

            if i >= m:
                print('dont show')
                # abcd remove a
                #    i
                hashCode = hashCode - (ord(source[i - m]) * power) % BASE

                if hashCode < 0:
                    hashCode = hashCode + BASE

            # double check string
            if hashCode == targetCode:
                if source[i - m + 1 : i + 1] == target:
                    return i - m + 1
        return -1

source = "abcd"
target = "akk"
a = Solution().strStr2(source, target)
print("ans:", a)

print('2'.isalnum())
