class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(-5)
node3 = TreeNode(11)
node4 = TreeNode(1)
node5 = TreeNode(2)
node6 = TreeNode(4)
node7 = TreeNode(-2)

root.left = node2
root.right = node3

node2.left = node4
node2.right = node5

node3.left = node6
node3.right = node7



class ResultType:
    def __init__(self, sum, size):
        self.sum = sum
        self.size = size

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the maximum average of subtree
    average, node = 0, None
    
    def findSubtree2(self, root):
        # Write your code here
        self.helper(root)
        return self.node
        
    def helper(self, root):
        if root is None: 
            return 0, 0
        
        left_sum, left_size = self.helper(root.left)
        right_sum, right_size = self.helper(root.right)
        
        sum = left_sum + right_sum + root.val
        size = left_size + right_size + 1
        avg = sum / size

        if self.node is None or avg > self.average :
            self.node = root
            self.average = avg

        return sum, size
        

sol = Solution()
ans = sol.findSubtree2(root)
print(ans.val)
