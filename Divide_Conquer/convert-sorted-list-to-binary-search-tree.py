class Solution(object):
    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        1. 注意divide 劃分剩下最左邊和最右邊的情況(取 n=2 & n=1兩個corner case測試)
        """
        if not head:
            return None
        if not head.next:
            return TreeNode(head.val)

        slow = head
        fast = head.next
        prev = None
        while fast and fast.next:
            prev = slow
            slow = slow.next
            fast = fast.next.next

        if prev is None:
            h1 = None
        else:
            h1 = head
            prev.next = None

        h2 = slow.next
        slow.next = None

        root = TreeNode(slow.val)
        root.left = self.sortedListToBST(h1)
        root.right = self.sortedListToBST(h2)

        return root
