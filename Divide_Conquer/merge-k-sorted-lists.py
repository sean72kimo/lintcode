
from lib.linkedlist import list2LinkedList, printLinkedList
"""
三種解法
1. heap
2. divide and conquer (similar to merge sort)
3. merge 2 by 2 (similar to merge 2 sorted list)

"""

"""
Definition of ListNode
"""
class ListNode(object):

    def __init__(self, val, mark = 'a', next = None):
        self.val = val
        self.next = next
        self.mark = mark

    def __repr__(self):
        return "<Node {}, {}>".format(self.val, self.mark)

    def __lt__(self, other):
        return self.val < other.val

class Solution:
    """
    @param lists: a list of ListNode
    @return: The head of one sorted list.
    """
    # divideAndConquer
    def mergeKLists_divideAndConquer(self, lists):
        return self.divide(lists, 0, len(lists) - 1)

    def divide(self, lists, start, end):
        if start >= end:
            return lists[start]
         
        mid = (start + end) // 2
        left = self.divide(lists, start, mid)
        right = self.divide(lists, mid+1, end)
        return self.conquer(left, right)
    
    # return head of sorted list
    def conquer(self, one, two):
        dummy = ListNode(0)
        curr = dummy

        while one and two:
            if one.val < two.val:
                curr.next = one
                one = one.next
                curr = curr.next
            else:
                curr.next = two
                two = two.next
                curr = curr.next
        if one:
            curr.next = one
        if two:
            curr.next = two
        
        return dummy.next

    # minHeap
    def mergeKLists_minHeap(self, lists):
        # write your code here
        if len(lists) == 0 or lists is None:
            return

        trav = dummy = ListNode(0)
        heap = []
        for ll in lists:
            if ll:
                self.heappushNode(heap, ll)

        print(len(heap), heap)
        while heap:
            node = heappop(heap)[1]
            trav.next = node
            trav = trav.next

            if trav.next:
                self.heappushNode(heap, trav.next)

        return dummy.next


    def heappushNode(self, heap, node):
        heappush(heap, (node.val, node))


class Solution:  # from merge 2 sorted list, merge 2 by 2
    def mergeKLists(self, lists):
        if not lists:
            return
        if len(lists) == 1:
            return lists[0]

        while len(lists) > 1:
            n = len(lists)
            tmp = []
            for i in range(1, n, 2):
                lst = self.merge(lists[i], lists[i - 1])
                tmp.append(lst)
            if n % 2:
                tmp.append(lists[-1])
            lists = tmp

        return lists[0]

    def merge(self, left, right):
        if not left:
            return right
        if not right:
            return left
        dummy = ListNode()
        curr = dummy

        while left and right:
            if left.val < right.val:
                curr.next = left
                curr = curr.next
                left = left.next
            else:
                curr.next = right
                curr = curr.next
                right = right.next

        if left:
            curr.next = left

        if right:
            curr.next = right

        return dummy.next


lists = [
  [2,4],
  [-1]
]
linked_lists = []
for lst in lists:
    head = list2LinkedList(lst)
    linked_lists.append(head)
    
sol = Solution()
ans = sol.mergeKLists_minHeap(linked_lists)
printLinkedList(ans)


