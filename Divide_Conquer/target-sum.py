class Solution:
    def findTargetSumWays(self, nums, S):
        """
        :type nums: List[int]
        :type S: int
        :rtype: int
        """
        if len(nums) == 0:
            return
        n = len(nums)
        res = self.helper(nums, S)
        return res.count(S)
    
    def helper(self, A, S):
        if len(A) == 1:
            return [A[0], -A[0]]
        start = 0
        end = len(A) - 1
        mid = (start + end) // 2

        left = self.helper(A[:mid+1], S)
        right = self.helper(A[mid+1:], S)
        
        res = self.merge(left, right)
        return res
    def merge(self, A, B):
        
        res = []
        for x in A:
            for y in B:
                res.append(x+y)

        return res
    
nums = [1, 1, 1, 1,1]
S = 3
a = Solution().findTargetSumWays(nums, S)
print("ans:", a)

# A = [1,1]
# start = 0
# end = len(A) - 1
# mid = (start + end) // 2
# print(start, end, mid)
# print(A[start:mid+1], A[mid+1:])