"""
Input: "2*3-4*5"

(2*(3-(4*5))) = -34
((2*3)-(4*5)) = -14
((2*(3-4))*5) = -10
(2*((3-4)*5)) = -10
(((2*3)-4)*5) = 10
"""
class Solution(object):
    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """
        if len(input) == 0:
            return []

        if input.isdigit():
            return [int(input)]

        res = []
        for i in range(len(input)):
            if input[i] in {'+', '-', '*'}:
                lsub = input[:i]
                rsub = input[i + 1 :]

                lres = self.diffWaysToCompute(lsub)
                rres = self.diffWaysToCompute(rsub)

                for x in lres:
                    for y in rres:
                        rt = self.operation(x, y, input[i])
                        res.append(rt)
        return res


    def operation(self, i, j, op):
        if op == '+':
            return i + j
        elif op == '-':
            return i - j
        else:
            return i * j

import operator
class Solution2(object):
    def diffWaysToCompute(self, input):
        if input is None or len(input) == 0:
            return []
        lookup = [[None for _ in range(len(input) + 1)] for _ in range(len(input) + 1)]
        ops = {'+': operator.add, '-': operator.sub, '*': operator.mul}
        ans = self.dfs(input, 0, len(input), ops, lookup)
        return ans

    def dfs(self, input, left, right, ops, lookup):
        if lookup[left][right]:
            return lookup[left][right]

        if input[left:right].isdigit():
            print(int(input[left:right]))
            return [int(input[left:right])]

        res = []
        for i in range(left, right):

            if input[i] in ops:

                lsub = self.dfs(input, left, i, ops, lookup)
                rsub = self.dfs(input, i + 1, right, ops, lookup)

                for x in lsub:
                    for y in rsub:
                        res.append(ops[input[i]](x, y))

        lookup[left][right] = res
        return res

input = "2*3-4*5"
a = Solution2().diffWaysToCompute(input)
print("ans:", a)













