class Solution(object):
    def beautifulArray(self, N):
        """
        :type N: int
        :rtype: List[int]
        time nlogn
        space n
        """
        res = [1]

        while len(res) < N:
            l = [i * 2 - 1 for i in res]
            r = [i * 2 for i in res]
            res = l + r

        ans = []
        for i in res:
            if i <= N:
                ans.append(i)
        return ans


class Solution_dfs(object):
    def beautifulArray(self, N):
        """
        :type N: int
        :rtype: List[int]
        time nlogn
        space n
        """
        self.mem = {}
        return self.dfs(N)

    def dfs(self, n):
        if n in self.mem:
            print(n, self.mem[n])
            return self.mem[n]
        if n == 1:
            return [1]

        lsub = self.dfs(n // 2 + n % 2)
        rsub = self.dfs(n // 2)
        odd = [i * 2 - 1 for i in lsub]
        even = [i * 2 for i in rsub]
        res = odd + even

        self.mem[n] = res
        return res
N = 10
a = Solution_dfs().beautifulArray(N)
print("ans:", a)