class Solution:
    # @param {int} numCourses a total of n courses
    # @param {int[][]} prerequisites a list of prerequisite pairs
    # @return {int[]} the course order
    def findOrder(self, numCourses, prerequisites):
        # Write your code here

        # for i in range(numCourses):
        #     edges[i] = []
        edges = {key: [] for key in range(numCourses)}
        degrees = [0 for i in range(numCourses)]


        for i, j in prerequisites:
            edges[j].append(i)
            degrees[i] += 1

        queue = list()

        for i in range(numCourses):
            if degrees[i] == 0:
                queue.append(i)

        order = []
        while queue:
            node = queue.pop(0)
            order.append(node)

            for nei in edges[node]:
                degrees[nei] -= 1
                if degrees[nei] == 0:
                    queue.append(nei)

        if len(order) == numCourses:
            return order

sol = Solution()
ans = sol.findOrder(numCourses = 4, prerequisites = [[1,0],[2,0],[3,1],[3,2]])
print(ans)
