class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
    def __str__(self):
        return "Node{0}".format(self.val)

root = TreeNode(3)
node2 = TreeNode(9)
node3 = TreeNode(20)


node4 = TreeNode(15)
node5 = TreeNode(7)

root.left = node2
root.right = node3
node3.left = node4
node3.right = node5


def printQ (queue):
    print('Q:[', end='')
    for node in queue:
        print(node.val, end='')
    print(']')

class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        # write your code here
        result = []
        if root is None:
            return result

        q = [root]
        while q:
            for n in q:
                print (n.val)

            new_q = []
            result.append([n.val for n in q])
            for node in q:
                if node.left:
                    new_q.append(node.left)
                if node.right:
                    new_q.append(node.right)
            q = new_q

        return result



    def levelOrder2(self, root):
        # write your code here
        result = []
        if root is None:
            return result

        q = [root]
        while q:
            level = []
            size = len(q)

            for i in range(size):
                # for n in q:
                #     print(n.val)
                # print("---------")
                head = q.pop(0)
                level.append(head.val)

                if head.left:
                    q.append(head.left)
                if head.right:
                    q.append(head.right)

            result.append(level)

        return result



    def levelOrder3(self, root):
        # write your code here
        result = []
        if root is None:
            return result

        visited = set()
        queue = [root]
        result.append([root.val])
        visited.add(root.val)

        while queue:

            level =[]
            current = queue.pop(0)

            if current.left:
                queue.append(current.left)
                visited.add(current.left.val)
                level.append(current.left.val)

            if current.right:
                queue.append(current.right)
                visited.add(current.right.val)
                level.append(current.right.val)

            if level:
                result.append(level)

        return result




    def levelOrder4(self, root):
        # write your code here
        result = []
        if root is None:
            return result

        visited = set()
        queue = [root]
        result.append([root.val])
        visited.add(root.val)

        while queue:

            level =[]
            current = queue.pop(0)
            neighbors = [current.left, current.right]

            for nei in neighbors:
                if nei and nei.val not in visited:
                    queue.append(nei)
                    visited.add(nei.val)
                    level.append(nei.val)

            if level:
                result.append(level)

        return result


    def levelOrder5(self, root):
        # write your code here
        result = []
        if not root:
            return result

        q = [root]
        while q:

            tmp = []

            for node in q:
                print('visitng:', node.val)
                tmp.append(node.val)
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)

            q.pop(0)
            print(result)
            result.append(tmp)


        return result


sol = Solution()
ans = sol.levelOrder5(root)
print('ans:',ans)