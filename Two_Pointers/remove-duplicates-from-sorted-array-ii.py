"""
time O(n)
採用相向雙指針，做法類似sort colors ii，整理過的元素放在l左邊，r往前移動

r pointer 是當前要判斷的元素，
如果不保留，則r+=1(跳過)，
如果保留，r和l元素(要保留的元素換到l位置，然後l+=1代表l左邊整理完成)

Solution2作法類似，區別在於把要保留的元素，覆寫在l指針左側
"""
class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) <= 2:
            return

        l = r = 0
        duplicate = False
        while r < len(nums):
            if r == 0:
                r += 1
                l += 1
                continue

            if r == 1:
                if nums[l] == nums[l - 1]:
                    duplicate = True
                r += 1
                l += 1
                continue

            if nums[r] == nums[l - 1]:
                if duplicate:
                    print(r, duplicate)
                    r += 1
                    continue


            nums[r], nums[l] = nums[l], nums[r]
            duplicate = False
            if nums[l] == nums[l - 1]:
                duplicate = True
            print(l, r, duplicate)
            l += 1
            r += 1

        return l


class Solution2:
    def removeDuplicates(self, nums):
        if len(nums) <= 2:
            return

        i = j = count = 1

        while i < len(nums):
            if nums[i] == nums[i - 1]:
                count += 1
            else:
                count = 1

            if count <= 2:
                nums[j] = nums[i]
                j += 1

            i += 1

        return j
nums = [1, 1, 1, 2, 2, 3]

nums = [1,1,1,2,2,2,3,3]
l = Solution().removeDuplicates(nums)
print("ans:", nums[:l])


