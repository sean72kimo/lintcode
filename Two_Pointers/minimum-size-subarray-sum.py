"""
Given an array of n positive integers and a positive integer s,
find the minimal length of a subarray of which the sum ≥ s.
If there isn't one, return -1 instead.

Example
Given the array [2,3,1,2,4,3] and s = 7,
the subarray [4,3] has the minimal length under the problem constraint.
"""


class Solution(object):
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0 or sum(nums) < s:
            return 0

        mp = collections.defaultdict(int)
        i, j, summ = 0, 0, 0
        ans = float('inf')

        while i < len(nums):
            summ += nums[i]

            while summ >= s:
                d = i - j + 1
                ans = min(ans, d)
                summ -= nums[j]
                j += 1

            i += 1

        return ans if ans != float('inf') else 0
# nums = [1, 1, 1, 1, 1, 1, 1, 1]
# s = 10
nums = [2, 3, 1, 2, 4, 3]
s = 7
print('ans:', Solution().minimumSize(nums, s))
7
[2,3,1,2,4,3]
 0 1 2 3 4 5