"""
雙指針，j走在前面，i走在後面
i代表前一個不重複數字
所以如果nums[j]和nums[j-1]重複，那麼nums[j]應該要去比較nums[i]尋找峰或是谷
"""
from typing import List


class Solution:
    def countHillValley(self, nums: List[int]) -> int:
        if len(nums) <= 2:
            return 0
        n = len(nums)
        i = 0
        j = 1
        res = 0
        while i < j and j < n - 1:
            if nums[i] < nums[j] > nums[j + 1]:
                res += 1
                i = j
            elif nums[i] > nums[j] < nums[j + 1]:
                res += 1
                i = j
            elif nums[i] == nums[j]:
                i = j
            j += 1
        return res
