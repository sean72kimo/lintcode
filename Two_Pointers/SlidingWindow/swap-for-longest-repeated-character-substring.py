from collections import Counter


class Solution:
    def maxRepOpt1(self, text: str) -> int:

        counter = Counter(text)
        left = 0
        ans = 0
        cur = Counter()
        for right, char in enumerate(text):
            cur[char] += 1
            c, n = cur.most_common(1)[0]
            # 如果窗口外的不够替换，或者替换次数超过1，则缩小窗口
            while right - left + 1 > counter[c] or right - left + 1 - n > 1:
                cur[text[left]] -= 1
                left += 1
                c, n = cur.most_common(1)[0]
            ans = max(ans, right - left + 1)
        return ans

text = "ababa"
# text = "aaabaa"
a = Solution().maxRepOpt1(text)
print("ans:", a)

