"""
此題計數：ans += i - j + 1 因為單一元素也算一個subarray, ex: i = j = 0, 也構成 1 個subarray
vs 611. Valid Triangle Number
ans += right - left, 此為固定右端點，左端點往右移動，可以構成幾組解
"""
import collections


class Solution:
    def subarraysWithKDistinct(self, A: List[int], K: int) -> int:
        if len(A) == 0 or K == 0:
            return 0
        return self.at_most_k(A, K) - self.at_most_k(A, K - 1)

    def at_most_k(self, A, K):
        mp = collections.defaultdict(int)
        i = j = 0
        ans = 0
        while i < len(A):
            mp[A[i]] += 1

            while len(mp) > K:
                mp[A[j]] -= 1
                if mp[A[j]] == 0:
                    mp.pop(A[j])
                j += 1

            ans += i - j + 1  # 單一元素也算一個subarray
            i += 1

        return ans

