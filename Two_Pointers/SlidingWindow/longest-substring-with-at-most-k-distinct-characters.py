import collections


class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        if len(s) == 0 or k == 0:
            return 0
        mp = collections.defaultdict(int)
        ans = 0
        i = j = 0
        while i < len(s):
            mp[s[i]] += 1

            while j < i and len(mp) > k:
                mp[s[j]] -= 1
                if mp[s[j]] == 0:
                    mp.pop(s[j])
                j += 1

            ans = max(ans, i - j + 1)
            i += 1

        return ans

class Solution2:
    """
    @param: s: A string
    @param: k: An integer
    @return: An integer
    """
    def lengthOfLongestSubstringKDistinct(self, s, k):
        # write your code here
        if s is None:
            return None
        if k is None or k == 0:
            return 0

        n = len(s)
        hash = {}
        i, j = 0, 0
        longest = 0


        for i in range(n):

            while j < n:
                c = s[j]
                if c in hash:
                    hash[c] += 1
                else:
                    if len(hash) == k:
                        break
                    hash[c] = 1
                j += 1


            longest = max(longest, j - i)

            c = s[i]
            if c in hash:
                if hash[c] > 1:
                    hash[c] -= 1
                else:
                    del hash[c]
        return longest

s = "eceba"
k = 3
s = "eqgkcwGFvjjmxutystqdfhuMblWbylgjxsxgnoh"
k = 16
print('ans:', Solution().lengthOfLongestSubstringKDistinct(s, k))
print(s[0])


