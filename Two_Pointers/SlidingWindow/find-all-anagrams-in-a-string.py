import collections
"""
窗口大小固定, similar to 567. Permutation in String
"""
class Solution2(object):
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]

        time O(n)
        space O(n)
        counter = collections.Counter(p) is at most 26 char,
        """
        counter = collections.Counter(p)
        mp = collections.defaultdict(int)

        i, j = 0, 0

        ans = []
        while i < len(s):
            mp[s[i]] += 1
            size = i - j + 1

            while j <= i and size == len(p):
                if counter == mp: # counter is at most 26 char,
                    ans.append(j)
                mp[s[j]] -= 1
                if mp[s[j]] == 0:
                    mp.pop(s[j])
                j += 1
                size = i - j + 1

            i += 1
        return ans



class Solution(object):
    """
    :type s: str
    :type p: str
    :rtype: List[int]

    time O(n)
    space O(1)
    mp = collections.Counter(p) is at most 26 char,
    """
    def findAnagrams(self, s, p):
        mp = collections.Counter(p)
        i, j = 0, 0
        ans = []
        while i < len(s):
            mp[s[i]] -= 1
            size = i - j + 1

            while size > len(p):
                mp[s[j]] += 1
                if mp[s[j]] == 0:
                    mp.pop(s[j])
                j += 1
                size = i - j + 1


            if size == len(p) and self.is_anagram(mp):
                ans.append(j)
            i += 1

        return ans

    def is_anagram(self, mp):
        for k, v in mp.items():
            if v != 0:
                return False
        return True

s = "cbaebabacd"
p = "abc"
exp = [0, 6]
a = Solution().findAnagrams(s, p)
print("ans:", a==exp, a)

