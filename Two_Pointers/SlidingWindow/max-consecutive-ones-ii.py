"""
三種O(n) time作法
1. sliding window
2. string process
3. DP
"""
class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) <= 1:
            return len(nums)

        i = j = 0
        f = 0
        ans = float('-inf')

        while i < len(nums):
            if nums[i] != 1:
                f += 1

            while j < i and f > 1:

                if nums[j] == 0:
                    f -= 1
                j += 1

            ans = max(i - j + 1, ans)
            i += 1

        return ans

""" string process
1. use multiple O(n) to convert input into a strings of 1 and 0
1. split the string via 0 會得到一個list,  每個item in the list是由1組成，一個不同長度的子字串
1. 此方法也可以用在 max-consecutive-ones-iis
"""
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        if len(nums) <= 1:
            return len(nums)

        tmp = ''.join(list(map(str, nums)))
        print(tmp)
        lst_of_ones = tmp.split('0')
        print(lst_of_ones)

        if len(lst_of_ones) == 1:  # for case there is no zeros in the input
            return len(lst_of_ones[0])

        ans = 0
        for i in range(1, len(lst_of_ones)):
            size = len(lst_of_ones[i]) + len(lst_of_ones[i - 1]) + 1
            ans = max(size, ans)

        return ans


"""
DP
"""


class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        n = len(nums)
        f = [[0, 0] for _ in range(n + 1)]
        ans = 0
        for i in range(n):
            if i == 0:
                if nums[i]:
                    f[i][0] = 1
                    f[i][1] = 1
                else:
                    f[i][0] = 0
                    f[i][1] = 1
                continue

            if nums[i]:
                f[i][0] = f[i - 1][0] + 1
                f[i][1] = f[i - 1][1] + 1
            else:
                f[i][0] = 0
                f[i][1] = f[i - 1][0] + 1

        ans = 0
        for i in range(n):
            ans = max(max(f[i]), ans)
        return ans
