import collections
from sys import maxsize
class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """

        if not s or len(s)==0:
            return ""

        map=collections.defaultdict(int)
        for ch in t:
            map[ch]+=1

        count=len(t)
        begin=0
        end=0
        d=maxsize  # or len(s) + 1
        head=0  # start of substring

        while end<len(s):
            e_ch=s[end]

            if map[e_ch]>0:
                count-=1

            map[e_ch]=map[e_ch]-1
            end+=1

            while count==0:

                if end-begin<d:
                    head=begin
                    d=end-begin

                s_ch=s[begin]
                map[s_ch]+=1
                if map[s_ch]>0:
                    count+=1
                begin+=1

        if d>len(s):
            return ""

        return s[head:head+d]



S="ADOBECODEBANC"
T="ABC"
# S="ADOBECODEBANC"
# T="ABX"
# S="aa"
# T="aa"
ans=Solution().minWindow(S, T)
print('ans:', ans)


class Solution2(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """

        if len(s)==0 or len(t)==0:
            return ""

        left = 0
        right = 0
        mp = collections.Counter(t)

        count = len(t)

        ans = ""
        # S = "ADOBECODEBANC"
        # T = "ABC"
        
        while right < len(s):

            if s[right] in mp:
                mp[s[right]] -= 1
                print(mp)
                if mp[s[right]] >= 0:
                    count -= 1
            
            while count == 0:
                if ans == "":
                    ans = s[left : right+1]

                if right - left + 1<len(ans):
                    ans = s[left : right+1]

                if s[left] in mp:
                    mp[s[left]] += 1
                    if mp[s[left]] > 0:
                        count += 1
                left += 1

            right += 1

        return ans


a=Solution2().minWindow(S, T)
print('ans:', a)

