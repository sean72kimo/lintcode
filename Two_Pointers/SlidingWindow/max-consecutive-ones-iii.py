class Solution(object):
    def longestOnes(self, nums, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: int
        """
        # if not any(nums) and K == 0:
        #     return 0

        if len(nums) <= 1:
            return len(nums)

        i = j = 0
        f = 0
        ans = float('-inf')

        while i < len(nums):
            if nums[i] != 1:
                f += 1

            while j < i and f > K:

                if nums[j] == 0:
                    f -= 1
                j += 1

            ans = max(i - j + 1, ans)
            i += 1

        return ans