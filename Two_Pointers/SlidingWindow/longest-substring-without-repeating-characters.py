import collections


class Solution:
    """
    @param: s: a string
    @return: an integer
    """
    def lengthOfLongestSubstring(self, s):
        # write your code here
        if s is None:
            return None

        ans = 0
        left = 0
        last = {}
        n = len(s)

        for i in range(n):
            if s[i] in last and last[s[i]] >= left:
                left = last[s[i]] + 1
            last[s[i]] = i
            print(s[left], s[i], left, i, s[left:i + 1])
            ans = max(i - left + 1, ans)

        return ans
# s = "abcabcbb"
# print('ans:', Solution().lengthOfLongestSubstring(s))


from collections import defaultdict
from sys import maxsize

class Solution(object):
    def lengthOfLongestSubstring(self, s):

        if s is None or len(s) == 0: return 0

        n = len(s)
        start = 0
        end = 0
        cnt = 0
        d = -maxsize
        map = defaultdict(int)


        while end < len(s):
            che = s[end]
            map[che] += 1

            if map[che] > 1:
                cnt += 1

            end += 1

            while cnt > 0:

                chs = s[start]
                map[chs] -= 1
                if map[chs] == 1:
                    cnt -= 1
                start += 1


            d = max(d, end - start)

        return d
s = "pwwkew"
print('ans:', Solution().lengthOfLongestSubstring(s))


class Solution2(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0

        mp = collections.defaultdict(int)
        ans = 1
        i = j = 0
        k = 0
        while i < len(s):
            r = s[i]
            mp[s[i]] += 1

            if mp[s[i]] > 1:
                k += 1

            while k > 0:
                l = s[j]
                mp[s[j]] -= 1
                if mp[s[j]] <= 1:
                    k -= 1
                j += 1

            ans = max(ans, i - j + 1)
            i += 1

        return ans
s = "pwwkew"
print('ans:', Solution2().lengthOfLongestSubstring(s))