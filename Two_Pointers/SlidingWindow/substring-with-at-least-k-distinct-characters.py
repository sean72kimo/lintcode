import collections
class Solution:
    """
    @param s: a string
    @param k: an integer
    @return: the number of substrings there are that contain at least k distinct characters
    """

    def kDistinctCharacters(self, s, k):
        # Write your code here
        #mp = collections.Counter()
        start = 0
        mp = collections.Counter()
        n = len(s)
        l = r = 0
        count = 0
        ans = 0
        while r < n:
            mp[s[r]] += 1
            if mp[s[r]] == 1:
                count += 1

            while count == k:
                ans += n - r
                mp[s[l]] -= 1
                if mp[s[l]] == 0:
                    count -= 1
                l += 1

            r += 1
        return ans
s = "abcabcabca"
k = 4
Solution().kDistinctCharacters(s, k)