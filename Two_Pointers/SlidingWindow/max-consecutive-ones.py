from typing import List


class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        max_cnt = 0
        cur_cnt = 0

        for i in range(len(nums)):
            if nums[i] == 0:
                cur_cnt = 0
                continue

            cur_cnt += 1
            max_cnt = max(max_cnt, cur_cnt)

        return max_cnt
"""
1. use multiple O(n) to convert input into a strings of 1 and 0
1. split the string via 0 會得到一個list,  每個item in the list是由1組成，一個不同長度的子字串
1. 此方法也可以用在 max-consecutive-ones-iis
"""
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        tmp = ''.join(list(map(str, nums)))
        lst_of_ones = tmp.split('0')

        ans = 0
        for ones in lst_of_ones:
            ans = max(ans, len(ones))
        return ans