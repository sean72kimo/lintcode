import collections
a = "xxfggqwae"
a.sort()


class Solution:
    def characterReplacement(self, s: str, k: int) -> int:
        mp = collections.defaultdict(int)

        ans = 0
        most = (s[0], 1)
        j = 0
        for i in range(len(s)):
            mp[s[i]] += 1
            if mp[s[i]] >= most[1]:
                most = (s[i], mp[s[i]])

            while i - j + 1 - most[1] > k:
                mp[s[j]] -= 1
                j += 1

            ans = max(ans, i - j + 1)

        return ans

s = "AABACD"
k = 1
a = Solution().characterReplacement(s, k)
print("ans:",a )


class Solution:
    def characterReplacement(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        if len(s) == 0:
            return 0

        i, j = 0, 0

        mp = collections.Counter()
        ans = 0
        lo = 0
        i = 0
        for i in range(1, len(s) + 1):
            mp[s[i - 1]] += 1
            max_char_n = mp.most_common(1)[0][1]
            print(mp)
            if i - lo - max_char_n > k:

                mp[s[lo]] -= 1
                lo += 1

            ans = max(ans, i - lo)
        return ans
# s = "AABABBA"
# k = 1
# a = Solution().characterReplacement(s, k)
# print("ans:", a)



class Solution2(object):
    def characterReplacement(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        if len(s) == 0:
            return 0
        
        left = 0
        right = 0
        ans = 0
        mp = collections.defaultdict(int)

        most_count = 1
        while right < len(s):
            mp[s[right]] += 1
            if most_count < mp[s[right]]:
                most_count = mp[s[right]]
                print('ch:', s[right], most_count)

            while right - left + 1 - most_count > k:
                mp[s[left]] -= 1
                left += 1

                for key, value in mp.items():
                    if value > most_count:
                        most_count = value

            right += 1
            ans = max(ans, right - left)
            
        return ans
    
# s = "AABABBA"
# k = 1
# a = Solution2().characterReplacement(s, k)
# print("ans:", a)