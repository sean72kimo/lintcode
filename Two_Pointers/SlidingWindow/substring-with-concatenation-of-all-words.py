from collections import Counter
"""30. Substring with Concatenation of All Words
Input:
  s = "barfoothefoobarman",
  words = ["foo","bar"]
Output: [0,9]
"""
class Solution:
    def findSubstring(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        if len(s) == 0 or len(words) == 0:
            return []

        ans = []
        mp = collections.Counter(words)
        left = 0
        right = 0
        n = len(words)
        m = len(words[0])
        
        while right < len(s):
            if right + n * m > len(s):
                break
                
            clone = dict(mp)
            left = right
            
            while len(clone) != 0:
                
                sub = s[left : left+m]
                if sub not in clone:
                    break
                clone[sub] -= 1
                if clone[sub] == 0:
                    del clone[sub]
                
                if len(clone) == 0:
                    ans.append(right)
                left += m
            
            right += 1
        return ans