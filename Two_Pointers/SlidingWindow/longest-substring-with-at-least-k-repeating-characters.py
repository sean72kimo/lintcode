import collections
"""
最早無法找到移動滑窗的條件
參考網路解答
我們將滑窗的條件設定為：多少個獨特字母，將此題轉換成以下子問題
在1個獨特字母的情況下，那個字母最少出現k次，找到最長的子字串
在2個獨特字母的情況下，那些字母最少出現k次，找到最長的子字串
在3個獨特字母的情況下，那些字母最少出現k次，找到最長的子字串
在4個獨特字母的情況下，那些字母最少出現k次，找到最長的子字串 ...
在26個獨特字母的情況下，那些字母最少出現k次，找到最長的子字串
"""
class Solution(object):
    def longestSubstring(self, s: str, k: int) -> int:
        ans = 0
        for how_many_disdinct_ch in range(1, 27):
            size = self.helper(s, k, how_many_disdinct_ch)
            ans = max(size, ans)
        return ans

    def helper(self, s, k, m):

        mp = collections.Counter()
        cnt = 0
        res = 0
        j = 0
        for i in range(len(s)):
            mp[s[i]] += 1

            if mp[s[i]] == k:
                cnt += 1

            while len(mp) > m:
                ch = s[j]
                mp[ch] -= 1

                if mp[ch] == 0:
                    mp.pop(ch)
                elif mp[ch] == k - 1:
                    cnt -= 1
                j += 1

            if cnt == m:
                res = max(res, i - j + 1)
        return res
    
s = "aaabb"
k = 3
s = "ababbc"
k = 2
s = "bbaaacbd"
k = 3
s = "aaaaaaaaaaaaaaaabbbbbbbbbbbbaaaaaaabbbbbbbbbbbbcccccccccccdddddddddddddddddddeeeeeeeeeeeeeeefffffffffffffffgggggggggggggggggggghhhhhhhhhhhhhhhhiiiiiiiiiiiiiiiiiiiiiijjjjjjjjjjjjjjjjjjjjjjjjkkkkkkkkkkkkkkkkkkkk"
k = 20
a = Solution().longestSubstring(s, k)
print("ans:", a)

