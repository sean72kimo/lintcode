"""
time: O(n)

sorted array 想到  -> 1. binary search (本題不適用) 2. two pointers
採用 雙指針思路
該用同向還是相向? 通常需要調整sum 與target差距, 可能左邊多一點，可能右邊少一點，才會使用同向雙指針
採用相向雙指針，做法類似sort colors ii，整理過的元素放在l左邊，r往前移動

r pointer 是當前要判斷的元素，
如果不保留，則r+=1(跳過)，
如果保留，r和l元素(要保留的元素換到l位置，然後l+=1代表l左邊整理完成)
"""
class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) <= 1:
            return

        l = r = 0

        while r < len(nums):
            if r == 0:
                r += 1
                l += 1
                continue
            if nums[r] == nums[l - 1]:
                r += 1
                continue

            nums[r], nums[l] = nums[l], nums[r]
            r += 1
            l += 1

        return l
