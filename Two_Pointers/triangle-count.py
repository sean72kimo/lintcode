
# this is also 611. Valid Triangle Number
class Solution:
    """
    @param S: A list of integers
    @return: An integer
    """
    def triangleCount(self, nums):
        # write your code here


        if len(nums) < 3 :
            return 0
        nums.sort()
        ans = []
        res = 0
        # a + b > c

        for i in range(len(nums) - 1, 1, -1):
            left = 0
            right = i - 1

            while left < right:
                if nums[left] + nums[right] > nums[i]:
                    res = res + (right - left)
                    right -= 1
                else:
                    left += 1

        return res
nums = [4, 4, 4, 4]
nums = [3, 4, 6, 7]
a = Solution().triangleCount(nums)
print("ans:", a)


class Solution2:
    """
    @param S: A list of integers
    @return: An integer
    """
    def triangleCount(self, nums):
        # write your code here


        if len(nums) < 3 :
            return 0
        nums.sort()
        ans = []
        res = 0
        # a + b > c

        for i in range(len(nums) - 1, 1, -1):
            left = 0
            right = left + 1

            while left < i - 1 and right < i:
                
                if nums[left] + nums[right] <= nums[i]:
                    right += 1
                else:
                    print("[nums{}, nums{}, nums{}] = [{},{},{}]".format(left, right, i, nums[left], nums[right], nums[i]))
                    res = res + i - 1 - right + 1
                    right = left + 1
                
                left += 1

        return res

nums = [4, 4, 4, 4]
nums = [3, 4, 6, 7]
a = Solution2().triangleCount(nums)
print("ans:", a)