class Solution:
    """
    @param nums {int[]} n array of Integer
    @param target {int} = nums[index1] + nums[index2]
    @return {int[]} [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum(self, nums, target):
        # Write your code here
        if len(nums) == 0:
            return [None, None]
        left = 0
        right = len(nums) - 1

        while left < right:
            value =  nums[left] + nums[right]

            if value == target:
                return [left+1, right+1]
            if value < target:
                left += 1
            else:
                right -= 1

        return [None,None]

nums = [2, 7, 11, 15]
target = 9
sol = Solution()
print(sol.twoSum(nums, target))
