class Solution:
    """
    @param nums {int[]} n array of Integer
    @param target {int} = nums[index1] + nums[index2]
    @return {int[]} [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum5(self, nums, target):
        # Write your code here
        if len(nums) == 0:
            return 0

        nums.sort()
        l = 0
        r = len(nums) - 1

        count = 0
        while l < r:
            val = nums[l] + nums[r]
            if val > target:
                r -= 1
            else:
                count += r - l
                l += 1
        
        return count 
        
    
nums = [2, 7, 11, 15]
target = 24
sol = Solution()
print(sol.twoSum5(nums, target))
