class Solution:
    # @param nums {int[]} a list of integers
    # @param k {int} size of window
    # @return {int[]} the sum of element inside the window at each moving
    def winSum1(self, nums, k):
        # Write your code here
        res = []
        n = len(nums)
        if n < k or k <= 0:
            return[]

        head = 0
        tail = k

        while tail <= len(nums):

            num = 0
            for i in range(head, tail):
                num += nums[i]

            res.append(num)
            tail += 1
            head += 1
        return res

    def winSum2(self, nums, k):
        # Write your code here

        n = len(nums)
        if n < k or k <= 0:
            return[]
        res = [0] * (n - k + 1)

        for i in range(k):
            res[0] += nums[i]

        for i in range(1, len(res)):
            res[i] = sum[i - 1] - nums[i - 1] + nums[i + k - 1]


    def winSum(self, nums, k):
        # write your code here
        if nums is None or len(nums) == 0:
            return []

        if k <= 0:
            return []

        n = len(nums)

        res = []
        i = 0
        # j = k -1

        for i in range(n - k + 1):
            print(i, i + k)
            window_sum = sum(nums[i:i + k])
            res.append(window_sum)
            i += 1
        return res

sol = Solution()
nums = [1, 2, 7, 7, 2]
k = 3
ans = sol.winSum(nums, k)
print(ans)
print(sum(nums[1:4]))
