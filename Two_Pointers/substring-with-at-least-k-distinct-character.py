import collections
class Solution:
    """
    @param s: a string
    @param k: an integer
    @return: the number of substrings there are that contain at least k distinct characters
    """
    def __init__(self):
        self.ans = 0
    
    def kDistinctCharacters(self, s, k):
        # Write your code here
        #mp = collections.Counter()
        start = 0
        for left in range(len(s)):
            if left + k > len(s):
                break
            mp = collections.Counter()
            for right in range(left, len(s)):
                mp[s[right]] += 1
                if len(mp) == k:
                    self.ans = self.ans + len(s) - right
                    mp[s[left]] -= 1
                    if mp[s[left]] == 0:
                        del mp[s[left]]
                    start = right+1
                    break
        return self.ans
    

import collections
class Solution2:
    """
    @param s: a string
    @param k: an integer
    @return: the number of substrings there are that contain at least k distinct characters
    """
    def __init__(self):
        self.ans = 0
    
    def kDistinctCharacters(self, s, k):
        # Write your code here
        #mp = collections.Counter()
        start = 0
        mp = collections.Counter()
        n = len(s)
        l = r = 0
        count = 0
        ans = 0
        while r < n:
            mp[s[r]] += 1
            if mp[s[r]] == 1:
                count += 1

            while count == k:
                ans += n - r
                mp[s[l]] -= 1
                if mp[s[l]] == 0:
                    count -= 1
                l += 1

            r += 1
        return ans
s = "bccs"
k = 2
s = "abcabcabca"
k = 3
a = Solution().kDistinctCharacters(s, k)
print("ans:", a)
b = Solution2().kDistinctCharacters(s, k)
print("ans:", b)
"""
maintaining a window of m distinct char
when each distinct char has k of them, cnt += 1
when reaching all m distinct char has k of them
calculate the window size, maximize the window
"""
import collections
from sys import maxsize
class Solution(object):
    def minWindow(self, s, t):
        if not s or len(s) == 0:
            return ""
        mp = collections.Counter(t)
        count = len(t)
        print(mp)
        left = 0
        right = 0
        d = maxsize
        head = 0
        ans = ""
        """
        XADOBCAODEBANC
        ABC
        """
        while right < len(s):
            if mp[s[right]] > 0:
                count -= 1

            mp[s[right]] -= 1

            # first found a sub string includes all t
            while count == 0:
                if right - left + 1 < d:
                    ans = s[left : right + 1]
                    d = right - left + 1

                # try to move left to see if I can narrow this sub string more
                mp[s[left]] += 1
                if mp[s[left]] > 0:
                    count += 1
                left += 1

            right += 1

        if d == maxsize:
            return ""

        return ans


