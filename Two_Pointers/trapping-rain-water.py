from typing import List


class Solution:
    """
    @param: heights: a list of integers
    @return: a integer
    """
    def trapRainWater(self, heights):
        # write your code here
        if heights is None or len(heights) == 0 :
            return 0

        l = 0
        r = len(heights) - 1
        res = 0



        left_height = heights[l]  # start water height
        right_height = heights[r]

        while l < r:
            if left_height < right_height:
                l += 1
                if left_height > heights[l]:
                    res += (left_height - heights[l])
                else:
                    left_height = heights[l]
            else:
                r -= 1
                if right_height > heights[r]:
                    res += (right_height - heights[r])
                else:
                    right_height = heights[r]

        return res

heights = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
a = Solution().trapRainWater(heights)
print("ans:", a)


class Solution:
    def trapRainWater(self, heights: List[int]) -> int:
        if len(heights) == 0:
            return

        length = len(heights)
        stack = []  # desc
        ans = 0

        for r, r_h in enumerate(heights):
            if r == 7:
                print(".")
            while stack and heights[stack[-1]] < r_h:
                j = stack.pop()
                curr_h = heights[j]


                if stack:
                    l = stack[-1]
                    l_h = heights[stack[-1]]
                    width = r - l - 1
                    water = min(l_h, r_h) - curr_h
                    ans += water * width
            stack.append(r)

        return ans
heights = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
a = Solution().trapRainWater(heights)
print("ans:", a)

class Solution:
    def trap(self, heights: List[int]) -> int:
        if len(heights) == 0:
            return

        length = len(heights)

        left_max = [0] * length
        left_max[0] = heights[0]
        for i in range(1, length):
            left_max[i] = max(heights[i], left_max[i - 1])

        right_max = [0] * length
        right_max[length - 1] = heights[length - 1]
        for i in range(length - 2, -1, -1):
            right_max[i] = max(right_max[i + 1], heights[i])

        water = 0
        for i in range(length):
            water += max(0, min(left_max[i], right_max[i]) - heights[i])

        return water


class Solution:
    def trap(self, height: List[int]) -> int:
        if len(height) == 0:
            return

        left = 0
        right = len(height) - 1
        ans = 0

        while left + 1 < right:
            left_h = height[left]
            right_h = height[right]
            curr_height = min(left_h, right_h)

            if left_h < right_h:
                ans += max(left_h - height[left + 1], 0)
                if height[left + 1] < left_h:
                    height[left + 1] = left_h
                left += 1

            else:
                ans += max(right_h - height[right - 1], 0)
                if height[right - 1] < right_h:
                    height[right - 1] = right_h
                right -= 1

        return ans