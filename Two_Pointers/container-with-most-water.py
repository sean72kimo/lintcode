class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        if len(height) == 0:
            return 0
        left = 0
        right = len(height) - 1
        ans = 0
        while left < right:
            width = right - left
            h = min(height[left], height[right])
            area = width * h

            ans = max(ans, area)

            if height[left] < height[right]:
                left += 1
            else:
                right -= 1

        return ans

class Solution:
    """
    @param: heights: a vector of integers
    @return: an integer
    """
    def maxArea(self, h):
        # write your code here
        maxArea = 0
        i = 0
        j = len(h) - 1

        while i < j:
            w = j - i  # 注意! 右邊減去左邊(因為j>i)
            if h[i] < h[j]:
                area = h[i] * w
                maxArea = max(area, maxArea)
                i += 1
            else:
                area = h[j] * w
                maxArea = max(area, maxArea)
                j -= 1

        return maxArea

heights = [1, 3, 2]
heights = [4, 2, 3, 5, 1]
print('ans:', Solution().maxArea(heights))
