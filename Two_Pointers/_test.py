class Solution(object):
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        if height is None or len(height) == 0:
            return 0
        
        l = 0
        r = len(height) - 1
        lh = height[l]
        rh = height[r]

        ans = 0
        while l < r:

            lh = max(lh, height[l])
            ans = ans + lh - height[l]
            print(l, height[l], lh, ans)
            l += 1

        print(l, height[l], lh, ans)
            # rh = max(rh, height[r])
            # ans = ans + rh - height[r]
            # r -= 1

        return ans
height = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
#         0  1  2  3  4  5  6  7  8  9  10 11
Solution().trap(height)


