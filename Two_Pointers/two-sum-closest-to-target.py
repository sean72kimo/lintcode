from sys import maxsize
class Solution:


    """
    @param: nums: an integer array
    @param: target: An integer
    @return: the difference between the sum and the target
    """
    def twoSumClosest(self, nums, target):
        if len(nums) == 0:
            return None
        nums.sort()
        l = 0
        r = len(nums) - 1
        diff = maxsize

        while l < r:
            sum = nums[l] + nums[r]
            if sum < target:
                diff = min(diff, target - sum)
                l += 1
            else:
                diff = min(diff, sum - target)
                r -= 1

        return diff

nums = [-1, 2, 1, -4]
target = 4
nums = [0,1,2]
target = 0
sol = Solution()
print("ans:",sol.twoSumClosest(nums, target))



class Solution2:
    """
    @param nums: an integer array
    @param target: An integer
    @return: the difference between the sum and the target
    """
    def __init__(self):
        self.diff = float('inf')
        
    def twoSumClosest(self, A, T):
        # write your code here
        if len(A) < 2 :
            return 0
        
        A.sort()
        
        i = 0
        j = len(A) - 1
        
        for i in range(len(A)):
            if i > 0 and A[i] == A[i-1]:
                continue
            
            while j > 0 and A[j] > T - A[i]:
                j -= 1
            
            print(i, j)
            self.update(A, T, i, j)
            self.update(A, T, i, j+1)
            self.update(A, T, i, j-1)

        return self.diff
    
    def update(self, A, T, i, j):
        if i < 0 or i > len(A) - 1 or j < 0 or j > len(A) - 1 or i == j:
            return
        
        diff = abs(A[j] + A[i] - T)
        self.diff = min(self.diff, diff)
        
        
nums = [-1, 2, 1, -4]
target = 4
nums = [0,1,2]
target = 0
sol = Solution2()
print("ans:",sol.twoSumClosest(nums, target))

