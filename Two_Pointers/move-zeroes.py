class Solution:
    # @param {int[]} nums an integer array
    # @return nothing, do this in-place
    def moveZeroes(self, nums):
        left = 0
        right = 0
        
        while right < len(nums):

            if nums[right]:
                nums[right], nums[left] = nums[left], nums[right]
#                 tmp = nums[left]
#                 nums[left] = nums[right]
#                 nums[right] = tmp
                left += 1 
                
            right += 1


sol = Solution()
nums = [0, 1, 0, 3, 12]

ans = sol.moveZeroes(nums)
print(nums)