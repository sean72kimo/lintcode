class TwoSum(object):

    def __init__(self):
        # initialize your data structure here
        self.nums = list()
        

    # Add the number to an internal data structure.
    # @param number {int}
    # @return nothing
    def add(self, number):
        # Write your code here
        if not isinstance(number, int):
            raise
        self.nums.append(number)
            

    # Find if there exists any pair of numbers which sum is equal to the value.
    # @param value {int}
    # @return true if can be found or false
    def find(self, value):
        if not isinstance(value,int):
            raise
        hash = {}
        for i in range(len(self.nums)):
            # a + b = value
            a = value - self.nums[i]
            if a in hash:
                return True
            hash[self.nums[i]] = i
        
        return False

# Your TwoSum object will be instantiated and called as such:
twoSum = TwoSum()
twoSum.add(1)
twoSum.add(2)
twoSum.add(3)
print(twoSum.find(7))