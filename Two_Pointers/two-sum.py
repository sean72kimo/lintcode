class Solution:
    """
    @param nums {int[]} n array of Integer
    @param target {int} = nums[index1] + nums[index2]
    @return {int[]} [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum(self, nums, target):
        hash = {}
        for i in range(len(nums)):
            hash[nums[i]] = i

        for i in range(len(nums)):
            a = target - nums[i]
            if a in hash:
                return [i + 1, hash[a] + 1]

        return []

    def twoSum2(self, nums, target):
        # Write your code here
        hash = {}

        for i in range(len(nums)):
            # a + b = target
            # a = target - b
            a = target - nums[i]
            print("{0}-{1}={2}".format(target, nums[i], a))
            if a in hash:
                return[hash[a] + 1 , i + 1 ]

            hash[nums[i]] = i

        return [None, None]

nums = [1, 0, -1]
target = 0

nums = [2, 7, 11, 15]
target = 9
sol = Solution()
print(sol.twoSum2(nums, target))


