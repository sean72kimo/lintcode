class Solution:
    """
    @param nums: the array
    @param target: the target
    @return: the number of subsets which meet the following conditions
    """
    def subsetWithTarget(self, nums, target):
        # Write you code here
        if len(nums) == 0:
            return 0

        nums.sort()

        size = len(nums)
        ans = 0

        for i in range(size):
            j = i
            while j + 1 < size and nums[i] + nums[j + 1] < target:
                j += 1
            print(i, j)
            if nums[i] + nums[j] < target:
                ans = ans + (1 << (j - i))


        return ans
nums = [1, 2, 3, 4, 5]
target = 5
a = Solution().subsetWithTarget(nums, target)
print("ans:", a)
print(1 << 3)

