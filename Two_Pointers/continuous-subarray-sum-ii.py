# leetcode 523. Continuous Subarray Sum
class Solution(object):
    def checkSubarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        n = len(nums)
        if n == 0:
            return False
        runningSum = 0
        map = {0:-1}

        for i in range(n):
            runningSum = runningSum + nums[i]
            if k != 0:
                runningSum = runningSum % k

            if runningSum in map:
                return True
                # if i - map[runningSum] > 1:
                #    return True
            else:
                map[runningSum] = i
        return False

nums = [1, 4, 3]
k = 3
a = Solution().checkSubarraySum(nums, k)
print("ans:", a)

nums = [2, 5, 33, 6, 7, 25, 15]
addup = 0
presum = [0] * (len(nums) + 1)
pre = []
for i, n in enumerate(nums):
    addup = addup + n
    presum[i + 1] = addup
    pre.append(addup)

print('target=', sum(nums[2:4]))
print(pre[4], pre[1])
print(presum[5], presum[2])

