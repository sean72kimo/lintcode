class Solution(object):
    def __init__(self):
        self.ans = []
    def sortTransformedArray(self, nums, a, b, c):
        """
        :type nums: List[int]
        :type a: int
        :type b: int
        :type c: int
        :rtype: List[int]
        """
        start = 0
        end = len(nums) - 1

        while start <= end:
            startNum = self.getNum(nums[start], a, b, c)
            endNum = self.getNum(nums[end], a, b, c)
            #print(start, end, startNum, endNum)
            
            if a > 0:
                if startNum >= endNum:
                    self.ans.append(startNum)
                    start += 1
                else:
                    self.ans.append(endNum)
                    end -= 1
            else:
                if startNum >= endNum:
                    self.ans.append(endNum)
                    end -= 1
                else:
                    self.ans.append(startNum)
                    start += 1

        if a > 0:
            self.ans.reverse()
            
        return self.ans
    
    def getNum(self, x, a, b, c):
        return a *x ** 2 + b * x + c
    
nums = [-4, -2, 2, 4]
a, b, c, = 1, 3, 5
a, b, c = -1, 3, 5

a = Solution().sortTransformedArray(nums, a, b, c)
print("ans:", a)

# tmp = []
# for x in nums:
#     tmp.append((Solution().getNum(x, a, b, c), x))
# print(tmp)
str = "sean"
orig = list(str)
for i, n in enumerate(orig, 1):
    print(i ,n)