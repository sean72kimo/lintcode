"""
sort color, regular partition and three-way partition
"""
class Solution:
    """
    @param nums: A list of integer which is 0, 1 or 2
    @return: nothing
    """
    def sortColors(self, nums):
        # write your code here
        if len(nums) == 0:
            return

        pl = 0
        pr = len(nums) - 1
        i = 0

        while i <= pr:
            if nums[i] == 0:
                nums[i], nums[pl] = nums[pl], nums[i]
                pl += 1
                i += 1
            elif nums[i] == 1:
                i += 1
            else:
                nums[pr], nums[i] = nums[i], nums[pr]
                pr -= 1

    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        self.partition(nums, 0)
        self.partition(nums, 1)

    def partition(self, A, p):
        left = 0
        right = len(A) - 1

        while left <= right:
            while left <= right and A[left] <= p:
                left += 1
            while left <= right and A[right] > p:
                right -= 1
            if left <= right:
                A[left], A[right] = A[right], A[left]
                left += 1
                right -= 1


nums = [1, 0, 1, 2]
Solution().sortColors(nums)
print(nums)
