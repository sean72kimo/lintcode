from sys import maxsize
class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        # a + b + c = target
        # a + b = target - c
        if not nums or len(nums) < 3:
            return

        n = len(nums)
        nums.sort()


        best = nums[0] + nums[1] + nums[2]

        for i in range(n - 1):

            l = i + 1
            r = n - 1

            while l < r:

                sum = nums[i] + nums[l] + nums[r]
                print(sum)

                if abs(target - sum) < abs(target - best):
                    best = sum

                if sum < target:
                    l += 1
                else:
                    r -= 1
        return best
nums = [1, 2, 4, 8, 16, 32, 64, 128]
target = 82
# nums = [-1, 2, 1, -4]
# target = 1
ans = Solution().threeSumClosest(nums, target)
print('ans:', ans)
