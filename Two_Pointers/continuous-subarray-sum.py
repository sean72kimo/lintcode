from sys import maxsize
class Solution:
    """
    @param: A: An integer array
    @return: A list of integers includes the index of the first number and the index of the last number
    """
    def continuousSubarraySum(self, A):
        # write your code here
        if not A or len(A) == 0 :
            return[]
        ans = -0x7fffffff
        n = len(A)
        addup = 0
        start = end = 0
        res = []
        for i in range(n):
            if addup < 0:
                start = end = i
                addup = A[i]
            else:
                addup += A[i]
                end = i

            if addup > ans:
                ans = addup
                res = [start, end]
        return res
A = [-3, 1, 3, -3, 4]
a = Solution().continuousSubarraySum(A)
print('ans:', a)
