"""
compare Solution_WA,
Solution_AC讓搜索的範圍越來越小
Solution_WA讓搜索的範圍越來越大
"""

class Solution_AC:
    """
    @param numbersbers : Give an array numbersbers of n integer
    @return : Find all unique triplets in the array which gives the sum of zero.
    """
    def threeSum(self, numbers):
        if len(numbers) < 3:
            return None
        numbers.sort()
        res = []

        for i in range(len(numbers) - 1):
            if i and numbers[i] == numbers[i - 1]:
                continue

            target = -numbers[i]
            left = i + 1
            right = len(numbers) - 1

            while left < right:
                value = numbers[left] + numbers[right]
                if value == target:
                    res.append([numbers[i], numbers[left] , numbers[right]])
                    left += 1
                    right -= 1

                    while left < right and numbers[left] == numbers[left - 1]:
                        left += 1
                    while left < right and numbers[right] == numbers[right + 1]:
                        right -= 1

                elif value < target:
                    left += 1
                else:
                    right -= 1
        return res





class Solution_WA(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if len(nums) == 0:
            return []
        nums.sort()
        ans = []
        for i in range(2, len(nums)):
            if i != 2 and nums[i] == nums[i - 1]:
                continue

            left = 0
            right = i - 1
            t = -nums[i]

            while left < right:
                v = nums[left] + nums[right]

                if v == t:
                    ans.append([nums[left], nums[right], nums[i]])
                    left += 1
                    right -= 1

                    while left < right and nums[left] == nums[left - 1]:
                        left += 1

                    while left < right and nums[right] == nums[right + 1]:
                        right -= 1


                elif v < t:
                    left += 1
                    while left < right and nums[left] == nums[left - 1]:
                        left += 1

                elif v > t:
                    right -= 1
                    while left < right and nums[right] == nums[right + 1]:
                        right -= 1

        return ans

numbers = [-2,0,1,1,2]
sol = Solution()
print(sol.threeSum(numbers))