class Solution:
    """
    @param nums: an array of Integer
    @param target: an integer
    @return: [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum7(self, nums, target):
        # write your code here
        if len(nums) == 0:
            return []

        hsmap = {}
        for i in range(len(nums)):
            hsmap[nums[i]] = i + 1
        print(hsmap)
        nums.sort()

        left = 0
        right = left + 1

        while left < right and right < len(nums):
            diff = nums[right] - nums[left]

            if diff == -target or diff == target:
                return [ hsmap[nums[left]]  , hsmap[nums[right]]  ]

            if diff < abs(target):
                right += 1
            elif diff > abs(target):
                left += 1


        return []
nums = [1, 0, -1]
target = -2
a = Solution().twoSum7(nums, target)
print("ans:", a)
