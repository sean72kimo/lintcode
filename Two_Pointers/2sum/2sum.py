"""
sort the array
"""
class Solution:
    def twoSum(self, nums, target):
        A = sorted(nums)
        print(A)

        left = 0
        right = len(A) - 1
        ans = []
        while left < right:
            val = A[left] + A[right]

            if val > target:
                right -= 1
            elif val < target:
                left += 1
            else:
                ans.append([ A[left], A[right] ])
                left += 1


        return ans



"""
use hashmap
"""
class Solution2:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        if not nums:
            return []

        hsmap = collections.defaultdict()
        for i, n in enumerate(nums):
            val = target - n
            if val in hsmap:
                j = hsmap[val]
                return [j, i]
            hsmap[n] = i

nums = [5, 1, 2, 3, 8, 4, 9, ]
target = 13
a = Solution().twoSum(nums, target)
print("ans:", a)
