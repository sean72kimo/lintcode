class Solution:
    def twoSumLessThanK(self, A: List[int], K: int) -> int:
        if len(A) == 0:
            return -1
        A.sort()
        n = len(A)
        left = 0
        right = n - 1
        diff = float('inf')
        ans = float('-inf')

        while left < right:
            S = A[left] + A[right]

            if S < K:
                if K - S < diff:
                    diff = K - S
                    ans = S
                left += 1

            elif S >= K:
                right -= 1

        if ans == float('-inf'):
            return -1

        return ans
