from typing import List
"""
3sum系列，採取"縮小搜索範圍"的方式
本題求所有解的個數，所以不需要去重
"""
class Solution:
    def threeSumSmaller(self, nums: List[int], target: int) -> int:
        nums.sort()
        n = len(nums)
        ans = 0
        for i in range(n - 1, -1, -1):
            left = 0
            right = i - 1

            while left < right:
                val = nums[left] + nums[right] + nums[i]

                if val >= target:
                    right -= 1

                elif val < target:
                    print(left, right, i, val)
                    ans += (right - left)
                    left += 1
        return ans

nums = [-2, 0, 1, 3]
target = 0
nums = [3,1,0,-2]
target = 4
a = Solution().threeSumSmaller(nums, target)
print("ans:", a)