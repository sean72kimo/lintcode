class Solution:
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        A = sorted(nums)
        print(A)
        ans = []
        
        for k in range(len(A)-1, -1, -1):
            if k < len(A)-1 and A[k] == A[k+1]:
                continue
            self.twoSum(A, k, ans)


        return ans
    
    def twoSum(self, A, right, ans):

        T = -A[right] 
        i = 0
        j = right - 1

        while i < j:
            if i > 0 and A[i] == A[i-1]:
                i += 1
                continue
            
            v = A[i] + A[j]
            
            
            if v > T:
                j -= 1
            elif v < T:
                i += 1
            else:
                ans.append((A[i], A[j], A[right]))
                i+=1
                


        
nums = [-1,0,1,2,-1,-4]
nums = [0,0,0,0]
a = Solution().threeSum(nums)
print(a)

n = 5
for i in range(2, n, 1):
    print(i)