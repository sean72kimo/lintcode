from typing import List


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        if not nums or len(nums) < 4:
            return []
        nums.sort()
        ll = len(nums)
        ans = []

        for i in range(ll):
            if i != 0 and nums[i] == nums[i - 1]:
                continue

            for j in range(i + 1, ll):
                if j != i + 1 and nums[j] == nums[j - 1]:
                    continue

                t = target - nums[i] - nums[j]

                left = j + 1
                right = ll - 1

                while left < right:
                    val = nums[left] + nums[right]

                    if val == t:
                        # ans.append([nums[i],nums[j],nums[left],nums[right]])
                        ans.append((nums[i], nums[j], nums[left], nums[right]))
                        left += 1
                        right -= 1
                        while left < right and nums[right] == nums[right + 1]:
                            right -= 1
                        while left < right and nums[left] == nums[left - 1]:
                            left += 1
                    elif val < t:
                        left += 1
                        while left < right and nums[left] == nums[left - 1]:
                            left += 1
                    elif val > t:
                        right -= 1
                        while left < right and nums[right] == nums[right + 1]:
                            right -= 1

        return ans

nums = [1, 0, -1, 0, -2, 2]
target = 0
nums = [1, 0, -1, -1, -1, -1, 0, 1, 1, 1, 2]
target = 2
a = Solution().fourSum(nums, target)
print("ans:", a)
