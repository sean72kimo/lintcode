import collections
from typing import List

# 3sum-with-multiplicity
#
class Solution:
    def fourSumCount(self, A: List[int], B: List[int], C: List[int], D: List[int]) -> int:
        ans = 0
        counter = collections.Counter()
        for a in A:
            for b in B:
                counter[a + b] += 1

        for c in C:
            for d in D:
                target = -(c + d)
                ans += counter[target]

        return ans