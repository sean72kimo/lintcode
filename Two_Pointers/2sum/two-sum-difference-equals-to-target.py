class Solution_hashmap:
    """
    @param nums: an array of Integer
    @param target: an integer
    @return: [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum7(self, nums, target):
        # write your code here
        if not any(nums):
            return [-1, -1]
        mp = {}
        
        left = 0
        right = len(nums) - 1
        
        for i in range(len(nums)):

            #nums[i] - x = target
            x = nums[i] - target
            if x in mp:
                return [mp[x]+1, i+1]

            #x - nums[i] = target
            x = target + nums[i]
            if x in mp:
                return [mp[x]+1, i+1]
            

            mp[nums[i]] = i    
        
        return [-1, -1]

class Pair:
    def __init__(self, idx, num):
        self.num = num
        self.idx = idx
        
    def __repr__(self):
        return str(self.num)
    
class Solution:
    """
    @param nums: an array of Integer
    @param target: an integer
    @return: [index1 + 1, index2 + 1] (index1 < index2)
    """
    def twoSum7(self, nums, target): #two pointer
        
        n = len(nums)
        res = []
        if nums is None or len(nums) == 0:
            return []
        
        def mykey(p):
            return p.num
        
        pair = []
        for i, n in enumerate(nums):
            pair.append(Pair(i, n))
        
        pair.sort(key=mykey)
        i = j = 0

        res = []
        for i in range(len(nums)):
            while (j < len(nums) and pair[j].num < pair[i].num - target ):
                j+=1
 
            if i == j and target == 0:
                j+=1
             
            if j < len(nums) and pair[j].num == pair[i].num - target:
                print((i,j))
                res.append(min(pair[j].idx, pair[i].idx) + 1)
                res.append(max(pair[j].idx, pair[i].idx) + 1)
                return res
             
        return res

            
nums = [2,7,15,24]
target = -5
a = Solution().twoSum7(nums, target)
print("ans:", a)
            
            
            
            
            


