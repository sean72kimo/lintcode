class Solution:
    """
    @param nums: an array of integer
    @param target: An integer
    @return: an integer
    """
    def twoSum2(self, nums, target):
        # write your code here
        A = sorted(nums)
        n = len(A)
        j = len(A) - 1
        ans = 0
        
        for i in range(len(A)):
            while j >= 0 and A[j] > target - A[i]:
                j -= 1
            
            if j < i:
                break
            
            ans += len(A)-1 - j + 1
        return ans


class Solution2:
    """
    @param nums: an array of integer
    @param target: An integer
    @return: an integer
    """
    def twoSum2(self, nums, target):
        # write your code here
        A = sorted(nums)
        
        i = 0
        j = len(A) - 1
        ans = 0
        
        while i < j:
            val = A[i] + A[j]
            
            if val > target:
                ans += j - i
                j -= 1
            else:
                i += 1
        return ans


sol = Solution2()
nums = [1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99]
target = -64
a = sol.twoSum2(nums, target)
print("ans:", a, a==93)
nums = [2,7,11,15]
target = 3
a = sol.twoSum2(nums, target)
print("ans:", a)

