import collections


class Solution(object):
    def numPairsDivisibleBy60(self, time):
        """
        :type time: List[int]
        :rtype: int
        (60 - t % 60) % 60
        括號內：x + t = 60, x = 60 - t , t取模
        括號外，為什麼要在%60? if t = 60, we are looking for 0, so we % 60 to set it into bucket[0]
        """
        if len(time) == 0:
            return 0

        counter = collections.defaultdict(int)
        ans = 0

        for i, t in enumerate(time):
            ans += counter[(60 - t % 60) % 60]
            counter[t % 60] += 1

        return ans
time = [60,60,60]
exp = 3
a = Solution().numPairsDivisibleBy60(time)
print("ans:",a==exp, a)