import collections


class Solution:
    def findPairs(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """

        myset = collections.Counter(nums)
        ans = set()
        res = 0
        for n in myset:

            x = n + k
            if x in myset:
                if x == n:
                    if myset[n] > 1:
                        ans.add((n, x))
                else:
                    ans.add((n, x))

        return len(ans)