from typing import List


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        n = len(nums)
        nums.sort()
        diff = float('inf')
        ans = float('inf')
        for i in range(n - 2):

            left = i + 1
            right = n - 1

            while left < right:
                val = nums[i] + nums[left] + nums[right]
                d = abs(val - target)
                if d < diff:
                    diff = d
                    ans = val

                if val == target:
                    return ans

                elif val < target:
                    left += 1

                elif val > target:
                    right -= 1
        return ans

nums = [-1, 2, 1, -4]
target = 1
# nums = [0,1,2]
# target = 3
a = Solution().threeSumClosest(nums, target)
print("ans", a)
