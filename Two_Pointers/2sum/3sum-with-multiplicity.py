import collections
from typing import List


class Solution(object):
    def threeSumMulti(self, A, target):

        # mp = collections.defaultdict(int)
        mp = [0] * 301

        res = 0
        MOD = 10 ** 9 + 7

        for i, val in enumerate(A):
            print(target - val)
            res += mp[target - val]

            for j in range(i):
                temp = A[j] + val
                mp[temp] += 1

        return res % MOD
A = [1,1,2,2,3,3,4,4,5,5]
target = 8

A = [12,97,74,39,56,3,85,39,18,29,41,7,33,97,13,47,11,52,32,45,8,36,35,45,59,54,18,55,63,65,57,63,60,71,86,76,65,12,59,83,70,100,20,2,41,70,53,39,54,64,48,93,86,100,75,100,23,2,46,54,81,10,94,32,75,31,20,35,49,46,46,96,43,75,37,37,51,86,4,18,30,73,65,1,55,22,32,12,86,100,95,24,16,40,13,95,43,87,46,86]
target = 213
a = Solution().threeSumMulti(A, target)
print("ans:", a)
