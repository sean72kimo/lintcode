class Solution:
    """
    @param: s: A string
    @return: Whether the string is a valid palindrome
    """
    def isPalindrome(self, s):
        # write your code here
        if s is None or len(s) == 0:
            return True

        i = 0;
        j = len(s) - 1

        while i < j:
            while not s[i].isalnum() and i < j:
                i += 1
            while not s[j].isalnum() and i < j:
                j -= 1

            if s[i].lower() != s[j].lower():
                return False

            i += 1
            j -= 1

        return True


s = "A man, a plan, a canal: Panama"
s = ".,"


print(Solution().isPalindrome(s))

# for i in range(len(s)):
#     print(s[i], s[i].isalnum())
