class Solution:
    # @param nums {int[]} an array of integer
    # @param target {int} an integer
    # @return {int} an integer
    def twoSum6(self, nums, target):
        # Write your code here
        if len(nums) == 0:
            return 0

        nums.sort()
        l = 0
        r = len(nums) - 1
        count = 0
        while l < r:
            value = nums[l] + nums[r]
            if value == target:
                count += 1
                l += 1
                r -= 1
                while (l < r) and nums[l] == nume[l - 1]:
                    l += 1
                while (l < r) and nums[r] == nume[r + 1]:
                    r -= 1
            elif value > target:
                r -= 1
            else:
                l += 1

        return count




nums = [7, 11, 11, 1, 2, 3, 4]
target = 22
sol = Solution()
print(sol.twoSum6(nums, target))
