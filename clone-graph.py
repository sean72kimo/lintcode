class UndirectedGraphNode(object):
    def __init__(self, x):
        self.label = x
        self.neighbors = []
    def __str__(self):
        return "<Node %s>" % (self.label)

class Solution(object):
    def cloneGraph(self, node):
        """
        :type node: UndirectedGraphNode
        :rtype: UndirectedGraphNode
        """

        if not node:
            return None



        cloned = {}
        cloned[node] = UndirectedGraphNode(node.label) #start_cloned_node
        queue = [node]

        while queue:
            current = queue.pop(0)
            for neighbor in current.neighbors:
                if neighbor not in cloned:
                    queue.append(neighbor)
                    cloned[neighbor] = UndirectedGraphNode(neighbor.label)

                cloned[current].neighbors.append(cloned[neighbor])
        return cloned[node]


A = UndirectedGraphNode('A')
B = UndirectedGraphNode('B')
C = UndirectedGraphNode('C')
D = UndirectedGraphNode('D')
E = UndirectedGraphNode('E')
F = UndirectedGraphNode('F')
G = UndirectedGraphNode('G')
H = UndirectedGraphNode('H')

A.neighbors = [B,D,G]
B.neighbors = [A,E,F]
C.neighbors = [F,H]
D.neighbors = [A,F]
E.neighbors = [B,G]
F.neighbors = [B,C,D]
G.neighbors = [A,E]
H.neighbors = [C]


sol = Solution()
res = sol.cloneGraph(A)
# print(res)
# res = sol.myDFS(A)
print(res)