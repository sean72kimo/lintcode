class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)

root.left = node2
root.right = node3

"""
stack
"""
class StackSol:
    """
    @param root: The root of binary tree.
    @return: Preorder in list which contains node values.
    """
    def preorder(self, root):
        if root is None:
            return []
        stack = [root]
        preorder = []
        while stack:
            node = stack.pop()
            preorder.append(node.val)
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
        return preorder
    
    def inorder(self, root):
        inorder = []

        if root is None:
            return inorder
        
        current = root
        stack = []
        while current or stack:

            if current:
                stack.append(current)
                current = current.left
            else:
                node = stack.pop()
                inorder.append(node.val)
                print(inorder)
                current = node.right

        return inorder



sol = StackSol()

ans = sol.inorder(root)
print('Answer:',ans)
            

"""
traverse
"""
class TraverseSol:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
    result = []
    def preorderTraversal(self, root):
        if root is None:
            self.result
        self.preorder(root)
        return self.result
        
    def preorder(self, node):
        if node:
            print(node.val, self.result)
            self.result.append(node.val)
            self.preorder(node.left)
            self.preorder(node.right)
        return self.result
        
    def inorder(self, node):
        if node:
            self.inorder(node.left)
            self.result.append(node.val)
            self.inorder(node.right)
        return self.result

    def postorder(self, node):
        
        if node:
            print(node.val)
            self.postorder(node.left)
            self.postorder(node.right)
            
            self.result.append(node.val)
            print(self.result)
        return self.result
"""
divide and conquer
"""
class Solution3(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """

        res = []
        if root is None:
            return res
        
        print(root.val)
        left_res = self.preorderTraversal(root.left)
        print('[left_res]', left_res)

        right_res = self.preorderTraversal(root.right)
        print('[right_res]', right_res)

        res.append(root.val)

        res += left_res

        res += right_res
        print('[res]', res)

        return res 
        


class Solution4(object):
    def maxDepth(self, root):
        if root is None:
            return 0
        left = self.maxDepth(root.left)
        print ('left',left)
        right = self.maxDepth(root.right)
        print ('right',right)
        return max(left, right) + 1

# sol = Solution2()
# # ans = sol.preorderTraversal(root)
# ans = sol.inorder(root)
# print(ans)
