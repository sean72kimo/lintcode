# compare
from functools import cmp_to_key


def mycmp(slf, othr):
    if ord(slf) > ord(othr):
        return 1
    elif ord(slf) < ord(othr):
        return -1
    else:
        return 0


def mycmp2(x, y):
    if ord(x) > ord(y):
        return 1
    elif ord(x) < ord(y):
        return -1
    else:
        return 0


arr = ['a', 'b', 'c', 'd']
# def mykey(ch):
#     return ord(ch)
arr.sort(key=cmp_to_key(mycmp2))
print(arr)
