class Solution(object):
    def isToeplitzMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: bool
        """
        for r, row in enumerate(matrix):
            for c, val in enumerate(row):
                if r == 0:
                    continue
                if c == 0:
                    continue

                if not matrix[r - 1][c - 1] == val:
                    return False
        return True


# 此方法有點類似N-queen
class Solution2(object):
    def isToeplitzMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: bool
        """
        group = {}
        for r, row in enumerate(matrix):
            for c, val in enumerate(row):
                val = matrix[r][c]
                if (r-c) in group and group[r-c] != val:
                    return False

                group[r-c] = val

        return True