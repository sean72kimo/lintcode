"""
139. Subarray Sum Closest
https://www.lintcode.com/problem/subarray-sum-closest

如果要找subarray sum = 0, 怎麼做？prefix sum
如果要找subarray sum 最靠近 0, 也是用prefix sum
計算出 prefix sum
排序後，prefix sum差值最小的兩個，必定靠在一起 i and i-1
(排序時，用一個pari (val, i)) i代表前i個數字的prefix sum為val
找出差值最小的一組，並將"前i個" 轉回nums下標
"""
class Solution:
    """
    @param: nums: A list of integers
    @return: A list of integers includes the index of the first number and the index of the last number
    """

    def subarraySumClosest(self, nums):
        # write your code here
        if len(nums) == 0:
            return [-1, -1]

        pre = [(0,0)]
        for i, n in enumerate(nums):
            pre.append((pre[-1][0] + n, i+1))
        sorted_pre = sorted(pre, key=lambda x: x[0])

        diff = float('inf')
        for i in range(1, len(sorted_pre)):
            d = sorted_pre[i][0] - sorted_pre[i - 1][0]

            if d < diff:
                diff = d

                a = sorted_pre[i - 1][1]
                b = sorted_pre[i][1]

        ans = [a,b]
        ans.sort()
        ans[-1] -= 1

        return ans



nums = [-3,1,-1,1,-3,5]
exp = [1,2]

nums = [-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-1000]
exp = [0,8]
sol = Solution()
a = sol.subarraySumClosest(nums)
print("ans:", a)

