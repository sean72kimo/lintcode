from typing import List


class Solution:
    def maxNonOverlapping(self, nums: List[int], target: int) -> int:
        mp = {}
        mp[0] = 0
        summ = 0
        res = 0
        for i in range(len(nums)):
            summ += nums[i]

            val = summ - target
            if val in mp:
                res = max(res, mp[val] + 1)

            mp[summ] = res

        return res


class Solution:
    def maxNonOverlapping(self, nums: List[int], target: int) -> int:
        mp = {0: -1}
        summ = 0
        res = 0
        last_i = -1

        for i in range(len(nums)):

            summ += nums[i]
            val = summ - target

            if val in mp and mp[val] >= last_i:
                res += 1
                last_i = i

            mp[summ] = i
            print(mp)

        return res


nums = [-1,3,5,1,4,2,-9]
target = 6

nums = [2,1,1,1,1]
target = 2

a = Solution().maxNonOverlapping(nums, target)
print("ans:", a)


print(max("123", "231"))


print(len(output) == len(Expected))

for i in range(len(output)):
    if output[i] != Expected[i]:
        print(i, output[i])
        break