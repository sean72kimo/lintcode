import collections

"""
用兩個mp: freq and need
freq mp記錄每個數字出現幾次
need mp記錄尾巴需要延伸多長(延伸至哪個數)
if nums[i] = 2 若要構成一個len >= 3的subarray, 那麼freq[3], freq[4]必須 > 1, 
freq[3]-=1, freq[4]-=1, 代表被使用於 [2,3,4] subarray 
並且紀錄是否可以構成更長(>4)以上的subarray? need[4], 
if next nums[i] = 4, 滿足上面的need[4], 調整 need 為need[5] (記得刪除need[4]這個舊的需求)
每輪都要調整當前數字的freq[nums[i]] -= 1



用freq map先过一遍存频率，再建一个map存我们能用到的tail number。再过第二遍的时候，若freq==0 continue；
若能接上前面的顺子，就接；
不能则新开一个顺子（记住新开时候直接要把连着的两个数字剔除，因为要保证长度为 3）；
都不行则为false。记住最后别忘了更新当前频率

对于每一个element，我们有两种选择
把它加入之前构造好的顺子中
用它新开一个顺子
此处用贪心策略，如果1能满足总是先满足1，因为新开顺子可能失败，即使新开顺子成功，当1能满足的时候，将新开顺子加入之前的顺子也能成功，所以能够选择策略1的时候没必要冒风险选择策略2

目标是用策略1或者2消耗掉所有的元素

如果两个策略都无法选择，直接返回false

"""
class Solution(object):
    def isPossible(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) < 2:
            return False
        freq = collections.Counter(nums)
        need = collections.Counter()

        for num in nums:
            if freq[num] == 0:
                continue

            if need[num] > 0:
                need[num] -= 1
                need[num + 1] += 1

            elif freq[num + 1] > 0 and freq[num + 2] > 0:
                freq[num + 1] -= 1
                freq[num + 2] -= 1
                need[num + 3] += 1

            else:
                return False

            freq[num] -= 1

        return True

nums = [1,2,3,3,4,5]
a = Solution().isPossible(nums)
print("ans:", a)