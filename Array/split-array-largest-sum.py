class Solution_dp(object):
    def splitArray(self, nums, m):
        n = len(nums)
        f = [[float('inf') for _ in range(m+1)] for _ in range(n+1)]

        presum = [0]
        for i in range(n):
            presum.append(presum[-1] + nums[i])

        for i in range(n+1): #前幾個元素
            for j in range(1, m+1): #分成幾組
                if i == 0 and j == 0:
                    f[i][j] = 0
                    continue

                if i == 0:
                    f[i][j] = 0
                    continue

                if j == 1:
                    f[i][j] = presum[i]
                    continue

                for k in range(i):
                    f[i][j] = min(max(f[k][j-1], presum[i] - presum[k]), f[i][j])

        return f[n][m]



class Solution_binary_search(object):
    def splitArray(self, nums, m):
        if len(nums) == 0:
            return 0
        if m == 1:
            return sum(nums)

        start = max(nums)
        end = sum(nums)

        while start + 1 < end:
            mid = (start + end) // 2
            if self.largest_sum_satisfy_m(nums, m, mid):
                end = mid
            else:
                start = mid

        if self.largest_sum_satisfy_m(nums, m, start):
            return start
        return end

    def largest_sum_satisfy_m(self, nums, m, largest_sum):
        num_of_sub = 0
        curr_sum = 0
        for num in nums:
            if curr_sum + num <= largest_sum:
                curr_sum += num
            else:
                num_of_sub += 1
                curr_sum = num
        num_of_sub += 1
        return num_of_sub <= m


class Solution_dfs_mem(object):
    def splitArray(self, nums, m):
        n = len(nums)
        presum = [0]
        for i in range(n):
            presum.append(presum[-1] + nums[i])
        self.mem = {}
        return self.dfs(nums, m, 0, presum)

    def dfs(self, nums, m, start, presum):
        if (start, m) in self.mem:
            return self.mem[(start, m)]

        if m == 1:
            return presum[len(nums)] - presum[start]

        maxSum = float('inf')
        for i in range(start, len(nums)):
            left_sum = presum[i+1] - presum[start]
            right_sum = self.dfs(nums, m-1, i+1, presum)
            maxSum = min(maxSum, max(left_sum, right_sum))
            print(maxSum, left_sum, right_sum)

        self.mem[(start, m)] = maxSum
        return maxSum

class Solution_TLE_brute_force(object):
    def splitArray(self, nums, m):
        """
        :type nums: List[int]
        :type m: int
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        if m == 0 or m == 1:
            return sum(nums)

        self.ans = float('inf')
        path = []
        self.dfs(nums, 0, m-1, path)
        return self.ans

    def dfs(self, nums, start, m, path):
        if m < 0:
            return

        if start == len(nums):
            if m == 0:

                self.ans = min(self.ans, max(path))
            return


        for i in range(start, len(nums)):
            summ = sum(nums[start:i+1])
            path.append(summ)

            if m == 1:  #最後剩餘的右半部分
                path.append(sum(nums[i+1:]))
                self.dfs(nums, len(nums), m - 1, path)
                path.pop()

            self.dfs(nums, i+1, m-1, path)
            path.pop()

nums = [7,2,5,10,8]
m = 2
a = Solution_dp().splitArray(nums, m)
print("ans:", a)