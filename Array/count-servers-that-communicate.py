from typing import List


class Solution:
    def countServers(self, grid: List[List[int]]) -> int:
        if not grid:
            return 0
        n = len(grid)
        m = len(grid[0])

        rows = [set() for _ in range(n)]
        cols = [set() for _ in range(m)]
        computers = set()

        for i in range(n):
            for j in range(m):
                if not grid[i][j]:
                    continue
                rows[i].add((i, j))
                cols[j].add((i, j))

        for row in rows:
            if len(row) <= 1:
                continue
            for computer in row:
                computers.add(computer)

        for col in cols:
            if len(col) <= 1:
                continue
            for computer in col:
                computers.add(computer)

        return len(computers)



