from typing import List
"""
初始t1 t2為最大值，for loop每個數，讓t1 盡量小，t2大於t1但盡量小。
因為要求絕對上升，等於(<=)的狀況也需要更新t1 or t2

case [2,3,1,5] will end up with t1 = 1, t2 = 3, 
但因為t2有值，也代表著”曾經”有一個數小於t2, 
所以 nums[i]=5	大於t2, 也代表著滿足 increasing triplet 的條件
"""

class Solution:
    def increasingTriplet(self, nums: List[int]) -> bool:
        n = len(nums)
        t1 = t2 = float('inf')
        for i in range(n):
            if nums[i] <= t1:
                t1 = nums[i]
            elif nums[i] <= t2:
                t2 = nums[i]
            else:
                return True
        return False
nums = [2,3,1,5]
a = Solution().increasingTriplet(nums)
print("ans:", a)
