import heapq

"""
左右窗口，
左邊為到位置i最小的數(含位置i)
右邊為比left[i]大，但盡可能小 (用pq實現)
for loop原始數列，找到 left[i] < right[i] < nums[i]
"""
class Solution2(object):
    def find132pattern(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if not len(nums) or len(nums) < 3:
            return False

        left = [nums[0]]
        n = len(nums)
        for i in range(1, n):
            left.append(min(left[-1], nums[i])) # 到位置i最小的數(含位置i)

        right = [float('-inf') for _ in range(n)] # 比left[i]大，但盡可能小

        q = []

        for i in range(n - 1, -1, -1):

            heapq.heappush(q, nums[i])
            while q and q[0] <= left[i]:
                heapq.heappop(q)
            if q:
                right[i] = q[0]

        for i in range(n):
            if right and left[i] < right[i] < nums[i]:
                return True
        return False

"""
單調遞減棧
概念也是類似排序。
從後往前掃，如果整個序列完美遞減，那麼沒有132 pattern
如果進入while loop pop, 代表在當前i的右手邊有數字比當前i還要小 (s2), 當前i則為s3
繼續往前掃，如果找到比s3還小的數字，那麼代表132 pattern成立
"""
class Solution(object):
    def find132pattern(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if not len(nums) or len(nums) < 3:
            return False

        stack = []
        s3 = float('-inf')

        for n in nums[::-1]:
            if n < s3:
                #found s1
                return True

            while stack and n > stack[-1]:
                s3 = stack.pop()

            stack.append(n)

        return False

nums = [1,0,1,-4,-3]
nums = [6,12,3,4,6,11,20]
nums = [3,1,4,2]
sol = Solution()
a = sol.find132pattern(nums)
print("ans:", a)
