class Solution_next_array:
    def maxDistToClosest(self, seats):

        n = len(seats)
        left_dist = [n for _ in range(n)]
        right_dist = [n for _ in range(n)]

        for i in range(n):
            if seats[i] == 1:
                left_dist[i] = 0
                continue

            if i > 0:
                left_dist[i] = left_dist[i - 1] + 1

        for i in range(n - 1, -1, -1):
            if seats[i] == 1:
                right_dist[i] = 0
                continue

            if i < n - 1:
                right_dist[i] = right_dist[i + 1] + 1

        ans = 0
        for i in range(n):
            if seats[i] == 1:
                continue

            ans = max(ans, min(left_dist[i], right_dist[i]))

        print(left_dist)
        print(right_dist)
        return ans

seats = [1,0,0,0,1,0,1]
a = Solution_next_array().maxDistToClosest(seats)
print("ans:", a)


class Solution_group_zero:
    def maxDistToClosest(self, seats):
        left = -1
        ans = 0
        n = len(seats)

        for i in range(n):
            if seats[i] == 0:
                continue

            if left == -1:
                ans = max(ans, i)
            else:
                ans = max(ans, (i - left)//2 )

            left = i

        if seats[-1] == 0:
            ans = max(ans, n - 1 - left)

        return ans

Solution_group_zero().maxDistToClosest(seats)
seats = [1,0,0,0,1,0,1]
a = Solution_group_zero().maxDistToClosest(seats)
print("ans:", a)

