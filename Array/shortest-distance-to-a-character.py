import bisect
from typing import List


class Solution:
    def shortestToChar(self, S: str, C: str) -> List[int]:
        n = len(S)
        idx = []
        ans = [float('inf')] * n

        for i in range(n):
            if S[i] == C:
                idx.append(i)
        print(idx)
        for i in range(n-1, -1, -1):
            if S[i] == C:
                ans[i] = 0
                continue

            ceil = bisect.bisect_left(idx, i)
            floor = bisect.bisect_right(idx, i)

            if ceil == floor:
                ans[i] = abs(ceil - i)
            else:
                right =  ceil - i if ceil - i >= 0 else float('inf')
                left =  i - floor if i - floor >= 0 else float('inf')

                d = min(right, left)
                ans[i] = d

        return ans
S = "aaba"
C = "b"
#
S = "loveleetcode"
C = 'e'
ans = Solution().shortestToChar(S, C)
print("ans:", ans)
