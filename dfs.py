class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)

root.right = node3
node3.left = node2
node3.right = node4
node4.right = node5

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {int} the length of the longest consecutive sequence path
    result = None
    path = []
    def dfs_template(self, root):
        # Write your code here
        if root is None:
            return None
        self.dfs(root)

    def dfs1(self, root):
        if root is None:
            return

        self.path.append(root.val)

        self.dfs(root.left)
        self.dfs(root.right)

        self.path.pop()


    def dfs(self, root):

        if root.left is None and root.right is None:
            print('now', root.val)
            self.path.append(root.val)
            print(self.path)
            self.path.pop()
            return

        self.path.append(root.val)

        if root.left:
            self.dfs(root.left)

        if root.right:
            self.dfs(root.right)

        self.path.pop()



sol = Solution()
ans = sol.dfs_template(root)

print('[ans]',ans)