

class GameConsole:
    def play_game(self, logs):
        games = {}
        for log in logs:
            timestamp, user_id, game_id, status = log.split(',')

            game = games.get(game_id)
            if not game:
                game = {}

            user_journey = game.get(user_id)
            if not user_journey:
                user_journey = {"join": 0, "quit": 0}

            user_journey[status] = int(timestamp)
            game[user_id] = user_journey

            games[game_id] = game
        print(games)

        top_game = None
        top_duration = 0
        for game_id in games:
            user_journey = games[game_id]
            for user_id in user_journey:
                user_status = user_journey[user_id]

                duration = user_status['quit'] - user_status['join']

                if not top_game or duration > top_duration:
                    top_game = game_id
        return top_game






logs = [
"1000000000,user1,1001,join",
"1000000005,user2,1002,join",
"1000000010,user1,1001,quit",
"1000000020,user2,1002,quit"
]

my_top_game = GameConsole().play_game(logs)
print(my_top_game)