import random


class solution:
    def __init__(self):
        self.N = 5
        self.color = 4
        self.valid = 0
        # generate 2xN for gravity
        N = self.N
        self.board = [[random.randrange(1, self.color + 1) for _ in range(N)] for _ in range(N)]

        print("initial board")
        for i in range(N):
            print(self.board[i])
        print("============")

    def checkBoard(self):
        board = self.board
        done = True
        n = len(board)
        # check row
        for r in range(n):
            for c in range(n - 2):
                t1 = abs(board[r][c])
                t2 = abs(board[r][c + 1])
                t3 = abs(board[r][c + 2])

                if t1 != 0 and t1 == t2 and t2 == t3:
                    board[r][c] = -t1
                    board[r][c + 1] = -t2
                    board[r][c + 2] = -t3

                    done = False

        # check col
        for c in range(n):
            for r in range(n - 2):
                t1 = abs(board[r][c])
                t2 = abs(board[r + 1][c])
                t3 = abs(board[r + 2][c])

                if t1 != 0 and t1 == t2 and t2 == t3:
                    board[r][c] = -t1
                    board[r + 1][c] = -t2
                    board[r + 2][c] = -t3

                    done = False

        # check gravity
        if not done:
            self.valid += 1
            for c in range(n):
                idx = n - 1
                for r in range(n - 1, -1, -1):
                    if board[r][c] > 0:
                        board[idx][c] = board[r][c]
                        idx -= 1

                # fill the missing
                for r in range(idx, -1, -1):
                    board[r][c] = random.randrange(1, self.color + 1)

            self.board = board

        # print(self.valid)
        # return board if done else self.checkBoard()
        return self.valid if done else self.checkBoard()

    def swap(self, p0, p1):
        v = self.valid
        board = self.board
        r, c = p0
        nr, nc = p1
        self.board[r][c], self.board[r][c] = self.board[nr][nc], self.board[r][c]
        r = self.checkBoard()
        return True if r > v else False

    def genearteBoard(self):
        pass

    def calculateScore(self):
        pass

    # def calculateDaysBetweenDates(begin, end):


t = solution()
r = t.checkBoard()
rr = t.swap([0, 0], [1, 0])
# for i in range(len(r)):
#     print(r[i])
print(rr)
# solution().checkBoard()