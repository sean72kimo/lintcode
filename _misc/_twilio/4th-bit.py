class Solution:
    def fourthDigits(self, num):
        print('{} in binary {:08b}'.format(num, num))
        return 1 if num & 0b1000 else 0
        count = 0
        remain = 0

        while num != 0 and count < 4:
            remain = num % 2
            num = num // 2
            count += 1

        if count < 4:
            return 0

        return remain




if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # a = map(int, input().rstrip().split())
    # b = map(int, input().rstrip().split())

    # n = int(input().rstrip())
    n = 25

    result = Solution().fourthDigits(n)
    print(result)