import collections


class Solution:
    def computeParameterValue(self, sources):
        mp = collections.OrderedDict()
        for source in sources:
            for report in source:
                p, v = report.split(':')
                mp[p] = v

        print(mp.values())
        return list(mp.values())




if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # a = map(int, input().rstrip().split())
    # b = map(int, input().rstrip().split())

    # s = input().rstrip()
    # t = input().rstrip()
    #
    # print(s)
    # print(t)
    #reports = [{'P1':'x', 'P2':'y', 'P5':'z'}, {'P1':'b', 'P5':'a', 'P3':'w'}]

    reports = [['P1:x', 'P2:y', 'P5:z'], ['P1:b', 'P5:a', 'P3:w']]

    print(reports)
    result = Solution().computeParameterValue(reports)
    print(result)