
class Solution():
    def missingWords(self, s, t):
        if s is None or len(s) == 0:
            return[]
        s = s.split()
        t = t.split()

        if len(t) == 0:
            return s

        i = j = 0

        ans = []
        while i < len(s) and j < len(t):
            if s[i] == t[j]:
                i += 1
                j += 1
            else:
                ans.append(s[i])
                i += 1
        while i < len(s):
            ans.append(s[i])
            i += 1
        return ans

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # a = map(int, input().rstrip().split())
    # b = map(int, input().rstrip().split())

    s = input().rstrip()
    t = input().rstrip()

    result = Solution().missingWords(s, t)
    print(result)