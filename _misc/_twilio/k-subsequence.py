import collections


class Solution(object):
    def ksub(self, A, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: int
        """
        if K == 0:
            return 0
        mp = collections.defaultdict(int)
        mp[0] = 1
        summ = 0
        ans = 0

        for i in range(len(A)):
            summ += A[i]
            remain = summ % K
            ans += mp[remain]
            mp[remain] += 1

        return ans

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    k = int(input().rstrip())
    n = int(input().rstrip())
    nums = []
    for _ in range(n):
        nums.append(int(input().rstrip()))


    result = Solution().ksub(nums, k)
    print(result)
