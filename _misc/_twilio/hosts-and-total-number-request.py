# req logs
# list the num of reqs from each hose
import collections


class Solution:
    def host(self, file_name, contents):
        counter = collections.Counter()
        for content in contents:
            data = content.split(' ')
            host = data[0]
            counter[host] += 1

        output = 'records_' + file_name
        with open(output, "w+") as f:
            for k, v in counter.items():
                string = k + ' ' + str(v) + '\n'
                f.write(string)

        return
if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # a = map(int, input().rstrip().split())
    # b = map(int, input().rstrip().split())

    file_name = "hosts_access_log_00.txt"
    with open(file_name) as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    print(len(content), content)
    Solution().host(file_name, content)