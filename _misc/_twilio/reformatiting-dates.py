class Solution:
    def reformat(self, strings):
        mp = {"Jan":'01', "Feb":'02', "Mar":'03', "Apr":'04', "May":"05", "Jun":'06',
              "Jul":'07', "Aug":'08', "Sep":'09', "Oct":'10', "Nov":'11', "Dec":'12'}
        ans = []

        for string in strings:
            date, month, year = string.split(' ')
            d = date[:-2]
            if len(d) == 1:
                d = '0' + d
            s = year + '-' + mp[month] + '-' + d
            ans.append(s)

        return ans


if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')


    n = int(input().rstrip())
    dates = []
    for _ in range(n):
        dates.append(input().rstrip())

    # dates = ['6th Jun 1933']
    result = Solution().reformat(dates)
    print(result)