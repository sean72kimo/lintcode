import collections


class Solution:
    def sorting(self, nums):
        mp = collections.Counter(nums)
        tmp = [(v, k) for k, v in mp.items()]
        print(tmp, type(tmp[0]))
        def mykey(x):
            return x[0], x[1]
        tmp.sort(key=mykey)
        ans = [y for x, y in tmp]
        return ans


if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nums = list(map(int, input().rstrip().split()))
    print(nums)
    result = Solution().sorting(nums)
    print(result)
