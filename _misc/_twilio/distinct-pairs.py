import os

class Solution:
    def countPairs(self, A, k):
        A.sort()
        left = 0
        right = len(A) - 1

        vst = set()
        # while left < right:
        #     v = A[left] + A[right]
        #     if v > k:
        #         right -= 1
        #     elif v < k:
        #         left += 1
        #     else:
        #         t = tuple(sorted([A[left], A[right]]))
        #         vst.add(t)
        #         left += 1
        #         right -= 1

        ans = 0
        while left < right:
            v = A[left] + A[right]
            if v > k:
                right -= 1
                while left < right and A[right] == A[right + 1]:
                    right -= 1
            elif v < k:
                left += 1
                while left < right and A[left] == A[left - 1]:
                    left += 1
            else:
                ans += 1
                left += 1
                right -= 1
                while left < right and A[right] == A[right + 1]:
                    right -= 1
                while left < right and A[left] == A[left - 1]:
                    left += 1


        return len(vst)

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # a = map(int, input().rstrip().split())
    # b = map(int, input().rstrip().split())

    n = int(input().rstrip())
    A = []
    for i in range(n):
        A.append(int(input().rstrip()))
    k = int(input().rstrip())
    print(type(A[0]), A)
    print(type(k), k)
    result = Solution().countPairs(A, k)
    print(result)
    # fptr.write(' '.join(map(str, result)))
    # fptr.write('\n')
    #
    # fptr.close()

# A = [1,3,46,1,3,9]
# k = 47
#
# A = [1,2,3,6,7,8,9,1]
# k = 10
# a = Solution().countPairs(A, k)
# print("ans:", a)



    # nm = raw_input().split()
    #
    # n = int(nm[0])
    #
    # m = int(nm[1])
    #
    # topic = []
    #
    # for _ in xrange(n):
    #     topic_item = raw_input()
    #     topic.append(topic_item)
    #
    # result = acmTeam(topic)