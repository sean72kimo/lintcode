"""
給字典 words = ["cat", "dog", "a"]

user 開始輸入，有點類似autocomplete
every user key stroke will return if user is typing a "word" or not
"""

class TrieNode:
    def __init__(self, ch):
        self.ch = ch
        self.isWord = False
        self.word = ""
        self.children = {}

    def __repr__(self):
        return "<{}>".format(self.ch)

class Trie:
    def __init__(self):
        self.root = TrieNode('#')

    def insert(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt

        node.isWord = True
        node.word = word

    def is_word(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                return False

            if nxt.isWord:
                return True

            node = nxt

        return node.isWord

class Solution:
    def __init__(self):
        self.store = ""
        self.trie = Trie()

    def build(self, words):

        for word in words:
            rev = word[::-1]
            self.trie.insert(rev)

    def is_word(self, ch):
        self.store += ch
        rev = self.store[::-1]

        return self.trie.is_word(rev)



words = ["cat", "dog", "a"]
sol = Solution()
sol.build(words)
ans = []
for ch in ["a", "c", "a", "t",]:
    ret = sol.is_word(ch)
    ans.append(ret)
print(ans)