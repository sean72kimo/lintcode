class Score:
    def num_of_ways(self, finalA, finalB, scores):
        scores.sort()
        self.mem = {}
        return self.dfs(finalA, finalB, scores)

    def dfs(self, A, B, nums):
        if (A, B) in self.mem:
            return self.mem[(A, B)]

        if A < 0 or B < 0:
            return 0

        if A == 0 and B == 0:
            return 1

        res = 0
        for i in range(len(nums)):
            res += self.dfs(A - nums[i], B, nums)
            res += self.dfs(A, B - nums[i], nums)

        self.mem[(A, B)] = res
        return res

finalA = 7
finalB = 5
scores = [2, 5, 7]


finalA = 1
finalB = 1
scores = [1]
score = Score()
a = score.num_of_ways(finalA, finalB, scores)
print("ans:", a)


"""
定义：
dp[i][j] : count of A reaches i score and B reaches j score

ini:
dp[0][0] = 1

for i in [0, targetA]:
    for j in [0, targetB]:
        for k in nums:
            dp[i][j] += dp[i - k][j] + dp[i][j - k], if i - k or j - k >= 0


"""
class Score_DP:
    def num_of_ways(self, targetA, targetB, nums):

        f = [[0 for _ in range(targetB+1)] for _ in range(targetA+1)]
        f[0][0] = 1

        for i in range(targetA+1):
            for j in range(targetB+1):
                if i == 0 and j == 0:
                    f[i][j] = 1
                    continue

                for k in nums:
                    if i - k >= 0:
                        f[i][j] += f[i - k][j]
                    if j - k >= 0:
                        f[i][j] += f[i][j - k]
        return f[targetA][targetB]


# finalA = 7
# finalB = 5
# scores = [2, 5, 7]


# finalA = 1
# finalB = 1
# scores = [1]
score = Score_DP()
a = score.num_of_ways(finalA, finalB, scores)
print("ans:", a)