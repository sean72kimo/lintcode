class Solution:
    def userHistory(self, input):
        A = input[0]
        B = input[1]
        
        m = len(A)
        n = len(B)
        
        f = [[0 for _ in range(n + 1)] for _ in range(m + 1)]
        size = 0
        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if A[i - 1] == B[j - 1]:
                    f[i][j] = f[i - 1][j - 1] + 1

                    if f[i][j] > size:
                        size = f[i][j]
                        end = (i,j)
                        print(end, size)

                else:
                    f[i][j] = 0
        
        print(A[end[0]-size : end[0]])
        return A[end[0]-size:end[0]]

input = [
             ["3234.html", "xys.html", "7hsaa.html"],
             ["3234.html", "sdhsfjdsh.html", "xys.html", "7hsaa.html"]
]
input = [
    ["page8.html", "page1.html", "page2.html", "page4.html", "page7.html", "page11.html", "page3.html"],
    ["page8.html", "page3.html", "page1.html", "page2.html", "page4.html", "page7.html"]
]
a = Solution().userHistory(input)
print("ans:", a)
# output = ["xys.html", "7hsaa.html"]
