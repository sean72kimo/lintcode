import collections
class Solution:
    def can_compose(self, words, s, ):
        if not words:
            return False

        counter = collections.Counter(s)

        for word in words:
            tmp = collections.Counter(word)
            flag = True

            for ch, cnt in tmp.items():
                if ch not in counter or cnt != counter[ch]:
                    flag = False
            if flag:
                return True
        return False


words = {"cat", "baby", "dog", "bird", "car", "ax"}
s = "tcabnihjs"

a = Solution().can_compose(words, s)
print("ans:", a)