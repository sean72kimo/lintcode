"""

https://paste.ubuntu.com/p/wrz5sh4bdh/
"""

class Solution:
    def can_win(self, s):
        if not s:
            return False
        self.ans = False
        self.pair_cnt = 0
        sorted_s = sorted(s)
        print(sorted_s)
        self.dfs(sorted_s, 0, [])
        return self.ans

    def dfs(self, s, start, path):
        if start == len(s):
            if self.pair_cnt == 1:
                self.ans = True
            return

        for i in range(start, len(s)):
            size = i+1-start
            if size > 3:
                break
            sub = s[start:i+1]
            print(sub)
            if size == 2 and self.pair(s, start):
                path.append(sub)
                self.pair_cnt += 1
                self.dfs(s, i+1, path)
                self.pair_cnt -= 1
                path.pop()

            if size == 3 and self.triple(s, start):
                path.append(sub)
                self.dfs(s, i+1, path)
                path.pop()

            if size == 3 and self.straight(s, start):
                path.append(sub)
                self.dfs(s, i+1, path)
                path.pop()

    def straight(self, s, i):
        return int(s[i+1]) == int(s[i]) + 1 and int(s[i+2]) == int(s[i+1]) + 1

    def triple(self, s, i):
        return s[i] == s[i+1] == s[i+2]

    def pair(self, s, i):
        return s[i] == s[i+1]
hand1 = "11123"          # True. 11 123
hand2 = "12131"          # True. Also 11 123. Tiles are not necessarily sorted.
hand3 = "11123455"       # True. 111 234 55
hand4 = "11122334"       # True. 11 123 234
hand5 = "11234"          # True. 11 234
hand6 = "123456"         # False. Needs a pair
hand7 = "11133355577"    # True. 111 333 555 77
hand8 = "11223344556677" # True. 11 234 234 567 567 among others
hand9 = "12233444556677" # True. 123 234 44 567 567
hand10 = "11234567899"   # False.
hand11 = "00123457"      # False.
hand12 = "0012345"       # False. A run is only three tiles
hand13 = "11890"         # False. 890 is not a valid run
hand14 = "99"            # True.
hand15 = "111223344"     # False.
a = Solution().can_win(hand3)
print("ans:", a)