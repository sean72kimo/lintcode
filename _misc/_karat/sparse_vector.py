class SparseVector:
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.values = {}
        for i, n in enumerate(nums):
            if not n :
                continue

            self.values[i] = n

    # Return the dotProduct of two sparse vectors
    def dotProduct(self, vec: 'SparseVector') -> int:
        if len(self.nums) != len(vec.nums):
            return -1
        ans = 0
        for idx, val in self.values.items():
            if idx in vec.values:
                ans += vec.values[idx] * self.values[idx]
        return ans