class TrieNode:
    def __init__(self, curr):
        self.children = {}
        self.isWord = False
        self.curr = curr
        self.word = ""

    def __repr__(self):
        return "<TrieNode>{}".format(self.curr)

class Trie:
    def __init__(self):
        self.root = TrieNode('#')

    def autocomplete(self, input):
        for word in input:
            self.insert(word)

    def insert(self, word):
        node = self.root

        for letter in word:
            next_node = node.children.get(letter)
            if not next_node:
                next_node = TrieNode(letter)
                node.children[letter] = next_node
            node = next_node

        node.isWord = True
        node.word = word

    def search(self, phrase):
        node = self.root
        for letter in phrase:
            node = node.children.get(letter)
            if not node:
                return False
        return node.isWord

    def process(self, phrase):
        # temp = list(phrase)
        res = []
        for i, letter in enumerate(phrase):

            if i - 1 >= 0 and (phrase[i].isupper() and phrase[i - 1].isupper()):
                res.append('.')
            res.append(letter)

        return ''.join(res)

    def startWith(self, phrase):
        string = self.process(phrase)

        node = self.root
        for letter in string:
            
            node = node.children.get(letter)
            if not node:
                return False

        suffix = []
        self.getAll(node, [], suffix)
        ans = []
        for itm in suffix:
            ans.append(phrase + itm)
        return ans

    def getAll(self, node, path, phrase):
        if len(node.children) == 0 :
            phrase.append(''.join(path[:]))
            return

        for ch in node.children:
            path.append(ch)
            self.getAll(node.children[ch], path, phrase)
            path.pop()



input = [
        "GraphView",
        "DataGraphView",
        "DataController",
        "GraphViewController",
        "DataScienceView"
]

trie = Trie()
trie.autocomplete(input)
# print(trie.startWith("Data"))
# ["DataGraphView", "DataController", "DataScienceView"]
print(trie.startWith("GVi"))
# ["GraphView", "GraphViewController"]
# print(trie.search("GraphController"))
# [""]
