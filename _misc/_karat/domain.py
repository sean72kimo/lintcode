import collections
class Solution:

    def clickCount(self, A):
        hsmap = collections.defaultdict(int)
        for domain, click in A:
            tmp = domain.split('.')[::-1]
            
            for i in range(1, len(tmp)+1):
                s = '.'.join(tmp[:i])
                hsmap[s] += int(click)
        
        ans = []
        for domain, click in hsmap.items():
            itm = [domain, str(click)]
            ans.append(itm)
            
        return ans



input = [
           ["google.com", "60"],
           ["yahoo.com", "50"],
           ["sports.yahoo.com", "80"]
]
a = Solution().clickCount(input)
print("ans:")
for r in a:
    print(r)
# output = [
#             ["com", "190"], 
#             ["google.com", "60"],
#             ["yahoo.com", "130"]
#             ["sports.yahoo.com", "80"]
# ]