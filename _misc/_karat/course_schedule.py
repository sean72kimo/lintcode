"""
Q1:

You are a developer for a university. Your current project is to develop a system
for students to find courses they share with friends. The university has a system
for querying courses students are enrolled in, returned as a list of (ID, course)
pairs.
Write a function that takes in a list of (student ID number, course name) pairs
and returns, for every pair of students, a list of all courses they share.

Sample Input:

student_course_pairs_1 = [
  ["58", "Software Design"],
  ["58", "Linear Algebra"],
  ["94", "Art History"],
  ["94", "Operating Systems"],
  ["17", "Software Design"],
  ["58", "Mechanics"],
  ["58", "Economics"],
  ["17", "Linear Algebra"],
  ["17", "Political Science"],
  ["94", "Economics"],
  ["25", "Economics"],
]

Sample Output (pseudocode, in any order):

find_pairs(student_course_pairs_1) =>
{
  [58, 17]: ["Software Design", "Linear Algebra"]
  [58, 94]: ["Economics"]
  [58, 25]: ["Economics"]
  [94, 25]: ["Economics"]
  [17, 94]: []
  [17, 25]: []
}

Additional test cases:

Sample Input:

student_course_pairs_2 = [
  ["42", "Software Design"],
  ["0", "Advanced Mechanics"],
  ["9", "Art History"],
]

Sample output:

find_pairs(student_course_pairs_2) =>
{
  [0, 42]: []
  [0, 9]: []
  [9, 42]: []
}

Q2:
Students may decide to take different "tracks" or sequences of courses in the
Computer Science curriculum. There may be more than one track that includes the
same course, but each student follows a single linear track from a "root" node to
a "leaf" node. In the graph below, their path always moves left to right.
Write a function that takes a list of (source, destination) pairs, and returns the
name of all of the courses that the students could be taking when they are halfway
through their track of courses.


/*
Students may decide to take different "tracks" or sequences of courses in the Computer Science curriculum. There may be more than one track that includes the same course, but each student follows a single linear track from a "root" node to a "leaf" node. In the graph below, their path always moves left to right.

Write a function that takes a list of (source, destination) pairs, and returns the name of all of the courses that the students could be taking when they are halfway through their track of courses.

Sample input:
all_courses = [
    ["Logic", "COBOL"],
    ["Data Structures", "Algorithms"],
    ["Creative Writing", "Data Structures"],
    ["Algorithms", "COBOL"],
    ["Intro to Computer Science", "Data Structures"],
    ["Logic", "Compilers"],
    ["Data Structures", "Logic"],
    ["Creative Writing", "System Administration"],
    ["Databases", "System Administration"],
    ["Creative Writing", "Databases"],
    ["Intro to Computer Science", "Graphics"],
]

Sample output (in any order):
          ["Data Structures", "Creative Writing", "Databases", "Intro to Computer Science"]

All paths through the curriculum (midpoint *highlighted*):

*Intro to C.S.* -> Graphics
Intro to C.S. -> *Data Structures* -> Algorithms -> COBOL
Intro to C.S. -> *Data Structures* -> Logic -> COBOL
Intro to C.S. -> *Data Structures* -> Logic -> Compiler
Creative Writing -> *Databases* -> System Administration
*Creative Writing* -> System Administration
Creative Writing -> *Data Structures* -> Algorithms -> COBOL
Creative Writing -> *Data Structures* -> Logic -> COBOL
Creative Writing -> *Data Structures* -> Logic -> Compilers

Visual representation:


                    ____________
                    |          |
                    | Graphics |
               ---->|__________|
               |                          ______________
____________   |                          |            |
|          |   |    ______________     -->| Algorithms |--\     _____________
| Intro to |   |    |            |    /   |____________|   \    |           |
| C.S.     |---+    | Data       |   /                      >-->| COBOL     |
|__________|    \   | Structures |--+     ______________   /    |___________|
                 >->|____________|   \    |            |  /
____________    /                     \-->| Logic      |-+      _____________
|          |   /    ______________        |____________|  \     |           |
| Creative |  /     |            |                         \--->| Compilers |
| Writing  |-+----->| Databases  |                              |___________|
|__________|  \     |____________|-\     _________________________
               \                    \    |                       |
                \--------------------+-->| System Administration |
                                         |_______________________|

Complexity analysis variables:

n: number of pairs in the input
"""

import collections


class Solution(object):
    def find_pairs(self, records):
        students = set()
        courseStudent = collections.defaultdict(list)
        for record in records:

            student = record[0]
            course = record[1]
            courseStudent[course].append(student)
            students.add(student)
        sharedCourse = collections.defaultdict(list)
        students = list(students)

        print(sharedCourse)
        for c, s in courseStudent.items():
            for i, s1 in enumerate(s):
                for s2 in s[i + 1:]:
                    pairs = tuple(sorted([s1, s2]))
                    sharedCourse[tuple(pairs)].append(c)

        for i, s1 in enumerate(students):
            for s2 in students[i + 1:]:
                pairs = tuple(sorted([s1, s2]))
                if pairs in sharedCourse:
                    continue
                sharedCourse[pairs] = []


        return sharedCourse

    def find_midway(self, courses):
        firstCourses = set()
        secondCourses = set()
        hashmap = collections.defaultdict(list)
        for c1, c2 in courses:
            firstCourses.add(c1)
            secondCourses.add(c2)
            hashmap[c1].append(c2)

        # 0 indegree
        starters = firstCourses - secondCourses

        self.paths = []
        res = set()

        def dfs(c, path):
            if c not in hashmap:
                self.paths.append(path[:])
                return

            for next_c in hashmap[c]:
                path.append(next_c)
                dfs(next_c, path)
                path.pop()
            return

        for s in list(starters):
            dfs(s, [s])
        for p in self.paths:
            print(p)
            idx = (len(p) - 1) // 2

            # trick: idx = (len(p) - 1) // 2 is equivalent as below
            # if len(p) % 2:
            #     i = len(p) // 2
            # else:
            #     i = len(p) // 2 - 1

            res.add(p[idx])
        return list(res)

sol = Solution()
student_course_pairs_1 = [
  ["58", "Software Design"],
  ["58", "Linear Algebra"],
  ["94", "Art History"],
  ["94", "Operating Systems"],
  ["17", "Software Design"],
  ["58", "Mechanics"],
  ["58", "Economics"],
  ["17", "Linear Algebra"],
  ["17", "Political Science"],
  ["94", "Economics"],
  ["25", "Economics"],
]
a = sol.find_pairs(student_course_pairs_1)
print("ans:")
for k in a:
    print(k, a[k])

all_courses = [
    ["Logic", "COBOL"],
    ["Data Structures", "Algorithms"],
    ["Creative Writing", "Data Structures"],
    ["Algorithms", "COBOL"],
    ["Intro to Computer Science", "Data Structures"],
    ["Logic", "Compilers"],
    ["Data Structures", "Logic"],
    ["Creative Writing", "System Administration"],
    ["Databases", "System Administration"],
    ["Creative Writing", "Databases"],
    ["Intro to Computer Science", "Graphics"],
]
a = sol.find_midway(all_courses)
exp = ["Data Structures", "Creative Writing", "Databases", "Intro to Computer Science"]
print("ans:", set(a)==set(exp), a)