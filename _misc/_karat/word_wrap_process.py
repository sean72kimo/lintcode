"""
给一个word list 和最大的长度，要求把这些word用 - 串联起来，但不能超过最大的长度。
"""

"""
We are building a word processor and we would like to implement a "reflow" functionality that also applies full justification to the text.
Given an array containing lines of text and a new maximum width, re-flow the text to fit the new width. Each line should have the exact specified width. If any line is too short, insert '-' (as stand-ins for spaces) between words as equally as possible until it fits.
Note: we are using '-' instead of spaces between words to make testing and visual verification of the results easier.

lines = [ "The day began as still as the",
          "night abruptly lighted with",
          "brilliant flame" ]

reflowAndJustify(lines, 24) ... "reflow lines and justify to length 24" =>

        [ "The--day--began-as-still",
          "as--the--night--abruptly",
          "lighted--with--brilliant",
          "flame" ] // <--- a single word on a line is not padded with spaces
"""

class Solution:
    def cascade(self, words, maxLen):
        words = words.split()
        ans = []
        tmp = []
        remain = maxLen
        for word in words:
            if remain - len(word) - 1 > 0:
                tmp.append(word)
                remain = remain - len(word) - 1
            else:
                ans.append('-'.join(tmp))
                tmp = []
                remain = maxLen
        ans.append('-'.join(tmp))
        return ans



    def reflow(self, lines, maxLen):
        words = []
        ans = []
        for line in lines:
            tmp = line.split()
            words.extend(tmp)

        ans = []
        tmp = []
        remain = maxLen
        for word in words:
            if remain - len(word) - 1 > 0:
                tmp.append(word)
                remain = remain - len(word) - 1
            else:
                ans.append(tmp)
                tmp = []
                remain = maxLen

        ans.append(tmp)
        res = []
        for row in ans:
            clone = row[:]
            size = 0
            for word in clone:
                size += len(word)

            while size < maxLen:
                for i in range(len(clone)):
                    clone[i] += '-'
                    size += 1
                    if size == maxLen:
                        break
            res.append(''.join(clone))

        return res

lines = "The day began as still as the night abruptly lighted with"
maxLen = 24


# a = Solution().cascade(lines, maxLen)
# print(a)


lines = [ "The day began as still as the",
          "night abruptly lighted with",
          "brilliant flame" ]
maxLen = 24
a = Solution().reflow(lines, maxLen)
print("ans:")
for r in a:
    print(r)
