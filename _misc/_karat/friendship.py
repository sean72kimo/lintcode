import collections
class Solution:
    def findFriend(self, E, F):
        friend = collections.defaultdict(set)
        for itm in F:
            a, b = itm.split(',')
            friend[a].add(b)
            friend[b].add(a)

        for itm in E:
            id, name, dept = itm.split(',')
            friend[id]

        print(friend)

    def findDeptFriend(self, E, F):

        friend = collections.defaultdict(set)
        for itm in F:
            a, b = itm.split(',')
            friend[a].add(b)
            friend[b].add(a)
        print(friend)
        for itm in E:
            id, name, dept = itm.split(',')
            friend[id]

        e2d = collections.defaultdict(str)
        d2e = collections.defaultdict(set)

        for itm in E:
            eid, name, dept = itm.split(',')
            d2e[dept].add(eid)
            e2d[eid] = dept
        # print(d2e)
        # print(e2d)
        ans = collections.defaultdict(list)

        for dept, employees in d2e.items():
            ans[dept].append(len(employees))

        for dept, employees in d2e.items():
            cnt = 0
            for e in employees:
                for f in friend[e]:
                    if f not in d2e[dept]:
                        cnt += 1
                        break
            ans[dept].append(cnt)
        print(ans)



employeesInput = [
      "1,Richard,Engineering",
      "2,Erlich,HR",
      "3,Monica,Business",
      "4,Dinesh,Engineering",
      "6,Carla,Engineering",
      "9,Laurie,Directors"
]

friendshipsInput = [
      "1,2",
      "1,3",
      "1,6",
      "2,4"
]
# Solution().findFriend(employeesInput, friendshipsInput)
Solution().findDeptFriend(employeesInput, friendshipsInput)
