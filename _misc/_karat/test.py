class Solution(object):

    def calculate(self, s):
        if len(s) == 0:
            return 0
        res = []
        sign = 1
        num = 0
        stack = [1]
        oper = {'+', '-'}

        
        for i, ch in enumerate(s):
            if ch.isdigit():
                num = num * 10 + int(ch)
            # (1-(4+5+2)-3)+(6+8)
            elif ch in oper:
                res.append(num * sign)
                num = 0

                if ch == '+':
                    sign = stack[-1] * 1
                if ch == '-':
                    sign = stack[-1] * -1

            elif ch == '(':
                stack.append(sign)
            elif ch == ')':
                stack.pop()
        
        res.append(num * sign)
        print(res)
        print(stack)
        return sum(res)

s = "(1+2+3)"
s = "-(1-(4+5+2))"
a = Solution().calculate(s)
print("ans:", a)



s = "super.!"
print(s.strip(".!?;"))