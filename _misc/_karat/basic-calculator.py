class Solution(object):
    def calculate2(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0
        res = 0
        sign = 1
        num = 0
        stack = []
        for i in range(len(s)):
            c = s[i]

            if c.isdigit():
                num = num * 10 + int(c)

            elif c == '+' or c == '-':
                res += sign * num
                if len(stack):
                    if c == '+':
                        sign = stack[-1] * 1
                    else:
                        sign = stack[-1] * -1
                else:
                    if c == '+':
                        sign = 1
                    else:
                        sign = -1

                num = 0
            elif c == '(':
                stack.append(sign)
            elif c == ')':
                stack.pop()

        res = res + sign * num
        return res


    def calculate1(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s)==0:
            return 0
        oper={'+', '-', '*', '/'}
        s = s.strip(" ")
        if s[0] == '-':
            op='-'
        else:
            op='+'

        stack=[]
        num=0
        for i, ch in enumerate(s):
            if ch.isspace():
                continue

            if ch.isdigit():
                num=num*10+int(ch)

            if ch in oper or i==len(s)-1:
                if op=='+':
                    stack.append(num)
                elif op=='-':
                    stack.append(-num)
                elif op=='*':
                    stack.append(stack.pop()*num)
                elif op=='/':
                    tmp=stack.pop()
                    if tmp<0:
                        tmp=-tmp
                        stack.append(-(tmp//num))
                    else:
                        stack.append(tmp//num)

                op = ch
                num = 0

        return sum(stack)

    def calculate3(self, s, code):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0
        oper = {'+', '-', '*', '/'}
        s = s.strip(" ")
        if s[0] == '-':
            op = '-'
        else:
            op = '+'

        stack = []
        num = 0
        char = None

        for i, ch in enumerate(s):
            if ch.isspace():
                continue

            if ch.isdigit():
                num = num * 10 + int(ch)

            if ch.isalpha():
                if ch in code:
                    num = code[ch]
                else:
                    char = ch

            if ch in oper or i == len(s) - 1:
                if op == '+':
                    if char:
                        stack.append('+' + char)
                    else:
                        stack.append(num)
                elif op == '-':
                    if char:
                        stack.append('-' + char)
                    else:
                        stack.append(-num)


                op = ch
                num = 0
                char = None


        tmp = []
        for itm in stack:
            if len(tmp) == 0:
                tmp.append(itm)
                continue
            if isinstance(itm, int):
                if isinstance(tmp[-1], int):
                    tmp.append(tmp.pop() + itm)
                else:
                    tmp.append(itm)
            else:
                tmp.append(itm)

        print(tmp)

        ans = ""
        for itm in tmp:
            if isinstance(itm, int):
                if itm >= 0:
                    string = '+' + str(itm)
                else:
                    string = str(itm)
            else:
                string = itm

            ans += string

        return ans

code = {'a':-5, 'b':2, 'c':3}
# s = 'a+b+c+1'
# s = 'a+b+1-d+c+1-100+d'
s = "(1-(4+5+2)-3)+(6+8)"
a = Solution().calculate3(s, code)
print("ans:", a)




expression = "6+9-12"


# You are building an educational website and want to create a simple calculator for students to use. The calculator will only allow addition and subtraction of positive integers.

# Given an expression string using the "+" and "-" operators like "5+16-2", write a function to parse the string and returns the result.



# Sample input/output:
# "6+9-12" => 3
# "1+2-3+4-5+6-7" => -2
class Solution:
    def calculate(self, s):
        if len(s) == 0:
            return 0
        oper = {'+', '-'}
        op = '+'
        stack = []
        num = 0
        for i, ch in enumerate(s):
            if ch.isdigit():
                num = num * 10 + int(ch)
            
            if ch in oper or i == len(s)-1:
                if op == '+':
                    stack.append(num)
                elif op == '-':
                    stack.append(-num)
                op = ch
                num = 0
                
        return sum(stack)

s = "600+9-12"
# a = Solution().calculate(s)
# print("ans:", a)

# We also want to allow parentheses in our input. Given an expression string using the "+", "-", "(", and ")" operators like "5+(16-2)", write a function to parse the string and evaluate the result.

# Sample input:
#     expression1 = "5+16-((9-6)-(4-2))" -4+2
#     expression2 = "22+(2-4)"
 
# Sample output:
#     evaluate(expression1) => 20
#     evaluate(expression2) => 20


class Solution2:
    def calculate(self, s):
        if len(s) == 0:
            return 0
        
        res = []
        stack = []
        sign = 1
        num = 0
        oper = {'+', '-'}
        
        for i, c in enumerate(s):
            if c.isdigit():
                num = num * 10 + int(c)
            elif c in oper:
                res.append(sign * num)
                if len(stack):
                    if c == '+':
                        sign = stack[-1] * 1
                    else:
                        sign = stack[-1] * -1
                else:
                    if c == '+':
                        sign = 1
                    else:
                        sign = -1
                num = 0
            
            elif c == '(':
                stack.append(sign)
            elif c == ')':
                stack.pop()

        res.append(num * sign)
        return sum(res)

s = "22+(2-4)"
s = "5+16-((9-6)-(4-2))"
s = "(5+2)-(3-1)"
# a = Solution2().calculate(s)
# print("ans:", a)
        
# We want to allow students to use variables when entering expressions in the calculator. In addition to the formula string, we’ll add a new input to our function that holds variables and their values: 
#     {"e": 8, "y": 7, "pressure": 5}
# and our string inputs now have a format like 
#       "(e+3)-temperature-pressure+2".


# Evaluate the formula result as fully as possible using the input variables. It is possible that not all variables have known values, in which case you should preserve them in the output.

# Sample input:
#     variables = {"e": 8, "y": 7, "pressure": 5}
#     expression = "(e+3)-temperature-pressure+2"
#  11 - tmpe -3
# Sample output:
#     "8-temperature"


# expression = "(e+3)-temperature-pressure+2"
# variables = {
#   "e": 8,
#   "y": 7,
#   "pressure": 5
# }
class Solution3:
    def calculate(self, s, code):
        if len(s) == 0:
            return 0
        # (e+3)-temperature-pressure+2
        res = []
        stack = []
        sign = 1
        num = 0
        oper = {'+', '-'}
        string = ""
        flag = False
        for i, c in enumerate(s):
            if c.isdigit():
                num = num * 10 + int(c)
            elif c.isalpha() and c in code:
                num = code.get(string, None)
                
            elif c.isalpha() and c not in code:
                
                string += c
                num = code.get(string, None)
                if num is None:
                    num = string
                    
                    
            elif c in oper:
                if isinstance(num, int):
                    res.append(sign * num)
                else:
                    if sign == 1:
                        res.append('+' + num)
                    else:
                        res.append('-' + num)
                
                if len(stack):
                    if c == '+':
                        sign = stack[-1] * 1
                    else:
                        sign = stack[-1] * -1
                else:
                    if c == '+':
                        sign = 1
                    else:
                        sign = -1
                num = 0
            
            elif c == '(':
                stack.append(sign)
            elif c == ')':
                stack.pop()

        res.append(num * sign)
        print(res)
        #return sum(res)
        
code = {
  "e": 8,
  "y": 7,
  "pressure": 5
}
# code = {
#   "e": 8,
#   "y": 7,
#   "p": 5
# }
expression = "(e+3)-t-p+2"

Solution3().calculate(expression, code)

