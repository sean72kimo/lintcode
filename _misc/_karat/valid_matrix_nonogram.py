"""
Valid Matrix
Q1: 给一个N * N的矩阵，判定是否是有效的矩阵。有效矩阵的定义是每一行或者每一列的数字都必须正好是
1到N的数。输出一个bool。

Q2: nonogram
A nonogram is a logic puzzle, similar to a crossword, in which the player is given
a blank grid and has to color it according to some instructions. Specifically,
each cell can be either black or white, which we will represent as 0 for black and
1 for white.
+------------+
| 1  1  1  1 |
| 0  1  1  1 |
| 0  1  0  0 |
| 1  1  0  1 |
| 0  0  1  1 |
+------------+

For each row and column, the instructions give the lengths of contiguous runs of
black (0) cells. For example, the instructions for one row of [ 2, 1 ] indicate
that there must be a run of two black cells, followed later by another run of one
black cell, and the rest of the row filled with white cells.

These are valid solutions: [ 1, 0, 0, 1, 0 ] and [ 0, 0, 1, 1, 0 ] and also [ 0, 0, 1, 0, 1 ]
This is not valid: [ 1, 0, 1, 0, 0 ] since the runs are not in the correct order.
This is not valid: [ 1, 0, 0, 0, 1 ] since the two runs of 0s are not separated by
1s.
Your job is to write a function to validate a possible solution against a set of
instructions. Given a 2D matrix representing a player's solution; and instructions
for each row along with additional instructions for each column; return True or
False according to whether both sets of instructions match.
Example instructions #1

matrix1 = [[1,1,1,1],
           [0,1,1,1],
           [0,1,0,0],
           [1,1,0,1],
           [0,0,1,1]]
rows1_1    =  [], [1], [1,2], [1], [2]
columns1_1 =  [2,1], [1], [2], [1]
validateNonogram(matrix1, rows1_1, columns1_1) => True
Example solution matrix:
matrix1 ->

Example instructions #2
(same matrix as above)
rows1_2    =  [], [], [1], [1], [1,1]
columns1_2 =  [2], [1], [2], [1]
validateNonogram(matrix1, rows1_2, columns1_2) => False

"""
class Solution:
    def valid_matrix(self, matrix):
        transpose = []
        for j in range(len(matrix[0])):
            tmp = []p
            for i in range(len(matrix)):
                tmp.append(matrix[i][j])
            transpose.append(tmp)

        def check(m):
            standard = set(list(range(1, len(m) + 1)))
            for line in m:
                if set(line) != standard:
                    return False
            return True

        return check(matrix) and check(transpose)


class Solution:
    def valid_nonogram(self, matrix, rows, cols):
        transpose = []
        for j in range(len(matrix[0])):
            tmp = []
            for i in range(len(matrix)):
                tmp.append(matrix[i][j])
            transpose.append(tmp)
        a = self.helper(matrix, rows)
        b = self.helper(transpose, cols)

        return a and b

    def helper(self, matrix, rows):
        for r in range(len(matrix)):
            string = ""
            for integer in matrix[r]:
                string += str(integer)

            if matrix[r].count(0) != sum(rows[r]):
                return False

            zeros = string.split('1')
            if not self.valid(zeros, rows[r]):
                return False

        return True

    def valid(self, zeros, spec):
        zeros = list(filter(lambda x: x != "", zeros))
        num_zeros = list(map(len, zeros))

        return num_zeros == spec

matrix =  [[1,1,1,1],
           [0,1,1,1],
           [0,1,0,0],
           [1,1,0,1],
           [0,0,1,1]]
rows1 = [], [1], [1,2], [1], [2],
columns1 = [2,1], [1], [2], [1],
e1 = True
ans = Solution().valid_nonogram(matrix, rows1, columns1)
print("ans:", ans==e1, ans)

a = [1,2]
b = [0,1,2,3]
print(a in b)
exit()
# rows2 = [], [], [1], [1], [1, 1]
# columns2 = [2], [1], [2], [1]
# e2 = False
#
# ans = Solution().valid_nonogram(matrix, rows2, columns2)
# print("ans:", ans==e2, ans)



matrix2 = [
[ 1, 1 ],
[ 0, 0 ],
[ 0, 0 ],
[ 1, 0 ]
]
rows = [[],[2], [2], [1]]
columns = [[1, 1], [3]]
e = False
ans = Solution().valid_nonogram(matrix, rows, columns)
print("ans:", ans==e, ans)
print("===============")

class SolutionConcise(object):
    def valid_matrix(self, matrix):
        transpose = []
        for j in range(len(matrix[0])):
            tmp = []
            for i in range(len(matrix)):
                tmp.append(matrix[i][j])
            transpose.append(tmp)

        def check(m):
            standard = set(list(range(1, len(m) + 1)))
            for line in m:
                if set(line) != standard:
                    return False
            return True

        return check(matrix) and check(transpose)

    def valid_nonogram(self, matrix, rows, cols):
        if not matrix or len(matrix) != len(rows) or len(matrix[0]) != len(cols):
            return False

        def check(m, r):
            for i in range(len(m)):
                str_row = [str(x) for x in m[i]]
                zeros = ''.join(str_row).split('1')
                zeros = list(filter(lambda x: x != "", zeros))
                num_zeros = list(map(len, zeros))
                if num_zeros != r[i]:
                    return False
            return True

        transpose = []
        for j in range(len(matrix[0])):
            tmp = []
            for i in range(len(matrix)):
                tmp.append(matrix[i][j])
            transpose.append(tmp)

        return check(matrix, rows) and check(transpose, cols)


sol = Solution()
# matrix1 = [[1, 1, 3], [2, 3, 1], [3, 1, 2]]
# matrix2 = [[1, 2, 3], [2, 3, 1], [3, 1, 2]]
# print(sol.valid_matrix(matrix1))
# print(sol.valid_matrix(matrix2))
matrix1 = [[1, 1, 1, 1],
           [0, 1, 1, 1],
           [0, 1, 0, 0],
           [1, 1, 0, 1],
           [0, 0, 1, 1]]
rows1_1 = [[], [1], [1, 2], [1], [2]]
columns1_1 = [[2, 1], [1], [2], [1]]
print(sol.valid_nonogram(matrix1, rows1_1, columns1_1))


rows1_2 = [[], [], [1], [1], [1,1]]
columns1_2 =  [[2], [1], [2], [1]]
print(sol.valid_nonogram(matrix1, rows1_2, columns1_2))


matrix2 = [
[ 1, 1 ],
[ 0, 0 ],
[ 0, 0 ],
[ 1, 0 ]
]
rows = [[],[2], [2], [1]]
columns = [[1, 1], [3]]
print(sol.valid_nonogram(matrix2, rows, columns))