
"""
// The people who buy ads on our network don't have enough data about how ads are working for
// their business. They've asked us to find out which ads produce the most purchases on their website.

// Our client provided us with a list of user IDs of customers who bought something on a landing page
// after clicking one of their ads:

// # Each user completed 1 purchase.
// completed_purchase_user_ids = [
//   "3123122444","234111110", "8321125440", "99911063"]

// And our ops team provided us with some raw log data from our ad server showing every time a
// user clicked on one of our ads:
// ad_clicks = [
//  #"IP_Address,Time,Ad_Text",
//  "122.121.0.1,2016-11-03 11:41:19,Buy wool coats for your pets",
//  "96.3.199.11,2016-10-15 20:18:31,2017 Pet Mittens",
//  "122.121.0.250,2016-11-01 06:13:13,The Best Hollywood Coats",
//  "82.1.106.8,2016-11-12 23:05:14,Buy wool coats for your pets",
//  "92.130.6.144,2017-01-01 03:18:55,Buy wool coats for your pets",
//  "92.130.6.145,2017-01-01 03:18:55,2017 Pet Mittens",
//]

//The client also sent over the IP addresses of all their users.

//all_user_ips = [
//  #"User_ID,IP_Address",
//  "2339985511,122.121.0.155",
//  "234111110,122.121.0.1",
//  "3123122444,92.130.6.145",
//  "39471289472,2001:0db8:ac10:fe01:0000:0000:0000:0000",
//  "8321125440,82.1.106.8",
//  "99911063,92.130.6.144"
//]

// Write a function to parse this data, determine how many times each ad was clicked,
// then return the ad text, that ad's number of clicks, and how many of those ad clicks
// were from users who made a purchase.


// Expected output:
// Bought Clicked Ad Text
// 1 of 2  2017 Pet Mittens
// 0 of 1  The Best Hollywood Coats
// 3 of 3  Buy wool coats for your pets
"""
import collections
class Solution:
    def conversion_rate(self, all_user_ips, ad_clicks, completed_purchase_user_ids):
        uid_to_ip = {}
        to_uid = {}
        purchased_ip = []

        for data in all_user_ips:
            uid, ip = data.split(',')
            to_uid[ip] = uid
            if uid in completed_purchase_user_ids:
                purchased_ip.append(ip)

        ads_click_cnt = collections.Counter()
        ads_purchased_cnt = collections.Counter()
        for data in ad_clicks:

            ip, time, ads = data.split(',')
            ads_click_cnt[ads] += 1
            if ip in purchased_ip:
                ads_purchased_cnt[ads] += 1


        ans = []
        for ad in ads_click_cnt:
            clicks = ads_click_cnt[ad]
            purhcased = ads_purchased_cnt[ad]
            msg = f"{purhcased} of {clicks}  {ad}"
            ans.append(msg)

        return ans



completed_purchase_user_ids = ["3123122444","234111110", "8321125440", "99911063"]

ad_clicks = [
  #"IP_Address,Time,Ad_Text",
  "122.121.0.1,2016-11-03 11:41:19,Buy wool coats for your pets",
  "96.3.199.11,2016-10-15 20:18:31,2017 Pet Mittens",
  "122.121.0.250,2016-11-01 06:13:13,The Best Hollywood Coats",
  "82.1.106.8,2016-11-12 23:05:14,Buy wool coats for your pets",
  "92.130.6.144,2017-01-01 03:18:55,Buy wool coats for your pets",
  "92.130.6.145,2017-01-01 03:18:55,2017 Pet Mittens",
]

all_user_ips = [
  #"User_ID,IP_Address",
  "2339985511,122.121.0.155",
  "234111110,122.121.0.1",
  "3123122444,92.130.6.145",
  "39471289472,2001:0db8:ac10:fe01:0000:0000:0000:0000",
  "8321125440,82.1.106.8",
  "99911063,92.130.6.144"
]

a = Solution().conversion_rate(all_user_ips, ad_clicks, completed_purchase_user_ids)
print("ans:", a)