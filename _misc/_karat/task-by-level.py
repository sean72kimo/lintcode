import collections
import time
class Solution:
    def taskOrder(self, input):
        
        ind = collections.defaultdict(int)
        act = set()
        graph = collections.defaultdict(set)
        
        for start, end in input:
            graph[start].add(end)
        
        for start, end in input:
            ind[end] += 1
            act.add(start)
            act.add(end)
        
        que = []
        vst = set()
        ans = [que]
        for itm in act:
            if itm not in ind:
                que.append(itm)
                vst.add(itm)
        
        while que:
            new_q = []
            
            for work in que:
                for nei in graph[work]:
                    ind[nei] -= 1

                    if ind[nei] == 0:
                        new_q.append(nei)
                        vst.add(nei)
            if new_q:
                ans.append(new_q)
            que = new_q

        return ans


    def find_stale_steps(self, tasks, last):
        graph = collections.defaultdict(set)
        ind = collections.defaultdict(int)
        act = set()
        
        for start , end in tasks:
            graph[start].add(end)
            ind[end] += 1
            act.add(start)
            act.add(end)
        
        zeroInd = []
        vst = set()
        for a in act:
            #if len(graph[a]) == 0:
            #    graph[a].add("#")
            if a not in ind:
                zeroInd.append(a)
                vst.add(a)
        print(graph)
        t2t = collections.defaultdict(int)
        for task, t in last:
            tt = self.strip_time(t)
            t2t[task] = tt
        path = []
        for a in zeroInd:
            self.dfs(graph, a, path, t2t, False)
        print(path)
    
    def dfs(self, graph, curr, path, t2t, flag):
        #if curr == '#':
        #    print("------path:", path[:])
        #    return
        
        for nextTask in graph[curr]:
            if not self.done(nextTask, curr, t2t) and not flag:
                flag = True

            if flag:
                path.append(nextTask)
                self.dfs(graph, nextTask, path, t2t, flag)
                flag = False
            else:
                self.dfs(graph, nextTask, path, t2t, flag)


    def done(self, nextT, currT, t2t):
        # if time1 > time2 means nextT has been performed
        
        time1 = t2t[nextT]
        time2 = t2t[currT]
        
        #print(nextT, time1)
        #print(currT, time2)
        #print("========================")
        
        if not time1:
            return False

        for i in range(len(time1)):
            if time1[i] > time2[i]: 
                return True
        return False
            
    def strip_time(self, t):
        ret = []
        t = t.split('-')
        year = t[0][:4]
        month = t[0][4:6]
        date = t[0][6:]
        hour = t[1][0:2]
        minute = t[1][2:4]
        ret.extend([int(year), int(month), int(date), int(hour), int(minute)])
        return ret
        
        
        
tasks = [
    ["clean", "mapper"],
    ["metadata", "statistics"],
    ["mapper", "update"],
    ["update", "statistics"],
    ["clean", "metadata"],
    ["mapper", "reducer"],
    ["metadata", "timestamp"]
]

last_execution_times = [
  ["clean", "20170302-1129"],
  ["mapper", "20170302-1155"],
  ["update", "20170302-1150"],
  ["statistics", "20170302-1153"],
  ["metadata", "20170302-1130"],
  ["reducer", "20170302-1540"]
]

a = Solution().taskOrder(tasks)
print("ans:", a)
a = Solution().find_stale_steps(tasks, last_execution_times)

