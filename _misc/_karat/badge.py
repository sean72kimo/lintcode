import collections


class HitCounter:
    def __init__(self, name):
        self.hit_time = [0] * 60
        self.hit_count = [0] * 60
        self.name = name

    def __repr__(self):
        return f"<HitCounter> {self.name}"

    def hit(self, timestamp: int) -> None:
        t = timestamp % 60

        last_hit_time = self.hit_time[t]
        if timestamp - last_hit_time < 60:
            self.hit_time[t] = timestamp
            self.hit_count[t] += 1
        else:
            self.hit_time[t] = timestamp
            self.hit_count[t] = 1

    def getHits(self, timestamp: int) -> int:
        ans = []
        for i, time in enumerate(self.hit_time):
            diff = timestamp - time
            if diff < 60:
                hh = str(time // 60)
                mm = str(time % 60).zfill(2)
                ans.append(hh+mm)
        return ans

class Solution:
    def withinHour(self, t1, t2):
        
        if t2 < 100:
            t2 = t2 + 2400
             
        if t2 <= t1 + 100:
            
            return True 

    def unusualTime(self, R):
        
        def mykey(x):
            return x[1]
        R.sort(key = mykey)

        hsmap = collections.defaultdict(list)
        ans = {}
        
        for i in range(len(R)):

            name = R[i][0]
            time = R[i][1]

            if len(hsmap[name]) == 0:
                hsmap[name].append(time)

            else:
                if self.withinHour(hsmap[name][-1], time):
                    hsmap[name].append(time)
                else:
                    if len(hsmap[name]) < 3:
                        hsmap[name] = [time]
        for key, val in hsmap.items():
            if len(val) >= 3:
                ans[key] = val
        return ans

    def enterExit(self, R):
        enter_without_exit = set()
        exit_without_enter = set()
        
        hsmap = collections.defaultdict(list)
        for i in range(len(R)):
            name = R[i][0]
            act = R[i][1]

            if len(hsmap[name]) == 0 and act == "exit":
                exit_without_enter.add(name)
            elif len(hsmap[name]) == 0 and act == "enter":
                hsmap[name].append("enter")
            elif hsmap[name][-1] == "enter" and act == "enter":
                enter_without_exit.add(name)
            elif hsmap[name][-1] == "enter" and act == "exit":
                hsmap[name].pop()

        for name, act in hsmap.items():
            if len(act):
                if act[0] == "enter":
                    enter_without_exit.add(name)
                else:
                    exit_without_enter.add(name)
        return enter_without_exit, exit_without_enter

    """
    design hit counter
    https://leetcode.com/problems/design-hit-counter/
    """
    def multiple_access(self, records):
        mp = {}
        ans = {}
        for name, time in records:
            hh = int(time[:2]) * 60
            mm = int(time[2:])
            timestamp = hh + mm
            print(name, timestamp)
            if name not in mp:
                mp[name] = HitCounter(name)
            hit_counter = mp[name]
            hit_counter.hit(timestamp)
            hit_record = hit_counter.getHits(timestamp)
            if len(hit_record) > 1:
                ans[name] = hit_record
        return ans

sol = Solution()
"""
Given a list of people who enter and exit, find the people who entered without
their badge and who exited without their badge.
"""
records = [
   ["Martha",   "exit"],
   ["Paul",     "enter"],
   ["Martha",   "enter"],
   ["Martha",   "exit"],
   ["Jennifer", "enter"],
   ["Paul",     "enter"],
   ["Curtis",   "enter"],
   ["Paul",     "exit"],
   ["Martha",   "enter"],
   ["Martha",   "exit"],
   ["Jennifer", "exit"],
]

# print("ans:", sol.enterExit(records))
"""
一小时内access多次
给 list of [name, time], time is string format: '1300' //下午一点
return: list of names and the times where their swipe badges within one hour. if there are multiple intervals that satisfy the condition, return any one of them.
name1: time1, time2, time3...
name2: time1, time2, time3, time4, time5...
example:
input: [
['James', '1300'], ['Martha', '1600'], ['Martha', '1620'], ['Martha', '1530']
] 
output: {
'Martha': ['1600', '1620', '1530']
}
"""
input = [
    ['James', '1300'], ['Martha', '1600'], ['Martha', '1620'], ['Martha', '1530']
]
output = {
    'Martha': ['1600', '1620', '1530']
}
# print("ans:", sol.multiple_access(input))

"""
unusualTime ?
"""
time_records = [
  ["Paul", 1355],
  ["Jennifer", 1910],
  ["John", 830],
  ["Paul", 1315],
  ["John", 835],
  ["Paul", 1405],
  ["Paul", 1630],
  ["John", 855],
  ["John", 915],
  ["John", 930],
  ["Jennifer", 1335],
  ["Jennifer", 730],
  ["John", 1630]
]

print("ans:", sol.unusualTime(time_records))