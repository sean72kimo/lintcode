class Type:
    def __init__(self, time, event):
        self.time = time
        self.event = event
    def __lt__(self, other):
        if self.time != other.time:
            return self.time < other.time
        return self.event < other.event
    def __repr__(self):
        return "<Time{}>{}".format(self.flag, self.time)
    
class Solution:
    def addMeetings(self, input, time):
        temp = []
        start = 1
        end = 0
        for inp in input:
            temp.append(Type(inp[0], start))
            temp.append(Type(inp[1], end))
        
        temp.append(Type(time[0], start))
        temp.append(Type(time[1], end))
        temp.sort()
        
        for i in range(1, len(temp)):
            if temp[i].event == temp[i-1].event:
                return False
        # cnt = 0
        # for event in temp:
        #     if event.event == start:
        #         cnt += 1
        #     elif event.event == end:
        #         cnt -= 1
        #     if cnt > 1:
        #         return False
        return True
    
    def avalibility(self, input):
        def mykey(x):
            return x[0]
        
        input.sort(key = mykey)
        temp =[input[0]]
        
        START = 0
        END = 1
        for i in range(1, len(input)):
            inp = input[i]
            if inp[START] <= temp[-1][END]:
                s = temp[-1][START]
                e = max(temp[-1][END], inp[END])
                temp.pop()
                temp.append([s, e])
            else:
                temp.append(inp)
        
        
        
        ans = [[0, temp[0][START]]]
        for i in range(1, len(temp)):
            ans.append([temp[i-1][END],temp[i][START]])
        return ans
        
input = [[1300, 1500], [930, 1200],[830, 845]]
time = [820, 830]
time = [1450, 1500]
print("ans:",Solution().addMeetings(input, time))

# input = [[1300, 1500], [930, 1200],[830, 845], [1450, 1600]]
# print("ans:",Solution().avalibility(input))