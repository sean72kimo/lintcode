from lib.binarytree import TreeNode
import collections
class Solution:
    def createGraph(self, A):
        graph = collections.defaultdict(set)
        ind = collections.defaultdict(int)
        
        all_nodes = set()
        for parent, child in A:
            graph[parent].add(child)
            ind[child] += 1
            all_nodes.add(parent)
            all_nodes.add(child)
        return self.indegree(A, graph, ind, all_nodes)
        
    def indegree(self, A, graph, ind, all_nodes):
        ans = []
        oneF = []
        zeroF = []
        for node in all_nodes:
            if ind[node] == 0:
                zeroF.append(node) 
            if ind[node] == 1:
                oneF.append(node)
        ans.append(oneF)
        ans.append(zeroF)
        return ans
        
    def hasLCA(self, A, n1, n2):
        graph = collections.defaultdict(set)
        
        for start, end in A:
            graph[end].add(start)
        
        que = collections.deque([n1, n2])
        vst = {n1, n2}
        while que:
            node = que.popleft()
            for nei in graph[node]:
                if nei in vst:
                    return nei
                que.append(nei)
                vst.add(nei)

    def farthest(self, A, n):
        graph = collections.defaultdict(set)
        for start, end in A:
            graph[end].add(start)
            
        if n not in graph:
            return n
        
        que = [n]
        vst = {n}
        dis = 0
        maxx = 0
        farthest = []
        
        while que:
            new_q = []
            dis += 1

            for node in que:
                if len(graph[node]) == 0:
                    if dis == maxx:
                        farthest.append(node)
                    if dis > maxx:
                        maxx = dis
                        farthest = [node]
                
                for nei in graph[node]:
                    if nei in vst:
                        continue
                    new_q.append(nei)
                    vst.add(nei)
                    
            que = new_q
        return farthest, maxx

"""
  1    2    3
/  \  /      \
4    5        6
                \
                  7
"""


input =  [[1,4], [1,5], [2,5], [3,6], [6,7]]
input =  [[1,3], [3,6], [2,3], [4,5], [5,6],[5,7],[4,8],[8,9]]

sol = Solution()
# print("1/0 parent:",sol.createGraph(input))
a = sol.hasLCA(input, 6, 7)

print("LCA:",a)
print("farthest:", sol.farthest(input, 6))
# n1 = TreeNode(1)
# n2 = TreeNode(2)
# n3 = TreeNode(3)
# n4 = TreeNode(4)
# n5 = TreeNode(5)
# n6 = TreeNode(6)
# n7 = TreeNode(7)
# 
# n1.left = n4
# n1.right = n5
# n2.left = n5
# n3.right = n6
# n6.right = n7

"""
# Suppose we have some input data describing a graph of relationships between parents and children over multiple generations. The data is formatted as a list of (parent, child) pairs, where each individual is assigned a unique integer identifier.

# For example, in this diagram, 3 is a child of 1 and 2, and 5 is a child of 4:

#   11
#    \
# 1   2   4
#  \ /   / \
#   3   5   8
#    \ / \   \
#     6   7   10

# Write a function that, for a given individual in our dataset, returns their earliest known ancestor -- the one at the farthest distance from the input individual. If there is more than one ancestor tied for "earliest", return any one of them. If the input individual has no parents, the function should return null (or -1).

# Sample input and output:


# findEarliestAncestor(parentChildPairs, 8) => 4
# findEarliestAncestor(parentChildPairs, 7) => 4
# findEarliestAncestor(parentChildPairs, 6) => 11
# findEarliestAncestor(parentChildPairs, 1) => null or -1

import collections


class Solution:
    def indegree(self, pairs):
        if len(pairs) == 0:
            return []

        ind = collections.defaultdict(int)
        all_node = set()
        for s, e in pairs:
            ind[e] += 1
            all_node.add(s)
            all_node.add(e)

        zero = []
        one = []
        for node in all_node:
            if node not in ind:
                zero.append(node)
            elif ind[node] == 1:
                one.append(node)
        return [zero, one]

    def build_graph(self, A):
        graph = collections.defaultdict(set)

        for parent, child in A:
            graph[child].add(parent)

        return graph

    def findEarliestAncestor(self, parentChildPairs, n):
        graph = self.build_graph(parentChildPairs)

        que = [n]
        vst = {n}
        dis = 0
        maxx = 0
        farthest = []

        if n not in graph:
            return -1

        while que:
            new_q = []
            dis += 1

            for node in que:
                if len(graph[node]) == 0:
                    if dis == maxx:
                        farthest.append(node)
                    elif dis > maxx:
                        maxx = dis
                        farthest = [node]

                for nei in graph[node]:
                    if nei in vst:
                        continue
                    new_q.append(nei)
                    vst.add(nei)

            que = new_q
        if len(farthest) == 0:
            return -1
        return farthest

    def has_lca(self, parentChildPairs, n1, n2):
        graph = self.build_graph(parentChildPairs)

        que = collections.deque([n1])
        vst = {n1}

        # find parents of n1
        while len(que):
            curr = que.popleft()
            for nei in graph[curr]:
                if nei in vst:
                    continue
                que.append(nei)
                vst.add(nei)

        que2 = collections.deque([n2])
        vst2 = {n2}
        while len(que2):
            curr = que2.popleft()
            if curr in vst:
                return True

            for nei in graph[curr]:
                if nei in vst2:
                    continue
                que2.append(nei)
                vst2.add(nei)
        return False


#   11
#    \
# 1   2   4
#  \ /   / \
#   3   5   8
#    \ / \   \
#     6   7   10
parent_child_pairs = [
    (1, 3), (2, 3), (3, 6), (5, 6),
    (5, 7), (4, 5), (4, 8), (8, 10), (11, 2)
]

# findEarliestAncestor(parentChildPairs, 8) => 4
# findEarliestAncestor(parentChildPairs, 7) => 4
# findEarliestAncestor(parentChildPairs, 6) => 11
# findEarliestAncestor(parentChildPairs, 1) => null or -1
sol = Solution()
a = sol.findEarliestAncestor(parent_child_pairs, 7)
print("ans:", a)
"""