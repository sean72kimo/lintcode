import collections
class Solution:
    """
    there is an image filled with 0s and 1s. There is at most one rectangle in this image filled with 0s, find the rectangle.
    Output could be the coordinates of top-left and bottom-right elements of the rectangle, or top-left element, width and height.
    """
    def findOneRectangle(self, A):
        n = len(A)
        m = len(A[0])
        for i in range(n):
            for j in range(m):
                if A[i][j] == 0:
                    upper_left = self.find_upper_left(A, (i,j))
                    bottom_right = self.find_bottom_right(A)
                    ans = [upper_left + bottom_right]
                    return ans

    def find_upper_left(self, A, start):
        x, y = start
        n = len(A)
        m = len(A[0])

        for i in range(x,n):
            for j in range(y,m):
                if A[i][j] == 0:
                    return [i,j]

    def find_bottom_right(self, A):
        n = len(A)
        m = len(A[0])
        for i in range(n-1,-1,-1):
            for j in range(m-1,-1,-1):
                if A[i][j] == 0:
                    return [i,j]

    """
    for the same image, it is filled with 0s and 1s. It may have multiple rectangles filled with 0s. 
    The rectangles are separated by 1s. Find all the rectangles.
    """
    def findAllRectangle(self, A):
        n = len(A)
        m = len(A[0])
        vst = [[False for _ in range(m)] for _ in range(n)]
        ans = []
        for i in range(n):
            for j in range(m):

                if A[i][j] == 0 and not vst[i][j]:
                    bottom_right = self.bfs_search(A, i, j, vst)
                    ans.append([i,j] + bottom_right)

        return ans

    def bfs_search(self, A, i, j, vst):
        n = len(A)
        m = len(A[0])
        
        dxy = [[0,1],[1,0]]
        que = collections.deque([(i, j)])
        vst[i][j] = True
        #A[i][j] = 3

        
        while que:
            i,j = que.popleft()
            
            for dx, dy in dxy:
                ni = i + dx
                nj = j + dy
                if not ( 0 <= ni < n and 0 <= nj < m):
                    continue
                if A[ni][nj] != 0:
                    continue
                #A[ni][nj] = 3
                vst[ni][nj] = True
                que.append((ni, nj))
        
        return [i,j]

    def __init__(self):
        self.dxy = [[0,1],[1,0],[0,-1],[-1,0]]
    """
    the image has random shapes filled with 0s, separated by 1s. Find all the shapes. 
    Each shape is represented by coordinates of all the elements inside.
    """
    def numDistinctIslands(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0
        
        n = len(grid)
        m = len(grid[0])
        ans = set()
        vst = [[False for _ in range(m)] for _ in range(n)]
        for i in range(n):
            for j in range(m):
                if grid[i][j] == 1:
                    if vst[i][j]:
                        continue
                    path=[]
                    self.dfs(i, j, i, j, grid, path, vst)
                    ans.add(tuple(path))
                    print(path)
        return len(ans)
                    
    def dfs(self, i0, j0, i, j, grid, path, vst):
        
        n = len(grid)
        m = len(grid[0])
        
        if not (0 <= i < n and 0 <= j < m):
            return
        
        if vst[i][j]:
            return
        
        if grid[i][j] == 0:
            return
        
        #grid[i][j] = -1
        vst[i][j] = True
        path.append((i-i0, j-j0))
        
        for dx, dy in self.dxy:
            ni = i + dx
            nj = j + dy
            self.dfs(i0, j0, ni, nj, grid, path, vst)



input = [
    [1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 0, 1],
    [1, 1, 1, 0, 0, 1],
    [1, 1, 1, 1, 1, 1]
]

a=Solution().findAllRectangle(input)
print("ans:", a)
for r in input:
    print(r)
    

input = [
    [1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1]
]

# a = Solution().findOneRectangle(input)
# print("ans:", a)
