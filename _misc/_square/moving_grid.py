# ## Square
# Write a function that given the following information:
# - Square grid of size N
# - Initial starting coordinates
# - A list of steps (Up, Down, Left, Right)
# *Example*
# - 5x5 Grid
# - Starting coordinate (0, 1)
# - Steps: [L, D, D, R]
# ┌───┬───┬───┬───┬───┐
# │ . ← S │   │   │   │
# ├─↓─┼───┼───┼───┼───┤
# │ . │   │   │   │   │
# ├─↓─┼───┼───┼───┼───┤
# │ . → D │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# └───┴───┴───┴───┴───┘
# Answer: (2, 1)
# *Example*
# - 5x5 Grid
# - Starting coordinate (0, 1)
# - Steps: [L, L, U, U]
# ┌───┬───┬───┬───┬─↑─┐
# ← . ← S │   │   │ . ←
# ├───┼───┼───┼───┼───┤
# │.  │.  │.  │.  │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │ D │
# ├───┼───┼───┼───┼─↑─┤
# │   │   │   │   │ . │
# └───┴───┴───┴───┴─↑─┘
# Answer: (3, 4)
# *Example*
# - 5x5 Grid
# - Starting coordinate (0, 1)
# - Obstacle at (0, 2)
# - Steps: [R, R, D]
# ┌───┬───┬───┬───┬───┐
# │   │ S → X │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# └───┴───┴───┴───┴───┘
# Answer: (0, 1)
# --------------------------------------
# --------------------------------------
# Write a function that given a square grid, and a collection of available steps,
# an initial starting coordinate, and a destination coordinate;
# determine a possible path required to navigate from the starting coordinate
# to the destination coordinate using a subset of the available steps, each available step can
# only be used once.
# Again, obstacles may be present and will block any potential paths. Boundary conditions should
# be treated in the same way as the previous exercise.
# *Example*
# - 5x5 Grid
# - Starting coordinate (0, 1)
# - Destination coordinate (4, 3)
# - Available Steps: [U, D, U, R, R, D, L, L, L]
# ┌───┬─↑─┬───┬───┬───┐
# │ X │ S │ X │   │   │
# ├───┼───┼───┼───┼───┤
# │ X │ X │ X │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │ . → . → . │   │
# ├───┼─↑─┼───┼─↓─┼───┤
# │ X │ . │ X │ D │   │
# └───┴───┴───┴───┴───┘
# Possible answer: [U, U, R, R, D]
# Possible answer: [U, U, L, L, L, D]



import collections


class Solution:
    def moving_grid(self, start, n, operations, obstacle):

        dxy = {
            "R": [0, 1],
            "D": [1, 0],
            "L": [0, -1],
            "U": [-1, 0],
        }

        i, j = start
        for d in operations:
            i = i + dxy[d][0]
            j = j + dxy[d][1]

            i = i % 5
            j = j % 5

            if (i, j) in obstacle:
                i -= dxy[d][0]
                j -= dxy[d][1]

                i = i % 5
                j = j % 5
                break

        return (i,j)

    def find_path(self, start, end, n, available_steps, obstacle):

        dxy = {
            "R": [0, 1],
            "D": [1, 0],
            "L": [0, -1],
            "U": [-1, 0],
        }
        path = []
        que = collections.deque([start])
        vst = {(start[0], start[1])}
        steps = collections.Counter(available_steps)
        print(que)
        while que:
            i, j = que.popleft()
            if [i,j] == end:
                return path
            for d, coord in dxy.items():
                x = i
                y = j

                if d not in steps:
                    continue
                x = x + coord[0]
                y = y + coord[1]
                if x >= 5:
                    x = x % 5
                if y >= 5:
                    y = y % 5
                if x < 0:
                    x = n + x
                if y < 0:
                    y = n + y

                if (x, y) in obstacle:
                    continue

                if (x, y) in vst:
                    continue

                steps[d] -= 1
                if steps[d] == 0:
                    steps.pop(d)

                path.append(d)
                vst.add((x, y))
                que.append((x, y))

        return []


    def find_path_dfs(self, start, end, n, available_steps, obstacle):
        self.ans = set()
        self.dxy = {
            "R": [0, 1],
            "D": [1, 0],
            "L": [0, -1],
            "U": [-1, 0],
        }
        path = []
        print(start)
        # self.dfs(start, end, n, available_steps, obstacle, set(), [])
        steps = collections.Counter(available_steps)
        self.res = []
        self.dfs2(start, end, steps, obstacle, [], 1)
        print(self.res)
        return self.ans

    def dfs2(self, curr, end, steps, obstacle, path, layer):
        if curr == end:
            self.ans.add(''.join(path))
            self.res.append(''.join(path))
            return

        for d, [dx, dy] in self.dxy.items():
            if d not in steps or steps[d] <= 0:
                continue
            i , j = curr
            i += dx
            j += dy

            i, j = self.convert(i, j)
            if (i,j) in obstacle:
                continue

            # print(f"layer {layer} add: ({i}:{j})")

            path.append(d)
            obstacle.add((i, j))
            steps[d] -= 1
            nxt = [i, j]

            self.dfs2(nxt, end, steps, obstacle, path, layer+1)

            # print(f"layer {layer} remove: ({i}:{j})")

            steps[d] += 1
            path.pop()
            obstacle.remove((i, j))

            print(f"excpet layer {layer} remove: ({i}:{j})")


    def dfs(self, curr, end, n, available_steps, obstacle, vst, path):
        if curr == end:
            self.ans.add(''.join(path))
            return

        for p in range(len(available_steps)):
            if p in vst:
                continue
            d = available_steps[p]
            i, j = curr
            i += self.dxy[d][0]
            j += self.dxy[d][1]
            i, j = self.convert(i, j)

            if (i, j) in obstacle:
                continue

            obstacle.add((i, j))
            path.append(d)
            vst.add(p)

            self.dfs([i,j], end, n, available_steps, obstacle, vst, path)

            vst.remove(p)
            path.pop()
            obstacle.remove((i, j))

    def convert(self, x, y):
        if x >= 5:
            x = x % 5
        if y >= 5:
            y = y % 5
        if x < 0:
            x = 5 + x
        if y < 0:
            y = 5 + y
        return x, y
start = [0,1]
n = 5
operations = ["L", "D", "D", "R"]
operations = ["L", "L", "U", "U"]
operations = ["R", "R", "D"]
available_steps = ["U", "D", "U", "R", "R", "D", "L", "L", "L"]
end = [4,3]
obstacle = {(0,0),(0,2),(1,0),(1,1),(1,2),(4,0),(4,2)}
a = Solution().find_path_dfs(start, end, n, available_steps, obstacle)
print("ans:", a)


# ┌───┬─↑─┬───┬───┬───┐
# │ X │ S │ X │   │   │
# ├───┼───┼───┼───┼───┤
# │ X │ X │ X │   │   │
# ├───┼───┼───┼───┼───┤
# │   │   │   │   │   │
# ├───┼───┼───┼───┼───┤
# │   │ . → . → . │   │
# ├───┼─↑─┼───┼─↓─┼───┤
# │ X │ . │ X │ D │   │
# └───┴───┴───┴───┴───┘