# You're trying to read a number from an old seven-segment display pocket calculator (like this one: https://imgur.com/v3OwJ6U)
# but some segments are broken and won't turn on. You know that the digits that form the number have increasing values,
# but because the display is broken, you're not sure what the number is and quickly realize that
# there can be more than one possibility.
# Find a way to encode what segments are on and off, and write a routine that
# receives that information as input and returns a sequence of integers representing all possible numbers that
# the calculator is trying to display. Generate only numbers with digits of increasing values (from left to right).
# For example, given the input depicted in this image: https://imgur.com/a/8H7iLAe,
# the routine should return 00, 06, 08, 66, 68, 88. It's OK to skip the leading zero as the values must be integers, not strings.
# A list of all seven-segment numbers is available at: https://imgur.com/RqeNRn1
import collections


class Solution:
    def get_all_possibile(self, broken):
        mp = {
            0: "1111110",
            1: "1100000",
            3: "1111001",
            4: "1100101",
            5: "0110111",
            6: "0111111",
            7: "1100110",
            8: "1111111",
            9: "1110111"
        }
        possible = collections.defaultdict(list)
        for broken_number in broken:

            on = set()
            for i, v in enumerate(broken_number):
                if v == '1':
                    on.add(i)
            flag = True
            for k, encode in mp.items():
                for i in on:
                    if encode[i] != '1':
                        flag = False
                if flag:
                    possible[broken_number].append(k)
                flag = True
        self.ans = []
        all_possible = list(possible.values())
        self.dfs(all_possible, 0, [])
        return self.ans

    def dfs(self, all_possible, start, path):
        if start == len(all_possible):
            self.ans.append(''.join(list(map(str, path))))
            return

        for num in all_possible[start]:
            if path and num < path[-1]:
                continue
            path.append(num)
            self.dfs(all_possible, start+1, path)
            path.pop()






broken = ["0111100", "0111110", "0000100"]
# broken = ["0111100", "0111110"]
a = Solution().get_all_possibile(broken)
print("ans:", a)
