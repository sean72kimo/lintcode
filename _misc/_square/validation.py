from abc import ABC, abstractmethod


class BaseValidator(ABC):
    @abstractmethod
    def check(self, attr):
        pass


class ValidateOneLargeRed(BaseValidator):
    def __init__(self):
        super().__init__()

    def check(self, balls):
        check = False
        for ball in balls:
            if ball.color == 'red' and ball.size == 'large':
                check = True
        if not check:
            raise

class NoTwoRed(Exception):
    def __init__(self):
        super().__init__("No two red")

class ValidateTwoLargeRed(BaseValidator):
    def __init__(self):
        super().__init__()

    def check(self, balls):
        check = 0
        for ball in balls:
            if ball.color == 'red' and ball.size == 'large':
                check += 1
        if check < 2:
            raise NoTwoRed

class NoLargeBlue(Exception):
    def __init__(self):
        super().__init__("No Large Blue")


class ValidateLargeBlue(BaseValidator):
    def __init__(self):
        super().__init__()

    def check(self, balls):
        check = False
        for ball in balls:
            if ball.color == 'blue' and ball.size == 'large':
                check = True
        if not check:
            raise NoLargeBlue()

class Ball:
    def __init__(self, size, color):
        self.size = size
        self.color = color


class Solution:

    def take_input(self, balls, validation_suite):
        for validator in validation_suite:
            validator.check(balls)





large_red = Ball("large", "red")
small_blue = Ball("small", "blue")
balls = [large_red, small_blue]

validation_suite = [ValidateTwoLargeRed()]
sol = Solution()
sol.take_input(balls, validation_suite)
