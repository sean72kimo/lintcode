import collections


class Solution:
    def pagination(self, total, visible, curr):
        if curr <= 0 or curr > total:
            return -1
        upper = curr
        lower = curr

        while 1 <= lower or upper <= total:
            cnt = upper - lower + 1
            if cnt == visible:
                break
            lower = lower - 1 if lower > 1 else lower - 0
            upper = upper + 1 if upper < total else upper + 0



        ans = collections.deque([])
        for i in range(lower, upper+1):
            if i == curr:
                ans.append(str(i)+"*")
            else:
                ans.append(str(i))

        if ans[0] not in ["1", "1*"]:
            ans.appendleft('<')

        if ans[-1] not in [str(total), str(total)+'*']:
            ans.append('>')

        return ' '.join(ans)


data = {
    "total": 10,
    "visible": 7,
    "curr": 5
}

data = {
    "total": 10,
    "visible": 4,
    "curr": 1
}
a = Solution().pagination(**data)
print("ans:", a)