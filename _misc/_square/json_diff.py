# https://www.1point3acres.com/bbs/thread-905422-1-1.html
import json

jsonString = '{"a":54, "b": 28}'
obj = json.loads(jsonString)
print(type(obj))

string = json.dumps(obj)
print(string)

left = {
  "a":5,
  "b":6,
  "c":5,
  "x":9
}

right = {
  "d": 8,
  "b": {"x": 2},
  "x": 9,
  "a": 8

}

