# ######### PART 1 #########
# We've been asked by an up-and-coming airport to help them quickly and efficiently build daily departure schedules!
# Part 1: Straightforward Schedule
# Before the start of each day, all airliners are required to send in their planned departures for that day. For each flight, the airliners provide three pieces of information: a flight id, a planned departure time, and the number of passengers on each flight. We want to determine if a given schedule is possible (i.e. no scheduling conflicts), and if so, we want to be able to build and return the schedule, ordered from earliest to latest departure.
# Our airport only has one runway. A scheduling conflict is determined by the existence of more than one flight with the same departure time.
# A flight has an id, time, passengers.
# As an example, if this was the set of flights requested for the day:
# United_123, 05:30, 180
# Alaska_475, 14:50, 92
# Alaska_472, 16:00, 48
# Southwest_447, 07:30, 176
# Southwest_847, 15:10, 151
# We would want our scheduler to return an ordered schedule like the following:
# [
#   <United_123, 05:30, 180>,
#   <Southwest_447, 07:30, 176>,
#   <Alaska_475, 14:50, 92>,
#   <Southwest_847, 15:10, 151>,
#   <Alaska_472, 16:00, 48>,
# ]
# ######### PART 2 #########
# Part 2: Rescheduling Flights
# What happens when the day doesn't go according to plan? I'm sure we've all experience flight delays, which can be caused by a myriad of reasons: weather, maintenance, forgetting to refuel the plane - yes, this happened to me :), etc.
# Let's add functionality to let our airliners reschedule any existing flight and re-order the schedule accordingly. If a reschedule attempt creates a schedule conflict, we will reject the reschedule attempt and return the original schedule.
# ######### PART 3 #########
# Part 3: Rescheduling Flights with Conflicts
# So far, we've built our airport scheduling policy to reject ‍‌‌‌‍‍‍‌‌‌‌‌‌‌‌‌‍‌‍any requests that would result in a scheduling conflict. Let's be more accomodating! If an airliner requests a rescheduling that results in a timing conflict, we should reschedule that flight with the next available time slot.



from functools import cmp_to_key
from sortedcontainer import SortedDict

class Flight:
    def __init__(self, airline, time, count):
        self.time = time
        self.airline = airline
        self.count = count
    def __lt__(self, other):
        return self.time > other.time
    def __repr__(self):
        return f"<Flight {self.airline} departures at {self.time}>"

class Solution:
    def __init__(self):
        self.flight_to_info = {}
        self.events = SortedDict()

    def update_schedule(self, flight, time):
        if flight not in self.flight_to_info:
            raise
        old_time = self.flight_to_info[flight][1]
        name, old_time, count = self.events.pop(old_time)
        t = self.get_unix_time(time)
        self.events[t] = (name, t, count)
        self.flight_to_info[flight][1] = t

        return

    def update_and_resolve_schedule(self, flight, time):
        print(self.flight_to_info)
        if flight not in self.flight_to_info:
            raise
        t = self.get_unix_time(time)

        while t in self.events:
            t += 1

        f = self.flight_to_info[flight]
        f[1] = t
        self.events[t] = f
        print(self.events)

    def get_unix_time(self, clock_time):
        hh,mm = clock_time.split(':')
        t = int(hh) * 60 + int(mm)
        return t

    def get_clock_time(self, unix_time):
        hh = str(unix_time // 60).zfill(2)
        mm = str(unix_time % 60).zfill(2)
        return f"{hh}:{mm}"

    def generate_schedule(self, schedule):

        tmp = SortedDict()
        for each in schedule:

            data = each.split(',')
            t = self.get_unix_time(data[1])
            flight = [data[0], t, data[2]]
            self.flight_to_info[data[0]] = flight
            if t in self.events:
                raise
            self.events[t] = flight

        return list(self.events.values())
        # sorted_events = sorted(events)
        # print(sorted_events)
        # print(events)



schedule =  [
    "Alaska_475,14:50,92",
    "Eva,06:30,92",
    "United_123,05:30,180",
    "Alaska_472,16:00,48",
    "Southwest_447,07:30,176",
    "Southwest_847,15:10,151",
]
sol = Solution()



a = sol.generate_schedule(schedule)
print("ans:", a)
# sol.update_schedule("Eva", "01:00")
sol.update_and_resolve_schedule("Eva", "05:30")

a = [1,2,3]
def mycmp(self, other):

    if self > other:
        return 1
    elif self < other:
        return -1
    else:
        return 0
b = sorted(a, key=cmp_to_key(mycmp))
