"""
https://www.1point3acres.com/bbs/thread-901074-1-1.html
以下是完整原题，有三部分：
ICE FIELDS AND BOULDER MAZE
INPUT
10 12 # height, width
0 0   # starting row,col A
8 4   # exit row,col.    B
2 1   # boulder row,col
(下面的都是boulder的位置哈，用#表示)
5 2   # ...
6 3
9 4
9 6
1 8
1 10
7 10
3 11
4 11
OUTPUT（类似这样的，但是下面跟上面的input不是对应的，大家凑活看看吧）
+------------+
|A                 |
|        #       #       |
|          #       |
|           #                |
|           #                |
|  #                |
|   #                |
|          #                |
|     B            |
|    # #              |
+------------+
part1:
maze = createMaze(inputList)
printMaze(maze)

PART 2: Moving on the ICE 注意 #标志就想障碍物， 会被block，
possible_moves(maze, cur_row, cur_col) -> list of moves
e.g.
(0,0) -> (0,11), (9,0)
3,3 -> (0,3), (3,0), (3, 10), (5,3)

PART 3: can A reach B?你可以调用part2的api
canAreachB(maze) -> bool
"""


class Solution:
    def __init__(self):
        self.grid = None

    def create_maze(self, height, width, start, end, boulders):
        self.grid = [[1 for _ in range(width)] for _ in range(height)]

        for i, j in boulders:
            self.grid[i][j] = '#'

    def print_maze(self):
        for row in self.grid:
            print(row)
        print("===========")
