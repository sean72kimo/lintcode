"""
Rock climbing is a sport where you climb rocks.
In climbing up a rock face/wall, you take predetermined routes.
Each route has a rating corresponding to its difficulty,
which lets climbers compare and attempt climbs that are well-suited to their respective skill levels.
Let's create some routes!
There are two important things to record: the route's name, and its difficulty rating.
There are other things like location we might want to add later.
Let's make the routes "Awesome", "5.8" and "Epic", "5.9",
and add a way to print them in the format "Awesome: 5.8".
"Mike's Route: 5.2" easier than "Awesome: 5.8"?

Ratings are "5.0", "5.1", "5.2",... "5.8", "5.9" with each being more difficult than the last.
Given 2 routes, we need the ability to compare 2 routes by difficulty
Ratings are "5.0", "5.1", "5.2",... "5.8", "5.9", "5.10a", "5.10b", "5.10c", "5.10d", "5.11a", ..., "5.15d".
Given 2 routes, we need the ability to compare 2 routes by difficulty
"""
class Route:
    def __init__(self, difficulty):

        for i in range(len(difficulty)-1, -1, -1):
            if difficulty[i].isdigit():
                break

        number = difficulty[:i+1]
        integer, decimal = number.split('.')
        letters = difficulty[i+1:]
        self.integer = int(integer)
        self.decimal = int(decimal)
        self.difficulty = difficulty
        self.number = number
        self.letters = letters


    def __lt__(self, other):
        if self.integer == self.integer and self.decimal == other.decimal:
            return self.letters < other.letters

        if self.integer == other.integer:
            return self.decimal < other.decimal

        return self.integer < other.integer

    def __repr__(self):
        return f"<Route {self.difficulty}>"

r_58 = Route("5.8")
r_510a = Route("5.10a")
r_510b = Route("5.10b")

print(r_510a < r_510b)


routes = [r_58, r_510a, r_510b]
routes.sort(reverse=True)
print(routes)