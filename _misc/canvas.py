class Rect:
    def __init__(self, name, i, j, h, w):
        self.name = name
        self._i = i
        self._j = j
        self.w = w
        self.h = h

    @property
    def i(self):
        return self._i

    @i.setter
    def i(self, i):
        self._i = i

    @property
    def j(self):
        return self._j

    @j.setter
    def j(self, j):
        self._j = j

    def __repr__(self):
        return f"<Rect {self.name}>"

class Canvas:
    def __init__(self, m, n):
        self.canvas = None
        self.m = m
        self.n = n
        self.set_canvas()
        self.rect = {}
        self.order = []

    def set_canvas(self):
        self.canvas = [['.' for _ in range(self.m)] for _ in range(self.n)]

    def print_canvas(self):

        for row in self.canvas:
            print(row)
        print("========================")

    def add_rect(self, rect):
        # check input legal
        if rect.name in self.rect:
            return -1
        self.order.append(rect)
        self.rect[rect.name] = (rect, len(self.order)-1)

        i = rect.i
        j = rect.j
        # check rect on canvas
        if not (0 <= i < i + rect.h <= len(self.canvas[0])):
            return -1

        if not (0 <= j < j + rect.w <= len(self.canvas)):
            return -1

        for x in range(i, i+rect.h):
            for y in range(j, j+rect.w):
                self.canvas[x][y] = rect.name

        self.print_canvas()


    def move_rect(self, name, i, j):
        # check i,j legal and name in self.rect
        if name not in self.rect:
            return -1
        rect, idx = self.rect[name]

        # check rect on canvas
        if not (0 <= i < i + rect.h <= len(self.canvas[0])) or not (0 <= j < j + rect.w <= len(self.canvas)):
            self.resize(rect, i, j)

        rect.i = i
        rect.j = j

        self.order.pop(idx)
        self.order.append(rect)
        self.rect[name] = (rect, len(self.order)-1)

        self.set_canvas()
        for rect in self.order:
            i = rect.i
            j = rect.j
            for x in range(i, i + rect.h):
                for y in range(j, j + rect.w):
                    self.canvas[x][y] = rect.name
        self.print_canvas()

    def resize(self, rect, i, j):
        height = i + rect.h
        width = j + rect.w
        print(height, width)
        # n * m matrix , n row, m col
        self.m = max(self.m, width)
        self.n = max(self.n, height)

        self.set_canvas()
        self.print_canvas()




canvas = Canvas(7, 4)
canvas.print_canvas()
# rect_a = Rect('a', 0, 0, 2, 2)
# canvas.add_rect(rect_a)
#
rect_b = Rect('b', 1, 1, 1, 2)
canvas.add_rect(rect_b)
print(canvas.order)
#
canvas.move_rect('b', 10, 0)
# print(canvas.order)

