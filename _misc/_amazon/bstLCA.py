from lib.binarytree import BinaryTree

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

class Solution:

    def createBST(self, values):
        root = None
        for i in values:
            root = self.insert(root, i)

        return root

    def insert(self, root, i):
        if root is None:
            return TreeNode(i)
        if root.val < i:
            root.right = self.insert(root.right, i)
        else:
            root.left = self.insert(root.left, i)

        return root

    def findDistance(self, root, p, q):
        if root is None or p is None or q is None:
            return -1

        dist1 = self.getDistFromRoot(root, p)
        dist2 = self.getDistFromRoot(root, q)
        if dist1 == -1 or dist2 == -1:
            return -1
        else:
            lca = self.lca(root, p, q)
            dist3 = self.getDistFromRoot(root, lca)
            return dist1 + dist2 - 2 * dist3

    def getDistFromRoot(self, root, node):
        if not root:
            return -1
        if root.val == node.val:
            return 0
        if root.val > node.val:
            dis = self.getDistFromRoot(root.left, node)
            if dis != -1:
                return dis + 1
        else:
            dis = self.getDistFromRoot(root.right, node)
            if dis != -1:
                return dis + 1

    def _getDistFromRoot(self, root, val):
        if not root:
            return -1
        if root.val == val:
            return 0
        if root.val > val:
            dis = self._getDistFromRoot(root.left, val)
            return dis + 1
        else:
            dis = self._getDistFromRoot(root.right, val)
            return dis + 1

    def lca(self, root, p, q):
        if not root:
            return None

        if root == p or root == q:
            return root

        left = self.lca(root.left, p, q)
        right = self.lca(root.right, p, q)

        if left and right:
            return root
        elif left:
            return left
        elif right:
            return right
        else:
            return None

    def bstLca(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        while root:
            if root.val > q.val and root.val > p.val:
                root = root.left
            elif root.val < q.val and root.val < p.val:
                root = root.right
            else:
                return root

values = [5, 6, 3, 1, 2, 4, 7, 8, 9, 10]
root = Solution().createBST(values)
print(BinaryTree().serialize(root))

a = Solution()._getDistFromRoot(root, 10)
print("ser:", a)


