class Solution:
    # @param {Connection[]} connections given a list of connections
    # include two cities and cost
    # @return {Connection[]} a list of connections from results
    def lowestCost(self, connections):
        # Write your code here
        cityID = {}
        n = 0
        for conn in connections:
            if conn.city1 not in cityID:
                cityID[conn.city1] = n
                n += 1
            if conn.city2 not in cityID:
                cityID[conn.city2] = n
                n += 1

        self.father = [i for i in range(n)]

        def mykey(x):
            return (x.cost, x.city1, x.city2)

        connections.sort(key=mykey)

        ans = []
        for conn in connections:
            id1 = cityID[conn.city1]
            id2 = cityID[conn.city2]

            if not self.union(id1, id2):
                ans.append(conn)

        if len(ans) != n - 1:
            return []
        return ans

    def find(self, x):
        if self.father[x] == x:
            return self.father[x]
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        ra = self.find(a)
        rb = self.find(b)

        if ra != rb:
            self.father[ra] = rb
            return False
        return True