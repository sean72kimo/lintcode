from heapq import heapify, heappop, heappush
class Movie:
    def __init__(self, id, rating, similarMovies):
        self.id = id
        self.rating = rating
        self.similarMovies = similarMovies
    def __lt__(self, other):
        return self.rating < other.rating

class Solution:
    """
    @param rating: the rating of the movies
    @param G: the realtionship of movies
    @param S: the begin movie
    @param K: top K rating
    @return: the top k largest rating moive which contact with S
    """
    def topKMovie(self, rating, G, S, K):
        # Write your code here

        vst = set()
        movies = []
        for i in range(len(rating)):
            movie = Movie(i, rating[i], [])
            movies.append(movie)

        for i, nei in enumerate(G):
            for m in nei:
                movies[i].similarMovies.append(movies[m])

        movie = movies[S]
        # minHeap = [(movie.rating, movie)]
        minHeap = []
        que = [movie]
        vst = {movie.id}

        while que:
            curMov = que.pop(0)
            for nextMov in curMov.similarMovies:
                if nextMov.id in vst or nextMov.id == S:
                    continue
                vst.add(nextMov.id)
                que.append(nextMov)
                item = (nextMov.rating, nextMov)
                heappush(minHeap, item)
                if len(minHeap) > K:
                    heappop(minHeap)
        ans = []
        while minHeap:
            m = heappop(minHeap)[1]
            ans.append(m.id)

        ans.reverse()

        return ans

    def __init__(self):
        self.father = []

    def topKMovie2(self, rating, G, S, K):
        # Write your code here
        if len(rating) == 0 or len(G) == 0:
            return []

        n = len(G)
        self.father = [ i for i in range(n)]
        # print('init fater', self.father)
        for i, group in enumerate(G):
            for g in group:
                self.union(i, g)


        heap = []
        ans = []
        root = self.find(S)
        for i in range(n):
            rr = self.find(i)
            if rr == root and i != S:
                heap.append((-rating[i], i))

        heapify(heap)

        while K > 0:
            rating, movie = heappop(heap)

            ans.append(movie)
            K -= 1

        return ans

    def find(self, x):
        if self.father[x] == x:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        roota = self.find(a)
        rootb = self.find(b)
        if roota != rootb:
            self.father[rootb] = roota

rating = [10, 20, 30, 40]
G = [[1, 3], [0, 2], [1], [0]]
S = 0
K = 2
#         0   1   2   3   4   5   6   7   8
rating = [10, 20, 30, 40, 50, 60, 70, 80, 90]
G = [[1, 4, 5], [0, 2, 3], [1, 7], [1, 6, 7], [0], [0], [3], [2, 3], []]
S = 5
K = 3

# rating = [24542, 3439, 17967, 10116, 12531, 23504, 22382, 15074, 27854, 25768, 11502, 31373, 15500, 23406, 24536, 16474, 25280, 18493, 10348, 12764]
# G = [[9, 18, 12], [15, 9, 13, 2, 16], [11, 14, 8, 7, 16, 1], [19, 13], [16, 14, 5, 7, 12, 11], [18, 13, 4, 14], [9, 11], [11, 2, 16, 14, 4, 12], [2, 10, 9], [0, 14, 8, 6, 1, 17], [8, 19, 18, 14], [7, 2, 12, 19, 16, 6, 14, 4, 13], [11, 17, 15, 16, 7, 4, 0], [5, 16, 1, 11, 3], [18, 17, 2, 4, 9, 5, 7, 10, 11], [12, 1, 19], [4, 12, 7, 13, 11, 2, 1], [14, 12, 9], [14, 5, 0, 19, 10], [3, 18, 10, 11, 15]]
# S = 5
# K = 4
a = Solution().topKMovie(rating, G, S, K)
print("ans:", a)
b = Solution().topKMovie2(rating, G, S, K)
print("ans:", b)
print("cmp:", a == b)


# a = [0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99]
# b = [0, 1, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 33, 34, 35, 36, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51, 52, 53, 55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 76, 77, 78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 96, 97, 99]
#
# aa = set(a)
# bb = set(b)
# print(len(a), len(aa))
# print(len(b), len(bb))
#
# print(aa ^ bb)
