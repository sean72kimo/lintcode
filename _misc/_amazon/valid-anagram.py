import collections
class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        s_count = collections.defaultdict(int)


        for ch in s:
            s_count[ch] += 1

        for ch in t:
            if ch not in s_count:
                return False

            s_count[ch] -= 1

            if s_count[ch] == 0:
                del s_count[ch]

        return len(s_count) == 0
