import collections
class Solution1:
    def KDistinctCharacter(self, s, k):
        if len(s) == 0:
            return []
        
        left = 0
        right = 0
        hsmap = {}
        ans = []
        while right < len(s):

            while s[right] in hsmap:
                hsmap[s[left]] -= 1
                if hsmap[s[left]] == 0:
                    hsmap.pop(s[left])
                left += 1
                
            hsmap[s[right]] = 1

            if right - left + 1 == k:
                sub = s[left:right+1]
                if sub not in ans:
                    ans.append(sub)
                hsmap[s[left]] -= 1
                if hsmap[s[left]] == 0:
                    hsmap.pop(s[left])
                left += 1
            
            right += 1
            
        return ans
s = 'awaglknagawunagwkwagl'
k = 4

a = Solution1().KDistinctCharacter(s, k)
test = ["wagl", "aglk", "glkn", "lkna", "knag", "gawu", "awun", "wuna", "unag", "nagw", "agwk", "kwag"]
print("ans:", a, a==test)


