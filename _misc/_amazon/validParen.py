class Solution:
    def validParenthesis(self, s):
        if s is None or len(s) == 0:
            return True

        stack = []
        for c in list(s):
            if c == '(' or c == '[' or c == '{':
                stack.append(c)
            if c == ')':
                if len(stack) == 0 or stack.pop() != '(':
                    return False
            if c == ']':
                if len(stack) == 0 or stack.pop() != '[':
                    return False
            if c == '}':
                if len(stack) == 0 or stack.pop() != '{':
                    return False
        return len(stack) == 0

s = "()[]{}"
a = Solution().validParenthesis(s)
print("ans:", a)