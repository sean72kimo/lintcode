
import heapq
class Point:
    def __init__(self, x, y, ox, oy):
        self.x = x
        self.y = y
        self.ox = ox
        self.oy = oy

    def __lt__(self, other):
        d1 = abs(self.x - self.ox) ** 2 + abs(self.y - self.oy) ** 2
        d2 = abs(other.x - other.ox) ** 2 + abs(other.y - other.oy) ** 2

        if d1 == d2:
            if self.x == self.x:
                return self.y > other.y
            return self.x > other.x
        return d1 > d2

class Solution2:
    def kClosest(self, points, origin, k):
        ans = []
        ox = origin[0]
        oy = origin[1]
        maxHeap = []
        for x, y in points:
            p = Point(x, y, ox, oy)
            heapq.heappush(maxHeap, p)
            if len(maxHeap) > k:
                heapq.heappop(maxHeap)

        while maxHeap:
            p = heapq.heappop(maxHeap)
            ans.append([p.x, p.y])
        ans.reverse()
        return ans


points = [[4, 6], [4, 7], [4, 4], [2, 5], [1, 1]]
origin = [0, 0]
k = 3
a = Solution().kClosest(points, origin, k)
print("ans:", a)
# return [[1,1],[2,5],[4,4]]
# 解题思路参考：
# 解决办法就是heap（priorityqueue），坑：要自己写comparator，注意比较距离的
# 公式是x*x+y*y.




import heapq
class Type:
    def __init__(self, x, y, ox, oy):
        self.x = x
        self.y = y
        self.ox = ox
        self.oy = oy

    def __lt__(self, other):
        d1 = abs(self.x - self.ox) ** 2 + abs(self.y - self.oy) ** 2
        d2 = abs(other.x - other.ox) ** 2 + abs(other.y - other.oy) ** 2

        if d1 == d2:
            if self.x == other.x:
                return self.y > other.y
            return self.x > other.x
        return d1 > d2


class Solution:
    def kClosest(self, points, origin, k):
        ans = []
        ox = origin.x
        oy = origin.y
        maxHeap = []
        for point in points:
            p = Type(point.x, point.y, ox, oy)
            heapq.heappush(maxHeap, p)
            if len(maxHeap) > k:
                heapq.heappop(maxHeap)

        while maxHeap:
            p = heapq.heappop(maxHeap)
            ans.append([p.x, p.y])
        ans.reverse()
        return ans

