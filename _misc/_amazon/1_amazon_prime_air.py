import collections


class Solution:
    def operationDistance(self, maxDistance, f, r):
        def mykey(x):
            return (x[1], x[0])
        f.sort(key = mykey)
        r.sort(key = mykey)

        print(f)
        print(r)
        i = 0
        j = len(r) - 1
        target = float('-inf')
        ans = []
        while i < len(f) and j >= 0:
            d = f[i][1] + r[j][1]
            if d == maxDistance:
                target = maxDistance
                break

            if d < maxDistance:
                target = max(target, d)
                i += 1
            else:
                j -= 1
        print(target)
        mp = collections.defaultdict(list)

        for idx, dist in r:
            mp[dist].append(idx)

        for idx, dist in f:
            t = target - dist
            if t in mp:
                ii = mp[t]
                for i in ii:
                    pair = [idx, i]
                    ans.append(pair)
        return ans

maxDistance = 10000
f = [[1,3000],[2,5000],[3,7000],[4,10000]]
r = [[1,2000],[2,3000],[3,4000],[4,5000]]
exp = [[2,4],[3,2]]


maxDistance = 7000
f = [[1,2000],[2,4000],[3,6000]]
r = [[1,2000]]
exp = [[2,1]]


maxDistance = 9000
f = [[4,1000],[2,3000],[3,4000],[5,4000],[1,5000]]
r = [[1,2000],[3,5000],[2,5000],[4,6000]]
exp = [[2,4],[3,2],[3,3],[5,3],[5,2]]


maxDistance = 20
f = [[1,8],[2,7],[3,14]]
r = [[1,5],[2,10],[3,14]]
exp = [[3,1]]
a = Solution().operationDistance(maxDistance, f, r)
print("ans:", a==exp, a)