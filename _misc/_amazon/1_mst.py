class Solution:
    def find(self, x):
        if self.father[x] == x:
            return self.father[x]
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        ra = self.find(a)
        rb = self.find(b)

        if ra != rb:
            self.father[ra] = rb
            return False

        return True

    def mst(self, cities_cnt, roads_cnt, roads, new_road_cnt, costNewRoadsConstruct):
        self.father = [i for i in range(cities_cnt + 1)]
        for road in roads:
            road.append(0)

        costNewRoadsConstruct.extend(roads)

        def mykey(x):
            return x[2], x[0], x[1]

        costNewRoadsConstruct.sort(key=mykey)

        total_road_cnt = 0
        c = 0
        for conn in costNewRoadsConstruct:
            a = conn[0]
            b = conn[1]

            if not self.union(a, b):
                total_road_cnt += 1
                c += conn[2]

        if total_road_cnt != cities_cnt - 1:
            return -1

        return c


numTotalAvailableCities = 6
numTotalAvailableRoads = 3
roadsAvailable = [[1,4],[4,5],[2,3]]
numNewRoadConstruct = 4
costNewRoadsConstruct = [[1,2,5],[1,3,10],[1,6,2],[5,6,5]]
exp = 7
a = Solution().mst(numTotalAvailableCities,numTotalAvailableRoads,roadsAvailable,numNewRoadConstruct, costNewRoadsConstruct)
print("ans", a==exp, a)