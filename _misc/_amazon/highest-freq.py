import collections
class Solution:
    def highestFreq(self, string, exclude):
        ans = ""
        if len(string) == 0:
            return ""
        
        # turn all letters in exclude into lower case
        myexclude = []
        for item in exclude:
            myexclude.append(item.lower())
        myexclude = set(myexclude)
        
        # turn all letters in given string into lower case
        # keep only letters, nums, and space(as delimiter)
        # count phrase freq using Counter
        tmp = ""
        string = string.lower()
        for ch in string:
            if ch.isalnum() or ch.isspace():
                tmp += ch
        tmp = tmp.split(sep=" ")
        mp = collections.Counter(tmp)
        print(mp)
        
        # find maxFreq
        maxFreq = 0
        for phrase, cnt in mp.items():
            if phrase in myexclude:
                continue
            if cnt > maxFreq:
                ans = phrase
                maxFreq = cnt
        
        return ans
        
        
        


string = "Jimmy has an apple, it Jimmy JiMMY apple is on the table"
exclude =  ["an", "A", "is", "the"]



a = Solution().highestFreq(string, exclude)
print("ans:", a)

m = {1,2,3}
print(type(m), None in m, 3 in m)