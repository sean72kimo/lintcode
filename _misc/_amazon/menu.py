"""
大概意思是给人推荐菜谱，input是两个二维String 数组，第一个用来存菜名和对应
的菜系，第二个用来存人名和这个人喜欢的菜系，要去返回一个二维String数组，
存的值是人名对应可能喜欢的菜名(如果人名后面对应的是“*”，那么就表示所有菜系
都要)。
"""
import collections
class Solution:
    def menu(self, listA, listB):

        countryHasDish = collections.defaultdict(list)
        for dish, country in listA:
            countryHasDish[country].append(dish)

        ans = []
        for name, country in listB:
            if country == '*':
                for dishes in countryHasDish.values():
                    for dish in dishes:
                        ans.append([name, dish])
                continue

            for dish in countryHasDish[country]:
                ans.append([name, dish])

        return ans


listA = [["Pizza", "Italian"], ["Pasta", "Italian"], ["Burger", "American"]]
listB = [["Peter", "Italian"], ["Adam", "American"]]
a = Solution().menu(listA, listB)
print(a)
test = [["Peter", "Pizza"], ["Peter", "Pasta"], ["Adam", "Burger"]]

listA = [["Pizza", "Italian"], ["Pasta", "Italian"], ["Burger", "American"]]
listB = [["Peter", "*"]]
test = [["Peter", "Pizza"], ["Peter", "Pasta"], ["Peter", "Burger"]]
b = Solution().menu(listA, listB)
print(b)


