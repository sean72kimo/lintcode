class Solution:
    def strictlyAscending(self, nums):
        count = 1
        mx = 1
        start = 0

        for i in range(1, len(nums)):
            if nums[i] > nums[i - 1]:
                count = count + 1

            else:
                if mx < count:
                    mx = count
                    start = i - count
                count = 1

        if mx < count:
            mx = count
            start = i - count + 1

        return start
    
nums = [3,2,3,1,2,3]
test = [3]
a = Solution().strictlyAscending(nums)
print("ans a:", a, a in test)

nums = [1,2,3,2,3,4,3,4,5] 
test = [0, 3, 6] #one of them is fine
b = Solution().strictlyAscending(nums)
print("ans b:", b, b in test)


nums = [2,2,2,2,2,2,2,2] 
c = Solution().strictlyAscending(nums)
print("ans c:", c)


