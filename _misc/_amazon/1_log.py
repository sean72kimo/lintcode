from functools import cmp_to_key


class Solution2(object):
    def mycmp(self, a, b):

        logA = a[1:]
        logB = b[1:]

        if logA == logB:
            if a[0] >= b[0]:
                return 1
            else:
                return -1

        for aa, bb in zip(logA, logB):
            if aa > bb:
                return 1
            elif aa < bb:
                return -1


    def reorderLogFiles(self, logs):
        """
        :type logs: List[str]
        :rtype: List[str]
        """
        ans = []
        intStrList = []
        logList = []
        for line in logs:
            temp = line.split(" ")
            if temp[1].isdigit() or temp[1][0] == '-':
                intStrList.append(line)
            else:
                logList.append(temp)

        def mykey(x):
            n = len(x)
            cmp = []
            for i in range(1, n):
                cmp.append(x[i])
            cmp.append(x[0])
            return cmp

        # logList.sort(key = cmp_to_key(self.mycmp))
        logList.sort(key=mykey)
        for log in logList:
            ans.append(' '.join(log))

        return ans + intStrList



logs = ["j mo", "5 m w", "g 07", "o 2 0", "t q h"]
exp = ["5 m w","j mo","t q h","g 07","o 2 0"]
logs = ["j mo", "5 m w"]
exp = ["5 m w","j mo"]
a = Solution2().reorderLogFiles(logs)
print("ans:", a==exp, a)





class Solution2(object):
    def reorderLogFiles(self, logs):
        """
        :type logs: List[str]
        :rtype: List[str]
        """
        ans = []
        intStrList = []
        logList = []
        for line in logs:
            temp = line.split(" ")
            if temp[1].isdigit() or temp[1][0] == '-':
                intStrList.append(line)
            else:
                logList.append(temp)

        def mykey(x):
            n = len(x)
            cmp = []
            for i in range(1, n):
                cmp.append(x[i])
            cmp.append(x[0])
            return cmp

        # logList.sort(key = cmp_to_key(self.mycmp))
        logList.sort(key=mykey)
        for log in logList:
            ans.append(' '.join(log))

        return ans + intStrList