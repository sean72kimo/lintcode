# 438. Find All Anagrams in a String
class Solution:
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """
        if not s or not p:
            return []

        ans = []
        chars = [0 for _ in range(6)]
        plen = len(p)
        slen = len(s)

        def toInt(char):
            return ord(char) - ord('a')


        for i in range(plen):
            chars[toInt(p[i])] += 1

        start = 0
        end = 0
        matched = 0

        while end < slen:
            if chars[toInt(s[end])] >= 1:
                matched += 1
            chars[toInt(s[end])] -= 1
            end += 1

            if matched == plen:
                ans.append(start)

            if end - start == plen:
                if chars[toInt(s[start])] >= 0:
                    matched -= 1
                chars[toInt(s[start])] += 1
                start += 1
                print("move start,", s[start:end + 1], chars)
        return ans


s = "cbaebabacd"
# s = "cbac"
s = "cbaeacb"
p = "abc"
a = Solution().findAnagrams(s, p)
print("ans:", a)
