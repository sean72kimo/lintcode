import collections
class Solution(object):
    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        ans = []
        res = []
        if len(S) == 0:
            return ans
        hsmap = collections.defaultdict(int)
        for i, ch in enumerate(S):
            hsmap[ch] = i

        right = left = 0
        for i, ch in enumerate(S):
            right = max(right, hsmap[ch])
            if i == right:
                ans.append(i - left + 1)
                res.append(S[left : i + 1])
                left = i + 1

        return ans, res
S = "ababcbacadefegdehijhklij"
# S = "abacdfeffhijkik"
a, b = Solution().partitionLabels(S)
print('ans:', a, b)
