class Solution1:
    def KDistinctCharacter(self, s, k):
        if len(s) == 0:
            return []
        
        left = 0
        right = 0
        hsmap = {}
        ans = []
        while right < len(s):

            while s[right] in hsmap:
                hsmap[s[left]] -= 1
                if hsmap[s[left]] == 0:
                    hsmap.pop(s[left])
                left += 1
                
            hsmap[s[right]] = 1

            if right - left + 1 == k:
                sub = s[left:right+1]
                if sub not in ans:
                    ans.append(sub)
                hsmap[s[left]] -= 1
                if hsmap[s[left]] == 0:
                    hsmap.pop(s[left])
                left += 1
            
            right += 1
            
        return ans
s = 'awaglknagawunagwkwagl'
k = 4

a = Solution1().KDistinctCharacter(s, k)
test = ["wagl", "aglk", "glkn", "lkna", "knag", "gawu", "awun", "wuna", "unag", "nagw", "agwk", "kwag"]
print("ans:", a, a==test)




class Solution:
    def KDistinctCharacter_n2(self, s, k):
        if s is None or k <= 0:
            return []

        ans = []
        mp = dict()
        l = 0
        r = 0
        arr = list(s)
        for l in range(len(arr) - k):
            skip = False
            r = 0
            for i in range(k):
                r = l + i
                ch = arr[r]

                if mp.get(ch, -1) == -1:
                    mp[ch] = r
                else:
                    skip = True
                    mp.clear()
                    break

            if not skip:
                sub = s[l:r + 1]
                mp.clear()
                if sub not in ans:
                    ans.append(sub)

        return ans


    def KDistinctCharacter_ptt(self, s, k):

        ans = []
        word = dict()
        len = 0
        i = 0

        for ch in list(s):
            if(ch in word and i - word[ch] < k):

                len = i - word[ch]
            else:
                len += 1
            word[ch] = i

            if(len >= k):
                sub = s[i - k + 1:i + 1]
                if sub not in ans:
                    ans.append(sub)
            i += 1

        return ans

    def KDistinctCharacter_skip(self, s, k):
        if s is None or k <= 0:
            return []

        ans = []
        mp = dict()
        l = 0
        r = 0
        arr = list(s)
        # for l in range(len(arr) - k):
        while l < len(arr) - k:
            skip = False
            r = 0
            for i in range(k):
                r = l + i
                ch = arr[r]

                if mp.get(ch, -1) == -1:
                    mp[ch] = r
                else:
                    l = mp.get(ch)
                    skip = True
                    mp.clear()

                    break

            if not skip:
                sub = s[l : r + 1]
                mp.clear()
                if sub not in ans:
                    ans.append(sub)

            l += 1
        return ans

    def KDistinctCharacter_slicing(self, s, k):
        if s is None or k <= 0:
            return []

        ans = []
        for i, ch in enumerate(s):
            sub = s[i : i + k]
            if len(set(sub)) != k:
                continue

            if sub in ans:
                continue

            ans.append(sub)

        print(ans)
        return ans

import time
s = 'awaglknagawunagwkwagl'
k = 4
a = Solution().KDistinctCharacter_ptt(s, k)
test = ["wagl", "aglk", "glkn", "lkna", "knag", "gawu", "awun", "wuna", "unag", "nagw", "agwk", "kwag"]
print("ans:", a)
print("ans:", a == test)

a = Solution().KDistinctCharacter_skip(s, k)
print("ans:", a == test)
if a != test:
    z = zip(a, test)
    for i in z:
        if i[0] != i[1]:
            print('..', i)
        else:
            print('', i)


