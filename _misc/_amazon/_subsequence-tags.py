import collections
class Solution:
    #def subSequenceTags(self, T, A):
    def subSequenceTags(self, t, s):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
#         if len(s) == 0:
#             return ""
#         if len(t) == 0:
#             return s[:1]
        
        ans = ""
        left = 0
        right = 0

        mp = collections.Counter(t)
        count = len(mp)
        

        while right < len(s):
            if s[right] in mp:
                mp[s[right]] -= 1

                if mp[s[right]] == 0:
                    count -= 1

            while count == 0:
                
                if ans == "":
                    ans = s[left : right + 1]
                    
                
                if right - left + 1 < len(ans):
                    ans = s[left : right + 1]

                if s[left] in mp:
                    mp[s[left]] += 1
                    if mp[s[left]] > 0:
                        count += 1
                left += 1

            right += 1
        return ans
target = ["made", "in", "Spain"]
available = ["made", "weather", "forecast", "says", "that", "made", "rain", "in", "Spain", "stays"]
a = Solution().subSequenceTags(target, available)
print("ans:", a)
test = [5,8]