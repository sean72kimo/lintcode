import heapq


class Solution:
    def kclosest(self, n, locations, k):
        if k >= n:
            return locations
        if k == 0:
            return []

        maxheap = []

        for s, e in locations:
            d = s ** 2 + e ** 2
            itm = (-d, s, e)
            heapq.heappush(maxheap, itm)

            if len(maxheap) > k:
                heapq.heappop(maxheap)

        ans = []
        while maxheap:
            d, x, y = heapq.heappop(maxheap)
            ans.append([x,y])

        return ans[::-1]

numDestinations = 3
allLocations = [[1,2],[3,4],[1,-1]]
numDeliveries = 2
exp = [[1,-1], [1,2]]
a = Solution().kclosest(numDestinations, allLocations, numDeliveries)
print("ans:", a==exp, a)
