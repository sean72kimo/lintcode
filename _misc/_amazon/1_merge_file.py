import heapq


class Solution:
    def mergeCost(self, files):
        if len(files) == 0:
            return 0

        heapq.heapify(files)

        ans = 0
        while len(files) > 1:

            a = heapq.heappop(files)
            b = heapq.heappop(files)
            ans += a + b
            heapq.heappush(files, a+b)


        return ans



files = [8,4,6,12]; exp = 58
files = [ 2, 3, 4, 5, 6, 7 ]; exp =  68
# files = [2,3,4]; exp = 14
# files = [1,2,5,10,35,89]; exp = 224
files = [3,1,2] #9
files = [8,3,5,2,15] #66
a = Solution().mergeCost(files)
print("ans:", a == exp, a)
