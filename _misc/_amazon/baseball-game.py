class Solution(object):
    def calPoints(self, ops):
        """
        :type ops: List[str]
        :rtype: int
        """
        stack = []
        for i in ops:
            if i == "C":
                stack.pop()
            elif i == "D":
                score = stack[-1] * 2
                stack.append(score)
            elif i == "+":
                score = stack[-1] + stack[-2]
                stack.append(score)
            else:  # i.isnumeric()
                stack.append(int(i))

        return sum(stack)
ops = ["5", "2", "C", "D", "+"]
ops = ["5", "-2", "4", "C", "D", "9", "+", "+"]
a = Solution().calPoints(ops)
print("ans:", a)
