class Solution:
    """
    @param nums: an integer array
    @param target: An integer
    @return: the difference between the sum and the target
    """

    def twoSumClosest(self, target, nums):
        # write your code here

        t = target - 30
        i = 0
        j = len(nums) - 1

        nums.sort()

        ans = float('inf')
        res = []

        while i < j:
            v = nums[i] + nums[j]
            print((i, j), nums[i], nums[j], v)

            if v == t:
                if len(res) == 0:
                    res = [i,j]
                    return res
                elif nums[j] > nums[res[-1]]:
                    res = [i,j]


            elif v > t:
                j -= 1
            else:
                i += 1

        return res

truckSpace = 90
packageSpace = [1,10,25,35,60]
exp = [2,3]
a = Solution().twoSumClosest(truckSpace, packageSpace)
print("ans:", a==exp, a)