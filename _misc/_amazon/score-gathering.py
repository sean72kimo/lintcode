from collections import defaultdict
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
    def __repr__(self):
        return "<TreeNode> {}".format(self.val)

class Solution:
    def createBST(self, root, val):
        if root is None:
            return TreeNode(val)

        left = right = None
        if val > root.val:
            right = self.createBST(root.right, val)
        else:
            left = self.createBST(root.left, val)

        if left:
            root.left = left
        if right:
            root.right = right

        return root

    def score_gathering(self, A):
        score = defaultdict(int)
        for a in A:
            score[a] += 1

        vst = set()
        root = None
        for a in A:
            if a in vst:
                continue
            print("adding:", a)
            root = self.createBST(root, a)
            vst.add(a)


        que = [root]
        ans = str(root.val) + ':' + str(score[root.val])
        while que:
            newQ = []
            for node in que:
                print(node)
                if node.left:
                    newQ.append(node.left)
                    ans += ',' + str(node.left.val) + ':' + str(score[node.left.val])
                else:
                    ans += ','
                if node.right:
                    newQ.append(node.right)
                    ans += ',' + str(node.right.val) + ':' + str(score[node.right.val])
                else:
                    ans += ','
            que = newQ

        ans = ans.strip(',')
        return ans
A = [4, 2, 5, 5, 6, 4, 1]
# A = [4, 2, 5]
a = Solution().score_gathering(A)
print("ans:", a)

