import collections
import re
class Solution(object):
    def mostCommonWord(self, paragraph, availableTagList):
        """
        :type paragraph: str
        :type banned: List[str]
        :rtype: str
        """
        banned = set()
        for itm in availableTagList:
            banned.add(itm.lower())

        lst = re.split('\W+', paragraph)

        temp = []
        for itm in lst:
            w = itm.lower()

            if w not in banned:
                temp.append(w)
        print(temp)
        counter = collections.Counter(temp)
        
        max_cnt = float('-inf')
        ans = []
        for word, cnt in counter.items():
            if cnt > max_cnt:
                max_cnt = cnt
                
        for word, cnt in counter.items():
            if cnt == max_cnt and word not in banned:
                ans.append(word)
            
        return ans

# paragraph = "Jack and Jill went to the market to buy bread and cheese. Cheese is Jack's and Jill's favorite food"
# banned = ["and", "he", "the", "to", "is", "Jack", "Jill"]
# a = Solution().mostCommonWord(paragraph, banned)
# print("ans:",a)
# 
# 
# 
# paragraph = "Rose is a flower red rose are flower"
# banned = ["are", "a", "is"]
# a = Solution().mostCommonWord(paragraph, banned)
# print("ans:",a)



paragraph = "Jack and Jill went to the market to buy bread and cheese. Cheese is Jack's and Jill's favorite food"
banned = ["and", "he", "the", "to", "is"]
a = Solution().mostCommonWord(paragraph, banned)
print("ans:",a)