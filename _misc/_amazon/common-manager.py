class Employee:
    def __init__(self, data):
        self.data = data
        self.children = []
    def add_child(self, obj):
        self.children.append(obj)
    def get_name(self):
        return self.data
    def print_child(self):
        print (self.data)
    def print_all_child(self):
        for child in self.children:
            print (child.data)
    def __repr__(self):
        return "<employee>{}".format(self.data)

def get_dict_manager_name(root):
    dict_name_node = {}
    dict_name_parent = {}
    dict_name_node[root.data] = root
    lst = [root]

    while len(lst) != 0:
        node = lst.pop()
        for child in node.children:
            dict_name_node[child.data] = child
            dict_name_parent[child.data] = node
            lst.append(child)

    return dict_name_parent, dict_name_node

def closest_common_manager(root, name1, name2):
    dict_name_parent, dict_name_node = get_dict_manager_name(root)
    node1 = dict_name_node[name1]
    node2 = dict_name_node[name2]
    list1 = []
    list2 = []
    common_ancestor = root
    while node1 != root or node2 != root:
        if node1 == node2:
            common_ancestor = node1
            break
        if node1 in list2:
            common_ancestor = node1
            break
        if node2 in list1:
            common_ancestor = node2
            break
        list1.append(node1)
        list2.append(node2)
        if node1 != root:
            node1 = dict_name_parent[node1.data]
        if node2 != root:
            node2 = dict_name_parent[node2.data]
    print ("Ancestor of ", name1, " and ", name2, " is ", common_ancestor.data)

# driver program
root = Employee("bill")
dom = Employee("Dom")
root.add_child(dom)
root.add_child(Employee("samir"))
root.add_child(Employee("michael"))

dom.add_child(Employee("bob"))
dom.add_child(Employee("porter"))
peter = Employee("peter")
dom.add_child(peter)

peter.add_child(Employee("milton"))
peter.add_child(Employee("nina"))

# closest_common_manager(root, "bill", "nina")
# closest_common_manager(root, "milton", "nina")
# closest_common_manager(root, "Dom", "nina")
# closest_common_manager(root, "nina", "porter")
# closest_common_manager(root, "nina", "samir")
# closest_common_manager(root, "peter", "nina")



employee = {
    "BILL" : ["DOM", "SAMIR", "MICHAEL"],
    "DOM" : ["BOB", "PETER", "PORTER"],
    "PETER" : ["MILTON", "NINA"],
    }

class Employee:
    def __init__(self, name):
        self.name = name
        self.children = []
    def __str__(self):
        return "<Employee>{}".format(self.name)
    def __repr__(self):
        return self.__str__()


Bill = Employee("BILL")
Dom = Employee("DOM")
Peter = Employee("PETER")
Samir = Employee("SAMIR")
Michael = Employee("MICHAEL")
Bob = Employee("BOB")
Porter = Employee("PORTER")
Milton = Employee("MILTON")
Nina = Employee("NINA")
Sean = Employee("SEAN")
Alex = Employee("ALEX")
Steve = Employee("STEVE")
Victor = Employee("VICTOR")

Bill.children.extend([Dom, Samir, Michael])
Dom.children.extend([Bob, Peter, Porter])
Peter.children.extend([Milton, Nina])
Nina.children.extend([Sean])
Michael.children.extend([Alex, Steve])

class Solution:
    def __init__(self):
        self.ans = []

    def closestCommonManager(self, root, name1, name2):
        if not name1 or not name2:
            return None
        path = [root.name]
        vst = set()
        self.dfs(root, name1.name, path, vst)
        self.dfs(root, name2.name, path, vst)
        print(self.ans)
        if len(self.ans) < 2:
            return None

        p1 = self.ans[0]
        p2 = self.ans[1]


        # if len(p1) < len(p2):
        #     lca = self.getLCA(p1, p2)
        # else:
        #     lca = self.getLCA(p2, p1)
        while p1 and p2 and p1[0] == p2[0]:
            n1 = p1.pop(0)
            n2 = p2.pop(0)

        # print("lca:", n1)
        return n1

    def getLCA(self, path1, path2):
        if len(path1) == 1:
            return path1[0]

        for i in range(1, len(path1)):
            if path1[i] != path2[i]:
                return path1[i - 1]


    def dfs(self, root, name, path, vst):
        if root.name == name:
            self.ans.append(path[:])
            return

        for child in root.children:
            if child.name in vst:
                continue
            path.append(child.name)
            vst.add(child.name)
            self.dfs(child, name, path, vst)
            path.pop()
            vst.remove(child.name)



print(Solution().closestCommonManager(Bill, Milton, Nina) == "PETER")
print(Solution().closestCommonManager(Bill, Milton, Bill) == "BILL")
print(Solution().closestCommonManager(Bill, Milton, Porter) == "DOM")
print(Solution().closestCommonManager(Bill, Porter, Milton) == "DOM")
print(Solution().closestCommonManager(Bill, Porter, Peter) == "DOM")
print(Solution().closestCommonManager(Bill, Porter, Samir) == "BILL")
print(Solution().closestCommonManager(Bill, Porter, Sean) == "DOM")
print(Solution().closestCommonManager(Bill, Porter, Steve) == "BILL")
print(Solution().closestCommonManager(Bill, Porter, Victor) == None)



def dfs_path(graph, end, start = 'BILL', path = None):
    if path is None:
        path = [start]
    if start == end:
        yield path
    for next in set(graph[start]) - set(path):
        try:
            for i in dfs_path(graph, end, next, path + [next]):
                yield i
        except KeyError:
            continue

def closestCommonManager(name1, name2):
    path_name1 = []
    path_name2 = []
    for name in dfs_path(employee, name1):
        path_name1 += name
    for name in dfs_path(employee, name2):
        path_name2 += name
    max_len = max(path_name1.__len__(), path_name2.__len__())
# #    print path_name1,path_name2,max_len
    for i in range(max_len):
        try:
            if path_name1[i] != path_name2[i]:
                break
        except IndexError:
            print (path_name1[i - 1])
            return
    print (path_name1[i - 1])

# closestCommonManager('BILL', 'NINA')
# closestCommonManager('MILTON', 'NINA')  # PETER
# closestCommonManager('PORTER', 'NINA')  # DOM
# closestCommonManager('SAMIR', 'NINA')  # BILL
# closestCommonManager('PETER', 'NINA')  # PETER

