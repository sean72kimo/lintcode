import collections
class Solution:
    def substringOneRepeate_nk(self, s, k):
        ans = []
        
        for left in range(len(s) - k + 1):
            sub = s[left:left + k]
            counter = collections.Counter(sub)
            for cnt in counter.values():
                if cnt > 2:
                    continue
                if cnt == 2:
                    ans.append(sub)
                    continue
        return ans

        
    def substringOneRepeate_n(self, s, k):
        left = 0 
        right = 0
        mp = collections.defaultdict(int)
        count = 0
        ans = []
        while right < len(s):
            mp[s[right]] += 1

            while mp[s[right]] > 2:
                mp[s[left]] -= 1
                if mp[s[left]] == 0:
                    del mp[s[left]]
                left += 1

            if mp[s[right]] == 2:
                flag = True

            if right - left + 1 > k:
                mp[s[left]] -= 1
                if mp[s[left]] == 0:
                    del mp[s[left]]
                left += 1

            #if len(mp) == k - 1 and flag == True:
            if right - left + 1 == k and len(mp) == k - 1 and flag == True:
                ans.append(s[left : right + 1])
                if s[left] in mp:
                    mp[s[left]] -= 1
                    if mp[s[left]] == 1:
                        flag = False
                    if mp[s[left]] == 0:
                        del mp[s[left]]
                left += 1

            right += 1
            
        return ans
s = "asadfaghjkjqoiiii"
k = 5
s = "aaaaaaa"
k = 10
s = "asdfaghjkjqoiiii"
k = 5
test = ["asdfa", "ghjkj", "hjkjq", "jkjqo", "jqoii"]
a = Solution().substringOneRepeate_nk(s, k)
print("ans:", a)
b = Solution().substringOneRepeate_n(s, k)
print("ans:", b, b == a)
print(a == test)