from functools import cmp_to_key
class Solution:

    def orderLog(self, logFileSize , logLines):
        
        ans = []
        intStrList = []
        logList = []
        for line in logLines:
            temp = line.split(" ")
            if temp[1].isdigit() or temp[1][0] == '-':
                intStrList.append(line)
            else:
                logList.append(temp)
        
        logList.sort(key = cmp_to_key(self.mycmp))
        for log in logList:
            ans.append(' '.join(log))
        
        return ans + intStrList

    
    def mycmp(self, a, b):

        logA = a[1:]
        logB = b[1:]

        if logA == logB:
            if a[0] >= b[0]:
                return 1
            else:
                return -1

        for aa, bb in zip(logA, logB):
            if aa > bb:
                return 1 
            elif aa < bb:
                return -1




logFileSize = 5
# logLines = [
#     "a1 9 2 3 1",
#     "g1 act car",
#     "g1 Bct car",
#     "zo4 -4 -7",
#     "ab1 off KEY dog",
#     "a8 act zoo"
# ]
logLines = [
    "a1 9 2 3 1",
    "g1 Act car",
    "zo4 4 7",
    "ab1 off KEY dog",
    "a8 act zoo"
]
logLines = [
    "ab1 off KEY dog",
    "ab1 off KEY"
]
logLines = [
    "mi2 jog mid pet",
    "wz3 34 54 398",
    "a1 alps cow bar",
    "x4 45 21 7"
]
logLines = [
    "t2 13 121 98",
    "r1 box ape bit",
    "b4 xi me nu",
    "br8 eat nim did",
    "w1 has uni gry",
    "f3 52 54 31"
]
a = Solution().orderLog(logFileSize, logLines)
print("ans:")
for row in a:
    print(row)




