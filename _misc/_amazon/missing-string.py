"""
2, there are two strings, in which some characters are missing. 
fortunately we know the number of missing characters,
to determing are the two strings possiblely to be the same
for example, A2LE and 2PL1 return true
2A2d and abad2 return false bcause A != a
"""
class Solution:
    def getMatrix(self, s):
        tmp = []
        i = 0
        numStr = ""
        while i < len(s):
            if not s[i].isalpha():
                numStr += s[i]
            else:
                if numStr:
                    slots = int(numStr)
                    tmp.extend([None] * slots)
                    numStr = ""
                tmp.append(s[i])
            i += 1
            
        if numStr:
            slots = int(numStr)
            tmp.extend([None] * slots)
            numStr = ""

        return tmp
        
    def missingString(self, s1, s2):
        tmp1 = self.getMatrix(s1)
        tmp2 = self.getMatrix(s2)
        print(tmp1)
        print(tmp2)
        if len(tmp1) != len(tmp2):
            return False
        zipped = zip(tmp1, tmp2)
        
        for a, b in zipped:
            if a is None and b is None:
                continue
            
            if a is None and b is not None:
                continue
            
            if b is None and a is not None:
                continue
            
            if a != b:
                return False
        
        return True


    def getMatrix2(self, s):
        tmp = []
        i = 0
        numStr = ""
        while i < len(s):
            if not s[i].isalpha():
                numStr += s[i]
            else:
                if numStr:
                    tmp.append(int(numStr))
                    numStr = ""
                tmp.append(s[i])
            i += 1
            
        if numStr:
            tmp.append(int(numStr))
            numStr = ""

        return tmp
    

    def missingString2(self, s1, s2):
        tmp1 = self.getMatrix2(s1)
        tmp2 = self.getMatrix2(s2)
        print(tmp1)
        print(tmp2)
        size1 = 0
        
        for i in tmp1:
            if isinstance(i, int):
                size1 += i
            else:
                size1 += 1

        size2 = 0
        for i in tmp2:
            if isinstance(i, int):
                size2 += i
            else:
                size2 += 1
        
        if size1 != size2:
            return False
        print(tmp1, tmp2)
        i = 0
        j = 0
        while i < len(tmp1) and j < len(tmp2):
            if i < len(tmp1) and j < len(tmp2) and isinstance(tmp1[i], str) and isinstance(tmp2[j], str):
                if tmp1[i] == tmp2[j]:
                    i += 1
                    j += 1
                else:
                    return False

            if i < len(tmp1) and j < len(tmp2) and isinstance(tmp1[i], int) and isinstance(tmp2[j], int):
                if tmp1[i] < tmp2[j]:
                    tmp2[j] -= tmp1[i]
                    i += 1
                elif tmp1[i] > tmp2[j]:
                    tmp1[i] -= tmp2[j]
                    j += 1
                else:
                    i += 1
                    j += 1
                    
            if i < len(tmp1) and j < len(tmp2):
                if isinstance(tmp1[i], int) and isinstance(tmp2[j], str):
                    tmp1[i] -= 1
                    j += 1
                if isinstance(tmp1[i], str) and isinstance(tmp2[j], int):
                    tmp2[j] -= 1
                    i += 1

        return True

s1 = "2PL1"
s2 = "A2LE"

a = Solution().missingString2(s1, s2)
print(a)

s1 = "2A2d"
s2 = "abad2"
a = Solution().missingString(s1, s2)
print(a)
