from sys import maxsize
class Solution(object):
    def cutOffTree(self, forest):
        """
        :type forest: List[List[int]]
        :rtype: int
        """

        if not forest or len(forest) == 0:
            return -1

        def bfs(forest, sr, sc, tr, tc):
            R, C = len(forest), len(forest[0])

            queue = [(sr, sc, 0)]
            seen = {(sr, sc)}

            while queue:
                r, c, d = queue.pop(0)
                if r == tr and c == tc:
                    return d
                for nr, nc in ((r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)):
                    if (0 <= nr < R and 0 <= nc < C and
                            (nr, nc) not in seen and forest[nr][nc]):
                        seen.add((nr, nc))
                        queue.append((nr, nc, d + 1))
            return -1


        trees = []
        for r, row in enumerate(forest):
            for c, v in enumerate(row):
                if v > 1:
                    tree = (v, r, c)
                    trees.append(tree)
        trees.sort()

        sr = sc = ans = 0
        for _, tr, tc in trees:
            d = bfs(forest, sr, sc, tr, tc)

            if d < 0:
                return -1
            ans += d

            sr = tr
            sc = tc

        return ans

forest = [
 [1, 2, 3],
 [0, 0, 4],
 [7, 6, 5]
]
a = Solution().cutOffTree(forest)
print("ans:", a)



# java
"""
class Solution {
    int[] dr = {-1, 1, 0, 0};
    int[] dc = {0, 0, -1, 1};

    private int bfs(List<List<Integer>> forest, int sr, int sc, int tr, int tc) {
        int R = forest.size(), C = forest.get(0).size();
        Queue<int[]> queue = new LinkedList();
        queue.add(new int[]{sr, sc, 0});
        boolean[][] seen = new boolean[R][C];
        seen[sr][sc] = true;
        while (!queue.isEmpty()) {
            int[] cur = queue.poll();
            if (cur[0] == tr && cur[1] == tc) return cur[2];
            for (int di = 0; di < 4; ++di) {
                int r = cur[0] + dr[di];
                int c = cur[1] + dc[di];
                if (0 <= r && r < R && 0 <= c && c < C &&
                        !seen[r][c] && forest.get(r).get(c) > 0) {
                    seen[r][c] = true;
                    queue.add(new int[]{r, c, cur[2]+1});
                }
            }
        }
        return -1;
    }

    public int cutOffTree(List<List<Integer>> forest) {
        List<int[]> trees = new ArrayList();
        for (int r = 0; r < forest.size(); ++r) {
            for (int c = 0; c < forest.get(0).size(); ++c) {
                int v = forest.get(r).get(c);
                if (v > 1) trees.add(new int[]{v, r, c});
            }
        }

        Collections.sort(trees, (a, b) -> Integer.compare(a[0], b[0]));

        int ans = 0, sr = 0, sc = 0;
        for (int[] tree: trees) {
            int d = bfs(forest, sr, sc, tree[1], tree[2]);
            if (d < 0) return -1;
            ans += d;
            sr = tree[1]; sc = tree[2];
        }
        return ans;
    }
}
"""
