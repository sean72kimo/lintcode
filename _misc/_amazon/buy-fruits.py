class Solution:
    """
    @param codeList: The codeList
    @param shoppingCart: The shoppingCart
    @return: The answer
    """
    def buyFruits(self, codeList, cart):
        # Write your code here
        lst = []
        for itemList in codeList:
            for item in itemList:
                lst.append(item)

        if len(lst) > len(cart):
            return 0

        n = len(lst)
        m = len(cart)
        for i in range(m - n + 1):
            for j in range(n):
                if lst[j] == 'anything' or lst[j] == cart[i + j]:
                    if j == n - 1:
                        return 1
                    continue

                if lst[j] != cart[i + j]:
                    break;

        return 0
codeList = [["apple", "apple"], ["orange", "banana", "orange"]]
cart = ["orange", "apple", "apple", "orange", "banana", "orange"]

codeList = [["orange", "banana", "orange"], ["apple", "apple"]]
cart = ["orange", "apple", "apple", "orange", "banana", "orange"]

codeList = [["apple", "apple"], ["orange", "anything", "orange"]]
cart = ["orange", "apple", "apple", "orange", "mango", "orange"]
a = Solution().buyFruits(codeList, cart)
print("ans:", a)
