# quick sort
class Sort:
    def sortIntegers(self, A):
        self.quickSort(A, 0, len(A) - 1)

    def quickSort(self, A, start, end):
        if start >= end:
            return

        # 1. use value to sort, not idex
        # A[l], A[r], A[m]
        l = start
        r = end
        m = int((start + end) / 2)
        pivot = A[m]

        # 2. left <= right, not left < right
        while l <= r:
            # 3. A[l] < pivot , not A[l] <= pivot
            # to avoid case like [1,1,1,1,1], left pointer will move to the end of the array,
            # so the whole array is not divded equally.

            while l <= r and A[l] < pivot:
                l += 1

            while l <= r and A[r] > pivot:
                r -= 1

            if l <= r:
                A[l], A[r] = A[r], A[l]
                l += 1
                r -= 1

        self.quickSort(A, start, r);
        self.quickSort(A, l, end);


# unsortedArray = [6, 5, 3, 3, 0, 3, 3, 1, 8, 7, 2, 4]
# quick_sort = Sort()
# quick_sort.sortIntegers(unsortedArray)
#
# print('ans:', unsortedArray)




A = [6, 5, 3, 3, 0, 3, 3, 1, 8, 7, 2, 4]
# A = [1, 2, 3, 4, 4, 4, 4, 5, 6, 7]
Solution().sortIntegers(A)
print('ans:', A)
