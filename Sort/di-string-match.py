class Solution(object):
    def diStringMatch(self, S):
        """
        :type S: str
        :rtype: List[int]
        類似wiggle sort, 從頭取一個，從尾取一個，反覆執行。
        最後lo 一定等於 hi, 再append to ans
        (因為數字範圍為0~len(S) 總共 n+1個數字), for loop S, 總共移動n次，不論如何，lo和hi會重疊
        """
        n = len(S)
        ans = []
        lo = 0
        hi = len(S)
        for x in S:
            if x == 'I':
                ans.append(lo)
                lo += 1
            else:
                ans.append(hi)
                hi -= 1

        return ans + [hi]