"""
https://www.lintcode.com/problem/532/
"""
class Solution(object):
    def reversePairs(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0 :
            return 0
        temp = [None for _ in range(len(nums))]
        return self.sort(nums, 0, len(nums) - 1, temp)
        
    def sort(self, nums, start, end, temp):
        if start == end:
            return 0;
        
        mid = (end - start) // 2 + start
        
        summ = 0
        print("left:{}, right:{}".format(nums[start:mid+1], nums[mid+1:end+1]))
        summ += self.sort(nums, start, mid, temp)
        summ += self.sort(nums, mid+1, end, temp)
        summ += self.merge(nums, start, end, temp)
        
        return summ
        
        
    def merge(self, nums, start, end, temp):
        mid = (end - start) // 2 + start
        left = start
        right = mid + 1
        index = start
        summ = 0

        while left <= mid and right <= end:
            if nums[left] < nums[right]:
                temp[index] = nums[left]
                left += 1
                index += 1
            else:
                temp[index] = nums[right]
                right += 1
                index += 1
                summ += mid - left + 1
        
        while left <= mid:
            temp[index] = nums[left]
            index += 1
            left += 1
            
        while right <= end:
            temp[index] = nums[right]
            index += 1
            right += 1
        
        for k in range(start, end + 1):
            nums[k] = temp[k]
        
        return summ

nums =  [2,4,1,3,5]
nums = [3,2,1,4,5]
a = Solution().reversePairs(nums)
print("ans:", a)
print(nums)