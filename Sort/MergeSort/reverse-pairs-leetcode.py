class Solution(object):
    def reversePairs(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0 :
            return 0
        temp = [None for _ in range(len(nums))]
        return self.sort(nums, 0, len(nums) - 1, temp)
        
    def sort(self, nums, start, end, temp):
        if start == end:
            return 0;
        
        mid = (end - start) // 2 + start
        
        print("1. left:{}, right:{}".format(nums[start:mid+1], nums[mid+1:end+1]))
        left = self.sort(nums, start, mid, temp)
        right = self.sort(nums, mid+1, end, temp)
        print("2. left:{}, right:{}".format(nums[start:mid+1], nums[mid+1:end+1]))
        cnt = left + right
        i = start
        j = mid + 1
        
        while i <= mid:
            while j <= end and nums[i] > 2 * nums[j]:
                j += 1
            
            cnt += j - (mid + 1)
            #print("cnt",cnt, j, mid)
            i += 1
        #print("total cnt",cnt)
        self.merge(nums, start, end, temp)
        
        return cnt
        
        
    def merge(self, nums, start, end, temp):
        mid = (end - start) // 2 + start
        left = start
        right = mid + 1
        index = start
        
        while left <= mid and right <= end:
            if nums[left] <= nums[right]:
                temp[index] = nums[left]
                left += 1
                index += 1
            else:
                temp[index] = nums[right]
                right += 1
                index += 1
        
        while left <= mid:
            temp[index] = nums[left]
            index += 1
            left += 1
            
        while right <= end:
            temp[index] = nums[right]
            index += 1
            right += 1
        
        for k in range(start, end + 1):
            nums[k] = temp[k]

nums = [2, 4, 1, 3, 5]
nums = [1,3,2,3,1]
a = Solution().reversePairs(nums)
print("ans:", a)