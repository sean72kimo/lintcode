class Solution:
    def countSmaller(self, nums):
        if len(nums) == 0:
            return []
        temp = [None for _ in range(len(nums))]
        smaller = [0 for _ in range(len(nums))]
        A = list(enumerate(nums))
        print("lst", A)
        self.sort(A, 0, len(A) - 1, temp, smaller)
        print("lst", A)
        return smaller

    def sort(self, A, start, end, temp, smaller):
        if start >= end:
            return

        mid = (start + end) // 2
        self.sort(A, start, mid, temp, smaller)
        self.sort(A, mid + 1, end, temp, smaller)
        self.merge(A, start, end, temp, smaller)

    def merge(self, A, start, end, temp, smaller):
        mid = (start + end) // 2
        left = start
        right = mid + 1
        idx = start

        while left <= mid and right <= end:
            if A[left][1] <= A[right][1]:
                temp[idx] = A[left]
                idx += 1
                left += 1
                smaller[A[left][0]] += right - mid + 1  # THINK!!!!!
            else:
                temp[idx] = A[right]
                idx += 1
                right += 1

        while left <= mid:
            temp[idx] = A[left]
            idx += 1
            left += 1

        while right <= end:
            temp[idx] = A[right]
            idx += 1
            right += 1

        for k in range(start, end + 1):
            A[k] = temp[k]

class Solution2:
    def countSmaller(self, nums):


        smaller = [0] * len(nums)
        lst = list(enumerate(nums))
        print("lst", lst)
        self.sort(lst, smaller)
        print("lst", lst)
        return smaller

    def sort(self, enum, smaller):
        half = len(enum) // 2
        if half:
            left = self.sort(enum[:half], smaller)
            right = self.sort(enum[half:], smaller)
            m = len(left)
            n = len(right)
            i = j = 0
            while i < m or j < n:
                if j == n or i < m and left[i][1] <= right[j][1]:
                    enum[i + j] = left[i]
                    smaller[left[i][0]] += j
                    i += 1
                else:
                    enum[i + j] = right[j]
                    j += 1
        return enum
nums = [5, 2, 6, 1]
a = Solution().countSmaller(nums)
print("ans:", a)
