class Solution:
    # @param k & A a integer and an array
    # @return ans a integer
    def kthLargestElement(self, k, nums):
        if nums is None or len(nums) == 0:
            return -1

        n = len(nums)
        return self.quickselect(nums, 0, n - 1, k)

    def quickselect(self, nums, start , end, k):
        if start == end:
            return nums[start]

        i = start
        j = end
        m = (start + end) // 2
        pivot = nums[m]

        while i <= j:

            while i <= j and nums[i] > pivot:
                i += 1
            while i <= j and nums[j] < pivot:
                j -= 1

            if i <= j:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1
                j -= 1
        print(nums)
        if start + k - 1 <= j:
            return self.quickselect(nums, start, j, k)

        if start + k - 1 >= i:
            return self.quickselect(nums, i, end, k - (i - start))

        return nums[j + 1]

nums = [3, 2, 1, 5, 6, 4]
k = 2
# nums = [9, 3, 2, 4, 8]
# k = 3
print(Solution().kthLargestElement(k, nums))
