"""
815. Course Schedule IV
https://www.lintcode.com/problem/course-schedule-iv/
"""

import collections

class Solution_dfs_mem:
    """
    @param n: an integer, denote the number of courses
    @param p: a list of prerequisite pairs
    @return: return an integer,denote the number of topologicalsort
    """

    def __init__(self):
        self.ans = 0

    def fac(self, n):
        if n == 0:
            return 1
        return n * self.fac(n - 1)

    def topologicalSortNumber(self, n, p):
        # Write your code here

        if len(p) == 0:
            return self.fac(n)

        ind = [0 for _ in range(n)]
        done = [False for _ in range(n)]
        graph = collections.defaultdict(set)

        for end, start in p:
            graph[start].add(end)
            ind[end] += 1
        self.mem = {}
        self.ans = self.dfs(0, graph, ind, done, n)
        return self.ans

    def dfs(self, level, graph, ind, done, n):
        s = str(ind)
        if (s, level) in self.mem:
            return self.mem[(s, level)]

        if level == n:
            return 1

        res = 0
        for course in range(n):
            if done[course] or ind[course] != 0:
                continue

            done[course] = True
            for nei in graph[course]:
                ind[nei] -= 1

            res += self.dfs(level + 1, graph, ind, done, n)

            done[course] = False
            for nei in graph[course]:
                ind[nei] += 1

        self.mem[(s, level)] = res
        return res


class Solution_dfs_topdown:
    """
    @param n: an integer, denote the number of courses
    @param p: a list of prerequisite pairs
    @return: return an integer,denote the number of topologicalsort
    """

    def __init__(self):
        self.ans = 0

    def fac(self, n):
        if n == 0:
            return 1
        return n * self.fac(n - 1)

    def topologicalSortNumber(self, n, p):
        if len(p) == 0:
            return self.fac(n)
        # Write your code here
        graph = collections.defaultdict(list)
        ind = collections.defaultdict(int)

        for e, s in p:
            graph[s].append(e)
            ind[e] += 1

        vst = set()
        self.dfs(n, graph, ind, vst)
        return self.ans

    def dfs(self, n, graph, ind, vst):
        if len(vst) == n:
            self.ans += 1
            return

        for course in range(n):
            # if course in vst or ind[course] != 0:
            #     continue

            if course not in vst and ind[course] == 0:
                vst.add(course)
                for nei in graph[course]:
                    ind[nei] -= 1

                self.dfs(n, graph, ind, vst)

                vst.remove(course)
                for nei in graph[course]:
                    ind[nei] += 1
            
n = 2
p = [[1,0]]

n = 10
p = []
exp = 3628800

n = 3
p = [[1,0],[2,0],[2,1]]
exp = 1
a = Solution().topologicalSortNumber(n, p)
print("ans:", a==exp, a)
            
            
            
            
            
        