import collections
class Solution(object):
    def __init__(self):
        self.father = []
    def canFinish(self, num, pre):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """

        # generate graph
        graph = collections.defaultdict(list)
        for end, start in pre:
            graph[start].append(end)
        print(graph)

        # get indegree
        ind = collections.defaultdict(int)
        for end, start in pre:
            ind[end] += 1
        print(ind)
        # find start point
        q = []
        for n in range(num):
            if n not in ind:
                q.append(n)

        while q:
            node = q.pop(0)
            for nei in graph[node]:
                ind[nei] -= 1
                if ind[nei] == 0:
                    q.append(nei)
                    del ind[nei]

        # print(ind)
        return len(ind) == 0

numCourses = 3
prerequisites = [[0, 1], [0, 2], [1, 2]]
# numCourses = 2
# prerequisites = [[1, 0]]
a = Solution().canFinish(numCourses, prerequisites)
print("ans:", a)








# def crush(C):
#     stack = []
#     cnt = 0
#     for ch in C:
#         if len(stack) == 0 or ch != stack[-1]:
#             stack.append(ch)
#             continue
# 
#         if ch == stack[-1]:
#             stack.append(ch)
#             print('cnt for letter', stack)
#             cnt = 0
#             for i in range(len(stack)-1,-1, -1):
#                 if stack[i] == ch:
#                     cnt += 1
#                 else:
#                     break
#                 
#             if cnt >= 3:
#                 for i in range(cnt):
#                     stack.pop()
#                 cnt = 0
#                 continue
#             
#         
#     return ''.join(stack)
# 
# 
# C = "AABBBAACC"
# # C = "ABCCCBB"
# # C = "ABBBCC"
# print("ans:",crush(C))

        
            
            
        
    
    
    
