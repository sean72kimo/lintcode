class Solution:
    # @param {int[]} org a permutation of the integers from 1 to n
    # @param {int[][]} seqs a list of sequences
    # @return {boolean} true if it can be reconstructed only one or false
    def sequenceReconstruction(self, org, seqs):
        # Write your code here
        from collections import defaultdict
        edges = defaultdict(list)
        indegrees = defaultdict(int)
        nodes = set()
        for seq in seqs:
            nodes |= set(seq)
            for i in range(len(seq)):
                if i == 0:
                    indegrees[seq[i]] += 0
                if i < len(seq) - 1:
                    edges[seq[i]].append(seq[i + 1])
                    indegrees[seq[i + 1]] += 1
        print(edges)
        print(indegrees)
        cur = [k for k in indegrees if indegrees[k] == 0]
        res = []

        while len(cur) == 1:
            cur_node = cur.pop()
            res.append(cur_node)
            for node in edges[cur_node]:
                indegrees[node] -= 1
                if indegrees[node] == 0:
                    cur.append(node)
        if len(cur) > 1:
            return False
        return len(res) == len(nodes) and res == org



import collections
class Solution2:
    """
    @param org: a permutation of the integers from 1 to n
    @param seqs: a list of sequences
    @return: true if it can be reconstructed only one or false
    """
    def sequenceReconstruction(self, org, seqs):
        # write your code here
        
        graph = collections.defaultdict(list)
        ind = collections.defaultdict(int)
        allNode = set()
        
        for seq in seqs:
            for i, n in enumerate(seq):
                if i > 0:
                    ind[n] += 1
            
                if i != len(seq)-1:
                    graph[seq[i]].append(seq[i+1])
                
                allNode.add(n)
        
        path = []
        que = collections.deque([])
        for n in allNode:
            if n not in ind:
                que.append(n)
                
        while que:

            if len(que) > 1:
                return False
            
            n = que.popleft()
            path.append(n)
            
            for nei in graph[n]:
                ind[nei] -= 1
                if ind[nei] == 0:
                    ind.pop(nei)
                    que.append(nei)
                    
        
        #print(path, org)
        return path == org
    
org = [1,2,3]
seqs = [[1,2],[1,3]]

# org = [1,2,3]
# seqs = [[1,2]]

# org = [1,2,3]
# seqs = [[1,2],[1,3],[2,3]]

# org = [4,1,5,2,6,3]
# seqs = [[5,2,6,3],[4,1,5,2]]

a = Solution().sequenceReconstruction(org, seqs)
print("ans:", a)

b = Solution2().sequenceReconstruction(org, seqs)
print("ans:", b)
