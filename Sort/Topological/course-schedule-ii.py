import collections


class Solution_dfs_TLE(object):
    def findOrder(self, n, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        graph = collections.defaultdict(list)
        ind = [0 for _ in range(n)]
        vst = [False for _ in range(n)]
        for e, s in prerequisites:
            graph[s].append(e)
            ind[e] += 1

        path = []
        self.ans = []
        self.dfs(graph, ind, n, vst, path)
        print(self.ans)
        return path

    def dfs(self, graph, ind, n, vst, path):
        if len(path) == n:
            self.ans.append(path[:])
            return True

        for course, indegree in enumerate(ind):
            if vst[course] or indegree:
                continue

            for nei in graph[course]:
                ind[nei] -= 1
            vst[course] = True
            path.append(course)
            self.dfs(graph, ind, n, vst, path)

            # 找到一組解，可以提早return ，或者持續dfs找到所有解
            # if self.dfs(graph, ind, n, vst, path):
            #     return True

            for nei in graph[course]:
                ind[nei] += 1
            vst[course] = False
            path.pop()
        return False

n, pre = 4, [[1,0],[2,0],[3,1],[3,2]]
a = Solution_dfs_TLE().findOrder(n, pre)
print("ans:", a)


class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        if len(prerequisites) == 0:
            return list(range(numCourses))

        graph = collections.defaultdict(list)
        ind = collections.defaultdict(int)

        for e, s in prerequisites:
            graph[s].append(e)
            ind[e] += 1

        que = collections.deque([])
        ans = []
        for i in range(numCourses):
            if i not in ind:
                que.append(i)
                ans.append(i)

        while que:
            # curr = que.pop() #dfs
            curr = que.popleft() #bfs

            for nei in graph[curr]:
                ind[nei] -= 1
                if ind[nei] == 0:
                    que.append(nei)
                    ans.append(nei)

        if len(ans) == numCourses:
            return ans
        return []