import collections

"""
in-degree == 1 的點為root
需要考慮只有一個root or 兩個root的情況
leetcode solution解說
1-2-3 ans:[2]

1-2-3-4 ans:[2, 3]
"""
class Solution(object):
    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        if len(edges) == 0:
            return [0]

        graph = collections.defaultdict(list)
        ind = collections.Counter()

        for s, e in edges:
            graph[s].append(e)
            graph[e].append(s)
            ind[s] += 1
            ind[e] += 1

        que = []
        vst = set()
        for i in range(n):
            if ind[i] == 1:
                que.append(i)
                ind.pop(i)

        while que:
            new_q = []
            path = []
            for curr in que:
                path.append(curr)

                for nei in graph[curr]:
                    ind[nei] -= 1
                    if ind[nei] == 0:
                        ind.pop(nei)

                    if ind[nei] == 0:
                        new_q.append(nei)
            que = new_q

        return path

class Solution2(object):

    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        graph = collections.defaultdict(list)
        ind = collections.defaultdict(int)

        for s, e in edges:
            graph[s].append(e)
            graph[e].append(s)
            ind[e] += 1
            ind[s] += 1

        que = collections.deque([])

        for i in range(n):
            if ind[i] == 1:
                que.append(i)

        while que:
            new_q = []
            res = []
            for curr in que:
                res.append(curr)
                ind[curr] -= 1
                if ind[curr] == 0:
                    ind.pop(curr)

                for nei in graph[curr]:
                    ind[nei] -= 1
                    if ind[nei] == 1:
                        new_q.append(nei)
            que = new_q

        return res


n = 4
edges = [[1, 0], [1, 2], [1, 3]]
n = 6
edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]
# n = 8
# edges = [[1, 0], [1, 2], [1, 3], [6, 2], [7, 6]]

a = Solution().findMinHeightTrees(n, edges)
print("ans:", a)
