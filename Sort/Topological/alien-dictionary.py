import collections
"""
1 建圖，
1.1 先遍歷每一個ch，確認他們都會出現在graph裡面 for word in words and for each ch in word, graph[ch]
1.2 建立neighbor list
2 建立ind
3 bfs via heap, 進行topological sorting, after done, if len(ans) != len(graph) return "" else ans
use heap can make sure lexicographical order
grpah裡面包含所有的ch, 所以len(ans)一定要等於len(graph)
"""


class Solution(object):
    """
    @param words: a list of words
    @return: a string which is correct order
    """

    def alienOrder(self, words):

        graph = self.build_graph(words)

        if not graph:
            return ""

        return self.topological_sort(graph)

    def build_graph(self, words):
        # key is node, value is neighbors
        graph = collections.defaultdict(set)
        for word in words:
            for ch in word:
                graph[ch]

        # add edges
        n = len(words)
        for i in range(n - 1):
            size = min(len(words[i]), len(words[i + 1]))
            word1 = words[i]
            word2 = words[i + 1]

            for j, (c1, c2) in enumerate(zip(word1, word2)):
                if c1 != c2:
                    graph[c1].add(c2)
                    break

                if j == size - 1:
                    if len(words[i]) > len(words[i + 1]):
                        return None

        return graph

    def topological_sort(self, graph):
        # initialize indegree
        indegree = {node: 0 for node in graph}

        # calculate indegree
        for node in graph:
            for neighbor in graph[node]:
                indegree[neighbor] = indegree[neighbor] + 1

        # use heapq instead of regular queue so that we can get the
        # smallest lexicographical order
        queue = [node for node in graph if indegree[node] == 0]
        heapify(queue)

        # regular bfs algorithm to do topological sorting
        topo_order = ""
        while queue:
            node = heappop(queue)
            topo_order += node
            for neighbor in graph[node]:
                indegree[neighbor] -= 1
                if indegree[neighbor] == 0:
                    heappush(queue, neighbor)

        # if all nodes popped
        if len(topo_order) == len(graph):
            return topo_order

        return ""


words = [
  "wrt",
  "wrf",
  "er",
  "ett",
  "rftt"
]

words = ["ri","xz","qxf","jhsguaw","dztqrbwbm","dhdqfb","jdv","fcgfsilnb","ooby"]
words, expected = ["abc","ab"], ""
a = Solution().alienOrder(words)
print("ans:", a)

from heapq import heappush, heappop, heapify


class Solution2:
    """
    @param words: a list of words
    @return: a string which is correct order
    """

    def alienOrder(self, words):
        graph = self.build_graph(words)
        if not graph:
            return ""
        return self.topological_sort(graph)

    def build_graph(self, words):
        # key is node, value is neighbors
        graph = {}

        # initialize graph
        for word in words:
            for c in word:
                if c not in graph:
                    graph[c] = set()

                    # add edges
        n = len(words)
        for i in range(n - 1):
            for j in range(min(len(words[i]), len(words[i + 1]))):
                if words[i][j] != words[i + 1][j]:
                    graph[words[i][j]].add(words[i + 1][j])
                    break
                if j == min(len(words[i]), len(words[i + 1])) - 1:
                    if len(words[i]) > len(words[i + 1]):
                        return None

        return graph

    def topological_sort(self, graph):
        # initialize indegree
        indegree = {
            node: 0
            for node in graph
        }

        # calculate indegree
        for node in graph:
            for neighbor in graph[node]:
                indegree[neighbor] = indegree[neighbor] + 1

        # use heapq instead of regular queue so that we can get the
        # smallest lexicographical order
        queue = [node for node in graph if indegree[node] == 0]
        heapify(queue)

        # regular bfs algorithm to do topological sorting
        topo_order = ""
        while queue:
            node = heappop(queue)
            topo_order += node
            for neighbor in graph[node]:
                indegree[neighbor] -= 1
                if indegree[neighbor] == 0:
                    heappush(queue, neighbor)

        # if all nodes popped
        if len(topo_order) == len(graph):
            return topo_order

        return ""


words, expected = ["abc","ab"], ""
a = Solution2().alienOrder(words)
print("ans:", a)