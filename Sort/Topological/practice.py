import collections
class Solution:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, num, pre):
        # write your code here
        graph = collections.defaultdict(list)
        for end, start in pre:
            graph[start].append(end)

        ind = collections.defaultdict(int)
        for end, start in pre:
            ind[end] += 1
        print(ind)

        q = []
        for n in range(num):
            if ind.get(n, 0) == 0:
            # if ind[n] == 0:
            # if n not in ind:
                q.append(n)
        print(ind, q)

        while q:
            node = q.pop(0)

            for nei in graph[node]:
                print(nei)
                ind[nei] -= 1

                if ind[nei] == 0:
                    q.append(nei)
                    del ind[nei]
        print(ind)
        return len(ind) == 0

num = 2
pre = [[1, 0]]
a = Solution().canFinish(num, pre)
print("ans:", a)
