import heapq
from _ast import List



"""
quick select avg O(n), worst case O(n^2)
heap nlogk
"""

# heap
class Point:
    def __init__(self, d, x, y):
        self.d = d
        self.x = x
        self.y = y

    def __lt__(self, other):
        return self.d > other.d


class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        max_heap = []

        for x, y in points:
            d = x ** 2 + y ** 2
            heapq.heappush(max_heap, Point(d, x, y))

            if len(max_heap) > k:
                heapq.heappop(max_heap)

        ans = []
        while max_heap:
            p = heapq.heappop(max_heap)
            ans.append([p.x, p.y])

        return ans

# quick select
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_dis(self):
        return self.x ** 2 + self.y ** 2


class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:

        self.quick_select(points, k, 0, len(points) - 1)
        return points[:k]

    def quick_select(self, points, k, start, end):
        if start >= end:
            return points[start]

        mid = (start + end) // 2
        pivot = self.get_dist(points[mid])
        left = start
        right = end

        while left <= right:
            while left <= right and self.get_dist(points[left]) < pivot:
                left += 1

            while left <= right and self.get_dist(points[right]) > pivot:
                right -= 1

            if left <= right:
                points[left], points[right] = points[right], points[left]
                left += 1
                right -= 1

        if start + k - 1 <= right:
            self.quick_select(points, k, start, right)
        elif start + k - 1 >= left:
            self.quick_select(points, k - (left - start), left, end)

    def get_dist(self, points):
        return points[0] ** 2 + points[1] ** 2



