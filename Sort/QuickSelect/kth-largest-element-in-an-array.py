from typing import List


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:

        return self.quick_select(nums, k, 0, len(nums) - 1)

    def quick_select(self, nums, k, start, end):
        if start >= end:
            return nums[start]

        left = start
        right = end
        mid = (start + end) // 2
        pivot = nums[mid]

        while left <= right:
            while left <= right and nums[left] > pivot:
                left += 1

            while left <= right and nums[right] < pivot:
                right -= 1

            if left <= right:
                nums[left], nums[right] = nums[right], nums[left]
                left += 1
                right -= 1

        if k + start - 1 <= right:
            return self.quick_select(nums, k, start, right)
        elif k + start - 1 >= left:
            return self.quick_select(nums, k - (left - start), left, end)

        return nums[right + 1]