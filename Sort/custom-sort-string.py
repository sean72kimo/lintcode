import bisect
import collections
from functools import cmp_to_key
from collections import defaultdict
"""
三種解法
1. similar to 269. Alien Dictionary 
2. counting (easiest and most optimal)
3. custom sort
"""



# similar to 269. Alien Dictionary
class Solution:
    def customSortString(self, order: str, s: str) -> str:

        graph = collections.defaultdict(list)
        ind = collections.Counter()
        char_cnt = collections.Counter(s)

        for i in range(1, len(order)):
            ch1 = order[i - 1]
            ch2 = order[i]

            graph[ch1].append(ch2)
            ind[ch2] += 1

        que = collections.deque([])

        for ch in s:
            if ind[ch] == 0:
                que.append(ch)
        print(que)
        ans = []
        while que:
            curr = que.popleft()

            if curr in char_cnt:
                ans.append(curr * char_cnt[curr])
                char_cnt.pop(curr)

            for nei in graph[curr]:
                ind[nei] -= 1

                if ind[nei] == 0:
                    if nei in char_cnt:
                        que.append(nei)
                    ind.pop(nei)

        return ''.join(ans)

# O(order) + O(s)
class Solution:
    def customSortString(self, order: str, s: str) -> str:
        cnt = collections.Counter(s)
        order_set = set(order)
        ans = []
        for ch in order:
            ans.append(ch * cnt[ch])
            cnt[ch] = 0

        for ch in s:
            if ch not in order_set:
                ans.append(ch)

        return ''.join(ans)


# time t * log(t) where t = len(T)
class Solution:
    def customSortString(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: str
        """
        myord = defaultdict(int)
        for i, c in enumerate(S):
            myord[c] = i

        def mycmp(slf, other):
            print("comparing ", slf, " and ", other)
            diff = myord[slf] - myord[other]

            if diff > 0:
                return 1
            elif diff < 0:
                return -1
            else:
                return 0

        t = list(T)
        t.sort(key = cmp_to_key(mycmp))

        return ''.join(t)


S = "cba"
T = "abcd"
a = Solution().customSortString(S, T)
print("ans:", a)



a = ["a","b","c", "d"]
def mycmp(a, b):
    if a > b:
        return 1
    elif a < b:
        return -1
    else:
        return 0
    
    
    
    
print(sorted(a, key = cmp_to_key(mycmp)))