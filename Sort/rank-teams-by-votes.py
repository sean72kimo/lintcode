import collections
from typing import List


class Solution:
    def rankTeams(self, votes: List[str]) -> str:
        count = {v: [0] * len(votes[0]) + [v] for v in votes[0]}

        for vote in votes:
            for i, v in enumerate(vote):
                count[v][i] -= 1
        result = list(count.items())

        def mykey(x):
            return x[1], x[0]

        result.sort(key=mykey)

        ans = []
        for ch, _ in result:
            ans.append(ch)
        return ''.join(ans)
        # return ''.join(sorted(votes[0], key=count.get))



a = ["A","B","C"]
print(a[4:])