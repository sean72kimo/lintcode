class Solution:
    """
    @param A: an array
    @return: total of reverse pairs
    """
    def reversePairs(self, A):
        # write your code here
        temp = [None] * len(A)
        return self.mergeSort(A, 0, len(A) - 1, temp)

    def mergeSort(self, A, start, end, temp):
        if start >= end:
            return 0

        sum = 0
        mid = (start + end) // 2
        sum += self.mergeSort(A, start, mid, temp)
        sum += self.mergeSort(A, mid + 1, end, temp)
        sum += self.merge(A, start, mid, end, temp)

        return sum

    def merge(self, A, start, mid, end, temp):

        left = start
        right = mid + 1
        idx = start
        sum = 0

        while left <= mid and right <= end:
            if A[left] < A[right]:
                temp[idx] = A[left]
                idx += 1
                left += 1
            else:
                print("====")
                print(mid == left, A)
                print("A[{}] = {}, A[{}] = {}".format(left, A[left], right, A[right]))
                temp[idx] = A[right]
                idx += 1
                right += 1
                # sum = sum + mid - left + 1
                sum += 1

        while left <= mid:
            temp[idx] = A[left]
            left += 1
            idx += 1

        while right <= end:
            temp[idx] = A[right]
            right += 1
            idx += 1


        A[start : end + 1] = temp[start : end + 1]
        return sum

arr = [0, 7, 1, 2, 3, 6, 4, 5]
arr = [2, 4, 1, 3, 5]
a = Solution().reversePairs(arr)
print("ans:", a)


