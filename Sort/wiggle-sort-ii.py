class Solution2(object):
    def wiggleSort(self, A):
        n = len(A)
        tmp = sorted(A)
        m = (n - 1) // 2
        j = n - 1



        print(m)
        print(tmp)
        print(tmp[:m + 1])
        print(tmp[m + 1:])


        for i in range(n):
            if i % 2 == 0:
                A[i] = tmp[m]
                m -= 1
            else:
                A[i] = tmp[j]
                j -= 1

nums = [1, 3, 2, 2, 3, 1]
nums = [1, 1, 2, 1, 2, 2, 1]
nums = [2, 3, 3, 2, 2, 2, 1, 1]
# nums = [1, 5, 1, 1, 6, 4]
# nums = [4, 5, 5, 6]
# Solution2().wiggleSort(nums)
# print('ans:', nums)



"""
75. Sort Colors
215. Kth Largest Element in an Array

1. quick select to find median O(n)
2. 3-way partition as in sort-color, copy result to tmp array
3. from idx of median, m and last position j = n-1
4. copy tmp to given array, A
m-=1
j-=1
"""
class Solution:
    def wiggleSort(self, nums):
        """
        Do not return anything, modify nums in-place instead.
        """
        if len(nums) <= 1:
            return

        n = len(nums)
        if n % 2 == 0:
            kth = n // 2  # median is the kth number
        else:
            kth = (n // 2) + 1

        A = nums[:]
        median = self.quick_select(A, 0, len(A) - 1, kth)
        self.partition(A, median)
        r = len(nums) - 1
        l = kth - 1  # convert kth to index
        for i in range(len(nums)):
            if i % 2 == 0:
                nums[i] = A[l]
                l -= 1
            else:
                nums[i] = A[r]
                r -= 1

    def partition(self, nums, pivot):
        i = 0
        l = 0
        r = len(nums) - 1

        while i <= r:
            if nums[i] < pivot:
                nums[i], nums[l] = nums[l], nums[i]
                i += 1
                l += 1
            elif nums[i] == pivot:
                i += 1
            elif nums[i] > pivot:
                nums[i], nums[r] = nums[r], nums[i]
                r -= 1

    def quick_select(self, A, start, end, k):
        if start == end:
            return A[start]

        i = start
        j = end
        m = (start + end) // 2
        p = A[m]

        while i <= j:
            while i <= j and A[i] < p:
                i += 1
            while i <= j and A[j] > p:
                j -= 1

            if i <= j:
                A[i], A[j] = A[j], A[i]
                i += 1
                j -= 1

        if start + k - 1 <= j:
            return self.quick_select(A, start, j, k)
        if start + k - 1 >= i:
            return self.quick_select(A, i, end, k - (i - start))
        return A[j + 1]


nums = [1, 3, 2, 2, 3, 1]
nums = [1, 1, 2, 1, 2, 2, 1]
nums = [2, 3, 3, 2, 2, 2, 1, 1]
# nums = [1, 5, 1, 1, 6, 4]
nums = [4, 5, 5, 6]
nums = [1, 5, 4, 4, 5, 1, 1, 5, 3, 4, 2, 4, 4, 1, 1, 1, 2, 4, 5]
Solution().wiggleSort(nums)
print('ans:', nums)

nums = [1, 5, 1, 1, 6, 4]




