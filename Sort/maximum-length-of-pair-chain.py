from sys import maxsize
class Solution(object):
    def findLongestChain(self, pairs):
        """
        :type pairs: List[List[int]]
        :rtype: int
        """

        def mykey(x):
            return x[1]

        pairs.sort(key = mykey)
        new = [pairs[0]]
        cur = -maxsize
        res = 0

        for p in pairs:
            if cur < p[0]:
                cur = p[1]
                res = res + 1

        return res


pairs = [[1, 2], [3, 3], [2, 4], [7, 8], [9, 10], [10, 12]]
ans = Solution().findLongestChain(pairs)
print('ans:', ans)
