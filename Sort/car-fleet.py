from typing import List


class Solution:
    def carFleet(self, target: int, position: List[int], speed: List[int]) -> int:
        if len(position) == 0:
            return 0
        if len(position) == 1:
            return 1

        cars = sorted(zip(position, speed))

        times = []
        for pos, sp in cars:
            t = (target - pos) / sp
            times.append(t)

        ans = 0

        while len(times) > 1:
            lead = times.pop()
            if lead < times[-1]:
                ans += 1
            else:
                times[-1] = lead

        return ans + 1
# target = 12
# position = [10,8,0,5,3]
# speed = [2,4,1,1,3]
# sol = Solution()
# a = sol.carFleet(target, position, speed)
# print("ans:", a)


class Solution_sort(object):
    def carFleet(self, target, position, speed):
        """
        :type target: int
        :type position: List[int]
        :type speed: List[int]
        :rtype: int
        """
        cars = []
        n = len(position)
        for i in range(n):
            c = (position[i], speed[i])
            cars.append(c)

        def mykey(x):
            return target - x[0]

        cars.sort(key=mykey)

        time = 0
        ans = 0
        for i in range(n):
            t = (target - cars[i][0]) / cars[i][1]
            if t > time:
                time = t
                ans += 1
            else:
                pass

        return ans










target = 12
position = [10,8,0,5,3]
speed = [2,4,1,1,3]
exp = 3
target = 10
position = [6,8]
speed = [3,2]
exp = 2
sol = Solution()
a = sol.carFleet(target, position, speed)
print("ans:", a)