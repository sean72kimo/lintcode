import collections
import heapq
from typing import List


class Solution:
    def isNStraightHand(self, hand: List[int], W: int) -> bool:
        l = len(hand)
        if l % W:
            return False
        if W == 1:
            return True
        groups = len(hand) // W
        cnt = collections.Counter(hand)
        heapq.heapify(hand)

        for i in range(groups):
            start = heapq.heappop(hand)

            while cnt[start] == 0:  # if the number is no loner available
                start = heapq.heappop(hand)  # we pop again

            for j in range(W):
                if cnt[start] == 0:
                    return False
                cnt[start] -= 1  # decrease its counts
                start += 1

        return True

"""
n = len(hand)
time: nlogn(sort) + n^2 (remove)
"""
class Solution_n2:
    def isNStraightHand(self, hand: List[int], W: int) -> bool:
        if len(hand) % W:
            return False

        hand.sort()
        while len(hand):
            try:
                base = hand[0]
                for i in range(W):
                    hand.remove(base + i)
            except:
                return False

        return True


class Solution_sort:
    def isNStraightHand(self, hand: List[int], W: int) -> bool:
        if len(hand) % W:
            return False

        cards = collections.Counter(hand)
        keys = sorted(cards.keys())

        start = keys[0]

        while len(cards):
            s = start

            while s not in cards:
                s += 1

            start = s
            for _ in range(W):

                if start not in cards:
                    return False

                cards[start] -= 1

                if cards[start] == 0:
                    cards.pop(start)

                start = start + 1


            start = s



        return True



hand = [1,2,3,6,2,3,4,7,8]
W = 3
exp = True

# hand = [1, 2, 3, 4, 5]
# W = 4
# exp = False

sol = Solution()
a = sol.isNStraightHand(hand, W)
print("ans:", a)


