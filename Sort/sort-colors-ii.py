class Solution:
    """
    @param: colors: A list of integer
    @param: k: An integer
    @return: nothing
    """
    def sortColors2(self, colors, k):
        # write your code here
        if colors is None and len(colors) == 0:
            return
        n = len(colors)
        s = 0
        e = n - 1
        colorfrom = 1
        colorto = k
        self.rainbowsort(colors, s, e, colorfrom, colorto)

    def rainbowsort(self, colors, start, end, colorfrom, colorto):
        if colorfrom == colorto:
            return
        if start >= end:
            return

        colormid = (colorfrom + colorto) // 2
        l = start
        r = end
        while l <= r:
            while l <= r and colors[l] <= colormid:
                l += 1
            while l <= r and colors[r] > colormid:
                r -= 1

            if l <= r:
                colors[l], colors[r] = colors[r], colors[l]
                l += 1
                r -= 1

        self.rainbowsort(colors, start, r, colorfrom, colormid)
        self.rainbowsort(colors, l, end, colormid + 1, colorto)

colors = [3, 2, 2, 1, 4]
k = 4
Solution().sortColors2(colors, k)
print("ans:", colors)
