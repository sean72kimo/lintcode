from collections import defaultdict, Counter
class Solution(object):
    def frequencySort(self, s):
        """
        :type s: str
        :rtype: str
        """
        n = len(s)
        if n == 0:
            return ""

        count = Counter(s)
        bucket = defaultdict(list)

        for letter, cnt in count.items():
            bucket[cnt].append(letter)

        ans = []
        for i in range(n, -1, -1):
            if bucket[i]:
                for letter in bucket[i]:
                    ans.extend([letter] * i)

        return ''.join(ans)

def main():
    s = "Aabb"
    a = Solution().frequencySort(s)
    print("ans:", a)


if __name__ == '__main__':
    main()
