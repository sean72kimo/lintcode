class Solution(object):
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int

        1.記錄每一個數字最後出現的位置在哪
        (bucket的概念，可以記錄1~9個桶子，分別出現過在哪幾個idx
        2.for loop num array, 把每一個數字和9~1比較，看看9~1是否可以找到大於當前數字者
        3.因為我們是從頭往後找，所以若是找到9~1大於當前數字，交互後，數字一定更大
        4.但仍要滿足9~1出現在原數組中(查找bucket) 並且9~1是出現在當前數字後面(不然把大數就越換越小了)
        """
        digits = list(str(num))
        n = len(digits)
        buckets = [None for _ in range(10)]

        for i in range(n):
            v = int(digits[i])
            buckets[v] = i

        for i in range(n):
            for k in range(9, -1, -1):
                if int(digits[i]) >= k:
                    break

                if buckets[k] and buckets[k] > i:
                    digits[i], digits[buckets[k]] = digits[buckets[k]], digits[i]
                    return int(''.join(digits))

        return num


class Solution:
    def maximumSwap(self, num: int) -> int:
        digits = list(str(num))
        n = len(digits)
        #  每個桶子代表數字一到九的其中一個數字
        buckets = [None for _ in range(10)]

        # input為一串數字。找到每個數字最後出現的位置，記錄在bucket中
        for i in range(n):
            v = int(digits[i])
            buckets[v] = i

        print(digits)
        for i in range(n):  # 每一個數字都和1~9比較一下
            for k in range(9, -1, -1):  #
                # 要滿足9~1出現在原數組中(查找bucket) 並且9~1是出現在當前數字後面(不然把大數就越換越小了)
                # if int(digits[i]) >= k:
                #     break

                    # 每個桶字都代表1~9的其中一個數字
                # 如果數字k最後出現的位置大於i, 表示可以將這個數字k往前換，成為一個新的num (new_num), 且new_num > num
                if buckets[k] and buckets[k] > i:
                    digits[i], digits[buckets[k]] = digits[buckets[k]], digits[i],
                    return int(''.join(digits))

        return int(''.join(digits))

num = 10
ans = Solution().maximumSwap(num)
print("ans:", ans)

word = "sean"
for i, ch in enumerate(word):
    print(i, ch)
print(word[1:])