#https://www.youtube.com/watch?v=Y2KL1ddRi3U
class Bucket:
    def __init__(self):
        self.max = 0
        self.min = float('inf')

    def update_bucket(self, num):
        self.max = max(num, self.max)
        self.min = min(num, self.min)

    def __repr__(self):
        return str([self.min, self.max])


class Solution:
    def maximumGap(self, nums):
        if not nums or len(nums) < 2:
            return 0
        if len(nums) == 2:
            return abs(nums[0] - nums[1])

        n = len(nums)
        buckets = [Bucket() for _ in range(n - 1)]
        mx = max(nums)
        mn = min(nums)
        if mx == mn:
            return 0
        print(buckets)
        for i in range(n):
            j = self.find_bucket(nums[i], mx, mn, n - 1)
            buckets[j].update_bucket(nums[i])

        res = 0
        curr_min = mn
        for j in range(len(buckets)):
            if buckets[j].min == float('inf'):
                continue
            res = max(res, buckets[j].min - curr_min)
            curr_min = buckets[j].max
        return res

    def find_bucket(self, num, mx, mn, bucket_cnt):
        size = (mx - mn) / bucket_cnt
        idx = (num - mn) // size
        return min(int(idx), bucket_cnt - 1)


class Solution2:
    """
    @param: nums: an array of integers
    @return: the maximun difference
    """
    def maximumGap(self, nums):
        # write your code here
        if not nums or len(nums) < 2:
            return 0

        n = len(nums)
        mx = max(nums)
        mn = min(nums)
        bucketRange = max(1, (mx - mn) // (n - 1))
        print("bucketRange:", bucketRange)
        bucketLen = ((mx - mn) // (bucketRange)) + 1
        buckets = [None] * bucketLen

        for k in nums:
            loc = (k - mn) // bucketRange

            b = buckets[loc]
            if not b:
                b = [k, k]  # [min, max] in a bucket
                buckets[loc] = b
            else:
                b[0] = min(k, b[0])
                b[1] = max(k, b[1])

        mxGap = 0
        for x in range(bucketLen):
            if not buckets[x]:
                continue

            y = x + 1
            while y < bucketLen and buckets[y] is None:
                y += 1
            if y < bucketLen:
                print(buckets)

                mxGap = max(mxGap, buckets[y][0] - buckets[x][1])

            x = y

        return mxGap



def main():
    nums = [0, 6, 3, 9, 12, 16, 19, 22]
    nums = [3,6,9,1]
    nums = [1,1,1,1,1,5,5,5,5,5]
    nums = [2,3,4,5,8]
    a = Solution2().maximumGap(nums)
    print("ans:", a)


if __name__ == '__main__':
    main()

