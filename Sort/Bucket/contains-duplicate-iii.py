class Solution(object):
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        """
        :type nums: List[int]
        :type k: int
        :type t: int
        :rtype: bool
        全部的bucket都是保證在window k裡面
        將每一個數字歸類到bucket中
        buckt的大小為 (t+1)
        ex t = 3, 3//(3+1) = 0, 4//(3+1) = 1,
        每次檢查同一個 bucket以及左右兩個bucket,
        同一個bucket必定小於t
        檢查左右兩個bucket時候要檢查一下距離，例如num有可能存在於右邊桶子的上界 or 左邊桶子的下界
        當 i-k >= 0的時候，將舊的桶子刪除(因為不滿足 i, j距離 <= k的條件)
        """
        if k <= 0 or len(nums) == 0 or t < 0:
            return False
        bucket = {}
        for i , num in enumerate(nums):
            key = num // (t+1)
            if key in bucket:# and abs(bucket[key] - num) <= t: 這個bucket裡面保證距離一定小於t
                return True

            if key + 1 in bucket and abs(bucket[key+1] - num) <= t:
                return True

            if key - 1 in bucket and abs(bucket[key-1] - num) <= t:
                return True

            if i - k >= 0: #sliding window, 離開window的key可以淘汰
                old_k = nums[i-k] // (t + 1)
                bucket.pop(old_k)

            bucket[key] = num

        return False





def main():
    nums = [1, 3, 1]
    k = 1
    t = 1
    a = Solution().containsNearbyAlmostDuplicate(nums, k, t)
    print("ans:", a)


if __name__ == "__main__":
    main()
