from collections import Counter
from collections import defaultdict
import collections
import heapq
class Solution(object):
    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        if len(nums) == 0:
            return []

        counter = collections.Counter(nums)
        minheap = []
        for num, cnt in counter.items():
            item = (cnt, num)
            heapq.heappush(minheap, item)
            if len(minheap) > k:
                heapq.heappop(minheap)

        ans = []
        while len(minheap) != 0:
            ans.append(heapq.heappop(minheap)[1])
        return ans[::-1]


class Solution_bucket(object):
    def topKFrequent(self, A, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        if A is None or len(A) == 0 or  k == 0:
            return []

        n = len(A)
        hs = defaultdict(int)
        bucket = defaultdict(list)

        for a in A:
            hs[a] += 1

        for key, v in hs.items():
            bucket[v].append(key)
        print(bucket)

        ans = []
        while n:

            if n in bucket:

                while bucket[n]:
                    print(n, bucket[n], k)
                    ans.append(bucket[n].pop())
                    k -= 1

                    if k == 0:
                        return ans
            n -= 1


class Solution_bucket2(object):
    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        size = len(nums)
        counter = collections.Counter(nums)
        bucket = [[] for _ in range(size + 1)]

        for num, freq in counter.items():
            bucket[freq].append(num)
        print(bucket)
        ans = []
        for i in range(len(bucket) - 1, -1, -1):
            while len(bucket[i]):
                ans.append(bucket[i].pop())
                if len(ans) == k:
                    return ans
        return ans

def main():
    A = [1, 1, 1, 2, 2, 3]
    k = 2
    A = [3, 0, 1, 0]
    k = 1
    a = Solution_bucket2().topKFrequent(A, k)
    print("ans:", a)

if __name__ == '__main__':
    main()
