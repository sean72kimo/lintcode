import collections
import heapq
class Solution(object):
    def topKFrequent(self, words, k):
        """
        :type words: List[str]
        :type k: int
        :rtype: List[str]
        """
        heap = []
        count = collections.Counter(words)
        print(count)
        n = len(words)
        for wrd, frq in count.items():
            heapq.heappush(heap, (frq, wrd))
            heap.append((-frq, wrd))
        heapq.heapify(heap)
        ans = []
        for _ in range(k):
            ans.append(heapq.heappop(heap)[1])

        return ans

    def topKFrequent2(self, words, k):
        """
        :type words: List[str]
        :type k: int
        :rtype: List[str]
        """
        heap = []
        count = collections.Counter(words)
        print(count)
        n = len(words)
        for wrd, frq in count.items():

            if len(heap) > k:
                if heap[0][0] < frq:
                    heapq.heappop(heap)

            heapq.heappush(heap, (frq, wrd))

        print(len(heap))
        ans = []
#         for _ in range(k):
#             ans.append(heapq.heappop(heap)[1])

        return ans

def main():
    words = ["i", "love", "leetcode", "i", "love", "coding"]
    k = 2
    a = Solution().topKFrequent2(words, k)
    print("ans:", a)


if __name__ == '__main__':
    main()
