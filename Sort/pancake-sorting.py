from typing import List


class Solution:

    def flip(self, arr, i, j):
        while i < j:
            arr[i], arr[j] = arr[j], arr[i]
            i += 1
            j -= 1

    def pancakeSort(self, nums: List[int]) -> List[int]:
        if not nums or len(nums) == 1:
            return nums
        n = len(nums)
        ans = []
        for i in range(n - 1, -1, -1):
            max_idx = 0
            for j in range(i, -1, -1):
                if nums[j] > nums[max_idx]:
                    max_idx = j

            ans.append(max_idx + 1)
            ans.append(i + 1)
            self.flip(nums, 0, max_idx)
            self.flip(nums, 0, i)

        return ans


class FlipTool:

    @classmethod
    def flip(cls, arr, i):
        arr[:i+1] = arr[:i+1][::-1]
        return arr

class Solution:
    """
    @param array: an integer array
    @return: nothing
    """
    def pancakeSort(self, array):
        for target in range(len(array) - 1, 0, -1):
            for i in range(target, 0, -1):
                if array[i] > array[0]:
                    FlipTool.flip(array, i)

            FlipTool.flip(array, target)
array = [4,2,3]
array = [6,11,10,12,7,23,20]
Solution().pancakeSort(array)
print(array)



class Solution:
    def pancakeSort(self, A: List[int]) -> List[int]:
        """ sort like bubble-sort
            sink the largest number to the bottom at each round
        """
        def flip(sublist, k):
            i = 0
            while i < k / 2:
                sublist[i], sublist[k-i-1] = sublist[k-i-1], sublist[i]
                i += 1

        ans = []
        value_to_sort = len(A)
        while value_to_sort > 0:
            # locate the position for the value to sort in this round
            index = A.index(value_to_sort)

            # sink the value_to_sort to the bottom,
            #   with at most two steps of pancake flipping.
            if index != value_to_sort - 1:
                # flip the value to the head if necessary
                if index != 0:
                    ans.append(index+1)
                    flip(A, index+1)
                # now that the value is at the head, flip it to the bottom
                ans.append(value_to_sort)
                flip(A, value_to_sort)

            # move on to the next round
            value_to_sort -= 1

        return ans

arr = [3,2,4,7]
oper = Solution().pancakeSort(arr)
print("ans:", oper, arr)

