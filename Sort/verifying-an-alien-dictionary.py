import collections
from typing import List



"""
Time complexity
M: all chars count for word in words
N: size of order
O(N) + O(M)
主要邏輯裡，掃過每個word的每個char兩次，所以time complexity為O(M)
"""
class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:
        mp = collections.defaultdict(int)

        for i in range(len(order)):
            ch = order[i]
            mp[ch] = i

        for i in range(len(words) - 1):
            word1 = words[i]
            word2 = words[i + 1]

            for j in range(len(word1)):
                if j >= len(word2):
                    return False

                if mp[word1[j]] == mp[word2[j]]:
                    continue
                elif mp[word1[j]] < mp[word2[j]]:
                    break
                elif mp[word1[j]] > mp[word2[j]]:
                    return False

        return True