import bisect

from sortedcontainers import SortedList


class MyCalendar:

    def __init__(self):
        self.calendar = SortedList()

    def book(self, start: int, end: int) -> bool:
        if len(self.calendar) == 0:
            self.calendar.add((start, end))
            return True
        i = bisect.bisect_right(self.calendar, (start, end))

        if i > 0 and self.calendar[i - 1][1] > start:
            return False

        if i < len(self.calendar) and self.calendar[i][0] < end:
            return False

        self.calendar.add((start, end))
        return True

# Your MyCalendar object will be instantiated and called as such:
# obj = MyCalendar()
# param_1 = obj.book(start,end)


ops = ["MyCalendar","book","book","book","book","book","book","book","book","book","book"]
val = [[],[47,50],[33,41],[39,45],[33,42],[25,32],[26,35],[19,25],[3,8],[8,13],[1,4]]
exp = [None,True,True,False,False,True,False,True,True,True,False]

ops = ["MyCalendar","book","book"]
val = [[],[10,20],[19,30]]
exp = [None, True, False, True]

a = []
calendar = None
for op, v in zip(ops, val):
    if op =="MyCalendar":
        calendar = MyCalendar()
        a.append(None)
    if op == "book":
        ret = calendar.book(v[0],v[1])
        a.append(ret)

print("ans:", a == exp, a)