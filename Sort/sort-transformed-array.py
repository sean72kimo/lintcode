class Solution(object):

    def sortTransformedArray(self, nums, a, b, c):
        """
        :type nums: List[int]
        :type a: int
        :type b: int
        :type c: int
        :rtype: List[int]
        """
        ans = []
        left = 0
        right = len(nums) - 1

        while left <= right:
            l = self.calculate(nums[left], a, b, c)
            r = self.calculate(nums[right], a, b, c)
            print((left, right), l , r)
            if a >= 0:
                if l > r:
                    ans.append(l)
                    left += 1
                else:
                    ans.append(r)
                    right -= 1

            elif a < 0:
                if l > r:
                    ans.append(l)
                    left += 1
                else:
                    ans.append(r)
                    right -= 1

        ans = ans[::-1]

        return ans

    def calculate(self, x, a, b, c):
        return a * x * x + b * x + c


nums = [-4, -2, 2, 4]
a = 1
b = 3
c = 5
a = Solution().sortTransformedArray(nums, a, b, c)
print("ans", a)
