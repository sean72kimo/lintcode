class Solution:
    # @param k & A a integer and an array
    # @return ans a integer
    def kthLargestElement(self, k, A):
        n = len(A)
        if n == 0:
            return 0
        return self.quickselect(k, A, 0, n - 1)
        return self.partition(k, A, 0, n - 1)

    def partition(self, k, A, start, end):
        if start == end:
            return A[start]

        i = start
        j = end
        m = (start + end) // 2
        p = A[m]

        while i <= j :
            while i <= j and A[i] > A[m]:
                i += 1
            while i <= j and A[j] < A[m]:
                j -= 1
            if i <= j:
                A[i], A[j] = A[j], A[i]
                i += 1
                j -= 1

        if start + k - 1 <= j:
            return self.partition(k, A, start, j)
        if start + k - 1 >= i:
            return self.partition(k - (i - start), A, i, end)

        return A[j + 1]


    def quickselect(self, k, A, start, end):
        if start == end:
            return A[start]

        i = start
        j = end
        m = (start + end) // 2
        pivot = A[m]

        while i <= j:

            while i <= j and A[i] < pivot:
                i += 1
            while i <= j and A[j] > pivot:
                j -= 1

            if i <= j:
                A[i], A[j] = A[j], A[i]
                i += 1
                j -= 1

        if start + k - 1 <= j:
            return self.quickselect(k, A, start, j)

        if start + k - 1 >= i:
            return self.quickselect(k - (i - start), A, i, end)

        return A[j + 1]

k = 3
A = [9, 3, 2, 4, 8]

a = Solution().kthLargestElement(k, A)
print('ans:', a, A)

