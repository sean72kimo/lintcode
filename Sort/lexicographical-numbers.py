class Solution(object):
    def lexicalOrder(self, n):
        ans = []
        for i in range(1, 10):
            self.dfs(i, n, ans)
        return ans

    def dfs(self, curr, n, ans):
        print(curr)
        if curr > n:
            return

        ans.append(curr)

        for i in range(10):
            nxt = curr * 10 + i

            self.dfs(nxt, n, ans)

class Solution_loop(object):
    def lexicalOrder(self, n):
        ans = []
        curr = 1
        for i in range(n):
            ans.append(curr)
            if curr * 10 <= n:
                curr = curr * 10
            else:
                if curr >= n:
                    curr = curr // 10
                curr += 1
                while curr % 10 == 0:
                    curr = curr // 10
        return ans

from functools import cmp_to_key
class Solution_Sort(object):  # TLE
    def lexicalOrder(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        arr = [str(i) for i in range(1, n + 1)]

        arr.sort(key = cmp_to_key(self.mycmp))
        arr = map(int, arr)
        return list(arr)

    def mycmp(self, a, b):
        if len(a) == len(b):
            if a > b:
                return 1
            elif a < b:
                return -1
            else:
                return 0

        for i in range(min(len(a), len(b))):
            if a[i] > b[i]:
                return 1
            elif a[i] < b[i]:
                return -1
            else:
                continue

        if len(a) > len(b):
            return 1
        else:
            return -1

n = 13
# a = Solution_Sort().lexicalOrder(n)
# print("ans:", a)
# b = Solution_loop().lexicalOrder(n)
# print("ans:", b)
c = Solution().lexicalOrder(n)
print("ans:", c)

