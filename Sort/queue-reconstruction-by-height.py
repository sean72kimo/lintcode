class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        # this sorts the list by height where the largest heights are first.
        # each group of heights is also sorted in ascending order
        # (e.g. [[7,0], [7,1], [6,1], [5,0], [5,2], [4,4]] would be the
        # resulting list after sorting the default test case for run code)

        # by inserting each person into the new list using k as the insertion
        # index and by starting with the tallest people we leverage the what
        # insertion does: push every element behind it back 1. By inserting
        # from tallest to shortest, we make sure that, at the time of insertion,
        # everything is being put into the list with exactly k people in front
        # of them that are taller or equal in height
        """
        if not people:
            return []

        ans = []

        def mykey(x):
            return -x[0], x[1]

        line = sorted(people, key=mykey)

        for h, p in line:
            ans.insert(p, [h, p])

        return ans



def mycmp(x, y):
    print(x)
    return x

def cmp_to_key(mycmp):
    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0

        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0

    return K

class Solution(object):
    def reconstructQueue(self, people):
        if not people: return []

        # obtain everyone's info
        # key=height, value=k-value, index in original array
        peopledct, height, res = {}, [], []

        insertion_order = sorted(people, key = lambda (x, y): (-x[0], x[1]))
        # people.sort(reverse = True, key = lambda x: x[0])
        print(insertion_order)
        # print(people)
        ordered_line = []

        for person in people:
            ordered_line.insert(person[1], person)


        return ordered_line
ppl = [[7, 1], [4, 4], [7, 0]]

print('ans:', Solution().reconstructQueue(ppl))
