from functools import cmp_to_key


class Solution(object):

    def findMinArrowShots(self, points):
        """
        :type points: List[List[int]]
        :rtype: int
        
        按照end 排序
        如果下一顆氣球的起始位置 <= end, 則代表可以用同一支箭射穿
        如果下一個氣球的起始位置 > end, 則需要用另外一支箭，並更新end
        """

        if len(points) == 0:
            return 0

        n = len(points)

        def mykey(x):
            return x[1]

        points.sort(key=mykey)

        ans = 1
        end = points[0][1]
        for i in range(1, n):
            if points[i][0] <= end:
                continue
            else:
                end = points[i][1]
                ans += 1
        return ans


class Solution2(object):

    def findMinArrowShots(self, points):
        """
        :type points: List[List[int]]
        :rtype: int
        """
        n = len(points)
        END = 1
        START = 0

        if n == 0 :
            return 0

        def mycmp(a, b):
            print(a, b)
            return a[0] - b[0]

        points.sort(key=cmp_to_key(mycmp))

        ans = 1
        end = points[0][END]
        for i in range(1, n):
            if points[i][START] <= end:
                end = min(points[i][END], end)
            else:
                ans += 1
                end = points[i][END]

        return ans


points = [[10, 16], [2, 8], [1, 6], [7, 12]]
# points = [[1, 2], [2, 3], [3, 4], [4, 5]]
ans = Solution().findMinArrowShots(points)
print('ans:', ans)

