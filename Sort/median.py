class Solution:
    """
    @param: : A list of integers
    @return: An integer denotes the middle number of the array
    """

    def median(self, nums):
        # write your code here
        if not nums or len(nums) == 0:
            return 0

        n = len(nums)

        k = n // 2
        if n % 2:
            k = k + 1

        return self.quickselect(nums, 0, len(nums) - 1 , k)

    def quickselect(self, nums, start , end, k):
        if start == end:
            return nums[start]


        i = start
        j = end
        m = (start + end) // 2
        pivot = nums[m]
        print("======================")
        print("[{},{}] m:{}, p:{}".format(start, end, m, pivot))
        print(nums)

        while i <= j:

            while i <= j and nums[i] < pivot:
                i += 1
            while i <= j and nums[j] > pivot:
                j -= 1

            if i <= j:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1
                j -= 1
                print(nums)

        if start + k - 1 <= j:
            return self.quickselect(nums, start, j, k)

        if start + k - 1 >= i:
            return self.quickselect(nums, i, end, k - (i - start))

        return nums[j + 1]


nums = [4, 5, 1, 2, 3]
nums = [2, 3, 3, 2, 2, 2, 1, 1]
a = Solution().median(nums)
print(nums)
print("median:", a)
