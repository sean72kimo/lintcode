from copy import deepcopy
class Solution:
    # @param {int[]} A an integer array
    # @return nothing
    def sortIntegers(self, A):
        # Write your code here
        if not A:
            return

        temp = [0] * len(A)

        self.mergeSort(A, 0, len(A) - 1, temp)
        return A

    def mergeSort(self, A, start, end, temp):
        mid = int((start + end) / 2)
        print(start, mid, end)
        if start >= end:
            return

        self.mergeSort (A, start, mid, temp)
        self.mergeSort (A, mid + 1, end, temp)
        self.merge(A, start, mid, end, temp)



    def merge(self, A, start, mid, end, temp):

        left = start
        right = mid + 1
        index = start  # for temp

        while left <= mid and right <= end:
            if (A[left] < A[right]):
                temp[index] = A[left]
                left += 1
                index += 1
            else:
                temp[index] = A[right]
                right += 1
                index += 1

        while left <= mid:
            temp[index] = A[left]
            left += 1
            index += 1

        while right <= end:
            temp[index] = A[right]
            right += 1
            index += 1

        for i in range(start, end + 1):
            A[i] = deepcopy(temp[i])



given = [6, 5, 3, 1, 8, 7, 2, 4]
# given = [3,2,1,5,4]
sol = Solution()
# print("ans:", sol.sortIntegers(given))




