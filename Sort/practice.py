class Solution:
    def sortIntegers2(self, A):

        merged = self.mergeSort(A)
        print(merged)
        A[:] = merged[:]


    def mergeSort(self, A):

        if len(A) <= 1:
            return A


        mid = len(A) // 2
        print(A[:mid], A[mid:])

        left = self.mergeSort(A[:mid])
        right = self.mergeSort(A[mid:])
        merged = self.merge(left, right)

        print("({} + {} = {})".format(left, right, merged))
        return merged

    def merge(self, A, B):
        temp = []
        l = 0
        r = 0
        while l < len(A) and r < len(B):
            if A[l] < B[r]:
                temp.append(A[l])
                l += 1
            else:
                temp.append(B[r])
                r += 1
        temp += A[l:]
        temp += B[r:]
        return temp

#         if l != len(A):
#             temp += A[l:]
#
#         if r != len(B):
#             temp += B[r:]


A = [3, 2, 1, 4, 5]
Solution().sortIntegers2(A)
print(A)




from functools import cmp_to_key
def mycmp_down(a, b):
    # return b - a

    if a > b:
        return -1
    elif a < b:
        return 1
    else:
        return 0

def mycmp_up(a, b):
    return a - b



inputs = [2, 15, 36, 45, 9, 29, 16, 23, 4, 9]

# inputs.sort(key = cmp_to_key(mycmp_acc))
inputs.sort(key = cmp_to_key(mycmp_down))

print('ans:', inputs)
