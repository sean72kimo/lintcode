import collections


class Solution:
    """
    while cnt and cnt in vst:
    when cnt > 0，代表有字符，此時 cnt -= 1才有意義
    """
    def minDeletions(self, s: str) -> int:
        counter = collections.Counter(s)
        max_freq = max(counter.values())
        vst = set()

        ans = 0
        for ch, cnt in counter.items():  # max 26 loop

            while cnt and cnt in vst:
                cnt -= 1
                ans += 1

            vst.add(cnt)

        return ans
s = "abcabc"
e = 3

s = "bbcebab"
e = 2
a = Solution().minDeletions(s)
print("ans:", a==e, a)