# https://leetcode.com/discuss/interview-question/381172/google-phone-screen-sort-a-2d-array
class Solution:
    def sort2DArray(self, A):
        m, n = len(A), len(A[0])
        self.helper(0, m * n - 1, m, n)

    def helper(self, start, end, m, n):
        if start >= end:
            return

        pivot = A[start // n][start % n]

        i, j = start + 1, end

        while i <= j:
            if A[i // n][i % n] < pivot:
                i += 1
            else:
                A[i // n][i % n], A[j // n][j % n] = A[j // n][j % n], A[i // n][i % n]
                j -= 1
        A[start // n][start % n], A[(i - 1) // n][(i - 1) % n] = A[(i - 1) // n][(i - 1) % n], A[start // n][start % n]
        self.helper(start, i - 1, m, n)
        self.helper(i, end, m, n)



class Solution2:
    def sort2DArray(self, A):
        m, n = len(A), len(A[0])
        self.helper(0, m * n - 1, m, n)

    def helper(self, start, end, m, n):
        if start >= end:
            return
        print(start, end, n, (start // n, start % n))
        # pivot = A[start // n][start % n]
        k = (start + end) // 2
        pivot = A[k // n][k % n]

        i, j = start, end

        while i <= j:
            while i <= j and  A[i // n][i % n] < pivot:
                i += 1

            while i <= j and A[j // n][j % n] > pivot:
                j -= 1

            if i <= j:
                A[i // n][i % n], A[j // n][j % n] = A[j // n][j % n], A[i // n][i % n]
                i += 1
                j -= 1

        self.helper(start, j, m, n)
        self.helper(i, end, m, n)

A = [
    [5, 12, 17, 21, 23],
    [1, 2, 4, 6, 8],
    [12, 14, 18, 19, 27],
    [3, 7, 9, 15, 25]
]
Solution2().sort2DArray(A)
for row in A: print(row)