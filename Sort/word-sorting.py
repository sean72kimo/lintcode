from functools import cmp_to_key
class Solution:
    """
    @param alphabet: the new alphabet
    @param words: the original string array
    @return: the string array after sorting
    """
    def wordSort(self, alphabet, words):
        # Write your code here
        myord = {}
        rank = {}
        for i, c in enumerate(alphabet):
            myord[c] = i

        cnt = 0
        for ch in alphabet:
            rank[ch] = cnt
            cnt += 1


        def mycmp(a, b):
            print(a, b)
            length = min(len(a), len(b))

            for i in range(length):
                print(a[i], b[i])
                if myord[a[i]] < myord[b[i]]:
                    return -1
                elif myord[a[i]] > myord[b[i]]:
                    return 1

            if len(a) > len(b):
                return 1
            else:
                return -1


        def mycmp2(a, b):
            length = min(len(a), len(b))
            for i in range(length):
                if a[i] != b[i]:
                    return myord[a[i]] - myord[b[i]]

            return len(a) - len(b)

        yy = sorted(words, key = cmp_to_key(mycmp2))
        return yy

alphabet = "zbadefghijklmnopqrstuvwxyc"
words = ["bca", "czb", "za", "zba", "ade"]

a = Solution().wordSort(alphabet, words)
print("ans:", a)



from functools import cmp_to_key
from string import ascii_lowercase
class Solution2:
    """
    @param alphabet: the new alphabet
    @param words: the original string array
    @return: the string array after sorting
    """
    def wordSort(self, alphabet, words):
        # Write your code here
        myord = {}
        orig_ord = {}

        for i , c in enumerate(alphabet):
            myord[i] = c

        yy = sorted(words)
        print(yy)

        ans = []
        for word in yy:
            # print(word)
            string = ""
            for w in word:
                rank = ascii_lowercase.index(w)
                new_ch = myord[rank]
                string += new_ch
            ans.append(string)
        print(ans)
alphabet = "zbadefghijklmnopqrstuvwxyc"
words = ["bca", "czb", "za", "zba", "ade"]
a = Solution2().wordSort(alphabet, words)
print("ans:", a)





