# Definition for an interval.
class Interval(object):

    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

    def __repr__(self):
        return "[{},{}]".format(self.start, self.end)


from functools import cmp_to_key


class Solution(object):

    def eraseOverlapIntervals(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        n = len(intervals)
        if n <= 1:
            return 0

        def mycmp(a, b):
            return a.start - b.start

        def mykey(x):
            return (x.start, x.end)

        inter = sorted(intervals, key=cmp_to_key(mycmp))
        inter = sorted(intervals, key=mykey)

        print(inter)

        end = inter[0].end
        cnt = 0
        for i in range(1, n):
            if inter[i].start < end:
                cnt += 1
                end = min(inter[i].end, end)
            else:
                end = inter[i].end

        return cnt


intervals = []
inputs = [ [1, 2], [2, 3], [3, 4], [1, 3] ]
inputs = [[0, 20], [1, 1], [2, 4], [2, 5], [3, 5], [4, 6]]
for s, e in inputs:
    intervals.append(Interval(s, e))
ans = Solution().eraseOverlapIntervals(intervals)
print('ans:', ans)
