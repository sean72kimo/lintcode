from functools import cmp_to_key


class Solution(object):
    def largestNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: str
        """
        if nums is None or len(nums) == 0:
            return ""

        def mycmp(a, b):
            # 303, 330
            if a + b > b + a:
                return -1
            elif a + b < b + a:
                return 1
            else:
                return 0

        tmp = sorted(map(str, nums), key=cmp_to_key(mycmp))
        if tmp[0] == '0':
            return '0'
        return "".join(tmp)


def mycmp(a, b):
    if b > a:
        return 1
    elif b < a:
        return -1
    else:
        return 0


nums = [2,6,7,1,0,8,5,4,3]
nums.sort(key=cmp_to_key(mycmp))
print(nums)
