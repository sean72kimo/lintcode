import heapq
from typing import List

class Solution(object):
    def assignBikes(self, workers, bikes):
        """
        :type workers: List[List[int]]
        :type bikes: List[List[int]]
        :rtype: List[int]
        """
        def dist(a, b):
            return abs(a[0] - b[0]) + abs(a[1] - b[1])
        pairs = [(dist(posw, posb), idxw, idxb) for idxw, posw in enumerate(workers) for idxb, posb in enumerate(bikes)]
        pairs.sort()
        bikesSeen = set()
        res = [None] * len(workers)

        for _, idxw, idxb in pairs:
            if idxb not in bikesSeen and res[idxw] == None:
                res[idxw] = idxb
                bikesSeen.add(idxb)
                if len(bikesSeen) == len(workers):
                    break

        return res

class Item:
    def __init__(self, d, worker, bike):
        self.dist = d

        self.worker_idx = worker[0]
        self.worker = worker[1]

        self.bike_idx = bike[0]
        self.bike = bike[1]

    def __lt__(self, other):
        if self.dist != other.dist:
            return self.dist < other.dist

        if self.worker_idx != other.worker_idx:
            return self.worker_idx < other.worker_idx

        return self.bike_idx < other.bike_idx

    def __repr__(self):
        return "<Itm dis:{}, worker:{}, bike:{}>".format(self.dist, self.worker_idx, self.bike_idx)


class Solution:
    def assignBikes(self, workers: List[List[int]], bikes: List[List[int]]) -> List[int]:



        heap = []
        for i, worker in enumerate(workers):
            for j, bike in enumerate(bikes):
                d = self.distance(worker, bike)
                itm = Item(d, (i, worker), (j, bike))
                heap.append(itm)
        heapq.heapify(heap)

        n = len(workers)
        ans = [None for _ in range(n)]
        vst = set()
        print(heap)
        while n:
            if len(heap) == 0:
                break

            itm = heapq.heappop(heap)

            if ('b',itm.bike_idx) in vst or ans[itm.worker_idx] is not None:
                continue

            ans[itm.worker_idx] = itm.bike_idx
            vst.add(('w', itm.worker_idx))
            vst.add(('b', itm.bike_idx))

            n -= 1

        return ans

    def distance(self, a, b):
        return abs(a[0] - b[0]) + abs(a[1] - b[1])





class Item:
    def __init__(self, d, w, b):
        self.dist = d
        self.w_idx = w
        self.b_idx = b
    def __lt__(self, other):
        if self.dist != other.dist:
            return self.dist < other.dist
        if self.w_idx != other.w_idx:
            return self.w_idx < other.w_idx
        return self.b_idx < other.b_idx
    def __repr__(self):
        return "<{}({}, {})>".format(self.dist, self.w_idx, self.b_idx)

class Solution5(object):


    def assignBikes(self, workers, bikes):

        def dist(a, b):
            return abs(a[0] - b[0]) + abs(a[1] - b[1])
        print(workers)
        heap = []
        taken = 0
        for i, worker in enumerate(workers):
            for j, bike in enumerate(bikes):
                d = dist(worker, bike)
                itm = Item(d, i, j)
                heapq.heappush(heap, itm)

        ans = [None] * len(workers)
        cnt = 0
        taken = [False] * len(bikes)
        man = [False] * len(workers)

        while len(heap):

            item = heapq.heappop(heap)
            d = item.dist
            w = item.w_idx
            b = item.b_idx
            if taken[b] or man[w]:
                continue
                # for j, bike in enumerate(bikes):
                #     if j == b:
                #         continue
                #     d = dist(workers[w], bike)
                #     itm = Item(d, w, j)
                #     heapq.heappush(heap, itm)
            else:
                taken[b] = True
                man[w] = True
                ans[w] = b
                cnt += 1
                if cnt == len(workers):
                    break
        return ans







workers = [[0,0],[2,1]]
bikes = [[1,2],[3,3]]
exp = [1,0]

workers = [[0,0],[1,1],[2,0]]
bikes = [[1,0],[2,2],[2,1]]
exp = [0,2,1]
a = Solution5().assignBikes(workers, bikes)
print("ans:", a)