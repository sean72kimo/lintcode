"""
Definition of Interval.
"""
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end


from functools import cmp_to_key
class Solution:
    """
    @param: airplanes: An interval array
    @return: Count of airplanes are in the sky.
    """
    def countOfAirplanes(self, airplanes):
        times = []

        def sorter(x , y):
            if x[0] != y[0]:
                return x[0] - y[0]
            else:
                return x[1] - y[1]

        for airplane in airplanes:
            times.append((airplane.start, 1))
            times.append((airplane.end, -1))

        times.sort(key = cmp_to_key(sorter))

        now = 0
        ans = 0
        for t, delta in times:
            now += delta
            ans = max(now, ans)

        return ans

input = [
  [1, 10],
  [2, 3],
  [5, 8],
  [4, 7]
]
airplanes = []
for s, e in input:
    airplanes.append(Interval(s, e))

a = Solution().countOfAirplanes(airplanes)
print("ans:", a)







