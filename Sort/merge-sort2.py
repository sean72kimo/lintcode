class Sort:
    def mergeSort(self, alist):
        if len(alist) <= 1:
            return alist

        mid = int(len(alist) / 2)
        left = self.mergeSort(alist[:mid])
        right = self.mergeSort(alist[mid:])

        return self.mergeSortedArray(left, right)

    # @param A and B: sorted integer array A and B.
    # @return: A new sorted integer array
    def mergeSortedArray(self, A, B):
        sortedArray = []
        l = 0
        r = 0
        while l < len(A) and r < len(B):
            if A[l] < B[r]:
                sortedArray.append(A[l])
                l += 1
            else:
                sortedArray.append(B[r])
                r += 1
        sortedArray += A[l:]
        sortedArray += B[r:]

        return sortedArray

class Solution:
    def sortIntegers(self, A):
        if len(A) <= 1:
            return A
        mid = len(A) // 2
        left = self.sortIntegers(A[:mid])
        right = self.sortIntegers(A[mid:])
        A = self.mergeSortedArray(left, right)
        return A

    def mergeSortedArray(self, A, B):
        tmp = []

        l = 0
        r = 0

        while l < len(A) and r < len(B):
            if A[l] < B[r]:
                tmp.append(A[l])
                l += 1
            else:
                tmp.append(B[r])
                r += 1

        if l < len(A):
            tmp += A[l:]
        if r < len(B):
            tmp += B[r:]

        return tmp


class Solution2:
    def sortIntegers(self, A):
        if len(A) == 0:
            return A

        self.mergeSort(A)
        print(A)


    def mergeSort(self, A):
        if len(A) <= 1:
            return A[:]
        mid = len(A) // 2
        left = self.mergeSort(A[:mid])
        right = self.mergeSort(A[mid:])
        A[:] = self.merge(left, right)
        return A

    def merge(self, A, B):
        tmp = []

        l = 0
        r = 0

        while l < len(A) and r < len(B):
            if A[l] < B[r]:
                tmp.append(A[l])
                l += 1
            else:
                tmp.append(B[r])
                r += 1

        if l < len(A):
            tmp += A[l:]
        if r < len(B):
            tmp += B[r:]

        return tmp

unsortedArray = [6, 5, 3, 1, 8, 7, 2, 4]
merge_sort = Solution2()
print("ans:", merge_sort.sortIntegers(unsortedArray))
