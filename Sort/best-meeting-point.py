"""
將每個grid[i][j] == 1的 i 和 j 紀錄下來，存在rows and cols
use rows as example
rows.sort
rows[i] ~ rows[j] 的中點就是最佳距離，距離就是 rows[j] - rows[i]
累加所有距離
縱座標執行同樣的步驟，即可找到曼哈頓距離
"""

class Solution:
    def minTotalDistance(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])

        rows = []
        cols = []
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    rows.append(i)
                    cols.append(j)

        return self.calc(rows) + self.calc(cols)

    def calc(self, nums):
        nums.sort()
        i = 0
        j = len(nums) - 1
        res = 0
        while i < j:
            res += nums[j] - nums[i]
            i += 1
            j -= 1

        return res