# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

    def __repr__(self):
        return "[{},{}]".format(self.start, self.end)
    
# class Point():
#     def __init__(self, time, type):
#         self.time = time
#         self.type = type
#     def __repr__(self):
#         type = "START"
#         if self.type == 0:
#             type = "END"
#         return "<P>({},{})".format(self.time, type)
#         
#         
#     def __lt__(self, other):
#         
#         if self.time == other.time:
#             return other.type < self.type
#         
#         return self.time < other.time
        
class Solution(object):
    def employeeFreeTime(self, schedule):
        """
        :type schedule: List[List[Interval]]
        :rtype: List[Interval]
        """
        START = 1
        END = 0
        if len(schedule) == 0:
            return []
        times = []
        
        for each in schedule:
            for itm in each:
                times.append(itm)
            
        times.sort(key = lambda x : x.start)
        ret = self.merge(times)
        print(ret)
        ans = []
        for i in range(len(ret)):
            if i == 0:
                continue
            idle = Interval(ret[i-1].end, ret[i].start)
            ans.append(idle)
        
        return ans
        
        
    def merge(self, times):
        ret = []
        for i, itm in enumerate(times):
            if len(ret) == 0:
                ret.append(itm)
                continue
            
            if itm.start > ret[-1].end:
                ret.append(itm)
            else:
                ret[-1].end = max(itm.end, ret[-1].end)
        return ret
        

        
intervals = []

schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
schedule = [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
for item in schedule:
    p = []
    for s, e in item:
        p.append(Interval(s, e))
    intervals.append(p)


    
a = Solution().employeeFreeTime(intervals)
print(a)
