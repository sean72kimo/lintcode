class Singleton:

    __instance = None
    def __new__(cls, val=None):

        if Singleton.__instance is None:
            print(object)
            Singleton.__instance = object.__new__(cls)
            Singleton.__instance.val = val
            return Singleton.__instance
        else:
            print("obj has created")
            return Singleton.__instance
        
    
s = Singleton(5)
a = Singleton(6)
b = Singleton(7)