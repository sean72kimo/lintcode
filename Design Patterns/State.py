class ComputerState:
    name = "state"
    allowed = []

    def switch(self, state):
        """ Switch to new state """
        print(self.allowed)
        if state.name in self.allowed:
            print ('Current:', self , ' => switched to new state:', state.name)
            self.state = state
        else:
            print('Current:', self , ' => switching to', state.name, 'not possible.')

    def __str__(self):
        return self.name

class Off(ComputerState):
    name = "Off"
    allowed = ['On']

class On(ComputerState):
    name = "On"
    allowed = ['Off', 'Suspend', 'Hibernate']

class Suspend(ComputerState):
    name = "Suspend"
    allowed = ['On']

class Hibernate(ComputerState):
    name = "Hibernate"
    allowed = ['On']

class Computer:
    def __init__(self):
        self.state = Off()

    def change(self, state):
        self.state.switch(state)

pc = Computer()
pc.change(On())

