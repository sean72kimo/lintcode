class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(5)

node4 = TreeNode(3)
node5 = TreeNode(4)
node6 = TreeNode(6)

root.left = node2
root.right = node3

node2.left = node4
node2.right = node5

node3.right = node6



class Solution:
    # @param root: a TreeNode, the root of the binary tree
    # @return: nothing
    prevNode = None
    
    def flatten(self, root):
        if root is None:
            return

        self.flatten(root.right)
        self.flatten(root.left)
        root.right = self.prevNode
        root.left = None
        self.prevNode = root


        

    def printNodes2(self, root):
        while root is not None:
            print(root.val, '-> ', end='', flush=True)
            root = root.right

sol = Solution()
sol.flatten(root)
sol.printNodes2(root)
print("")


