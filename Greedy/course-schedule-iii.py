"""
greedy
按照課程天數排，短天期的放前面
想修越多課，盡量選課程短的課
如果選當前課程，總計時間超過截止日，那麼取消當前課程，選下一個
"""

import heapq


class Solution_greedy:
    """
    @param courses: duration and close day of each course
    @return: the maximal number of courses that can be taken
    """

    def scheduleCourse(self, courses):
        # write your code here
        courses = sorted(courses, key=lambda x: x[1])
        curt_time = 0
        heap = []

        for duration, deadline in courses:
            curt_time += duration
            heapq.heappush(heap, -duration)
            if curt_time > deadline:
                last_session_time = -heapq.heappop(heap)
                curt_time -= last_session_time
        return len(heap)

"""
dfs + mem TLE
backtracking, 
課程按照上課天數排列，選取或者不選取該堂課，何者可以修較多的課？
"""

class Solution_dfs_TLE:
    """
    @param courses: duration and close day of each course
    @return: the maximal number of courses that can be taken
    """

    def scheduleCourse(self, courses):
        # write your code here
        courses = sorted(courses, key=lambda x: x[1])
        self.mem = {}
        return self.dfs(0, courses, 0)

    def dfs(self, i, course, curr_time):
        if (i, curr_time) in self.mem:
            return self.mem[(i, curr_time)]
        if i == len(course):
            return 0

        taken = 0
        if curr_time +  course[i][0] <= course[i][1]:
            taken = self.dfs(i+1, course, curr_time +  course[i][0]) + 1

        not_taken = self.dfs(i+1, course, curr_time)
        res = max(taken, not_taken)
        self.mem[(i, curr_time)] = res
        return res

courses = [[100, 200], [200, 1300], [1000, 1250], [2000, 3200]] #3
a = Solution_greedy().scheduleCourse(courses)
b = Solution_dfs_TLE().scheduleCourse(courses)
print(a == b, a, b)


