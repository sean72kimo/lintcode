class Solution_window(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        類似窗口的概念
        每一段分割點的最右邊元素，必定是整段分割點最大值，且絕對小於下一段元素的最小值

        所以用兩個list, left and right
        left 記錄當前點(含)，目前為止左邊最大值
        right記錄當前點(含)，目前為止右邊最小值
        如果當前點i的左邊最大值 < i+1開始的subarry右邊最小值，滿足上述的分割條件，代表找到一個切割點
        ex max(arr[:i]) < min(arr[i+1:]) 則可以分成 arr[:i] and arr[i+1:]兩段，兩段各自排序後，並不會有任何重疊
        切割段數 = 切割點 + 1
        """
        if len(arr) == 0:
            return 0
        print(arr)
        n = len(arr)
        left = [None] * n  # maxOfLeft
        right = [None] * n  # minOfRight

        left[0] = arr[0]
        for i in range(1, n):
            left[i] = max(arr[i], left[i - 1])

        right[n - 1] = arr[n - 1]
        for i in range(n - 2, -1, -1):
            right[i] = min(arr[i], right[i + 1])

        ans = 0
        for i in range(n - 1):
            if left[i] < right[i + 1]:
                ans += 1
        return ans + 1

class Solution(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        if len(arr) == 0:
            return 0

        mx = [float('-inf') for _ in range(len(arr))]

        for i in range(len(arr)):
            if i == 0:
                mx[0] = arr[0]
                continue

            mx[i] = max(mx[i - 1], arr[i])

        ans = 0
        for i in range(len(arr)):
            if mx[i] == i:
                ans += 1

        return  ans


class Solution_greedy(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        greedy 和 stack 兩個做法都是在找區間極值

        原輸入是無序的，但最後是有序的，原輸入和排序數組其實就是一個permutation
        每一個數最後都有自己排序後的位置(槽)
        找尋左手邊能夠提供的空槽數目

        ex [1,2,0,4,3]
        數字2為區間arr[0:2]最大值。他左邊需要兩個空槽需要排滿。
        i == 0, 數字2不可能放這邊，因為0的左邊沒有空槽
        i == 1, 數字2不可能放這邊，因為0的左邊只有一個空槽, i0
        i == 2, 數字2可以放這邊，因為2的左邊有兩個空槽 i0 and i1
        因此i==2為一個分割點
        """
        # A = sorted(arr)

        cnt = 0
        mx = arr[0]
        for i, n in enumerate(arr):
            mx = max(n, mx)
            if mx == i:
                cnt += 1

        return cnt


class Solution(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        if len(arr) == 0:
            return 0

        stack = []
        for i in range(len(arr)):
            maxV = arr[i]
            while len(stack) and arr[i] < stack[-1]:
                maxV = max(maxV, stack.pop())
            stack.append(maxV)
        print(stack)
        return len(stack)




arr = [1,0,2,3,4]
#  chunks are: 1, 0 | 2 | 3 | 4

arr = [1,0,3,2,4]
#  chunks are: 1, 0 | 3, 2 | 4

arr = [1,3,0,2]
#  chunks are: 1, 0, 3, 2

# arr = [1,3,0,2,4,6,5,7]
# #  chunks are: 1, 3, 0, 2 | 4 | 6, 5 | 7
#
# arr = [0,2,1,4,3,5,7,6]
# #  chunks are: 0 | 2, 1 | 4, 3 | 5 | 7, 6
#
# arr = [1,2,0,4,3]
# #  chunks are: 1, 2, 0 | 4, 3
#
# arr = [1,2,0,3]
# #  chunks are: 1, 2, 0 | 3

a = Solution().maxChunksToSorted(arr)
print("ans:", a)
