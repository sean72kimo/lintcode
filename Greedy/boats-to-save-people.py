from typing import List

"""
If the heaviest person can share a boat with the lightest person, then do so. 
Otherwise, the heaviest person can't pair with anyone, so they get their own boat.
"""
class Solution:
    def numRescueBoats(self, people: List[int], limit: int) -> int:

        people.sort()
        cnt = 0
        i = 0
        j = len(people) - 1

        while i <= j:

            if people[i] + people[j] <= limit:
                i += 1
            j -= 1
            cnt += 1

        return cnt



people = [3,5,3,4]
limit = 5
a = Solution().numRescueBoats(people, limit)
print("ans:", a)
