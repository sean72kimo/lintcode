import collections


class Solution(object):
    def minSwapsCouples(self, row):
        """
        :type row: List[int]
        :rtype: int
        """
        if len(row) == 0:
            return 0
        mp = collections.defaultdict(int)

        for i, n in enumerate(row):
            mp[n] = i

        ans = 0
        for i in range(0, len(row), 2):
            x = row[i]
            if row[i + 1] == x ^ 1:
                continue

            num = x ^ 1
            j = mp[num]
            later = row[i + 1]

            row[i + 1], row[j] = row[j], row[i + 1]
            mp[num] = i + 1
            mp[later] = j
            ans += 1

        return ans
