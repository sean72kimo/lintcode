class Solution:
    def minDominoRotations(self, A: List[int], B: List[int]) -> int:
        ra = self.check(A, B, A[0])
        rb = self.check(A, B, B[0])
        if ra == -1 and rb == -1:
            return -1
        if ra != -1 and rb != -1:
            return min(ra, rb)
        if ra != -1:
            return ra
        return rb

    def check(self, A, B, x):
        a = b = 0
        for i in range(len(A)):
            if A[i] != x and B[i] != x:
                return -1
            elif A[i] != x:
                a += 1
            elif B[i] != x:
                b += 1
        return min(a, b)