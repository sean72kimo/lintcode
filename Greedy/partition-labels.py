def toIdx(ch):
    return ord(ch) - ord('a')

def toCh(i):
    return chr(i + ord('a'))

import collections
class Solution(object):
    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        ans = []
        if len(S) == 0:
            return ans

        hsmap = collections.defaultdict(int)
        for i, ch in enumerate(S):
            hsmap[ch] = i

        right = left = 0
        for i, ch in enumerate(S):
            right = max(right, hsmap[ch])
            print(ch, right)
            if i == right:
                ans.append(i - left + 1)
                left = i + 1

        return ans



S = "ababcbacadefegdehijhklij"
a = Solution().partitionLabels(S)
print("ans:", a)
