import collections


class Solution_heap(object):

    def rearrangeString(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: str
        """
        if k == 0:
            return s

        h = [(-freq, ch) for (ch, freq) in collections.Counter(s).items()]
        heapq.heapify(h)
        res = []

        while len(res) < len(s):
            q = []
            for _ in range(k):
                if len(res) == len(s):
                    return ''.join(res)
                if not h:
                    return ''

                freq, ch = heapq.heappop(h)
                res.append(ch)
                if freq + 1 < 0:
                    q.append((freq + 1, ch))
            while q:
                heapq.heappush(h, q.pop())

        return ''.join(res)


class Solution(object):

    def rearrangeString(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: str
        """
        counter = collections.Counter(s)
        cnts = [0] * 26
        for c in s:
            cnts[ord(c) - ord('a')] += 1

        sorted_cnts = []
        for ch, cnt in counter:
            itm = (cnt, ch)
            sorted_cnts.append(itm)
        sorted_cnts.sort(reverse=True)

#         sorted_cnts = []
#         for i in range(26):
#             sorted_cnts.append((cnts[i], chr(i + ord('a'))))
#         sorted_cnts.sort(reverse=True)

        mx_cnt = max(counter.values())
        max_cnt = sorted_cnts[0][0]
        blocks = [[] for _ in range(max_cnt)]
        print(blocks)
        i = 0
        for cnt, ch in sorted_cnts:
            for _ in range(cnt):
                blocks[i].append(ch)
                i = (i + 1) % max(cnt, max_cnt - 1)

        for i in range(max_cnt - 1):
            if len(blocks[i]) < k:
                return ""
        ans = ""
        for block in blocks:
            sub = ''.join(block)
            ans += sub
        return ans


s = 'aabbc'
# s = 'aaabc'
k = 3
s = "aaadbbccxxx"
k = 2
a = Solution().rearrangeString(s, k)
print("ans:", a)
