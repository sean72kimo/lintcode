class Solution_window(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        if len(arr) == 0:
            return 0
        print(arr)
        n = len(arr)
        left = [None] * n  # maxOfLeft
        right = [None] * n  # minOfRight

        left[0] = arr[0]
        for i in range(1, n):
            left[i] = max(arr[i], left[i - 1])

        right[n - 1] = arr[n - 1]
        for i in range(n - 2, -1, -1):
            right[i] = min(arr[i], right[i + 1])

        print(left)
        print(right)
        ans = 0
        for i in range(n - 1):
            if left[i] <= right[i + 1]:
                print(i)
                ans += 1
        return ans + 1

class Solution(object):
    def maxChunksToSorted(self, arr):
        """
        :type arr: List[int]
        :rtype: int
        """
        sorted_arr = sorted(arr)
        maxx = [None] * len(arr)
        maxx[0] = arr[0]
        for i in range(1, len(arr)):
            maxx[i] = max(maxx[i - 1], arr[i])
        print("arr: ", arr)
        print("srt: ", sorted_arr)
        print("max: ", maxx)

        ans = 0
        upper = 2 ** 31 - 1
        for i in range(len(arr) - 1, -1, -1):
            if maxx[i] == sorted_arr[i]:
                if sorted_arr[i] > upper:
                    continue
                ans += 1
                upper = arr[i]
        return ans
arr = [2,1,4,4,3,5,7,6]
arr = [2,1,3,4,4]
sol = Solution_window()
a = sol.maxChunksToSorted(arr)
print("ans", a)