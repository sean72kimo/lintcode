class Solution(object):
    def canJump(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if nums is None or len(nums) == 0:
            return True

        # 一開始只能到達index 0
        # (reach的初始狀態為0，但是不須擔心，因為一開始我們就處在index 0, which is reach-able, ie, i <= reach)
        reach = 0

        for i, n in enumerate(nums):
            if i > reach:  # 如果無法到達index i，則無法獲得更多的jump，因此return False
                return False
            # 當前點 i + nums[i](跳耀距離)是否大於目前的reach? 如果大於，則取i+nums[i]
            # 如果大於reach，則取此為新的reach
            reach = max(reach, i + n)

        return True

nums = [1, 2, 1, 0, 1, 4]
a = Solution().canJump(nums)
print(a)
