
from sys import maxsize
class Solution_DP(object):
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        size = len(nums)
        # dp[i] 表示跳到位置i需要幾次跳躍
        # dp[i] = float('inf') 代表無法跳到位置 i
        f = [maxsize for _ in range(size)]
        f[0] = 0

        for i in range(1, size):
            for j in range(i):
                if j + nums[j] >= i and f[j] != maxsize: #從 j 可以跳到 i
                    f[i] = min(f[i], f[j] + 1) # dp[j] 為跳到j的步數, 然後再跳一步可以到i


        return f[-1]

"""
https://www.youtube.com/watch?v=dJ7sWiOoK7g

3,3,1,2,4,1,2,3

從i = 0, 此時能跳躍的選項有一步、兩步或是三步。也就是可以跳到3, A[1]或是1, A[2]或是2, A[3]
我們不段更新farest(貪心)至最大距離，也就是A[2]
[A[1], A[2], A[3]]就是我們下一個階段可以使用的跳躍能力
在此區間中，設定移動右邊界(r)為A[3], 左邊界(l)為A[1]，然後繼續尋找最遠的跳躍，進而找出下一個“跳躍能力區間”
看看最後當r到達len(nums)-1需要多少個step

"""
class Solution(object):
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        l = r = 0
        res = 0
        farthest = 0

        while r < len(nums) - 1:
            for i in range(l, r + 1):
                farthest = max(farthest, nums[i] + i)
            l = r + 1
            r = farthest
            res += 1

        return res


# nums = [2, 3, 1, 1, 4]
# a = Solution().jump(nums)
# print("ans:", a)
nums = [2,3,1,1,4, 5, 6]
a = Solution().jump(nums)
print("ans", a)