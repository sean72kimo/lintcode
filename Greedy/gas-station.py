class Solution(object):
    def canCompleteCircuit(self, gas, cost):
        """
        :type gas: List[int]
        :type cost: List[int]
        :rtype: int
        """
        if len(gas) == 0:
            return -1

        # 1. 如果總油量少於總花費，絕對不可能完成路徑。
        if sum(gas) < sum(cost):
            return -1

        n = len(gas)
        k = 0
        # 油箱一開始為空
        tank = 0

        # 通過上面1.的判斷，下面的邏輯絕對可以找到一個站(i)完成旅程
        # 從站0開始，計算每一站的花費，若是其中某站(i)無法完成旅程至下一站，則將油箱清空，
        # 並且我們可知，從此站(包含此站)之前，不可能找到一個起點來完成旅程。原因：https://www.youtube.com/watch?v=wDgKaNrSOEI&ab_channel=Knapsack
        # 在i後面的某站，一定含有非常多的油料來補足前面的不足 (通過1.的判斷，下面的邏輯絕對可以找到一個站(i)完成旅程)
        # 因此k = i + 1
        for i in range(n):
            tank = tank + gas[i] - cost[i]
            if tank < 0:
                tank = 0
                k = i + 1

        return k


gas = [1, 2, 3, 0]
cost = [5, 1, 1, 100]
a = Solution().canCompleteCircuit(gas, cost)
print('ans:', a)

