"""
Definition of TreeNode:
"""
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

root = TreeNode(3)
node2 = TreeNode(9)
node3 = TreeNode(20)


node4 = TreeNode(15)
node5 = TreeNode(7)

root.left = node2
root.right = node3
node3.left = node4
node3.right = node5



class Solution:

    '''
    @param root: An object of TreeNode, denote the root of the binary tree.
    This method will be invoked first, you should design your own algorithm
    to serialize a binary tree which denote by a root node to a string which
    can be easily deserialized by your own "deserialize" method later.
    '''
    def serialize(self, root):
        # write your code here
        res = []
        if root is None:
            return "{}"

        queue = [root]
        res.append(str(root.val))

        while queue:
            new_q = []

            for node in queue:

                if node.left:
                    new_q.append(node.left)
                    res.append(str(node.left.val))
                else:
                    res.append('#')


                if node.right:
                    new_q.append(node.right)
                    res.append(str(node.right.val))
                else:
                    res.append('#')

            queue = new_q

        while res[-1] == '#':
            res.pop()

        ans = '{' + ','.join(res) + '}'
        return ans

    '''
    @param data: A string serialized by your serialize method.
    This method will be invoked second, the argument data is what exactly
    you serialized at method "serialize", that means the data is not given by
    system, it's given by your own serialize method. So the format of data is
    designed by yourself, and deserialize it here as you serialize it in
    "serialize" method.
    '''
    def deserialize(self, data):
        # write your code here
        if data == '{}' or data is None:
            return None
        data = data.rstrip()
        values = data[1:-1].split(',')

        root = TreeNode(int(values[0]))
        queue = [root]
        isLeftChild = True
        index = 0

        for val in values[1:]:
            if val is not '#':
                node = TreeNode(int(val))
                if isLeftChild:
                    queue[index].left = node
                else:
                    queue[index].right = node
                queue.append(node)

            if not isLeftChild:
                index += 1
            isLeftChild = not isLeftChild

        return root



sol = Solution()
ans = sol.serialize(root)
# print('serializer:', ans)

data = '{3,9,20,#,#,15,7}'
sol.deserialize(data)