import collections
from sys import maxsize
class MinWindow(object):
    def minWindow(self, s, t):
        if not s or len(s) == 0:
            return ""
        mp = collections.Counter(t)
        count = len(t)
        print(mp)
        left = 0
        right = 0
        d = maxsize
        head = 0
        ans = ""
        """
        XADOBCAODEBANC
        ABC
        """
        while right < len(s):
            if mp[s[right]] > 0:
                count -= 1

            mp[s[right]] -= 1

            # first found a sub string includes all t
            while count == 0:
                print(count, s[left : right + 1])
                if right - left + 1 < d:
                    ans = s[left : right + 1]
                    d = right - left + 1

                # try to move left to see if I can narrow this sub string more
                mp[s[left]] += 1
                if mp[s[left]] > 0:
                    count += 1
                left += 1
                print(count)

            right += 1

        if d == maxsize:
            return ""

        return ans

    def minWindow_bad(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """

        if not s or len(s) == 0:
            return ""

        pos = collections.defaultdict(int)
        counter = collections.Counter(t)

        left = 0
        right = 0
        count = sum(counter.values())
        ans = s

        while right < len(s):
            if s[right] in counter and counter[s[right]] > 0:
                count -= 1
                counter[s[right]] -= 1
                pos[s[right]] = right


            if count == 0:
                if right - left + 1 < len(ans):
                    ans = s[left : right + 1]

                tmp = min(pos.values())
                counter[s[tmp]] += 1
                del pos[s[tmp]]
                if len(pos) == 0:
                    left = tmp
                else:
                    left = min(pos.values())
                count = 1

            right += 1

        if len(ans) == len(s) and count != 0:
            return ""
        return ans

s = "XADOBCAODEBANC"
t = "ABC"
# s = "a"
# t = "a"
# a = MinWindow().minWindow(s, t)
# print("ans:", a)


class Solution:
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """

        if len(nums) == 0:
            return 0

        for i in range(len(nums) - 2, -1, -1):
            if nums[i] < nums[i + 1]:
                pivot = i
                break

        if i == 0:
            nums[:] = nums[::-1]
            return

        for i in range(len(nums) - 1, pivot, -1):
            if nums[i] > nums[pivot]:
                nums[pivot], nums[i] = nums[i], nums[pivot]
                break

        print(nums, nums[pivot + 1:][::-1], nums[:pivot:-1], nums[len(nums) - 1:pivot:-1])
        nums[pivot + 1:] = nums[pivot + 1:][::-1]
        print(nums)

nums = [1, 2, 3, 6, 5, 4]
# Solution().nextPermutation(nums)


# compare
from functools import cmp_to_key
def mycmp(slf, othr):
#     if ord(slf) > ord(othr):
#         return 1
#     elif ord(slf) < ord(othr):
#         return -1
#     else:
#         return 0
#     print(slf, othr)
    return ord(slf) - ord(othr)


arr = ['a', 'b', 'c', 'd']

arr.sort(key = cmp_to_key(mycmp))

string = "the   sky is blue  "
lst = string.strip(' ')

