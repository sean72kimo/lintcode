class Solution(object):
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        digits = list(str(num))
        n = len(digits)
        buckets = [None for _ in range(10)]

        for i in range(n):
            v = int(digits[i])
            buckets[v] = i

        for i in range(n):
            for k in range(9, -1, -1):
                if int(digits[i]) >= k:
                    break

                if buckets[k] and buckets[k] > i:
                    digits[i], digits[buckets[k]] = digits[buckets[k]], digits[i]
                    return int(''.join(digits))

        return num


ans = Solution().maximumSwap(983683)
print('ans:', ans)
