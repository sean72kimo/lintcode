class Solution:
    """
    @param n: n
    @param k: the k-th permutation
    @return: the k-th permutation
    """
    def getPermutation(self, n, k):
        fac = [1]
        for i in range(1, n + 1):
            fac.append(fac[-1] * i)

        eligible = list(range(1, n + 1))

        per = []

        # 第 k 個數，轉為下標 k = k - 1
        # 選取第 1 位，代表每個區間有 (n - 1)! 種排列
        # n - (i + 1) 轉換第1位為第1個數, 剩下n-1個數, 求出 (n-1)!種排列, 也就是每個區間有多少個排列
        # digit = (k - 1) // fac[n - i - 1] 第k個數是在哪一個區間？ (k - 1)將k從第k轉換成下標
        # k = (k - 1) % fac[n - i - 1] + 1, 轉換idx k + 1 -> 第k個
        for i in range(n):
            digit = (k - 1) // fac[n - i - 1]

            per.append(eligible[digit])

            eligible.remove(eligible[digit])

            k = (k - 1) % fac[n - i - 1] + 1

        return "".join([str(x) for x in per])



class Solution2:
    """
    @param n: n
    @param k: the k-th permutation
    @return: the k-th permutation
    """

    def getPermutation(self, n, k):
        fac = [1]
        for i in range(1, n + 1):
            fac.append(fac[-1] * i)

        eligible = list(range(1, n + 1))

        per = []
        # 我們可以將第k全部轉換為下標j做計算，省去在第k與下標之間的轉換
        # 第 k 個數，轉為下標 k = k - 1
        j = k - 1

        # 選取第 1 位，代表每個區間有 (n - 1)! 種排列
        # n - (i + 1) 轉換第1位為第1個數, 剩下n-1個數, 求出 (n-1)!種排列, 也就是每個區間有多少個排列
        # k = (k - 1) % fac[n - i - 1] + 1, 轉換idx k + 1 -> 第k
        for i in range(n):
            digit = j // fac[n - i - 1]

            per.append(eligible[digit])

            eligible.remove(eligible[digit])

            j = j % fac[n - i - 1]
        return "".join([str(x) for x in per])

n, k = 4, 13
exp = 3124
a = Solution().getPermutation(n, k)
b = Solution2().getPermutation(n, k)
print("ans:", a, b)