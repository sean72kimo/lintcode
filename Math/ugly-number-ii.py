from heapq import heappush, heappop
class Solution:
    """
    @param: n: An integer
    @return: the nth prime number as description.
    """
    def nthUglyNumber(self, n):
        if n <= 1:
            return n

        primes = [2, 3, 5]
        heap = [1]

        for i in range(3):
            heappush(heap, primes[i])

        for _ in range(n):
            value = heappop(heap)

            for p in primes:
                new = p * value
                if new not in heap:
                    heappush(heap, new)

        return value


    def nthUglyNumber2(self, n):
        res = [1]
        i2 = i3 = i5 = 0
        while len(res) < n:
            m2 = res[i2] * 2
            m3 = res[i3] * 3
            m5 = res[i5] * 5

            m = min(m2, m3, m5)

            if m == m2:
                i2 += 1
            if m == m3:
                i3 += 1
            if m == m5:
                i5 += 1

            res.append(m)
            print(res)

        return res[n - 1]

a = Solution().nthUglyNumber(10)
print('ans:', a)
