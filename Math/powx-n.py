class Solution(object):
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if n == 0:
            return 1

        if n < 0:
            x = 1 / x
            n = -n

        digits = []

        while n > 0:
            digits.append(n & 1)
            n = n >> 1

        tmp = x
        ans = 1
        for d in digits:
            if d:
                ans *= tmp
            tmp = tmp * tmp

        return ans
a = Solution().myPow(3, 5)
print("ans:", a)
