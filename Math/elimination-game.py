class Solution(object):
    def lastRemaining(self, n):
        """
        :type n: int
        :rtype: int
        """
        a = [i for i in range(1, n + 1)]
        ans = self.dfs(a)
        return ans[0]


    def dfs(self, a):
        if len(a) == 1:
            return a

        n_a = a[1::2][::-1]
        return self.dfs(n_a)

ans = Solution().lastRemaining(24)
