class Solution(object):
    def checkPerfectNumber(self, num):
        """
        :type num: int
        :rtype: bool

        edge case num <= 1
        i * i __le__ num 繼續進行迴圈
        因為假設  i * j = num , i < j , j這個數字會在求解i的時候得到，我們不需要重複計算，不然就跟暴力解一樣了
        注意！如果 i*i = summ , 那我們只需要sum += i 一次就好，不然就重複計算兩個divisor, i了
        """
        if num <= 1:
            return False

        summ = 1
        i = 2

        while i * i <= num:
            if num % i == 0:
                summ += i
                if i * i != num:
                    summ = summ + num // i
            i += 1

        return summ == num

a = Solution().checkPerfectNumber(28)
print("ans:", a)