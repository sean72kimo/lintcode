class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        if x < 0:
            return False

        if x != 0 and x % 10 == 0:
            return False
        n = 0
        while x > n:
            n = n * 10 + x % 10
            x = x // 10

        print(x, n)
        return x == n or x == n // 10



ans = Solution().isPalindrome(10)
print(ans)



