from lib.point import Point, getPoint
import collections, heapq

class Solution:
    def findPoints(self, center, k, pp):
        heap = []
        for p in pp:
            dist = self.dist(center, p)
            heapq.heappush(heap, (-dist, p))

            if len(heap) > k:
                heapq.heappop(heap)

        ans = []
        while heap:
            dist, p = heapq.heappop(heap)
            ans.append(p)

        print(ans[::-1])
        return ans[::-1]
        
    def dist(self, a, b):
        return (a.x - b.x) ** 2 + (a.y - b.y) ** 2


pp = getPoint(n = 10)
center = Point(0,0)
k = 3
a = Solution().findPoints(center, k, pp)
print("ans:", a)
