import collections
class Point:
    def __init__(self, a = 0, b = 0):
        self.x = a
        self.y = b
    def __repr__(self):
        return "P[{},{}]".format(self.x, self.y)
class Solution(object):
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        if len(points) == 0:
            return 0

        mxPoint = 1
        for p1 in points:
            duplicate = 0
            infinite = 0
            gradient = collections.defaultdict(int)
            for p2 in points:
                dx = p1.x - p2.x
                dy = p1.y - p2.y

                if dx == 0 and dy == 0:
                    g = 'DUP'

                elif dx == 0:
                    g = '0'
                else:
                    gcd = self.gcd(dx, dy)
                    g = (dx / gcd, dy / gcd)

                gradient[g] += 1

            mxPoint = max(mxPoint, gradient['DUP'])
            for g, cnt in gradient.items():
                if g == 'DUP':
                    continue
                mxPoint = max(mxPoint, cnt + gradient['DUP'])

        return mxPoint

    def gcd(self, a, b):
        while b:
            tmp = b
            b = a % b
            a = tmp
        return a

input = [[1, 1], [2, 2], [3, 3]]

input = [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]]

input = [[1, 1], [1, 1], [1, 1]]
points = []
for x, y in input:
    points.append(Point(x, y))
a = Solution().maxPoints(points)
print("ans:", a, a == 3)

input = [[0, 0], [0, 0]]
points = []
for x, y in input:
    points.append(Point(x, y))
a = Solution().maxPoints(points)
print("ans:", a, a == 2)


input = [[1, 1], [2, 2], [3, 3]]
points = []
for x, y in input:
    points.append(Point(x, y))
a = Solution().maxPoints(points)
print("ans:", a, a == 3)


input = [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]]
points = []
for x, y in input:
    points.append(Point(x, y))
a = Solution().maxPoints(points)
print("ans:", a, a == 4)
