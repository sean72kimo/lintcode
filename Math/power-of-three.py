class Solution(object):
    def isPowerOfThree(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0:
            return False

        k = 3
        MAX = 2 ** 31 - 1
        while k < MAX:
            k = k ** 3

        return n > 0 and k % n == 0

Solution().isPowerOfThree(999)
