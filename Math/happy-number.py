class Solution(object):
    def isHappy(self, n):
        print(n)
        """
        :type n: int
        :rtype: bool
        """
        return self.dfs(n, set())

    def dfs(self, n, vst):
        if n == 0:
            return False
        if n == 1:
            return True

        val = 0
        while n:
            d = n % 10
            val += d ** 2
            n = n // 10

        if val in vst:
            return val == 1

        vst.add(val)

        return self.dfs(val, vst)

a = Solution().isHappy(123)
print("ans:", a)
