from collections import defaultdict
from copy import deepcopy
class Solution(object):
    def multiply(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        n = len(A)  # A[n][t]
        m = len(B[0])  # B[t][m]
        t = len(A[0])

        C = [[0 for j in range(m)] for i in range(n)]  # C[n][m]

        BB = defaultdict(dict)
        for i in range(t):
            # col = defaultdict(dict)
            for j in range(m):
                if B[i][j]:
                    BB[i][j] = B[i][j]
                    # col[j] = B[i][j]
                    # BB[i] = col.copy()

        for i in range(n):
            for k in range(t):
                if A[i][k] == 0:
                    continue

                for j in range(m):
                    if BB[k] and j in BB[k]:
                        C[i][j] += A[i][k] * BB[k][j]

        return C

A = [[1, 0, 0], [-1, 0, 3]]
B = [[7, 0, 0], [0, 0, 0], [0, 0, 1]]

C = Solution().multiply(A, B)
print("ans:", C)
for r in C:
    print(r)





import collections 
class Solution2:
    def multiply(self, A, B):
        
        A_row_size = len(A)
        A_col_size = len(A[0])
        B_row_size = len(B)
        B_col_size = len(B[0])

        if A_row_size == 0 or B_row_size == 0:
            return []

        if A_col_size != B_row_size:
            return []

        C = [[0 for _ in range(B_col_size)] for _ in range(A_row_size)]
        BB = collections.defaultdict(dict)
        for i in range(B_row_size):
            for j in range(B_col_size):
                if B[i][j] != 0:
                    BB[i][j] = B[i][j]
        print(BB[0])
                
        
        for i in range(A_row_size):
            for k in range(B_row_size):
                # we can skip if A[i][k] == 0
                
                for j in range(B_col_size):
                    if BB[k] and j in BB[k].keys():
                        C[i][j] += A[i][k] * BB[k][j] # eumerate the kth row of B
        return C
                
        



A = [[1, 0, 0], [-1, 0, 3]]
B = [[7, 0, 0], [0, 0, 0], [1, 0, 1]]


C = Solution2().multiply(A, B)
print("ans:", C)
for r in C:
    print(r)
