class Solution:
    def fractionToDecimal(self, numerator, denominator):
        """
        :type numerator: int
        :type denominator: int
        :rtype: str
        """
        if denominator == 0:
            return
        if numerator == 0:
            return "0"

        ans = []
        if numerator * denominator >= 0:
            sign = 1
        else:
            sign = -1

        if sign == -1:
            ans.append('-')

        numerator = abs(numerator)
        denominator = abs(denominator)
        ans.append(str(numerator // denominator))
        numerator = numerator % denominator

        if numerator == 0:
            return ''.join(ans)

        ans.append('.')

        pos = {}

        ans = ''.join(ans)
        print(ans)
        i = len(ans)
        answer = 0
        table = {}
        while numerator != 0:
            if numerator not in table:
                table[numerator] = i
            else:
                i = table[numerator]
                ans = ans[:i] + '(' + ans[i:] + ')'
                return ans
            numerator = numerator * 10
            ans += str(numerator // denominator)
            numerator %= denominator
            i += 1
        return ans
numerator = 1
denominator = 6
# numerator = 1
# denominator = 99
numerator = 3
denominator = 4
numerator = 1
denominator = 333
a = Solution().fractionToDecimal(numerator, denominator)
print("ans:", a)


print(pow(2, 31) - 1)
