import collections
class Point:
    def __init__(self, a = 0, b = 0):
        self.x = a
        self.y = b
    def __repr__(self):
        return "P[{},{}]".format(self.x, self.y)

class Solution(object):
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        if len(points) == 0:
            return 0

        mxPoint = 1
        for p1 in points:
            duplicate = 0
            infinite = 0
            gradient = collections.defaultdict(int)
            for p2 in points:
                dx = p1.x - p2.x
                dy = p1.y - p2.y

                if dx == 0 and dy == 0:
                    g = 'DUP'
                elif dx == 0:
                    g = '0'
                else:
                    gcd = self.gcd(dx, dy)
                    g = (dx / gcd, dy / gcd)

                gradient[g] += 1

            mxPoint = max(mxPoint, gradient['DUP'])
            for g, cnt in gradient.items():
                if g == 'DUP':
                    continue
                mxPoint = max(mxPoint, cnt + gradient['DUP'])

        return mxPoint

    def gcd(self, a, b):
        while b:
            tmp = b
            b = a % b
            a = tmp
        return a

class Solution:
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        if len(points) == 0:
            return 0

        mxPoint = 1
        for p1 in points:
            duplicate = 0
            infinite = 0
            gradient = collections.defaultdict(int)

            for p2 in points:
                dx = p1.x - p2.x
                dy = p1.y - p2.y

                if dx == 0 and dy == 0:
                    duplicate += 1
                    continue

                if dx == 0:
                    infinite += 1
                    continue

                gcd = self.gcd(dx, dy)
                g = (dy / gcd , dx / gcd)
                # g = np.longdouble(dy) / np.longdouble(dx)
                g = dy / dx
                gradient[g] += 1

            mxPoint = max(mxPoint, infinite + duplicate)

            for g, cnt in gradient.items():
                mxPoint = max(cnt + duplicate, mxPoint)

        return mxPoint

    def gcd(self, a, b):
        while b:
            tmp = b
            b = a % b
            a = tmp

        return a



class Solution2:
    def maxPoints(self, points):
        """
        :type points: List[Point]
        :rtype: int
        """
        size = len(points)

        if size <= 1:
            return size

        mx_count = 0

        for index1 in range(size):
            p1 = points[index1]
            gradients = collections.defaultdict(int)
            infinite_count = 0
            duplicate_count = 0

            for index2 in range(index1, size):
                p2 = points[index2]
                dx = p2.x - p1.x
                dy = p2.y - p1.y

                if dx == 0 and dy == 0:
                    print(p1, p2, duplicate_count)
                    duplicate_count += 1

                if dx == 0:
                    infinite_count += 1
                else:
                    g = dy / dx
                    gradients[g] += 1
            print(p1, gradients, infinite_count)
            mx_count = max(mx_count, infinite_count)

            for k, v in gradients.items():
                v += duplicate_count
                mx_count = max(mx_count, v)

        return mx_count
input = [[0, 0], [0, 0]]
input = [[1,1],[2,2],[3,3]]
input = [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]]
points = []
for x, y in input:
    points.append(Point(x, y))
a = Solution().maxPoints(points)
print("ans:", a)

# import bisect
# path = [1,2]
# print(bisect.bisect(path, 2147483647.0))
