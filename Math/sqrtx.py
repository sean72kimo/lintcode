class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        start = 1
        end = x

        while start + 1 < end:
            mid = start + (end - start) // 2
            val = mid ** 2
            if val < x:
                start = mid
            elif val > x:
                end = mid
            else:
                return mid
        if end ** 2 < x:
            return end

        return start

a = Solution().mySqrt(4)
print('ans:', a)



from math import sqrt
print(int(sqrt(8)))
