def isPali(x):
    return str(x) == str(x)[::-1]

class Solution(object):
    def largestPalindrome(self, n):
        """
        :type n: int
        :rtype: int
        """
        a = 0
        while n:
            a += 10 ** (n - 1) * 9
            n -= 1

        b = a
        x = a * b
        term = 0
        while not isPali(x):
            if term % 2:
                a -= 1
            else:
                b -= 1

            x = a * b
            term += 1
            print("{0}*{1}={2}".format(a, b, x))
        return x % 1337

class Solution2(object):
    def largestPalindrome(self, n):
        if n == 1: return 9
        if n == 2: return 987
        for a in range(2, 9 * 10 ** (n - 1)):
            hi = (10 ** n) - a
            lo = int(str(hi)[::-1])
            if a ** 2 - 4 * lo < 0: continue
            if (a ** 2 - 4 * lo) ** .5 == int((a ** 2 - 4 * lo) ** .5):
                x = (lo + 10 ** n * (10 ** n - a))
                print ("x:", x)
                return x % 1337


class Solution3(object):
    def largestPalindrome(self, n):
        """
        :type n: int
        :rtype: int
        """
        upper = 10 ** n - 1
        lower = upper // 10
        print(upper, lower)

        for i in range(upper, lower, -1):
            t = str(i)
            p  = int(t + t[::-1])

            j = upper
            while j ** 2 > p:
                print('j:',j)
                if p % j == 0:
                    print(p)
                    return p % 1337
                j -= 1

        return 9

# ans = Solution2().largestPalindrome(2)
# print('ans:', ans)
ans = Solution3().largestPalindrome(2)
print('ans:', ans)


list = 1[     3[  5[]6[] ]  2[] 4[]    ]