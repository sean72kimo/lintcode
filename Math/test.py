
class Solution2(object):
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if n < 0:
            x = 1 / x
            n = -n

        digits = []

        while n:
            digits.append(n & 1)
            n = n >> 1

        tmp = x
        ans = 1
        for d in digits:
            if d == 1:
                ans = ans * tmp
            tmp = tmp * tmp
            print(tmp)

a = Solution2().myPow(3, 5)
print("ans:", a)


