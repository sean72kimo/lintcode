from typing import List
import collections
class Solution:
    def minAreaRect(self, points: List[List[int]]) -> int:
        columns = collections.defaultdict(list)

        for x, y in points:
            columns[y].append(x)

        lastx = {}
        ans = float('inf')
        print(columns)

        for x in sorted(columns):
            column = columns[x]
            column.sort()

            for j, y2 in enumerate(column):
                for i in range(j):
                    y1 = column[i]
                    if (y1, y2) in lastx:
                        ans = min(ans, (x - lastx[y1, y2]) * (y2 - y1))
                    lastx[y1, y2] = x
        print()
        if ans == float('inf'):
            return 0
        return ans


points = [[1,1],[1,3],[3,1],[3,3],[2,2]]
# a = Solution().minAreaRect(points)
# print("ans:", a)
