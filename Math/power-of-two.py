class Solution(object):
    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0 or n < 0:
            return False
        if n == 1:
            return True

        return n & (n-1) == 0

class Solution(object):
    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0 or n < 0:
            return False
        if n == 1:
            return True
        MAX = 2 ** 31 - 1

        k = 2
        while k < MAX:
            k = k ** 2

        return k % n == 0

a = Solution().isPowerOfTwo(8)
print("ans:", a)


class Solution_bit(object):
    def isPowerOfTwo(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n == 0:
            return False
        if n == 1:
            return True
        b = format(n, 'b')
        if '1' in b[1:]:
            return False
        return True

k = 4
while k < 2**31-1:
    k = k ** 4
print(k, k%8)