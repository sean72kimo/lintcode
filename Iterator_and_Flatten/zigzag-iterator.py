import collections

# Time:  init: O(n) / hasNext O(1) / next O(1)
# Space: O(n1+n2)
class ZigzagIterator1(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self.v = []

        if len(v1) and len(v2):
            for i, (a, b) in enumerate(zip(v1, v2)):
                self.v.append(a)
                self.v.append(b)

            if i + 1 != len(v1):
                self.v.extend(v1[i + 1:])
            elif i + 1 != len(v2):
                self.v.extend(v2[i + 1:])
        elif len(v1):
            self.v = v1
        elif len(v2):
            self.v = v2

        self.i = 0

    def next(self):
        """
        :rtype: int
        """
        if not self.hasNext():
            return -1
        val = self.v[self.i]
        self.i += 1
        return val

    def hasNext(self):
        """
        :rtype: bool
        """
        return self.i != len(self.v)


"""
similar to ZigzagIterator1 but put most of the process in hasNext
"""
class ZigzagIterator2(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self.v = []
        self.num = None
        if len(v1) and len(v2):
            for i, (a, b) in enumerate(zip(v1, v2)):
                self.v.append(a)
                self.v.append(b)

            if i + 1 != len(v1):
                self.v.extend(v1[i + 1:])
            elif i + 1 != len(v2):
                self.v.extend(v2[i + 1:])
        elif len(v1):
            self.v = v1
        elif len(v2):
            self.v = v2

        self.i = 0

    def next(self):
        """
        :rtype: int
        """
        val = self.num
        self.num = None
        return val

    def hasNext(self):
        """
        :rtype: bool
        """

        if self.i >= len(self.v):
            return False

        if self.num is None:
            self.num = self.v[self.i]
            self.i += 1

        if self.num is not None:
            return True
        return False


# Time:  # Time:  init: O(1) / hasNext O(1) / next O(1)
# Space: O(1)
class ZigzagIterator(object):

    def __init__(self, v1, v2):
        """
        Initialize your data structure here.
        :type v1: List[int]
        :type v2: List[int]
        """
        self.q = collections.deque([])
        if len(v1):
            itm = (v1, 0, len(v1))
            self.q.append(itm)

        if len(v2):
            itm = (v2, 0, len(v2))
            self.q.append(itm)

        self.elm = None

    def next(self):
        """
        :rtype: int
        """
        if not self.hasNext():
            return -1

        v, i, size = self.q.popleft()
        ni = i + 1
        if ni == size:
            pass
        else:
            itm = (v, ni, size)
            self.q.append(itm)

        return v[i]

    def hasNext(self):
        """
        :rtype: bool
        """
        return len(self.q) != 0





v1 = [1, 2]
v2 = [3, 4, 5, 6]

# v1 = []
# v2 = []

i, v = ZigzagIterator(v1, v2), []
while i.hasNext():
    n = i.next()
    v.append(n)
print("ans:", v)

