import collections

class Vector2D_deque(object):

    def __init__(self, vec2d):
        """
        :type v: List[List[int]]
        """
        self.vec2d = vec2d
        itm = [self.vec2d[0], 0]
        self.que = []
        self.que.append(itm)
        self.row = 0
        self.num = None


    def next(self):
        """
        :rtype: int
        """
        if self.num is None:
            self.hasNext()


        num = self.num
        self.num = None
        return num


    def hasNext(self):
        """
        :rtype: bool
        """

        if len(self.que):

            vec, i = self.que[-1]

            while i == len(vec):
                self.que.pop()
                self.row += 1

                while self.row != len(self.vec2d) and len(self.vec2d[self.row]) == 0:
                    self.row += 1

                if self.row == len(self.vec2d):
                    return False

                itm = [self.vec2d[self.row], 0]
                self.que.append(itm)

            self.num = vec[i]
            self.que[-1][1] += 1
            return True
        return False



class Vector2D_pointers(object):

    def __init__(self, vec2d):
        """
        :type v: List[List[int]]
        """
        self.grid = vec2d
        self.row = 0
        self.col = -1
        self.num = None

    def next(self):
        """
        :rtype: int
        """

        if self.num is None:
            self.hasNext()

        num = self.num
        self.num = None
        return num


    def hasNext(self):
        """
        :rtype: bool
        """
        if len(self.grid) == 0 or self.row == len(self.grid):
            return False
        if self.num:
            return True

        self.col += 1
        if self.col >= len(self.grid[self.row]):
            self.row += 1
            self.col = 0

        while self.row < len(self.grid) and len(self.grid[self.row]) == 0:
            self.row += 1
            self.col = 0

        if self.row == len(self.grid):
            return False

        self.num = self.grid[self.row][self.col]
        return True

# Your Vector2D object will be instantiated and called as such:
# obj = Vector2D(v)
# param_1 = obj.next()
# param_2 = obj.hasNext()


ops = ["Vector2D","next","next","next","hasNext","hasNext","next","hasNext"]
val = [[[[1,2],[3],[4]]],[None],[None],[None],[None],[None],[None],[None]]
exp = [None,1,2,3,True,True,4,False]

ans = []
for i, op in enumerate(ops):
    if op == "Vector2D":
        obj = Vector2D_deque(val[i][0])
        ans.append(None)
    elif op == "next":
        ans.append(obj.next())
    elif op == "hasNext":
        ans.append(obj.hasNext())

print("ans:", ans == exp, ans)

ans = []
for i, op in enumerate(ops):
    if op == "Vector2D":
        obj = Vector2D_pointers(val[i][0])
        ans.append(None)
    elif op == "next":
        ans.append(obj.next())
    elif op == "hasNext":
        ans.append(obj.hasNext())

print("ans:", ans == exp, ans)