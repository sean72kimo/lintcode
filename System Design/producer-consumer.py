from threading import Thread, Lock, Condition
import time
import random

queue = []
lock = Lock()

# Producer keeps on adding to the queue
class ProducerThread(Thread):
    global queue
    def run(self):
        while True:
            num = random.choice(range(1, 6))
            lock.acquire()
            queue.append(num)
            print ("Produced", num)
            lock.release()
            time.sleep(random.random())


# Consumer keeps on removing from the queue
class ConsumerThread(Thread):
    def run(self):
        global queue
        while True:
            lock.acquire()
            if not queue:
                print('IndexError: pop from empty list')
            num = queue.pop(0)
            print ("Consumed", num)
            lock.release()
            time.sleep(random.random())


condition = Condition()
class ProducerCondition(Thread):
    def run(self):
        while True:
            num = random.choice(range(1, 6))
            condition.acquire()
            queue.append(num)
            print ("Produced", num)
            condition.notify()
            condition.release()
            time.sleep(random.random())


# Consumer keeps on removing from the queue
class ConsumerCondition(Thread):
    def run(self):
        while True:
            condition.acquire()
            if not queue:
                print("wait for notification")
                condition.wait()
            num = queue.pop(0)
            print ("Consumed", num)
            condition.release()
            time.sleep(random.random())

ProducerCondition().start()
ConsumerCondition().start()


# start one producer thread and one consumer thread
# ProducerThread().start()
# ConsumerThread().start()
