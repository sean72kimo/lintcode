import collections
class RateLimiter:
    def __init__(self):
        self.bucket = collections.Counter()
        
    def is_ratelimited(self, timestamp, event, rate, increment):
        print(self.bucket)
        limited = False
        start = rate.find("/")
        total_time = int(rate[:start])
        type = rate[start+1:]

        key = event + "#" + str(timestamp)
        self.bucket[key] += 1
        #print(self.bucket)
        cnt = 0

        for t in range(60):
            prev = timestamp - t
            key = event + "#" + str(prev)
            cnt = cnt + self.bucket[key]
            #if timestamp == 65: print(self.bucket[key])
        
        if cnt <= total_time:
            limited = False
        else:
            limited = True
        if timestamp == 65: print("at", timestamp, cnt, limited)
        return limited



class RateLimiter2:

    def __init__(self):
        # do some intialize if necessary
        self.mp = {}

    # @param {int} timestamp the current timestamp
    # @param {string} event the string to distinct different event
    # @param {string} rate the format is [integer]/[s/m/h/d]
    # @param {boolean} increment whether we should increase the counter
    # @return {boolean} true or false to indicate the event is limited or not
    def is_ratelimited(self, timestamp, event, rate, increment):
        # Write your code here
        start = rate.find("/")
        total_time = int(rate[:start])
        type = rate[start+1:]
        
        time = 1
        if type == 'm':
            time *= 60
        elif type == 'h':
            time = time * 60 * 60
        elif type == 'd':
            time = time * 60 * 60 * 24
        last_time = timestamp - time + 1
        
        if event not in self.mp:
            self.mp[event] = []

        rt = self.find_event(self.mp[event], last_time) >= total_time
        if increment and not rt:
            self.insert_event(self.mp[event], timestamp)

        return rt

    def insert_event(self, event, timestamp):
        event.append(timestamp)

    def find_event(self, event, last_time):
        l, r = 0, len(event) - 1
        if r < 0 or event[r] < last_time:
            return 0
    
        ans = 0
        while l <=r:
            mid = (l + r) >> 1
            if event[mid] >= last_time:
                ans = mid
                r = mid - 1
            else:
                l = mid + 1
        return len(event) - 1 - ans + 1 
    
limiter = RateLimiter()

print(limiter.is_ratelimited(1, "login", "3/m", True) is False)
print(limiter.is_ratelimited(11, "login", "3/m", True) is False)
print(limiter.is_ratelimited(21, "login", "3/m", True) is False)
print(limiter.is_ratelimited(30, "login", "3/m", True) is True)
print(limiter.is_ratelimited(65, "login", "3/m", True) is False)
print(limiter.is_ratelimited(300, "login", "3/m", True) is False)
