from threading import Thread, Condition
import time
import random
condition = Condition()
queue = []
class ConsumerThread(Thread):
    def run(self):
        global queue
        while True:
            condition.acquire()

            # Check if the queue is empty before consuming. If yes then call wait() on condition instance.
            # wait() blocks the consumer and also releases the lock associated with the condition.
            # This lock was held by consumer, so basically consumer loses hold of the lock.
            # Now unless consumer is notified, it will not run.
            if not queue:
                print ("Nothing in queue, consumer is waiting")
                condition.wait()
                print ("Producer added something to queue and notified the consumer")
            num = queue.pop(0)
            print ("Consumed", num)
            condition.release()
            time.sleep(random.random())

class ProducerThread(Thread):
    def run(self):
        nums = range(5)
        global queue
        while True:
            # Producer can acquire the lock because lock was released by consumer
            condition.acquire()

            # Producer puts data in queue and calls notify() on the condition instance.
            num = random.choice(nums)
            queue.append(num)
            print ("Produced", num)

            # Once notify() call is made on condition, consumer wakes up. 
            # But waking up doesn't mean it starts executing. 
            # notify() does not release the lock. Even after notify(), lock is still held by producer.
            condition.notify()

            # Producer explicitly releases the lock by using condition.release().
            condition.release()

            # And consumer starts running again.
            # Now it will find data in queue and no IndexError will be raised.
            time.sleep(random.random())


ProducerThread().start()
ConsumerThread().start()