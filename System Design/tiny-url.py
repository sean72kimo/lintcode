
letters = "abcdefghij"
BASE = len(letters)
def decode(short):
    id = 0
    for i, ch in enumerate(short):
        if ch == "0":
            continue
        id = id * BASE + letters.find(ch)
    return id

def encode(id):
    string = "" 
    while id:
        string = letters[id % BASE] + string
        id = id // BASE 
    while len(string) < 6:
        string = "0" + string
    return string

short = encode(1024)
print(short)
id = decode(short)
print(id)