class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

node1 = TreeNode(3)
node2 = TreeNode(9)
node3 = TreeNode(20)
node1.left = node2
node1.right = node3

node4 = TreeNode(15)
node5 = TreeNode(7)
node3.left = node4
node3.right = node5


class Solution:
    """
    @param root: The root of binary tree.
    @return: True if this Binary tree is Balanced, or false.
    """
    def isBalanced(self, root):
        balanced, _ = self.validate(root)
        return balanced

    def validate(self, root):
        if root is None:
            return True, 0

        balanced, leftHeight = self.validate(root.left)
        if not balanced:
            return False, 0
        balanced, rightHeight = self.validate(root.right)
        if not balanced:
            return False, 0

        return abs(leftHeight - rightHeight) <= 1, max(leftHeight, rightHeight) + 1


sol = Solution()
ans = sol.isBalanced(node1)
print(ans)