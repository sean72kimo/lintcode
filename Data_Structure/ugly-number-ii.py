import collections


class Solution:
    """
    @param {int} n an integer.
    @return {int} the nth prime number as description.
    """
    def nthUglyNumber0(self, n):
        # write your code here
        uglys = [0] * n
        uglys[0] = 1

        p2, p3, p5 = 0, 0, 0

        for i in range(1, n):

            next = uglys[i - 1]

            while uglys[p2] * 2 <= next:
                p2 += 1
            while uglys[p3] * 3 <= next:
                p3 += 1
            while uglys[p5] * 5 <= next:
                p5 += 1
            num = min(uglys[p2] * 2 , uglys[p3] * 3 , uglys[p5] * 5)

            uglys[i] = num

        return uglys[n - 1]


    def nthUglyNumber(self, n):
        from heapq import heappush, heappop

        if n == 0:
            return None

        heap = [1]
        hashset = []
        uglys = [0] * n
        primes = [2, 3, 5]

        for i in range(n):
            val = heappop(heap)
            uglys[i] = val
            for j in range(3):
                item = val * primes[j]
                if item not in hashset:
                    heappush(heap, item)
                    hashset.append(item)

        return uglys[-1]




print('ans', Solution().nthUglyNumber(10))

from heapq import heappush, heappop
heap = []
data = [1, 3, 5, 7, 9, 2, 4, 6, 8, 0]
for item in data:
    heappush(heap, item)
# print(heap)
