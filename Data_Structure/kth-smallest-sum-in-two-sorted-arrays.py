"""
kth-smallest-sum-in-two-sorted-arrays

Given two integer arrays sorted in ascending order and an integer k.
Define sum = a + b, where a is an element from the first array and b is an element from the second one.
Find the kth smallest sum out of all possible sums.

Given [1, 7, 11] and [2, 4, 6].
For k = 3, return 7.
For k = 4, return 9.
For k = 8, return 15.
"""

from heapq import heappop, heappush
def print_hash(hash):
    for i in hash:
        print(i)
    print('........')

class Solution:

    """
    @param: A: an integer arrays sorted in ascending order
    @param: B: an integer arrays sorted in ascending order
    @param: k: An integer
    @return: An integer
    """
    def kthSmallestSum(self, A, B, k):
        # write your code here
        dx = [0, 1]
        dy = [1, 0]
        n = len(A)
        m = len(B)
        hash = [[False for i in range(m)] for j in range(n)]

        minHeap = [(A[0] + B[0], 0, 0)]

        for i in range(k - 1):
            sum, x, y = heappop(minHeap)

            for j in range(2):
                next_x = x + dx[j]
                next_y = y + dy[j]


                if next_x < n and next_y < m:
                    print('A[{0}] + B[{1}] = {2}'.format(next_x, next_y, A[next_x] + B[next_y]))
                    if hash[next_x][next_y] == False:
                        hash[next_x][next_y] = True
                        new_sum = A[next_x] + B[next_y]
                        item = (new_sum, next_x, next_y)
                        heappush(minHeap, item)
                    else:
                        print('  ', next_x, next_y, A[next_x] + B[next_y])

        return heappop(minHeap)[0]

A = [1, 7, 10]
B = [2, 4, 6]

k = 3
k = 4
k = 8

# A = [1, 1, 5, 4, 5, 6, 7, 10, 11, 15]
# B = [2]
# k = 5

tmp = []
for i in A:
    for j in B:
        sum = i + j
        tmp.append(sum)

tmp.sort(reverse=False)
print(tmp)
print(tmp[k - 1])

print('ans:', Solution().kthSmallestSum(A, B, k))
