from heapq import heappush, heappop, heapify
import heapq
class Solution(object):
    def nthSuperUglyNumber(self, n, primes):
        """
        :type n: int
        :type primes: List[int]
        :rtype: int
        """
        if n <= 1:
            return n

        k = len(primes)
        times = [0] * k
        uglys = [1]

        heap = []
        for i in range(k):
            heap.append((primes[i] * uglys[times[i]], i))

        heapify(heap)

        print(heap)
        print(times, uglys)
        print("====")

        while len(uglys) < n:

            umin, t = heappop(heap)
            times[t] += 1
            print(umin, t, " // times[{}] = {}".format(t, times[t]), times, uglys)
            if umin != uglys[-1]:
                uglys.append(umin)

            print(umin, "   // times[{}] = {}".format(t, times[t]), times, uglys, "primes[{}] * uglys[{}] = {} * {}".format(t, times[t], primes[t], uglys[times[t]]))
            p = primes[t] * uglys[times[t]]
            heappush(heap, (p, t))
        print(uglys)
        return uglys[-1]

nth = 12
primes = [2, 7, 13, 19]
primes = [2, 3, 5]
a = Solution().nthSuperUglyNumber(nth, primes)
print('ans:', a)
