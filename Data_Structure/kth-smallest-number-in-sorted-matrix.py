
from heapq import heappop, heappush, heappushpop
import heapq
class Solution:
    # @param matrix: a matrix of integers
    # @param k: an integer
    # @return: the kth smallest number in the matrix
    def kthSmallest(self, matrix, k):
        # write your code here

        m, n = len(matrix), len(matrix[0])

        visited = [[False for i in range(n)] for j in range(m)]
        visited = [[False] * n for _ in range(m)]
        # tuple (val, i, j)
        minHeap = [(matrix[0][0], 0, 0)]
        result = None
        visited[0][0] = True


        for _ in range(k):
            result, i , j = heappop(minHeap)

            if i + 1 < m and not visited[i + 1][j]:
                visited[i + 1][j] = True
                item = (matrix[i + 1][j], i + 1 , j)
                heappush(minHeap, item)

            if  j + 1 < n and not visited[i][j + 1] :
                visited[i][j + 1] = True
                item = (matrix[i][j + 1], i , j + 1)
                heappush(minHeap, item)

        return result


matrix = [
  [1 , 5 , 7],
  [3 , 7 , 8],
  [4 , 8 , 9],
]
k = 4
print('ans:', Solution().kthSmallest(matrix, k))
