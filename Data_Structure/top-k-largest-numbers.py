import heapq

class Solution_heap:
    '''
    @param {int[]} nums an integer array
    @param {int} k an integer
    @return {int[]} the top k largest numbers in array
    '''

    def topk(self, nums, k):
        ans = []
        minHeap = []

        for n in nums:
            heapq.heappush(minHeap, n)
            print(n, minHeap)
            if len(minHeap) > k:
                heapq.heappop(minHeap)

        return sorted(minHeap, reverse = True)

    def topk2(self, nums, k):
        # Write your code here
        heapq.heapify(nums)

        remove_len = len(nums) - k
        for i in range(remove_len):
            heapq.heappop(nums)

        return sorted(nums, reverse = True)

class Solution:  # quick-sort / quick-select
    '''
    @param {int[]} nums an integer array
    @param {int} k an integer
    @return {int[]} the top k largest numbers in array
    '''

    def topk(self, nums, k):
        # Write your code here
        kth = self.quickselect(nums, 0, len(nums) - 1, k)
        print("{}th largest: {}".format(k, kth))

        ans = []
        for n in nums:
            if n > kth:
                ans.append(n)
        ans.append(kth)
        return sorted(ans, reverse = True)


    def quickselect(self, nums, start, end, k):
        if start >= end:
            return nums[start]

        i = start
        j = end
        m = (start + end) // 2
        pivot = nums[m]

        while i <= j:
            while i <= j and nums[i] > pivot:
                i += 1

            while i <= j and nums[j] < pivot:
                j -= 1

            if i <= j:
                nums[i], nums[j] = nums[j], nums[i]
                i += 1
                j -= 1

        if start + k - 1 <= j:
            return self.quickselect(nums, start, j, k)

        if start + k - 1 >= i:
            return self.quickselect(nums, i, end, k - (i - start))

        return nums[j + 1]

nums = [3, 10, 1000, -99, 4, 100]

print(sorted(nums, reverse = True))
k = 3
sol = Solution()
print(sol.topk(nums, k))
