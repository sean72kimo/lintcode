from string import ascii_letters
class Solution(object):
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        if len(path) == 0:
            return path

        paths = path.split("/")

        stack = []

        # a b .. .. c

        for p in paths:
            if p == "." or p == "":
                continue

            if p != "..":
                stack.append(p)
            else:
                if stack:
                    stack.pop()

        return '/' + '/'.join(stack)

def main():
    path = "/a/./b/../../c/"
    path = "/a/./b/../../c/"
    path = "/home//foo/"
    a = Solution().simplifyPath(path)
    print("ans:", a)

if __name__ == "__main__":
    main()
