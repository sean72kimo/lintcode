import collections
class Solution(object):
    def leastBricks(self, wall):
        """
        :type wall: List[List[int]]
        :rtype: int
        """
        counter = collections.Counter()
        count = 0

        for row in wall:
            left = 0
            for i in range(len(row) - 1):
                left += row[i]
                counter[left] += 1
                count = max(count, counter[left])

        return len(wall) - count
wall = [[1, 2, 2, 1], [3, 1, 2], [1, 3, 2], [2, 4], [3, 1, 2], [1, 3, 1, 1]]
wall = [[1], [1], [1]]
a = Solution().leastBricks(wall)
print("ans:", a)
