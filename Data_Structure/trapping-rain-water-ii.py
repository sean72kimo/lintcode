from heapq import heappush, heappop
class Solution:
    """
    @param: heights: a matrix of integers
    @return: an integer
    """
    def trapRainWater(self, heights):
        # write your code here
        if heights is None or len(heights) == 0:
            return 0

        m = len(heights)
        n = len(heights[0])

        visited = [[0 for i in range(n)] for j in range(m)]
        # offsets = [[0, 1], [0, -1], [1, 0], [-1, 0]]
        offsets = [ (-1, 0), (0, -1), (1, 0), (0, 1) ]
        min_heap = []

        for i in range(m):
            heappush(min_heap, (heights[i][0], i , 0))
            visited[i][0] = 1
            heappush(min_heap, (heights[i][n - 1], i , n - 1))
            visited[i][n - 1] = 1

        for i in range(n):
            heappush(min_heap, (heights[0][i], 0 , i))
            visited[0][i] = 1
            heappush(min_heap, (heights[m - 1][i], m - 1, i))
            visited[m - 1][i] = 1

        total_water = 0


        while min_heap:
            h, x, y = heappop(min_heap)

            for dx, dy in offsets:
                new_x = x + dx
                new_y = y + dy
                if 0 <= new_x < m and 0 <= new_y < n and not visited[new_x][new_y]:
                    visited[new_x][new_y] = 1

                    new_h = max(h, heights[new_x][new_y])
                    heappush(min_heap, (new_h, new_x, new_y))

                    if h > heights[new_x][new_y]:
                        total_water += h - heights[new_x][new_y]

        return total_water

heights = [[12, 13, 0, 12], [13, 4, 13, 12], [13, 8, 10, 12], [12, 13, 12, 12], [13, 13, 13, 13]]
ans = Solution().trapRainWater(heights)
print(ans)
