# space O(n)
# time O(n) for popMax, O(1) for the rest
# use 2 stacks, maxStack 長度和 stack一樣，紀錄當前最大值
# popMax時，用一個buf輔助，將stack中的值用self.pop() ，倒入buf，直到找到max
# 再將buf倒回stack (self.push)
# self.pop() and self.push()會自動處理self.stack and self.maxStack
class MaxStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.maxStack = []

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        
        if len(self.maxStack) == 0 or x > self.maxStack[-1]:
            self.maxStack.append(x)
        else:
            self.maxStack.append(self.maxStack[-1])
    
        self.stack.append(x)
        
    def pop(self):
        """
        :rtype: int
        """
        self.maxStack.pop()
        return self.stack.pop()

    def top(self):
        """
        :rtype: int
        """
        return self.stack[-1]

    def peekMax(self):
        """
        :rtype: int
        """
        return self.maxStack[-1]
        

    def popMax(self):
        """
        :rtype: int
        """
        # print(self.stack)
        # print(self.maxStack)
        buf = []
        mx = self.peekMax()
        
        while self.top() != mx:
            buf.append(self.pop())
        self.pop()
        
        while buf:
            self.push(buf.pop())

        return mx


class MaxStack2(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.maxHeap = []
        self.toPop_heap = {} #to keep track of things to remove from the heap
        self.toPop_stack = set() #to keep track of things to remove from the stack

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        heapq.heappush(self.maxHeap, (-x,-len(self.stack)))
        self.stack.append(x)
        
    def pop(self):
        """
        :rtype: int
        """             
        self.top()
        x = self.stack.pop()
        key = (-x,-len(self.stack))
        self.toPop_heap[key] = self.toPop_heap.get(key,0) + 1
        return x

    def top(self):
        """
        :rtype: int
        """
        while self.stack and len(self.stack)-1 in self.toPop_stack:
            x = self.stack.pop()
            self.toPop_stack.remove(len(self.stack))
        return self.stack[-1]
        
    def peekMax(self):
        """
        :rtype: int
        """
        while self.maxHeap and self.toPop_heap.get(self.maxHeap[0],0):
            x = heapq.heappop(self.maxHeap)
            self.toPop_heap[x] -= 1
        return -self.maxHeap[0][0]

    def popMax(self):
        """
        :rtype: int
        """
        self.peekMax()
        x,idx = heapq.heappop(self.maxHeap)
        x,idx = -x,-idx
        self.toPop_stack.add(idx)
        return x
# Your MaxStack object will be instantiated and called as such:
# obj = MaxStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.peekMax()
# param_5 = obj.popMax()
op = ["push", "peekMax", "push", "peekMax", "push", "pop", "pop", "push", "peekMax", "push", "popMax", "top", "push", "push", "peekMax", "popMax", "popMax"]
val = [[92], [], [54], [], [22], [], [], [-57], [], [-24], [], [], [26], [-71], [], [], []]
op = ["MaxStack", "push", "push", "push", "top", "popMax", "top", "peekMax", "pop", "top"]
val = [[], [5], [1], [5], [], [], [], [], [], []]
op = ["MaxStack", "push", "push", "popMax", "peekMax"]
val = [[], [5], [1], [], []]
stack = MaxStack2()
for i, act in enumerate(op):
    if act == "push":
        stack.push(val[i][0])
    elif act == "peekMax":
        print("peekMax", stack.peekMax())
    elif act == "pop":
        print("pop", stack.pop())
    elif act == "top":
        print("top", stack.top())
    elif act == "popMax":
        print("popMax", stack.popMax())

