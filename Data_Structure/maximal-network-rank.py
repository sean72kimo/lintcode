import collections
from typing import List


class Solution:
    def maximalNetworkRank(self, n: int, roads: List[List[int]]) -> int:
        if len(roads) == 0:
            return 0

        adj = [[0 for _ in range(n)] for _ in range(n)]
        edg = collections.Counter()
        for a, b in roads:
            edg[a] += 1
            edg[b] += 1
            adj[a][b] = 1
            adj[b][a] = 1
        print(adj)
        ans = 0
        for i in range(n):
            for j in range(i + 1, n):
                rank = edg[i] + edg[j] - adj[i][j]
                ans = max(ans, rank)
        return ans

n = 4
roads = [[0,1],[0,3],[1,2],[1,3]]

n = 5
roads = [[0,1],[0,3],[1,2],[1,3],[2,3],[2,4]]
e = 5
a = Solution().maximalNetworkRank(n, roads)
print("ans:", a==e ,a)

