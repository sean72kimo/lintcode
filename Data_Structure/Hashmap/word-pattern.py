import collections


class Solution:
    def wordPattern(self, pattern: str, string: str) -> bool:
        strs = string.split()
        if len(pattern) != len(strs):
            return False

        n = len(strs)
        m1 = collections.defaultdict(str)
        m2 = collections.defaultdict(str)

        for i in range(n):
            if pattern[i] in m1 and m1[pattern[i]] != strs[i]:
                return False

            if strs[i] in m2 and m2[strs[i]] != pattern[i]:
                return False

            m1[pattern[i]] = strs[i]
            m2[strs[i]] = pattern[i]

        return True

class Solution:
    def wordPattern(self, pattern: str, string: str) -> bool:
        strs = string.split()
        if len(pattern) != len(strs):
            return False

        res = self.match(pattern, strs)

        return res

    def match(self, pattern, strs):
        mp = {}

        for x, y in zip(pattern, strs):
            if x in mp and mp[x] != y:
                return False
            mp[x] = y
        return len(set(mp.values())) == len(mp.values())


class Solution:
    def wordPattern(self, pattern, string):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        
        words = string.split()
        
        if len(words) != len(pattern):
            return False
        
        idxMp = {}
        for i in range(len(words)):
            x = idxMp.get(pattern[i], None)
            y = idxMp.get(words[i], None)
            print(x,y)
            if x != y:
                return False
            
            idxMp[pattern[i]] = i
            idxMp[words[i]] = i
        
        return True
    
pattern = "abc"
string = "b c a"
a = Solution().wordPattern(pattern, string)
print("ans:", a)