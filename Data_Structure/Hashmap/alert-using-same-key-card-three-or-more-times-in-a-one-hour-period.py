import collections
from typing import List

"""
1. 用name_to_time dict記錄每個員工打卡的時間點
    1. 將時間字符串轉換成timestamp in int
1. loop over name_to_time dict , 當拜訪每個name的時候，產生一個deque輔助sliding window
    1. 每個時間戳都放入deque, 如果當前時間戳和deque第一個時間戳已經超過一個小時，剔除deque第一個時間戳
    1. 如果deque裡面有超過三個時間戳，代表符合題目條件。將此人名放到答案中，並中斷loop
"""

class Solution:
    def alertNames(self, keyName: List[str], keyTime: List[str]) -> List[str]:
        name_to_time = collections.defaultdict(list)

        for name, hour_min in zip(keyName, keyTime):
            hour, minute = hour_min.split(':')
            timestamp = int(hour) * 60 + int(minute)
            name_to_time[name].append(timestamp)

        ans = []

        for name, time_list in name_to_time.items():
            time_list.sort()
            dq = collections.deque()

            for time in time_list:
                dq.append(time)
                if time - dq[0] > 60:
                    dq.popleft()
                if len(dq) >= 3:
                    ans.append(name)
                    break

        return sorted(ans)

