class Solution(object):
    def findAndReplacePattern(self, words, pattern):
        """
        :type words: List[str]
        :type pattern: str
        :rtype: List[str]
        """

        ans = []
        for word in words:
            if self.helper(word, pattern):
                ans.append(word)
        return ans


    def helper(self, word, pattern):
        mw = {}
        mp = {}
        for w, p in zip(word, pattern):
            if w not in mw:
                mw[w] = p
            if p not in mp:
                mp[p] = w

            if (mw[w], mp[p]) != (p, w):
                return False

        return True


words = ["abc","deq","mee","aqq","dkd","ccc"]
# words = ["mee","aqq"]
pattern ="abb"
a = Solution().findAndReplacePattern(words, pattern)
print("ans:", a)