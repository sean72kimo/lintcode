import collections
from typing import List

"""
字符串處理。
split domain, 並且拼接出不同sub-domain, 並且計數。
"""
class Solution:
    def subdomainVisits(self, cpdomains: List[str]) -> List[str]:
        counter = collections.Counter()

        for domain in cpdomains:
            cnt, domain = domain.split()

            frag = domain.split('.')

            for i in range(len(frag)):
                sub = '.'.join(frag[i:])
                counter[sub] += int(cnt)

        ans = []
        for string, cnt in counter.items():
            ans.append(str(cnt) + " " + string)

        return ans



cpdomains = ["9001 discuss.leetcode.com"]
a = Solution().subdomainVisits(cpdomains)
