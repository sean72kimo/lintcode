# Definition for a point.
class Point:
    def __init__(self, a=0, b=0, name=None):
        self.x = a
        self.y = b
        self.name = name

    def __str__(self):
        return '[{0}, {1}]'.format(self.x, self.y)

from heapq import heappop, heappush, _heapify_max, _heappushpop_max
from math import sqrt
from sys import maxsize

class Type:
    def __init__(self, dist, point=None):
        self.dist = dist
        self.point = point

    def __lt__(self, other):
#         if self.dist != other.dist:
#             return self.dist > other.dist
#         if self.point.x != other.point.x:
#             return self.point.x > other.point.x
#         return self.point.y > other.point.y

        if self.dist != other.dist:
            return self.dist < other.dist
        if self.point.x != other.point.x:
            return self.point.x < other.point.x
        return self.point.y < other.point.y

    def __str__(self):
        return "{0:.2f}-{1}".format(self.dist, str(self.point))


[4, 6], [4, 7], [4, 4],


p5 = Point(1, 1, 'p5')
p6 = Point(2, 5, 'p6')
p7 = Point(4, 4, 'p7')
p8 = Point(4, 7, 'p8')

t5 = Type(1.41, p5)
t6 = Type(5.39, p6)
t7 = Type(5.66, p7)
t8 = Type(8.06, p8)

# print('if >', t5 < t6)
#
heap = [t5, t6, t8]
# heapify(heap)
# for t in heap:
#     print(t, end = ' ')
# print('\n')
#
#
# heappush(heap, t7)
# heappop(heap)
# for t in heap:
#     print(t, end = ' ')
# print('\n')



class Solution:
    # @param {Pint[]} points a list of points
    # @param {Point} origin a point
    # @param {int} k an integer
    # @return {Pint[]} the k closest points
    def getDistance(self, p1, p2):
        dist = sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)
        return dist
    # user dictionary {distance : point}
    # will have to deal with same distance issue, such as (1,1) and (-1,-1)
    # need to modify dict to {distance : [point, point, ...]}
    # very annoying to remove point from valuse list
    def kClosest(self, points, origin, k):
        tmp = {}
        for p in points:
            dist = self.getDistance(p, origin)
            if len(tmp) < k:
                tmp.update({dist:p})
                tmp_sorted = sorted(tmp, reverse=True)
                print('1', tmp)
            else:

                del tmp[tmp_sorted[0]]
                tmp_sorted.pop(0)
                tmp.update({dist:p})
                tmp_sorted = sorted(tmp, reverse=True)
                print('2', p)

        rt = []
        tmp_sorted.reverse()
        for i in tmp_sorted:
            rt.append(tmp[i])

        return rt



    # use max heap
    def _kClosest(self, points, origin, k):
        # Write your code here
        max_heap = []

        for p in points:
            dist = self.getDistance(p, origin)
            t = Type(dist, p)

            max_heap.append(t)
            _heapify_max(max_heap)

            if len(max_heap) > k:
                max_heap.pop(0)

        ret = []
        max_heap.sort(reverse=True)
        while len(max_heap) > 0:
            ret.append(max_heap.pop(0).point)

        ret.reverse()
        return ret

    def _kkClosest(self, points, origin, k):
        # Write your code here
        max_heap = []

        for p in points:
            dist = self.getDistance(p, origin)
            t = Type(dist, p)

            heappush(max_heap, t)

            if len(max_heap) > k:
                heappop(max_heap)

        ret = []
        while len(max_heap) > 0:
            ret.append(heappop(max_heap).point)

        ret.reverse()
        return ret






points = []
coords = [ [2, 5], [4, 6], [4, 7], [4, 4], [1, 1] ]
origin = Point(0, 0)
k = 3



coords = [[-1, -1], [1, 1], [100, 100]]
origin = Point(0, 0)
k = 2

for c in coords:
    points.append(Point(c[0], c[1]))


origin = Point(0, 0)

ans = Solution().kClosest(points, origin, k)
print('ans:')
for t in ans:
    print(t, end=' ')
print('\n')

# tmp = {10.2: p5, 2.1: 'B', 3.0: 'B', 4.2222: 'E', 5.66666: 'A'}
# a = sorted(tmp)
# print(a)
# print(tmp)
