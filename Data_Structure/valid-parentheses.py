class Solution:
    """
    @param: s: A string
    @return: whether the string is a valid parentheses
    """
    def isValidParentheses(self, s):

        left = ['[', '{', '(']
        right = {
            '}' : '{',
            ']' : '[',
            ')' : '('
        }
        stack = []
        for ch in s:
            if ch in left:
                stack.append(ch)
            else:
                if not stack:
                    return False

                if right[ch] != stack[-1]:
                    return False

                stack.pop()

        return not stack

    def isValidParentheses2(self, s):
        # write your code here
        left = ['[', '{', '(']

        right = {
            '}' : '{',
            ']' : '[',
            ')' : '('
        }
        n = len(s)
        if n == 0 or n == 1:
            return False

        if s[0] in right:
            return False
        else:
            stack = [s[0]]
        i = 1

        while len(stack):
            c = s[i]

            if c in right:
                last = stack.pop()

                if last != right[c]:
                    return False

            else:
                stack.append(c)

            i += 1


        print(stack)
        if len(stack):
            return False
        else:
            return True

s = "()"
s = "()[]{}"
s = "([)]"
s = "(("
s = "[])"
print(Solution().isValidParentheses(s))





"""
// Given an ascii string containing parentheses and other characters, write a function that balances the paretheses
// by removing the minimum number of unnecessary parentheses.

// Examples:
// ")())((honey badger))())()" -> "()((honey badger))()()"
// "{{{}}}"                    -> "{{{}}}"
// "))][(("                    -> ""
// ""                          -> ""
"""

def is_valid(s):
    left = {
        '[' : ']',
        '(' : ')',
        '{' : '}'
    }

    right = {
        ']' : '[',
        ')' : '(',
        '}' : '{'
    }


    cnt = 0
    stack = []

    for ch in s:
        if ch.isalpha():
            continue

        if len(stack) == 0:
            if ch in right:
                return False
            else:
                stack.append(ch)

        elif ch in right:
            if right[ch] != stack[-1]:
                return False
            else:
                stack.pop()

        elif ch in left:
            stack.append(ch)

    return len(stack) == 0


def removeParen(s):
    vst = set()
    vst.add(s)
    que = []
    que.append(s)
    ans = []
    found = False
    paren = {'(', '[', '{', ')', ']', '}'}

    while que:
        s = que.pop(0)

        if is_valid(s):
            ans.append(s)
            found = True

        if found:
            continue
        start = 0
        flag = False
        for i in range(len(s)):
            if s[i] not in paren:  # s[i] is char
                continue

            new_s = s[:i] + s[i + 1 :]

            if new_s not in vst:
                vst.add(new_s)
                que.append(new_s)

    return ans



s = ")())((honey badger))())()"

a = removeParen(s)
print("ans:", a)
