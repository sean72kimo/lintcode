'''
Definition for a Record
'''
class Record:
    def __init__(self, id, score):
        self.id = id
        self.score = score
import heapq
class Solution:
    # @param {Record[]} results a list of <student_id, score>
    # @return {dict(id, average)} find the average of 5 highest scores for each person
    # <key, value> (student_id, average_score)
    def getAvg(self, scores):
        return sum(scores) / 5.0

    def highFive(self, results):
        record = {}
        rt = {}
        for r in results:
            student = r.id
            score = r.score

            if student not in record:
                record[student] = []

            record[student].append(score)
        ans = {}
        for stu in record:
            if len(record[stu]) >= 5:
                record[stu] = sorted(record[stu], reverse = True)[:5]
                avg = self.getAvg(record[stu])
                ans.update({stu: avg})

        return ans


results = []
results_tmp = [[1, 91], [1, 92], [2, 93], [2, 99], [2, 98], [2, 97], [1, 60], [1, 58], [2, 100], [1, 61]]
for r in results_tmp:
    results.append(Record(r[0], r[1]))

sol = Solution()
print(sol.highFive(results))
