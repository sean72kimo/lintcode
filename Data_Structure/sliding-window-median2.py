from  heapq import *
from collections import OrderedDict

def toTreeMap(paramMap, reverse = False):
    "将paramMap转换为java中的treeMap形式.将map的keys变为heapq.创建有序字典."
    keys = paramMap.keys()

    heap = []
    for item in keys:
        if reverse:
            item = -item
        heappush(heap, item)
    print('heap', heap)
    sort = []
    while heap:
        item = heappop(heap)
        if reverse:
            item = -item
        sort.append(item)
    print('sort', sort)
    resMap = OrderedDict()
    for key in sort:
        resMap[key] = paramMap.get(key)

    return resMap


class TreeMap:
    def __init__(self, paramMap = {}, reverse = False):
        self.reverse = reverse
        self.treemap = OrderedDict()
        self.toTreeMap(paramMap)

    def __repr__(self):
        ele = []
        for k, v in self.treemap.items():
            # string = "<TreeMap({0},{1})>".format(k, v)
            string = "<TreeMap({0})>".format(k)
            string = "{0}".format(k)
            string = "{0}".format(v)
            ele.append(v)
        return str(ele)

    # k = size of paramMap
    # O( k log k)
    # each heappush or heappop is O(log k)
    # iterate over k keys, so k * logk ?
    def toTreeMap(self, paramMap = {}):

        keys = paramMap.keys()
        values = paramMap.values()


        heap = []
        for item in values:
            if self.reverse:
                item = -item
            heappush(heap, item)

        sort = []
        while heap:
            item = heappop(heap)
            if self.reverse:
                item = -item
            sort.append(item)

        for key in sort:
            self.treemap[key] = paramMap.get(key)

    # time:  not sure about sorted(self.treemap.items())
    # O(n log n)?

    # space:
    # since this created a new OrderedDict,
    # does it count as extra O(n) from od instance besides the original self.treemap?
    # but I pointed the original treemap to od,
    # so the original self.treemap space will be garbage collected , am I correct?
    def add(self, itm = {}, which = None):
        self.treemap.update(itm)
        tmp = sorted(self.treemap.items(), reverse = self.reverse)
        od = OrderedDict(tmp)
        self.treemap = od

        if which == 'minheap':
            print(self.treemap)

    # time: O(log n) ? like binary search in a BST?
    def remove(self, key = None):
        self.treemap.pop(key)

    # time: O(1)
    def pop(self, last = False):
        itm = self.treemap.popitem(last = last)
        return {itm[0]:itm[1]}

    # time:O(1)
    def peek(self):
        keyList = list(self.treemap.keys())


        key = keyList[0] if keyList else None
        return key

    # time:O(1)
    @property
    def size(self):
        return len(self.treemap)

class Solution:
    """
    @param: nums: A list of integers
    @param: k: An integer
    @return: The median of the element inside the window at each moving
    """
    ans = []

    def adjust(self, minheap, maxheap):

        # if len(nums) is odd, such as 5
        # len(maxheap) = len(minheap) + 1
        # becuase maxheap contains median

        # if len(nums) is even, such as 4
        # len(maxheap) == len(minheap)

        if minheap.size > maxheap.size:
            # heappush(maxheap, -heappop(minheap))
            maxheap.add(minheap.pop())

        if maxheap.size > minheap.size + 1:
            # heappush(minheap, -heappop(maxheap))
            minheap.add(maxheap.pop())

        if maxheap.size and minheap.peek():

            if  maxheap.peek() > minheap.peek():
                # maxroot = -maxheap[0]
                # minroot = minheap[0]
                # heapreplace(maxheap, -minroot)
                # heapreplace(minheap, maxroot)

                max_itm = maxheap.pop()
                min_itm = minheap.pop()
                maxheap.add(min_itm)
                minheap.add(max_itm)

    def medianSlidingWindow(self, nums, k):
        n = len(nums)
        # ascending, min at minheap[0]
        minheap = TreeMap({})
        # descending, max at maxheap[0]
        maxheap = TreeMap({}, reverse = True)

        for i in range(n):

            num = nums[i]
            print('add', num)
            if maxheap.size == 0 or num <= maxheap.peek():
                maxheap.add({num:i})
            else:
                minheap.add({num:i}, 'minheap')


            # adjust each time when you add a new item
            self.adjust(minheap, maxheap)
            print('s:', maxheap , minheap)

            # start output median
            if i - k + 1 >= 0:
                self.ans.append(maxheap.peek())

                kick = nums[i - k + 1]
                print('kick:', kick)

                if kick <= maxheap.peek():
                    # I have problem here
                    # maxheap.remove(-kick)
                    # heapify(maxheap)
                    maxheap.remove(kick)
                else:
                    # minheap.remove(kick)
                    # heapify(minheap)
                    minheap.remove(kick)

                print('b:', maxheap , minheap)
                self.adjust(minheap, maxheap)
                print('a:', maxheap , minheap)


        return self.ans

nums = [1, 2, 7, 7, 2, 10, 3, 4, 5]
k = 3
nums = [1, 2, 7, 7, 2]
k = 3
# nums = [0, 4, 6, 0, 3, 2, 6, 9, 4, 1]
# k = 3
# print('ans:', Solution().medianSlidingWindow(nums, k))




#######################
d1 = {}
d1[0] = 5
d1[1] = 6
d1[2] = 7
d1[3] = 8
# d1.move_to_end('c', last=False)

# def print_d(d1):
#     for k, v in d1.items():
#         print(k, v)
#
# rt = toTreeMap(d1, True)
# print(list(rt.items()))

treemap = TreeMap(d1, True)
# print(treemap)
# treemap.add({66:'B'})
# print(treemap)
# itm = treemap.pop()
# print(treemap, itm)
# itm = treemap.popitem()
# print(treemap, itm, type(itm))
# itm = treemap.peekitem()
# print(itm)














