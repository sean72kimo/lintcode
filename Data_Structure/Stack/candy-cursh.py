class Solution:
    def candyCrush(self, C):
        stack = []

        for ch in C:
            if len(stack) and ch != stack[-1]:
                self.crash(stack)

            stack.append(ch)
        self.crash(stack)

        return ''.join(stack)

    def crash(self, stack):
        tmp = stack[-1]
        cnt = 0
        for i in range(len(stack) - 1, -1, -1):
            if stack[i] != tmp:
                break
            cnt += 1

        if cnt >= 3:
            for _ in range(cnt):
                stack.pop()


C = "AABBCCCCCBC"
# C = "ABBCCCB"
# C = "ABBBCC"
a = Solution().candyCrush(C)
print("ans:", a)
