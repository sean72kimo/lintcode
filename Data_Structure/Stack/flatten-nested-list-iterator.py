class NestedIterator(object):

    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.stack = [[nestedList, 0]]

    def next(self):
        """
        :rtype: int
        """

        lst , i = self.stack[-1]
        self.stack[-1][1] += 1
        return lst[i].getInteger()

    def hasNext(self):
        """
        :rtype: bool
        """

        while len(self.stack):
            lst , i = self.stack[-1]
            if i == len(lst):
                self.stack.pop()
            else:
                if lst[i].isInteger():
                    return True
                self.stack[-1][1] += 1
                self.stack.append([lst[i].getList(), 0])
        return False

