class Solution:
    """
    @param expression: A string array
    @return: The Reverse Polish notation of this expression
    """

    def convertToRPN(self, s):
        # write your code here
        stack = []

        n = len(s)
        operator = ["+", "-", "*", "/"]
        operators = {"+": 1, "-": 1, "*": 2, "/": 2}
        paren = ["(", ")"]

        num = 0
        rpn = []
        w = 1
        for i in range(n):
            if s[i].isdigit():
                if num is None:
                    num = s[i]
                else:
                    num = num * 10 + int(s[i])

            elif s[i] == "(":
                stack.append(s[i])

            elif s[i] == ")":
                if num is not None:
                    rpn.append(str(num))
                    num = None
                while stack and stack[-1] != "(":
                    rpn.append(stack.pop())
                stack.pop()

            elif s[i] in operators:

                if num is not None:
                    rpn.append(str(num))
                    num = None
                while len(stack) and stack[-1] != "(" and operators[s[i]] <= operators[stack[-1]]:
                    o = stack.pop()
                    rpn.append(o)
                stack.append(s[i])

        if num is not None:
            rpn.append(str(num))

        while stack:
            rpn.append(stack.pop())
        return rpn

class Solution_weight:
    """
    @param expression: A string array
    @return: The Reverse Polish notation of this expression
    """

    def convertToRPN(self, s):
        # write your code here
        stack = []

        n = len(s)
        operator = ["+", "-", "*", "/"]
        operators = {"+": 1, "-": 1, "*": 2, "/": 2}

        paren = ["(", ")"]

        num = None
        rpn = []
        w = 1
        for i in range(n):
            if s[i].isdigit():
                if num is None:
                    num = s[i]
                else:
                    num = num * 10 + int(s[i])

            elif s[i] in paren:
                if s[i] == paren[0]:
                    w += 10
                else:
                    w -= 10

            elif s[i] in operators:
                if num is not None:
                    rpn.append(str(num))
                    num = None
                while len(stack) and operators[s[i]] + w <= stack[-1][1]:
                    oper, wei = stack.pop()
                    rpn.append(oper)
                stack.append((s[i], operators[s[i]] + w))

        if num:
            rpn.append(str(num))
        while stack:
            rpn.append(stack.pop()[0])
        return rpn
s = ["3","-","4","+","5"]

s = ["(", "5", "-", "6", ")", "*", "7"]
exp = ["5","6","-","7","*"]


s = ["(", "(", "13", "+", "14", ")", "*", "5", "-", "11", ")", "*", "10"]
val = eval(''.join(s))
exp =  ["13", "14", "+", "5", "*", "11", "-", "10", "*"]


s = ["(", "(", "13", "+", "14", ")", "*", "5", "-", "11", ")", "*", "0"]
val = eval(''.join(s))
exp =  ["13", "14", "+", "5", "*", "11", "-", "10", "*"]

s = "( ( 1 + 3 ) * 5 ) + ( ( 7 + 8 ) * 2 )"
s = s.split()
print(s)
val = eval(''.join(s))



a = Solution_weight().convertToRPN(s)
print("ans:", a)

b = Solution().convertToRPN(s)
print("ans:", b)

class Calc:
    def calculate(self, exp):
        stack = []
        operator = ["+", "-", "*", "/"]
        for ch in exp:
            if ch.isdigit():
                stack.append(ch)
            else:
                a2 = stack.pop()
                a1 = stack.pop()
                val = eval(a1 + ch + a2)
                stack.append(str(val))
        return stack[0]
va = Calc().calculate(a)
vb = Calc().calculate(b)
print(val, va, vb)
