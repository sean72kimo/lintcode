class Solution(object):
    def dailyTemperatures(self, T):
        """
        :type T: List[int]
        :rtype: List[int]
        """
        if len(T) == 0:
            return []

        stack = []
        ans = [0 for _ in range(len(T))]

        for i, t in enumerate(T):
            while len(stack) and t > T[stack[-1]]:
                j = stack.pop()
                ans[j] = i-j
            stack.append(i)
        return ans

T = [73, 74, 75, 71, 69, 72, 76, 73]
exp = [1, 1, 4, 2, 1, 1, 0, 0]
a = Solution().dailyTemperatures(T)
print("ans:", a==exp, a)