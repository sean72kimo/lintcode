class Solution:
    def pre_less(self, A):
        stack = []
        ans = [-1] * len(A)
        for i in range(len(A)):
            while len(stack) and A[i] <= A[stack[-1]]:  # 絕對遞增
                stack.pop()

            if stack:
                ans[i] = A[stack[-1]]
            stack.append(i)

        return ans

    def nxt_less(self, A):
        stack = []
        ans = [-1] * len(A)
        for i in range(len(A)):
            while len(stack) and A[i] <= A[stack[-1]]: # 絕對遞增
                x = stack[-1]
                ans[x] = A[i]
                stack.pop()

            stack.append(i)

        return ans

A = [3, 7, 8, 4]
sol = Solution()
rt = sol.pre_less(A)
print(rt)
rt = sol.nxt_less(A)
# print(rt)
