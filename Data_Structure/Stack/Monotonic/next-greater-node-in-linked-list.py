import collections

from lib.linkedlist import ListNode, list2LinkedList,printLinkedList


class Solution(object):
    def nextLargerNodes(self, head):
        """
        :type head: ListNode
        :rtype: List[int]
        將linkedlist轉成array並用stack解
        """
        mp = collections.defaultdict(lambda: -1)
        nums = []
        curr = head
        while curr:
            nums.append(curr.val)
            curr = curr.next

        stack = []
        for i in range(len(nums) - 1, -1, -1):
            while stack and nums[i] >= nums[stack[-1]]:
                stack.pop()

            if len(stack):
                mp[i] = stack[-1]

            stack.append(i)

        ans = []
        for i in range(len(nums)):
            v = nums[mp[i]] if mp[i] != -1 else 0
            ans.append(v)

        return ans


class Solution(object):
    def nextLargerNodes(self, head):
        """
        :type head: ListNode
        :rtype: List[int]
        將linkedlist轉成array 並練習從前往後，用stack解
        """
        if not head:
            return []

        nums = []
        curr = head
        while curr:
            nums.append(curr.val)
            curr = curr.next

        stack = []
        ans = [0] * len(nums)
        for i in range(len(nums)):
            while len(stack) and nums[i] > nums[stack[-1]]:  # decrease
                ans[stack[-1]] = nums[i]
                stack.pop()

            stack.append(i)

        return ans

class Solution(object):
    def nextLargerNodes(self, head):
        res, stack = [], []
        while head:
            while stack and head.val > stack[-1][1]:
                itm = stack.pop()
                res[itm[0]] = head.val

            itm = [len(res), head.val]
            stack.append(itm)
            res.append(0)
            head = head.next
        return res


class Solution2(object):
    def nextLargerNodes(self, head):
        curr = head

        cnt = 0
        while curr:
            cnt += 1
            curr = curr.next

        res = [0] * cnt
        stack = []
        i = 0
        while head:
            while stack and head.val > stack[-1][1]:
                itm = stack.pop()
                res[itm[0]] = head.val

            itm = [i, head.val]
            stack.append(itm)
            res[i] = 0
            head = head.next
            i += 1
        return res

data = [1,7,5,1,9,2,5,1]
exp = [7,9,9,9,0,5,0,0]

head = list2LinkedList(data)
a = Solution2().nextLargerNodes(head)
print("ans:", a==exp, a)