class Solution(object):
    def removeKdigits(self, num, k):
        """
        :type num: str
        :type k: int
        :rtype: str
        """
        if len(num) == 0:
            return '0'
        if k == 0:
            return num

        stack = []
        for ch in num:
            while stack and ch < stack[-1] and k > 0:
                stack.pop()
                k -= 1

            stack.append(ch)

        for _ in range(k):
            if len(stack):
                stack.pop()

        if len(stack) == 0:
            return '0'

        if all(x == '0' for x in stack):
            return '0'

        return ''.join(stack).lstrip('0')


num = "10"
k = 2
a = Solution().removeKdigits(num, k)
print("ans:", a)
