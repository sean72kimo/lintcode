import collections
from typing import List

"""
小技巧：將原本的數組複製兩份相加
clone = nums + nums
便可以達到循環的目的
"""
class Solution(object):
    def nextGreaterElements(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) == 0:
            return []

        n = len(nums)
        arr = nums + nums
        stack = []
        mp = {}
        # 下面這種方法也可
        # 順向遞減，在while loop內更新
        # for i in range(n*2):
        #     while stack and arr[i] > arr[stack[-1]]: #遞減
        #         j = stack.pop()
        #         mp[j%n] = i%n
        #     stack.append(i)

        # 下面這種方法也可
        # 逆向遞減， 在while loop外更新
        for i in range(n * 2 - 1, -1, -1):
            while stack and arr[i] >= arr[stack[-1]]:  # 遞減
                stack.pop()

            if len(stack):
                mp[i % n] = stack[-1] % n
            stack.append(i)

        ans = []
        for i in range(n):
            if i not in mp:
                ans.append(-1)
                continue
            ans.append(nums[mp[i]])
        return ans

nums = [1,2,1]
exp = [2,-1,2]

# nums = [3,8,4,1,2]
# exp = [8,-1,8,2,3]
# a = Solution().nextGreaterElements(nums)
# print("ans:", a==exp, a)


class Solution2:
    def nextGreaterElements(self, nums: List[int]) -> List[int]:
        clone = nums + nums
        stack = []
        mp = collections.defaultdict(lambda: -1)

        for i in range(len(clone)):
            while stack and clone[stack[-1]] < clone[i]:
                j = stack.pop()
                mp[j] = i
            stack.append(i)

        size = len(nums)
        ans = []

        for i in range(len(nums)):
            nxt_idx = mp[i]
            if nxt_idx == -1:
                ans.append(nxt_idx)
            else:
                idx = nxt_idx % size
                ans.append(nums[idx])

        return ans

# nums = [3,8,4,1,2]
# exp = [8,-1,8,2,3]
a = Solution().nextGreaterElements(nums)
print("ans:", a==exp, a)