class Solution_n2(object):
    def sumSubarrayMins(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        mod = 10 ** 9 + 7
        n = len(A)
        ans = 0
        for i in range(len(A)):
            left = right = 1

            while i - left >= 0 and A[i] < A[i - left]:
                left += 1

            while i + right < len(A) and A[i] <= A[i + right]:
                right += 1

            print(left, right)
            ans += A[i] * left * right

        return ans % mod


class Solution_stack(object):
    def sumSubarrayMins(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        mod = 10 ** 9 + 7
        n = len(A)

        pre = [None] * n
        stack = []
        # 順向遞增， 在while loop外更新
        for i in range(len(A)):
            while len(stack) and A[i] <= A[stack[-1]]:
                stack.pop()
            pre[i] = stack[-1] if len(stack) else -1
            stack.append(i)

        # 逆向遞增， 在while loop外更新
        nxt = [None] * n
        stack = []
        for k in range(n-1, -1, -1):
            while stack and A[k] < A[stack[-1]]:
                stack.pop()
            nxt[k] = stack[-1] if stack else n
            stack.append(k)

        ans = 0
        for i in range(len(A)):
            ans = (ans + A[i] * (i-pre[i]) * (nxt[i]-i)) % mod

        return ans



A = [3,1,2,4] # 17
A = [2,1,1,3] # 13
a = Solution_n2().sumSubarrayMins(A)
b = Solution_stack().sumSubarrayMins(A)
print("ans:",a, b, a==b)