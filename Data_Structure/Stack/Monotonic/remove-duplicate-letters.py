import collections


class Solution(object):
    def removeDuplicateLetters(self, s):
        """
        :type s: str
        :rtype: str
        """
        counter = collections.Counter(s)

        vst = set()
        stack = []
        for ch in s:
            if ch not in vst:
                while stack and stack[-1] > ch and counter[stack[-1]]:
                    vst.remove(stack.pop())
                stack.append(ch)
                vst.add(ch)

            counter[ch] -= 1
        return ''.join(stack)

def main():
    s = "ayzcacyz"
    s = "cxbacdcbc"
    # s = "kbfedfgfef"

    a = Solution().removeDuplicateLetters(s)
    print("ans:", a)


if __name__ == "__main__":
    main()
