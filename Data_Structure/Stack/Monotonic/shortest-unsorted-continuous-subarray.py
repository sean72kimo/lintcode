"""
如果數組已經排序，遍歷數組放入單調上升棧的過程，絕對不會進入while loop, 結束for循環後，整個棧裡面的數字，就是原輸入的排序數組

1. 用單調棧找區間極值
768. Max Chunks To Make Sorted II
769. Max Chunks To Make Sorted

2. 用單調棧找數字的相對位置
如何用單調棧在亂序數組中，找到區間最大值or最小值？ Solution_stack_restore
第一個for loop找到每個 subarray的區間最小值
[2,7,6,4,8,10,9,15]
[2] <- 7, ok
[2,7] <- 6, not ok, pop 7 (代表7應該在右側某處而非當前位置)
[2,6] <- 4, not ok, pop 6 (代表6應該在右側某處而非當前位置)
[2,4] <- 8, ok
"""
class Solution_stack_monotonic(object):
    def findUnsortedSubarray(self, nums):
        stack = []
        l = float('inf')
        for i in range(len(nums)):
            while len(stack) and nums[i] <= nums[stack[-1]]:  # 單調上升
                l = min(l, stack.pop())
            stack.append(i)

        stack = []
        r = float('-inf')
        for i in range(len(nums) - 1, -1, -1):  # 單調下降
            while len(stack) and nums[i] >= nums[stack[-1]]:
                r = max(r, stack.pop())
            stack.append(i)

        print(l, r)
        return r - l + 1 if r > l else 0

nums = [0, 3, 2, 1, 4, 6, 5, 7]

nums =  [1,3,0,2]
sol = Solution_stack_monotonic()
a = sol.findUnsortedSubarray(nums)
print("ans:", a)

class Solution_sort(object):
    def findUnsortedSubarray(self, A):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(A) == 0:
            return 0

        B = sorted(A)
        ans = []

        for i in range(len(A)):
            if B[i] != A[i]:
                ans.append(i)

        return ans[-1] - ans[0] + 1 if len(ans) else 0


# nums = [2,6,4,8,10,9,15] #5
# sol = Solution_stack_monotonic()
# a = sol.findUnsortedSubarray(nums)
# print("ans:", a)