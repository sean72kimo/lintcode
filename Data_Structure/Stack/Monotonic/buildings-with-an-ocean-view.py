from typing import List

# 由後往前處理
class Solution:
    def findBuildings(self, heights: List[int]) -> List[int]:
        n = len(heights)
        stack = []
        ans = []

        for i in range(n - 1, -1, -1):
            while stack and heights[stack[-1]] < heights[i]:
                stack.pop()

            if len(stack) == 0 or (stack and heights[i] > heights[stack[-1]]):
                ans.append(i)
                stack.append(i)

        return ans[::-1]

# 由後往前處理
# 只記錄最高的一個
class Solution(object):
    def findBuildings(self, heights):
        """
        :type heights: List[int]
        :rtype: List[int]
        """
        max_h = -1
        n = len(heights)
        ans = []

        for i in range(n - 1, -1, -1):
            if heights[i] > max_h:
                ans.append(i)

            max_h = max(heights[i], max_h)

        return ans[::-1]

# 由前往後處理
class Solution:
    def findBuildings(self, heights: List[int]) -> List[int]:
        if not heights:
            return []
        stack = []
        n = len(heights)
        for i in range(n):
            if not stack:
                stack.append(i)
                continue

            while stack and heights[i] >= heights[stack[-1]]:
                stack.pop()
            stack.append(i)

        return stack