class Solution:
    def sorrArrUsingStack(self, arr):


        stack = []

        while len(arr):
            v = arr.pop()
            while len(stack) and v < stack[-1]:
                x = stack.pop()
                arr.append(x)

            stack.append(v)


        return stack


arr = [8, 5, 7, 1, 9, 12, 10]
sol = Solution()
a = sol.sorrArrUsingStack(arr)
print("ans:", a)