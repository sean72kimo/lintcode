import collections


class Solution(object):
    def nextGreaterElement(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]

        nums1 為nums2 subset, 尋找nums1 每個元素，映射回nums2的位置，並找尋其右手邊第一個較大的元素

        循環nums2, 建立一個mp, curr_v : next_greater
        每個nums2裡的元素，都能快速知道其右手邊第一個較大的元素是誰

        建立一個mp:
        1.
        循環nums2，維持一個遞減單調棧(下面做法，stack裡面存放的是val, 若改為idx也可)
        stack的元素，越左邊是較早出現的元素(idx 越小)，越右邊是越晚出現的元素(idx 越大)
        當前元素idx 一定大於stack[-1]的idx
        如果當前元素值大於stack[-1], 則代表stack[-1] next greater element就是當前元素
        且當前元素必定在stack[-1]右手邊
        pop stack, and create map mp[stack.pop()] = curr
        2.
        stack剩下中的元素，代表找不到右手邊比以其大的數字, pop them and mp[stack.pop()] = -1

        建立答案:
        從mp裡面找到nums1對應的next greater element

        """
        stack = []
        ans = []
        mp = {}

        for num in nums2:
            while len(stack) and num > stack[-1]:
                n = stack.pop()
                mp[n] = num
            stack.append(num)

        while stack:
            n = stack.pop()
            mp[n] = -1

        for num in nums1:
            ans.append(mp[num])

        return ans



class Solution_idx(object):
    """
    stack 存idx的做法, 活用 defaultdict default value by setting lambda
    """

    def nextGreaterElement(self, nums1, nums2):
        stack = []
        mp = collections.defaultdict(lambda: -1)

        for i in range(len(nums2) - 1, -1, -1):
            while len(stack) and nums2[stack[-1]] <= nums2[i]:
                stack.pop()

            if len(stack):
                mp[nums2[i]] = nums2[stack[-1]]

            stack.append(i)

        ans = []
        for n in nums1:
            ans.append(mp[n])

        return ans


class Solution(object):
    def nextGreaterElement(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        stack = []
        nxt_greater = collections.defaultdict(lambda: -1)
        pre_greater = collections.defaultdict(lambda: -1)
        for n in nums2:
            print(n, stack)
            while stack and n > stack[-1]:  # down
                nxt_greater[stack[-1]] = n

                stack.pop()

            if len(stack): pre_greater[n] = stack[-1]
            stack.append(n)

        print(nxt_greater)
        print(pre_greater)

nums1 = [4,1,2]
nums2 = [1,5,3,4,2]
exp = [-1,3,-1]

# nums1 = [6, 1, 3, 2, 7]
# nums2 = [3, 2, 5, 1, 7, 6]
# exp = [-1,7,5,5,-1]
a = Solution().nextGreaterElement(nums1, nums2)
print("ans:", a==exp, a)


