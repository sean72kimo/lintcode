class Solution(object):
    def largestRectangleArea(self, heights):
        """
        :type heights: List[int]
        :rtype: int
        """
        stack = []
        area = 0
        """
        stack 維護一個高度上升的單調棧 (記錄下標，即可透過下標轉換成高度)
        如果當前高度curr height <= stack[-1] height 
        pop stack並計算上一個高度能得到多少面積
        注意技巧：

        w = curr_i - stack[-1] - 1, 因為此高度會影響的面積為 stack[-1] 往左往右兩邊延伸，而非只有往右邊延伸
        而非 w = curr_i - stack[-1]
        ex: [2,1,5,6,5,4] i=5 (height 4) 的時候，計算 height 5 (idx 4) 所影響的面積會出現錯誤 

        i 的範圍為 0 ~ n+1
        最後將curr height設為-1 去觸發並計算stack裡面最後的內容
        """
        for i in range(len(heights) + 1):
            if i == len(heights):
                curr = -1
            else:
                curr = heights[i]

            while stack and curr <= heights[stack[-1]]:
                idx = stack.pop()
                h = heights[idx]

                if len(stack) == 0:
                    w = i
                else:
                    w = i - stack[-1] - 1
                area = max(area, h * w)

            stack.append(i)

        return area


heights = [2, 1, 5, 6, 2, 3]
heights = [1, 3, 2, 6, 7, 3, 4]
heights = [1, 2, 2]
heights = [2, 1, 5, 6, 5, 4]
a = Solution().largestRectangleArea(heights)

print("ans:", a )

