"""394. Decode String
s = "3[a]2[bc]", return "aaabcbc".
s = "3[a2[c]]", return "accaccacc".
s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
"""
class Solution(object):
    def decodeString(self, s):
        """
        :type s: str
        :rtype: str
        """
        stack = []

        num = 0
        # [ and ] don't go into stack, they are trigger
        for i, ch in enumerate(s):
            if ch.isdigit():
                num = num * 10 + int(ch)
                continue
                
            if ch.isalpha():
                stack.append(ch)
                
            elif ch == '[': # '[' is the trigger 
                stack.append(str(num)) # convert num to str and put back for line 29 isdigit()
                num = 0
                
            elif ch == ']':
                tmp = ""
                while stack and not stack[-1].isdigit():
                    tmp = stack.pop() + tmp #注意string cascade方向
                if stack:
                    times = int(stack.pop()) #要將 num string 轉成 int
                    tmp = tmp * times
                stack.append(tmp)
                
        return ''.join(stack)
            