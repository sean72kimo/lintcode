class Solution:
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) < 2:
            return 0
        stack = [(-1, '#')]
        ans = 0
        for i, ch in enumerate(s):

            if ch == '(':
                stack.append((i, ch))
            else:
                if stack[-1][1] == '(':
                    stack.pop()
                    d = i - stack[-1][0]
                    ans = max(d, ans)
                else:
                    stack.append((i, ch))

        return ans


class Solution:
    def longestValidParentheses(self, s: str) -> int:
        if not s:
            return 0

        # -1 先放一個index = -1 代表上一個非法位置，便於計算長度。
        # stack[-1], ie stack最後一個index代表：上一次的非法括號位置(index)。下一次的合法括號可以用此index計算長度
        stack = [-1]
        ans = 0
        for i, ch in enumerate(s):
            if ch == "(":
                stack.append(i)
            else:
                # 這個pop有兩個作用：
                # 1. if current stack top是合法匹配左括號，那麼彈出後，棧頂的index恰好是上一個非法括號的下標，正好拿來計算長度
                # 2. if current stack top是非法括號，以後也不可能拿來幫助長度計算，
                #    因為當前的i也是一個非法括號，可以拿來計算未來可能的合法長度
                stack.pop()
                if stack:
                    ans = max(ans, i - stack[-1])
                else:
                    # 因為當前的i也是一個非法括號，可以拿來計算未來可能的合法長度
                    stack.append(i)
        return ans


s = "))"
expected = 0
s = ")()())"
expected = 4
a = Solution().longestValidParentheses(s)
print(a)