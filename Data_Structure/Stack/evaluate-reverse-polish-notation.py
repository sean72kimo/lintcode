class Solution(object):
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        oper = ['+','-','*','/']
        stack = []
        if not any(tokens):
            return
        
        if len(tokens) == 1:
            return int(tokens[-1])
        
        
        for ch in tokens:
            if ch not in oper:
                stack.append(int(ch))
            else:
                print(stack)
                second = stack.pop()
                first = stack.pop()

                if ch == '+': 
                    res = first + second
                elif ch == '-': 
                    res = first - second
                elif ch == '*': 
                    res = first * second
                elif ch == '/':
                    print(second, first, float(first)/second)
                    res =  int(float(first)/second)

                stack.append(res)
        print(stack)
        return stack[-1]
tokens = ["4", "13", "5", "/", "+"]
tokens = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]

a = Solution().evalRPN(tokens)
print("ans:", a)
        