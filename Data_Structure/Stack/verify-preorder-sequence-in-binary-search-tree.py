from sys import maxsize
class Solution(object):
    def verifyPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        stack = []
        mn = -maxsize

        for p in preorder:
            if p < mn:
                return False
            print("===", p)
            while stack and p > stack[-1]:
                print(stack)
                mn = stack.pop()
            print("mn", mn)
            stack.append(p)
        return True
preorder = [10, 5, 2, 6, 12]
# a = Solution().verifyPreorder(preorder)
# print("ans:", a)



# post order bst verification
class Solution2(object):
    def verifyPreorder(self, postorder):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        stack = []
        mx = maxsize

        for p in postorder[::-1]:
            print(p)
            if p > mx:
                return False

            while stack and p < stack[-1]:
                mx = stack.pop()

            stack.append(p)
        return True

postorder = [2, 6, 5, 9, 13, 12, 10]
a = Solution2().verifyPreorder(postorder)
print("ans:", a)
