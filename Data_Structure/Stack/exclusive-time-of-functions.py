from typing import List


class Solution1:
    def exclusiveTime(self, n: int, logs: List[str]) -> List[int]:
        if not logs:
            return [0]
        ans = [0] * n
        stack = []

        # prev的功用有點類似pointer指向之前的一個時間點。
        # 隨著thread的執行，prev也得一直更新
        prev = 0
        for log in logs:
            data = log.split(':')
            pid = int(data[0])
            status = data[1]
            curr = int(data[2])

            if status == "start":
                if stack:
                    i = stack[-1]
                    ans[i] += curr - prev

                stack.append(pid)
                prev = curr

            elif status == "end":
                i = stack.pop()
                ans[i] += curr - prev + 1
                prev = curr + 1

        return ans


class Solution2:
    def exclusiveTime(self, n: int, logs: List[str]) -> List[int]:
        if not logs:
            return [0]

        ans = [0] * n
        stack = []
        for log in logs:
            data = log.split(':')
            pid = int(data[0])
            status = data[1]
            curr = int(data[2])

            if status == "start":
                if stack:
                    i, prev_time = stack[-1]
                    ans[i] += curr - prev_time

                stack.append([pid, curr])

            elif status == "end":
                i, prev_time = stack.pop()
                ans[i] += curr - prev_time + 1

                # 此方法和Soluton1類似。此法將時間的推移，直接更新在前一個thread的data裏。
                if stack:
                    stack[-1][1] = curr + 1
        return ans