class Solution(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0

        self.i = 0
        s = s.strip() + ')'
        data = []
        for ch in s:
            if ch.isspace():
                continue
            data.append(ch)
        return self.parser(data)

    def parser(self, data):

        stack = []
        num = 0
        op = '+'
        oper = {'+', '-', '*', '/'}

        while self.i < len(data):
            if data[self.i].isdigit():
                num = num * 10 + int(data[self.i])
                self.i += 1
                continue

            ch = data[self.i]
            if ch == '(':
                self.i += 1
                num = self.parser(data)

            if ch in oper or ch == ')':
                if op == '+':
                    stack.append(num)
                    num = 0
                    op = ch

                elif op == '-':
                    stack.append(-num)
                    num = 0
                    op = ch

                elif op == '*':
                    if data[self.i] == '-' and data[self.i - 1] == '*':
                        num, ch = self.get_num(data)

                    stack[-1] = stack[-1] * num
                    num = 0
                    op = ch

                elif op == '/':
                    if data[self.i] == '-' and data[self.i - 1] == '/':
                        num, ch = self.get_num(data)

                    if stack[-1] * num > 0:
                        stack[-1] = stack[-1] // num
                    else:
                        stack[-1] = -(abs(stack[-1]) // abs(num))
                    num = 0
                    op = ch

                if ch == ')':
                    return sum(stack)
            self.i += 1

        return sum(stack)

    def get_num(self, data):
        num = 0
        self.i += 1
        while self.i < len(data) and data[self.i].isdigit():
            num = num * 10 + int(data[self.i])
            self.i += 1

        op = data[self.i]
        return -num, op


s="2-1+2"
# s= "1+1"
# s="(6-2+8)"
# s="(123)"
# s = "2147483647"
# s = "(-2147483648)"
# s = "(-123)"
arr = ["2","*","6","-","(","23","+","7",")","/","(","1","+","2",")"]
s = ''.join(arr)
s = "2*(5+5*2)/-3+(6/2+8)"
s="2-1"
print(s)
a = Solution().calculate(s)
print("ans:", a)



class Solution2(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        if len(s) == 0:
            return 0

        data = []
        self.i = 0
        for ch in s:
            if ch.isspace():
                continue
            data.append(ch)
        data.append(')')

        return self.eval(data)

    def eval(self, data):
        stack = []
        num = 0
        op = '+'
        oper = {"+", "-", "*", "/"}

        while self.i < len(data):
            if data[self.i].isdigit():
                num = num * 10 + int(data[self.i])
                self.i += 1
                continue

            ch = data[self.i]
            if ch == '(':
                self.i += 1
                num = self.eval(data)

            if ch in oper or ch == ')':
                if op == '+':
                    stack.append(num)
                    op = ch
                    num = 0
                elif op == '-':
                    stack.append(-num)
                    op = ch
                    num = 0

            self.i += 1
        return sum(stack)

s = "(1+(4+5+2)-3)+(6+8)"
a = Solution2().calculate(s)
print("ans:", a)