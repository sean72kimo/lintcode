class Solution(object):
    def parseTernary(self, expression):
        """
        :type expression: str
        :rtype: str
        """
        if len(expression) == 0:
            return ""

        stack = []
        for ch in expression:
            if len(stack) > 3 and stack[-1] == ':':
                right = ch
                stack.pop()  # :
                left = stack.pop()
                stack.pop()  # ?
                op = stack.pop()
                res = left if op == 'T' else right
                stack.append(res)
                continue

            stack.append(ch)
        return stack[0]

string = "T?1:F?3:4"
exp = 'T'
sol = Solution()
a = sol.parseTernary(string)
print("ans:", a==exp, a)