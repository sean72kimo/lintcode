class Solution:

    def reverseParentheses(self, s: str) -> str:
        stack = []

        for i, ch in enumerate(s):
            if ch == ')':
                tmp = []
                while stack and stack[-1] != '(':
                    ch = stack.pop()

                    if len(ch) == 1:
                        tmp.append(ch)
                    else:
                        tmp.append(ch[::-1])

                if stack[-1] == '(':
                    stack.pop()
                stack.append(''.join(tmp))

            else:
                stack.append(ch)

        return ''.join(stack)

s = "(u(love)i)"
s  ="(ed(et(oc))el)"
s = "a(bc(mno)p)q"
a = Solution().reverseParentheses(s)
print("ans:", a)
