class Solution:
    def removeDuplicates(self, s: str, k: int) -> str:
        stack = []
        ans = ""
        for ch in s:
            if not stack or stack[-1][0] != ch:
                stack.append([ch,1])
            elif stack[-1][1] + 1 < k:
                stack[-1][1] += 1
            else:
                stack.pop()

        for ch , cnt in stack:
            ans += ch * cnt
        return ans

s = "deeedbbcccbdaa"
k = 3
a = Solution().removeDuplicates(s,k)
print("ans:", a)


a = [0, 0]
if not a:
    print("n")
else:
    print("y")

print(any(a))

a = "/"
x = a.split('/')
if any(x):
    print('yes')