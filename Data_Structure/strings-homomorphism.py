class Solution:
    """
    @param: s: a string
    @param: t: a string
    @return: true if the characters in s can be replaced to get t or false
    """
    def isIsomorphic(self, s, t):
        # write your code here

        hashmap = {}

        for i in range(len(s)):
            if hashmap.get(s[i], None) is None:
                hashmap[s[i]] = t[i]
            else:
                if hashmap[s[i]] != t[i]:
                    return False



        hashmap2 = {}
        for j in range(len(t)):
            if hashmap2.get(t[j], None) is None:
                hashmap2[t[j]] = s[j]
            else:
                if hashmap2[t[j]] != s[j]:
                    return False

        return True

s = "foo"
t = "bar"
s = "paper"
t = "title"
print(Solution().isIsomorphic(s, t))
