import bisect
class Solution(object):
    def kEmptySlots(self, flowers, k):
        """
        :type flowers: List[int]
        :type k: int
        :rtype: int
        """
        days = [0] * len(flowers)
        for i in range(len(flowers)):
            days[flowers[i]-1] = i
        print("F:",flowers)
        print("D:",days)
        result = float("inf")
        i, left, right = 0, 0, k+1
        
        while right < len(days):
            if days[i] < days[left] or days[i] <= days[right]:  
                if i == right:
                    result = min(result, max(days[left], days[right]))
                left, right = i, k+1+i;
            i += 1
        return -1 if result == float("inf") else result+1
    
    def kEmptySlots_stephan(self, flowers, k):
        blooming = []
        for day, flower in enumerate(flowers, 1):
            i = bisect.bisect(blooming, flower)
            print(blooming)
            for neighbor in blooming[i-(i>0):i+1]:
                if abs(flower - neighbor) - 1 == k:
                    return day
            blooming.insert(i, flower)
        return -1
    
    def kEmptySlots_wrong(self, F, k):
        """
        :type flowers: List[int]
        :type k: int
        :rtype: int
        """
        if not F or len(F) == 0 :
            return -1
        size = len(F)
        deque = []

        D = [None for _ in range(size)]
        
        for day, pot in enumerate(F): 
            D[pot-1] = day
        
        print("F:",F)
        print("D:",D)
        j = 0
        winMin = []
        cand = []
        
        while j < size:
            while deque and D[j] < D[deque[-1]]:
                deque.pop()
                
            deque.append(j)
            
            if j < k - 1:
                j+=1
                continue
            
            i = j - k + 1
            while deque and deque[0] < i:
                deque.pop(0)
            
            localMn = D[deque[0]]
            winMin.append(localMn)

            if i > 0 and j+1 < size:
                if localMn > D[i-1] and localMn > D[j+1]:
                    print(localMn)
                    cand.append(max(D[i-1],D[j+1])+1)
                
            j+=1

        if cand:
            return min(cand)
        return -1
    
        
F = [1,2,3,4,5]
k = 2
F = [1,3,2]
k = 1
F = [1,2,3]
k = 1
F = [ 6, 5, 8, 9, 7, 1, 10, 2, 3, 4]
k = 2
F = [3,9,2,8,1,6,10,5,4,7]
k = 1

# table = ['.' for i in range(len(F))]
# for i, f in enumerate(F):
#     table[f-1] = 'T'
#     print("day:", i+1 , table)
    
a = Solution().kEmptySlots(F, k)
print("ans:",a)
a = Solution().kEmptySlots_wrong(F, k)
print("ans:",a)




blooming = [1, 2, 3, 8, 9]
