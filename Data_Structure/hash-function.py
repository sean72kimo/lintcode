class Solution:
    """
    @param key: A String you should hash
    @param HASH_SIZE: An integer
    @return an integer
    """
    def hashCode(self, key, HASH_SIZE):
        # write your code here
        ans = 0
        for i in key:
            ans = (ans * 33 + ord(i)) % HASH_SIZE
        return ans

key = "abcd"
size = 100
sol = Solution()
sol.hashCode(key, size)
