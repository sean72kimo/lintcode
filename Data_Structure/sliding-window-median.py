from  heapq import *
def heapremove1(hp, kick):
    i = hp.index(kick)
    hp[i] = -float("inf")
    m = len(hp)
    i_n = i
    while (i <= int((m - 1) / 2)):
        i_n = i * 2 + 1
        if i * 2 + 2 < m:
            if (hp[i] <= hp[i * 2 + 1]) and (hp[i] <= hp[i * 2 + 2]):
                break
            if hp[i * 2 + 1] <= hp[i * 2 + 2]:
                i_n = i * 2 + 1
            else:
                i_n = i * 2 + 2
        elif i * 2 + 1 < m:
            if (hp[i] <= hp[i * 2 + 1]):
                break
            i_n = i * 2 + 1
        else:
            break
        (hp[i], hp[i_n]) = (hp[i_n], hp[i])
        i = i_n

    while (i > 0):
        i_n = int((i - 1) / 2)
        if (hp[i_n] <= hp[i]):
            break
        (hp[i], hp[i_n]) = (hp[i_n], hp[i])
        i = i_n

    heappop(hp)


def heapremove2(hp, kick):
    i = hp.index(kick)
    hp[i] = hp[-1]
    h.pop()
    hp.heapify


class Solution:
    """
    http://blog.csdn.net/mebiuw/article/details/54408831

    @param: nums: A list of integers
    @param: k: An integer
    @return: The median of the element inside the window at each moving
    """
    ans = []

    def adjust(self, minheap, maxheap):

        # if len(nums) is odd, such as 5
        # len(maxheap) = len(minheap) + 1
        # becuase maxheap contains median

        # if len(nums) is even, such as 4
        # len(maxheap) == len(minheap)

        if len(minheap) > len(maxheap):
            heappush(maxheap, -heappop(minheap))

        if len(maxheap) > len(minheap) + 1:
            heappush(minheap, -heappop(maxheap))

        if any(minheap):
            if -maxheap[0] > minheap[0]:
                maxroot = -maxheap[0]
                minroot = minheap[0]
                heapreplace(maxheap, -minroot)
                heapreplace(minheap, maxroot)

    def medianSlidingWindow(self, nums, k):
        n = len(nums)
        minheap = []
        maxheap = []

        for i in range(n):

            num = nums[i]
            if not any(maxheap) or num <= -maxheap[0]:
                print(num)
                heappush(maxheap, -num)
            else:
                heappush(minheap, num)
            print(maxheap, minheap)
            # adjust each time when you add a new item
            self.adjust(minheap, maxheap)

            # start output median
            if i - k + 1 >= 0:

                # if k % 2 == 1:
                #    median = float(-maxheap[0])
                # else:
                #    median = (-maxheap[0] / 2) + (minheap[0] / 2)
                # self.ans.append(median)
                self.ans.append(-maxheap[0])

                kick = nums[i - k + 1]

                if kick <= -maxheap[0]:
                    # maxheap.remove(-kick)
                    heapremove1(maxheap, -kick)
                else:
                    # minheap.remove(kick)
                    heapremove1(minheap, kick)

                self.adjust(minheap, maxheap)

        return self.ans




nums = [1, 2, 7, 7, 2]
nums = [1, 2, 7, 7, 2]
nums = [1, 2, 7, 8, 5]
nums = [1, 2, 7, 7, 2]
# nums = [1, 2, 7, 7, 2]
k = 3
nums = [1, 2, 7, 7, 2, 10, 3, 4, 5]
k = 3
# nums = [0, 4, 6, 0, 3, 2, 6, 9, 4, 1]
# k = 3
# print('ans:', Solution().medianSlidingWindow(nums, k))


import bisect
class Solution2:
    def medianSlidingWindow(self, nums, k):
        window = sorted(nums[:k])
        medians = []
        lst = zip(nums, nums[k:] + [0])
        print(nums[k:] + [0])
        print(nums)
        print(list(lst))
        print(~(k // 2), (k // 2))
        for a, b in lst:
            medians.append((window[k // 2] + window[~(k // 2)]) / 2.)
            window.remove(a)
            bisect.insort(window, b)
        return medians

k = 3
nums = [1, 2, 7, 7, 2, 10, 3, 4, 5]
print('ans:', Solution2().medianSlidingWindow(nums, k))
