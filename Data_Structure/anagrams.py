class Solution:
    """
    @param strs: A list of strings
    @return: A list of strings
    """
    def anagrams(self, strs):
        # write your code here
        if len(strs) == 0:
            return []
            
        hsmap = {}
        for s in strs:
            
            string = ''.join(sorted(s))

            if string not in hsmap:
                hsmap[string] = [s]
            else:
                hsmap[string].append(s)
        ans = []
        for key, value in hsmap.items():
            if len(hsmap[key]) > 1:
                ans += value
        
        return ans
strs = ["",""]
strs = ["lint", "intl", "inlt", "code"]
# strs = ["ab", "ba", "cd", "dc", "e"]
a = Solution().anagrams(strs)
print("ans:", a)




from sys import maxsize
class Solution:
    """
    @param nums: A list of integers
    @return: An integer denote the sum of maximum subarray
    """
    def maxSubArray(self, nums):
        if nums is None or len(nums) == 0:
            return 0
        
        rsum = [0]
        maxx = -maxsize
        summ = 0
        
        
        i = 1
        for n in nums:
            rsum.append(rsum[i-1] + n)
            i+=1
        print(rsum)

        minn = maxsize
        for i, n in enumerate(rsum):
            minn = min(minn, n)
            maxx = max(n - minn, maxx)
        
        print(maxx)
        return maxx
nums = [-2,2,-3,4,-1,2,1,-5,3]
Solution().maxSubArray(nums)