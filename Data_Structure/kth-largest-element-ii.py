import heapq
from heapq import heappush, heappop
class Solution:
    # @param nums {int[]} an integer unsorted array
    # @param k {int} an integer from 1 to n
    # @return {int} the kth largest element
    def kthLargestElement2_(self, nums, k):
        # Write your code here
        nums.sort(reverse = True)
        return nums[k - 1]

    def kthLargestElement2(self, nums, k):
        heap = []
        for item in nums:
            heappush(heap, item)
            if len(heap) > k:
                heappop(heap)
        return heappop(heap)

nums = [9, 3, 2, 4, 8]
k = 3
print(Solution().kthLargestElement2(nums, k))
