"""
Definition of ListNode
"""
class ListNode(object):

    def __init__(self, val, next = None):
        self.val = val
        self.next = next

class Solution:
    """
    @param hashTable: A list of The first node of linked list
    @return: A list of The first node of linked list which have twice size
    """
    capacity = None
    def addNode2List(self, node, val):
        if node.next:
            self.addNode2List(node.next, val)
        else:
            node.next = ListNode(val)


    def addnode(self, newHashTable, val):
        p = val % self.capacity
        if newHashTable[p] == None:
            newHashTable[p] = ListNode(val)
        else:
            self.addNode2List(newHashTable[p], val)


    def rehashing(self, hashTable):
        # write your code here
        self.capacity = 2 * len(hashTable)
        newHashTable = [None for i in range(self.capacity)]

        for item in hashTable:
            n = item
            while n:
                self.addnode(newHashTable, n.val)
                n = n.next

        return newHashTable

def printNodeList(node):
    while node != None:
        if node.val:
            print(node.val, end = '->')
        else:
            print('None', end = '->')
        node = node.next
    print('None')

node9 = ListNode(9)
node21 = ListNode(21, node9)
node14 = ListNode(14)

hashTable = [None, node21, node14, None]

for n in hashTable:
    printNodeList(n)


sol = Solution()
ans = sol.rehashing(hashTable)
print("====== re-hashing ====")
for n in ans:
    printNodeList(n)
