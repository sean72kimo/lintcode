import collections


class Solution(object):

    def findStrobogrammatic(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        if n == 0:
            return []
        if n == 1:
            return ["0", "1", "8"]
        rotate = {
            "0" : "0",
            "1" : "1",
            "6" : "9",
            "8" : "8",
            "9" : "6"
        }

        que = collections.deque([])
        if n % 2 == 0:
            que.append("")
        else:
            que.append("0")
            que.append("1")
            que.append("8")

        ans = []
        while que:
            num = que.popleft()
            if len(num) == n:
                if num[0] != "0" :
                    ans.append(num)
            else:
                for k, v in rotate.items():
                    que.append(k + num + v)

        return ans


a = Solution().findStrobogrammatic(2)
print("ans:", a)
