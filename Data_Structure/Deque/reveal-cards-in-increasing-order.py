import collections


class Solution(object):
    def deckRevealedIncreasing(self, deck):
        """
        :type deck: List[int]
        :rtype: List[int]
        """
        N = len(deck)
        index = collections.deque(range(N))
        ans = [None] * N
        sorted_deck = sorted(deck)
        for card in sorted_deck:
            ans[index.popleft()] = card
            if index:
                index.append(index.popleft())
        return ans
deck = [17,13,11,2,3,5,7]
sol = Solution()
a = sol.deckRevealedIncreasing(deck)
print("ans:", a)