"""
https://stackoverflow.com/questions/4098179/anyone-know-this-python-data-structure
"""
import collections
import bisect

class FastTable:

    def __init__(self):
        self.__deque = collections.deque()

    def __len__(self):
        return len(self.__deque)

    def head(self): # O(1)
        return self.__deque.popleft()

    def tail(self): # O(1)
        return self.__deque.pop()

    def peek(self): # O(1)
        return self.__deque[-1]

    def insert(self, obj): # O(k)
        index = bisect.bisect_left(self.__deque, obj)
        self.__deque.rotate(-index)
        self.__deque.appendleft(obj)
        self.__deque.rotate(index)


deque = collections.deque([0,1,2,3,4,5,6])
print(deque)
deque.rotate(-2)
