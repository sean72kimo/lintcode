import collections


class Solution(object):
    def maxSlidingWindow(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        if k == 0 or len(nums) == 0:
            return []

        ans = []
        deque = collections.deque([])
        for i in range(len(nums)):
            while len(deque) and nums[i] >= nums[deque[-1]]:
                deque.pop()

            deque.append(i)

            if i + 1 < k:
                continue

            while len(deque) and i - deque[0] >= k:
                deque.popleft()

            ans.append(nums[deque[0]])

        return ans


# nums = [1, 3, -1, -3, 5, 3, 6, 7]
# k = 3
# a = Solution().maxSlidingWindow(nums, k)
# print("ans:", a)
