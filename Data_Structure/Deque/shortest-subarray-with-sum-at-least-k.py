import collections


class Solution(object):

    def shortestSubarray(self, A, K):
        """
        :type A: List[int]
        :type K: int
        :rtype: int
        """
        presum = [0]
        for a in A:
            presum.append(presum[-1] + a)

        n = len(A)
        d = collections.deque([])
        res = float('inf')
        print('  ', A)
        print('pre', presum)
        for i in range(n + 1):
            while d and presum[i] - presum[d[0]] >= K:
                print('now', d)
                j = d.popleft()

                dist = i - j
                if dist < res:
                    print(i, A[i - 1], j)
                    res = dist

            # j ~ i-1 這一段的sum = pre[i] - pre[j]
            # if sum <= 0，代表這一段subarray sum是無用功，無法幫助我達成 >= K
            while d and presum[i] - presum[d[-1]] <= 0:
                d.pop()

            d.append(i)

        if res == float('inf'):
            return -1

        return res


A = [84, -37, 1, -1, 32, 40, 95]
K = 167
a = Solution().shortestSubarray(A, K)
print("ans:", a)
