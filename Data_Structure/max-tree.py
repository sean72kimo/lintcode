"""
Definition of TreeNode:
"""
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

class Solution:
    # @param A: Given an integer array with no duplicates.
    # @return: The root of max tree.
    def print_stack(self, stack):
        print('[', end = '')
        for i in stack:
            print(i.val, end = ',')
        print(']')
    def maxTree(self, A):
        # write your code here
        stack = []
        for ele in A:
            node = TreeNode(ele)

            while stack and ele > stack[-1].val:

                node.left = stack.pop()
            if stack:
                stack[-1].right = node
            stack.append(node)
        self.print_stack(stack)
        return stack[0]

    def maxTree_divide_conquer(self, A):
        if A is None or len(A) == 0:
            return None
        top_root = max(A)
        top_root_idx = A.index(top_root)
        print(top_root_idx)

        top = TreeNode(top_root)
        l_top = self.maxTree_divide_conquer(A[:top_root_idx])
        r_top = self.maxTree_divide_conquer(A[top_root_idx + 1:])
        top.left = l_top
        top.right = r_top

        return top

A = [2, 5, 6, 0, 3, 1]
sol = Solution()
ans = sol.maxTree(A)
# ans = sol.maxTree_divide_conquer(A)
print('ans:', ans.val)
