import collections
class ValidWordAbbr:
    """
    @param: dictionary: a list of words
    """
    def __init__(self, dictionary):
        # do intialization if necessary

        self.dict = dictionary
        self.abbr_dict = {}

        self.d = collections.defaultdict(set)
        for word in dictionary:
            self.d[self.create_abbr(word)].add(word)
        print(self.d)

        for w in self.dict:
            abbr = self.create_abbr(w)
            if abbr not in self.abbr_dict:
                self.abbr_dict[abbr] = {w}
            else:
                self.abbr_dict[abbr].add(word)
        print(self.abbr_dict)
    """
    @param: word: a string
    @return: true if its abbreviation is unique or false
    """
    def isUnique(self, word):
        # write your code here
        if len(word) == 0:
            return True


        abbr = self.create_abbr(word)

        if abbr not in self.abbr_dict:
            return True

        if len(self.abbr_dict[abbr]) == 1 and word in self.abbr_dict[abbr]:
            return True
        else:
            return False






    def create_abbr(self, word):
        if len(word) <= 2:
            return word
        len_abbr = len(word) - 2
        abbr = word[0] + str(len_abbr) + word[-1]
        return abbr

# Your ValidWordAbbr object will be instantiated and called as such:
# obj = ValidWordAbbr(dictionary)
# param = obj.isUnique(word)

# dictionary = [ "deer", "door", "cake", "card" ]
# obj = ValidWordAbbr(dictionary)
# print(obj.isUnique("deer"))
# print(obj.isUnique("dear"))
# print(obj.isUnique("make"))

dictionary = [ "a", "a" ]
obj = ValidWordAbbr(dictionary)
print(obj.isUnique("a"))

q = collections.defaultdict(set)
q['a'] = 'i'
print(q.get('a'))

test = {
'last':'Hsu',
'first':'sean'
}

print(test)
print(test.get('last'))