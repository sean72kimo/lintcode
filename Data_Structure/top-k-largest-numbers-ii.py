from heapq import heappop, heappush, heapify
import heapq
class Solution:

    # @param {int} k an integer
    def __init__(self, k):
        # initialize your data structure here.
        self.k = k
        self.minheap = []


    # @param {int} num an integer
    def add(self, num):

        if len(self.minheap) < self.k:
            heappush(self.minheap, num)
        else:
            if num > self.minheap[0]:
                heappop(self.minheap)
                heappush(self.minheap, num)


    # @return {int[]} the top k largest numbers
    def topk(self):
        # Write your code here

        return sorted(self.minheap, reverse = True)


sol = Solution(3)
sol.add(3)
sol.add(10)
print(sol.topk())
sol.add(1000)
sol.add(-99)
print(sol.topk())
