class MyQueue:
    def __init__(self,):
        # do intialization if necessary
        self.stack1 = []
        self.stack2 = []


    def push(self, element):
        # write your code here
        self.stack1.append(element)


    def pop(self,):
        # write your code here
        return self.stack1.pop(0)


    def top(self,):
        # write your code here
        return self.stack1[0]

q = MyQueue()
q.push(1)
print(q.pop())
q.push(2)
q.push(3)
print(q.top())
print(q.pop())
