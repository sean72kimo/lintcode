class Solution(object):
    def calPoints(self, ops):
        """
        :type ops: List[str]
        :rtype: int
        """

        stack = []

        for o in ops:
            if o == "C":
                stack.pop()
                continue
            if  o == "D":
                point = stack[-1] * 2
                stack.append(point)
                continue
            if o == "+":
                point = stack[-1] + stack[-2]
                stack.append(point)
                continue

            stack.append(int(o))

        return sum(stack)

ops = ["5", "2", "C", "D", "+"]
ops = ["5", "-2", "4", "C", "D", "9", "+", "+"]
a = Solution().calPoints(ops)
print("ans:", a)
