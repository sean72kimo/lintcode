import collections


class Solution:
    """
    @param: s: a string which consists of lowercase or uppercase letters
    @return: the length of the longest palindromes that can be built
    """
    def longestPalindrome(self, s):
        # Write your code here
        hash = {}

        for c in s:
            if c in hash:
                del hash[c]
            else:
                hash[c] = True
            print(hash)

        remove = len(hash)

        if remove:
            remove -= 1

        return len(s) - remove

s = 'aaa'
print(Solution().longestPalindrome(s))


class Solution:
    def longestPalindrome(self, s: str) -> int:
        counter = collections.Counter(s)

        ans = 0
        once = 0
        for ch, cnt in counter.items():
            if cnt % 2 == 0:
                ans += cnt
            elif cnt % 2 == 1:
                ans += cnt - 1
                once = 1

        return ans + once
