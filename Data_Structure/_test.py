class Node:
    def __init__(self, val=0):
        self.val = val
        self.next = None
    def __repr__(self):
        return "<N>{}".format(self.val)

class mySet:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.mp = {}
        self.head = Node(0)
        self.tail = Node(0)
        self.head.next = self.tail
    
    def __repr__(self):
        rt = ""
        curr = self.head
        while curr:
            rt += str(curr.val) + "->"
            curr = curr.next
        rt += "None"
        return rt 

    def insert(self, val):
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        if val in self.mp:
            return False
        
        node = Node(val)
        first = self.head.next
        self.mp[first.val] = node
        self.mp[val] = self.head
        self.head.next = node
        node.next = first

        return True
    
    def remove(self, val):
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        :type val: int
        :rtype: bool
        """
        if val not in self.mp:
            return False
        
        prev = self.mp[val]
        curr = prev.next
        prev.next = curr.next
        curr.next = None
        return True

s = mySet()
print(s)
s.insert(3)
s.insert(4)
s.insert(5)
print(s)
s.remove(4)
print(s)

