class BIT(object):

    def __init__(self, n):
        self.sum = [0 for _ in range(n + 1)]

    def lowbit(self, x):
        return x & (-x)

    def query(self, i):
        rt = 0
        while i > 0:
            rt += self.sum[i]
            i -= self.lowbit(i)
        return rt

    def update(self, i, delta):
        while i < len(self.sum):
            self.sum[i] += delta
            i += self.lowbit(i)

        return


class NumArray(object):

    def __init__(self, nums):
        """
        :type nums: List[int]
        """
        self.nums = nums
        n = len(nums)
        self.tree = BIT(n)
        for i, v in enumerate(nums):
            self.tree.update(i + 1, v)

    def update(self, i, val):
        """
        :type i: int
        :type val: int
        :rtype: void
        """
        self.tree.update(i + 1, val - nums[i])
        self.nums[i] = val

    def sumRange(self, i, j):
        """
        :type i: int
        :type j: int
        :rtype: int
        """
        self.tree.query(j + 1) - self.tree.query(i)
