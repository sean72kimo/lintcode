import collections


class BIT(object):

    def __init__(self, n):
        self.sums = [0 for i in range(n + 1)]

    def lowbit(self, x):
        return x & (-x)

    def update(self, i, delta): #add
        while i < len(self.sums):
            self.sums[i] += delta
            i += self.lowbit(i)

    def query(self, i): #sum
        rt = 0
        while i > 0:
            rt += self.sums[i]
            i -= self.lowbit(i)
        return rt

    def __repr__(self):
        return "BIT:" + str(self.sums)

class Solution:
    """
    @param A: an array
    @return: total of reverse pairs
    """
    def reversePairs(self, A):
        #discretization
        r = 1
        sorted_a = sorted(set(A))
        rank = collections.defaultdict(int)
        for n in sorted_a:
            rank[n] = r
            r += 1
        print(rank)
        size = len(rank)
        bit = BIT(size)

        ans = 0
        for i in range(len(A)):
            v = rank[A[i]]
            ans += bit.query(size) - bit.query(v)
            bit.update(v, 1)
        return ans

A = [2, 4, 1, 3, 5]
A = [1,9,2,3,4] #3
A = [-1,-2] #1
A = [4,4,2]
sol = Solution()
a = sol.reversePairs(A)
print("ans:", a)

