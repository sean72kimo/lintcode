import collections


class BIT(object):

    def __init__(self, n):
        self.sums = [0 for i in range(n + 1)]

    def lowbit(self, x):
        return x & (-x)

    def update(self, i, delta):
        while i < len(self.sums):
            self.sums[i] += delta
            i += self.lowbit(i)

    def query(self, i):
        rt = 0
        while i > 0:
            rt += self.sums[i]
            i -= self.lowbit(i)
        return rt

    def __repr__(self):
        return "BIT:" + str(self.sums)


class Solution(object):
    def countSmaller(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) == 0:
            return []

        num_set = set(nums)
        sort_num = sorted(num_set)

        rank = collections.defaultdict(int)
        r = 1
        for n in sort_num:
            rank[n] = r
            r += 1
        print(rank)
        tree = BIT(len(rank))
        ans = []
        n = len(nums)
        for i in range(n - 1, -1, -1):
            r = rank[nums[i]]
            ans.append(tree.query(r - 1))
            tree.update(r, 1)
        print(tree.sums)
        return ans[::-1]


nums = [5, 2, 6, 1]
exp = [2, 1, 1, 0]

nums = [7, 1, 3, 2, 9, 2, 1]
nums = [4, 1, 3, 2, 5, 2, 1] #rank

nums = [5, 4, 1, 3, 5, 9, 7]
nums = [4, 3, 1, 2, 4, 6, 5] #rank
# exp = [5, 0, 3, 1, 2, 1, 0]
# nums = [-1,-2]
a = Solution().countSmaller(nums)
print("ans:", a)
