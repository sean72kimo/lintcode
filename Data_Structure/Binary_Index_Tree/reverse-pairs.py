import collections


class BIT(object):
    def __init__(self, n):
        self.nums = [0 for _ in range(n+1)]

    def lowbit(self, x):
        return x & (-x)

    def update(self, i, delta):
        while i < len(self.nums):
            self.nums[i] += delta
            i += self.lowbit(i)

    def query(self, i):
        rt = 0
        while i > 0:
            rt += self.nums[i]
            i -= self.lowbit(i)
        return rt


class Solution(object):
    def reversePairs(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        v = sorted(nums)
        print(v)
        m = collections.defaultdict(int)
        for i in range(len(v)):
            m[v[i]] = i + 1
        print(m)

nums = [1,3,2,3,1]
exp = 2
a = Solution().reversePairs(nums)
print("ans:", a, a == exp)
