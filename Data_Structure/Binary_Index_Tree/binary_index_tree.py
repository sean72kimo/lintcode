class BIT(object):

    def __init__(self, n):
        self.nums = [0 for _ in range(n + 1)]

    def lowbit(self, x):
        return x & (-x)

    def update(self, i, delta):
        while i < len(self.nums):
            self.nums[i] += delta
            i += self.lowbit(i)

    def query(self, i):
        rt = 0
        while i > 0:
            rt += self.nums[i]
            i -= self.lowbit(i)
        return rt
