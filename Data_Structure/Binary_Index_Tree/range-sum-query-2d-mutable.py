class NumMatrix(object):

    def __init__(self, matrix):
        m, n = len(matrix), len(matrix[0])
        self.matrix = [[0 for _ in range(n + 1)] for _ in range(m + 1)]
        self.fenwick = [[0 for _ in range(n + 1)] for _ in range(m + 1)]
        for i in range(0, m):
            for j in range(0, n):
                self.update(i, j, matrix[i][j])

    def update(self, row, col, val):
        # public interface, row and col are zero based
        row += 1
        col += 1
        delta = val - self.matrix[row][col]
        self.matrix[row][col] = val

        i, j, numRows, numCols = row, col, len(self.fenwick), len(self.fenwick[0])
        while i < numRows:
            j = col
            while j < numCols:
                self.fenwick[i][j] += delta
                j += self.lowbit(j)
            i += self.lowbit(i)

    def query(self, row, col):
        # private interface, row and col are 1 based
        i, j, result = row, col, 0
        while i > 0:
            j = col
            while j > 0:
                result += self.fenwick[i][j]
                j -= self.lowbit(j)
            i -= self.lowbit(i)
        return result

    def sumRegion(self, row1, col1, row2, col2):
        # public interface, row and col are zero based
        row2 += 1
        col2 += 1
        return self.query(row2, col2) + self.query(row1, col1) - self.query(row1, col2) - self.query(row2, col1)

    def lowbit(self, k):
        return k & -k


ops = ["NumMatrix", "sumRegion", "update", "sumRegion"]
val = [[[[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]]], [2, 1, 4, 3], [3, 2, 2], [2, 1, 4, 3]]
exp = [None, 8, None, 10]

ans = []
for i, op in enumerate(ops):
    print((i, op), val[i])
    if op == "NumMatrix":
        obj = NumMatrix(val[i][0])
        ans.append(None)
    elif op == "sumRegion":
        r1, c1, r2, c2 = val[i]
        ans.append(obj.sumRegion(r1, c1, r2, c2))
    elif op == "update":
        r, c, v = val[i]
        ans.append(obj.update(r, c, v))

print("ans:", ans)

