class Solution(object):
    def __init__(self):
        self.father = []
    def find(self,x):
        if self.father[x] == x:
            return x
        return self.find(self.father[x])
    
    def connect(self, a, b):
        if self.find(a) == -1 or self.find(b) == -1:
            return False
        
        root_a = self.find(a)
        root_b = self.find(b)
        if root_a != root_b:
            self.father[root_a] = root_b
            return True
        return False
        
    def numIslands2(self, m, n, positions):
        """
        :type m: int
        :type n: int
        :type positions: List[List[int]]
        :rtype: List[int]
        """
        self.father = [-1 for _ in range(n*m)]
        dxy = [[0, 1], [0, -1], [1, 0], [-1, 0]]
        count = 0
        ans = []
        for i, j in positions:
            idx = i*n + j
            
            if self.father[idx] == -1:
                self.father[idx] = idx
                count += 1

            for dx, dy in dxy:
                ni = i + dx
                nj = j + dy
                
                if not self.inbound(ni, nj, m, n):
                    continue
                    
                nei = ni * n + nj
                if self.connect(nei, idx):
                    count-=1
            ans.append(count)
        return ans
        
    def inbound(self, i, j, m ,n):
        return 0 <= i < m and 0 <= j < n
    
m = 3
n = 3
positions = [[0,0], [0,1], [1,2], [2,1]]
a = Solution().numIslands2(m, n, positions)
print("ans:", a)