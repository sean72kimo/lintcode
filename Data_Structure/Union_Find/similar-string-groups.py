from typing import List


class Solution:
    def numSimilarGroups(self, strs: List[str]) -> int:
        n = len(strs)
        uf = UnionFind(strs)

        for i in range(n):
            for j in range(1, n):
                if self.is_similar(strs[i], strs[j]):
                    uf.union(i,j)
        return uf.cnt

    def is_similar(self, s1, s2):
        count = 0
        for i in range(len(s1)):
            if s1[i] != s2[i]:
                count += 1
        return count == 0 or count == 2

class UnionFind:
    def __init__(self, strs):
        n = len(strs)
        self.father = [i for i in range(n)]
        self.cnt = len(strs)

    def find(self, x):
        if self.father[x] == x:
            return self.father[x]

        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        ra = self.find(a)
        rb = self.find(b)

        if ra != rb:
            self.father[ra] = rb
            self.cnt -= 1

strs = ["tars","rats","arts","star"]
exp = 2

strs = ["abc","abc"]
exp = 1
a = Solution().numSimilarGroups(strs)
print("ans:", a)