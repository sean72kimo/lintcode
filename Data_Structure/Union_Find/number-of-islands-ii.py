"""
不可將二維轉一維之後用一維判斷是否出界，要用二維判斷。例如 m = 3, n = 3 (x,y) = (1,3)  1*3+3 = 6 < 9似乎在界內，但其實出界了
如果有重複的島嶼(已經vst)在初始的 positions內，直接將count 加入ans 並且  continue
如果(nx, ny) not in vst, 代表隔壁沒有島嶼，continue
如果(nx, ny) in vst隔壁有島嶼，但是roota != rootb → 連接後 size -= 1
可用vst去判斷是否出現過這個島嶼，或者初始self.father[i] = -1，若為 -1 則代表沒有出現過該島
"""
class Solution(object):
    def __init__(self):
        self.f = []

    def find(self, x):
        if self.f[x] == x:
            return x
        self.f[x] = self.find(self.f[x])
        return self.f[x]

    def connect(self, a, b):
        if self.f[a] == -1 or self.f[b] == -1:
            return False

        root1 = self.find(a)
        root2 = self.find(b)

        if root1 != root2:
            self.f[root1] = root2
            return True

        return False

    def numIslands2(self, m, n, positions):
        ans = []
        direction = [[0, 1], [0, -1], [1, 0], [-1, 0]]
        count = 0
        k = m * n
        self.f = [-1 for i in range(k)]

        for x, y in positions:
            curr = x * n + y

            if self.f[curr] == -1:
                self.f[curr] = curr
                count += 1

            for d in direction:
                nx = x + d[0]
                ny = y + d[1]

                neighbor = nx * n + ny

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if not 0 <= neighbor < k:
                    print(x, y, nx)
                    continue

                if self.connect(curr, neighbor):
                    count -= 1

            ans.append(count)
        return ans


class Solution(object):
    def __init__(self):
        self.f = []

    def find(self, x):
        if self.f[x] == x:
            return x
        self.f[x] = self.find(self.f[x])
        return self.f[x]

    def connect(self, a, b):
        root1 = self.find(a)
        root2 = self.find(b)

        if root1 != root2:
            self.f[root1] = root2
            return False
        return True

    def numIslands2(self, m, n, positions):
        ans = []
        direction = [[0, 1], [0, -1], [1, 0], [-1, 0]]
        count = 0
        k = m * n
        self.f = [i for i in range(k)]
        vst = set()

        for x, y in positions:
            if (x, y) in vst:
                ans.append(count)
                continue

            curr = x * n + y
            vst.add((x, y))
            count += 1

            # if self.f[curr] == -1:
            #     self.f[curr] = curr
            #     count += 1

            for d in direction:
                nx = x + d[0]
                ny = y + d[1]
                neighbor = nx * n + ny

                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) not in vst:
                    continue

                if not self.connect(curr, neighbor):
                    count -= 1

            ans.append(count)
        return ans


n = 4
m = 5
A = [[1, 1], [0, 1], [3, 3], [3, 4]]

n = 3
m = 3
A = [[0,0],[2,0],[0,1],[2,1],[0,2],[2,2],[0,1],[1,2]]
exp = [1, 2, 2, 2, 2, 2, 2, 1]



# n = 3
# m = 3
# A = [[0,1],[1,2],[2,1],[1,0],[0,2],[0,0],[1,1]]
# exp = [1,2,3,4,3,2,1]


# n = 3
# m = 3
# A = [[0,0],[0,1],[1,2],[2,1]]
# exp = [1,1,2,3]

a = Solution().numIslands2(n, m, A)
print('ans:',a == exp, a)
