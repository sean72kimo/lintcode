'''
Definition for a Connection
class Connection:

    def __init__(self, city1, city2, cost):
        self.city1, self.city2, self.cost = city1, city2, cost
'''
from functools import cmp_to_key
class Solution:

    def __init__(self):
        self.father = []
        self.ans = []

    def find(self, x):
        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])
        return self.father[x]


    def mycmp(self, slf, other):
        if slf.cost != other.cost:
            return slf.cost - other.cost

        if slf.city1 != other.city1:
            if slf.city1 > other.city1:
                return 1
            else:
                return -1

        if slf.city2 > other.city2:
            return 1
        elif slf.city2 < other.city2:
            return -1
        else:
            return 0

    # @param {Connection[]} connections given a list of connections
    # include two cities and cost
    # @return {Connection[]} a list of connections from results

    def lowestCost(self, connections):
        connections.sort(key = cmp_to_key(self.mycmp))
        cityID = {}
        n = 0
        for conn in connections:
            if conn.city1 not in cityID:
                cityID[conn.city1] = n
                n += 1

            if conn.city2 not in cityID:
                cityID[conn.city2] = n
                n += 1

        self.father = [i for i in range(n)]

        ans = []
        for conn in connections:
            id1 = cityID[conn.city1]
            id2 = cityID[conn.city2]

            root1 = self.find(id1)
            root2 = self.find(id2)

            if root1 != root2:
                self.father[root1] = root2
                ans.append(conn)

        if len(ans) != n - 1:
            return []

        return ans

