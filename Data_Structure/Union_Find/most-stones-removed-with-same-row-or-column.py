import collections


class Solution(object):
    def removeStones(self, stones):
        """
        :type stones: List[List[int]]
        :rtype: int
        """

        row = {}
        col = {}
        n = 0
        for coord in stones:
            n = max(max(coord), n)
        n += 1

        N = n * n
        self.father = [i for i in range(N)]
        self.size = 0

        for i, j in stones:
            self.size += 1
            a = i * n + j
            b = None
            if i in row:
                x, y = row[i]
                b = x * n + y
                self.union(a, b)

            if j in col:
                x, y = col[j]
                b = x * n + y
                self.union(a, b)

            row[i] = (i, j)
            col[j] = (i, j)

        return len(stones) - self.size

    def union(self, a, b):
        ra = self.find(a)
        rb = self.find(b)
        if ra != rb:
            self.father[ra] = rb
            self.size -= 1
            return True
        return False

    def find(self, x):

        if x == self.father[x]:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]



class Solution(object):
    def removeStones(self, stones):
        """
        :type stones: List[List[int]]
        :rtype: int
        """

        row = {}
        col = {}
        n = 10000
        # for coord in stones:
        #     n = max(max(coord), n)
        # n += 1
        # N = n * n
        # self.father = [i for i in range(N)]

        self.father = collections.defaultdict(lambda : -1)
        self.size = 0

        for i, j in stones:
            self.size += 1
            a = i * n + j
            b = None
            if i in row:
                x, y = row[i]
                b = x * n + y
                self.union(a, b)

            if j in col:
                x, y = col[j]
                b = x * n + y
                self.union(a, b)

            row[i] = (i, j)
            col[j] = (i, j)

        return len(stones) - self.size

    def union(self, a, b):
        ra = self.find(a)
        rb = self.find(b)
        if ra != rb:
            self.father[ra] = rb
            self.size -= 1
            return True
        return False

    def find(self, x):
        if x == self.father[x]:
            return x

        if self.father[x] == -1:
            self.father[x] = x
        else:
            self.father[x] = self.find(self.father[x])
        return self.father[x]
stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]] #5
stones = [[0,0],[0,2],[1,1],[2,0],[2,2]] #3
# stones = [[0,0]] #0
#
stones = [[0,1],[1,0],[1,1]] #2
sol = Solution()
a = sol.removeStones(stones)
print("ans:", a)
