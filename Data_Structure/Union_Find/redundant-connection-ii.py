class Solution:
    def findRedundantDirectedConnection(self, edges: List[List[int]]) -> List[int]:
        n = len(edges)
        self.father = [i for i in range(n + 1)]
        parent = [0 for _ in range(n + 1)]
        edge1 = None
        edge2 = None
        lastEdgeCauseCircle = None

        for u, v in edges:

            if parent[v]:
                edge1 = [parent[v], v]
                edge2 = [u, v]
            else:
                parent[v] = u
                ru = self.find(u)
                rv = self.find(v)

                if ru != rv:
                    self.father[rv] = ru
                else:
                    lastEdgeCauseCircle = [u, v]

        if edge1 and edge2:
            if lastEdgeCauseCircle:
                return edge1
            else:
                return edge2

        return lastEdgeCauseCircle

    def find(self, x):
        if self.father[x] == x:
            return self.father[x]
        self.father[x] = self.find(self.father[x])
        return self.father[x]
