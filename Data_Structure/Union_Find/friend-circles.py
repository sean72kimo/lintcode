
class Solution(object):
    def __init__(self):
        self.father = []

    def find(self, x):
        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])
        return self.father[x]


    def findCircleNum(self, M):
        """
        :type M: List[List[int]]
        :rtype: int
        """


        self.father = list(range(len(M)))

        group = len(M)

        # build graph
        for i in range(len(M)):
            for j in range(len(M[0])):
                if M[i][j] == 1:
                    root1 = self.find(i)
                    root2 = self.find(j)

                    if root1 != root2:
                        self.father[root2] = root1
                        group -= 1
        return group

M = [[1, 1, 0],
 [1, 1, 1],
 [0, 1, 1]]
a = Solution().findCircleNum(M)
print("ans:", a)
