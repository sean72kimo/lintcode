# 九章java solution將每個listA listB裡面的每個string轉化成為int id去執行union find
# 我直接將string作為hsmap的key and value
# 1. self.father default 每個string的father為自己
# 2. union
# 3. count which string appears the most, this is largest_group
# 4. find who is larget_group
# 5. find who belongs to larget_group in self.father
import collections
class Solution:
    """
    @param ListA: The relation between ListB's books
    @param ListB: The relation between ListA's books
    @return: The answer
    """
    def __init__(self):
        self.father = collections.defaultdict(str)

    def maximumAssociationSet(self, ListA, ListB):
        # Write your code here
        # 1. self.father default 每個string的father為自己
        merge = set(ListA + ListB)
        for string in merge:
            self.father[string] = string

        # 2. union
        for i in range(len(ListA)):
            if i > len(ListB) - 1:
                break
            self.union(ListA[i], ListB[i])

        # 3. count which string appears the most, this is largest_group
        count = collections.defaultdict(int)
        for a in merge:
            rr = self.find(a)
            count[rr] += 1

        # 4. find who is larget_group
        mx = 0
        largest_group = None
        for k, v in count.items():
            if  v > mx:
                mx = v
                largest_group = k

        # 5. find who belongs to larget_group in self.father
        ans = []
        for k, v in self.father.items():
            if v == largest_group:
                ans.append(k)
        return ans


    def find(self, x):
        if self.father[x] == x:
            return self.father[x]
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        roota = self.find(a)
        rootb = self.find(b)

        if roota != rootb:
            self.father[roota] = rootb

ListA = ["abc", "abc", "abc"]
ListB = ["bcd", "acd", "def"]

ListA = ["a", "b", "d", "e", "f"]
ListB = ["b", "c", "e", "g", "g"]
a = Solution().maximumAssociationSet(ListA, ListB)
print("ans:", a)
