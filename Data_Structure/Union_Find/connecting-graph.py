"""
Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.

You need to support the following method:
1. connect(a, b), add an edge to connect node a and node b.
2.query(a, b)`, check if two nodes are connected
"""

class ConnectingGraph:

    # @param {int} n
    def __init__(self, n):
        # initialize your data structure here.
        self.father = list(range(0, n + 1))


    def find(self, x):
        if self.father[x] == x:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]


    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        # Write your code here
        root_a = self.find(a)
        root_b = self.find(b)
        if root_a != root_b:
            self.father[root_a] = root_b


    # @param {int} a, b
    # return {boolean} true if they are connected or false
    def query(self, a, b):
        # Write your code here
        root_a = self.find(a)
        root_b = self.find(b)
        return root_a == root_b


c = ConnectingGraph(5)

print(c.father)
c.connect(1, 2)
print(c.father)
c.connect(2, 4)
print(c.father)
print(c.query(1, 4))
print(c.father)
