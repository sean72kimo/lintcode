# Definition for a undirected graph node
from copy import deepcopy
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []

class Solution:
    # @param {UndirectedGraphNode[]} nodes a array of undirected graph node
    # @return {int[][]} a connected set of a undirected graph
    father = []
    ans = []

    def connectedSet(self, nodes):


        self.union_find_method(nodes)
#         self.dfs_method(nodes)
        return self.ans


    def dfs_method(self, nodes):
        self.visited = {}
        for node in nodes:
            self.visited[node] = False

        for node in nodes:
            if not self.visited[node]:
                temp = []
                self.dfs(node, temp)
                self.ans.append(sorted(temp))


    def dfs(self, node, temp):
        self.visited[node] = True
        temp.append(node.label)
        for nei in node.neighbors:
            if not self.visited[nei]:
                self.visited[node] = True
                self.dfs(nei, temp)




    def union_find_method(self, nodes):
        self.nodes = nodes
        len_nodes = len(nodes)

        self.father = {}
        for n in nodes:
            self.father[n] = n

        for node in nodes:
            for nei in node.neighbors:
                self.connect(node, nei)

        # find father in each connected graph
        roots = []
        for k, v in self.father.items():
            if v not in roots:
                roots.append(v)

        for r in roots:
            temp = []
            for k, v in self.father.items():
                if v == r:
                    temp.append(k.label)
            self.ans.append(sorted(temp))


    def find(self, x):
        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])
        return self.father[x]


    def connect(self, x, y):
        root1 = self.find(x)
        root2 = self.find(y)
        self.father[root1] = root2


node1 = UndirectedGraphNode(1)
node2 = UndirectedGraphNode(2)
node3 = UndirectedGraphNode(3)
node4 = UndirectedGraphNode(4)
node5 = UndirectedGraphNode(5)

node1.neighbors = [node2, node4]
node2.neighbors = [node1, node4]
node4.neighbors = [node2, node1]
#
node3.neighbors = [node5]
node5.neighbors = [node3]
nodes = [ node1, node2, node3, node4, node5]
# ans = Solution().connectedSet(nodes)
# print('~~~~~ ans ~~~~~', ans)

# test = '1,2,4#2,1,4#3,5#4,1,2#5,3'

test = '-15,4,-13,0,-8#-14,-3,10#-13,-15#-12,-4,-8#-11,-1#-10,13,6#-9#-8,-6,2,-15,-12#-7,6,-2,14#-6,-8#-5,-4#-4,-12,-5#-3,-14,1#-2,-7,4,9#-1,0,1,-11#0,-1,8,-15,3#1,-1,-3#2,-8,10#3,13,0#4,-15,-2#5,6,10#6,-7,-10,5#7#8,0#9,-2#10,11,5,-14,2#11,10#12#13,-10,3#14,-7,14,14'
from lib.undirected_graph import CreateGraph
graph = CreateGraph()
all_nodes = graph.create(test)
graph.print_nodes(all_nodes)

ans = Solution().connectedSet(all_nodes)
print('~~~~~ ans ~~~~~', ans)


















