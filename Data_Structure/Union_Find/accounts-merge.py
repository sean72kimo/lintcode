import collections
from typing import List

class Solution:
    def __init__(self):
        self.father = []

    def find(self, x):
        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        roota = self.find(a)
        rootb = self.find(b)

        if roota != rootb:
            self.father[roota] = rootb

    def accountsMerge(self, accounts: List[List[str]]) -> List[List[str]]:
        email_mp = collections.defaultdict(int)
        id_mp = collections.defaultdict(int)
        email_to_name_mp = collections.defaultdict(str)
        r = 0

        for account in accounts:
            for email in account[1:]:
                if email in email_mp:
                    continue
                email_mp[email] = r
                id_mp[r] = email
                email_to_name_mp[email] = account[0]
                r += 1

        n = len(email_mp)
        self.father = [i for i in range(n)]

        for account in accounts:
            emails = account[1:]
            for i in range(len(emails)):
                if i == 0:
                    continue
                id1 = email_mp[emails[i]]
                id2 = email_mp[emails[i - 1]]
                self.union(id1, id2)

        mp = collections.defaultdict(list)
        for email, idx in email_mp.items():
            p_id = self.find(idx)
            mp[p_id].append(email)

        ans = []
        for idx, lst in mp.items():
            name = email_to_name_mp[lst[0]]
            tmp = [name] + lst
            ans.append(tmp)

        return ans


accounts = [["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["John", "johnsmith@mail.com", "john00@mail.com"], ["Mary", "mary@mail.com"], ["John", "johnnybravo@mail.com"]]
# accounts = [["David", "David0@m.co", "David4@m.co", "David3@m.co"], ["David", "David5@m.co", "David5@m.co", "David0@m.co"], ["David", "David1@m.co", "David4@m.co", "David0@m.co"], ["David", "David0@m.co", "David1@m.co", "David3@m.co"], ["David", "David4@m.co", "David1@m.co", "David3@m.co"]]
a = Solution().accountsMerge(accounts)
print("ans:", a)



class Solution(object):
    def __init__(self):
        self.father = collections.defaultdict(str)
        self.father = []

    def find(self, x):
        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def union(self, a, b):
        roota = self.find(a)
        rootb = self.find(b)
        if roota != rootb:
            self.father[rootb] = roota

    def accountsMerge(self, accounts):
        """
        :type accounts: List[List[str]]
        :rtype: List[List[str]]
        """

        if accounts is None or len(accounts) == 0:
            return []

        # build graph
        c = 0
        self.father = list(range(len(accounts)))
        email_to_id = {}
        id_to_name = {}
        mapping = collections.defaultdict(int)

        for i, account in enumerate(accounts):
            for email in account[1:]:
                if email not in mapping:
                    mapping[email] = i
                else:
                    self.union(mapping[email], i)

        print(mapping)
        print(self.father)

        ans = collections.defaultdict(list)
        for email, account_idx in mapping.items():
            i = self.find(account_idx)
            if i not in ans:
                ans[i].append(accounts[i][0])
            ans[i].append(email)


        for value in ans.values():
            value[1:] = sorted(value[1:])
        return list(ans.values())
accounts = [["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["John", "johnsmith@mail.com", "john00@mail.com"], ["Mary", "mary@mail.com"], ["John", "johnnybravo@mail.com"]]
# accounts = [["David", "David0@m.co", "David4@m.co", "David3@m.co"], ["David", "David5@m.co", "David5@m.co", "David0@m.co"], ["David", "David1@m.co", "David4@m.co", "David0@m.co"], ["David", "David0@m.co", "David1@m.co", "David3@m.co"], ["David", "David4@m.co", "David1@m.co", "David3@m.co"]]
b = Solution().accountsMerge(accounts)
print("ans:", b)

