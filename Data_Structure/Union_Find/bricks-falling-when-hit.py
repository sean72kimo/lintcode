from typing import List


class UnionFindSet:
    def __init__(self, max_size):
        self.father = [i for i in range(max_size)]
        self.size = [1] * max_size

    def find(self, x):
        if self.father[x] == x:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    def merge(self, a, b):
        ra = self.find(a)
        rb = self.find(b)

        if ra != rb:
            self.father[rb] = ra
            self.size[ra] += self.size[rb]

    def getSize(self, x):
        return self.size[self.find(x)]


class Solution:
    def hitBricks(self, grid: List[List[int]], hits: List[List[int]]) -> List[int]:
        self.rows = len(grid)
        self.cols = len(grid[0])
        edgePoint = self.rows * self.cols
        dxy = [[0,1],[1,0],[0,-1],[-1,0]]
        ufs = UnionFindSet(self.rows * self.cols + 1)

        for hit in hits:
            grid[hit[0]][hit[1]] -= 1

        ufs = UnionFindSet(self.rows * self.cols + 1)
        edgePoint = self.rows * self.cols

        for i in range(self.cols):
            if grid[0][i] == 1:
                ufs.merge(self.ID(0, i), edgePoint)

        for i in range(self.rows):
            for j in range(self.cols):
                if grid[i][j] != 1:
                    continue
                if j + 1 < self.cols and grid[i][j + 1] == 1:
                    ufs.merge(self.ID(i, j), self.ID(i, j + 1))
                if i + 1 < self.rows and grid[i + 1][j] == 1:
                    ufs.merge(self.ID(i, j), self.ID(i + 1, j))

        hitCnt = len(hits)
        ret = [0] * hitCnt

        for i in range(hitCnt - 1, -1, -1):
            x, y = hits[i][0], hits[i][1]
            grid[x][y] += 1

            if grid[x][y] != 1:
                continue

            edgeBricks = ufs.getSize(edgePoint)
            if x == 0:
                ufs.merge(self.ID(x, y), edgePoint)

            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                if 0 <= nx < self.rows and 0 <= ny < self.cols and grid[nx][ny] == 1:
                    ufs.merge(self.ID(x, y), self.ID(nx, ny))

            ret[i] = max(ufs.getSize(edgePoint) - edgeBricks - 1 , 0)

        return ret

    def ID(self, x, y):
        return x * self.cols + y

grid = [[1,0,0,0],[1,1,1,0]]
hits = [[1,0]]
exp = [2]


grid = [[1,1,1],[0,1,0],[0,0,0]]
hits = [[0,2],[2,0],[0,1],[1,2]]
exp = [0,0,1,0]

sol = Solution()
a = sol.hitBricks(grid, hits)
print("ans:", a)