class ConnectingGraph3:

    # @param {int} n
    def __init__(self, n):
        self.father = list(range(0, n + 1))
        self.count = n


    def find(self, x):
        if self.father[x] == x:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]


    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        # Write your code here
        root_a = self.find(a)
        root_b = self.find(b)
        if root_a != root_b:
            self.father[root_a] = root_b
            self.count -= 1



    # @param {int} a
    # return {int} the number of connected component
    # in the graph
    def query(self):
        return self.count

c = ConnectingGraph3(5)
print(c.query())
c.connect(1, 2)
print(c.query())
c.connect(2, 4)
print(c.query())
