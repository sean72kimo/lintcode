# n = 5
# edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
# time  O(2*len(edges))
# space O(n)
class Solution_UnionFind:
    # @param {int} n an integer
    # @param {int[][]} edges a list of undirected edges
    # @return {boolean} true if it's a valid tree, or false

    father = []
    def validTree(self, n, edges):
        self.father = list(range(0 , n))
        for i in edges:
            root1 = self.find(i[0])
            root2 = self.find(i[1])

            if root1 == root2:
                return False
            else:
                self.father[root1] = root2
        return len(edges) == n - 1


    def find(self, n):
        if self.father[n] == n:
            return n
        else:
            self.father[n] = self.find(self.father[n])
        return self.father[n]


n = 5
edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
s = Solution_UnionFind()
ans = s.validTree(n, edges)
print('ans:', ans)
# print(s.father)
# sol = Solution()
# ans = sol.validTree(6, [[0,1],[0,2],[0,3],[1,4], [4,5]])
# ans = sol.validTree(5, [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]])
# ans = sol.validTree(5, [[0,1],[0,2],[0,3],[1,4]])
# print("--ans", ans)





