class Solution(object):
    def __init__(self):
        self.father = []

    def find(self, x):
        if self.father[x] == x:
            return x
        return self.find(self.father[x])

    def connect(self, a, b):
        root_a = self.find(a)
        root_b = self.find(b)
        if root_a != root_b:
            self.father[root_a] = root_b


    def areSentencesSimilarTwo(self, words1, words2, pairs):
        """
        :type words1: List[str]
        :type words2: List[str]
        :type pairs: List[List[str]]
        :rtype: bool
        """
        if len(words1) != len(words2):
            return False

        lenP = len(pairs)
        if lenP == 0 and words1 != words2:
            return False

        self.father = list(range(lenP))
        mapping = {}

        for pid, (p0, p1) in enumerate(pairs):
            if p0 not in mapping:
                mapping[p0] = pid
            else:
                self.connect(mapping[p0], pid)

            if p1 not in mapping:
                mapping[p1] = pid
            else:
                self.connect(mapping[p1], pid)



        for w1, w2 in zip(words1, words2):
            if w1 == w2:
                continue
            f1 = f2 = None
            if w1 in mapping:
                f1 = self.find(mapping[w1])
            else:
                return False

            if w2 in mapping:
                f2 = self.find(mapping[w2])
            else:
                return False

            if f1 != f2:
                return False

        return True

if __name__ == "__main__":
    words1 = ["an", "extraordinary", "meal"]
    words2 = ["one", "good", "dinner"]
    pairs = [["great", "good"], ["extraordinary", "good"], ["well", "good"], ["wonderful", "good"], ["excellent", "good"], ["fine", "good"], ["nice", "good"], ["any", "one"], ["some", "one"], ["unique", "one"], ["the", "one"], ["an", "one"], ["single", "one"], ["a", "one"], ["truck", "car"], ["wagon", "car"], ["automobile", "car"], ["auto", "car"], ["vehicle", "car"], ["entertain", "have"], ["drink", "have"], ["eat", "have"], ["take", "have"], ["fruits", "meal"], ["brunch", "meal"], ["breakfast", "meal"], ["food", "meal"], ["dinner", "meal"], ["super", "meal"], ["lunch", "meal"], ["possess", "own"], ["keep", "own"], ["have", "own"], ["extremely", "very"], ["actually", "very"], ["really", "very"], ["super", "very"]]

    words1 = ["a", "very", "delicious", "meal"]
    words2 = ["one", "really", "good", "dinner"]
    pairs = [["great", "good"], ["extraordinary", "good"], ["well", "good"], ["wonderful", "good"], ["excellent", "good"], ["fine", "good"], ["nice", "good"], ["any", "one"], ["some", "one"], ["unique", "one"], ["the", "one"], ["an", "one"], ["single", "one"], ["a", "one"], ["truck", "car"], ["wagon", "car"], ["automobile", "car"], ["auto", "car"], ["vehicle", "car"], ["entertain", "have"], ["drink", "have"], ["eat", "have"], ["take", "have"], ["fruits", "meal"], ["brunch", "meal"], ["breakfast", "meal"], ["food", "meal"], ["dinner", "meal"], ["super", "meal"], ["lunch", "meal"], ["possess", "own"], ["keep", "own"], ["have", "own"], ["extremely", "very"], ["actually", "very"], ["really", "very"], ["super", "very"]]
    a = Solution().areSentencesSimilarTwo(words1, words2, pairs)
    print("ans:", a)
