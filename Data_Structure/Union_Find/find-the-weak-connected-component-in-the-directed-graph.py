"""
find-the-weak-connected-component-in-the-directed-graph
"""

# Definition for a directed graph node
# class DirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []
# class Solution:
#     # @param {DirectedGraphNode[]} nodes a array of directed graph node
#     # @return {int[][]} a connected set of a directed graph
#     def __init__(self):
#         self.father = {}
#
#     def connectedSet2(self, nodes):
#         # each node at first is its own father
#         for node in nodes:
#             self.father[node] = node
#
#         # connect and find father
#         for node in nodes:
#             for nei in node.neighbors:
#                 self.connect(node, nei)
#
#         result = []
#         g = {}
#         cnt = 0
#         for node in nodes:
#             father = self.find(node)
#             if father not in g:
#                 cnt += 1
#                 g[father] = cnt
#
#             if len(result) < cnt:
#                 result.append([])
#
#             result[g[father] - 1].append(node.label)
#
#         return result
#
#
#
#     def find(self, x):
#         if self.father[x] == x:
#             return x
#
#         self.father[x] = self.find(self.father[x])
#         return self.father[x]
#
#     def connect(self, x, y):
#         rootx = self.find(x)
#         rooty = self.find(y)
#         if rootx != rooty:
#             self.father[rootx] = rooty


class Solution:
    # @param {DirectedGraphNode[]} nodes a array of directed graph node
    # @return {int[][]} a connected set of a directed graph
    def __init__(self):
        self.f = {}

    def merge(self, x, y):
        x = self.find(x)
        y = self.find(y)
        if x != y:
            self.f[x] = y

    def find(self, x):
        if self.f[x] == x:
            return x

        self.f[x] = self.find(self.f[x])
        return self.f[x]

    def connectedSet2(self, nodes):
        # Write your code here
        for node in nodes:
            self.f[node.label] = node.label

        for node in nodes:
            for nei in node.neighbors:
                self.merge(node.label, nei.label)

        result = []
        g = {}
        cnt = 0

        for node in nodes:
            x = self.find(node.label)
            if not x in g:
                cnt += 1
                g[x] = cnt


            if len(result) < cnt:
                result.append([])
                print('first', result)

            key = g[x] - 1
            print(g, x, key)

            result[key].append(node.label)

        return result



test = '1,2,4#2,4#3,5#4#5#6,5'
from lib.undirected_graph import CreateGraph
graph = CreateGraph()
all_nodes = graph.create(test)
graph.print_nodes(all_nodes)

ans = Solution().connectedSet2(all_nodes)
print('~~~~~ ans ~~~~~', ans)

