class ConnectingGraph2:

    # @param {int} n
    def __init__(self, n):
        # initialize your data structure here.
        # [0, 1, 2, 3, 4, 5]
        self.father = list(range(0, n + 1))
        self.size = [1 for _ in range(n + 1)]


    def find(self, x):
        if self.father[x] == x:
            return x
        self.father[x] = self.find(self.father[x])
        return self.father[x]

    # @param {int} a, b
    # return nothing
    def connect(self, a, b):
        root_a = self.find(a)
        root_b = self.find(b)
        if root_a != root_b:
            self.father[root_a] = root_b
            self.size[root_b] += self.size[root_a]


    # @param {int} a
    # return {int}  the number of nodes connected component
    # which include a node.
    def query(self, a):
        # Write your code here
        print(self.size)
        root_a = self.find(a)
        return self.size[root_a]

c = ConnectingGraph2(5)
print(c.query(1))
c.connect(1, 2)
print(c.query(1))
c.connect(2, 4)
print(c.query(1))
