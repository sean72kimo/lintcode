class Solution(object):
    def __init__(self):
        self.father = []

    def find(self, n):
        if self.father[n] == n:
            return n

        self.father[n] = self.find(self.father[n])
        return self.father[n]

    def countComponents(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: int
        """
        if n <= 1:
            return n

        ans = n
        self.father = list(range(n))

        for e0, e1 in edges:
            root0 = self.find(e0)
            root1 = self.find(e1)

            if root0 != root1:
                ans -= 1
                self.father[root0] = root1

        # print(self.father)
        return ans

n = 5
edges = [[0, 1], [1, 2], [3, 4], [4, 3]]
# edges = [[0, 1], [1, 2], [2, 3], [3, 4]]
a = Solution().countComponents(n, edges)
print("ans:", a)
