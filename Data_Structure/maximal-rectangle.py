class Solution:
    # @param {boolean[][]} matrix, a list of lists of boolean
    # @return {int} an integer
    def maximalRectangle(self, matrix):
        # Write your code here
        if matrix is None or len(matrix) == 0:
            return 0
        n = len(matrix)
        m = len(matrix[0])
        height = [0 for i in range(m)]
        max_area = 0

        for i in range(n):
            for j in range(m):
                if matrix[i][j]:
                    height[j] = height[j] + 1
                else:
                    height[j] = 0

            area = self.largestRectangleArea(height)
            max_area = max(max_area, area)
        return max_area


    def largestRectangleArea(self, height):
        stack = []
        area = 0
        m = len(height)
        for i in range(m + 1):
            if i == m:
                curr = -1
            else:
                curr = height[i]

            while stack and curr <= height[stack[-1]]:
                # do something
                prev_idx = stack.pop()
                h = height[prev_idx]
                if stack:
                    w = i - stack[-1] - 1
                else:
                    w = i
                area = max(area, w * h)

            stack.append(i)
        return area


matrix = [
  [1, 1, 0, 0, 1],
  [0, 1, 0, 0, 1],
  [0, 0, 1, 1, 1],
  [0, 1, 1, 1, 1],
  [0, 0, 0, 0, 1]
]

height = [0, 1, 2, 2, 4]
sol = Solution()
ans = sol.maximalRectangle(matrix)
# ans = sol.largestRectangleArea(height)
print('ans:', ans)


