"""median-data-stream
time O(nlogn)
space O(2n)
"""
import heapq
class Solution:
    """
    @param nums: A list of integers
    @return: the median of numbers
    """
    def __init__(self):
        self.maxHeap = []
        self.minHeap = []
        
    def medianII(self, A):
        # write your code here
        n = len(A)
        res = []
        if n == 0:
            return res
            
        for i in range(n):
            # insert
            if len(self.maxHeap) == 0 or A[i] <= -self.maxHeap[0]:
                heapq.heappush(self.maxHeap, -A[i])
            else:
                heapq.heappush(self.minHeap, A[i])

            # balance
            self.balance()
            
            # median
            res.append(-self.maxHeap[0])
        
        return res
            
    def balance(self):
        while len(self.maxHeap) < len(self.minHeap):
            v = heapq.heappop(self.minHeap)
            heapq.heappush(self.maxHeap, -v)
        
        while len(self.minHeap) < len(self.maxHeap) - 1:
            v = -heapq.heappop(self.maxHeap)
            heapq.heappush(self.minHeap, v)


A = [1,2,3,4,5]
a = Solution().medianII(A)
print("ans:", a)