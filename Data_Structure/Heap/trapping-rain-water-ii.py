import heapq


class Solution(object):
    def trapRainWater(self, mp):
        """
        :type heightMap: List[List[int]]
        :rtype: int
        """
        if len(mp) == 0:
            return 0

        vst = set()
        heap = []

        m = len(mp)
        n = len(mp[0])

        for i in range(m):
            for j in range(n):
                itm = (mp[i][j], i, i)
                heapq.heappush(heap, itm)
                vst.add((i, j))

        height = 0
        ans = 0
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        while heap:
            h, i, j = heapq.heappop(heap)

            if h > height:
                height = h
            else:
                ans += height - h

            for dx, dy in dxy:
                nx = i + dx
                ny = j + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue
                if (nx, ny) in vst:
                    continue

                itm = (mp[nx][ny], nx, ny)
                heapq.heappush(heap, itm)
                vst.add((nx, ny))
        return ans



grid = [
  [1, 4, 3, 1, 3, 2],
  [3, 2, 1, 3, 2, 4],
  [2, 3, 3, 2, 3, 1]
]

a = Solution().trapRainWater(grid)
print("ans:", a)
