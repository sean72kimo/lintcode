import heapq
import itertools
class Solution(object):
    def kSmallestPairs(self, nums1, nums2, k):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :type k: int
        :rtype: List[List[int]]
        """
        if nums1 is None or len(nums1) == 0 or nums2 is None or len(nums2) == 0:
            return []
        heap = []
        pairs = []

        def push(i, j):
            print(i, j)
            if i < len(nums1) and j < len(nums2):
                heapq.heappush(heap, (nums1[i] + nums2[j], i, j))

        for i in range(k):
            push(i, 0)

        while heap and len(pairs) < k:
            s, i, j = heapq.heappop(heap)
            pairs.append([nums1[i], nums2[j]])
            push(i, j + 1)


        return pairs



    def kSmallestPairs2(self, nums1, nums2, k):
        return sorted(itertools.product(nums1, nums2), key = sum)[:k]


class Solution2(object):
    def kSmallestPairs(self, nums1, nums2, k):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :type k: int
        :rtype: List[List[int]]
        """
        if len(nums1) == 0 or len(nums2) == 0:
            return []

        minheap = []
        ans = []
        for i in range(k):
            self.push(nums1, nums2, minheap, i, 0)

        while len(minheap):
            v, i, j = heapq.heappop(minheap)
            ans.append([nums1[i], nums2[j]])
            if len(ans) == k:
                return ans
            self.push(nums1, nums2, minheap, i, j + 1)

    def push(self, nums1, nums2, minheap, i, j):
        if i < len(nums1) and j < len(nums2):
            itm = (nums1[i] + nums2[j], i, j)
            heapq.heappush(minheap, itm)

nums1 = [1, 7, 11, 16]
nums2 = [2, 9, 10, 15]
k = 4



nums1 = [1,1,2]
nums2 = [1,2,3]
k = 10
print('ans:', Solution2().kSmallestPairs(nums1, nums2, k))
print('ans:', Solution().kSmallestPairs2(nums1, nums2, k))
