import heapq


class Solution(object):
    def maxPerformance(self, n, speed, efficiency, k):
        """
        :type n: int
        :type speed: List[int]
        :type efficiency: List[int]
        :type k: int
        :rtype: int
        """
        modulo = 10 ** 9 + 7

        candidates = zip(efficiency, speed)

        # 按照效率，從大到小排列
        candidates = sorted(candidates, key=lambda t: t[0], reverse=True)
        speed_heap = []
        speed_sum, perf = 0, 0

        for curr_efficiency, curr_speed in candidates:
            if len(speed_heap) > k - 1:
                speed_sum -= heapq.heappop(speed_heap)

            heapq.heappush(speed_heap, curr_speed)
            speed_sum += curr_speed
            perf = max(perf, speed_sum * curr_efficiency)

        return perf % modulo