class Solution:
    def longestDiverseString(self, a: int, b: int, c: int, heapq=None) -> str:
        max_heap = []

        if a:
            heapq.heappush(max_heap, (-a, 'a'))
        if b:
            heapq.heappush(max_heap, (-b, 'b'))
        if c:
            heapq.heappush(max_heap, (-c, 'c'))

        s = []
        while max_heap:
            first, char1 = heapq.heappop(max_heap)
            if len(s) >= 2 and s[-1] == s[-2] == char1:

                if not max_heap:  # if there is no other choice, just return
                    return ''.join(s)
                second, char2 = heappop(max_heap)  # char with second most rest numbers
                s.append(char2)

                if second + 1 < 0:  # only if there is rest number count, add it back to heap
                    heappush(max_heap, (second + 1, char2))
                heappush(max_heap, (first, char1))  # also need to put this part back to heap
                continue

            s.append(char1)

            if first + 1 < 0:
                heappush(max_heap, (first + 1, char1))

        return "".join(s)
