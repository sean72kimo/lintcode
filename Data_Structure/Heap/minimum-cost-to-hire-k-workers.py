import heapq
class Solution(object):
    def mincostToHireWorkers(self, quality, wage, K):
        from fractions import Fraction
        workers = sorted((Fraction(w, q), q, w)
                         for q, w in zip(quality, wage))
        print(workers)

        ans = float('inf')
        pool = []
        sumq = 0
        for ratio, q, _ in workers:
            heapq.heappush(pool, -q)
            sumq += q

            if len(pool) > K:
                sumq += heapq.heappop(pool)

            if len(pool) == K:
                ans = float(min(ans, ratio * sumq))

        return ans

quality = [10,20,5]
wage = [70,50,30]
K = 2

quality = [3,1,10,10,1]
wage = [4,8,2,2,7]
K = 2
a = Solution().mincostToHireWorkers(quality, wage, K)
print("ans:", a)