
# nlogn + mlogm
class SolutionTwoPointers:
    def minAvailableDuration(self, slots1, slots2, duration):
        # build up a heap containing time slots last longer than duration
        slots1.sort()
        slots2.sort()
        i = 0
        j = 0
        start = 0
        end = 1

        while i < len(slots1) and j < len(slots2):

            right = min(slots1[i][end], slots2[j][end])

            left = max(slots1[i][start], slots2[j][start])

            if right - left >= duration:
                return [left, left + duration]

            # move the one that ends earlier
            if slots1[i][end] < slots2[j][end]:
                i += 1
            else:
                j += 1


class SolutionHeap:
    def minAvailableDuration(self, slots1, slots2, duration):
        # build up a heap containing time slots last longer than duration



slots1 = [[10, 50], [60, 120], [140, 210]]
slots2 = [[0, 15], [60, 70]]
duration = 8
e = [60,68]

slots1 = [[10,50],[60,120],[140,210]]
slots2 = [[0,15],[60,70]]
duration = 12
e = []

a = Solution().minAvailableDuration(slots1, slots2, duration)

print("ans: ", a==e, a)