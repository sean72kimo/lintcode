"""median-sliding-window
time  O(n^2)
space O(2n)
"""
import heapq
class Solution:
    """
    @param nums: A list of integers
    @param k: An integer
    @return: The median of the element inside the window at each moving
    """
    def __init__(self):
        self.minHeap = []
        self.maxHeap = []
        
    def medianSlidingWindow(self, A, k):
        # write your code here
        if A is None or len(A) == 0:
            return []
        res = []
        for i in range(len(A)):
            # insert
            if len(self.maxHeap) == 0 or A[i] < -self.maxHeap[0]:
                heapq.heappush(self.maxHeap, -A[i])
            else:
                heapq.heappush(self.minHeap, A[i])
            
            # remove            
            if i - k >= 0:
                if A[i-k] <= -self.maxHeap[0]:
                    self.remove(-A[i-k], self.maxHeap)
                else:
                    self.remove(A[i-k], self.minHeap) 

            # balance
            self.balance()

            if i - k + 1 >= 0:
                res.append(-self.maxHeap[0])
        return res
                
            
    def remove(self, val, heap):
        idx = heap.index(val)
        heap.pop(idx)
        heapq.heapify(heap)
    
    def balance(self):
        while len(self.maxHeap) < len(self.minHeap):
            v = heapq.heappop(self.minHeap)
            heapq.heappush(self.maxHeap, -v)

        while len(self.minHeap) < len(self.maxHeap) - 1:
            v = -heapq.heappop(self.maxHeap)
            heapq.heappush(self.minHeap, v)
    