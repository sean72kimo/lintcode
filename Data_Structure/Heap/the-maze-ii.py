import collections
import copy
import heapq
class Solution(object):
    def shortestDistance(self, maze, start, dest):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int

        1 雷同maze 1, 但是要記錄到達每一個點的步數。可用一個 map <(x, y) : step>, vst[(nx, ny)] = d
        如果到達相同點，但是step較長，捨棄(continue)。if (nx, ny) in vst and d >= vst[(nx, ny)]: continue

        2 率先碰到dest, 一定是最短路徑

        注意！走四個方向的時候，step變數都要重新給予最早的值，再用此變數去計算長度，不然就會發生方向二採用方向一的step並持續增長
        for dx, dy in dxy:
            d = step
            nx = x + dx
            ny = y + dy
            d += 1
        """
        if len(maze) == 0:
            return -1

        m = len(maze)
        n = len(maze[0])
        dxy = [0, 1], [1, 0], [0, -1], [-1, 0],
        vst = {tuple(start): 0}
        que = [ (0, start[0], start[1], [tuple(start)] )] # 打印路徑
        while que:
            step, x, y, path = heapq.heappop(que)

            if [x, y] == dest:
                print(path)
                return step

            for dx, dy in dxy:
                d = step
                p = copy.deepcopy(path)
                nx = x + dx
                ny = y + dy
                d += 1
                p.append((nx, ny))

                while 0 <= nx < m and 0 <= ny < n and maze[nx][ny] != 1:
                    nx += dx
                    ny += dy
                    d += 1
                    p.append((nx, ny))

                nx -= dx
                ny -= dy
                d -= 1
                p.pop()

                itm = (d, nx, ny, p)

                if (nx, ny) in vst and d >= vst[(nx, ny)]:
                    continue

                heapq.heappush(que, itm)
                vst[(nx, ny)] = d
        return -1

M = [[0,0,1,0,0],[0,0,0,0,0],[0,0,0,1,0],[1,1,0,1,1],[0,0,0,0,0]]
start = [0,4]
destination = [4,4]
a = Solution().shortestDistance(M, start, destination)
print("ans:", a)

class Solution:
    def shortestDistance(self, maze, start, dest):
        """
        :type maze: List[List[int]]
        :type start: List[int]
        :type destination: List[int]
        :rtype: int

        目標：建立distance map (res)
        初始化，每個位置都是MAX steps, res[start[0], start[1]] = 0 steps
        從start出發，popleft()取得座標x, y, 並且開始滾動, 一邊滾動一邊紀錄距離 l
        停下之後，比較一下 l和res[x][y]本來的距離，看要不要更新成更短的距離
        若是此x,y可以更新res[x][y]的距離，代表距離短，持續丟入que裡面繼續走，用來對其他點更新
        若是此x,y可以更新res[x][y]的距離，代表距離遠，沒必要繼續使用，只會越走越遠
        """
        MAX = float('inf')
        m = len(maze)
        n = len(maze[0])
        res = [[MAX for _ in range(n)] for _ in range(m)]
        res[start[0]][start[1]] = 0
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        que = [(start[0], start[1])]
        que = collections.deque(que)

        while que:
            x, y = que.popleft()

            for dx, dy in dxy:
                l = res[x][y]
                nx = x + dx
                ny = y + dy
                l += 1

                while (0 <= nx < m and 0 <= ny < n) and maze[nx][ny] != 1:
                    nx += dx
                    ny += dy
                    l += 1

                nx -= dx
                ny -= dy
                l -= 1

                if res[nx][ny] > l:
                    res[nx][ny] = l
                    if [nx, ny] != dest:
                        que.append((nx, ny))

        if res[dest[0]][dest[1]] == MAX:
            return -1
        return res[dest[0]][dest[1]]

class Cell:
    def __init__(self, x, y, l):
        self.x = x
        self.y = y
        self.l = l
        
    def __lt__(self, other):
        return self.l < other.l
    def __repr__(self):
        return "<Cell>({},{}){}".format(self.x, self.y, self.l)
    
class Solution:
    """
    @param maze: the maze
    @param start: the start
    @param destination: the destination
    @return: the shortest distance for the ball to stop at the destination
    """
    def shortestDistance(self, M, start, destination):
        # write your code here
        if M is None or len(M) == 0:
            return False
            
        m = len(M)
        n = len(M[0])
        
        if start == destination:
            return 0
        
        que = []
        res = [[float('inf') for _ in range(n)] for _ in range(m)]
        vst = [[False for _ in range(n)] for _ in range(m)]
        dirs = [[0,1],[1,0],[-1,0],[0,-1]]
        p = Cell(start[0], start[1], 0)
        que.append(p)
        vst[p.x][p.y] = True

        while que:
            curr = heapq.heappop(que)

            if curr.x == destination[0] and curr.y == destination[1]:
                return curr.l
                
            for dx, dy in dirs:
                
                x, y, l = curr.x, curr.y, curr.l
                nx = x + dx
                ny = y + dy
                l += 1
                
                while 0 <= nx < m and 0 <= ny < n and M[nx][ny] != 1:
                    nx += dx
                    ny += dy
                    l += 1
                    
                nx -= dx
                ny -= dy
                l -= 1
                
                if vst[nx][ny] == True:
                    continue
                
                nxt = Cell(nx, ny, l)
                print('nxt:', nxt)
                heapq.heappush(que, nxt)
                vst[nxt.x][nxt.y] = True
        
        return -1



