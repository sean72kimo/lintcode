import heapq
from typing import List


class Solution:
    def getSkyline(self, buildings: List[List[int]]) -> List[List[int]]:
        res = []
        height = []

        for start, end, h in buildings:
            height.append([start, -h])  # 負號為上升edge, 正號為下降edge
            height.append([end, h])

        def mykey(x):
            return x[0], x[1]

        sorted_height = sorted(height, key=mykey)
        print(sorted_height)
        heap = [0]
        prev_height = 0

        for pos, h in sorted_height:
            if h < 0:
                heapq.heappush(heap, h)
            else:
                heap.remove(-h)
                heapq.heapify(heap)
            curr_height = -heap[0]
            if prev_height != curr_height:
                res.append([pos, curr_height])
                prev_height = curr_height
        return res

buildings = [ [2, 9, 10], [3, 7, 15], [5, 12, 12], [15, 20, 10], [19, 24, 8] ]
sol = Solution()
a = sol.getSkyline(buildings)
print("ans:", a)