import collections
import heapq


class SolutionSlidingWindow:
    def smallestRange(self, nums):
        """
        :type nums: List[List[int]]
        :rtype: List[int]
        """
        if not nums:
            return []
        lst = []
        for i, row in enumerate(nums):
            for num in row:
                lst.append([num, i])

        lst.sort(key=lambda x: x[0])
        count = collections.Counter()
        k = len(nums)
        left = 0
        ans = []
        for right in range(len(lst)):
            count[lst[right][1]] += 1

            while len(count) == k:
                if not ans or lst[right][0] - lst[left][0] < ans[1] - ans[0]:
                    ans = [lst[left][0], lst[right][0]]

                count[lst[left][1]] -= 1
                if count[lst[left][1]] == 0:
                    count.pop(lst[left][1])
                left += 1

        return ans


class SolutionHeap:

    def smallestRange(self, nums):
        """
        :type nums: List[List[int]]
        :rtype: List[int]
        """
        pq = []
        for i, row in enumerate(nums):
            itm = (row[0], i, 0)
            pq.append(itm)
        heapq.heapify(pq)

        right = max([row[0] for row in nums])

        ans = [float('-inf'), float('inf')]

        while pq:
            left, i, j = heapq.heappop(pq)
            if right - left < ans[1] - ans[0]:
                ans = [left, right]
            if j + 1 == len(nums[i]):
                return ans
            v = nums[i][j + 1]
            right = max(right, v)
            heapq.heappush(pq, (v, i, j + 1))


nums = [[4, 10, 15, 24, 26], [0, 9, 12, 20], [5, 18, 22, 30]]
a = Solution().smallestRange(nums)
print("ans:", a)
