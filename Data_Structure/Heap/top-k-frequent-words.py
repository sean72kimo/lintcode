import collections
import heapq
from pip._vendor.requests.packages.chardet.latin1prober import FREQ_CAT_NUM
class Solution:
    """
    @param: words: an array of string
    @param: k: An integer
    @return: an array of string
    """
    def topKFrequentWords(self, words, k):
        # write your code here
        if len(words) == 0:
            return []

        counter = collections.Counter(words)
        heap = []
        for string, count in counter.items():
            heap.append((-count, string))

        heapq.heapify(heap)
        ans = []
        while k:
            ans.append(heapq.heappop(heap)[1])
            k -= 1

        return ans
words = [
    "yes", "lint", "code",
    "yes", "code", "baby",
    "you", "baby", "chrome",
    "safari", "lint", "code",
    "body", "lint", "code"
]
# k = 3
# a = Solution().topKFrequentWords(words, k)
# print("ans:", a)

class Type:
    def __init__(self, word, freq):
        self.word = word
        self.freq = freq

    def __lt__(self, other):
        print(self.word , other.word)
        if self.freq == other.freq:
            return self.word > other.word

        return self.freq > other.freq
        return other.freq - self.freq
        return self.freq - other.freq

    def __repr__(self):
        return self.word
a = Type('a', 5)
aa = Type('aa', 2)
ab = Type('ab', 2)
lst = [a, aa, ab]
lst.sort()
print(lst)

heapq.heapify(lst)
print(lst)


class Solution2:
    def topKFrequentWords(self, words, k):
        if len(words) == 0:
            return []

        counter = collections.Counter(words)
        heap = []

        for word, count in counter.items():
            print("[]", word)
            item = Type(word, count)
            heapq.heappush(heap, item)
            if len(heap) > k:
                heapq.heappop(heap)

        ans = []
        for _ in range(k):
            ans.append(heapq.heappop(heap).word)

        return ans[::-1]
words = [
    "yes", "lint", "code",
    "yes", "code", "baby",
    "you", "baby", "chrome",
    "safari", "lint", "code",
    "body", "lint", "code"
]
# k = 3
# a = Solution2().topKFrequentWords(words, k)
# print("ans:", a)
# from functools import cmp_to_key
# arr = [1, 5, 3, 8, 7, 0, 11]
# # arr = ['a', 'ba', 'xyz']
# def mycmp(b, a):
#     print(b, a)
# #     return -a + b
#     return b > a
#     if b > a:
#         return 1
#     elif b < a:
#         return -1
#     else:
#         return 0
#
# arr.sort(key = cmp_to_key(mycmp))
# print(arr)

# print('bac' < 'b')



