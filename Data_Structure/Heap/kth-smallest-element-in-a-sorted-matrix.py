import heapq
class Solution:
    def kthSmallest(self, matrix, k):
        """
        :type matrix: List[List[int]]
        :type k: int
        :rtype: int

        time  O(klogk)
        space O(n)

        1. 每一個itm 為 (matrix[x][y], x, y)，放入minheap中
        2. itm = minheap pop, 此值(itm[0])為當前最小，透過itm 將此值相鄰的右邊與下方元素又加入minheap
        3. 注意！要檢查右邊和下方的元素是否已經出現過 vst
        4. 重複k次
        """
        m = len(matrix)
        n = len(matrix[0])

        itm = (matrix[0][0], 0, 0)
        minheap = [itm]
        dxy = [[0, 1], [1, 0]]
        vst = {(0, 0)}

        for _ in range(k):
            v, x, y = heapq.heappop(minheap)
            print((v,x, y))
            for dx, dy in dxy:
                nx = x + dx
                ny = y + dy
                if not (0 <= nx < m and 0 <= ny < n):
                    continue

                if (nx, ny) in vst:
                    continue

                itm = (matrix[nx][ny], nx, ny)
                heapq.heappush(minheap, itm)
                vst.add((nx, ny))
        return v


class SolutionBinarySearch(object):
    def kthSmallest(self, matrix, k):
        """
        :type matrix: List[List[int]]
        :type k: int
        :rtype: int
        在1 ~ 2^31-1 之間搜索可能的mid_val,
        計算整個矩陣中，數字小於mid_val的個數，進而去掉(1 ~ 2^31-1)一半的數字
        如何計數 小於 mid_val的個數？斜線般從右上往左下移懂
        time: (n + m) * log(2^31)
        """
        m = len(matrix)
        n = len(matrix[0])

        s = matrix[0][0]
        e = matrix[-1][-1]

        while s + 1 < e:
            mid = (s + e) // 2
            count = 0
            j = n - 1
            for i in range(m):
                while j >= 0 and matrix[i][j] > mid:
                    j -= 1
                count += (j + 1)
            print(mid, count)
            if count < k:
                s = mid
            else:
                e = mid

        count = 0
        j = n - 1
        for i in range(m):
            while j >= 0 and matrix[i][j] > s:
                j -= 1
            count += (j + 1)
        if count < k:
            return e
        return s



class SolutionBinarySearch_2(object):
    def kthSmallest(self, matrix, k):
        """
        :type matrix: List[List[int]]
        :type k: int
        :rtype: int
        在1 ~ 2^31-1 之間搜索可能的mid_val,
        計算整個矩陣中，數字小於mid_val的個數，進而去掉(1 ~ 2^31-1)一半的數字

        SolutionBinarySearch_2 和上面不同的地方在於如何計數 小於 mid_val的個數
        此法是loop over 所有的數字，上面的方法是是斜線般從右上往左下移動
        但是總體time complexity 變為 m * n * log(maxInt), 乾脆直接線性掃描

        time: n * m * log(2^31)
        """
        m = len(matrix)
        n = len(matrix[0])

        s = matrix[0][0]
        e = matrix[-1][-1]

        while s + 1 < e:
            mid = (s + e) // 2
            count = 0

            for i in range(m):
                for j in range(m):
                    if matrix[i][j] < mid:
                        count += 1
                    else:
                        break

            if count < k:
                s = mid
            else:
                e = mid

        count = 0
        j = n - 1
        for i in range(m):
            while j >= 0 and matrix[i][j] > s:
                j -= 1
            count += (j + 1)
        if count < k:
            return e
        return s
matrix = [[1,5,9],[10,11,13],[12,13,15]]
k = 8
exp = 13
# matrix = [[1,3,5],[6,7,12],[11,14,14]]
# k = 6
# exp = 11
a = SolutionBinarySearch2().kthSmallest(matrix, k)
print("ans:", a)
