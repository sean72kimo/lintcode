import heapq
from typing import List

"""
Heap solution 
similar to 646. Maximum Length of Pair Chain
"""


class Solution:
    def jobScheduling(self, startTime: List[int], endTime: List[int], profit: List[int]) -> int:

        jobs = []
        for i in range(len(startTime)):
            job = (startTime[i], endTime[i], profit[i])
            jobs.append(job)
        jobs.sort()

        max_profit = 0  # current profic
        heap = []
        for s, e, p in jobs:
            while heap and heap[0][0] <= s:
                end_time, profit_so_far = heapq.heappop(heap)
                max_profit = max(max_profit, profit_so_far)

            heapq.heappush(heap, (e, max_profit + p))

        ans = 0
        while heap:
            _, p = heapq.heappop(heap)
            ans = max(ans, p)

        return ans

startTime = [1,2,3,3]
endTime = [3,4,5,6]
profit = [50,10,40,70]
exp = 120
a = Solution().jobScheduling(startTime, endTime, profit)
print("ans:", a==exp , a)