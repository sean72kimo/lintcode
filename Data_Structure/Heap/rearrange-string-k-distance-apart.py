import collections, heapq


class Solution:

    def rearrangeString(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: str
        Time:  O(nlogc), c is the count of unique characters.
        Space: O(c)
        """
        if k == 0:
            return s
        counter = collections.Counter(s)
        maxheap = []
        for ch , cnt in counter.items():
            itm = (-cnt, ch)
            maxheap.append(itm)
        heapq.heapify(maxheap)

        res = []
        while len(res) < len(s):
            que = []
            for _ in range(k):
                if len(res) == len(s):
                    return ''.join(res)

                if not maxheap:
                    return ""

                cnt, ch = heapq.heappop(maxheap)
                res.append(ch)

                if cnt + 1 < 0:
                    nxt = (cnt + 1, ch)
                    que.append(nxt)

            while que:
                heapq.heappush(maxheap, que.pop())

        return ''.join(res)


s = "aabbcc"
k = 3
e = "abcabc"

s = "aaabc"
k = 3
e = ""
a = Solution().rearrangeString(s, k)
print("ans:", a, a == e)
