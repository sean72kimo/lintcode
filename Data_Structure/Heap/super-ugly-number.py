import heapq
class Solution(object):
    def nthSuperUglyNumber(self, n, primes):
        """
        :type n: int
        :type primes: List[int]
        :rtype: int
        """
        # Time:  O(n * logk) ~ O(n * klogk)
        # Space: O(k^2)
        ugly_number = 0

        heap = []
        heapq.heappush(heap, 1)
        for p in primes:
            heapq.heappush(heap, p)
            
        for _ in range(n):
            print(heap)
            ugly_number = heapq.heappop(heap)
            
            if ugly_number == 8*19:
                print("...........................")
                return ugly_number
            for i in range(len(primes)):
                if ugly_number % primes[i] == 0:
                    for j in range(i + 1):
                        print("{}*{}={}".format(ugly_number, primes[j],ugly_number * primes[j]))
                        heapq.heappush(heap, ugly_number * primes[j])
                    break

n = 30
primes = [2, 7, 13, 19] 
a = Solution().nthSuperUglyNumber(n, primes)
print("ans:", a)
