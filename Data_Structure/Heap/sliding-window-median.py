from typing import List

from sortedcontainers import SortedList


class Solution:
    def medianSlidingWindow(self, nums: List[int], k: int) -> List[float]:
        curr = SortedList()
        n = len(nums)
        ans = []

        if k % 2:
            mid = [k // 2]
        else:
            mid = [(k // 2) - 1, k // 2]

        for i in range(n):
            curr.add(nums[i])

            if len(curr) > k:
                curr.discard(nums[i - k])

            if len(curr) == k:
                tmp = []
                for j in mid:
                    tmp.append(curr[j])
                    print(tmp)
                median = sum([curr[j] for j in mid]) / len(mid)
                ans.append(median)

        return ans

nums = [1,3,-1,-3,5,3,6,7]
k = 2
a = Solution().medianSlidingWindow(nums, k)
print("ans:", a)