import heapq
class MedianFinder:

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.minHeap = []
        self.maxHeap = []


    def addNum(self, num):
        """
        :type num: int
        :rtype: void
        """
        if len(self.maxHeap) == 0 or num <= -self.maxHeap[0]:
            heapq.heappush(self.maxHeap, -num)
        else:
            heapq.heappush(self.minHeap, num)

        self.balance()

    def balance(self):
        while len(self.minheap) - len(self.maxheap) > 0:
            num = heapq.heappop(self.minHeap)
            heapq.heappush(self.maxHeap, -num)

        while len(self.maxheap) - len(self.minheap) > 1:
            num = -heapq.heappop(self.maxHeap)
            heapq.heappush(self.minHeap, num)

    def findMedian(self):
        """
        :rtype: float
        """
        if len(self.maxHeap) != len(self.minHeap):
            return -self.maxHeap[0] * 1.0

        return ((-self.maxHeap[0] * 1.0) + (self.minHeap[0] * 1.0)) / 2




ans = []
ops = ["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
val = [[], [1], [2], [], [3], []]

ops = ["MedianFinder", "addNum", "findMedian"]
val = [[], [1], []]

ops = ["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
val = [[], [1], [2], [], [3], []]

for i, op in enumerate(ops):
    print((i, op), val[i])
    if op == "MedianFinder":
        obj = MedianFinder()
        ans.append(None)
    elif op == "addNum":
        obj.addNum(val[i][0])
        ans.append(None)
    elif op == "findMedian":
        ans.append(obj.findMedian())
print("ans:", ans)

