class TrieNode:
    def __init__(self, ch):
        self.ch = ch
        self.children = {}
        self.isWord = False
        self.word = ""


class Trie:
    def __init__(self):
        self.root = TrieNode('#')

    def insert(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt
        node.isWord = True
        node.word = word

    def get_all_words(self, prefix):
        node = self.root

        for ch in prefix:
            nxt = node.children.get(ch)
            if nxt is None:
                return []
            node = nxt
        res = []
        path = []
        whole = []
        # self.dfs(node, path, res, whole)
        self.dfs_sort(node, path, res, whole)
        ans = [prefix + suffix for suffix in res]
        print(whole)
        return ans

    def dfs_sort(self, node, path, res, whole):
        if node.isWord:
            res.append(''.join(path))
            whole.append(node.word)

        for i in range(26):
            ch = chr(i + ord('a'))
            if ch not in node.children:
                continue

            path.append(ch)
            self.dfs_sort(node.children[ch], path, res, whole)
            path.pop()

    def get_ch(self, i):
        return chr(i + ord('a'))

    def dfs(self, node, path, res, whole):
        if node.isWord:
            res.append(''.join(path))
            whole.append(node.word)

        for ch in node.children:
            path.append(ch)
            self.dfs(node.children[ch], path, res, whole)
            path.pop()

words = ["ball", "area", "lead", "lady", "lade", "lbxy", "leac"]
words = ["lady", "lade"]
trie = Trie()
for word in words:
    trie.insert(word)

a = trie.get_all_words('l')
print("ans:", a)
