class TrieNode:
    def __init__(self):
        self.name = None
        self.child = {}
        self.isWord = False

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        node = self.root
        for letter in word:
            child = node.child.get(letter)

            if child:
                node = child
            else:
                child = TrieNode()
                node.child[letter] = child
                node.name = letter
                node = child

        node.isWord = True

    def search(self, word):
        node = self.root
        for letter in word:
            child = node.child.get(letter)
            if child:
                node = child
            else:
                return False

        return node.isWord

    def startWith(self, prefix):
        node = self.root
        for letter in prefix:
            child = node.child.get(letter)
            if child:
                node = child
            else:
                return []

        candi = []
        self.collectWords(prefix, node, candi)
        return candi

    def collectWords(self, prefix , node, candi):
        if node.isWord:
            return candi.append(prefix)

        for char in node.child:
            self.collectWords(prefix + char, node.child[char], candi)




class Solution2:
    """
    @param: words: a set of words without duplicates
    @return: all word squares
    """
    def __init__(self):
        self.trie = Trie()
        self.len = None
        self.square = []

    def wordSquares(self, words):
        # write your code here
        self.len = len(words[0])
        for word in words:
            self.trie.insert(word)

#         prefix = "l"
#         cands = self.trie.startWith(prefix)
#         print('cands for {0}: {1}'.format(prefix, cands))
        res = []
        sq = []
        prefix = ""
        self.dfs2(res, sq, 0, prefix)

        return res


    def dfs2(self, res, sq, idx, prefix):

        if idx == self.len:
            res.append(self.square[:])
            return

        pre = ""
        for i in range(idx):
            print('           ', i, idx, self.square)
            pre = pre + self.square[i][idx]
        print("[pre]", pre)

        cands = self.trie.startWith(pre)

        for cand in cands:
            print("pick '{0}' from {1}".format(cand, cands))
            self.square.append(cand)
            self.dfs2(res, sq, idx + 1, "")
            self.square.pop()

"""
            for i in range(idx , len(cand)):
                self.square.append(cand)
                self.dfs2(res, sq, idx + 1, "")
                self.square.pop()

"""




words = ["ball", "area", "lead", "lady", "lade"]
print('ans:', Solution2().wordSquares(words))


class Solution:
    # @param {string[]} words a set of words without duplicates
    # @return {string[][]} all word squares
    def wordSquares(self, words):
        l = len(words)
        if l == 0:
            return []
        trie = self.build_trie(words)
        res = []
        self.dfs(trie, res, [], 0, len(words[0]), '')
        return res


    def collect_trie(self, trie, res, item):
        if (not trie):
            if (len(item) > 0):
                res.append(item)
            return

        for c in trie:
            self.collect_trie(trie[c], res, item + c)


    def add_word(self, trie, word, start):
        if (start == len(word)):
            return
        if (word[start] not in trie):
            trie[word[start]] = {}
        self.add_word(trie[word[start]], word, start + 1)


    def build_trie(self, words):
        trie = {}
        for word in words:
            self.add_word(trie, word, 0)
        return trie

    def get_words(self, trie, prefix):
        for c in prefix:
            if c not in trie:
                return []
            trie = trie[c]
        res = []
        self.collect_trie(trie, res=res, item=prefix)
        return res


    def dfs(self, trie, res, item, chari, l, prefix):
#         print("pre", prefix, item)
        cands = self.get_words(trie, prefix)
        for cand in cands:
            itme_cand = item + [cand]
            if (len(item) == l - 1):

                res.append(itme_cand)
                continue

            for word in itme_cand:
                print('[]', word[chari + 1])
                n_pre = ''.join([word[chari + 1]])


            newprefix = ''.join([word[chari + 1] for word in (item + [cand])])
            print(newprefix)
            self.dfs(trie, res, item + [cand], chari + 1, l, newprefix)

#
# words = ["area", "lead", "wall", "lady", "ball", "lade"]
# print('ans:', Solution().wordSquares(words))
