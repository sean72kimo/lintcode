from collections import defaultdict


class TrieNode:
    def __init__(self):
        self.is_end = False
        self.children = {}
        self.value = None


class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, paths, value):
        node = self.root
        for i, p in enumerate(paths):
            nxt = node.children.get(p)
            if not nxt:
                if i == len(paths) - 1:
                    nxt = TrieNode()
                    node.children[p] = nxt
                else:
                    return False
            node = nxt

        if node.is_end:
            return False

        node.is_end = True
        node.value = value
        return True

    def search(self, paths):
        node = self.root

        for p in paths:
            nxt = node.children.get(p)
            if not nxt:
                return -1
            node = nxt

        if not node.is_end:
            return -1
        return node.value


class FileSystem2:

    def __init__(self):
        self.trie = Trie()

    def createPath(self, path: str, value: int) -> bool:
        if not path:
            return False

        paths = path.split('/')
        if not any(paths):
            return False
        return self.trie.insert(paths[1:], value)

    def get(self, path: str) -> int:
        if not path:
            return False

        paths = path.split('/')
        if not any(paths):
            return False

        return self.trie.search(paths[1:])

"""
# The TrieNode data structure.
class TrieNode(object):
    def __init__(self, name):
        self.map = defaultdict(TrieNode)
        self.name = name
        self.value = -1


class FileSystem:

    def __init__(self):
        self.root = TrieNode("")

    def createPath(self, path: str, value: int) -> bool:
        components = path.split("/")
        node = self.root

        # Iterate over all the components.
        for i in range(1, len(components)):

            name = components[i]
            nxt = node.map.get(name)
            if nxt is None:
                if i == len(components) - 1:
                    nxt = TrieNode(name)
                    node.map[name] = nxt
                else:
                    return False
            node = nxt

        if node.value != -1:
            return False

        node.value = value
        return True

    def get(self, path: str) -> int:

        # Obtain all the components
        cur = self.root

        # Start "curr" from the root node.
        components = path.split("/")

        # Iterate over all the components.
        for i in range(1, len(components)):

            # For each component, we check if it exists in the current node's dictionary.
            name = components[i]
            if name not in cur.map:
                return -1
            cur = cur.map[name]
        return cur.value
# Your FileSystem object will be instantiated and called as such:
# obj = FileSystem()
# param_1 = obj.createPath(path,value)
# param_2 = obj.get(path)
"""
ops = ["FileSystem","createPath","createPath","get","createPath","get"]
val = [[],["/leet",1],["/leet/code",2],["/leet/code"],["/c/d",1],["/c"]]
exp = [None, True, True, 2, False, -1]

ops = ["FileSystem","createPath","createPath","get","createPath","get"]
val = [[],["/leet",1],["/leet/code",2],["/leet/code"],["/leet/code",3],["/leet/code"]]
exp = [None,True,True,2,False,2]


ans = []
for i, op in enumerate(ops):
    if op == "FileSystem":
        obj = FileSystem2()
        ans.append(None)
    elif op == "createPath":
        ans.append(obj.createPath( val[i][0], val[i][1] ))
    elif op == "get":
        ans.append(obj.get(val[i][0]))

print(ans)