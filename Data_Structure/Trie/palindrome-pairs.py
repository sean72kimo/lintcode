class TrieNode:
    def __init__(self):
        self.isWord = False
        self.child = {}
        self.word_idx = -1

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word, i):
        node = self.root
        for letter in word:
            child = node.child.get(letter)
            if not child:
                child = TrieNode()
                node.child[letter] = child
            node = child
        node.isWord = True
        node.word_idx = i

    def search(self, word, rev):
        node = self.root
        for letter in word:
            child = node.child.get(letter)
            if not child:
                return False
            else:
                node = child
        return node.isWord


class Solution_Trie_undone(object):
    def __init__(self):
        self.ans = []

    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """

        if not words or len(words) == 0:
            return self.ans

        trie = Trie()
        idx_tbl = {}
        for idx, word in enumerate(words):
            trie.insert(word, i)
            idx_tbl[word] = idx


        for i, word in enumerate(words):
            rev = word[::-1]
            trie.search(word, rev)

        return self.ans

class Solution(object):
    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        res = []
        if not words or len(words) == 0:
            return res

        lookup = {}

        for i , w in enumerate(words):
            lookup[w] = i


        def isPalin(word):
            return word == word[::-1]

        for i in range(len(words)):
            word = words[i]
            print("working on:", word)
            for j in range(len(word) + 1):
                left = word[:j]
                right = word[j:]

#                 if not right or not left:
#                     continue


                x = lookup.get(left[::-1])
                print(left, right)
                if isPalin(right) and x is not None and x != i:
                    res.append([i, x])

                if not left:
                    continue

                y = lookup.get(right[::-1])
                if isPalin(left) and y is not None and y != i:
                    res.append([y, i])
        return res

words = ["bat", "tab", "cat"]
words = ["abcd", "dcba", "lls", "s", "sssll"]
words = ["a", "b", "c", "ab", "ac", "aa"]
# words = ["a", "ab"]
a = Solution().palindromePairs(words)
print("ans:", a)
#
# dic = {"a": 1}
# print(dic.get("b"))


