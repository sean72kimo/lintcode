class TrieNode:
    def __init__(self):
        self.isWord = False
        self.word = ""
        self.children = {}

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        node = self.root
        for c in word:
            child = node.children.get(c)
            if child is None:
                child = TrieNode()
                node.children[c] = child
            node = child
        node.isWord = True
        node.word = word

    def find(self, word):
        node = self.root
        for c in word:
            node = node.children.get(c)
            if node is None:
                return False
        return node.isWord



class Solution:
    # @param board, a list of lists of 1 length string
    # @param words: A list of string
    # @return: A list of string
    def wordSearchII(self, board, words):
        # write your code here
        if words is None or len(words) == 0:
            return []

        if board is None or len(board) == 0:
            return []

        self.ans = []
        self.directions = [[1, 0], [-1, 0], [0, 1], [0, -1]]
        n = len(board)
        m = len(board[0])

        if m == 0:
            return []

        self.visited = [[False for i in range(m)] for j in range(n)]
        self.trie = Trie()
        for w in words:
            self.trie.insert(w)

        for i in range(n):
            for j in range(m):
                char = board[i][j]
                self.search(board, i, j, self.trie.root)

        return self.ans


    def search(self, board, i, j, node):
        if node.isWord:
            if node.word not in self.ans:
                self.ans.append(node.word)

        n = len(board)
        m = len(board[0])
        if i < 0 or i >= n or j < 0 or j >= m:
            return False

        char = board[i][j]
        next_node = node.children.get(char)
        if not self.visited[i][j] and next_node:
            self.visited[i][j] = True
            for x, y in self.directions:
                self.search(board, i + x, j + y, next_node)
            self.visited[i][j] = False

board = [
  "doaf",
  "agai",
  "dcan"
]

board = ["aaa", "abb", "abb", "bbb", "bbb", "aaa", "bbb", "abb", "aab", "aba"]
for b in board:
    print(b)

word = {"dog", "dad", "dgdg", "can", "again"}
# word = {"again"}


print('ans:', Solution().wordSearchII(board, word))


