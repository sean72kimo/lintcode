class TrieNode:
    def __init__(self):
        self.child = {}
        self.isEnd = False
        self.curr = None
"""
special_search 這個方法是關鍵。
平常只要判斷整個word是否為is_word
此題是要判斷用trie裡面最長拼接，所以每個char都必須是is_word
"""
class Trie:
    def __init__(self):
        self.root = TrieNode()
        self.len = 0
        self.ans = ""
        self.all = []

    def insert(self, word):
        node = self.root
        for letter in word:
            child = node.child.get(letter)
            if not child:
                child = TrieNode()
                child.curr = letter
                node.child[letter] = child
            node = child
        node.isEnd = True

    def special_search(self, word):
        node = self.root
        for letter in word:
            child = node.child.get(letter)
            if not child or not child.isEnd:
                return False
            else:
                node = child
        return node.isEnd

    def dfs(self, node, path):

        if node == self.root:
            for letter in node.child:
                self.dfs(node.child[letter], path)

        if node.isEnd:
            path.append(node.curr)

            string = ''.join(path)
            if len(string) > len(self.ans):
                self.ans = string
            elif len(string) == len(self.ans):
                self.ans = string if string < self.ans else self.ans

            for letter in node.child:
                self.dfs(node.child[letter], path)
            path.pop()


        return



class Solution(object):
    def longestWord(self, words):
        """
        :type words: List[str]
        :rtype: str
        """
        if words is None or len(words) == 0:
            return ""
        trie = Trie()

        for word in words:
            trie.insert(word)
        trie.dfs(trie.root, [])

        return trie.ans
#         maxLen = 0
#         ans = ""
#         for word in words:
#             if trie.special_search(word):
#                 if len(word) > maxLen:
#                     maxLen = len(word)
#                     ans = word
#                 elif len(word) == maxLen:
#                     ans = word if word < ans else ans
#         return ans



def main():
    words = ["a", "banana", "b" , "ba", "app", "appl", "ap", "apple"]

    a = Solution().longestWord(words)
    print("ans:", a)

if __name__ == "__main__":
    main()
