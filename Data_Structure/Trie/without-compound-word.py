class TrieNode:
    def __init__(self, ch):
        self.ch = ch
        self.children = {}
        self.isWord = True
        self.word = ""

class Trie:
    def __init__(self):
        self.root = TrieNode("#")

    def insert(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt

        node.isWord = True
        node.word = word



class Solution:
    def withoutCompound(self, words):
        words.sort(key = len)
        trie = Trie()
        for word in words:
            trie.insert(word)


class Solution(object):
    def withoutCompound(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """

        dic = set()
        words.sort(key=len)
        ans = []
        for word in words:
            if not self.helper(dic, word) and len(word):
                ans.append(word)
            dic.add(word)
        return ans

    def helper(self, dic, s):
        n = len(s)
        f = [False for _ in range(n + 1)]
        f[0] = True
        for i in range(1, n + 1):
            for j in range(i):
                sub = s[j:i]
                if sub in dic and f[j]:
                    f[i] = True
                    break
        return f[n]

words = ['chat', 'ever', 'snapchat', 'snap', 'salesperson', 'per', 'person', 'sales', 'son', 'whatsoever', 'what', 'so']
exp = ['chat', 'ever', 'snap', 'per', 'sales', 'son', 'what', 'so']
a = Solution().withoutCompound(words)
print(sorted(a))
print(sorted(exp))



