"""
https://leetcode.com/problems/maximum-xor-of-two-numbers-in-an-array/discuss/130427/()-92
使用前綴樹記錄原輸入所有數字
每個root to leaf都是一個nums裡面的數字
若XOR要最大，那麼每次都要選擇相反的 0  or 1
"""
from typing import List
class TrieNode:
    def __init__(self, val):
        self.left = None
        self.right = None
        self.val = val


class Solution:
    def findMaximumXOR(self, nums: List[int]) -> int:
        root = TrieNode(0)
        for num in nums:
            curr = root
            for j in range(31, -1, -1):
                tmp = num & (1 << j)
                if tmp == 0:
                    if not curr.right:
                        curr.right = TrieNode(0)
                    curr = curr.right
                else:
                    if not curr.left:
                        curr.left = TrieNode(1)
                    curr = curr.left

        mx = 0
        for num in nums:
            res = 0
            curr = root
            for j in range(31, -1, -1):
                tmp = num & (1 << j)
                if curr.left and curr.right:
                    if tmp == 0:
                        curr = curr.left
                    else:
                        curr = curr.right
                else:
                    curr = curr.right if not curr.left else curr.left

                res += tmp ^ (curr.val << j)
            mx = max(mx, res)

        return mx
