import string
from string import ascii_lowercase
class TrieNode:
    def __init__(self, curr):
        self.isWord = False
        self.word = ""
        self.child = {}
        self.curr = curr

    def __repr__(self):
        return "<TrieNode>:{}".format(self.curr)

class Node:
    def __init__(self, ch):
        self.ch = ch
        self.children = {}
        self.isWord = False


class WordDictionary:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.root = Node("")

    def addWord(self, word: str) -> None:
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if not nxt:
                nxt = Node(ch)
                node.children[ch] = nxt
            node = nxt
        node.isWord = True

    def search(self, word):
        node = self.root
        return self.dfs(word, node)

    def dfs(self, word, node):
        # if len(word) == 0:
        #     return node.isWord

        for i, ch in enumerate(word):
            if ch == '.':
                sub = word[i + 1:]
                for child_ch in node.children:
                    if self.dfs(sub, node.children[child_ch]):
                        return True
                else:
                    return False

            nxt = node.children.get(ch)
            if nxt is None:
                return False
            node = nxt

        return node.isWord

    def bfs(self, word, node):
        que = [node]

        for ch in word:
            new_q = []
            if ch == '.':
                for curr in que:
                    for ch in curr.children:
                        new_q.append(curr.children[ch])
                que = new_q

            else:
                for curr in que:
                    if ch in curr.children:
                        new_q.append(curr.children[ch])
                que = new_q
        for curr in new_q:
            if curr.isWord:
                return True
        return False

    def dfs_pointer(self, word, idx, now):
        if idx == len(word):
            return now.isWord

        result = False
        c = word[idx]
        if c == '.':
            for ascii in string.ascii_letters:
                print('ascii', ascii)
                if now.children.get(ascii):
                    if self.dfs(word, idx + 1 , now.children[ascii]):
                        result = True

        elif c in now.children:
            if self.dfs(word, idx + 1 , now.children[c]):
                result = True

        else:
            result = False

        return result

    def dfs_enumerate_ascii_letters(self, word, node):
        """
        Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
        :type word: str
        :rtype: bool
        """
        print(word, node, node.curr)
        if node is None:
            return False

        if word == '':
            return node.isWord

        for i, letter in enumerate(word):
            if letter == '.':
                for ch in ascii_lowercase:
                    next_node = node.child.get(ch)
                    if self.helper(word[i + 1:], next_node, ):
                        return True
                return False

            else:

                node = node.child.get(letter)
                if node is None:
                    return False
        return node.isWord
ops = ["WordDictionary","addWord","addWord","addWord","search","search"]
val = [[],["bad"],["dad"],["mad"],["bad"],["mad"]]
exp = [None,None,None,None,True,True]

ops = ["WordDictionary","addWord","search"]
val = [[],["a"],[".a"]]
exp = [None,None,False,False]
ans = []
for i, op in enumerate(ops):
    if op == "WordDictionary":
        obj = WordDictionary()
        ans.append(None)
    elif op == "addWord":
        obj.addWord(val[i][0])
        ans.append(None)
    elif op == "search":
        r = obj.search(val[i][0])
        ans.append(r)
print(ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break