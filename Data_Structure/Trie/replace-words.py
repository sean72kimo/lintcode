class TrieNode:
    def __init__(self, curr = None):
        self.children = {}
        self.isWord = False
        self.curr = curr
        
class Solution(object):
    def __init__(self):        
        self.root = TrieNode()
        
    def replaceWords(self, dic, sentence):
        """
        :type dict: List[str]
        :type sentence: str
        :rtype: str
        """
        if len(sentence) == 0:
            return sentence
        
        tokens = sentence.split(" ")
        
        for word in dic:
            self.buildTrie(word)
        
        ans = []
        for token in tokens:
            shortest = self.getReplacement(token)
            ans.append(shortest)
        print()
        return ' '.join(ans)
    
    def buildTrie(self, word):
        node = self.root
        for letter in word:
            nxt = node.children.get(letter)
            if nxt is None:
                nxt = TrieNode(letter)
                node.children[letter] = nxt
            node = nxt
        node.isWord = True
    
    def getReplacement(self, token):
        node = self.root
        sb = ""
        for letter in token:
            nxt = node.children.get(letter)
            sb += letter
            
            if nxt:
                if nxt.isWord:
                    return sb
                node = nxt 
            else:
                return token
        return token
        
    
    
dic = ["cat", "bat", "rat"]
sentence = "the cattle was rattled by the battery"

dic = ["e","k","c","harqp","h","gsafc","vn","lqp","soy","mr","x","iitgm","sb","oo","spj","gwmly","iu","z","f","ha","vds","v","vpx","fir","t","xo","apifm","tlznm","kkv","nxyud","j","qp","omn","zoxp","mutu","i","nxth","dwuer","sadl","pv","w","mding","mubem","xsmwc","vl","farov","twfmq","ljhmr","q","bbzs","kd","kwc","a","buq","sm","yi","nypa","xwz","si","amqx","iy","eb","qvgt","twy","rf","dc","utt","mxjfu","hm","trz","lzh","lref","qbx","fmemr","gil","go","qggh","uud","trnhf","gels","dfdq","qzkx","qxw"]
sentence = "ikkbp miszkays wqjferqoxjwvbieyk gvcfldkiavww vhokchxz dvypwyb bxahfzcfanteibiltins ueebf lqhflvwxksi dco kddxmckhvqifbuzkhstp wc ytzzlm gximjuhzfdjuamhsu gdkbmhpnvy ifvifheoxqlbosfww mengfdydekwttkhbzenk wjhmmyltmeufqvcpcxg hthcuovils ldipovluo aiprogn nusquzpmnogtjkklfhta klxvvlvyh nxzgnrveghc mpppfhzjkbucv cqcft uwmahhqradjtf iaaasabqqzmbcig zcpvpyypsmodtoiif qjuiqtfhzcpnmtk yzfragcextvx ivnvgkaqs iplazv jurtsyh gzixfeugj rnukjgtjpim hscyhgoru aledyrmzwhsz xbahcwfwm hzd ygelddphxnbh rvjxtlqfnlmwdoezh zawfkko iwhkcddxgpqtdrjrcv bbfj mhs nenrqfkbf spfpazr wrkjiwyf cw dtd cqibzmuuhukwylrnld dtaxhddidfwqs bgnnoxgyynol hg dijhrrpnwjlju muzzrrsypzgwvblf zbugltrnyzbg hktdviastoireyiqf qvufxgcixvhrjqtna ipfzhuvgo daee r nlipyfszvxlwqw yoq dewpgtcrzausqwhh qzsaobsghgm ichlpsjlsrwzhbyfhm ksenb bqprarpgnyemzwifqzz oai pnqottd nygesjtlpala qmxixtooxtbrzyorn gyvukjpc s mxhlkdaycskj uvwmerplaibeknltuvd ocnn frotscysdyclrc ckcttaceuuxzcghw pxbd oklwhcppuziixpvihihp"

a = Solution().replaceWords(dic, sentence)
print("ans:", a)