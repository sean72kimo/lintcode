class TrieNode:
    def __init__(self, ch):
        self.ch = ch
        self.children = {}
        self.isWord = False
        self.word = ""


class Trie:
    def __init__(self):
        self.root = TrieNode('#')

    def insert(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                nxt = TrieNode(ch)
                node.children[ch] = nxt
            node = nxt
        node.isWord = True
        node.word = word

    def helper(self, word):
        cnt = 0
        node = self.root

        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                return cnt

            if len(node.children) == 1:
                cnt += 1

            node = nxt
        return cnt

    def autocomplete_count(self, words, s):
        for word in words:
            self.insert(word)

        return self.helper(s)

words = ["story", "snapchat", "snap"]
s = "snap"
exp = 3

# words = ["story", "snapchat", "snap", "chat"]
# s = "snap"
# exp = 2

# words = ["story", "snapchat", "snap"]
# s = "chat"
# exp = 0



trie = Trie()
a = trie.ac_count(words, s)
print("ans:", a)
