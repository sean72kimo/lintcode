from functools import cmp_to_key
class TrieNode:
    def __init__(self, ch = None):
        self.children = {}
        self.isEnd = False
        self.char = ch
        self.data = None
        self.rank = 0
        
    def __repr__(self):
        return "<Node>{}".format(self.char)
        
class AutocompleteSystem(object):

    def __init__(self, sentences, times):
        """
        :type sentences: List[str]
        :type times: List[int]
        """
        self.root = TrieNode()
        self.keyword = ""
        
        for i, word in enumerate(sentences):
            self.insert(word, times[i])
    
    def insert(self, word, hot):
        #print(word, hot)
        node = self.root
        for letter in word:
            nxt = node.children.get(letter)
            if nxt is None:
                nxt = TrieNode(letter)
                node.children[letter] = nxt
            node = nxt
        node.isEnd = True
        node.data = word
        node.rank += hot
        #print(node, node.data, node.rank)
    
    def dfs(self, root):
        
        ret = []
        if root is None:
            return []
        
        if root.isEnd:
            ret.append((root.rank, root.data))
            
        for child in root.children:
            tmp = self.dfs(root.children[child])
            ret.extend(tmp)
        
        return ret
            
    
    def search(self, sentence):
        node = self.root
        for c in sentence:
            nxt = node.children.get(c)
            if nxt is None:
                return []
            node = nxt
        return self.dfs(node)
    
    def mycmp(self, a, b):
        print(a, b)
        if a[0] != b[0]:
            return b[0] - a[0]
        
        if a[1] > b[1]:
            return 1
        elif a[1] < b[1]:
            return -1
        else:
            return 0
        
    def input(self, c):
        """
        :type c: str
        :rtype: List[str]
        """
        res = []
#         print(c)
        if c != '#':
            self.keyword += c
            res = self.search(self.keyword)
        else:
            self.insert(self.keyword, 1)
            self.keyword = ""
        
        sorted_res = sorted(res, key = cmp_to_key(self.mycmp))[:3]
        ans = []
        for itm in sorted_res:
            ans.append(itm[1])

        return ans

# Your AutocompleteSystem object will be instantiated and called as such:
# obj = AutocompleteSystem(sentences, times)
# param_1 = obj.input(c)



input = [
    [["i love you","island","iroman","i love leetcode"],[5,3,2,2]],
    ["i"],[" "],["a"],["#"]
]

sentences = input[0][0]
times = input[0][1]

auto = AutocompleteSystem(sentences, times)
oper = input[1:]
a = auto.input('i')
print("ans:", a)
# for i in oper:
#     auto.input(i[0])
