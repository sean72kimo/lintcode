import string
class TrieNode(object):
    def __init__(self):
        self.children = {}
        self.isWord = False
        self.curr = ""
    def __repr__(self):
        return "<Node>{}".format(self.curr)
class MagicDictionary(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.root = TrieNode()
        

    def buildDict(self, dict):
        """
        Build a dictionary through a list of words
        :type dict: List[str]
        :rtype: void
        """
        for word in dict:
            self.addWord(word)
        
    def addWord(self, word):
        node = self.root
        for ch in word:
            nxt = node.children.get(ch)
            if nxt is None:
                nxt = TrieNode()
                nxt.curr = ch
                node.children[ch] = nxt
            node = nxt
        return node.isWord
        

    def search(self, word):
        """
        Returns if there is any word in the trie that equals to the given word after modifying exactly one character
        :type word: str
        :rtype: bool
        """
        
        return self.dfs(self.root, word, False)
        
    def dfs(self, node, word, flag):
        
        if word == "":
            return node.isWord and flag is True
        
        for i, ch in enumerate(word):
            nxt = node.children.get(ch)
            
            if nxt is None:
                if flag is True:
                    return False
                if ch:
                    return False
                
                for letter in node.children:
                    n = node.children[letter]
                    return self.dfs(n, word[i+1: ], True)

            node = nxt
            

        return node.isWord and flag is True
    


# Your MagicDictionary object will be instantiated and called as such:
obj = MagicDictionary()
# obj.buildDict(dict)
# param_2 = obj.search(word)

op = ["buildDict", "search", "search", "search", "search"]
data = [[["hello","leetcode"]], ["hello"], ["hhllo"], ["hell"], ["leetcoded"]]
op = ["buildDict", "search"]
data = [[["hello","leetcode"]], ["leetcoded"]]
f = False
for i, o in enumerate(op):
    if o == "buildDict":
        for d in data[i]: 
            obj.buildDict(d)
    elif o == "search":
        print(obj.search(data[i][0]))