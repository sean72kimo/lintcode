from typing import List


class TrieNode():
    def __init__(self):
        self.children = {}
        self.is_word = False


class Trie():
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        node = self.root
        for char in word:
            nxt = node.children.get(char)
            if not nxt:
                nxt = TrieNode()
                node.children[char] = nxt
            node = nxt
        node.is_word = True


class StreamChecker:
    def __init__(self, words: List[str]):
        self.stream = []
        self.trie = Trie()
        for w in words:
            self.trie.insert(w[::-1])

    def query(self, letter: str) -> bool:
        self.stream.append(letter)
        i = len(self.stream) - 1
        node = self.trie.root

        # word = ''.join(self.stream)[::-1]

        while i >= 0:
            ch = self.stream[i]
            if node.is_word:
                return True

            nxt = node.children.get(ch)
            if not nxt:
                return False
            node = nxt
            i -= 1
        return node.is_word

# Your StreamChecker object will be instantiated and called as such:
# obj = StreamChecker(words)
# param_1 = obj.query(letter)






ops = ["StreamChecker","query","query","query","query","query","query","query","query","query","query","query","query"]
val = [[["cd","f","kl"]],["a"],["b"],["c"],["d"],["e"],["f"],["g"],["h"],["i"],["j"],["k"],["l"]]


ops = ["StreamChecker","query","query","query","query"]
val = [[["cd","f","kl"]],["a"],["b"],["c"],["d"]]
ans = []
for i, op in enumerate(ops):
    if op == "StreamChecker":
        obj = StreamChecker(val[i][0])
        ans.append(None)
    elif op == "query":
        ans.append(obj.query(val[i][0]))

print(ans)
