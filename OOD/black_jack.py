from abc import ABCMeta, abstractmethod
from enum import Enum

class Suit(Enum):
    HEART = 0
    DIAMOND = 1
    CLUBS = 2
    SPADE = 3

class Card(metaclass = ABCMeta):
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.is_available = True

    @property
    @abstractmethod
    def value(self):
        pass

    @value.setter
    @abstractmethod
    def value(self, other):
        pass

class BJCard(Card):
    def __init__(self, value, suit):
        super().__init__(value, suit)
        self._value = value

    def is_ace(self):
        return True if self._value == 1 else False

    def is_face(self):
        return True if 10 < self._value <= 13 else False

    @property
    def value(self):
        if self.is_ace():
            return 1
        elif self.is_face():
            return 10
        else:
            return self._value

    @value.setter
    def value(self, new_value):
        if 1 <= new_value <= 13:
            self._value = new_value
        else:
            raise ValueError('Invalid card value: {}'.format(new_value))

    def __repr__(self):
        return "<BJCard>[{},{}]".format(str(self.suit), str(self.value))

class Hand:
    def __init__(self, card = None):
        self.cards = []
    def __repr__(self):
        return ("<Hand>" + str(self.cards))
    def insertCard(self, card):
        self.cards.append(card)
    def totalValue(self):
        val = 0
        for c in self.cards:
            val += c.value
        return val

class GeneralPlayer(metaclass = ABCMeta):
    def __init__(self, name):
        # self.cards = [card, card]
        self.hand = Hand()
        self.name = name
        self.game = None
        self.stopDealing = False

    def __repr__(self):
        return ("<GeneralPlayer>" + self.name)

    def insertCard(self, card):
        self.hand.insertCard(card)

    def getNextCard(self):
        card = self.game.dealNextCard()
        self.insertCard(card)

    def getStopDealing(self):
        return self.stopDealing

    def setStopDealing(self, state):
        self.stopDealing = state

    @property
    def stop(self):
        return self.stopDealing
    @stop.setter
    def stop(self, state):
        self.stopDealing = state


class Player(GeneralPlayer):
    def __init__(self, name):
        super().__init__(name)
        self.totalBets = 100
        self.currentBets = 0
    def __repr__(self):
        return ("<Player>" + self.name)

    def join_game(self, game):
        if not isinstance(game, Game):
            raise ValueError
        game.add_player(self)

    def place_bets(self, amount):
        if amount < 1:
            raise ValueError
        self.totalBets -= amount
        self.currentBets += amount

    def updateBets(self, amount):
        self.currentBets += amount

class Dealer(GeneralPlayer):
    def __init__(self, name):
        super().__init__(name)
        print(self.hand)

    def __repr__(self):
        return ("<Dealer>" + self.name)

    def largerThan(self, player):
        if self.hand.totalValue() < player.hand.totalValue():
            player.totalBets += player.currentBets * 2
            return False
        else:
            player.currentBets = 0
            return True


class Game:
    def __init__(self, dealer):
        self.dealer = dealer
        self.players = []
        self.cards = []  # list of cards

    def compare_score(self):
        pass

    def add_player(self, player):
        if not isinstance(player, Player):
            raise ValueError
        self.players.append(player)
        player.game = self
        print("current players:", self.players)

    def shuffle(self):
        pass

    def dealInitCards(self):
        card = BJCard(2, Suit.HEART)
        participants = self.players + [self.dealer]

        for p in participants:
            p.insertCard(card)

    def dealNextCard(self):
        card = BJCard(10, Suit.HEART)
        return card

    def compareResult(self):
        for p in self.players:
            self.dealer.largerThan(p)

if __name__ == '__main__':


    dealer = Dealer("Dealer")
    Jack = Player("Jack")
    John = Player("John")

    game = Game(dealer)
    game.add_player(Jack)
    game.add_player(John)

    Sean = Player("Sean")
    Sean.join_game(game)

    game.dealInitCards()
    print("Sean:", Sean.hand, "Dealer:", dealer.hand)

    Sean.getNextCard()
    print(Sean.hand)

    game.compareResult()
    print(dealer.largerThan(Sean))



















## This is the text editor interface. 
## Anything you type or change here will be seen by the other person in real time.

"""
// 2 players
// 52 cards
// shuffle cards
// deal 5 cards to each player
// play
// give up cards to deck
// repeat ...
"""

rand() # return random number between 0 and 1
randInt() # returns random int between 0 and MAX_INT

class Game:
    def __init__(self):
        self.players = []
        self.cards = [Cards(1), Cards(2), Cards(3)] # 52

    def add_player(self, player):
        pass
    
    def deal(self):
        pass
    
    def put_back(self):
        pass
    
    def shuffle(self):
        pos = {Cards() : pos}
        vst = {Cards() : False}
        cnt = 52
        
        def get_new_pos():
            j = randInt() % 52
            return j
        
        for i, c in enumerate(self.cards):
            if vst[c] == True:
                continue
            
            old = pos[self.cards[i]]
            new = get_new_pos()
            
            while old == new:
                new = get_new_pos()
                
            self.cards[i], self.cards[new] = self.cards[new], self.cards[i]
            vst[c] == True
            vst[self.cards[new]] == True
            
        # while True:
        #     curr = get_card()
        #     if vst[curr] == True:
        #         continue

        #     old = pos[self.cards[curr]]
        #     new = get_new_pos()
        
        #     while old == new:
        #         new = get_new_pos()
            
        #     self.cards[curr], self.cards[new] = self.cards[new], self.cards[curr]
        #     vst[curr] = True
        #     cnt -= 1
        #     if cnt == 0:
        #         break
        
        
        
        

game = Game()    
game.deal()
class Hand:
    def __init__(self, card = None):
        self.cards = []

class Player:
    def __init__(self, name):
        self.name = name
        self.hand = Hand() 
        self.game

    def join_game    


class Suit(Enum):
    HEART = 0
    DIAMOND = 1
    SPARE = 2
    CLUBS = 3 
    
class Card:
    def __init__(self, value, suit):
        if 1 <= value <= 13:
            self.value = value
        else:
            raise ValueError()
        self.suit = suit

