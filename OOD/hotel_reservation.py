class Room:
    def __init__(self, cap = 1, price = 0):
        self.cap = cap
        self.price = price
        self.avalible = True

    def is_avaliable(self):
        return self.avalible

class SingleRoom(Room):
    def __init__(self, cap, price):
        super().__init__(cap, price)

class DoubleRoom(Room):
    def __init__(self, cap, price):
        super().__init__(cap, price)

class Request:
    def __init__(self, start, end , guest_cnt):
        self.start_date = start
        self.end_date = end
        self.guest_cnt = guest_cnt


class Reservations:
    def __init__(self, rooms):
        for room in rooms:
            self.reserved_rooms.append(room)

class Hotel:
    def __init__(self):
        self.rooms = [SingleRoom(), SingleRoom(), DoubleRoom(), DoubleRoom()]  # set of room obj
        self.reserved = {}  # room : [date] #list of date that is reserved
#         self.vacant = set()
#         self.occupied = set()
        self.reservations = []
        self.strategy = ReserveReqStrategy()  # ReserveReq, CancelReq, ModifyReq

    def handle_request_by_strategy(self, request):
        self.strategy = self.set_strategy(request)
        self.strategy.handle_request()

    def handle_request(self, request):
        # from request, find room for this request
        # generate reservation for this request
        # this reservation may reserve multiple rooms
        rooms = self.find_ava(self, request)
        if not rooms:
            return None
        reservation = self.make_reservation(request, rooms)
        return reservation

    def find_ava(self, request):
        start = request.start_date
        end = request.end_date
        guest_cnt = request.guest_cnt

        return True

    def make_reservation(self, request, rooms):
        start = request.start_date
        end = request.end_date
        dates = [start, start + 1, end]

        for room in rooms:
            self.reserved[room].extend(dates)
        reservation = Reservations(rooms)
        self.reservations.append(reservation)
        return reservation


single = SingleRoom(1, 100)
double = DoubleRoom(2, 200)

