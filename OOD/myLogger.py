from abc import ABC, abstractmethod
from typing import Any, Optional


class Handler(ABC):
    """
    The Handler interface declares a method for building the chain of handlers.
    It also declares a method for executing a request.
    """

    @abstractmethod
    def set_next(self, handler):
        pass

    @abstractmethod
    def write(self, request) -> Optional[str]:
        pass


class AbstractHandler(Handler):
    """
    The default chaining behavior can be implemented inside a base handler
    class.
    """
    def __init__(self, level:int):
        self._next_handler = None
        self.level = level

    def set_next(self, handler: Handler) -> Handler:
        self._next_handler = handler
        return handler


    def logMsg_handler(self, level: int, message: str) -> None:
        if level >= self.level:
            self.write(message)

        if self._next_handler:
            self._next_handler.logMsg_handler(level, message)

    @abstractmethod
    def write(self, request: Any) -> None:
        pass


"""
All Concrete Handlers either handle a request or pass it to the next handler in
the chain.
"""


class ConsoleLogger(AbstractHandler):
    def write(self, request: Any) -> None:
        print("Console print", request)


class ErrorLogger(AbstractHandler):
    def write(self, request: Any) -> None:
        print("Error print", request)


class FileLogger(AbstractHandler):
    def write(self, request: Any) -> None:
        print("File print", request)


def client_code(handler: Handler) -> None:
    """
    The client code is usually suited to work with a single handler. In most
    cases, it is not even aware that the handler is part of a chain.
    """

    for msg in [(3, "error"), (2, "save to file"), (1, "console debug")]:
        print(f"\nClient: Who can print a {msg}?")
        handler.logMsg_handler(*msg)



if __name__ == "__main__":
    console = ConsoleLogger(1)
    file = FileLogger(2)
    error = ErrorLogger(3)

    error.set_next(file).set_next(console)

    # The client should be able to send a request to any handler, not just the
    # first one in the chain.
    print("Chain: Error > File > Console")
    client_code(error)
    print("\n")

    # print("Subchain: Squirrel > Dog")
    # client_code(squirrel)