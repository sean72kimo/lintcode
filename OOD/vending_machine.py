from enum import Enum
from abc import ABCMeta, abstractmethod

class STOCK(Enum):
    COKE = 0
    SPRITE = 1

class COIN(Enum):
    DOLLAR = 1
    QUARTER = 0.25
    
    
class Item(metaclass = ABCMeta):
    @abstractmethod
    def getPrice(self):
        pass
    @abstractmethod
    def getInfo(self):
        pass
    
class Coke(Item):
    def getPrice(self):
        return 10
    def getInfo(self):
        return 'Coke'
    def __repr__(self):
        return '<Coke>'

class Sprite(Item):
    def getPrice(self):
        return 12
    def getInfo(self):
        return 'Sprite'
    def __repr__(self):
        return '<Sprite>'


class VendingMachin:
    def __init__(self):
        self.stock = {
            STOCK.COKE : 5,
            STOCK.SPRITE : 5
        }
        
        self.coins = {
            COIN.PENNY : 10,
            STOCK.QUARTER :25
        }
        
        self.toItem = {
            "A1" : STOCK.COKE,
            "A2" : STOCK.SPRITE
        }
        
        self.insertAmount = 0
        self.currSelect = None 
    def selectITem(self, btn):
        self.currSelect = self.toItem[btn]
        return self.toItem[btn] + 'selected'
        
    def insertCoin(self, coins):
        self.insertAmount = 10
        return  
    
    def executeTransaction(self):