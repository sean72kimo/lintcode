from abc import ABCMeta, abstractmethod
from enum import Enum

class Direction(Enum):
    UP = 0
    DOWN = 1

class Status(Enum):
    UP = 0
    DOWN = 1
    IDLE = 2

class StrategyInterface:
    def handleRequest(self, req, elevators): pass

class DefaultStrategy(StrategyInterface):
    def handleRequest(self, req, elevators): pass

class PeakHourStrategy(StrategyInterface):
    def handleRequest(self, req, elevators): pass

class ElevatorSystem:
    def __init__(self):
        self.elevators = []
        self.requests = []
        self.stragey = DefaultStrategy()

    def handleRequest(self, req):
        self.requests.append(req)
        normal = "8~17"
        peak = "17~18"

        # not good
        # if normal:
        #    pass
        # elif peak:
        #    pass

        # good
        self.stragey.handleRequest(req, self.elevators)

    def setStrategy(self, strategy):
        self.stragey = strategy


class Request:
    def __init__(self, level):
        self.level = level

class ExternalReq(Request):
    def __init__(self, direction, elevator, level):
        super().__init__(elevator, level)
        self.direction = direction

class InternalReq(Request):
    def __init__(self, elevator, level):
        super().__init__(elevator, level)

internal = InternalReq(1, 2)


class OverWeightExcption(Exception):
    pass
class InvalidRequest(Exception):
    pass
class ElevatorDescriptor:
    def __init__(self, elevator):
        self.elevator = elevator  # list of elevators

    def elevatorStatus(self):
        print(self.elevator)

class Elevator:
    def __init__(self):
        self.buttons = []
        # self.stops = []  # 1.sort 2. minheap
        # ## sort or minheap
        self.up_stops = []
        self.down_stops = []
        self.currentLevel = 1
        self.status = Status.UP
        self.gateOpen = False
        self.weightLimit = 500


    def handleExternalRequest(self):
        pass
    def handelInternalRequest(self):
        pass
    def _isRequestValid(self):
        pass

    # control status of the elevator. UP, DOWN, IDLE
    # we can do status control either in openGate or closeGate
    def openGate(self):
        if self.status == Status.UP:
            self.up_stops.pop(0)
        elif self.status == Status.DOWN:
            self.down_stops.pop(0)

    def closeGate(self):
        '''
        1.check overweight
        2.close the gate
        '''
        self._checkWeight()

    def _checkWeight(self):
        pass

class Button:
    def __init__(self, level, elevator):
        self.level = level
        self.elevator = elevator

    def press(self):
        return InternalReq(self.elevator, self.level)


if __name__ == "__main__":
    ElevatorSystem()
