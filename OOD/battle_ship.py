"""
每個player知道自己屬於哪一個gmae
player應該直接在board上面delpoy
或者應該由Game class提供一個deploy_ship method，然後player透過 self.game.deploy_ship() to board?
player透過self.game得知 hit_or_miss

Game class 增加 change_player
透過Game class取得現在是哪一個玩家，game.curr_player可以執行攻擊
動作結束後，透過game.change_player()選取下一個玩家進行下一個動作
"""
import random
from enum import Enum
class Direction:
    RIGHT = (0, 1)
    DOWN = (1, 0)

class AttackStatus(Enum):
    MISS = 0
    HIT = 1
    ERR = 2

class Ship:
    def __init__(self, coord, size, direction):
        self.size = size
        self.dir = direction
        self.parts = set()

        for i in range(size):
            self.parts.add(coord)
            new_coord = (coord[0] + direction[0], coord[1] + direction[1])
            coord = new_coord

    def get_size(self):
        return self.size

    def fire(self, coord):
        pass

    def is_alive(self):
        return len(self.parts) != 0
    
    def __repr__(self):
        return str(self.parts)

class Player:
    def __init__(self, name):
        self.ship = None
        self.game = None
        self.name = name

    def get_health(self):
        return len(self.ship.parts)

    def __repr__(self):
        return self.name

class Board:
    def __init__(self, m_by_n):
        x = m_by_n[0]
        y = m_by_n[1]
        self.grid = [[0 for _ in range(y)] for _ in range(x)]

    def deploy_ship(self, ship):
        for x, y in ship.parts:
            self.grid[x][y] = 1

    def valid(self, coord, size, direction):
        m = len(self.grid)
        n = len(self.grid[0])
        x, y = coord

        end_x = x + direction[0] * (size - 1)
        end_y = y + direction[1] * (size - 1)

        if not (0 <= end_x < m and 0 <= end_y < n):
            return False

        if self.grid[x][y] == 1:
            return False

        for i in range(size):
            x = x + direction[0]
            y = y + direction[1]
            if self.grid[x][y] == 1:
                return False

        return True

    def print_board(self):
        print('curr board:')
        for r in self.grid:
            print(r)

class Game:
    def __init__(self):
        self.board = Board((5, 5))
        self.player = []
        self.curr = None

    def change_player(self):
        while True:
            p = random.choice(self.player)
            if p != self.curr:
                self.curr = p
                return self.curr

    @property
    def curr_player(self):
        return self.curr

    def join_game(self, player):
        player.game = self
        self.player.append(player)
        if self.curr is None:
            self.curr = player

    def deploy_ship(self, player, coord, size, direction):
        board = self.get_board()
        valid = board.valid(coord, size, direction)
        if not valid:
            return False
        player.ship = Ship(coord, size, direction)
        board.deploy_ship(player.ship)

    def attack(self, coord):
        player = self.curr_player

        if coord in player.ship.parts:
            return AttackStatus.ERR
        player.ship.fire(coord)
        return self.hit_or_miss(coord)

    def hit_or_miss(self, coord):
        for p in self.player:
            if coord in p.ship.parts:
                p.ship.parts.remove(coord)
                return AttackStatus.HIT
        return AttackStatus.MISS

    def alive(self):
        ans = []
        for p in self.player:
            if p.ship.is_alive():
                ans.append(p)
        return ans

    def get_board(self):
        return self.board

game = Game()
player1 = Player('Sean')
player2 = Player('Jack')
game.join_game(player1)
game.join_game(player2)

game.deploy_ship(player1, (1, 1), 3, Direction().RIGHT)
game.deploy_ship(player2, (3, 1), 3, Direction().RIGHT)


game.board.print_board()
print(game.curr_player, game.attack((3, 2)))

game.change_player()
print(game.curr_player, game.attack((3, 2)))

print(game.alive())

print(game.curr_player, game.curr_player.get_health())

