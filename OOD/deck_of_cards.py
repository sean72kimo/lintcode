from abc import ABCMeta, abstractmethod
from enum import Enum

class Suit(Enum):
    HEART = 0
    DIAMOND = 1
    CLUBS = 2
    SPADE = 3

class Card(metaclass = ABCMeta):
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit
        self.is_available = True

    @property
    @abstractmethod
    def value(self):
        pass

    @value.setter
    @abstractmethod
    def value(self, other):
        pass

class BlackJackCard(Card):
    def __init__(self, value, suit):
        super(BlackJackCard, self).__init__(value, suit)
        self._value = value

    def is_ace(self):
        return True if self._value == 1 else False

    def is_face(self):
        return True if 10 < self._value <= 13 else False

    @property
    def value(self):
        if self.is_ace():
            return 1
        elif self.is_face():
            return 10
        else:
            return self._value

    @value.setter
    def value(self, new_value):
        if 1 <= new_value <= 13:
            self._value = new_value
        else:
            raise ValueError('Invalid card value: {}'.format(new_value))

class Hand:
    def __init__(self, cards):
        self.cards = cards
    def add_card(self, card):
        self.cards.append(card)
    def score(self):
        total_value = 0
        for card in self.cards:
            total_value += card.value
        return total_value

class BJHand(Hand):
    BJ = 21
    def __init__(self, cards):
        super(BJHand, self).__iniit__(cards)
    def score(self):
        return


class Deck:
    def __init__(self, cards):
        self.cards = cards
        self.deal_index = 0

    def remain_cards(self):
        return len(self.cards) - self.deal_index

    def deal_card(self):
        pass

    def shuffle(self):
        return


if __name__ == '__main__':
    card = BlackJackCard(2, Suit.HEART)
    print(card.value)
