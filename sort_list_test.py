# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None
class Solution(object):
    def sortval(self, node):
        return node.val

    def sortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        tmp = []
        if not head:
            return None

        while head:
            print(head.val)
            tmp.append(head)
            curr = head
            head = head.next
            curr.next = None


        tmp.sort(key=self.sortval)


        for i in range(len(tmp) - 1):
            tmp[i].next = tmp[i + 1]

        return tmp[0]
def stringToIntegerList(input):
    input = input.strip()
    input = input[1:-1]
    if not input:
        return []
    return [int(number) for number in input.split(",")]

def stringToListNode(input):
    # Generate list from the input
    numbers = stringToIntegerList(input)

    # Now convert that list into linked list
    dummyRoot = ListNode(0)
    ptr = dummyRoot
    for number in numbers:
        ptr.next = ListNode(number)
        ptr = ptr.next

    ptr = dummyRoot.next
    return ptr

def prettyPrintLinkedList(node):
    import sys
    while node and node.next:
        sys.stdout.write(str(node.val) + "->")
        node = node.next

    if node:
        print(node.val)
    else:
        print("Empty LinkedList")

def main():

    def readlines():
        myinput = ["[2,1]"]
        for line in myinput:
            yield line.strip('\n')

    lines = readlines()


    while True:
        try:
            line = next(lines)
            node = stringToListNode(line)
            prettyPrintLinkedList(node)
        except StopIteration:
            break

    ans = Solution().sortList(node)
    prettyPrintLinkedList(ans)

if __name__ == '__main__':
    main()
