/*平面有一堆的点point(x, y), 选出其中的两个点，这两个点连成的线，可以把坐标系里的点分成两半，两边的点的个数相同。
找出bottom left most point, 固定这个点选出的线，肯定能平分两半。*/

import java.util.*;
class Solution{
    public static double findSlop(int[] nums1, int[] nums2){
        int x = nums1[0] - nums2[0];
        int y = nums1[1] - nums2[1];
        if(x == 0){
            return y >= 0 ? Double.MAX_VALUE : Double.MIN_VALUE;
        }
        return 1.0 * y / x;
    }
    public static void findThePointsDivideHalf(int[][] nums) {
        int n = nums.length;
        if(n % 2 == 1){
            return;
        }
        int[] left = {Integer.MAX_VALUE, Integer.MAX_VALUE};
        int j = -1;
        for(int i = 0; i < n; i++){
            if(nums[i][0] < left[0]){
                left = nums[i];
                j = i;
            }
        }//3, 2, 0.5
        int[] o = new int[]{left[0], left[1]};
        Queue<Integer> q = new PriorityQueue<>((o1, o2) -> compare(o1, o2, nums, o));
        for(int i = 0; i < n; i++){
            if(j != i){
                q.add(i);
                if(q.size() >= n / 2){
                    q.remove();
                }
            }
        }
        System.out.println(Arrays.toString(left) + " , " + Arrays.toString(nums[q.peek()]));
    }//if have same slopes, we keep the point with the longest distance to the origin
    public static int compare(int o1, int o2, int[][] nums, int[] o) {
         int diff = Double.compare(findSlop(nums[o1], o), findSlop(nums[o2], o));
         return diff != 0 ? diff : nums[o1][0] - nums[o2][0];
    }
    public static void main(String[] args) {
        // int[][] maxtrix = {{0, 0}, {1, 3}, {1, 2}, {2, 1}};
        // findThePointsDivideHalf(maxtrix);
        // int[][] maxtrix1 = {{0, 0}, {1, 1}, {2, 2}, {3, 3}};
        // findThePointsDivideHalf(maxtrix1);
        int[][] maxtrix2 = {{0, 0}, {0, 0}, {0, 0}, {0, 0}};
        findThePointsDivideHalf(maxtrix2);
    }
}
