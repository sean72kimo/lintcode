class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)
node4 = TreeNode(4)
node5 = TreeNode(5)


root.left = node2
root.right = node3
node2.left = node4
node2.right = node5


"""
divide and conquer
"""
class Solution(object):
    def preorderTraversal_divide_conquer(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        try:
            print('current root', root.val)
        except:
            print('current root is None')
        res = []
        if root is None:
            return res

        left_res = self.preorderTraversal(root.left)
        right_res = self.preorderTraversal(root.right)

        res.append(root.val)
        print(res)
        res.extend(left_res)
        res.extend(right_res)

        return res

    def preorderTraversal_dfs(self, root):
        self.result = []

        if root is None:
            self.result

        self.traverse(root)
        return self.result


    def traverse(self, node):
        if node is None:
            return
        self.result.append(node.val)
        self.traverse(node.left)
        self.traverse(node.right)


sol = Solution()
ans = sol.preorderTraversal_dfs(root)

print(ans)