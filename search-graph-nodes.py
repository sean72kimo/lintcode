# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []

class Solution:
    # @param {UndirectedGraphNode[]} graph a list of undirected graph node
    # @param {dict} values a dict, <UndirectedGraphNode, (int)value>
    # @param {UndirectedGraphNode} node an Undirected graph node
    # @param {int} target an integer
    # @return {UndirectedGraphNode} a node
    def searchNode(self, graph, values, node, target):
        # Write your code here
        # for i in graph:
        #     print i.label
        #     for n in i.neighbors:
        #         print 'node', i.label, 'has neightbor', n.label

        if values[node] == target:
            return node

        visited = {}
        queue = [node]

        while queue:
            print 'Q:[',
            for i in queue:
                print i.label,
            print ']'

            current_node = queue.pop(0)
            visited[current_node.label] = True
            print 'visiting', current_node.label
            print visited


            for n in current_node.neighbors:
                print '  neighbor', n.label

                if values[n] == target:
                    return n

                # queue.append(n)
                if n.label not in visited:
                    queue.append(n)
        return None