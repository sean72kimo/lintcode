import random
class Solution:
    def reservoirSampling(self, stream, k):

        # init, put first k data from stream to res
        res = []
        for i in ragne(k):
            res.append(stream)

        # k < i < n
        # 1. res 沒有元素替換 選第 k+1 個時候，選中的概率是 k/(k+1),
        # 沒有元素替換概率是 1 - (k/(k+1)) = 1/(k+1)

        # 2. res 有一個元素替換: 選第 k+1 個時候，選中的概率是 k/(k+1), res中選一個的概率是1/k
        # 1/k * k/(k+1) = 1/(k+1)

        for i in range(k, len(stream)):
            rand = random.randint(i)
            if rand < k:
                res[rand] = stream[i]

        return res

