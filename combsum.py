class Solution(object):
    def __init__(self):
        self.ans = []

    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates.sort()
        self.dfs(candidates, target, [], 0)


        return self.ans

    def dfs(self, candidates, target, path, lv):
        lv += 1
        if target == 0:
            self.ans.append(path[:])
            return

        for i, c in enumerate(candidates):
            if c > target:
                return
            path.append(c)
            remain = target - c

            self.dfs(candidates[i:], remain, path, lv)
            # self.dfs(candidates, remain, path, lv)
            path.pop()

C = [2, 3, 6, 7]

T = 7
a = Solution().combinationSum(C, T)
print("ans:", a)
