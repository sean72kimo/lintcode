import math
class Solution:
    # @param nums: The integer array
    # @param target: Target number to find
    # @return the first position of target in nums, position start from 0 

    def lastPosition(self, nums, target):
        if not nums or not target:
            return -1
            
        start = 0
        end = len(nums) - 1 
        while start + 1 < end:
            mid = math.floor(start + (end - start) / 2)
            print ('start:', start, ', end:', end, ', mid', mid)
            if nums[mid] < target:
                start = mid
            elif nums[mid] > target:
                end = mid
            else:
                start = mid
        
        
        if nums[start] == target:
            return start
        elif nums[end] == target:
            return end
        else:
            return -1
        
        return -1


test = [[3,3,3,4,5,8,8,8,8], 8]
nums = test[0]
target = test[1]
sol = Solution()

last = sol.lastPosition(nums, target)
print(last)


