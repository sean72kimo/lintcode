class Solution:
    """
    @param A: an array
    @param n: an integer
    @return: makes the smallest absolute value of the difference between any two elements to largest
    """
    def maximumAbsolutValue(self, A, n):
        
        # Write your code here
        if not A:
            return 0
        A.sort()
        diff = []
        for i in range(1, len(A)):
            diff.append(A[i] - A[i-1])
            
        
        return max(diff)
A = [1,2,8,4,9,3]
n = 3
A = [1,2,8,4,9,3]
n = 2
a = Solution().maximumAbsolutValue(A, n)
print("ans:", a)