class Solution:
    # @param A and B: sorted integer array A and B.
    # @return: A new sorted integer array
    def mergeSortedArray(self, A, B):
        if A is None and B is None:
            return None

        if A is None:
            return B

        if B is None:
            return A

        result = [None] * (len(A) + len(B))
        i, j, idx = 0, 0, 0

        while i < len(A) and j < len(B):
            if A[i] < B[j]:
                result[idx] = A[i]
                idx += 1
                i += 1
            else:
                result[idx] = B[j]
                j += 1

        while i < len(A):
            result[idx] = A[i]
            idx += 1
            i += 1

        while j < len(B):
            result[idx] = B[j]
            idx += 1
            j += 1

        return result
