"""
Definition of ListNode
"""
class ListNode(object):

    def __init__(self, val, label, next=None):
        self.val = val
        self.label = label
        self.next = next
    def __str__(self):
        return "Node" + str(self.label)

class Solution:
    """
    @param head: The first node of the linked list.
    @return: You should return the head of the sorted linked list,
                  using constant space complexity.
    """
    def sortList(self, head):

        if head is None or head.next is None:
            return head

        mid = self.findMiddle(head)
        print(head, mid)

        right = self.sortList(mid.next)
        mid.next = None
        left = self.sortList(head)

        return self.merge(left, right)



    def findMiddle(self, head):
        fast = head.next
        slow = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        return slow

    def merge(self, head1, head2):
        dummy = ListNode(0,0)
        prev = dummy
        while head1 and head2:
            if head1.val < head2.val:
                prev.next = head1
                head1 = head1.next
            else:
                prev.next = head2
                head2 = head2.next
            prev = prev.next

        if head1:
            prev.next = head1
        else:
            prev.next = head2

        return dummy.next


def printNodeList(node):
    while node != None:
        print(node.val, end='->')
        node = node.next
    print('')

node1 = ListNode(1,1)
node2 = ListNode(-1,2)
node3 = ListNode(2,3)
node4 = ListNode(6,4)
node5 = ListNode(5,5)
node6 = ListNode(3,6)

node1.next = node2
node2.next = node3
node3.next = node4
node4.next = node5
node5.next = node6
node6.next = None

printNodeList(node1)
sol = Solution()
ans = sol.sortList(node1)
printNodeList(ans)



