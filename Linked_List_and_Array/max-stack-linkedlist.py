from django.contrib.formtools import preview
class Node:
    def __init__(self, val):
        self.val = val
        self.next = None



class MaxStack:
    def __init__(self):
        self.dummy = Node(0)
        self.curr = self.dummy        
        self.max = []      #O(n) 
        self.mp = {}
        #self.stack = [1,3,2]

    def push(self, val): #O(1)
        newNode = Node(val)
        self.mp[newNode.val] = self.curr
        self.curr.next = newNode
        self.curr = self.curr.next
        
        if len(self.max) == 0 or val > self.max[-1]:
            self.max.append(val)
        else:
            self.max.append(self.max[-1])
    
    def pop(self):
        if self.curr.val == 0:
            return "empty"
        val = self.curr.val
        prev = self.mp[self.curr.val]
        self.curr = prev
        prev.next = None
        self.max.pop()
        return val
    
    def peek(self): #O(1)
        if self.curr is None:
            return "empty"
        return self.curr.val

    def get_max(self): #O(1)
        if len(self.max) == 0:
            return "empty"
        return self.max[-1]
        
        
ms = MaxStack()
ms.push(1) 
ms.push(3)
ms.push(2) 
# 1->3->2
print(ms.get_max())  # -> 3
ms.push(5)
# 1->3->2->5
print(ms.get_max())  # -> 5
# 1->3->2
print(ms.pop()) # -> 5

print(ms.get_max())
print(ms.peek())  # -> 2
print(ms.pop())  # -> 2
print(ms.pop())  # -> 3
print(ms.get_max())  # -> 1

print(ms.pop())
print(ms.pop())