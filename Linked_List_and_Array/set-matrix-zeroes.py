# time O(m*n)
# space O(1)s
class Solution:
    def setZeroes(self, matrix):
        """
        Do not return anything, modify matrix in-place instead.
        """
        first_col_zero = False
        first_row_zero = False
        m = len(matrix)
        n = len(matrix[0])
        # https://www.youtube.com/watch?v=-I8w2_sN93c
        # 第一行或是第一列的0, 有可能是來自於 (i, j) > (1, 1)開始的右下角正方形，
        # 而非是原本的0
        # 這樣子會誤判，而將第一行（或者第一列）設為全0
        # 所以用first_col_zero 和 first_row_zero記錄一下，第一行或是第一列是否要全為0
        for i in range(len(matrix)):
            if matrix[i][0] == 0:
                first_col_zero = True

        for j in range(len(matrix[0])):
            if matrix[0][j] == 0:
                first_row_zero = True
        # 用第一行第一列當作標記，看看之後是否要將此行或是此列設為0
        # first col and first row is used as indicator
        for i in range(m):
            for j in range(n):
                if matrix[i][j] == 0:
                    matrix[i][0] = 0
                    matrix[0][j] = 0

        # check first row and first col indication
        for i in range(1, m):
            for j in range(1, n):
                if matrix[i][0] == 0 or matrix[0][j] == 0:
                    matrix[i][j] = 0

        if first_row_zero:
            for j in range(len(matrix[0])):
                matrix[0][j] = 0

        if first_col_zero:
            for i in range(len(matrix)):
                matrix[i][0] = 0

matrix = [[0, 1, 2, 0], [3, 4, 5, 2], [1, 3, 1, 5]]
# matrix = [[1,1,1],[1,0,1],[1,1,1]]
# matrix = [[1,1,1],[0,1,2]]
Solution().setZeroes(matrix)

print("ans:")
for r in matrix:
    print(r)

class Solution(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: None Do not return anything, modify matrix in-place instead.
        """
        first_col = False
        m = len(matrix)
        n = len(matrix[0])

        # first col and first row is used as indicator
        for i in range(m):
            if matrix[i][0] == 0:
                first_col = True
            for j in range(n):
                if matrix[i][j] == 0:
                    matrix[i][0] = 0
                    matrix[0][j] = 0
        # 用第一行第一列當作
        # check first row and first col indication
        for i in range(1, m):
            for j in range(1, n):
                if not matrix[i][0] or not matrix[0][j]:
                    matrix[i][j] = 0

        if matrix[0][0] == 0:  # check first row
            for j in range(n):
                matrix[0][j] = 0

        if first_col:  # check first col
            for i in range(m):
                matrix[i][0] = 0





# time: O(m*n*(m+n))
# spce: O(1)
class Solution2(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        if len(matrix) == 0:
            return

        m = len(matrix)
        n = len(matrix[0])

        for i in range(m):
            for j in range(n):
                if matrix[i][j] == 0:
                    self.set_row(matrix, i, j)
                    self.set_col(matrix, i, j)

        for i in range(m):
            for j in range(n):
                if matrix[i][j] == '0':
                    matrix[i][j] = 0


    def set_row(self, matrix, i, j):
        row = matrix[i]
        for j in range(len(row)):
            if matrix[i][j] == 0:
                continue

            matrix[i][j] = '0'


    def set_col(self, matrix, i, j):
        m = len(matrix)
        for i in range(m):
            if matrix[i][j] == 0:
                continue

            matrix[i][j] = '0'

matrix = [[0, 1, 1], [0, 1, 1], [1, 1, 1]]
matrix = [[1, 0, 3]]
matrix = [[-1], [2], [3]]
matrix = [[0, 1, 2, 0], [3, 4, 5, 2], [1, 3, 1, 5]]
# Solution2().setZeroes(matrix)
#
# print("ans:")
# for r in matrix:
#     print(r)
