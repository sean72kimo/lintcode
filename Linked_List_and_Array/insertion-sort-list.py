# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def insertionSortList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return

        dummy = ListNode(0)
        dummy.next = head

        while head and head.next:
            if head.val <= head.next.val:
                head = head.next

            else:
                # at this point, head is at 6, since it found that  6 > 3

                curr = dummy
                # find where to insert.
                # dummy--> 1 --> 2 --> 4 --> 6 --> 3 --> 7
                # after leaving while loop, curr will be at 2, and curr.next is going to connect to 3
                while curr.next.val < head.next.val:
                    curr = curr.next

                n0 = curr.next  # 4
                n1 = head.next  # 3
                n2 = head.next.next  # 7

                curr.next = n1
                n1.next = n0
                head.next = n2

        return dummy.next




def stringToIntegerList(input):
    input = input.strip()
    input = input[1:-1]
    if not input:
        return []
    return [int(number) for number in input.split(",")]

def stringToListNode(input):
    # Generate list from the input
    numbers = stringToIntegerList(input)

    # Now convert that list into linked list
    dummyRoot = ListNode(0)
    ptr = dummyRoot
    for number in numbers:
        ptr.next = ListNode(number)
        ptr = ptr.next

    ptr = dummyRoot.next
    return ptr

def listNodeToString(node):
    if not node:
        return "[]"

    result = ""
    while node:
        result += str(node.val) + ", "
        node = node.next
    return "[" + result[:-2] + "]"

def main():
    import sys
    def readlines():
        myinput = ["[1,2,4,6,3,7]"]
        for line in myinput:
            yield line.strip('\n')
    lines = readlines()
    while True:
        try:
            line = next(lines)
            head = stringToListNode(line)

            ret = Solution().insertionSortList(head)

            out = listNodeToString(ret)
            print (out)
        except StopIteration:
            break

if __name__ == '__main__':
    main()
