from lib.linkedlist import list2LinkedList, printLinkedList, ListNode
class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(0)
        dummy.next = head
        head = dummy

        while head and head.next and head.next.next:
            if head.next.val == head.next.next.val:
                val = head.next.val

                while head.next and head.next.val == val:
                    head.next = head.next.next

            else:
                head = head.next

        return dummy.next

linkedlist = [1, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5]
linkedlist = [1]
head = list2LinkedList(linkedlist)
root = Solution().deleteDuplicates(head)
print("ans: ", end = '')
printLinkedList(root)
