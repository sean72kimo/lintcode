class Solution:
    """
    @param codeList: The codeList
    @param shoppingCart: The shoppingCart
    @return: The answer
    """
    def buyFruits(self, codeList, shoppingCart):
        # Write your code here
        lst = []

        for i in codeList:
            for j in i:
                lst.append(j)

        if len(lst) > len(shoppingCart):
            return 0

        start = 0
        while True:
            end = start + len(lst)
            if end > len(shoppingCart):
                break
            cart = shoppingCart[start:start + len(lst)]

            idx = 0
            while idx < len(cart):
                for itm in lst:
                    if itm == 'anything':
                        idx += 1
                    elif itm == cart[idx]:
                        idx += 1
                    else:
                        break

                if idx == len(cart):
                    return 1
                else:
                    break
            start += 1
        return 0






codeList = [["apple", "apple"], ["orange", "banana", "orange"]]
codeList = [["orange", "banana", "orange"], ["apple", "apple"]]
codeList = [["apple", "apple"], ["orange", "anything", "orange"]]
shoppingCart = ["orange", "apple", "apple", "orange", "banana", "orange"]
a = Solution().buyFruits(codeList, shoppingCart)
print("ans:", a)
