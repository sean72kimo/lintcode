class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if not any(matrix):
            return []
        m = len(matrix)
        n = len(matrix[0])

        ans = []
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        d = 0
        vst = set()
        i, j = 0 , 0

        while True:
            while 0 <= i < m and 0 <= j < n and (i, j) not in vst:
                v = matrix[i][j]
                ans.append(v)
                vst.add((i, j))
                prev = (i, j)
                i = i + dxy[d][0]
                j = j + dxy[d][1]
                if len(ans) == m * n:
                    return ans
            i = prev[0]
            j = prev[1]
            ans.pop()
            vst.remove((i, j))
            d += 1
            d = d % 4



matrix = [
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
matrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12]
]

a = Solution().spiralOrder(matrix)

print("ans:", a)
