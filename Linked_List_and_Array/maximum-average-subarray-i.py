from sys import maxsize
class Solution(object):
    def findMaxAverage(self, A, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: float
        """
        P = [0]
        for x in A:
            P.append(P[-1] + x)
        print(P)

A = [1, 12, -5, -6, 50, 3]
k = 4
a = Solution().findMaxAverage(A, k)
print("ans:", a)


from sys import maxsize
class Solution2(object):
    def findMaxAverage(self, A, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: float
        """
        summ = sum(A[:k])
        ns = summ
        for i in range(k, len(A)):
            ns = ns + A[i] - A[i - k]
            summ = max(ns, summ)
        return float(summ) / k

A = [1, 12, -5, -6, 50, 3]
k = 4
# a = Solution().findMaxAverage(A, k)
# print("ans:", a)



from sys import maxsize
class Solution3(object):
    def findMaxAverage(self, A, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: float
        """
        n = len(A)
        pre = [None] * (n + 1)
        pre[0] = 0
        ans = -maxsize

        for i in range(1, n + 1):
            pre[i] = pre[i - 1] + A[i - 1]

            if i >= k:
                avg = (pre[i] - pre[i - k]) / k
                ans = max(avg, ans)
        print(pre)
        return ans

A = [1, 12, -5, -6, 50, 3]
k = 4
a = Solution3().findMaxAverage(A, k)
# print("ans:", a)
