class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        if nums is None or len(nums) == 0 or k == 0:
            return False

        hs = {}
        for idx, val in enumerate(nums):

            if val in hs:
                j = hs[val]

                if abs(idx - j) <= k:
                    return True
                else:
                    hs[val] = idx

            else:
                hs[val] = idx

        return False

nums = [1, 0, 1, 1]
k = 1
print('ans:', Solution().containsNearbyDuplicate(nums, k))
