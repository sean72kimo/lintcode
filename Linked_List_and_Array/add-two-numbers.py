class Solution(object):
    def get_nums(self, head):
        n = 0
        d = 1
        while head:
            n = n + head.val * d
            head = head.next
            d = d * 10
        return n

    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        1. 注意corner case n1 + n2 = 0
        """
        if not l1 and not l2:
            return
        dummy = ListNode(0)
        curr = dummy
        carry = 0

        n1 = self.get_nums(l1)
        n2 = self.get_nums(l2)


        dummy = ListNode(0)
        curr = dummy
        n = n1 + n2

        # corner case
        if n == 0:
            return ListNode(0)

        while n:
            v = n % 10
            node = ListNode(v)
            curr.next = node
            curr = curr.next
            n = n // 10

        return dummy.next



class Solution2(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        1. 如果l1 or l2沒了怎麼辦? 補0 ex: ex:[2] + [5,6,8]
        2. 記得移動l1 or l2
        3. 如果結束後還有carry，記得補上最後一個node ex:[2,4,3] + [5,6,8]

        """
        if not l1 and not l2:
            return
        dummy = ListNode(0)
        curr = dummy
        carry = 0
        while l1 or l2:
            if l1:
                v1 = l1.val
                l1 = l1.next
            else:
                v1 = 0

            if l2:
                v2 = l2.val
                l2 = l2.next
            else:
                v2 = 0

            v = v1 + v2 + carry
            carry = v // 10

            node = ListNode(v % 10)
            curr.next = node
            curr = curr.next

        if carry:
            node = ListNode(carry)
            curr.next = node
        return dummy.next
