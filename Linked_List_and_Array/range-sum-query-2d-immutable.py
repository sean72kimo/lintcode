"""
similar to 1314. Matrix Block Sum
"""
class NumMatrix(object):

    def __init__(self, matrix):
        """
        :type matrix: List[List[int]]
        """
        if len(matrix) == 0 or len(matrix[0]) == 0:
            return

        n = len(matrix)
        m = len(matrix[0])

        self.dp = [[0] * (m + 1) for _ in range(n + 1)]
        for r in range(n):
            for c in range(m):
                self.dp[r + 1][c + 1] = self.dp[r + 1][c] + self.dp[r][c + 1] + matrix[r][c] - self.dp[r][c]

        for row in self.dp:
            print(row)

    def sumRegion(self, row1, col1, row2, col2):
        """
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        return self.dp[row2 + 1][col2 + 1] - self.dp[row1][col2 + 1] - self.dp[row2 + 1][col1] + self.dp[row1][col1]



matrix = [
  [3, 0, 1, 4, 2],
  [5, 6, 3, 2, 1],
  [1, 2, 0, 1, 5],
  [4, 1, 0, 1, 7],
  [1, 0, 3, 0, 5]
]


a = NumMatrix(matrix).sumRegion(2, 1, 4, 3)
print("ans:", a)

class NumMatrix2(object):

    def __init__(self, matrix):
        """
        :type matrix: List[List[int]]
        """
        self.matrix = matrix

        for row in self.matrix:
            for c in range(1, len(row)):
                row[c] = row[c] + row[c - 1]



    def sumRegion(self, row1, col1, row2, col2):
        """
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        val = 0
        for row in range(row1, row2 + 1):
            if col1 > 0:
                row_val = self.matrix[row][col2] - self.matrix[row][col1 - 1]
            else:
                row_val = self.matrix[row][col2]
            val += row_val

        return val

