from typing import List


class Solution:
    def rotateTheBox(self, box: List[List[str]]) -> List[List[str]]:
        m = len(box)
        n = len(box[0])

        for i in range(m):
            k = n - 1
            for j in range(n - 1, -1, -1):
                if box[i][j] == '*':
                    k = j - 1
                elif box[i][j] == '#':
                    box[i][j], box[i][k] = box[i][k], box[i][j]
                    k -= 1

        ans = [[None for _ in range(m)] for _ in range(n)]

        for i in range(m):
            for j in range(n):
                ans[j][i] = box[i][j]

        for i in range(n):
            for j in range(m//2):
                ans[i][j], ans[i][m-j-1] = ans[i][m-j-1], ans[i][j]

        return ans

box = [["#",".","#"]]
box = [["#",".","*","."], ["#","#","*","."]]
box = [["#","#","*",".","*","."],["#","#","#","*",".","."],["#","#","#",".","#","."]]
print("===== input:")
for r in box:
    print(r)


a = Solution().rotateTheBox(box)
print("====== ans: ")
for r in a:
    print(r)


# box = [
#     [1,2,3],
#     [4,5,6]
# ]
# m = len(box)
# n = len(box[0])
#
# ans = [[0 for _ in range(m)] for _ in range(n)]
#
# for i in range(m):
#     for j in range(n):
#         ans[j][i] = box[i][j]
#
# for i in range(n):
#     for j in range(m//2):
#         ans[i][j], ans[i][m-j-1] = ans[i][m-j-1], ans[i][j]
#
# for r in ans:
#     print(r)
a = "\t\taaa"
print(a.lstrip("\t"))