class Solution(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        ans = []
        var = {}
        for i, v in enumerate(equations):
            if v[0] not in var:
                var[v[0]] = [str(values[i]) + v[1]]
            else:
                var[v[0]].append(str(values[i]) + v[1])

            if v[1] not in var:
                var[v[1]] = [str(values[i]) + v[0]]
            else:
                var[v[1]].append([str(values[i]) + v[1]])
        print(var)
        comb = zip(equations, values)

        for i, j in queries:
            if i not in var or j not in var:
                ans.append(-1.0)


    def sub(self, tar, ori, var, equations, values):
        for i in var[ori]:
            # equations[i][0] = float(values)*equations[i][1]
            if equations[i][0] != tar:
                self.sub(tar, equations[i][0], var, equations, values)
            else:
                return float(values) * equations[i][1]

equations = [ ["a", "b"], ["b", "c"] ]
values = [2.0, 3.0]
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]
Solution().calcEquation(equations, values, queries)

