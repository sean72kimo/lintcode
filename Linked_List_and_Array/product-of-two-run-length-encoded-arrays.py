from typing import List

class Solution:
    def findRLEArray(self, encoded1: List[List[int]], encoded2: List[List[int]]) -> List[List[int]]:
        i = 0
        j = 0
        ans = []

        while i < len(encoded1) and j < len(encoded2):
            v1, f1 = encoded1[i]
            v2, f2 = encoded2[j]

            value = v1 * v2
            freq = min(f1, f2)

            encoded1[i][1] -= freq
            encoded2[j][1] -= freq

            if encoded1[i][1] == 0:
                i += 1
            if encoded2[j][1] == 0:
                j += 1

            if not ans or ans[-1][0] != value:
                ans.append([value, freq])
            else:
                ans[-1][1] += freq

        return ans

encoded1 = [[1,3],[2,3]]
encoded2 = [[6,3],[3,3]]
a = Solution().findRLEArray(encoded1, encoded2)
print("ans:", a)