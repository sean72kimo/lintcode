class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        island = 0
        nei = 0
        
        if grid is None or len(grid) == 0:
            return 0

        m = len(grid)
        n = len(grid[0])
        p = 0
        for i in range(m):
            for j in range(n):    
                if grid[i][j] == 0:
                    continue
                
                if j == 0 or grid[i][j-1] == 0: #left edge
                    p+=1
                if i == 0 or grid[i-1][j] == 0: #upper edge
                    p+=1
                if j == n-1 or grid[i][j+1] == 0: #right edge
                    p+=1
                if i == m-1 or grid[i+1][j] == 0: #lower edge
                    p+=1
        
        return p

grid = [[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]]

a = Solution().islandPerimeter(grid)
print("ans:", a)
    