class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        if len(nums) == 0 or nums is None:
            return 0
        i = 0
        n = len(nums)
        tail = 0
        while i < n - 1:
            if nums[i] == val:
                nums[i] = nums[i + 1]
                tail = i
            i += 1
        print(tail)
        return tail


nums = [1, 2, 3, 1, 2, 2, 3]
val = 3

Solution().removeElement(nums, val)



class Solution2:
    """
    @param: A: A list of integers
    @param: elem: An integer
    @return: The new length after remove
    """
    def removeElement(self, A, elem):
        # write your code here
        if len(A) == 0:
            return A

        ans = []

        i = 0
        j = len(A) - 1



        while i < j:
            if A[i] != elem:
                i += 1
            else:
                A[i], A[j] = A[j], A[i]
                j -= 1

        if A[i] == elem:
            return i

        return i + 1

A = [0, 4, 4, 0, 0, 2, 4, 4]
elem = 4
a = Solution2().removeElement(A, elem)
print("ans:", a)
