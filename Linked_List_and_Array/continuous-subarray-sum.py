from sys import maxsize
class Solution:
    """
    @param: A: An integer array
    @return: A list of integers includes the index of the first number and the index of the last number
    """
    def continuousSubarraySum(self, A):
        # write your code here
        if not A or len(A) == 0 :
            return[]

        rsum = [0]
        
        minSum = 0
        summ = 0
        start = -1
        end = 0
        ans = [start, end]
        maxV = float('-inf')
        
        for i, n in enumerate(A):
            summ += n
            
            local = summ - minSum
            
            if local >= maxV:
                maxV = local
                end = i
            
            if summ < minSum and ans[1] != end:
                
                minSum = summ
                if i < start:
                    start = i
            
            ans = [start, end]
            
        return ans

A = [-3, 1, 3, -3, 4]
A = [-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-4,5,-1000]
a = Solution().continuousSubarraySum(A)
print("ans", a)
