# https://algorithm.yuanbin.me/zh-hans/linked_list/copy_list_with_random_pointer.html#题解2---哈希表一次遍历
# Definition for singly-linked list with a random pointer.
# class RandomListNode:
#     def __init__(self, x):
#         self.label = x
#         self.next = None
#         self.random = None
def printNodeList(node):
    while node != None:
        print(node.label, end='->')
        node = node.next
    print('')


class Solution:
    # @param head: A RandomListNode
    # @return: A RandomListNode

    # hashmap version
    def copyRandomList(self, head):
        if head is None:
            return None
        
        hs = {}
        dummy = RandomListNode(0)
        dummy.next = head
        
        create = head
        conn = head
        rnd = head
        
        while create:
            hs[create] = RandomListNode(create.label)
            create = create.next
        
        while conn:
            nxt = conn.next
            if nxt:
                hs[conn].next = hs[nxt]
                
            rnd = conn.random
            if rnd:
                hs[conn].random = hs[rnd]
            
            conn = conn.next

        
        return hs[head]



# Definition for singly-linked list.
class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None
        
    def __repr__(self):
        return "<N>{}".format(self.label)

node1 = RandomListNode(1)
node2 = RandomListNode(2)
node3 = RandomListNode(3)
node4 = RandomListNode(4)
node5 = RandomListNode(5)


node1.next = node2
node2.next = node3
node3.next = node4
node4.next = node5

printNodeList(node1)

Solution().copyRandomList(node1)