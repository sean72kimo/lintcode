class Solution2(object):
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        num = n * n
        f = [[None for _ in range(n)] for _ in range(n)]
        cnt = 1
        i = j = 0
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        d = 0

        while True:
            while 0 <= i < n and 0 <= j < n and f[i][j] is None:
                # if f[i][j] is not None:
                #     break
                f[i][j] = cnt
                if cnt == num:
                    return f

                print(cnt, i, j, d)
                prev = [i, j]
                i = i + dxy[d][0]
                j = j + dxy[d][1]
                cnt += 1

            d = (d + 1) % 4
            i = prev[0]
            j = prev[1]
            cnt -= 1
            f[i][j] = None


a = Solution2().generateMatrix(3)
print("ans:")
for row in a:
    print(row)

class Solution(object):
    def generateMatrix(self, num):
        """
        :type n: int
        :rtype: List[List[int]]
        """

        if num == 0:
            return [[]]

        if num == 1:
            return [[1]]

        n = pow(num, 2)

        ans = [[None for _ in range(num)] for _ in range(num)]

        i = 0
        j = 0
        v = 1
        size = num - 1

        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0]]
        while v < n:
            # right:
            for direction, (dx, dy) in enumerate(dxy):
                for _ in range(size):
                    print([i, j], v, size)
                    ans[i][j] = v
                    v += 1

                    if not ans[i + dx][j + dy]:  # not visited
                        i = i + dx
                        j = j + dy
                    else:  # spiral inward
                        ni, nj = dxy[(direction + 1) % 4]
                        i = i + ni
                        j = j + nj

            size -= 2


        if num % 2:
            ans[i][j] = v

        return ans


#
# a = Solution().generateMatrix(5)
# print("ans:")
# for row in a:
#     print(row)

