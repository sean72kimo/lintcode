"""
similar to 304. Range Sum Query 2D - Immutable
"""
from typing import List


class Solution:
    def matrixBlockSum(self, mat: List[List[int]], k: int) -> List[List[int]]:
        n = len(mat)
        m = len(mat[0])
        dp = [[0 for _ in range(m + 1)] for _ in range(n + 1)]

        for r in range(n):
            for c in range(m):
                dp[r + 1][c + 1] = dp[r + 1][c] + dp[r][c + 1] + mat[r][c] - dp[r][c]

        ans = [[0 for _ in range(m)] for _ in range(n)]
        for i in range(n):
            for j in range(m):
                r1 = max(0, i - k)
                r2 = min(n - 1, i + k)

                c1 = max(0, j - k)
                c2 = min(m - 1, j + k)

                ans[i][j] = self.sumRegion(dp, r1, c1, r2, c2)

        return ans

    def sumRegion(self, dp, row1, col1, row2, col2) -> int:
        a = row2 + 1
        b = col2 + 1

        return dp[a][b] - dp[a][col1] - dp[row1][b] + dp[row1][col1]

