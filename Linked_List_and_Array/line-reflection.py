class Solution(object):
    def isReflected(self, points):
        """
        :type points: List[List[int]]
        :rtype: bool
        """
        if len(points) == 0:
            return True
        myset = set()
        for x, y in points:
            p = (x * 1.0, y)
            myset.add(tuple(p))

        max_x = max([x for x, _ in points])
        min_x = min([x for x, _ in points])
        mid = (max_x + min_x) / 2.0

        for x, y in points:
            delta = mid - x
            xx = mid + delta
            mirror = tuple((xx * 1.0, y))

            if mirror not in myset:
                return False

        return True

points = [[1,1],[-1,1]]
points = [[0,0],[0,0]]
points = [[1,1],[-1,1],[3,1]]
points = [[0,0],[1,0]]
a = Solution().isReflected(points)
print("ans:", a)
