import collections
class Solution(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """

        # write your code here
        if nums1 is None or nums2 is None:
            return []

        hs = {}
        for n in nums1:
            if n not in hs:
                hs[n] = 1
            else:
                hs[n] += 1

        print (hs)
        ans = []
        counts = collections.Counter(nums1)

        for n in nums2:
            if n in hs and hs[n] > 0:
                ans.append(n)
                hs[n] -= 1
        return ans


class Solution_sort(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        nums1.sort()
        nums2.sort()

        print(nums1)
        print(nums2)

        i = 0
        j = 0
        ans = []
        while i < len(nums1) and j < len(nums2):
            v1 = nums1[i]
            v2 = nums2[j]

            if v1 == v2:
                ans.append(v1)
                i += 1
                j += 1

            else:

                if nums1[i] < nums2[j]:
                    i += 1
                else:
                    j += 1

        return ans
nums1 = [1, 2, 2, 1]
nums2 = [2, 2]

nums1, nums2 = [4,9,5], [9,4,9,8,4]
print('ans', Solution_sort().intersect(nums1, nums2))
