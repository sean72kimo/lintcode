class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """

        rowIndex += 1
        if rowIndex == 0:
            return []

        ans = [1]
        
        for i in range(rowIndex):

            tmp = [1 for i in range(i+1)]

            for j in range(1, i):
                tmp[j] = ans[j] + ans[j-1]
            
            ans = tmp


        return ans

# rowIndex = 1
# a = Solution().getRow(rowIndex)
# print(a)


class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """

        val = 1
        ans = [1]

        for i in range(rowIndex,  0, -1):
            val = val * i
            d = rowIndex - i + 1
            val = val // d
            ans.append(val)

        return ans


rowIndex = 4
a = Solution().getRow(rowIndex)
print(a)