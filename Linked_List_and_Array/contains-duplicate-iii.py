import collections
from heapq import heappush, heappop
from collections import OrderedDict

def toTreeMap(myMap):
    key = myMap.keys()
    heap = []
    for i in key:
        heappush(heap, i)

    sort = []
    while heap:
        sort.append(heappop(heap))

    sortedMap = OrderedDict()
    for k in sort:
        sortedMap[k] = myMap[k]

    return sortedMap

class Solution:
    # @param {integer[]} nums
    # @param {integer} k
    # @param {integer} t
    # @return {boolean}
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        if k < 0 or t < 0:
            return False

        window = collections.OrderedDict()
        for n in nums:
            # Make sure window size
            if len(window) > k:
                window.popitem(False)

            bucket = n if not t else n // t
            # At most 2t items.
            for m in (window.get(bucket - 1), window.get(bucket), window.get(bucket + 1)):
                if m is not None and abs(n - m) <= t:
                    return True
            window[bucket] = n
        return False

