from sys import maxsize
class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number 
             and the index of the last number
    """
    def subarraySumClosest(self, nums):
        if nums is None or len(nums) == 0:
            return None
        if len(nums) == 1:
            reutnr [0,0]
        sums = [0] * (len(nums))
#         sums[0] = nums[0]
        for i in range(len(nums)):
            sums[i] = sums[i-1] + nums[i]

        
        print(sums)
        
        sortedSums = list(sums)
        sortedSums.sort()
        
        print(sortedSums)
        diff = maxsize

        for i in range(1,len(sortedSums)):

            if sortedSums[i] - sortedSums[i-1] < diff:
                diff = sortedSums[i] - sortedSums[i-1]
                num1 = sortedSums[i-1]
                num2 = sortedSums[i]
        
        index1 = sums.index(num1)
        index2 = sums.index(num2)
        if index1 > index2:
            return [index2 + 1, index1]
        else:
            return [index1 + 1, index2]
        
    import collections
    def subarraySumClosest2(self, nums):
        if nums is None or len(nums) == 0:
            return None
        if len(nums) == 1:
            return [0,0]
        # {key:val} = {pre_sum : index of nums array}
        prefixSum = {0 : 0}
        sum = 0
        minDiff = maxsize
        for i in range(len(nums)):
            sum += nums[i]
            prefixSum[sum] = i

        sortedSums = sorted(prefixSum)
        print(sortedSums)

        for i in range(0, len(sortedSums)):
            diff = sortedSums[i] - sortedSums[i-1]
            if diff < minDiff:
                minDiff = diff
                sum1 = sortedSums[i]
                sum2 = sortedSums[i-1]
        idx1 = prefixSum[sum1]
        idx2 = prefixSum[sum2]
        
        if idx1 > idx2:
            return [idx2+1, idx1]
        
        return [idx1+1,idx2] 

        

nums = [-3, 1, 1, -3, 5]
# nums = [5,10,5,3,2,1,1,-2,-4,3]
sol = Solution()
ans = sol.subarraySumClosest(nums)
print('[ans]',ans)

# ans = sol.subarraySumClosest2(nums)
# print('[ans]',ans)