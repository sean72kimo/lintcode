
class Solution:
    """
    @param: nums: an array with positive and negative numbers
    @param: k: an integer
    @return: the maximum average
    """

    def maxAverage(self, nums, k):
        if not nums:
            return 0

        start, end = min(nums), max(nums)
        while end - start > 1e-5:
            mid = (start + end) / 2
            if self.check_subarray(nums, k, mid):
                start = mid
            else:
                end = mid

        return start

    def check_subarray(self, nums, k, avg):
        summ = 0
        prevSum = 0
        prevMin = 0
        for i in range(len(nums)):
            summ = summ + nums[i] - avg

            if i + 1 >= k and summ >= 0:
                return True

            if i >= k:
                prevSum = prevSum + nums[i - k] - avg
                prevMin = min(prevMin, prevSum)
                if summ - prevMin >= 0:
                    return True
        return False


A = [1, 12, -5, -6, 50, 3]
k = 4
a = Solution().maxAverage(A, k)
print("ans:", a)



class Solution(object):
    def findMaxAverage(self, A, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: float
        """
        P = [0]
        for x in A:
            P.append(P[-1] + x)



        def possible(x):
            avg = P[0]

            for i in range(len(P)):
                presum = P[i]
                print(P[i])
                avg = min(avg, presum - x * i)

                if i + k >= len(P):
                    break

                if P[i + k] - (i + k) * x > avg:
                    return True
            return False


        def possible2(x):
            for i in range(k, len(P)):
                if P[i] / i > x:
                    return True
            return False

        start = min(A)
        end = max(A)
        esp = 1e-6

        while end - start > esp:
            mid = (start + end) / 2.0

            if possible2(mid):
                start = mid
            else:
                end = mid

        return start


A = [1, 12, -5, -6, 50, 3]
k = 4
a = Solution().findMaxAverage(A, k)
print("ans:", a)



