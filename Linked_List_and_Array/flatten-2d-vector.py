class Vector2D(object):

    def __init__(self, vec2d):
        """
        :type v: List[List[int]]
        """
        self.grid = vec2d
        self.row = 0
        self.col = -1
        self.num = None

    def next(self):
        """
        :rtype: int
        """

        if self.num is None:
            self.hasNext()

        num = self.num
        self.num = None
        return num

    def hasNext(self):
        """
        :rtype: bool
        """
        if len(self.grid) == 0 or self.row == len(self.grid):
            return False
        if self.num:
            return True

        self.col += 1
        print(self.row)
        if self.col >= len(self.grid[self.row]):
            self.row += 1
            self.col = 0

        while self.row < len(self.grid) and len(self.grid[self.row]) == 0:
            self.row += 1
            self.col = 0

        if self.row == len(self.grid):
            return False

        self.num = self.grid[self.row][self.col]
        return True


# Your Vector2D object will be instantiated and called as such:
# obj = Vector2D(v)
# param_1 = obj.next()
# param_2 = obj.hasNext()

# Your Vector2D object will be instantiated and called as such:
i, v = Vector2D2(vec2d), []
while i.hasNext():
    val = i.next()
    print(val)
    v.append(val)
print("ans:", v)


