"""
similar to 1647. Minimum Deletions to Make Character Frequencies Unique
目標：使這個字串成為左半都為0右半都為1
遍歷整個prefix長度，每i個，要花多少代價才能得到目標，取代價最小
"""
class Solution:
    def minFlipsMonoIncr(self, s: str) -> int:
        p = [0]

        for x in s:
            p.append(p[-1] + int(x))
        n = len(s)

        ans = n
        for i in range(len(p)):
            ones = p[n] - p[i]
            zeros = n - i - ones
            ans = min(ans, p[i] + zeros)

        return ans