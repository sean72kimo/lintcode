import collections


class Solution(object):
    def findPairs(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if k < 0:
            return 0
        myset = collections.Counter(nums)
        ans = set()
        res = 0
        for n in myset:
            """
            因為求的是 unique pairs, 所以(3,1) (1,3)只算是一組，所以我們取 x = n + k得到(1,3)即可
            若是k == 0, 注意需要有 myset[n] > 1 例如(1,1) 兩個1 才能構成一個pair
            """
            # x = n - k
            # if x in myset:
            #     if x == n:
            #         if myset[n] > 1:
            #             ans.add((x, n))
            #     else:
            #         ans.add((x, n))

            x = n + k
            if x in myset:
                if x == n:
                    if myset[n] > 1:
                        ans.add((n, x))
                else:
                    ans.add((n, x))

        return len(ans)
