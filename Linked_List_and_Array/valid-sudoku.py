class Solution(object):
    def isValidSudoku(self, board):
        row = [set() for i in range(9)]
        col = [set() for i in range(9)]
        grid = [set() for i in range(9)]


        for r in range(9):
            for c in range(9):
                if board[r][c] == '.':
                    continue
                if board[r][c] in row[r]:
                    return False
                if board[r][c] in col[c]:
                    return False

                a = int(r / 3) * 3
                b = int(c / 3)
                g = a + b

                if board[r][c] in grid[g]:
                    return False

                row[r].add(board[r][c])
                col[c].add(board[r][c])
                grid[g].add(board[r][c])


        return True


class Solution2(object):
    def isValidSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: bool
        """
        rows = [set() for _ in range(9)]
        cols = [set() for _ in range(9)]
        grid = [set() for _ in range(9)]
        print("---")
        print(rows)

        for i in range(9):
            for j in range(9):
                if board[i][j] == '.':
                    continue

                v = board[i][j]

                if v in rows[i]:
                    return False

                if v in cols[j]:
                    return False

                g = self.gid(i, j)
                if v in grid[g]:
                    return False

                rows[i].add(v)
                cols[j].add(v)
                grid[g].add(v)

        return True

    def gid(self, ii, jj):
        i = ii // 3
        j = jj // 3
        g = i * 3 + j

        return g

board = [[".", "8", "7", "6", "5", "4", "3", "2", "1"], ["2", ".", ".", ".", ".", ".", ".", ".", "."], ["3", ".", ".", ".", ".", ".", ".", ".", "."], ["4", ".", ".", ".", ".", ".", ".", ".", "."], ["5", ".", ".", ".", ".", ".", ".", ".", "."], ["6", ".", ".", ".", ".", ".", ".", ".", "."], ["7", ".", ".", ".", ".", ".", ".", ".", "."], ["8", ".", ".", ".", ".", ".", ".", ".", "."], ["9", ".", ".", ".", ".", ".", ".", ".", "."]]
board = [
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
a = Solution2().isValidSudoku(board)
print(a)
