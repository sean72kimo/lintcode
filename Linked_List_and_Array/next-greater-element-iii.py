class Solution:
    def nextGreaterElement(self, num: int) -> int:
        nums = list(str(num))
        n = len(nums)
        MAX = 2 ** 31 - 1
        for i in range(n-1, -1, -1):
            if i-1 >= 0 and nums[i-1] < nums[i]:
                break

        if i == 0:
            return -1

        p = i - 1

        for i in range(n-1, p, -1):
            if nums[i] > nums[p]:
                nums[i], nums[p] = nums[p], nums[i]
                break
        nums[p+1:] = nums[p+1:][::-1]

        ans = int(''.join(nums))
        if ans > MAX:
            return -1
        return ans

lst = [
    [230241, 230412]
]

for item in lst:
    print("test case:", item)
    a = Solution().nextGreaterElement(item[0])
    print("ans: ", a)
    if a != item[1]:
        print("== err, expected:", item[1])
        break