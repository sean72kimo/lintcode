"""
Definition of ListNode
"""
from lib.linkedlist import list2LinkedList, printLinkedList
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next


class Solution:
    """
    @param head: The head of linked list.
    @return: nothing
    """
    def reorderList(self, head):
        # write your code here
        if head is None:
            return head
            
        
        dummy = ListNode(0)
        dummy.next = head
        
        
        slow = head
        fast = head.next
        
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            
        prev = None
        curr = slow.next
        slow.next = None
        
        while curr:
            temp = curr.next
            curr.next = prev
            prev = curr
            curr = temp
            
        
        p2 = prev
        p1 = head
        help = dummy
        cnt = 0
        while p1 and p2:
            if cnt % 2 == 0:
                help.next = p1
                p1 = p1.next
                help = help.next
            else:
                help.next = p2
                p2 = p2.next
                help = help.next
            cnt += 1
        if p1:
            help.next = p1
        if p2:
            help.next = p2

        return dummy.next
    

data = [1,2,3,4]
head = list2LinkedList(data)

node = Solution().reorderList(head)
printLinkedList(node)

