class Solution(object):
    # binary search
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int

        time O(nlogn)
        1. 題目說，如果nums長度為n，每一個num必定介於 1 ~ n-1，我們在 1 ~ n-1 之間猜測可能為dup num
        2. s = 1, e = n - 1，二分法取m，代表我們猜m為dup
        3. 掃描nums，數看看(cnt)有多少數字<=m，
        4. 如果 cnt <= m則代表 1~m之間沒有重複數字，s往右移動
        5. 如果 cnt > m則代表 1~m之間沒重複數字，e往左移動
        """
        if nums is None or len(nums) == 0:
            return -1
        start = 1
        end = len(nums) - 1

        while start + 1 < end:
            mid = int((start + end) / 2)
            cnt = self.check_smaller_num(mid, nums)
            print(cnt, mid)
            if cnt <= mid:
                start = mid
            else:
                end = mid

        if self.check_smaller_num(start, nums) <= start:
            return end
        else:
            return start



    def check_smaller_num(self, mid, nums):
        cnt = 0
        for i  in range(len(nums)):
            if nums[i] <= mid:
                cnt += 1
        return cnt

nums = [4, 4, 5, 3, 1, 2, 6]
nums = [1, 4, 4, 2, 4]
# nums = [1, 2, 3, 4, 4, 5]

a = Solution().findDuplicate(nums)
print(a)

