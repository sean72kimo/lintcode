class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number
             and the index of the last number
    """
    def subarraySum(self, nums):
        hs = {0:-1}
        sum = 0
        for i in range(len(nums)):
            sum += nums[i]
            if sum in hs:
                return [hs[sum] + 1, i]
            
            hs[sum] = i
            print(hs)
        return

    def subarraySum2(self, nums):
        hs = {0:-1}
        pre = [0 for i in range(len(nums) + 1)]
        for i in range(len(nums)):

            pre[i + 1] = pre[i] + nums[i]

            if pre[i + 1] in hs:
                return [hs[pre[i + 1]] + 1, i]

            hs[pre[i + 1]] = i

        return


nums = [-3, 1, 2, -3, 4]
# nums = [-1, 3, 2, -1, 3, -2, 4]
# nums = [1 , -1]
print("ans:{0}".format(Solution().subarraySum(nums)))
