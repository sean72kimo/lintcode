import collections
class Solution:
    def multiply(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """



        table = collections.defaultdict(dict)
        A_row_size = len(A)
        A_col_size = len(A[0])
        B_row_size = len(B)
        B_col_size = len(B[0])

        if A_row_size == 0 or B_row_size == 0:
            return []

        if A_col_size != B_row_size:
            return []

        C = [[0 for _ in range(B_col_size)] for _ in range(A_row_size)]
        print(C)

        for i in range(A_row_size):
            for k in range(A_col_size):
                for j in range(B_col_size):
                    C[i][j] += A[i][k] * B[k][j]
        return C
A = [[1, 0, 0], [-1, 0, 3]]
B = [[7, 0, 0], [0, 0, 0], [0, 0, 1]]
ans = Solution().multiply(A, B)
print("ans:", ans)




