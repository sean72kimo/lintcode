class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        if nums is None or len(nums) == 0:
            return []
        cand1, cand2, cnt1, cnt2 = 0, 0, 0, 0

        for n in nums:

            if n == cand1:
                cnt1 += 1
            elif n == cand2:
                cnt2 += 1

            elif cnt1 == 0:
                cand1 = n
                cnt1 += 1

            elif cnt2 == 0:
                cand2 = n
                cnt2 += 1
            else:
                cnt1 -= 1
                cnt2 -= 1

        len1, len2 = 0, 0
        for n in nums:
            if n == cand1:
                len1 += 1
            elif n == cand2:
                len2 += 1

        oneThird = len(nums) // 3
        print ('len:', oneThird, len1, len2)

        ans = []
        if len1 > len(nums) // 3:
            ans.append(cand1)
        if len2 > len(nums) // 3:
            ans.append(cand2)

        return ans

    def majorityElement2(self, nums):
        if not nums:
            return []
        count1, count2, candidate1, candidate2 = 0, 0, 0, 1
        for n in nums:
            if n == candidate1:
                count1 += 1
            elif n == candidate2:
                count2 += 1
            elif count1 == 0:
                candidate1, count1 = n, 1
            elif count2 == 0:
                candidate2, count2 = n, 1
            else:
                count1, count2 = count1 - 1, count2 - 1

        print(count1, count2, candidate1, candidate2)
        print(nums.count(candidate1), nums.count(candidate2), len(nums) // 3, len(nums))
        return [n for n in (candidate1, candidate2)
                        if nums.count(n) > len(nums) // 3]
nums = [8, 8, 7, 7, 7]
nums = [2, 2]
print('ans:', Solution().majorityElement(nums))
