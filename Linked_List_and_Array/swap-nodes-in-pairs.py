# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None
from lib.linkedlist import ListNode, printLinkedList, list2LinkedList
class Solution(object):
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head or not head.next:
            return head

        dummy = ListNode(0)
        dummy.next = head
        prev = dummy

        while head and head.next:
            prev, head = self.swap(prev, head)

        return dummy.next

    def swap(self, prev, one):
        two = one.next
        three = one.next.next

        prev.next = two
        two.next = one
        one.next = three

        return one, three

lst = [1, 2, 3]
head = list2LinkedList(lst)
a = Solution().swapPairs(head)
print('ans:')
printLinkedList(a)

