class Solution:
    """
    @param n: a Integer
    @return: a spiral array
    """
    def spiralArray(self, n):
        # write your code here

        last = n ** 2
        ans = [[None for _ in range(n)] for _ in range(n)]
        val = 1
        i = 0
        j = 0
        d = 0
        steps = n - 1
        size = n - 1
        dxy = [(0, 1), (1, 0), (0, -1), (-1, 0)]
        while val <= last:

            dx, dy = dxy[d % 4]
            for idx in range(steps):
                print(val, i, j, idx)
                ans[i][j] = val

                if not ans[i + dx][j + dy]:
                    i += dx
                    j += dy
                else:
                    nx, ny = dxy[(d + 1) % 4]
                    i += nx
                    j += ny
                val += 1

            d += 1

            if d % 4 == 0:
                steps -= 2

        print(ans)
        return ans

a = Solution().spiralArray(3)
print("ans:")
for row in a:
    print(row)
