class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if len(nums) == 0:
            return nums

        n = len(nums)


        for i in range(n - 1, -1, -1):
            if i - 1 >= 0 and nums[i - 1] < nums[i]:
                pivot = i - 1
                break

        # this is the max num, so return reverse as description asked
        if i == 0:
            nums[:] = nums[::-1]
            print(nums)
            return

        # from the [end of nums to pivot+1], find ceil of nums[pivot], swap
        for i in range(n - 1, pivot, -1):
            print("scan:", i, nums[i])
            if nums[i] > nums[pivot]:
                nums[pivot], nums[i] = nums[i], nums[pivot]
                break

        # reverse the second half
        nums[pivot + 1:] = nums[:pivot:-1]

        return

nums = [5, 4, 7, 5, 3, 2]
a = Solution().nextPermutation(nums)
print("ans:", nums)



