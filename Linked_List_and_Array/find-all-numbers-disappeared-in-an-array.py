class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        # For each number i in nums,
        # we mark the number that i points as negative.
        # Then we filter the list, get all the indexes
        # who points to a positive number
        ans = []
        for i in range(len(nums)):
            index = abs(nums[i]) - 1
            nums[index] = -abs(nums[index])


        for i in range(len(nums)):
            if nums[i] > 0:
                ans.append(i + 1)

        return ans

nums = [4, 3, 2, 7, 8, 2, 3, 1]
ans = Solution().findDisappearedNumbers(nums)
print('ans:', ans)

n = [4, 3]

s1 = set(nums)
s2 = set(n)

print(s1 , s2)


A = {4, 5}
B = {4, 5, 6, 7, 8}

# use & operator
# Output: {4, 5}
print(A & B)