class Solution:
    """
    @param nums: An integer array
    @return: nothing
    """
    def recoverRotatedSortedArray(self, nums):
        # write your code here
        if len(nums) <= 1:
            return nums

        def reverse(arr, left, right):

            while left < right:
                tmp = arr[right]
                arr[right] = arr[left]
                arr[left] = tmp
                left += 1
                right -= 1

        n = len(nums)
        pre = nums[0]
        for i in range(1, n):
            if nums[i] >= pre:
                pre = nums[i]
            else:
                break
            
        if i == len(nums) - 1:
            return
        
        reverse(nums, i, len(nums)-1)
        reverse(nums, 0, i-1)
        reverse(nums, 0, len(nums)-1)
        
        

        
nums = [4,5,6,1,2,3]
nums = [1,2,3,4,5,6]
nums = [1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
Solution().recoverRotatedSortedArray(nums)
print(nums)
