import collections


class Solution:

    def findDiagonalOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if len(matrix) == 0:
            return

        dd = collections.defaultdict(list)

        m = len(matrix)
        n = len(matrix[0])

        for i in range(m):
            for j in range(n):
                dd[i + j].append(matrix[i][j])

        mx = max(dd.keys())
        ans = []
        for i in range(mx + 1):
            if i % 2 == 1:
                ans.extend(dd[i])
            else:
                ans.extend(dd[i][::-1])

        return ans


matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
expected = [1, 2, 4, 7, 5, 3, 6, 8, 9]
a = Solution().findDiagonalOrder(matrix)
print("ans:", a == expected, a)
