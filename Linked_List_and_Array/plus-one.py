class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        n = len(digits)
        carry = 0
        addup = 0
        digits[n - 1] += 1
        for i in range(len(digits) - 1, -1, -1):
            digits[i] = digits[i] + carry
            carry = digits[i] // 10
            digits[i] = digits[i] % 10

        if carry > 0:
            digits.insert(0, carry)

        return digits
digits = [9, 0, 9]
Solution().plusOne(digits)
