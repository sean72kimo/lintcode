"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""


class Solution:

    # @param {ListNode} node a list node in the list
    # @param {int} x an integer
    # @return {ListNode} the inserted new list node
    def insert(self, node, x):
        # Write your code here
        if not node:
            newNode = ListNode(x)
            newNode.next = newNode
            return newNode

        curr = node
        prev = None

        while True:
            prev = curr
            curr = curr.next

            # 1->2->3->4
            if x <= curr.val and x >= prev.val:
                print('case 1')
                break

            # 4->1->2->3
            # prev > curr代表已經走到環的最末端。在環的最末端插入的情況有兩種
            # 1. x 是最末 (x > prev)
            # 2. x 是最頭 (x < curr)
            if (prev.val > curr.val) and (x < curr.val or x > prev.val):
                print('case 2')
                break

            # 2->2->2 and insert 3
            if curr is node:
                print('case 3')
                break

        newNode = ListNode(x)
        newNode.next = curr
        prev.next = newNode

        return newNode


from lib.linkedlist import ListNode, printLinkedList


def printNodeList(node):
    head = node
    while node != None:
        print(node.val, end='->')
        node = node.next
        if node == head:
            break
    print('')


node1 = ListNode(1)
node2 = ListNode(1)
node3 = ListNode(1)
node4 = ListNode(4)
node5 = ListNode(5)
node6 = ListNode(6)

node1.next = node2
node2.next = node3
node3.next = node1

printNodeList(node1)
sol = Solution()
ans = sol.insert(node1, 3)
printNodeList(ans)

