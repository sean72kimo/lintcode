from typing import List


class Solution:
    def maxSumTwoNoOverlap(self, nums: List[int], L: int, M: int) -> int:
        n = len(nums)
        preSum = [0] * (n + 1)

        for i in range(1, n + 1):
            preSum[i] = preSum[i - 1] + nums[i - 1]
        res = 0;
        maxL = 0;
        maxM = 0;

        for i in range(L, n + 1):
            if i + M >= len(preSum):
                break
            maxL = max(maxL, preSum[i] - preSum[i - L])
            res = max(res, maxL + preSum[i + M] - preSum[i])

        for i in range(M, n + 1):
            if i + L >= len(preSum):
                break
            maxM = max(maxM, preSum[i] - preSum[i - M])
            res = max(res, maxM + preSum[i + L] - preSum[i])

        return res

nums = [0,6,5,2,2,5,1,9,4]
L = 1
M = 2
exp = 20
a = Solution().maxSumTwoNoOverlap(nums, L, M)
print("ans:", a)