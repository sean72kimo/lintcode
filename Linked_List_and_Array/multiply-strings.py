class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        1. num3為暫存空間，長度為 len(num1) + len(num2), ex: 99 * 99 = 9081, len(num3) = 4
        2. num3[i + j] += int(num1[i]) * int(num2[j])，先不用進位
        3. 掃一遍num3並處理進位。注意，將int轉回str存回num3[i]
        4. lstrip去掉左邊的0
        5. 如果string = "00", lstrip過後變為""，return "0"
        """
        num1 = list(num1[::-1])
        num2 = list(num2[::-1])
        num3 = [0] * (len(num1) + len(num2))
        carry = 0
        ans = ""

        for i in range(len(num1)):
            for j in range(len(num2)):
                num3[i + j] += int(num1[i]) * int(num2[j])

        carry = 0
        for i in range(len(num3)):
            n = num3[i] + carry
            v = n % 10
            carry = n // 10
            num3[i] = str(v)

        string = ''.join(num3[::-1])
        string = string.lstrip('0')
        if len(string) == 0:
            return "0"
        return string
