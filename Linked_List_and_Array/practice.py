from lib.linkedlist import list2LinkedList, printLinkedList
class ListNode(object):

    def __init__(self, val, next = None):
        self.val = val
        self.next = next


class Solution(object):
    def reverseBetween(self, head, m, n):

        dummy = ListNode(0)
        dummy.next = head
        curr = dummy

        for _ in range(m - 1):
            curr = curr.next

        beforeM = curr
        atN = curr.next

        prev = curr.next
        curr = curr.next.next

        for i in range(n - m):
            prev, curr = self.reverseList(curr, prev)

        beforeM.next = prev
        atN.next = curr
        return dummy.next

    def reverseList(self, curr, prev):

        temp = curr.next
        curr.next = prev
        prev = curr
        curr = temp

        return prev, curr




def main():
    lst = [1, 2, 3, 4, 5]
    m = 5
    n = 5
    last = ListNode(lst[-1])

    for num in lst[-2::-1]:
        node = ListNode(num)
        node.next = last
        last = node
    head = node

    head = ListNode(5)
    m = 1
    n = 1

    node = Solution().reverseBetween(head, m, n)

    print("ans:")
    printLinkedList(node)



if __name__ == "__main__":
    main()
