class Solution:
    # @param num :  a list of integer
    # @return : a list of integer
    def previousPermuation(self, A):
        # write your code here
    
        if len(A) <= 1:
            return A
        
        p = None
        for i in range(len(A)-2, -1, -1):
            if A[i] > A[i+1]:
                p = i
                break
            
        if p == None:
            return A[::-1]
            
        for i in range(len(A)-1, p, -1):
            if A[i] < A[p]:
                A[i], A[p] = A[p], A[i]
                break
            
        A[p+1:] = A[p+1:][::-1]
        
        return A
A = [5,4,7,5,6,8,9]
Solution().previousPermuation(A)
print("ans:",A)