# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
from pyparsing import Optional

from lib.linkedlist import ListNode

"""
similar to 61. Rotate List
但本題不需要取模
"""
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        if not head or n == 0:
            return head
        if not head.next:
            return None

        dummy = ListNode()
        dummy.next = head
        curr = head
        size = 0

        while curr:
            curr = curr.next
            size += 1
        # n = n % size
        k = size - n

        curr = dummy
        for _ in range(k):
            curr = curr.next

        remove = curr.next
        if remove:
            curr.next = remove.next
        else:
            curr.next = None

        return dummy.next