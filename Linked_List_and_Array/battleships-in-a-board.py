class Solution(object):
    def countBattleships(self, board):
        """
        https://leetcode.com/problems/battleships-in-a-board/description/
        :type board: List[List[str]]
        :rtype: int

        1.題目假設 input board一定valid
        2.上到下，左到右，掃描board, 遇到'X'且左邊或是上面非'X'，則cnt += 1
        """
        if not any(board):
            return 0
        m = len(board)
        n = len(board[0])

        ans = 0
        for i in range(m):
            for j in range(n):
                if board[i][j] == 'X':
                    if i - 1 >= 0 and board[i - 1][j] == 'X':
                        continue
                    if j - 1 >= 0 and board[i][j - 1] == 'X':
                        continue
                    ans += 1
        return ans
