class Solution(object):
    def imageSmoother(self, M):
        """
        :type M: List[List[int]]
        :rtype: List[List[int]]
        """


        n = len(M)
        m = len(M[0])
        res = [[0 for i in range(m)]for i in range(n)]
        dirs = [[0, 1], [0, -1], [1, 0], [-1, 0], [-1, -1], [1, 1], [-1, 1], [1, -1]]

        def inbound(x, y):
            return 0 <= x < n and 0 <= y < m

        for i in range(n):
            for j in range(m):
                nei = []
                nei.append(M[i][j])

                for x, y in dirs:

                    new_i = i + x
                    new_j = j + y
                    if inbound(new_i, new_j):
                        nei.append(M[new_i][new_j])

                        if i == 0 and j == 1 :
                            print (new_i, new_j)




                res[i][j] = sum(nei) // len(nei)

        return res

M = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]
a = Solution().imageSmoother(M)
print("----")
for row in a:
    print(row)
