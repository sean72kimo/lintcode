"""
287. Find the Duplicate Number
https://leetcode.com/problems/find-the-duplicate-number/description/



"""
class Solution_swap(object):
    """
    41. First Missing Positive
    https://leetcode.com/problems/first-missing-positive/description/
    1.目標:將每個元素移至其相對應的下標，例如
    數字2移動至A[1] (第2個數)
    數字3移動至A[2] (第3個數)
    數字4移動至A[3] (第4個數)
    2. 遍歷數組A，找出數字和下標無法對應的點，並回傳答案
    3. 考慮corner case 例如原來的輸入為 [1,2,3], [0], [1]
    """
    def missingNumber(self, A):

        for i, n in enumerate(A):
            while 0 <= A[i] - 1 < len(A) and A[i] != A[ A[i] - 1 ]:
                tmp = A[ A[i] - 1 ]
                A[ A[i] - 1 ] = A[i]
                A[i] = tmp

        for i in range(len(A)):
            if A[i] != i + 1:
                return i + 1
        if i + 1 == len(A):
            return 0
nums = [3, 0, 1]
nums = [9, 6, 4, 2, 3, 5, 7, 0, 1]
nums = [1]
nums = [0]

a = Solution_swap().missingNumber(nums)
print("ans:", a)

class Solution_sum(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        time O(n)
        space O(1)
        n 個數總和 ，1+2+3+4+...+n = ((1+n) * n)//2
        for num in nums, summ逐次減去num，最後剩下的那個數即為missing number
        """
        if len(nums) == 0:
            return 0

        n = len(nums)
        summ = ((1 + n) * n) // 2
        print(summ)
        for num in nums:
            summ -= num
        return summ

nums = [3, 0, 1]
nums = [9, 6, 4, 2, 3, 5, 7, 0, 1]

a = Solution_sum().missingNumber(nums)
print("ans:", a)



# class SolutionBinarySearch(object):
#     def missingNumber(self, nums):
#         """
#         :type nums: List[int]
#         :rtype: int
#         time O(n)
#         space O(1)
#         n 個數總和 ，1+2+3+4+...+n = ((1+n) * n)//2
#         for num in nums, summ逐次減去num，最後剩下的那個數即為missing number
#         """
#         if len(nums) == 0:
#             return 0
#
#         nums.sort()
#         n = len(nums)
#         s = 0
#         e = n
#         while s + 1 < e:
#             m = (s + e)//2
#             if m = nums[m]:
#                 s = m
#
#
# nums = [9, 6, 4, 2, 3, 5, 7, 0, 1]
# a = SolutionBinarySearch().missingNumber(nums)
# print("ans:", a)



class Solution2(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        time O(n)
        space O(n)
        1. 產生一個hashset存有 0 ~ n個數
        2. 從nums裡面讀取每一個num，將num從hashset中移除
        3. hashset最後剩下的那個數就是missing number
        """
        n = len(nums)
        all_of_them = set(range(0, n + 1))
        for num in nums:
            if num in all_of_them:
                all_of_them.remove(num)

        return list(all_of_them)[0]
