"""
思路：如果把數字從1開始排序，1,2,3,4,5
A[i] == A[A[i]-1]
所以目標就是把題目輸入的數組不斷地透過swap調整成 1,2,3,4...
最後再掃描缺少的正數是誰
返回值得corner case為：輸入數組並不缺少正數，所以答案應該是最末位數字再加一

"""
class Solution(object):
    def firstMissingPositive(self, A):
        """
        :type nums: List[int]
        :rtype: int
        """
        print("input: ", A)
        if not A:
            return 1
        n = len(A)
        i = 0
        # move A[i] to position A[i] - 1
        for i in range(len(A)):
            while A[i] > 0 and A[i] <= len(A) and :
                tmp = A[A[i]-1]
                A[A[i]-1] = A[i]
                A[i] = tmp

        print("sorted:", A)
        for i in range(n):
            if A[i] != i+1:
                
                return i+1
        
        return len(A) + 1
            

def main():
    A = [1,2,0]
    A = [3, 4, -1, 1]
#     A = [-5, 1000]
    A = [7,9,8,11,12]
    A = [1]
    
    a = Solution().firstMissingPositive(A)

    print("ans:", a)

    string = "abc"
    print(''.join(reversed(string)))

if __name__ == "__main__":
    main()
