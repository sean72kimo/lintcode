

class Solution:
    def reverseKGroup(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode

        1.先計算要執行幾次reverse sub-chain-reverse
        2.先找到sub-chain的開頭 , prev = None, curr = first node of sub-chain
        3.in reverse sub-chain
        3.1.tail = curr, 因為反轉後，sub-chain當前的head會變成tail
        3.2.結束後prev成為反轉過後 sub-chain的開頭，所以last要接到prev
            curr成為下一個sub-chain的開頭(等待下次反轉)
        3.3.last 移動到tail的位置
            (因為最早的head，反轉之後成為tail，last移動至此tail，下次翻轉後，則用last.next連接到下一個 sub-chain)
        """

        if not head:
            return head

        length = 0
        dummy = ListNode(0)
        dummy.next = head
        curr = dummy

        while curr and curr.next:
            curr = curr.next
            length += 1

        if length < k:
            return dummy.next

        cnt = length // k


        curr = head
        last = curr
        prev = None
        last = dummy
        print('...', cnt, length)
        for i in range(cnt):
            last, prev, curr = self.reverseKNodes(last, prev, curr, k)
            prev = None
        last.next = curr

        return dummy.next

    def reverseKNodes(self, last, prev, curr, k):
        hold = curr

        for _ in range(k):
            tmp = curr.next
            curr.next = prev
            prev = curr
            curr = tmp
        last.next = prev
        last = hold
        return last, prev, curr







class Solution2:
    # @param head, a ListNode
    # @param k, an integer
    # @return a ListNode
    def reverseKGroup(self, head, k):
        # Write your code here
        dummy = ListNode(0)
        dummy.next = head

        prev = dummy

        while prev:
            prev = self.reverseKNodes(prev, k)

        return dummy.next

    def reverseKNodes(self, head, k):
        if k <= 0:
            return None
        if head is None:
            return None

        print('head val', head.val)
        node1 = head.next
        nodek = head

        for i in range(k):
            if nodek is None:
                return None
            nodek = nodek.next

        if nodek is None:
            return None

        nodekplus = nodek.next
        print('before reverse nodek.val',nodek.val)

        prev = None
        curr = node1
        while curr is not nodekplus:
            temp = curr.next
            curr.next = prev
            prev = curr
            curr = temp

        print('after  reverse nodek.val',nodek.val)
        head.next = nodek
        node1.next = nodekplus
        return node1



def printNodeList(node):
    while node != None:
        print(node.val, end='->')
        node = node.next
    print('')

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

node1 = ListNode(1)
node2 = ListNode(2)
node3 = ListNode(3)
node4 = ListNode(4)
node5 = ListNode(5)
node6 = ListNode(6)
node7 = ListNode(7)


node1.next = node2
node2.next = node3
node3.next = node4
node4.next = node5
node5.next = node6
node6.next = node7

printNodeList(node1)
sol = Solution()
node = sol.reverseKGroup(node1, 3)
printNodeList(node)


