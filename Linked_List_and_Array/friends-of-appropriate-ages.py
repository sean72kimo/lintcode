
from typing import List

class Solution:
    def numFriendRequests(self, ages: List[int]) -> int:
        if len(ages) <= 1:
            return 0
        n = len(ages)
        ages.sort()
        ans = 0
        for i in range(n):
            for j in range(i + 1, n):

                if self.violation(ages[i], ages[j]):
                    pass
                else:
                    ans += 1

                if self.violation(ages[j], ages[i]):
                    pass
                else:
                    ans += 1

        return ans

    def violation(self, x, y):
        a = y <= 0.5 * x + 7
        b = y > x
        c = x < 100 < y

        return a or b or c
ages = [16,17,18]
a = Solution().numFriendRequests(ages)
print("ans:", a)