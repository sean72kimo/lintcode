from typing import List

"""
similar to valid sudoku
"""
class Solution:
    def checkValid(self, matrix: List[List[int]]) -> bool:

        m = len(matrix)
        rows = [set() for _ in range(m)]
        cols = [set() for _ in range(m)]

        for i in range(m):
            for j in range(m):
                if matrix[i][j] > m or matrix[i][j] < 1:
                    return False
                v = matrix[i][j]
                rows[i].add(v)
                cols[j].add(v)

        for row in rows:
            if len(row) != m:
                return False

        for col in cols:
            if len(col) != m:
                return False

        return True


