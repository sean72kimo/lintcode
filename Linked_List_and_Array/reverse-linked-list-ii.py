class ListNode(object):

    def __init__(self, val, next = None):
        self.val = val
        self.next = next

"""
分拆出一個reverse function更好理解
reverse between 2 and 4

Dummy -> 1 -> 2 -> 3 -> 4 -> 5 -> null
1.      b4M  last
2.           prev curr        (進行第一次reverse)
3.                prev curr   (進行第二次reverse)
Dummy -> 1 -> 2 <-> 3 <- 4   5 -> null
4.                      prev curr (兩次reverse結束後)
5. last.next = prev
Dummy -> 1 -> 2 <- 3 <- 4   5 -> null
6. b4M.next = curr
"""
class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        if not head or not head.next:
            return head

        dummy = ListNode(0)
        dummy.next = head
        curr = dummy
        beforeM = dummy
        cnt = 0

        while curr:
            beforeM = curr
            curr = curr.next
            cnt += 1
            if cnt == m:
                break

        last = curr
        prev = curr
        curr = curr.next

        for i in range(n - m):
            prev, curr = self.reverse(prev, curr)

        beforeM.next = prev
        last.next = curr

        return dummy.next

    def reverse(self, prev, curr):
        if curr is None:
            return

        temp = curr.next
        curr.next = prev
        prev = curr
        curr = temp

        return prev, curr


class Solution(object):
    def reverseBetween(self, head, m, n):
        print(head.val, m, n)
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        dummy = ListNode(0)
        dummy.next = head
        helper = dummy
        beforeM = dummy

        i = 1
        while head.next:
            if i + 1 == m:
                beforeM = head
                break
            head = head.next
            i += 1

        print(beforeM.val)

        head = beforeM.next
        prev = beforeM


        for i in range(m, n + 1):  # 2,3,4
            tmp = head.next

            head.next = prev
            prev = head
            head = tmp

        beforeM.next.next = head
        beforeM.next = prev

        return dummy.next


def main():
    lst = [1, 2, 3, 4, 5]
    m = 2
    n = 4
    last = ListNode(lst[-1])

    for num in lst[-2::-1]:
        node = ListNode(num)
        node.next = last
        last = node
    head = node

    head = ListNode(5)
    m = 1
    n = 1

    node = Solution().reverseBetween(head, m, n)

    while node != None:
        print(node.val, end = '->')
        node = node.next
    print()



if __name__ == "__main__":
    main()



