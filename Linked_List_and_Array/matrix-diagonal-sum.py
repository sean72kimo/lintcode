from typing import List

"""
similar to valid sudoku
"""

class Solution:
    def diagonalSum(self, mat: List[List[int]]) -> int:
        if not mat:
            return 0

        val = list()
        vst = set()

        m = len(mat)
        n = len(mat[0])

        for i in range(m):
            for j in range(n):
                if i == j or i + j == m - 1 and (i, j) not in vst:
                    val.append(mat[i][j])
                    vst.add((i, j))

        return sum(val)