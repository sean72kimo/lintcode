class Solution(object):
    def thirdMax(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return float('-inf')

        max1 = None
        max2 = None
        max3 = None

        for i, n in enumerate(nums):
            if max1 is None or n > max1:
                max3 = max2
                max2 = max1
                max1 = n

            elif (max2 is None or n > max2) and n != max1:
                max3 = max2
                max2 = n

            elif (max3 is None or n > max3) and n != max2 and n != max1:
                max3 = n

            print(i, (max1, max2, max3))

        return max3 if max3 is not None else max1

nums = [1, 2, 2, 5, 3, 5]
a = Solution().thirdMax(nums)
print("ans:", a)
