import collections


class Solution(object):
    def findJudge(self, N, trust):
        """
        :type N: int
        :type trust: List[List[int]]
        :rtype: int

        此題構成一個有向圖
        用trust構造出每個點的in_degree and out_degree
        judge 永遠所有人對於他的"進" & 沒有"出"，所以如果某個點的 ind[p] == N-1 & out[p] = 0 -> judge = p
        """
        if len(trust) == 0:
            return 1

        judge = 1
        ind = collections.defaultdict(int)
        out = collections.defaultdict(int)
        for a, b in trust:
            out[a] += 1
            ind[b] += 1

        for p in range(1, N + 1):
            if out[p] == 0 and ind[p] == N - 1:
                return p
        return -1

case0 = {
    'inp' : [ 2, [[1,2]] ],
    'exp' : 2
}
case1 = {
    'inp' : [ 3, [[1,3],[2,3]] ],
    'exp' : 3
}
case2 = {
    'inp' : [ 3, [[1,3],[2,3],[3,1]] ],
    'exp' : -1
}
case3 = {
    'inp' : [ 3, [[1,2],[2,3]] ],
    'exp' : -1
}
case4 = {
    'inp' : [ 4, [[1,3],[1,4],[2,3],[2,4],[4,3]] ],
    'exp' : 3
}
case5 = {
    'inp' : [ 1, [] ],
    'exp' : 1
}

tests = [case0,case1,case2,case3,case4,case5]
DEBUG = False
# DEBUG = True
sol = Solution()

for i, case in enumerate(tests):
    if DEBUG and i != 3:
        continue
    a = sol.findJudge(*case['inp'])
    if a != case['exp']:
        print("testing case#{} fail".format(i), a)
        DEBUG = True
        break