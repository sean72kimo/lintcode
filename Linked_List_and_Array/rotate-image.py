class Solution:
    """
    @param matrix: a lists of integers
    @return: nothing
    """
    def swap(self,matrix, i, j, x, y):
        temp = matrix[i][j]
        matrix[i][j] = matrix[x][y]
        matrix[x][y] = temp
        
    def rotate(self, matrix):
        # write your code here
        if len(matrix) == 0:
            return matrix
            
        n = len(matrix)
        m = len(matrix[0])

        
        for c in range(m):
            for r in range(n//2):
                
                tmp = matrix[r][c]
                matrix[r][c] = matrix[n - 1 - r][c]
                matrix[n - 1 - r][c] = tmp
                
        
        for i in range(n):
            for j in range(m//2):
                print("({},{}) <-> ({},{})".format(i,j,j,i))
                self.swap(matrix, i, j, j, i)
                
matrix = [
    [1,2],
    [3,4]
]
Solution().rotate(matrix)
print("ans:")
for r in matrix:
    print(r)
