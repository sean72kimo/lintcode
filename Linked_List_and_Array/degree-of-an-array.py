import collections
class Solution(object):
    def findShortestSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        counter = collections.Counter(nums)


        first = {}
        last = {}
        for i, v in enumerate(nums):
            if v not in first:
                first[v] = i
            last[v] = i

        degree = max(list(counter.values()))

        ans = float('inf')
        for v, c in counter.items():
            if c == degree:
                ans = min(ans, last[v] - first[v] + 1)

        return ans

nums = [1, 2, 2, 3, 1]
a = Solution().findShortestSubArray(nums)
print("ans:", a)



