class SparseVector:
    def __init__(self, nums):
        self.non_zero = {}
        for i, n in enumerate(nums):
            if n == 0:
                continue
            self.non_zero[i] = n

    # Return the dotProduct of two sparse vectors
    def dotProduct(self, vec: 'SparseVector') -> int:
        res = 0
        for i, n in self.non_zero.items():
            if i in vec.non_zero:
                res += n * vec.non_zero[i]
        return res

# Your SparseVector object will be instantiated and called as such:
# v1 = SparseVector(nums1)
# v2 = SparseVector(nums2)
# ans = v1.dotProduct(v2)