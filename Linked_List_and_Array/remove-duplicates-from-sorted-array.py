class Solution:
    # @param a list of integers
    # @return an integer
    def removeDuplicates(self, A):
        if not A:
            return 0

        tail = 0

        for i in range(1, len(A)):
            if A[i] != A[tail]:
                tail += 1
                A[tail] = A[i]
                print(A)

        return tail + 1

A = [1, 2, 3, 4]
Solution().removeDuplicates(A)
