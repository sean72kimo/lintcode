
"""
similar to 926. Flip String to Monotone Increasing
目標：使這個字串成為左半都為a右半都為b
遍歷整個prefix長度，每i個，要花多少代價才能得到目標，取代價最小
"""
class Solution:
    def minimumDeletions(self, s: str) -> int:

        p = [0]
        for ch in s:
            if ch == 'b':
                p.append(p[-1] + 1)
            else:
                p.append(p[-1])
        n = len(s)
        ans = n
        for i in range(len(p)):
            b = p[n] - p[i]
            a = n - i - b
            ans = min(p[i] + a, ans)

        return ans