from lib.linkedlist import ListNode, printLinkedList, list2LinkedList

class Solution(object):
    def plusOne(self, head):
        """
        :type head: ListNode
        :rtype: ListNode

        注意 while curr 迴圈累加到最後，curr是停在None的位置，用一個prev來記錄最尾端那個點
        計算出 d = v % 10, 記得將d賦值給curr.val
        """
        if head is None or head.val == 0:
            return ListNode(1)

        h = self.reverse(head)
        curr = h
        carry = 0
        cnt = 1
        prev = None
        while curr:
            if cnt == 1:
                curr.val += 1

            v = curr.val + carry
            d = v % 10
            curr.val = d
            carry = v // 10

            prev = curr
            curr = curr.next
            cnt += 1

        curr = prev
        if carry:
            curr.next = ListNode(carry)
            curr = curr.next

        h = self.reverse(h)

        return h

    def reverse(self, head):
        if not head.next:
            return head
        prev = None
        curr = head
        while curr:
            temp = curr.next
            curr.next = prev
            prev = curr
            curr = temp
        return prev

data = [1,2,3]
data = [9]
head = list2LinkedList(data)
sol = Solution()
a = sol.plusOne(head)
print("ans:")
printLinkedList(a)