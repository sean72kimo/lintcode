import collections
from typing import List

"""
1. 計算出兩個nums各自的sum
1. target = sum1 - sum2, 代表兩個數組差距
1. 兩個數組可以各自調整數字於1 ~ 6 之間，
1. 較大數組應該往下貢獻去逼近target, 較小數組應該往上貢獻去逼近target
1. 大數組若是往上調整，那麼小數組必須更加的往上調整，不合邏輯，因為目標是大小數組往中間靠近
1. 從貢獻值由大到小排序 gain.sort(reverse=True), 越快到達target越好
1. corner case is diff == 0, which is no adjustment needed

如果
長度較短數組，把所有數字都調整成6 得到sum_short
長度較長數組，把所有數字都調整成1, 但是數組和還是大於sum_short 
兩個數組都盡力了，還是找不到平衡點，return -1
無解的這個corner case 可以被最後的 for loop給含括
因為若是 diff -= gain[i], diff卻永遠無法 <= 0, 代表兩個數組永遠無罰弭平差異。 
"""
class Solution:
    def has_solution(self, nums1, nums2):

        if len(nums2) > len(nums1):
            return self.has_solution(nums2, nums1)
        long = len(nums1)
        short = len(nums2)
        return 1 * long <= 6 * short


    def minOperations(self, nums1: List[int], nums2: List[int]) -> int:

        if not self.has_solution(nums1, nums2):
            return -1

        sum1 = sum(nums1)
        sum2 = sum(nums2)
        if sum2 > sum1:
            return self.minOperations(nums2, nums1)

        diff = sum1 - sum2
        if diff == 0:
            return 0
        gain_in_large = [n - 1 for n in nums1]
        gain_in_small = [6 - n for n in nums2]

        gain = gain_in_large + gain_in_small
        gain.sort(reverse=True)

        count = 0
        for i in range(len(gain)):
            diff -= gain[i]
            count += 1

            if diff <= 0:
                return count

        return -1


class Solution2:
    def minOperations(self, nums1: List[int], nums2: List[int]) -> int:
        """
        Solution2更為簡潔，不需判斷 has_solution
        因為最後面的for loop 如果無法得到diff <= 0, 那就回傳-1即可。
        """
        sum1 = sum(nums1)
        sum2 = sum(nums2)

        if sum1 == sum2:
            return 0

        if sum2 > sum1:
            return self.minOperations(nums2, nums1)

        # assume sume1 > sum2
        diff = sum1 - sum2

        gain_in_large = [n - 1 for n in nums1]
        gain_in_small = [6 - n for n in nums2]

        gain = gain_in_large + gain_in_small
        gain.sort(reverse=True)

        for i, g in enumerate(gain):
            diff -= g

            if diff <= 0:
                return i + 1
        return -1

nums1 = [1,2,3,4,5,6]
nums2 = [1,1,2,2,2,2]
a = Solution().minOperations(nums1, nums2)
print("ans: ", a)




class Solution:
    def getFolderNames(self, names: List[str]) -> List[str]:
        versions = collections.defaultdict(int)

        ans = []
        for name in names:
            if name not in versions:
                versions[name] = 0
                ans.append(name)
                continue

            ver = versions[name]
            modified = name

            while modified in versions:
                ver += 1
                modified = f'{name}({ver})'

            ans.append(modified)
            versions[modified] = 0
            versions[name] = ver

        return ans

names = ["gta","gta(1)","gta","avalon"]
names = ["wano","wano","wano","wano"]
names = ["kaido","kaido(1)","kaido","kaido(1)","kaido(2)"]
a = Solution().getFolderNames(names)
print(a)