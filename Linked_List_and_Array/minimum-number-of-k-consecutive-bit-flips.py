from typing import List


class SolutionTLE:
    def minKBitFlips(self, nums: List[int], k: int) -> int:
        n = len(nums)
        res = 0

        for i in range(n - k + 1):
            if nums[i] == 1:
                continue

            # nums[i] == 0
            for j in range(k):
                nums[i + j] = nums[i + j] ^ 1

            res += 1

        for i in range(n):
            if nums[i] == 0:
                return -1

        return res