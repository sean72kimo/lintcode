class Solution(object):
    def findDuplicates(self, A):
        """
        :type nums: List[int]
        :rtype: List[int]
        利用輸入矩陣當作標記的功能。矩陣的index為0 base, 數字-1轉換成對應的位置。
        ex 如果數字2出現了，就把 A[idx=1]的值，改為負數，如果再次碰到2，A[idx=1]是負數，代表2出現過了

        注意！數字轉idx的時候，要轉為base0,並且要記得當前數字可能曾經被”標記”所以呈現負數，記得取絕對值。
        當發現某數字A[i]小於零的時候，表示數字= i+1曾經出現過。
        """
        if A is None or len(A) == 0:
            return A
        n = len(A)

        res = []

        for i in range(n):
            idx = abs(A[i]) - 1

            if i == 5 or i == 6:
                print(A[i], idx)

            if A[ idx ] >= 0:
                A[idx] = -A[idx]
            else:
                res.append(idx + 1)



        return res

nums = [10, 10, 5, 2, 9, 1, 1, 4, 3, 7]
a = Solution().findDuplicates(nums)
print('ans:', a)
