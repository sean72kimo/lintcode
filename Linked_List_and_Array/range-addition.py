from typing import List

"""
1. 目標是利用前綴和的特性
1. length = 5 [0,0,0,0,0]  
1. 假設有一個 update = [1,3,2]
1. 暴力法是 [0,2,2,2,0]
1. 但我們可以利用前綴和，只記錄2開始(start)的位置，並且在2結束(end+1)位置寫入-2, 抵銷(取消)前綴和的累加效果
"""
class Solution:
    def getModifiedArray(self, length: int, updates: List[List[int]]) -> List[int]:
        res = [0] * (length + 1)
        for s, e, v in updates:
            res[s] += v
            res[e + 1] -= v

        for i in range(1, length):
            res[i] += res[i - 1]

        return res[:-1]
