class Solution(object):
    def summaryRanges(self, nums):
        """
        :type nums: List[int]
        :rtype: List[str]
        """
        ans = []
        if nums is None or len(nums) == 0:
            return ans

        n = len(nums)
        range = []
        r = []
        s = 0
        i = 1
        ans.append(str(nums[0]))
        while i < n :
            if nums[i] - 1 == nums[i - 1]:
                if '->' in ans[-1]:
                    end = ans[-1].find('->')
                    ans[-1] = ans[-1][:end] + '->' + str(nums[i])
                else:
                    ans[-1] = ans[-1] + '->' + str(nums[i])

            else:
                ans.append(str(nums[i]))
            i += 1

        return ans

nums = [0, 1, 2, 4, 5, 7]
print('ans:', Solution().summaryRanges(nums))
