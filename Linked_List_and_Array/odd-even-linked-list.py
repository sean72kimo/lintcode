

from lib.linkedlist import ListNode, list2LinkedList, printLinkedList
class Solution(object):
    def oddEvenList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """

        cnt = 1
        if not head:
            return None

        if not head.next:
            return head

        dummy = ListNode(None)
        odd = dummy
        dummy2 = ListNode(None)
        even = dummy2


        while head and head.next:

            if cnt % 2:  # odd
                odd.next = head
                head = head.next
                odd = odd.next
                odd.next = head.next


            else:  # even
                even.next = head
                head = head.next
                even = even.next
                even.next = head.next

            cnt += 1


        end_of_odd = dummy
        while end_of_odd and end_of_odd.next:
            end_of_odd = end_of_odd.next


        end_of_odd.next = dummy2.next

        return dummy.next


class Solution2(object):
    def oddEvenList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        odd從head開始，一邊將curr連結odd, 一邊斷開odd to even 的連結並將even list連結起來。
        curr結束後，應該已經串起所有odd, 檢查是否有最後一個odd, 若有則連結，然後再將curr和even list連結
        """
        if not head or not head.next:
            return head

        dummy = ListNode(None)
        curr = dummy
        odd = head
        h2 = head.next

        while odd and odd.next:
            even = odd.next
            curr.next = odd
            curr = curr.next

            odd = odd.next.next
            if odd:
                even.next = odd.next

        if odd:
            curr.next = odd
            curr = curr.next
        curr.next = h2

        return dummy.next




data = []
head = list2LinkedList(data)
a = Solution2().oddEvenList(head)
print("ans:")
printLinkedList(a)

