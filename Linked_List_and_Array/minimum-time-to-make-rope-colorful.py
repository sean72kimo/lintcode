from typing import List

"""
greedy, 遇到連續字母，只留下cost最高的那個，其他刪去
用max_cost_so_far記錄當前連續字母中，max cost
res裡面積存著要刪去字母的cost (刪去cost小的)
"""
class Solution:
    def minCost(self, s: str, cost: List[int]) -> int:
        i = 0
        j = 0
        res = 0
        summ = 0
        max_cost_so_far = 0
        for i in range(len(s)):
            if i - 1 >= 0 and s[i] != s[i - 1]:
                max_cost_so_far = 0

            res += min(max_cost_so_far, cost[i])
            max_cost_so_far = max(max_cost_so_far, cost[i])

        return res

s = "bb"
cost = [1,2]
a = Solution().minCost(s, cost)
print("ans:", a)