# 二進制，低位0 or 1代表現在細胞存活，高位0 or 1代表下一輪細胞存活
# 0: (00)現在是死細胞，下一輪是死細胞
# 1: (01)現在是活細胞，下一輪是死細胞
# 2: (10)現在是死細胞。因為周圍有三個活細胞，下一輪會變成活細胞。
# 3: (11)現在是活細胞。周圍活細胞為2 or 3。下一輪當前細胞會繼續存活

class Solution(object):
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: None Do not return anything, modify board in-place instead.
        """

        n = len(board)
        m = len(board[0])

        for i in range(n):
            for j in range(m):
                cnt = self.nei_cnt(board, i, j)

                if board[i][j] == 0:
                    if cnt == 3:
                        board[i][j] = 2

                elif board[i][j] == 1:
                    if cnt < 2:
                        board[i][j] = 1
                    if cnt == 2 or cnt == 3:
                        board[i][j] = 3
                    if cnt > 3:
                        board[i][j] = 1

        for i in range(n):
            for j in range(m):
                if board[i][j] in [0, 1]:
                    board[i][j] = 0
                elif board[i][j] in [2, 3]:
                    board[i][j] = 1

        return

    def nei_cnt(self, board, x, y):
        cnt = 0
        dirs = [0, 1], [0, -1], [1, 0], [-1, 0], [1, 1], [-1, -1], [-1, 1], [1, -1],

        for dx, dy in dirs:
            nx = x + dx
            ny = y + dy

            if not (0 <= nx < len(board) and 0 <= ny < len(board[0])):
                continue

            if board[nx][ny] == 1 or board[nx][ny] == 3:
                cnt += 1

        return cnt


class Solution2(object):
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: None Do not return anything, modify board in-place instead.
        """
        if len(board) == 0:
            return

        m = len(board)
        n = len(board[0])
        dxy = [[0, 1], [1, 0], [0, -1], [-1, 0], [1, 1], [1, -1], [-1, 1], [-1, -1]]

        def inbound(x, y):
            return 0 <= x < m and 0 <= y < n

        for i in range(m):
            for j in range(n):

                nei = 0
                for dx, dy in dxy:
                    nx = i + dx
                    ny = j + dy
                    if not inbound(nx, ny):
                        continue
                    if board[nx][ny] & 0b01 == 1:
                        nei += 1
                print((i, j), nei)

                if board[i][j] & 0b01 == 0b01 and nei < 2:
                    board[i][j] = 1  # 01
                elif board[i][j] & 0b01 == 0b01 and (nei == 2 or nei == 3):
                    board[i][j] = 3  # 11
                elif board[i][j] & 0b01 == 0b01 and (nei > 3):
                    board[i][j] = 1  # 01
                elif board[i][j] & 0b01 == 0b00 and (nei == 3):
                    board[i][j] = 2  # 01

        for i in range(m):
            for j in range(n):
                board[i][j] = 1 if board[i][j] & 0b10 == 0b10 else 0

        print('ans:', board)


board = [[0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]]
board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
ans1 = Solution().gameOfLife(board)

board = [[0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]]
board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
ans2 = Solution2().gameOfLife(board)