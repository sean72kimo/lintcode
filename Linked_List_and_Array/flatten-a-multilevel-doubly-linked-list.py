from lib.dlinkedlist import ListNode, DoubleLinkedListUtil

class Solution2(object):
    def flatten(self, head):
        """
        :type head: Node
        :rtype: Node
        遇到 curr.child時，用n0(curr), n1(curr.next), 的方法，把當前點和下一點記錄下來。dfs回傳最末一個不為null的點(end)。
        將end與n1, n0對接。n1有可能為null, 所以n1.prev = end之前要判斷一下
        """
        if not head:
            return
        dummy = Node(0, None, None, None)
        dummy.next = head
        curr = head
        self.dfs(head)
        return dummy.next

    def dfs(self, curr):
        if not curr:
            return
        prev = None

        while curr:
            if curr.child:
                n0 = curr
                n1 = n0.next

                end = self.dfs(curr.child)

                n0.next = curr.child
                n0.child.prev = n0
                n0.child = None
                end.next = n1
                if n1:
                    n1.prev = end
            prev = curr
            curr = curr.next

        return prev


class Solution_while(object):
    def flatten(self, head):
        """
        :type head: Node
        :rtype: Node
        """

        p = head
        while p:
            if p.child is None:
                p = p.next
                continue

            tmp = p.child
            while tmp and tmp.next:
                tmp = tmp.next
            tmp.next = p.next
            if p.next:
                p.next.prev = tmp
            p.next = p.child
            p.child.prev = p
            p.child = None
        return head


class Solution(object):
    def __init__(self):
        self.prev = ListNode(0)
    def flatten(self, head):
        """
        :type head: Node
        :rtype: Node
        """
        if not head:
            return head

        if not head.next and not head.child:
            return head

        head, tail = self.helper(head)

        return head

    def helper(self, head):
        if not head:
            return None, None

        if head and head.next is None and head.child is None:
            return head, head

        curr = head
        prev = None

        while curr:

            if curr.child:

                nxt = curr.next
                curr.child.prev = curr
                curr.next = curr.child
                child_head, tail = self.helper(curr.child)
                curr.child = None

                if nxt:
                    tail.next = nxt
                    nxt.prev = tail

            prev = curr
            curr = curr.next

        # print(head.val, prev.val)
        return head, prev

arr1 = [1, 2, 3, 4, 5, 6]
arr2 = [7, 8, 9]
arr3 = [11, 12]
util = DoubleLinkedListUtil()
h1 = util.list2doublyLinkedList(arr1)
h2 = util.list2doublyLinkedList(arr2)
h3 = util.list2doublyLinkedList(arr3)

h1.next.next.child = h2
h2.next.child = h3

a = Solution().flatten(h1)
util.printLinkedList(a)
# print(util.verifyDoubleLinkedList(a))

