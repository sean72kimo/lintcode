class Solution:
    """
    n time
    n space
    """
    def uniqueOccurrences(self, arr: List[int]) -> bool:
        counter = collections.Counter(arr)
        seen = set()
        for num, cnt in counter.items():
            if cnt in seen:
                return False
            seen.add(cnt)

        return True


class SolutionConstant:
    """
    by keeping the count in the original input arr
    數組排序
    循環arr，計算當前數字出現幾次，如果是重複數字，數過已經無用，那就歸零
    第一次循環後，arr數組已經從原先的輸入轉換成每個數字出現幾次，類似一個count arr
    對這個count arr進行排序，檢查是否有重複的orrueenece
    """
    def uniqueOccurrences(self, arr: List[int]) -> bool:
        """constant space"""
        arr.sort()
        n = len(arr)
        count = 1
        for i in range(1, n):
            if arr[i] == arr[i - 1]:
                count += 1
                arr[i - 1] = 0
            else:
                arr[i - 1] = count
                count = 1

        arr[-1] = count

        arr.sort()
        for i in range(n - 1):
            if arr[i] == 0:
                continue
            if arr[i] == arr[i + 1]:
                return False
        return True

class SolutionConstant:
    """
    nlogn time
    constant space
    """
    def uniqueOccurrences(self, arr: List[int]) -> bool:

        size = 2001
        counter = [0] * size

        for n in arr:
            n = n + 1000
            counter[n] += 1

        counter.sort()
        for i in range(len(counter) - 1):
            if counter[i] == 0:
                continue
            if counter[i] == counter[i + 1]:
                return False
        return True
