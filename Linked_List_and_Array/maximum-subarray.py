from sys import maxsize
class Solution:
    """
    @param nums: A list of integers
    @return: An integer denote the sum of maximum subarray
    """
    def maxSubArray(self, nums):
        if nums is None or len(nums) == 0:
            return 0

        maxSum = -maxsize
        sum = 0
        minSum = 0

        for num in nums:
            sum += num
            maxSum = max(maxSum, sum - minSum)
            minSum = min(minSum, sum)

        return maxSum

    def maxSubArray2(self, nums):
        # write your code here
        if nums is None or len(nums) == 0:
            return 0

        maxSum = nums[0]
        minSum = 0
        sum = 0

        for num in nums:
            sum += num
            if sum - minSum > maxSum:
                maxSum = sum - minSum
            if sum < minSum:
                minSum = sum
        return maxSum


nums = [-2, 2, -3, 4, -1, 2, 1, -5, 3]
nums = [1, 2, 3, 4, 5]
# nums = [-1]
print(Solution().maxSubArray3(nums))

