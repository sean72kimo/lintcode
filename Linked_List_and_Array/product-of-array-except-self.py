class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """

        """
        1.開兩個長度同nums的array, f and g
        2.i >= 1,  f[i] = 除了nums[i]以外，左邊所有數的乘積 , ie f[i] = nums[0]*num[1]*...*nums[i-1]
        3.i <= n-2 g[i] = 除了nums[i]以外，右邊所有數的乘積 , ie g[i] = nums[n-1]*num[n-2]*...*nums[i+1]
        4.ans[i] = 左半(沒有包含nums[i]) * 右半(沒有包含nums[i]) = f[i] * g[i]
        """
        n = len(nums)
        f = [1] * n
        g = [1] * n

        for i in range(1, n):
            f[i] = nums[i - 1] * f[i - 1]

        for i in range(n - 2, -1, -1):
            g[i] = nums[i + 1] * g[i + 1]

        ans = []
        for i in range(n):
            ans.append(f[i] * g[i])

        return ans

    def productExceptSelf_constan_space(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        n = len(nums)
        f = [1] * 2
        g = [1] * 2

        ans = [1] * n
        for i in range(n):
            if i == 0:
                continue
            f[i % 2] = nums[i - 1] * f[(i - 1) % 2]
            ans[i] = f[i % 2] * ans[i]

        for i in range(n - 1, -1, -1):
            if i == n - 1:
                continue
            g[i % 2] = nums[i + 1] * g[(i + 1) % 2]
            ans[i] = g[i % 2] * ans[i]

        return ans

