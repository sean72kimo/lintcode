class Solution(object):
    def removeDuplicates(self, A):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not A:
            return A

        j = 0
        duplicate = 0

        for i in range(1, len(A)):
            print(A, i, j, duplicate)
            if A[i] != A[j] :
                j += 1
                A[j] = A[i]
                duplicate = 0

            elif A[i] == A[j] and duplicate < 1 :
                j += 1
                A[j] = A[i]
                duplicate += 1

            else:
                duplicate += 1




        return j + 1



def main():
    A = [1, 1, 1, 2, 2, 3]
    A = [1, 2, 3, 3, 3, 3, 3, 3, 4, 5]
    a = Solution().removeDuplicates(A)
    print("ans:", a, A[:a])

if __name__ == "__main__":
    main()
