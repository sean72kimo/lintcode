"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param two ListNodes
    @return a ListNode
    """

    def mergeTwoLists(self, l1, l2):
        dummy = ListNode(0)
        tmp = dummy
        
        while l1!=None and l2!=None:
            if l1.val < l2.val:
                tmp.next = l1
                l1 = l1.next
            else:
                tmp.next = l2
                l2 = l2.next
            tmp = tmp.next

            
        if l1!=None:
            tmp.next = l1
        else:
            tmp.next = l2

        return dummy.next

class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next


def printNodeList(node):
    head = node
    while node != None:
        print(node.val, end='->')
        node = node.next
        if node == head:
            break
    print('')

node1 = ListNode(1)
node2 = ListNode(2)
node3 = ListNode(3)
node4 = ListNode(4)
node5 = ListNode(5)
node6 = ListNode(6)

node1.next = node2
node2.next = node3
node3.next = None

node4.next = node5
node5.next = node6
node6.next = None

printNodeList(node1)
printNodeList(node4)
sol = Solution()
ans = sol.mergeTwoLists2(node1, node4)
printNodeList(ans)

