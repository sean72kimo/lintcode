class Solution(object):
    def get_num(self, head):
        n = 0
        while head:
            n = n * 10 + head.val
            head = head.next
        return n
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        1. same idea as add-two-numbers-i
        2. 先把l1, l2兩個list所組成的數字算出來 n1 , n2
        3. num = n1 + n2, 將num轉成list。注意corner case: num = 0
        """
        if not l1 and not l2:
            return
        n1 = self.get_num(l1)
        n2 = self.get_num(l2)
        num = n1 + n2
        if num == 0:
            return ListNode(0)
        dummy = ListNode(0)
        head = dummy
        while num:
            tmp = head.next
            node = ListNode(num % 10)
            head.next = node
            node.next = tmp
            num = num // 10


        return dummy.next
