from lib.linkedlist import ListNode, list2LinkedList, printLinkedList

"""
1 將鏈表改為環
2 計算總長度以及需要移動的步數 k (也許k > len(list),取 %。也許k==0 or 取模後為零, 代表不需要移動)
3 從dummy開始走k步，停在分割前一點
4 斷開分割點

similar to 19. Remove Nth Node From End of List 
但是19不需要取模
"""
class Solution(object):
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        dummy = ListNode(0)
        curr = dummy
        dummy.next = head
        size = 0
        while curr and curr.next:
            size += 1
            curr = curr.next
        curr.next = head
        end = curr

        k = (size - k) % size

        curr = dummy
        for i in range(k):
            curr = curr.next

        if curr == dummy:
            curr = end

        ans = curr.next
        curr.next = None
        return ans

arr = [1,2,3,4,5]
k = 5

arr = [1,2]
k = 2

head = list2LinkedList(arr)
a = Solution().rotateRight(head, k)
print("ans:")
printLinkedList(a)

class Solution1(object):
    def rotateRight(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        if not head:
            return
        if k == 0:
            return head
        dummy = ListNode(0)
        dummy.next = head
        curr = head
        leng = 0


        while curr:
            leng += 1
            curr = curr.next

        k = k % leng
        m = abs(leng - k)

        if k == 0:
            return head

        curr = head
        for i in range(m - 1):
            curr = curr.next


        n1 = curr.next
        curr.next = None

        dummy.next = n1

        while n1.next:
            n1 = n1.next
        n1.next = head


        return dummy.next

