class Solution_AC(object):
    def prisonAfterNDays(self, cells, N):
        """
        :type cells: List[int]
        :type N: int
        :rtype: List[int]
        1 頭尾兩個cell一定是0, 中間六個cell頂多只有 2^6種變化
        2 seen = {str(cell) : which n} 某種cell的情況在第n天出現過
        3 N = N % (seen[str(cells)] - N):  n是由大至小推導，(pre_seen_at_n - curr_n) 推出某隔x天便得到一個循環
        N = N % x 快速的將N往0逼近
        https://leetcode.com/problems/prison-cells-after-n-days/discuss/205684/JavaPython-Find-the-Loop-or-Mod-14
        """
        if N == 0:
            return cells

        seen = {str(cells): N}
        while N:
            seen[str(cells)] = N
            N -= 1

            cells2 = [0] * 8
            for i in range(1, 7):
                cells2[i] = 1 if cells[i - 1] == cells[i + 1] else 0
            cells = cells2

            if str(cells) in seen:
                steps = seen[str(cells)] - N
                print(N, steps, N%steps)
                N = N % steps
                print(N)



        return cells

class Solution_TLE(object):
    def prisonAfterNDays(self, cells, N):
        """
        :type cells: List[int]
        :type N: int
        :rtype: List[int]
        """
        K = 0
        vst = {}
        while K < N:
            t = tuple(cells)
            if t in vst:
                cells = list(vst[t])
            else:
                pre = tuple(cells[:])
                nxt = self.next_day(cells)
                vst[pre] = tuple(nxt)
            K += 1
        print(len(vst))
        return cells

    def next_day(self, cells):
        for i in range(1, len(cells) - 1):
            if cells[i - 1] & 0b01 == cells[i + 1] & 0b01:
                cells[i] = 3 if cells[i] & 0b01 == 0b01 else 2
            else:
                cells[i] = 0 if cells[i] & 0b01 == 0b00 else 1

        cells[0] = 0
        cells[-1] = 0

        for i in range(len(cells)):
            cells[i] = 1 if cells[i] & 0b10 == 0b10 else 0
        return cells

cells = [0, 1, 0, 1, 1, 0, 0, 1]
N = 10

cells = [1,0,0,1,0,0,1,0]
N = 1000
exp = [0,0,1,1,1,1,1,0]

sol = Solution_AC()
a = sol.prisonAfterNDays(cells, N)
print(a)
