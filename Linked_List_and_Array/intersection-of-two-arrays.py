class Solution:

    """
    @param: nums1: an integer array
    @param: nums2: an integer array
    @return: an integer array
    """
    def intersection(self, nums1, nums2):
        # write your code here
        return set(nums1) and set(nums2)

nums1 = [1, 2, 2, 1]
nums2 = [2, 2]
a = Solution().intersection(nums1, nums2)
print("ans:", a)
