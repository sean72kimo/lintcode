class Solution(object):
    def merge(self, nums1, m, nums2, n):
        
        if m == 0:
            nums1[:] = nums2[:]
        if n == 0:
            return
            
        idx1 = m-1
        idx2 = n-1
        i = len(nums1)-1
        
        while idx1 >= 0 and idx2 >= 0:
            if nums1[idx1] > nums2[idx2]:
                nums1[i] = nums1[idx1]
                idx1 -= 1
                i -=1
            else:
                nums1[i] = nums2[idx2]
                idx2 -= 1
                i -=1

nums1 = [0]
m = 0
nums2 = [1]
n = 1
Solution().merge(nums1, m, nums2, n)
print(nums1)