class Solution(object):
    def generate(self, num_rows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        ans = []

        for i in range(num_rows):
            row = [None for _ in range(i + 1)]
            row[0], row[-1] = 1, 1

            for j in range(1, len(row) - 1):
                row[j] = ans[i - 1][j - 1] + ans[i - 1][j]

            ans.append(row)

        return ans


Solution().generate(5)


a = [1, 3, 3, 1, 0]
# b = [1, 3, 3, 1]
# b.insert(0, 0)
# x = [ x + y for x, y in zip(a, b)]
# print(x)

