from lib.linkedlist import list2LinkedList, printLinkedList, ListNode

class Solution(object):
    def partition(self, head, x):
        """
        :type head: ListNode
        :type x: int
        :rtype: ListNode
        """

        curr = head

        d1 = ListNode(0)
        lt = d1
        d2 = ListNode(0)
        ge = d2

        while curr:
            if curr.val < x:
                lt.next = curr
                lt = lt.next
            else:
                ge.next = curr
                ge = ge.next

            tmp = curr.next
            curr.next = None
            curr = tmp

        lt.next = d2.next
        return d1.next


data = [4,3,2,5,2]
x = 1
head = list2LinkedList(data)
sol = Solution()
node = sol.partition(head, x)
printLinkedList(node)