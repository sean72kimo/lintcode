class Solution(object):
    def shortestWordDistance(self, words, word1, word2):
        """
        :type words: List[str]
        :type word1: str
        :type word2: str
        :rtype: int
        """
        n = len(words)
        i1 = -1
        i2 = -1
        mind = n

        for i, w in enumerate(words):
            w = words[i]

            if w == word1:
                i1 = i

            if w == word2:
                if word1 == word2:
                    i1 = i2

                i2 = i

            if i1 != -1 and i2 != -1:
                mind = min(mind, abs(i1 - i2))


        return mind

words = ["a", "c", "a", "a"]
word1 = "a"
word2 = "a"
ans = Solution().shortestWordDistance(words, word1, word2)
print('ans:', ans)
