import collections
class Solution(object):
    def countCornerRectangles(self, grid):
        count = collections.Counter()
        ans = 0
        for r, row in enumerate(grid):
            for c1, v1 in enumerate(row):
                if v1 == 0:
                    continue
                
                for c2 in range(c1+1, len(row)):
                    
                    if row[c2]:
                        
                        ans += count[c1, c2]
                        print(r, (c1, c2), ans)
                        count[c1, c2] += 1
        
        return ans
    
class Solution(object):
    def countCornerRectangles(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) == 0:
            return 0

        m = len(grid)
        n = len(grid[0])
        ans = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 0: continue

                for p in range(i+1, m):
                    for q in range(j+1, n):
                        if grid[p][q] == 0: continue

                        if grid[i][q] and grid[p][j]:
                            ans += 1
        return ans

grid = [[1, 0, 0, 1, 0],
 [0, 0, 1, 0, 1],
 [0, 0, 0, 1, 0],
 [1, 0, 1, 0, 1]
]
a = Solution().countCornerRectangles(grid)
print("ans:", a)