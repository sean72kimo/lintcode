import json
# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):

    def reverse(self, head):
        prev = None

        while head:
            print(head.val)
            tmp = head.next
            head.next = prev
            prev = head
            head = tmp

        return prev


    def isPalindrome(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        if not head:
            return True
        rev = None
        slow = head
        fast = head.next

        # find mid
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next


        mid = slow
        print(slow.val)

        # reverse second half
        p2 = self.reverse(mid.next)
        p1 = head

        while p1 and p2 and p1.val == p2.val:
            p1 = p1.next
            p2 = p2.next

        return p2 is None


def stringToListNode(input):
    # Generate list from the input
    numbers = json.loads(input)

    # Now convert that list into linked list
    dummyRoot = ListNode(0)
    ptr = dummyRoot
    for number in numbers:
        ptr.next = ListNode(number)
        ptr = ptr.next

    ptr = dummyRoot.next
    return ptr

def main():
    import sys
    def readlines():
        myinput = ["[1,2,4,2,1]"]
        for line in myinput:
            yield line.strip('\n')
    lines = readlines()
    while True:
        try:
            line = next(lines)
            head = stringToListNode(line)

            ret = Solution().isPalindrome(head)

            out = (ret)
            print(out)
        except StopIteration:
            break

if __name__ == '__main__':
    main()
