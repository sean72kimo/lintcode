import collections
class Solution(object):
    def findK(self, A, k):
        cands = collections.defaultdict(int)
        for i, n in enumerate(A):
            print(n,cands)
            if n in cands:
                cands[n] += 1
            else:
                if len(cands) == k - 1:
                    self.allCandsMinusOne(cands)
                else:
                    cands[n] += 1
        
        ans=[]
        for key, v in cands.items():
            if v > k/n:
                ans.append(key)
        return ans


    def allCandsMinusOne(self, cands):
        tmp = []
        for k,v in cands.items():
            cands[k] -= 1
            if cands[k] == 0:
                tmp.append(k)
        for k in tmp:
            cands.pop(k)

        
        
A = [1 ,2 ,3 ,3 ,5 ,2 ,2 ,3 ,3 ,3 ,5 ,6 ,2 ,2 ,2 ,3 , 3]
k = 3
A = [3, 1, 2, 2, 2, 1, 4, 3, 3]
k = 4
a = Solution().findK(A, k)
print(a)
