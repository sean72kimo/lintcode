# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
from lib.linkedlist import ListNode, list2LinkedList, printLinkedList


class Solution:
    def deleteDuplicatesUnsorted(self, head: ListNode) -> ListNode:
        dummy = ListNode()
        curr = head
        dummy.next = head
        vst = set()
        remove = set()
        while curr:
            if curr.val in vst:
                remove.add(curr.val)
            vst.add(curr.val)
            curr = curr.next

        curr = head
        prev = dummy

        while curr:
            if curr.val in remove:
                tmp = curr.next
                prev.next = curr.next
                curr.next = None
                curr = tmp
                continue

            prev = curr
            curr = curr.next

        return dummy.next

nums = [1,2,3,2]
node = list2LinkedList(nums)
printLinkedList(node)
