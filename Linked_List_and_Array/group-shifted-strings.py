import collections

"""
把每個string編碼。
  編碼方式：s[i]和s[i-1]差值，%26 (diff = s[i] - s[i-1], diff += 26 左移26位，因為可能差值有負數，例如ba
dict = {code: [list of strings with same code]}
"""
class Solution(object):
    def groupStrings(self, strings):
        """
        :type strings: List[str]
        :rtype: List[List[str]]
        """
        ans = []
        mp = collections.defaultdict(list)
        for s in strings:
            tmp = []
            for i in range(1, len(s)):
                diff = ord(s[i]) - ord(s[i - 1])
                d = str((diff + 26)% 26)
                tmp.append(d)
            code = '.'.join(tmp)
            mp[code].append(s)

        for k, v in mp.items():
            ans.append(v)

        return ans

strings = ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"]
strings = ["az","ba"]
# strings = ["a", "bc", "d"]
# strings = ["a", "a"]
a = Solution().groupStrings(strings)
print("ans:")
for r in a:
    print(r)


class Solution2(object):
    def groupStrings(self, strings):
        """
        :type strings: List[str]
        :rtype: List[List[str]]
        """
        ascii = collections.defaultdict(list)
        mp = collections.defaultdict(str)
        ans = []

        for string in strings:
            tmp = []

            for ch in string:
                tmp.append(ord(ch) - ord('a'))

            code = ','.join(map(str, tmp))
            ascii[code].append(code)
            mp[code] = string

            for i in range(1, 26):
                shift = self.shift(tmp, i)
                print(string, code, shift)
                if shift in ascii:
                    ascii[shift].append(code)
                    if code in ascii:
                        ascii.pop(code)

        for v in ascii.values():
            tmp = []
            for code in v:
                tmp.append(mp[code])
            ans.append(tmp)
        return ans


    def shift(self, tmp, i):
        shift = []
        for code in tmp:
            code += i
            code = code % 26
            shift.append(str(code))
        return ','.join(shift)


        return ans
# strings = ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"]
# # strings = ["a", "bc", "d"]
# # strings = ["a", "a"]
# a = Solution().groupStrings(strings)
# print("ans:")
# for r in a:
#     print(r)
