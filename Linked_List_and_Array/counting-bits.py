class Solution(object):
    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        # ans = []
        for n in range(num + 1):
            b = '{0:04b}'.format(n)
            print(n, b, b.count('1'))
        #     how_many_ones = b.count('1')
        #     ans.append(how_many_ones)


        result = [0]
        for i in range(1, num + 1):
            if i & 1:
                print('i&1', i, i & 1)
                ones = result[i >> 1] + 1
            else:
                print(i, i & 1)
                ones = result[i >> 1]
            result.append(ones)
        return result

num = 10
Solution().countBits(num)
