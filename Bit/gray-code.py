class Solution(object):
    def grayCode(self, n):
        results = [0]
        for i in range(n):
            for x in reversed(results):
                x = x + pow(2, i)

                print('{0:04b}, {1}'.format(x, x))

            results += [x]

        return results


Solution().grayCode(3)
