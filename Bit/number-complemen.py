class Solution(object):
    def findComplement(self, num):
        """
        :type num: int
        :rtype: int
        """
        print(format(num, 'b'))
        i = 1
        while i <= num:
            i = i << 1

        c = (i - 1) ^ num
        
        return c
n = 5
a = Solution().findComplement(n)
print("ans:")
print("input: ", "{0:0b}".format(n))
print("output:", "{0:0b}".format(a))

max_int = (1<<(31))-1
print(max_int, "{0:0b}".format(max_int))


print(n)
b = max_int ^ n
print(format(b,'b'))