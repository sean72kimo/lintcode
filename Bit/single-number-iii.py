class Solution:
    """
    @param A: An integer array
    @return: An integer array

    將整群數字分成兩組a[0], a[1]。每組中，各有一個single number, x and y。
    如何分組？
    1. 先將所有數字XOR起來，出現兩次的數字全部消除，XOR運算的結果為兩個single number XOR each other = k
       k = x xor y
    2. k中，出現1的位元，則代表x,y中，此位元不相等 (若位元相等，XOR會成0)
    3. 用lowbit找出右邊數來第一個不相等的bit當作分離兩數組的條件
    4. nums 中所有數字和lowbit做XOR, 看看是哪一組, 分別丟到a[0] or a[1]
    5. a[0] or a[1]裡面各自倆倆重複的數字會被消除，剩下single number, x and y
    """

    def singleNumberIII(self, A):
        # write your code here
        n = len(A)
        if n == 0:
            return []
        k = 0
        for i in range(n):
            k = k ^ A[i]

        lowbit = k & (-k)
        ans = [0, 0]
        for num in A:
            if lowbit & num:
                ans[0] ^= num
            else:
                ans[1] ^= num
        return ans


A = [1, 2, 3, 3, 2, 7, 1, 5]
a = Solution().singleNumberIII(A)
print("ans:", a)

