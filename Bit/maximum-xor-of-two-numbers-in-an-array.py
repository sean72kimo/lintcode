class Solution(object):
    def findMaximumXOR(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if nums is None or len(nums) == 0:
            return 0

        res = 0
        mask = 0
        for i in range(4, -1, -1):
            mask = mask | 1 << i
            myset = set()
            print('===================== bit {0} mask:{1}'.format(i, format(mask, '05b')))
            for n in nums:
                print("num:{0}  {1}".format(n, format(n, '05b')))
                v = n & mask
                print("v:{0}  {1}".format(v, format(v, '05b')), end = '')
                if v not in myset:
                    print("..add to myset")
                    myset.add(v)
                else:
                    print('')

                print("[", end = '')
                for s in myset:
                    print(format(s, '05b'), end = ', ')
                print(']')


            tmp = res | 1 << i
            for prefix in myset:
                if (tmp ^ prefix) in myset:
                    res = tmp
                    print("tmp:", tmp, "res:", res)
                    break

        return res


class Solution2(object):
    def findMaximumXOR(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if nums is None or len(nums) == 0:
            return 0

        res = 0
        mask = 0
        for i in range(5, -1, -1):
            mask = mask | 1 << i
            myset = set()

            for n in nums:

                print("num:{0}  {1}".format(n, format(n, '05b')))

                v = n & mask
                if v not in myset:
                    myset.add(n & mask)

                    print("v:{0}  {1} add to set".format(v, format(v, '05b')))

                    for i in myset:
                        print(format(i, '05b'), end = ', ')
                    print('')


            tmp = res | 1 << i
            for prefix in myset:
                if (tmp ^ prefix) in myset:
                    res = tmp
                    break

        return res

nums = [3, 5, 25, ]
a = Solution().findMaximumXOR(nums)
print('ans:', a)
