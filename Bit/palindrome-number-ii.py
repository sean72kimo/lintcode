class Solution:
    """
    @param n: non-negative integer n.
    @return: return whether a binary representation of a non-negative integer n is a palindrome.
    """
    def isPalindrome(self, n):
        num = format(n, 'b')
        return num == num[::-1]



    def isPalindrome2(self, num):
        # Write your code here


        if num == 0:
            return True

        length = 0

        n = num
        while n:
            length += 1
            n = n // 2

        d = length
        while d:
            p, q = 0, 0
            left = (1 << (d - 1))
            right = 1 << length - d
            d -= 1

            if left & num > 0 :
                p = 1

            if right & num > 0:
                q = 1

            if p != q:
                return False

            while d == d // 2:
                break
        return True

a = Solution().isPalindrome2(4)
print("ans:", a)
