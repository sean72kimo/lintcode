class Solution(object):

    def getSum(self, a, b):
        """
        :type a: int
        :type b: int
        :rtype: int
        """
        MAX = 2 ** 31 - 1
        MIN = -2 ** 32

        if b == 0:
            return a
        addup = a ^ b
        carry = (a & b) << 1
        return self.getSum(addup, carry)

