
class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int


        1.每個數字由二進位 32 bits 來表示，我們宣告三個變數 ones twos threes 分別記錄每個位元出現的次數， 逐條解釋 for loop 裡的程式，

        twos |= ones & nums[i]; ：先用 & 取出 ones 和nums[i] 中同為 1 的位元，會出現代表這些位元出現兩次，
        再跟 twos 進行 OR 運算，把出現兩次的位元記錄在 twos 裡

        ones ^= nums[i]; ：第一次出現的數字跟 ones 進行 XOR，把出現兩次的位元會清成 0，

        threes = ones & twos; threes 則是取出 ones 跟 twos 中同為 1 的位數，
        ones &= ~threes; 跟 twos &= ~threes; 都是已經把出現三次的存在 three 裡了，所以把 ones 跟 twos 的清掉，
        """
        if not any(nums):
            return

        ones = 0
        twos = 0
        threes = 0

        for i in range(len(nums)):
            twos |= ones & nums[i]
            ones ^= nums[i]
            threes = ones & twos
            ones &= ~threes
            twos &= ~threes

        return ones
nums = [2,3,2,2]
a = Solution().singleNumber(nums)
print(a)
