import collections


def get_rank(arr):
    sorted_arr = sorted(arr)
    r = 0
    rank = collections.defaultdict(int)
    for n in sorted_arr:
        rank[n] = r
        r += 1
    ans = []
    for n in arr:
        ans.append(rank[n])
    return ans