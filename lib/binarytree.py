"""
Definition of TreeNode:
"""


class TreeNode:

    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
        self.next = None

    def __repr__(self):
        return "<TreeNode {}>".format(self.val)


class TreeLinkNode:

    def __init__(self, x, left=None, right=None, next=None):
        self.val = x
        self.left = left
        self.right = right
        self.next = next

    def __repr__(self):
        return "<TreeLinkNode {}>".format(self.val)


root = TreeNode(3)
node2 = TreeNode(9)
node3 = TreeNode(20)

node4 = TreeNode(15)
node5 = TreeNode(7)

root.left = node2
root.right = node3
node3.left = node4
node3.right = node5


class BinaryTree:

    def __init__(self, tree_node_type=TreeNode):
        self.node = tree_node_type
        self.bst = '[8,3,10,1,6,9,14,null,null,4,7,null,null,13,null,null,5]'

    def same_tree(self, p, q):
        if p is None and q is None:
            return True
        if p is None or q is None:
            return False
        if p.val != q.val:
            return False
        left = self.same_tree(p.left, q.left)
        right = self.same_tree(p.right, q.right)
        return left and right

    def valid_bst(self, root):
        return self.validHelper(root, float('-inf'), float('inf'))

    def validHelper(self, root, mn, mx):
        if not root:
            return True

        if root.val <= mn or root.val >= mx:
            return False

        left = self.validHelper(root.left, mn, root.val)
        right = self.validHelper(root.right, root.val, mx)

        return left and right

    '''
    @param root: An object of TreeNode, denote the root of the binary tree.
    This method will be invoked first, you should design your own algorithm
    to serialize a binary tree which denote by a root node to a string which
    can be easily deserialized by your own "deserialize" method later.
    '''

    def serialize(self, root):
        # write your code here
        res = []
        if root is None:
            return "[]"

        queue = [root]
        res.append(str(root.val))

        while queue:
            new_q = []

            for node in queue:

                if node.left:
                    new_q.append(node.left)
                    res.append(str(node.left.val))
                else:
                    res.append('#')

                if node.right:
                    new_q.append(node.right)
                    res.append(str(node.right.val))
                else:
                    res.append('#')

            queue = new_q

        while res[-1] == '#':
            res.pop()

        ans = '[' + ','.join(res) + ']'
        return ans

    '''
    @param data: A string serialized by your serialize method.
    This method will be invoked second, the argument data is what exactly
    you serialized at method "serialize", that means the data is not given by
    system, it's given by your own serialize method. So the format of data is
    designed by yourself, and deserialize it here as you serialize it in
    "serialize" method.
    '''

    def deserialize(self, data):
        # write your code here
        if data == '[]' or data is None:
            return None
        data = data.rstrip()
        values = data[1:-1].split(',')

        root = self.node(int(values[0]))
        queue = [root]
        isLeftChild = True
        index = 0
        null = ['#', 'null']
        for val in values[1:]:
            if val not in null:
                node = self.node(int(val))
                if isLeftChild:
                    queue[index].left = node
                else:
                    queue[index].right = node
                queue.append(node)

            if isLeftChild == False:
                index += 1

            isLeftChild = not isLeftChild

        return root

    def drawtree(self, root):

        def height(root):
            return 1 + max(height(root.left), height(root.right)) if root else -1

        def jumpto(x, y):
            t.penup()
            t.goto(x, y)
            t.pendown()

        def draw(node, x, y, dx):
            if node:
                t.goto(x, y)
                jumpto(x, y - 20)
                t.write(node.val, align='center', font=('Arial', 12, 'normal'))
                draw(node.left, x - dx, y - 60, dx / 2)
                jumpto(x, y - 20)
                draw(node.right, x + dx, y - 60, dx / 2)

        import turtle
        t = turtle.Turtle()
        t.speed(0); turtle.delay(0)
        h = height(root)
        jumpto(0, 30 * h)
        draw(root, 0, 30 * h, 40 * h)
        t.hideturtle()
        turtle.mainloop()


if __name__ == '__main__' :

    sol = BinaryTree()
    ans = sol.serialize(root)

    data = '[3,9,20,#,#,15,7]'
    data = '[3,9,8,4,0,1,7,null,null,null,2,5]'

    root = sol.deserialize(data)
    sol.drawtree(root)
    string = sol.serialize(root)
    print(string == data)
