class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []




class CreateGraph:
    def __init__(self):
        self.all_nodes = []

    def create(self, test):
        connected = test.split('#')


        for conn in connected:
            nodes = conn.split(',')
            # convert all str num to int num
            nodes = list(map(int, nodes))

            root = self.is_created(nodes[0])
            if not root:
                root = UndirectedGraphNode(nodes[0])
                self.all_nodes.append(root)

            for i in range(1, len(nodes)):
                nei = self.is_created(nodes[i])

                if not nei:
                    nei = UndirectedGraphNode(nodes[i])
                    self.all_nodes.append(nei)

                root.neighbors.append(nei)
        return self.all_nodes


    def is_created(self, label):
        for n in self.all_nodes:
            if n.label == label:
                return n
        return None

    # print
    def print_nodes(self, all_nodes):
        for node in all_nodes:
            nei = []
            for n in node.neighbors:
                nei.append(n.label)
            print(node.label, 'nei', nei)
        print('=================\n\n\n')
