class Point:
    def __init__(self, a = 0, b = 0):
        self.x = a
        self.y = b
    def __repr__(self):
        return "P[{},{}]".format(self.x, self.y)

def getPoint(arr = None, n = None):

    points = []
    if n is not None:
        for i in range(n):
            points.append(Point(i, i))
        return points

    for x, y in arr:
        points.append(Point(x, y))
    return arr
