class ListNode(object):

    def __init__(self, x, nxt = None):
        self.val = x
        self.next = nxt

    def __repr__(self):
        return "<N{}>".format(str(self.val))


def printLinkedList(head):
    while head and head.next:
        print(head.val, end=' -> ')
        head = head.next
    print(head.val)


def list2LinkedList(arr):

    head = ListNode(arr[0])
    dummy = ListNode(0)
    dummy.next = head
    for idx, val in enumerate(arr[1:], 1):
        head.next = ListNode(val)
        head = head.next

    # print("[ll created]: ", end='')
    # printLinkedList(dummy.next)
    return dummy.next


if __name__ == '__main__':
    linkedlist = [-10, -3, 1, 5, 9]
    head = list2LinkedList(linkedlist)
    printLinkedList(head)
    dummy = ListNode(0)
    curr = dummy
    curr.next = head
    curr = curr.next
    print(curr)
    print(dummy.next)
