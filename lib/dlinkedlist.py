class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None
        self.prev = None
        self.child = None

    def __repr__(self):
        return str(self.val)

class DoubleLinkedListUtil:
    def verifyDoubleLinkedList(self, head):
        fwd = []
        rev = []
        prev = None
        while head:
            fwd.append(head.val)
            prev = head
            head = head.next

        head = prev
        while head:
            rev.append(head.val)
            prev = head
            head = head.prev

        return fwd == rev[::-1]
        
    def printLinkedList(self, head):
        while head:
            print(head.val, end = ' <-> ')
            head = head.next
        print('None')
        
    def printLinkedListReverse(self, head):
        while head:
            print(head.val, end = ' <-> ')
            head = head.prev
        print('None')
    
    def list2doublyLinkedList(self, arr):
        head = ListNode(arr[0])
        dummy = ListNode(0)
        dummy.next = head
        prev = None
        for idx, val in enumerate(arr[1:], 1):
            head.prev = prev
            head.next = ListNode(val)
            prev = head
            head = head.next
        head.prev = prev
    
        # print("[dll created]: ", end = '')
        # self.printLinkedList(dummy.next)

        return dummy.next


if __name__ == '__main__':
    linkedlist = [1,2,3,4,5,6]
    dll_util = DoubleLinkedListUtil()
    rt = dll_util.list2doublyLinkedList(linkedlist)
    print(rt)
    rt = dll_util.verifyDoubleLinkedList(rt)
    print(rt)

