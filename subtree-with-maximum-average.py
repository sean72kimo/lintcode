class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


root = TreeNode(1)
node2 = TreeNode(-5)
node3 = TreeNode(11)
node4 = TreeNode(1)
node5 = TreeNode(2)
node6 = TreeNode(4)
node7 = TreeNode(-2)

root.left = node2
root.right = node3

node2.left = node4
node2.right = node5

node3.left = node6
node3.right = node7



class ResultType:
    def __init__(self, node, sum, size):
        self.node = node
        self.sum = sum
        self.size = size

    def avg(self):
        return self.sum / self.size

class Solution:
    # @param {TreeNode} root the root of binary tree
    # @return {TreeNode} the root of the maximum average of subtree
    result = ResultType(None, 0 , 0)
    
    def findSubtree2(self, root):
        # Write your code here
        if root is None:
            return ResultType(None, 0 , 0)
        self.helper(root)
        return self.result
        
    def helper(self, node):
        if node is None:
            n = ResultType(None, 0 ,0)
            return n
        
        leftResult = self.helper(node.left)
        print('[left]',leftResult.sum, leftResult.size)
        rightResult= self.helper(node.right)
        
        treeSum = leftResult.sum + rightResult.sum + node.val
        treeSize = leftResult.size + rightResult.size + 1
        
        currentResult = ResultType(node, treeSum, treeSize) 
        print('[curr]',currentResult.sum, currentResult.size, currentResult.avg())

        if self.result.node is None or currentResult.avg() > self.result.avg(): 
            self.result = currentResult
        
        return self.result


sol = Solution()
ans = sol.findSubtree2(root)
print(ans.node.val)
