"""
類似prefix sum的概念
sliding window
用雙指針計算occurrence
注意！窗口滑動條件有二 while left <= right and prod >= k:
left <= right, 代表就算單只有nums[right]這個元素，也大於等於k, left += 1之後出現在right右邊，代表一個元素都不取
ans += right - left + 1 的計算結果為 0, 不影響答案
"""
class Solution(object):
    def numSubarrayProductLessThanK(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if nums is None or len(nums) == 0 :
            return 0
        if k < 1 :
            return 0

        ans = 0
        left = 0
        prod = 1
        for right , val in enumerate(nums):
            prod = prod * val
            while left <= right and prod >= k:
                prod = prod // nums[left]
                left += 1

            print(nums[left:right + 1])
            ans = ans + right - left + 1

        return ans
nums = [10, 5, 2, 6]
k = 100
nums = [1, 1, 1]
k = 1
a = Solution().numSubarrayProductLessThanK(nums, k)
print("ans:", a)
