class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if not nums or len(nums) == 0:
            return 0
        presum = {0:1}
        sum = 0
        res = 0
        for i in range(len(nums)):
            sum += nums[i]
            diff = sum - k

            if diff in presum:
                res = res + presum[diff]

            presum[sum] = presum.get(sum, 0) + 1


        return res

nums = [1, 1, 1, 1, 1, 1, 1, 1]
k = 2
a = Solution().subarraySum(nums , k)
print('ans:', a)






