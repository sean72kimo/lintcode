"""
先計算prefix sum
共計要有四段，每段之間要有最少1個空格
要切三刀, i,j,k
枚舉中間那一刀, j, 並用一個hashset記錄左邊和右邊出現過哪些subarray sum
  枚舉左邊那一刀, i, 並求第一和第二段各種可能的subarray sum1 and sum2, if sum1==sum2, add to hsset
  枚舉右邊那一刀, k, 並求第三和第四段各種可能的subarray sum3 and sum4, if sum3==sum4 and sum3 in hsset, retun True
"""
class Solution(object):
    def splitArray(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if len(nums) == 0:
            return 0

        pre = [0]
        for n in nums:
            pre.append(pre[-1] + n)

        n = len(nums)
        for j in range(3, n - 3):
            hsset = set()
            for i in range(1, j - 1):
                sum1 = pre[i]
                sum2 = pre[j] - pre[i + 1]
                if sum1 == sum2:
                    hsset.add(sum1)

            for k in range(j + 1, n - 1):
                sum3 = pre[k] - pre[j + 1]
                sum4 = pre[n] - pre[k + 1]
                if sum3 == sum4 and sum3 in hsset:
                    return True
        return False
    
class Solution:
    def splitArray(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        pre = [0]
        for n in nums:
            pre.append(pre[-1] + n)
        n = len(nums)
        for i in range(1, n):
            sum1 = pre[i]
            for j in range(i + 2, n - 3):
                sum2 = pre[j] - pre[i + 1]
                for k in range(j + 2, n - 1):
                    sum3 = pre[k] - pre[j + 1]
                    sum4 = pre[n] - pre[k + 1]
                    print(sum1, sum2, sum3, sum4)
                    if sum1 == sum2 == sum3 == sum4:
                        return True
        return False
nums = [1, 2, 1, 2, 1, 2, 1]
a = Solution().splitArray(nums)
print("ans:", a)
