# https://www.lintcode.com/problem/1844/
from typing import (
    List,
)

class Solution:
    """
    @param nums: a list of integer
    @param k: an integer
    @return: return an integer, denote the minimum length of continuous subarrays whose sum equals to k
    """
    def subarray_sum_equals_k_i_i(self, nums: List[int], k: int) -> int:
        # write your code here
        running_sum = [0]
        for i , n in enumerate(nums):
            running_sum.append(n + running_sum[i])
        print(running_sum)
        ans = float('inf')
        hsmap = {0 : 0}

        for i, n in enumerate(nums):
            v = running_sum[i + 1] - k
            if v in hsmap:
                length = i + 1 - hsmap[v]
                ans = min(length, ans)
            hsmap[running_sum[i + 1]] = i + 1

        return -1 if (ans == float('inf')) else ans



nums = [0,1,-1,4,2,-3]
k = 3
exp = 2

sol = Solution()
a = sol.subarray_sum_equals_k_i_i(nums, k)
print("ans:", a, a == exp)
