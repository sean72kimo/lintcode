"""
525. Contiguous Array
https://leetcode.com/problems/contiguous-array/
將 0 轉為 -1
尋找 summ - x = 0 -> summ = x, find if x in mp
"""
import collections


class Solution(object):
    def findMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        for i in range(len(nums)):
            if nums[i] == 0:
                nums[i] = -1
        mp = collections.defaultdict(int)
        mp[0] = -1
        summ = 0
        ans = 0
        for i in range(len(nums)):
            summ += nums[i]
            if summ in mp:
                ans = max(ans, i - mp[summ])

            if summ not in mp: #  因為題目要求最長子串，所以index記錄越左邊越好
                mp[summ] = i
        return ans