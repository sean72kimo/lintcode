import collections


class Solution(object):
    def maxSubArrayLen(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        if len(nums) == 0:
            return 0

        mp = collections.defaultdict(int)
        mp[0] = -1
        summ = 0
        ans = 0
        for i in range(len(nums)):
            summ += nums[i]
            x = summ - k
            if x in mp:
                ans = max(ans, i - mp[x])

            if summ not in mp:
                mp[summ] = i
        return ans