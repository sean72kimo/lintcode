"""
523. Continuous Subarray Sum
https://leetcode.com/problems/continuous-subarray-sum/

1 at least 2, 所以越長越好，第一次出現remain時，記錄下來，以後出現同樣的remain, 捨棄
2 初始 mp[0] = -1
3 corner case,
3.1 k == 0
3.2 k == 0 and [0,0] in nums

"""
class Solution:
    def checkSubarraySum(self, nums, k):
        if len(nums) == 0:
            return False

        if k == 0:
            for i in range(len(nums) - 1):
                if nums[i: i + 2] == [0, 0]:
                    return True
            return False

        summ = 0
        mp = collections.defaultdict(int)
        mp[0] = -1
        for i in range(len(nums)):
            summ += nums[i]

            remain = summ % k
            if remain in mp:
                if i - mp[remain] > 1:
                    return True

            if remain not in mp:
                mp[summ % k] = i
        return False

