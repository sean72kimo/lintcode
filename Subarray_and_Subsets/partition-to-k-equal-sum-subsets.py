"""
time: k*2^n, dfs搜索每一個未使用的元素去構成一個subset
暴力方式是所以每個loop都是從idx 開始搜索當前subset可能的元素，
我們可以用start去幫助我們優化。
目標remain: R,
假設取 nums[2], 下一個可用的元素必定在idx 2之後，所以下一層從i+1開始找。
假設取 nums[2], 將剩下的nums[3:]整排掃完，找不到可以配對的nums[i], 那以後也不可能從 nums[3:]找到一個數，回頭來配nums[2]
如果真能回頭搭配nums[2], 從nums[2]出發的時候就應該可以配到了。所以推得：我們可以用start優化
當完成眼前的 remain R 之後，下一層的目標又改回target T, 我們從頭看看之前沒被選中的元素，start from 0, 看看是否能配成target T

當我們找到一組subset sum為 target之後，下次搜索的start則設定為0從頭開始找尋所有未使用過的元素，拼成另外一組subset
"""
class Solution(object):
    def canPartitionKSubsets(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """

        if sum(nums) % k:
            return False
        target = sum(nums) // k
        vst = set()
        return self.dfs(nums, k, target, target, 0, vst)

    def dfs(self, nums, k, target, remain, start, vst):
        if k == 0:
            return True

        if remain < 0:
            return False

        if remain == 0:
            print(vst)
            return self.dfs(nums, k - 1, target, target, start, vst)

        for i in range(start, len(nums)):
            if i in vst:
                continue

            vst.add(i)
            if self.dfs(nums, k, target, remain - nums[i], i + 1, vst):
                return True
            vst.remove(i)

        return False

nums = [2, 4, 3, 3, 2, 5, 1]
k = 4
# nums = [2, 2, 2, 2, 3, 4, 5]
# k = 4
# nums = [1,1]
# k =2
a = Solution().canPartitionKSubsets(nums, k)
print("ans:", a)


