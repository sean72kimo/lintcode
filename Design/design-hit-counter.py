import collections

"""
如果上一次hits超過三百秒，那麼self.hits 歸零，self.time更新最近一次hits的時間
不然self.hits 累加
"""
class HitCounter:

    def __init__(self):
        self.hits = [0 for _ in range(300)]
        self.time = [0 for _ in range(300)]

    def hit(self, timestamp: int) -> None:

        t = timestamp % 300

        if timestamp - self.time[t] >= 300:
            self.time[t] = timestamp
            self.hits[t] = 1
        else:
            self.time[t] = timestamp
            self.hits[t] += 1

    def getHits(self, timestamp: int) -> int:
        t = timestamp % 300

        cnt = 0
        for i in range(300):
            if timestamp - self.time[i] < 300:
                cnt += self.hits[i]
        return cnt


"""
每次get / hit都做清理。清理過去300個slot, 每次都是contstant time clean up
"""
class HitCounter:

    def __init__(self):
        self.counter = collections.Counter()

    def hit(self, timestamp: int) -> None:

        for key in list(self.counter.keys()):
            if timestamp - key >= 300:
                self.counter.pop(key)

        self.counter[timestamp] += 1

    def getHits(self, timestamp: int) -> int:
        for key in list(self.counter.keys()):
            if timestamp - key >= 300:
                self.counter.pop(key)

        return sum(self.counter.values())

class HitCounter(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.hits = [0 for _ in range(300)]
        self.time = [0 for _ in range(300)]

    def hit(self, timestamp):
        """
        Record a hit.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: None
        """
        t = timestamp % 300

        if timestamp - self.time[t] < 300:

            self.hits[t] += 1

            if self.time[t] == 0:
                self.time[t] = timestamp
        else:
            self.hits[t] = 1
            self.time[t] = timestamp
        # print(self.hits)

    def getHits(self, timestamp):
        """
        Return the number of hits in the past 5 minutes.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: int
        """
        t = timestamp % 300
        cnt = 0
        for i in range(300):
            if timestamp - self.time[i] < 300:
                cnt += self.hits[i]
        return cnt




ops = ["HitCounter","hit","hit","hit","getHits","getHits","getHits","getHits","getHits","hit","getHits"]
val = [[],[2],[3],[4],[300],[301],[302],[303],[304],[501],[600]]
exp = [None,None,None,None,3,3,3,2,1,None,1]

ops = ["HitCounter","hit","hit","hit","getHits","hit","getHits","getHits"]
val = [[],[1],[2],[3],[4],[300],[300],[301]]
exp = [None,None,None,None,3,None,4,3]
ans = []
for i, op in enumerate(ops):
    if op == "HitCounter":
        obj = HitCounter()
        ans.append(None)
    elif op == "hit":
        r = obj.hit(val[i][0])
        ans.append(r)
    elif op == "getHits":
        r = obj.getHits(val[i][0])
        ans.append(r)

print(ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break