class TicTacToe(object):
    """
    1.注意! 每一個row / col/ dia/ anti_dia的數值總和，有可能為 +n or -n

    if abs(self.rows[row]) == self.n:
        return player
    """
    def __init__(self, n):
        """
        Initialize your data structure here.
        :type n: int
        """
        self.rows = [0 for _ in range(n)]
        self.cols = [0 for _ in range(n)]
        self.dia = 0
        self.anti_dia = 0
        self.n = n

    def move(self, row, col, player):
        """
        Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins.
        :type row: int
        :type col: int
        :type player: int
        :rtype: int
        """
        if player == 1:
            to_add = 1
        else:
            to_add = -1

        self.rows[row] += to_add
        self.cols[col] += to_add
        if row == col:
            self.dia += to_add
        if row + col == self.n - 1:
            self.anti_dia += to_add

        if self.dia == self.n:
            return 1
        elif self.dia == -self.n:
            return 2

        if self.anti_dia == self.n:
            return 1
        elif self.anti_dia == -self.n:
            return 2

        if self.rows[row] == self.n:
            return 1
        elif self.rows[row] == -self.n:
            return 2

        if self.cols[col] == self.n:
            return 1
        elif self.cols[col] == -self.n:
            return 2

        return 0

ops = ["TicTacToe", "move", "move", "move"]
vals = [[2], [0, 0, 2], [0, 1, 1], [1, 1, 2]]
ans = []
for i, op in enumerate(ops):
    if op == "TicTacToe":
        obj = TicTacToe(vals[0][0])
        ans.append(None)
    elif op == "move":
        r = obj.move(vals[i][0], vals[i][1], vals[i][2])
        ans.append(r)
print(ans)
