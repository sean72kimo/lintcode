import collections


class LogSystem(object):
    def __init__(self):
        self.logs = []

    def put(self, tid, timestamp):
        self.logs.append((tid, timestamp))

    def retrieve(self, s, e, gra):
        index = {'Year': 5, 'Month': 8, 'Day': 11,
                 'Hour': 14, 'Minute': 17, 'Second': 20}[gra]
        start = s[:index]
        end = e[:index]

        return [tid for tid, timestamp in self.logs
                if start <= timestamp[:index] <= end]



ops = ["LogSystem","put","put","put","retrieve","retrieve"]
val = [[],[1,"2017:01:01:23:59:59"],[2,"2017:01:01:22:59:59"],[3,"2016:01:01:00:00:00"],["2016:01:01:01:01:01","2017:01:01:23:00:00","Year"],["2016:01:01:01:01:01","2017:01:01:23:00:00","Hour"]]
exp = [None,None,None,None,[3,1,2],[1,2]]


ans = []
for i, op in enumerate(ops):
    if i == 5:
        break
    if op == "LogSystem":
        obj = LogSystem()
        ans.append(None)

    elif op == "put":
        obj.put(val[i][0], val[i][1])
        ans.append(None)

    elif op == "retrieve":
        r = obj.retrieve(val[i][0],val[i][1],val[i][2])
        ans.append(r)

print('ans:',ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break

