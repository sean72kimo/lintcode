import random
class RandomHashMap:
    def __init__(self):
        self.map = {}
        self.buffer = []

    def add(self, data):
        if data in self.map:
            return False

        buffer.append(data)
        self.map[data] = len(buff) - 1
        return True

    def remove(self, data):
        if data not in self.map:
            return False

        index = self.map[data]
        tmp = self.buffer[-1]
        self.buffer[index], self.buffer[-1] = self.buffer[-1], self.buffer[index]
        self.map[tmp] = index
        self.buffer.pop()
        self.map.pop(data)

        return True

    def removeRandom(self):
        idx = random.randrange(0, len(self.buffer))
        ele = self.buffer[idx]
        self.remove(ele)
        return ele


mp = {2, 4, 5, 1, 3}
print(mp.pop(), mp)


["RandomizedCollection", "insert", "remove", "insert", "remove", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom"]
[[], [0], [0], [-1], [0], [], [], [], [], [], [], [], [], [], []]
