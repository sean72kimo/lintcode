import collections


class Node:
    def __init__(self, key=None, value=None, freq=1):
        self.key = key
        self.value = value
        self.freq = freq
        self.next = None
        self.prev = None


class DLL:
    def __init__(self):
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.prev = self.tail.prev
        node.next = self.tail

        self.tail.prev.next = node
        self.tail.prev = node

    def delete(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev
        del node

    def get_head(self):
        return self.head.next

    def is_empty(self):
        return self.head.next is None


class LFUCache:

    def __init__(self, capacity: int):
        self.min_freq = 0
        self.mp = {}
        self.cap = capacity
        self.freq = collections.defaultdict(DLL)

    def get(self, key: int) -> int:
        if key not in self.mp:
            return -1
        print(key)
        node = self.mp[key]
        freq = node.freq
        value = node.value
        node.freq += 1
        self.freq[freq].delete(node)
        self.freq[freq + 1].append(node)

        if freq == self.min_freq and self.freq[freq].is_empty():
            self.freq.pop(freq)
            self.min_freq += 1

        return value

    def put(self, key: int, value: int) -> None:
        if self.cap == 0:
            return

        if self.get(key) != -1:
            self.mp[key].value = value
            return

        if len(self.mp) == self.cap:
            lfu = self.freq[self.min_freq].get_head()
            self.mp.pop(lfu.key)
            self.freq[self.min_freq].delete(lfu)

        self.min_freq = 1
        node = Node(key, value)
        self.mp[key] = node
        self.freq[node.freq].append(node)


# Your LFUCache object will be instantiated and called as such:
# obj = LFUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)


ops = ["LFUCache", "put", "put", "get", "put", "get", "get", "put", "get", "get", "get"]
val = [[2], [1, 1], [2, 2], [1], [3, 3], [2], [3], [4, 4], [1], [3], [4]]
ans = []
for i, op in enumerate(ops):
    if op == "LFUCache":
        obj = LFUCache(val[i][0])
        ans.append(None)
    elif op == "put":
        ans.append(obj.put(val[i][0], val[i][1]))
    elif op == "get":
        ans.append(obj.get(val[i][0]))
print(ans)
# Your LFUCache object will be instantiated and called as such:
# obj = LFUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
