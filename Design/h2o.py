from threading import Condition
import threading
import time
from queue import Queue
class H2O:
    def __init__(self):
        self.h_cnt = 0
        self.o_cnt = 0
        self.lock = threading.RLock()
        self.h = Condition(self.lock)
        self.o = Condition(self.lock)

    def H(self, who):
        with self.h:
            self.h_cnt += 1
            print('H', (self.h_cnt, self.o_cnt))
            if self.h_cnt < 2 or self.o_cnt < 1:
                print('h wait', who)
                self.h.wait()
            else:
                print('consumed by', who)
                self.h_cnt -= 2
                self.o_cnt -= 1
                self.o.notify()
            print(who, 'end ')

    def O(self, who):
        with self.o:
            self.o_cnt += 1
            print('O', (self.h_cnt, self.o_cnt))
            if self.h_cnt < 2 or self.o_cnt < 1:
                self.o.wait()
            else:
                print('consumed by', who)
                self.h_cnt -= 2
                self.o_cnt -= 1
                self.h.notify_all()
            print(who, 'end ')



h2o = H2O()
class H1(threading.Thread):
    def run(self):

        h2o.H(self)

class H2(threading.Thread):
    def run(self):
        h2o.H(self)

class O(threading.Thread):
    def run(self):
        time.sleep(0.5)
        h2o.O(self)


h1 = H1()
h2 = H2()
o1 = O()

h1.start()
h2.start()
o1.start()
