# class MaxStack2(object):
#
#     def __init__(self):
#         """
#         initialize your data structure here.
#         """
#         self.max = []
#         self.stack = []
#
#     def push(self, x):
#         """
#         :type x: int
#         :rtype: void
#         """
#         self.stack.append(x)
#         if len(self.max):
#             self.max.append(max(self.max[-1], x))
#         else:
#             self.max.append(x)
#
#     def pop(self):
#         """
#         :rtype: int
#         """
#         if len(self.stack) == 0:
#             return
#
#         self.max.pop()
#         return self.stack.pop()
#
#     def top(self):
#         """
#         :rtype: int
#         """
#         if len(self.stack) == 0:
#             return
#         return self.stack[-1]
#
#     def peekMax(self):
#         """
#         :rtype: int
#         """
#         if len(self.max) == 0:
#             return
#         return self.max[-1]
#
#     def popMax(self):
#         """
#         :rtype: int
#         """
#         buf = []
#         mx = self.peekMax()
#         while self.top() != mx:
#             buf.append(self.pop())
#
#         self.pop()
#         while buf:
#             self.push(buf.pop())
#
#         return mx

# Your MaxStack object will be instantiated and called as such:
# obj = MaxStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.peekMax()
# param_5 = obj.popMax()

import heapq


class Node(object):

    def __init__(self, key=None, val=None):
        self.val = val
        self.next = None
        self.prev = None
        self.key = key

    def __lt__(self, other):
        if self.val == other.val:
            return self.key > other.key
        return self.val < other.val

    def __repr__(self):
        return "<Node#{}, {}>".format(self.key, self.val)


class MaxStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.cnt = 0
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head
        self.maxheap = []

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """

        self.cnt += 1
        node = Node(self.cnt, x)

        old = self.head.next
        node.next = old
        node.prev = self.head

        old.prev = node
        self.head.next = node
        heapq.heappush(self.maxheap, (-x, node))
        print("push val", x, end=' ,in list: ')
        self.printStack()

    def pop(self):
        """
        :rtype: int
        """
        node = self.head.next
        tmp = node.next
        tmp.prev = self.head
        self.head.next = tmp

        self.maxheap.remove((-node.val, node))
        heapq.heapify(self.maxheap)

        print("pop val", node.val, end=' ,in list: ')
        self.printStack()
        return node.val

    def top(self):
        """
        :rtype: int
        """
        print("top", end=' ,in list: ')
        self.printStack()
        return self.head.next.val

    def peekMax(self):
        """
        :rtype: int
        """
        return -self.maxheap[0][0]

    def popMax(self):
        """
        :rtype: int
        """

        itm = heapq.heappop(self.maxheap)
        v, node = -itm[0], itm[1]

        a = node.prev
        b = node.next
        a.next = b
        b.prev = a

        print("pop MAX val", node.val, end=' ,in list: ')
        self.printStack()
        return v

    def printStack(self):
        head = self.head.next
        while head and head.next:
            print(head.val, end=' -> ')
            head = head.next
        print(head.val)


ops = ["MaxStack", "push", "push", "push", "top", "popMax", "top", "peekMax", "pop", "top"]
val = [[], [5], [1], [5], [], [], [], [], [], []]
exp = [None, None, None, "hello", "hello", None, "hello", "leet"]

ops = ["MaxStack", "push", "peekMax", "push", "peekMax", "push", "pop", "pop", "push", "peekMax", "push", "popMax", "top", "push", "push", "peekMax", "popMax", "popMax"]
val = [[], [92], [], [54], [], [22], [], [], [-57], [], [-24], [], [], [26], [-71], [], [], []]
exp = [None, None, 92, None, 92, None, 22, 54, None, 92, None, 92, -24, None, None, 26, 26, -24]

ans = []
for i, op in enumerate(ops):
#     print((i, op), val[i])
    if op == "MaxStack":
        obj = MaxStack()
        ans.append(None)
    elif op == "push":
        ans.append(obj.push(val[i][0]))
    elif op == "pop":
        ans.append(obj.pop())
    elif op == "top":
        ans.append(obj.top())
    elif op == "peekMax":
        ans.append(obj.peekMax())
    elif op == "popMax":
        ans.append(obj.popMax())
print("ans:", ans == exp, ans)
