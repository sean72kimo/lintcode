import collections


class Node:
    def __init__(self, k=None, v=None, f=1):
        self.key = k
        self.val = v
        self.freq = f
        self.prev = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.next = self.tail
        node.prev = self.tail.prev
        self.tail.prev.next = node
        self.tail.prev = node

    def remove(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev
        del node

    def is_empty(self):
        return self.head.next is self.tail

    def get_front(self):
        return self.head.next


class LFUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.cap = capacity
        self.mp = {}  # k:v to locate node
        self.freq = collections.defaultdict(DoubleLinkedList)  # each freq is a dll
        self.min_f = 0

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key not in self.mp:
            return -1

        node = self.mp[key]
        old_f = node.freq
        new_f = old_f + 1
        node.freq = new_f

        self.freq[old_f].remove(node)
        self.freq[new_f].append(node)

        if self.min_f == old_f and self.freq[old_f].is_empty():
            self.min_f += 1

        return node.val

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: None
        """
        if self.cap == 0:
            return

        if self.get(key) != -1:
            node = self.mp[key]
            node.val = value
            return

        if len(self.mp) == self.cap:
            lfu = self.freq[self.min_f].get_front()
            self.freq[self.min_f].remove(lfu)
            self.mp.pop(lfu.key)

        node = Node(key, value, 1)
        self.freq[1].append(node)
        self.min_f = 1
        self.mp[key] = node




ops = ["LFUCache", "put", "put", "get", "put", "get", "get", "put", "get", "get", "get"]
val = [[2], [1, 1], [2, 2], [1], [3, 3], [2], [3], [4, 4], [1], [3], [4]]
ans = []
for i, op in enumerate(ops):
    if op == "LFUCache":
        obj = LFUCache(val[i][0])
        ans.append(None)
    elif op == "put":
        ans.append(obj.put(val[i][0], val[i][1]))
    elif op == "get":
        ans.append(obj.get(val[i][0]))
print(ans)
# Your LFUCache object will be instantiated and called as such:
# obj = LFUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
