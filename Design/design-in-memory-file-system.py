import pprint
import json

class FileSystem(object):

    def __init__(self):
        self.fs = {}


    def ls(self, path):
        """
        :type path: str
        :rtype: List[str]
        """

        path = path.lstrip('/').split('/')
        root = self.fs
        if path[0] == '':
            return sorted(root.keys())

        for p in path:
            if p not in root:
                return ['-1']
            root = root[p]

        if list(root.keys()) == ['#']:
            return [p]

        return sorted(root.keys())


    def mkdir(self, path):
        """
        :type path: str
        :rtype: None
        """
        root = self.fs
        path = path.lstrip('/').split('/')

        for p in path:
            if p in root:
                root = root[p]
            else:
                root[p] = {}
                root = root[p]
        # print(json.dumps(self.fs, sort_keys = True, indent = 4))

    def addContentToFile(self, filePath, content):
        """
        :type filePath: str
        :type content: str
        :rtype: None
        """
        self.mkdir(filePath)
        path = filePath.lstrip('/').split('/')

        root = self.fs
        for p in path:
            root = root[p]
        if '#' not in root:
            root['#'] = content
        else:
            root['#'] += content


    def readContentFromFile(self, filePath):
        """
        :type filePath: str
        :rtype: str
        """
        path = filePath.lstrip('/').split('/')
        # print(filePath, path)
        root = self.fs
        for p in path:
            if p not in root:
                return '-1'
            root = root[p]
        return root['#']


ops = ["FileSystem","ls","mkdir","addContentToFile","ls","readContentFromFile"]
val = [[],["/"],["/a/b/c"],["/a/b/c/d","hello"],["/"],["/a/b/c/d"]]
exp = [None,[],None,None,["a"],"hello"]

ops = ["FileSystem","ls","mkdir","addContentToFile","ls","readContentFromFile","addContentToFile","readContentFromFile"]
val = [[],["/"],["/a/b/c"],["/a/b/c/d","hello world"],["/"],["/a/b/c/d"],["/a/b/c/d"," hello hello world"],["/a/b/c/d"]]
exp = [None,[],None,None,["a"],"hello world",None,"hello world hello hello world"]

ops = ["FileSystem","mkdir","ls","ls","mkdir","ls","ls","addContentToFile","readContentFromFile","ls","readContentFromFile"]
val = [[],["/zijzllb"],["/"],["/zijzllb"],["/r"],["/"],["/r"],["/zijzllb/hfktg","d"],["/zijzllb/hfktg"],["/"],["/zijzllb/hfktg"]]
exp = [None,None,["zijzllb"],[],None,["r","zijzllb"],[],None,"d",["r","zijzllb"],"d"]


ops = ["FileSystem","mkdir","ls","ls","mkdir","ls","ls","addContentToFile","ls","ls","ls"]
val = [[],["/goowmfn"],["/goowmfn"],["/"],["/z"],["/"],["/"],["/goowmfn/c","shetopcy"],["/z"],["/goowmfn/c"],["/goowmfn"]]
exp = [None,None,[],["goowmfn"],None,["goowmfn","z"],["goowmfn","z"],None,[],["c"],["c"]]
ans = []
for i, op in enumerate(ops):

    if op == "FileSystem":
        obj = FileSystem()
        ans.append(None)

    elif op == "ls":
        ret = obj.ls(val[i][0])
        ans.append(ret)

    elif op == "mkdir":
        obj.mkdir(val[i][0])
        ans.append(None)

    elif op == "addContentToFile":
        obj.addContentToFile(val[i][0], val[i][1])
        ans.append(None)

    elif op == "readContentFromFile":
        ret = obj.readContentFromFile(val[i][0])
        ans.append(ret)

print('ans:',ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break
