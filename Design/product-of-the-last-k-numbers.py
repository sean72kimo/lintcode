class ProductOfNumbers:

    def __init__(self):
        self.mp = {1:0}
        self.prefix = [1]
        self.flag = 0
        self.nums = []

    def add(self, num: int) -> None:
        self.nums.append(num)

        if num == 0:
            num = 1
            self.flag = len(self.nums)

        p = self.prefix[-1] * num
        self.prefix.append(p)
        n = len(self.prefix)-1
        self.mp[n] = p

    def getProduct(self, k: int) -> int:
        if len(self.mp) - k < self.flag:
            print(self.flag)
            return 0

        m = len(self.mp) - k
        size = len(self.mp)

        res = self.prefix[size] // self.prefix[m]
        return res

# Your ProductOfNumbers object will be instantiated and called as such:
# obj = ProductOfNumbers()
# obj.add(num)
# param_2 = obj.getProduct(k)



ops = ["ProductOfNumbers","add","add","add","add","add","getProduct","getProduct","getProduct","add","getProduct"]
val = [[],[3],[0],[2],[5],[4],[2],[3],[4],[8],[2]]
exp = [None,None,None,None,None,None,20,40,0,None,32]

# ops = ["ProductOfNumbers","add","getProduct","getProduct","getProduct","add","add","add"]
# val = [[],[1],[1],[1],[1],[7],[6],[7]]
# exp = [None,None,1,1,1,None,None,None]


ans = []
for i, op in enumerate(ops):
    print((i, op), val[i])
    if op == "ProductOfNumbers":
        obj = ProductOfNumbers()
        ans.append(None)
    elif op == "add":
        ans.append(obj.add(val[i][0]))
    elif op == "getProduct":
        ans.append(obj.getProduct(val[i][0]))

print("ans:", ans == exp, ans)