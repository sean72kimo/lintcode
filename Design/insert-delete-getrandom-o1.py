import random


class RandomizedSet:

    def __init__(self):
        self.nums = []
        self.pos = {}

    def insert(self, val: int) -> bool:
        if val in self.pos:
            return False
        self.nums.append(val)
        self.pos[val] = len(self.nums) - 1
        return True

    def remove(self, val: int) -> bool:
        if val not in self.pos:
            return False

        idx = self.pos[val]
        keep = self.nums[-1]

        self.nums[idx] = keep
        self.pos[keep] = idx

        """
        和RandomizedSetSol2解法不同之處在於
        RandomizedSet2試著將需要被移除的val實實在在的移到self.nums尾端再進行pop
        但這樣會造成corner case (詳見下方說明)
        
        此解法中，我們只考慮要留下來的數字keep, 不予理會val在self.nums的存在。
        無需交換移動keep, val。而是直接拷貝keep至正確位置，更新self.pos, 
        而原來在最末尾的數字，因為以幾被拷貝至正確位置了，所以最末尾數字可以直接pop
        """
        self.pos.pop(val)
        self.nums.pop()

        return True

    def getRandom(self) -> int:

        idx = random.randrange(0, len(self.nums))
        return self.nums[idx]


class RandomizedSetSol2(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.stack = []
        self.mp = {}

    def insert(self, val):
        """
        Inserts a value to the set. Returns true if the set did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        if val in self.mp:
            return False

        self.stack.append(val)
        self.mp[val] = len(self.stack) - 1
        print(self.stack)
        return True

    def remove(self, val):
        """
        Removes a value from the set. Returns true if the set contained the specified element.
        :type val: int
        :rtype: bool
        """
        if val not in self.mp:
            return False

        i = self.mp[val]
        keep = self.stack[-1]

        self.stack[i], self.stack[-1] = self.stack[-1], self.stack[i]
        self.stack.pop()
        # 如果加入某數 i ，又接著移除 i
        # if 先```self.mp.pop(val)```那麼下一步```self.mp[keep] = j``` 又將val給加回來了
        # 千萬按照下面順序
        self.mp[keep] = i
        self.mp.pop(val)

        # 或者可以判斷：加入的數字(i)是否和移除的數字(i)相同
        # 若相同，則不更新self.mp[i]的位置(因為被移除了)
        # 若不同，則更新。代碼如下:
        # self.mp.pop(val)
        # if k != val:
        #    self.mp[k] = i

        return True

    def getRandom(self):
        """
        Get a random element from the set.
        :rtype: int
        """

        i = random.randrange(0, len(self.stack))
        return self.stack[i]


ops = ["RandomizedSet", "insert", "remove", "insert", "remove", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom"]
val = [[], [0], [0], [-1], [0], [], [], [], [], [], [], [], [], [], []]

for i, op in enumerate(ops):

    if op == "RandomizedSet":
        obj = RandomizedSet()
    elif op == "insert":
        obj.insert(val[i][0])
    elif op == "remove":
        obj.remove(val[i][0])
    elif op == "getRandom":
        obj.getRandom()

grid = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12]
]


def print_anti_dia(grid):
    m = len(grid)
    n = len(grid[0])
    dxy = [1, -1]

    ans = []

    def inbound(i, j):
        return 0 <= i < m and 0 <= j < n

    for i in range(m):
        for j in range(n):
            if i == 0:
                x, y = i , j
                tmp = []
                while inbound(x, y):
                    tmp.append(grid[x][y])
                    x = x + dxy[0]
                    y = y + dxy[1]

                ans.append(tmp)
                continue

            if j == n - 1:
                if i == 0:
                    continue
                x, y = i , j
                tmp = []
                while inbound(x, y):
                    tmp.append(grid[x][y])
                    x = x + dxy[0]
                    y = y + dxy[1]
                ans.append(tmp)
                continue
    return ans

# a = print_anti_dia(grid)
# for r in a:
#     print(r)
