class Node(object):
    """
    double linked list node
    """

    def __init__(self, value, keys):
        self.value = value
        self.keys = keys
        self.prev = None
        self.next = None

    def __repr__(self):
        return "<Node {}, keys={}>".format(self.value, self.keys)

    def __str__(self):
        return "<Node {}, keys={}>".format(self.value, self.keys)


class DoubleLinkedList(object):

    def __repr__(self):
        node = self.head.next
        tmp = []
        while node and node is not self.tail:
            tmp.append(node)
            node = node.next

        return "<DLL {}>".format(tmp)

    def __init__(self):
        self.head = Node(0, set())
        self.tail = Node(0, set())
        self.head.next = self.tail
        self.tail.prev = self.head

    def insert(self, pos, node):  # insert at left of pos, head -> (node) -> pos
        node.prev = pos.prev
        node.next = pos
        pos.prev.next = node
        pos.prev = node

    def remove(self, node):
        node.prev.next = node.next
        node.next.prev = node.prev
        del node

    def empty(self):
        return self.head.next is self.tail


class AllOne(object):

    # 維持一個雙鏈表。鏈表上的每一個點代表value，每一個value包含all keys with same value，
    # 鏈表由小至大排列
    # 透過hashmap, 可以快速O(1)找到某一個key所對應到鏈表上的哪一個value node
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.mp = {}
        self.dll = DoubleLinkedList()
        self.vals = set()

    def inc(self, key):
        """
        Inserts a new key <Key> with value 1. Or increments an existing key by 1.
        :type key: str
        :rtype: void
        """

        if key not in self.mp:
            node = Node(0, {key})
            pos = self.dll.head.next
            print('1', self.dll)
            self.dll.insert(pos, node)
            print('2', self.dll)
            self.mp[key] = node

        bucket = self.mp[key]
        next_bucket = bucket.next

        if next_bucket is self.dll.tail or next_bucket.value > bucket.value + 1:
            node = Node(bucket.value + 1, set())
            self.dll.insert(next_bucket, node)
            next_bucket = node

        next_bucket.keys.add(key)
        self.mp[key] = next_bucket

        bucket.keys.remove(key)
        if not bucket.keys:
            self.dll.remove(bucket)

    def dec(self, key):
        """
        Decrements an existing key by 1. If Key's value is 1, remove it from the data structure.
        :type key: str
        :rtype: void
        """
        if key not in self.mp:
            return

        bucket = self.mp[key]
        prev_bucket = self.mp[key].prev

        if bucket.value > 1:
            if bucket.prev is self.dll.head or prev_bucket.value < bucket.value - 1:
                node = Node(bucket.value - 1, set())
                self.dll.insert(bucket, node)
                prev_bucket = node

            prev_bucket.keys.add(key)
            self.mp[key] = prev_bucket
        else:
            self.mp.pop(key)

        bucket.keys.remove(key)
        if len(bucket.keys) == 0:
            self.dll.remove(bucket)

    def getMaxKey(self):
        """
        Returns one of the keys with maximal value.
        :rtype: str
        """
        if self.dll.empty():
            return ""
        return next(iter(self.dll.tail.prev.keys))

    def getMinKey(self):
        """
        Returns one of the keys with Minimal value.
        :rtype: str
        """
        if self.dll.empty():
            return ""
        return next(iter(self.dll.head.next.keys))


ops = ["AllOne", "getMaxKey", "getMinKey"]
val = [[], [], []]

ops = ["AllOne", "inc", "inc", "inc", "inc", "inc", "inc", "dec", "dec", "getMinKey", "dec", "getMaxKey", "getMinKey"]
val = [[], ["a"], ["b"], ["b"], ["c"], ["c"], ["c"], ["b"], ["b"], [], ["a"], [], []]

ops = ["AllOne", "inc", "inc", "getMaxKey", "getMinKey", "inc", "getMaxKey", "getMinKey"]
val = [[], ["hello"], ["kitty"], [], [], ["leet"], [], []]
exp = [None, None, None, "hello", "hello", None, "hello", "leet"]

ans = []
for i, op in enumerate(ops):
#     print((i, op), val[i])
    if op == "AllOne":
        obj = AllOne()
        ans.append(None)
    elif op == "inc":
        ans.append(obj.inc(val[i][0]))
    elif op == "dec":
        ans.append(obj.dec(val[i][0]))
    elif op == "getMaxKey":
        ans.append(obj.getMaxKey())
    elif op == "getMinKey":
        ans.append(obj.getMinKey())
print("ans:", ans == exp, ans)
print(obj.dll)
