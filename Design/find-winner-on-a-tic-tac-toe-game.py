from typing import List


class Solution:
    def tictactoe(self, moves: List[List[int]]) -> str:
        if not moves:
            return "Pending"

        rows = [0, 0, 0]
        cols = [0, 0, 0]
        left_to_right = 0
        right_to_left = 0
        A = 1
        B = -1
        player = A

        for i, (r, c) in enumerate(moves):
            if i % 2 == 0:
                player = A
            else:
                player = B

            rows[r] += player
            cols[c] += player

            if r == c:
                left_to_right += player

            if r + c == 2:
                right_to_left += player

            # check_winner
            if 3 in [rows[r], cols[c], left_to_right, right_to_left]:
                return 'A'
            if -3 in [rows[r], cols[c], left_to_right, right_to_left]:
                return 'B'

        if len(moves) < 9:
            return "Pending"
        return "Draw"


class Solution:
    def tictactoe(self, moves: List[List[int]]) -> str:
        if not moves:
            return "Pending"
        self.rows = [0, 0, 0]
        self.cols = [0, 0, 0]
        self.top_left = 0
        self.top_right = 0

        for i, (x, y) in enumerate(moves):
            if y == 0:
                print(x, y)
            if i % 2:  # B
                self.rows[x] -= 1
                self.cols[y] -= 1

                if x == y:
                    self.top_left -= 1

                if x + y == 2:
                    self.top_right -= 1

            else:  # A
                self.rows[x] += 1
                self.cols[y] += 1

                if x == y:
                    self.top_left += 1

                if x + y == 2:
                    self.top_right += 1

            if self.rows[x] == 3:
                return 'A'
            elif self.rows[x] == -3:
                return 'B'

            if self.cols[y] == 3:
                return 'A'
            elif self.cols[y] == -3:
                return 'B'

            if self.top_left == 3:
                return 'A'
            elif self.top_left == -3:
                return 'B'

            if self.top_right == 3:
                return 'A'
            elif self.top_right == -3:
                return 'B'

        if len(moves) == 9:
            return "Draw"
        return "Pending"

moves = [[0,2],[1,2],[1,0],[2,2],[2,1],[0,1],[0,0],[1,1],[2,0]]
a = Solution().tictactoe(moves)
e = 'A'
print("ans:", a)