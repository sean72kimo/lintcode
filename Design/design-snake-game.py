import collections


class SnakeGame(object):

    def __init__(self, width, height, food):
        """
        Initialize your data structure here.
        @param width - screen width
        @param height - screen height
        @param food - A list of food positions
        E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0].
        :type width: int
        :type height: int
        :type food: List[List[int]]
        """
        self.w = width
        self.h = height
        self.food = collections.deque(food)
        self.dxy = [[-1, 0], [0, -1], [0, 1], [1, 0]]
        self.snake = collections.deque([[0, 0]])
        self.vst = {(0, 0)}
        self.score = 0

    def move(self, d):
        """
        Moves the snake.
        @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down
        @return The game's score after the move. Return -1 if game over.
        Game over when snake crosses the screen boundary or bites its body.
        :type direction: str
        :rtype: int
        """
        delta = {'U': 0, 'L': 1, 'R': 2, 'D': 3}

        i, j = self.snake[-1]
        ni = i + self.dxy[delta[d]][0]
        nj = j + self.dxy[delta[d]][1]

        if not (0 <= ni < self.h and 0 <= nj < self.w):
            print('OOB')
            return -1

        if len(self.food) and (ni, nj) == (self.food[0][0], self.food[0][1]):
            self.food.popleft()
            self.score += 1
            self.add_head(ni, nj)
            return self.score


        self.remove_tail()
        if (ni, nj) in self.vst:
            return -1
        self.add_head(ni, nj)

        return self.score

    def remove_tail(self):
        pi, pj = self.snake.popleft()
        self.vst.remove((pi, pj))

    def add_head(self, i, j):
        self.vst.add((i, j))
        self.snake.append((i, j))

# Your SnakeGame object will be instantiated and called as such:
# obj = SnakeGame(width, height, food)
# param_1 = obj.move(direction)

ops = ["SnakeGame","move","move","move","move","move","move"]
val = [[3,2,[[1,2],[0,1]]],      ["R"],["D"],["R"],["U"],["L"],["U"]]


ops = ["SnakeGame","move","move","move","move","move","move","move","move","move","move","move","move"]
val = [[3,3,[[2,0],[0,0],[0,2],[2,2]]],["D"],["D"],["R"],["U"],["U"],["L"],["D"],["R"],["R"],["U"],["L"],["D"]]
exp = [None,0,1,1,1,1,2,2,2,2,3,3,3]
ans = []
for i, op in enumerate(ops):
    if op == "SnakeGame":
        obj = SnakeGame(val[0][0], val[0][1], val[0][2])
        ans.append(None)
    elif op == "move":
        r = obj.move(val[i][0])
        # print(obj.snake)
        # print(obj.vst)
        ans.append(r)
print(ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break
