class Node:
    def __init__(self, value=None):
        self.val = value
        self.prev = None
        self.head = None


class MyLinkedList(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head
        self.size = 0

    def is_empty(self):
        return self.head.next is self.tail

    def get(self, index):
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        :type index: int
        :rtype: int
        """
        if index < 0 or index >= self.size:
            return -1
        if self.is_empty():
            return -1

        curr = self.head.next
        for i in range(index):
            curr = curr.next
        return curr.val

    def addAtHead(self, val):
        """
        Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
        :type val: int
        :rtype: None
        """
        node = Node(val)
        node.next = self.head.next
        node.prev = self.head
        self.head.next.prev = node
        self.head.next = node
        self.size += 1

    def addAtTail(self, val):
        """
        Append a node of value val to the last element of the linked list.
        :type val: int
        :rtype: None
        """
        node = Node(val)
        node.next = self.tail
        node.prev = self.tail.prev
        self.tail.prev.next = node
        self.tail.prev = node
        self.size += 1

    def addAtIndex(self, index, val):
        """
        Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
        :type index: int
        :type val: int
        :rtype: None
        """
        if index > self.size:
            return

        if index > self.size:
            return

        if index <= 0:
            self.addAtHead(val)
        elif index == self.size:
            self.addAtTail(val)
        else:
            node = Node(val)
            curr = self.head.next
            for i in range(index):
                curr = curr.next
            node.next = curr
            node.prev = curr.prev
            curr.prev.next = node
            curr.prev = node
            self.size += 1

    def deleteAtIndex(self, index):
        """
        Delete the index-th node in the linked list, if the index is valid.
        :type index: int
        :rtype: None
        """
        if index < 0 or index >= self.size:
            return
        if self.is_empty():
            return

        curr = self.head.next
        for i in range(index):
            curr = curr.next
        curr.prev.next = curr.next
        curr.next.prev = curr.prev
        curr.next = None
        curr.prev = None

        self.size -= 1




ops = ["MyLinkedList","addAtHead","addAtTail","addAtIndex","get","deleteAtIndex","get"]
val = [[],[1],[3],[1,2],[1],[1],[1]]
exp = [None,None,None,None,2,None,3]

ops =  ["MyLinkedList","addAtIndex","get","deleteAtIndex"]
val =  [[],[-1,0],[0],[-1]]
exp = [None,None,0,None]


ans = []
for i, op in enumerate(ops):
    if op == "MyLinkedList":
        obj = MyLinkedList()
        ans.append(None)
    elif op == "addAtHead":
        r = obj.addAtHead(val[i][0])
        ans.append(r)
    elif op == "addAtTail":
        r = obj.addAtTail(val[i][0])
        ans.append(r)
    elif op == "addAtIndex":
        r = obj.addAtIndex(val[i][0], val[i][1])
        ans.append(r)
    elif op == "deleteAtIndex":
        r = obj.deleteAtIndex(val[i][0])
        ans.append(r)
    elif op == "get":
        r = obj.get(val[i][0])
        ans.append(r)
print(ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break