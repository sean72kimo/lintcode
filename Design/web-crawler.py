# """
# This is HtmlParser's API interface.
# You should not implement it, or speculate about its implementation
# """
# class HtmlParser(object):
#    def getUrls(self, url):
#        """
#        :type url: str
#        :rtype List[str]
#        """
import collections


class Solution:
    def crawl(self, startUrl: str, htmlParser: 'HtmlParser') -> List[str]:
        vst = {startUrl}
        domain = startUrl.split("http://")[1].split("/")[0]
        ans = [startUrl]
        que = collections.deque([startUrl])

        while que:

            url = que.popleft()
            nxt_urls = htmlParser.getUrls(url)

            for nxt in nxt_urls:
                if nxt in vst:
                    continue
                if nxt.split("http://")[1].split("/")[0] != domain:
                    continue

                ans.append(nxt)
                vst.add(nxt)
                que.append(nxt)
        return ans
