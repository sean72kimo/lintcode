# The read4 API is already defined for you.
# def read4(buf4: List[str]) -> int:
import collections
from typing import List


class Solution:
    def __init__(self):
        self.que = collections.deque([None, None, None, None])

    def read(self, buf: List[str], n: int) -> int:
        i = 0
        while i < n:
            if not any(self.que):
                self.que = collections.deque([None, None, None, None])
                size = read4(self.que)
                if size == 0:
                    break

            while i < n and any(self.que):
                buf[i] = self.que.popleft()
                i += 1
        return i