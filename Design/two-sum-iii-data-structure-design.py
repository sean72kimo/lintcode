import collections
class TwoSum(object):
    """
    for add heavy
    """
    def __init__(self):
        self.nums = collections.defaultdict(int)

    def add(self, number):
        self.nums[number] += 1

    def find(self, value):

        for n in self.nums.keys():
            num = value - n

            if num in self.nums and (num != n or self.nums[n] > 1):
                return True

        return False

class TwoSum2(object):
    """
    for find heavy
    """
    def __init__(self):
        self.nums = set()
        self.sums = set()


    def add(self, number):
        for n in self.nums:
            summ = n + number
            self.sums.add(summ)

        if number not in self.nums:
            self.nums.add(number)

    def find(self, value):
        return value in self.sums


ops = ["TwoSum", "add", "add", "find"]
val = [[], [1], [2], [1]]
ans = []
for i, op in enumerate(ops):
    if op == "TwoSum":
        obj = TwoSum()
    elif op == "add":
        ans.append(obj.add(val[i][0]))
    elif op == "find":
        ans.append(obj.find(val[i][0]))
print(ans)



class GCD:
    def gcd(self, a, b):
        while b:
            print((a, b))
            tmp = b
            b = a % b
            a = tmp

        return a

print(GCD().gcd(8, 12))
