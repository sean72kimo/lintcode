import collections


class MyStack(object): # push O(n), pop O(1)

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.inQ = collections.deque([])
        self.tmpQ = collections.deque([])

    def push(self, x):
        """
        Push element x onto stack.
        :type x: int
        :rtype: None
        """
        self.tmpQ.append(x)
        while len(self.inQ):
            self.tmpQ.append(self.inQ.popleft())

        self.inQ = self.tmpQ
        self.tmpQ = collections.deque([])

    def pop(self):
        """
        Removes the element on top of the stack and returns that element.
        :rtype: int
        """
        return self.inQ.popleft()

    def top(self):
        """
        Get the top element.
        :rtype: int
        """
        return self.inQ[0]

    def empty(self):
        """
        Returns whether the stack is empty.
        :rtype: bool
        """
        return len(self.inQ) == 0 and len(self.tmpQ) == 0


class MyStack2(object): # push O(1), pop O(n)

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.inQ = collections.deque([])
        self.tmpQ = collections.deque([])

    def push(self, x):
        """
        Push element x onto stack.
        :type x: int
        :rtype: None
        """
        self.inQ.append(x)

    def pop(self):
        """
        Removes the element on top of the stack and returns that element.
        :rtype: int
        """
        while len(self.inQ) > 1:
            self.tmpQ.append(self.inQ.popleft())
        v = self.inQ.popleft()
        self.inQ = self.tmpQ
        self.tmpQ = collections.deque([])
        return v

    def top(self):
        """
        Get the top element.
        :rtype: int
        """
        return self.inQ[-1]

    def empty(self):
        """
        Returns whether the stack is empty.
        :rtype: bool
        """
        return len(self.inQ) == 0 and len(self.tmpQ) == 0


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()
ops =["MyStack","push","push","push","pop","pop","pop","empty"]
val = [[],[1],[2],[3],[],[],[],[]]
exp = [None,None,None,None,3,2,1,True]

ans = []
for i, op in enumerate(ops):
#     print((i, op), val[i])
    if op == "MyStack":
        obj = MyStack()
        ans.append(None)
    elif op == "push":
        ans.append(obj.push(val[i][0]))
    elif op == "pop":
        ans.append(obj.pop())
    elif op == "empty":
        ans.append(obj.empty())

print("ans:", ans == exp, ans)
