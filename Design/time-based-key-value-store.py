import bisect
import collections


class TimeMap(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.mp = collections.defaultdict(list)

    def set(self, key, value, timestamp):
        """
        :type key: str
        :type value: str
        :type timestamp: int
        :rtype: None
        """
        self.mp[key].append((timestamp, value))

    def get(self, key, timestamp):
        """
        :type key: str
        :type timestamp: int
        :rtype: str
        """
        lst = self.mp.get(key, None)

        if lst is None or len(lst) == 0:
            return ""

        i = bisect.bisect_right(lst, (timestamp, chr(127)))

        return lst[i - 1][1] if i else ""

ops = ["TimeMap","set","set","get","get","get","get","get"]
val = [[],["love","high",10],["love","low",20],["love",5],["love",10],["love",15],["love",20],["love",25]]
exp = [None,None,None,"","high","high","low","low"]

ops = ["TimeMap","set","set","get","get","get","get","get"]
val = [[],["love","high",10],["love","low",20],["love",5],["love",10],["love",15],["love",20],["love",25]]
exp = [None,None,None,"","high","high","low","low"]

ans = []
for i, op in enumerate(ops):
    if i == 4:
        break
    if op == "TimeMap":
        obj = TimeMap()
        ans.append(None)

    elif op == "set":
        obj.set(val[i][0], val[i][1], val[i][2])
        ans.append(None)

    elif op == "get":
        r = obj.get(val[i][0],val[i][1])
        ans.append(r)

print('ans:',ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break
