class HoppingIterator:
    def __init__(self, iterator, hops):
        self.buff = iterator
        self.hops = hops
        self.first = True
        self.nxt = next(self.buff, None)


    def hasNext(self):
        if self.first:
            self.first = False
            return self.nxt is not None

        for i in range(self.hops + 1):
            self.nxt = next(self.buff, None)

        return self.nxt is not None
    def next(self):
        return self.nxt




a = [1, 2, 3, 4, 5]
b = iter(a)
hops = 3
hp = HoppingIterator(b, hops)
print("ans:")
while hp.hasNext():
    print(hp.next())

