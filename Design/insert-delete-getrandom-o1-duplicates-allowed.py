import random
import collections
class RandomizedCollection(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.mp = collections.defaultdict(set)
        self.bf = []


    def insert(self, val):
        """
        Inserts a value to the collection. Returns true if the collection did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        self.bf.append(val)
        idx = len(self.bf) - 1
        self.mp[val].add(idx)

        return len(self.mp[val]) == 1

    def remove(self, val):
        """
        Removes a value from the collection. Returns true if the collection contained the specified element.
        :type val: int
        :rtype: bool
        """
        if val not in self.mp:
            return False

        remv_idx = self.mp[val].pop()
        keep_val = self.bf[-1]

        self.bf[remv_idx], self.bf[-1] = self.bf[-1], self.bf[remv_idx]

        self.mp[keep_val].add(remv_idx)
        self.mp[keep_val].remove(len(self.bf) - 1)

        if len(self.mp[val]) == 0:
            self.mp.pop(val)
        self.bf.pop()

        return True

    def getRandom(self):
        """
        Get a random element from the collection.
        :rtype: int
        """
        idx = random.randrange(0, len(self.bf))
        return self.bf[idx]



ops = ["RandomizedCollection", "insert", "remove", "insert", "remove", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom", "getRandom"]
val = [[], [0], [0], [-1], [0], [], [], [], [], [], [], [], [], [], []]


for i, op in enumerate(ops):
    print((i, op), val[i])
    if op == "RandomizedCollection":
        obj = RandomizedCollection()
    elif op == "insert":
        obj.insert(val[i][0])
    elif op == "remove":
        obj.remove(val[i][0])
    elif op == "getRandom":
        obj.getRandom()


