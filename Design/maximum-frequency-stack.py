import collections


class Node:
    def __init__(self, v=None):
        self.val = v
        self.prev = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.next = self.tail
        node.prev = self.tail.prev
        self.tail.prev.next = node
        self.tail.prev = node

    def pop(self):
        node = self.tail.prev
        x = node.val
        self.tail.prev = node.prev
        node.prev.next = self.tail
        del node
        return x

    def is_empty(self):
        return self.head.next is self.tail

    def get_front(self):
        return self.head.next


class Node:
    def __init__(self, v=None):
        self.val = v
        self.prev = None
        self.next = None


class DoubleLinkedList:
    def __init__(self):
        self.head = Node()
        self.tail = Node()
        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.next = self.tail
        node.prev = self.tail.prev
        self.tail.prev.next = node
        self.tail.prev = node

    def pop(self):
        node = self.tail.prev
        x = node.val
        self.tail.prev = node.prev
        node.prev.next = self.tail
        del node
        return x

    def is_empty(self):
        return self.head.next is self.tail

    def get_front(self):
        return self.head.next


class FreqStack(object):

    def __init__(self):
        self.counter = collections.defaultdict(int)
        self.freq = collections.defaultdict(DoubleLinkedList)
        self.max_f = 0

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        self.counter[x] += 1
        f = self.counter[x]
        self.max_f = max(self.max_f, f)
        self.freq[f].append(Node(x))

    def pop(self):
        """
        :rtype: int
        """
        f = self.max_f
        x = self.freq[f].pop()
        self.counter[x] -= 1
        while self.max_f and self.freq[self.max_f].is_empty():
            self.max_f -= 1
        return x

# Your FreqStack object will be instantiated and called as such:
# obj = FreqStack()
# obj.push(x)
# param_2 = obj.pop()

class FreqStack_stack(object):

    def __init__(self):
        self.freq = collections.defaultdict(int)
        self.stack = collections.defaultdict(list)
        self.max_freq = 0

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        self.freq[x] += 1
        f = self.freq[x]
        self.max_freq = max(self.max_freq, f)
        self.stack[f].append(x)

    def pop(self):
        """
        :rtype: int
        """
        x = self.stack[self.max_freq].pop()
        self.freq[x] -= 1
        if len(self.stack[self.max_freq]) == 0:
            self.max_freq -= 1

        return x