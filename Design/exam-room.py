"""
similar to 849. Maximize Distance to Closest Person
"""
import bisect

"""
O(p)
紀錄position的作法，跟下面兩題作法雷同
392. Is Subsequence (follow up, reuse function)
792. Number of Matching Subsequences
"""
class ExamRoom:

    def __init__(self, N: int):
        self.N = N
        self.students = []

    def seat(self) -> int:
        if len(self.students) == 0:
            self.students.append(0)
            return 0

        dist = self.students[0]
        pos = 0
        for i, p in enumerate(self.students):
            if i == 0:
                continue

            prev = self.students[i - 1]
            d = (p - prev) // 2
            if d > dist:
                dist = d
                pos = prev + d
        d = self.N - 1 - self.students[-1]
        if d > dist:
            pos = self.N - 1

        bisect.insort(self.students, pos)
        return pos

    def leave(self, p: int) -> None:
        self.students.remove(p)


"""
1 <= N <= 10^9
如果將所有座位列出來，並且標記哪個位子有坐人，線性掃描的結果會超時 O(N)
"""
class ExamRoom_TLE(object):

    def __init__(self, N):
        """
        :type N: int
        """
        self.seats = [0] * N
        self.N = N

    def seat(self):
        """
        :rtype: int
        """
        if not any(self.seats):
            self.seats[0] = 1
            return 0

        dis = 0
        left= -1
        pos = -1

        for i in range(self.N):
            if self.seats[i] == 0:
                continue

            if left == -1:
                dis = i
                pos = 0
                left = i
                continue

            if (i - left) // 2 > dis:
                dis = (i - left) // 2
                pos = left + (i - left) // 2

            left = i

        if self.seats[-1] == 0:
            if self.N - 1 - left > dis:
                pos = self.N - 1

        self.seats[pos] = 1

        return pos

    def leave(self, p):
        """
        :type p: int
        :rtype: None
        """

        self.seats[p] = 0
        return
# Your ExamRoom object will be instantiated and called as such:
# obj = ExamRoom(N)
# param_1 = obj.seat()
# obj.leave(p)


ops = ["ExamRoom","seat","seat","seat","seat","leave","seat"]
val = [[10],[],[],[],[],[4],[]]
exp = [None,0,9,4,2,None,5]
ans = []
for i, op in enumerate(ops):
    if op == "ExamRoom":
        obj = ExamRoom(val[0][0])
        ans.append(None)

    elif op == "seat":
        rt = obj.seat()
        ans.append(rt)

    elif op == "leave":
        obj.leave(val[i][0])
        ans.append(None)
print(ans, ans == exp)

# a = [0,0,0]
# if not any(a):
#     print('not a')