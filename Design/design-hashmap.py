class Node:
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.nxt = None


class MyHashMap:

    def __init__(self):
        self.cap = 100
        self.buf = [None] * self.cap

    def put(self, key: int, value: int) -> None:
        node = Node(key, value)
        i = hash(key) % self.cap

        if self.buf[i] is None:
            self.buf[i] = node
            return

        head = self.buf[i]
        prev = None
        while head:
            if head.key == key:
                head.val = value
                return
            prev = head
            head = head.nxt

        prev.nxt = node

    def get(self, key: int) -> int:
        i = hash(key) % self.cap
        if self.buf[i] is None:
            return -1
        head = self.buf[i]

        while head:
            if head.key == key:
                return head.val
            head = head.nxt
        return -1

    def remove(self, key: int) -> None:
        i = hash(key) % self.cap
        if self.buf[i] is None:
            return
        head = self.buf[i]
        prev = None
        while head:
            if head.key == key and prev is None:
                self.buf[i] = head.nxt
                return
            if head.key == key:
                prev.nxt = head.nxt

            prev = head
            head = head.nxt

        return

    # Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)