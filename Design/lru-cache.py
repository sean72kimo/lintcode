class Node:
    def __init__(self, k=None, v=None):
        self.key = k
        self.val = v
        self.next = None
        self.prev = None


class DoubleLinkedList:
    def __init__(self):
        self.head = Node()
        self.tail = Node()

        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.prev = self.tail.prev
        node.next = self.tail

        self.tail.prev.next = node
        self.tail.prev = node

    def remove(self, node):
        node.next.prev = node.prev
        node.prev.next = node.next
        del node

    def get_lru(self):
        return self.head.next

    def get_front(self):
        return self.head.next


class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.cap = capacity
        self.dll = DoubleLinkedList()
        self.mp = {}

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key not in self.mp:
            return -1

        node = self.mp[key]
        self.dll.remove(node)
        self.dll.append(node)

        return node.val

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: None
        """
        if self.cap == 0:
            return

        if self.get(key) != -1:
            node = self.mp[key]
            node.val = value
            return

        node = Node(key, value)
        if len(self.mp) == self.cap:
            lru = self.dll.get_front()
            self.dll.remove(lru)
            self.mp.pop(lru.key)

        self.dll.append(node)
        self.mp[key] = node

        return

