import threading
from queue import Queue
import random, time
import collections


class MyQueue:
    def __init__(self, cap):
        self.cap = cap
        self.lock = threading.Lock()
        self.not_full = threading.Condition(self.lock)
        self.not_empty = threading.Condition(self.lock)
        self.que = collections.deque([])

    def get(self):
        with self.not_empty:
            while len(self.que) == 0:
                self.not_empty.wait()
            val = self.que.popleft()
            self.not_full.notify()

            return val

    def put(self, val):
        with self.not_full:
            while len(self.que) >= self.cap:
                self.not_full.wait()
            self.que.append(val)
            self.not_empty.notify()



que = MyQueue(5)
class Produce1(threading.Thread):
    def run(self):
        print('P1 {} running'.format(self._ident))
        while True:
            val = random.choice(range(1, 6))
            que.put(val)
            print(self, "put", val)

    def __repr__(self):
        return "P1"

class Produce2(threading.Thread):
    def run(self):
        print('P2 running')
        while True:
            val = random.choice(range(1, 6))
            que.put(val)
            print(self, "put", val)
            time.sleep(random.random())
    def __repr__(self):
        return "P2"

class Consume1(threading.Thread):
    def run(self):
        print('C1 {} running'.format(self._ident))
        val = que.get()
        print(self, "done", val)

    def __repr__(self):
        return "C1"

class Consume2(threading.Thread):
    def run(self):
        print('C2 {} running'.format(self._ident))
        val = que.get()
        print(self, "done", val)

    def __repr__(self):
        return "C2"


c1 = Consume1()
c2 = Consume2()
c1.start()
c2.start()
time.sleep(1)
input('---------------------')
Produce1().start()
Produce2().start()
