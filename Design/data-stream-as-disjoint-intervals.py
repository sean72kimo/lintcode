import copy

#merge at get
class SummaryRanges(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.intervals = []

    def addNum(self, val):
        """
        :type val: int
        :rtype: None
        """
        self.intervals.append([val, val])
        return

    def merge(self):
        self.intervals.sort(key=lambda x: (x[0], x[1]))

        n = len(self.intervals)
        ans = []
        for i in range(n):
            if i == 0:
                ans.append(self.intervals[i])
                continue

            if ans[-1][1] + 1 >= self.intervals[i][0]:
                ans[-1][1] = max(ans[-1][1], self.intervals[i][1])
            else:
                ans.append(self.intervals[i])

        self.intervals = copy.deepcopy(ans)

    def getIntervals(self):
        self.merge()
        return copy.deepcopy(self.intervals)

#merge at add
class SummaryRanges2(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.intervals = []

    def addNum(self, val):
        """
        :type val: int
        :rtype: None
        """
        self.intervals.append([val, val])
        self.merge()
        return

    def merge(self):
        self.intervals.sort(key=lambda x: (x[0], x[1]))

        n = len(self.intervals)
        ans = []
        for i in range(n):
            if i == 0:
                ans.append(self.intervals[i])
                continue

            if ans[-1][1] + 1 >= self.intervals[i][0]:
                ans[-1][1] = max(ans[-1][1], self.intervals[i][1])
            else:
                ans.append(self.intervals[i])

        self.intervals = copy.deepcopy(ans)

    def getIntervals(self):
        return copy.deepcopy(self.intervals)

ops = ["SummaryRanges","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals"]
val = [[],[1],[],[3],[],[7],[],[2],[],[6],[]]
exp = [None,None,[[1,1]],None,[[1,1],[3,3]],None,[[1,1],[3,3],[7,7]],None,[[1,3],[7,7]],None,[[1,3],[6,7]]]
ans = []
for i, op in enumerate(ops):
    if op == "SummaryRanges":
        obj = SummaryRanges2()
        ans.append(None)
    elif op == "addNum":
        r = obj.addNum(val[i][0])
        ans.append(r)
    elif op == "getIntervals":
        r = obj.getIntervals()
        ans.append(r)


print(ans)
print(ans == exp)

if ans != exp:
    for i in range(len(ans)):
        if ans[i] != exp[i]:
            print('error at', i, ops[i], val[i], ans[i], exp[i])
            break



# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e




class SummaryRanges_heap(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.intervals = []

    def addNum(self, val):
        """
        :type val: int
        :rtype: None
        """
        heapq.heappush(self.intervals, (val, Interval(val, val)))

    def getIntervals(self):
        """
        :rtype: List[Interval]
        """
        stack = []
        while len(self.intervals):
            idx, curr = heapq.heappop(self.intervals)
            if len(stack) == 0:
                stack.append((idx, curr))
            else:
                _, prev = stack[-1]
                if prev.end + 1 >= curr.start:
                    prev.end = max(prev.end, curr.end)
                else:
                    stack.append((idx, curr))

        self.intervals = stack
        ans = []
        for inter in stack:
            ans.append(inter[1])
        return ans

# Your SummaryRanges object will be instantiated and called as such:
# obj = SummaryRanges()
# obj.addNum(val)
# param_2 = obj.getIntervals()


class SummaryRanges_binary_search(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.inter = []

    def _binary_search(self, val):
        if len(self.inter) == 0:
            return -1
        left, right = 0, len(self.inter) - 1
        while left + 1 < right:
            mid = left + (right - left) / 2
            if self.inter[mid].start == val:
                return mid
            elif self.inter[mid].start > val:
                right = mid
            else:
                left = mid
        if self.inter[right].start <= val:
            return right
        elif self.inter[left].start <= val:
            return left
        return -1

    def addNum(self, val):
        """
        :type val: int
        :rtype: void
        """
        """ 
        For a new number val, find the last(biggest) interval
        [s,t], such that s <= val. If no such interval exists, 
        return -1.
        """
        i = self._binary_search(val)
        if i != -1 and self.inter[i].end >= val:
            return
        if i != len(self.inter) - 1 and self.inter[i + 1].start == val + 1:
            self.inter[i + 1].start = val
        elif i != -1 and val - 1 == self.inter[i].end:
            self.inter[i].end = val
        else:
            self.inter = self.inter[:i + 1] + [Interval(val, val)] + self.inter[i + 1:]

        if i != -1 and i != len(self.inter) - 1 and self.inter[i].end + 1 == self.inter[i + 1].start:
            self.inter[i].end = self.inter[i + 1].end
            self.inter = self.inter[:i + 1] + self.inter[i + 2:]

    def getIntervals(self):
        """
        :rtype: List[Interval]
        """
        return self.inter



ops = ["SummaryRanges","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals"]
val = [[],[1],[],[3],[],[7],[],[2],[],[6],[]]
exp = [None,None,[[1,1]],None,[[1,1],[3,3]],None,[[1,1],[3,3],[7,7]],None,[[1,3],[7,7]],None,[[1,3],[6,7]]]
ans = []
for i, op in enumerate(ops):
    if op == "SummaryRanges_binary_search":
        obj = SummaryRanges2()
        ans.append(None)
    elif op == "addNum":
        r = obj.addNum(val[i][0])
        ans.append(r)
    elif op == "getIntervals":
        r = obj.getIntervals()
        ans.append(r)


print(ans)
print(ans == exp)

if ans != exp:
    for i in range(len(ans)):
        if ans[i] != exp[i]:
            print('error at', i, ops[i], val[i], ans[i], exp[i])
            break

