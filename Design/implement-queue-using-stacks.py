class MyQueue(object):# push O(1), pop O(n)

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.inS = []
        self.out = []

    def push(self, x):
        """
        Push element x to the back of queue.
        :type x: int
        :rtype: None
        """

        self.inS.append(x)

    def pop(self):
        """
        Removes the element from in front of queue and returns that element.
        :rtype: int
        """
        if not len(self.out):
            self.peek()

        v = self.out.pop()
        return v

    def peek(self):
        """
        Get the front element.
        :rtype: int
        """
        if len(self.out):
            v = self.out[-1]
            return v

        while len(self.inS):
            v = self.inS.pop()
            self.out.append(v)
        return v

    def empty(self):
        """
        Returns whether the queue is empty.
        :rtype: bool
        """
        return len(self.inS) == 0 and len(self.out) == 0


# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()

class MyQueue2(object): # push O(n), pop O(1)

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.inS = []
        self.tmp = []

    def push(self, x):
        """
        Push element x to the back of queue.
        :type x: int
        :rtype: None
        """
        while len(self.inS):
            self.tmp.append(self.inS.pop())

        self.inS.append(x)

        while len(self.tmp):
            self.inS.append(self.tmp.pop())

    def pop(self):
        """
        Removes the element from in front of queue and returns that element.
        :rtype: int
        """
        return self.inS.pop()

    def peek(self):
        """
        Get the front element.
        :rtype: int
        """
        return self.inS[-1]

    def empty(self):
        """
        Returns whether the queue is empty.
        :rtype: bool
        """
        return len(self.inS) == 0 and len(self.tmp) == 0