class Node:
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.next = None
        self.prev = None


class DoubleLinkedList:
    def __init__(self):
        self.head = Node(None, None)
        self.tail = Node(None, None)
        self.head.next = self.tail
        self.tail.prev = self.head

    def append(self, node):
        node.next = self.tail
        node.prev = self.tail.prev
        self.tail.prev.next = node
        self.tail.prev = node

    def remove(self, node):
        node.next.prev = node.prev
        node.prev.next = node.next
        del node

    def get_front(self):
        return self.head.next

    def is_empty(self):
        return self.head.next is self.tail

    def get_key(self, key):
        if self.head.next is self.tail:
            return None
        curr = self.head
        while curr:
            if curr.key == key:
                return curr
            curr = curr.next
        return None

    def get_key(self, key):
        if self.is_empty():
            return None
        curr = self.head
        while curr:
            if curr.key == key:
                return curr
            curr = curr.next
        return None



class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.cap = 100
        self.buf = [DoubleLinkedList() for _ in range(self.cap)]

    def put(self, key: int, value: int) -> None:
        """
        value will always be non-negative.
        """
        i = self.get_idx(key)
        node = Node(key, value)
        bucket = self.buf[i]
        if bucket.is_empty():
            bucket.append(node)
            return
        curr = bucket.get_key(key)
        if curr:
            curr.val = value
        else:
            bucket.append(node)

    def get(self, key: int) -> int:
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        """
        i = self.get_idx(key)
        bucket = self.buf[i]
        curr = bucket.get_key(key)
        return curr.val if curr else -1

    def remove(self, key: int) -> None:
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        """
        i = self.get_idx(key)
        bucket = self.buf[i]

        curr = bucket.get_key(key)
        if curr is None:
            return
        bucket.remove(curr)
        return

    def get_idx(self, key):
        code = hash(key)
        return code % self.cap


# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)
class Node_ll:
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.next = None
    def __repr__(self):
        return "N({},{})".format(self.key, self.val)

class MyHashMap_ll(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.cap = 100
        self.buf = [None] * self.cap

    def printLst(self, node):
        if not node:
            return 'empty'
        while node:
            print(node, end = ' --> ')
            node = node.next
        print()

    def put(self, key, value):
        """
        value will always be non-negative.
        :type key: int
        :type value: int
        :rtype: void
        """
        code = hash(key)
        i = code % self.cap
        node = Node(key, value)

        if self.buf[i] is None:
            self.buf[i] = node
            return

        head = self.buf[i]
        while head:
            if head.key == key:
                head.val = value
                return
            prev = head
            head = head.next
        prev.next = node


    def get(self, key):
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        :type key: int
        :rtype: int
        """
        code = hash(key)
        i = code % self.cap

        if self.buf[i] is None:
            return -1

        head = self.buf[i]

        while head:
            if head.key == key:
                return head.val
            head = head.next

        return -1


    def remove(self, key):
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        :type key: int
        :rtype: void
        """
        code = hash(key)
        i = code % self.cap

        if self.buf[i] is None:
            return

        head = self.buf[i]
        curr = head
        prev = None

        while curr:
            if curr.key == key:
                break
            prev = curr
            curr = curr.next

        if curr is None:
            return

        if curr.key == head.key:
            self.buf[i] = head.next
        else:
            prev.next = curr.next

    def rehashing(self):
        tmp = self.buff[:]
        self.buff = [None] * len(self.buff) * 2
        self.maxSize = len(self.buff)

        for head in tmp:
            while head:
                print('rehashing:', head, head.key, head.val)
                self.put(head.key, head.val)
                head = head.next

ops = ["MyHashMap", "put", "put", "get", "get", "put", "get", "remove", "get"]
val = [[], [1, 1], [2, 2], [1], [3], [2, 1], [2], [2], [2]]
ops = ["MyHashMap", "remove", "put", "put", "put", "put", "put", "put", "get", "put", "put"]
val = [[], [2], [3, 11], [4, 13], [15, 6], [6, 15], [8, 8], [11, 0], [11], [1, 10], [12, 14]]
ops = ["MyHashMap", "remove", "put", "remove", "remove", "get", "remove", "put", "get", "remove", "put", "put", "put", "put", "put", "put", "put", "put", "put", "put", "put", "remove", "put", "put", "get", "put", "get", "put", "put", "get", "put", "remove", "remove", "put", "put", "get", "remove", "put", "put", "put", "get", "put", "put", "remove", "put", "remove", "remove", "remove", "put", "remove", "get", "put", "put", "put", "put", "remove", "put", "get", "put", "put", "get", "put", "remove", "get", "get", "remove", "put", "put", "put", "put", "put", "put", "get", "get", "remove", "put", "put", "put", "put", "get", "remove", "put", "put", "put", "put", "put", "put", "put", "put", "put", "put", "remove", "remove", "get", "remove", "put", "put", "remove", "get", "put", "put"]
val = [[], [27], [65, 65], [19], [0], [18], [3], [42, 0], [19], [42], [17, 90], [31, 76], [48, 71], [5, 50], [7, 68], [73, 74], [85, 18], [74, 95], [84, 82], [59, 29], [71, 71], [42], [51, 40], [33, 76], [17], [89, 95], [95], [30, 31], [37, 99], [51], [95, 35], [65], [81], [61, 46], [50, 33], [59], [5], [75, 89], [80, 17], [35, 94], [80], [19, 68], [13, 17], [70], [28, 35], [99], [37], [13], [90, 83], [41], [50], [29, 98], [54, 72], [6, 8], [51, 88], [13], [8, 22], [85], [31, 22], [60, 9], [96], [6, 35], [54], [15], [28], [51], [80, 69], [58, 92], [13, 12], [91, 56], [83, 52], [8, 48], [62], [54], [25], [36, 4], [67, 68], [83, 36], [47, 58], [82], [36], [30, 85], [33, 87], [42, 18], [68, 83], [50, 53], [32, 78], [48, 90], [97, 95], [13, 8], [15, 7], [5], [42], [20], [65], [57, 9], [2, 41], [6], [33], [16, 44], [95, 30]]


# ops = ["MyHashMap", "put", "put", "put", "put", "remove", "get"]
# val = [[], [1, 1], [2, 2], [5, 5], [8, 8], [11], [11]]

ans = []
expected = [None, None, None, None, None, -1, None, None, -1, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, 90, None, -1, None, None, 40, None, None, None, None, None, 29, None, None, None, None, 17, None, None, None, None, None, None, None, None, None, 33, None, None, None, None, None, None, 18, None, None, -1, None, None, -1, 35, None, None, None, None, None, None, None, -1, -1, None, None, None, None, None, -1, None, None, None, None, None, None, None, None, None, None, None, None, None, -1, None, None, None, None, 87, None, None]
for i, op in enumerate(ops):

    if op == "MyHashMap":
        obj = MyHashMap()
        ans.append(None)
    elif op == "put":
        ans.append(obj.put(val[i][0], val[i][1]))
    elif op == "remove":
        ans.append(obj.remove(val[i][0]))
    elif op == "get":
        ans.append(obj.get(val[i][0]))
print(ans == expected)

# obj.printLst(2)
print(expected)
if ans != expected:
    for i in range(len(ans)):
        if ans[i] != expected[i]:
            break
print('at {}'.format(i), ops[35], val[35])



# for i in obj.buf:
#     obj.printLst(i)

# Your MyHashMap object will be instantiated and called as such:
# obj = MyHashMap()
# obj.put(key,value)
# param_2 = obj.get(key)
# obj.remove(key)
