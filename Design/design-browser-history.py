class BrowserHistory:

    def __init__(self, homepage: str):
        self.history = [homepage]
        self.curr = 0
        self.bound = 0

    def visit(self, url: str) -> None:
        self.curr += 1
        if self.curr == len(self.history):
            self.history.append(url)
        else:
            self.history[self.curr] = url
        self.bound = self.curr

    def back(self, steps: int) -> str:
        print(steps)
        self.curr = max(self.curr - steps, 0)
        return self.history[self.curr]

    def forward(self, steps: int) -> str:
        self.curr = min(self.curr + steps, self.bound)
        return self.history[self.curr]


ops = ["BrowserHistory","visit","visit","visit","back","back","forward","visit","forward","back","back"]
vals = [["leetcode.com"],["google.com"],["facebook.com"],["youtube.com"],[1],[1],[1],["linkedin.com"],[2],[2],[7]]
exp = [None,None,None,None,"facebook.com","google.com","facebook.com",None,"linkedin.com","google.com","leetcode.com"]
ans = []
for i, op in enumerate(ops):
    if op == "BrowserHistory":
        obj = BrowserHistory(vals[0][0])
        ans.append(None)
    elif op == "visit":
        r = obj.visit(vals[i][0])
        ans.append(r)
    elif op == "back":
        r = obj.back(vals[i][0])
        ans.append(r)
    elif op == "forward":
        r = obj.forward(vals[i][0])
        ans.append(r)
print('ans:',ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break
