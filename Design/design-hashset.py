class Node:
    def __init__(self, key):
        self.key = key
        self.next = None


class MyHashSet:

    def __init__(self):
        self.cap = 10
        self.buf = [None] * self.cap

    def add(self, key: int) -> None:
        node = Node(key)
        i = hash(key) % self.cap

        if self.buf[i] is None:
            self.buf[i] = node
            return

        head = self.buf[i]
        prev = None
        while head:
            if head.key == key:
                return
            prev = head
            head = head.next
        prev.next = node

    def remove(self, key: int) -> None:
        i = hash(key) % self.cap
        if self.buf[i] is None:
            return

        head = self.buf[i]
        prev = None
        while head:
            if head.key == key and prev is None:
                self.buf[i] = head.next
                return

            if head.key == key:
                prev.next = head.next
                return

            prev = head
            head = head.next

    def contains(self, key: int) -> bool:
        i = hash(key) % self.cap
        if self.buf[i] is None:
            return False

        head = self.buf[i]

        while head:
            if head.key == key:
                return True
            head = head.next
        return False


# Your MyHashSet object will be instantiated and called as such:
# obj = MyHashSet()
# obj.add(key)
# obj.remove(key)
# param_3 = obj.contains(key)


ops = ["MyHashSet","add","add","contains","contains","add","contains","remove","contains"]
val = [[],[1],[2],[1],[3],[2],[2],[2],[2]]
exp = [None,None,None,True,False,None,True,None,False]

ops = ["MyHashSet","add","contains","remove","contains"]
val = [[],[1],[1],[1],[1]]
exp = [None,None,True,None,False]

ops = ["MyHashSet","contains","remove","add","add","contains","remove","contains","contains","add","add","add","add","remove","add","add","add","add","add","add","add","add","add","add","contains","add","contains","add","add","contains","add","add","remove","add","add","add","add","add","contains","add","add","add","remove","contains","add","contains","add","add","add","add","add","contains","remove","remove","add","remove","contains","add","remove","add","add","add","add","contains","contains","add","remove","remove","remove","remove","add","add","contains","add","add","remove","add","add","add","add","add","add","add","add","remove","add","remove","remove","add","remove","add","remove","add","add","add","remove","remove","remove","add","contains","add"]
val = [[],[72],[91],[48],[41],[96],[87],[48],[49],[84],[82],[24],[7],[56],[87],[81],[55],[19],[40],[68],[23],[80],[53],[76],[93],[95],[95],[67],[31],[80],[62],[73],[97],[33],[28],[62],[81],[57],[40],[11],[89],[28],[97],[86],[20],[5],[77],[52],[57],[88],[20],[48],[42],[86],[49],[62],[53],[43],[98],[32],[15],[42],[50],[19],[32],[67],[84],[60],[8],[85],[43],[59],[65],[40],[81],[55],[56],[54],[59],[78],[53],[0],[24],[7],[53],[33],[69],[86],[7],[1],[16],[58],[61],[34],[53],[84],[21],[58],[25],[45],[3]]
exp = [None,False,None,None,None,False,None,True,False,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,False,None,True,None,None,True,None,None,None,None,None,None,None,None,True,None,None,None,None,False,None,False,None,None,None,None,None,True,None,None,None,None,True,None,None,None,None,None,None,True,True,None,None,None,None,None,None,None,False,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,False,None]
ans = []
print(len(ops))
for i, op in enumerate(ops):
    if i != 0:
        print(op, val[i][0])
    if i == 25:
        print('.')
    if op == "MyHashSet":
        obj = MyHashSet()
        ans.append(None)
    elif op == "add":
        r = obj.add(val[i][0])
        ans.append(r)
    elif op == "remove":
        r = obj.remove(val[i][0])
        ans.append(r)
    elif op == "contains":
        r = obj.contains(val[i][0])
        ans.append(r)

print(ans)
print(ans == exp)

if ans != exp:
    for i, (x, y) in enumerate(zip(ans, exp)):
        if x != y:
            print('error at', i, ops[i], val[i])
            break