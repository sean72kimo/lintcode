import collections
import copy
import bisect

"""
only stored the delta
and use binary search on snap_id to find the snap
when using bisect binary search, it would be good practice to put both [snap_id, val] to avoid confusion
https://leetcode.com/problems/snapshot-array/discuss/350562/JavaPython-Binary-Search
"""
class SnapshotArray_BinarySearch:

    def __init__(self, length: int):
        self.A = [[] for _ in range(length)]
        self.snap_id = 0

    def set(self, index: int, val: int) -> None:
        self.A[index].append([self.snap_id, val])

    def snap(self) -> int:
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index: int, snap_id: int) -> int:
        print(self.A[index])
        i = bisect.bisect_right(self.A[index], [snap_id, float('inf')]) - 1
        print(self.A)
        print(i, self.A[index])
        if i == -1:
            return 0
        return self.A[index][i][1]

"""
brute force to copy everything to create a snapshot when snap
"""
class SnapshotArray_BruteForce:

    def __init__(self, length: int):
        self.snap_mp = {}
        self.mp = collections.defaultdict(int)
        self.ver = 0

    def set(self, index: int, val: int) -> None:
        self.mp[index] = val

    def snap(self) -> int:
        self.snap_mp[self.ver] = copy.deepcopy(self.mp)
        self.ver += 1

        return self.ver - 1

    def get(self, index: int, snap_id: int) -> int:
        if snap_id not in self.snap_mp:
            return -1

        return self.snap_mp[snap_id][index]


ops = ["SnapshotArray", "set",  "set", "get"]
val = [[3], [0, 5], [0, 6], [0, 0]]
exp = [None,None,0,None,5]


ops = ["SnapshotArray","snap","snap","get","set","snap","set"]
val = [[4],[],[],[3,1],[2,4],[],[1,4]]
exp = [None,0,1,0,None,2,None]

a = []
snap = None
for op, v in zip(ops, val):
    if op =="SnapshotArray":
        snap = SnapshotArray_BinarySearch(v[0])
        a.append(None)
    if op == "set":
        snap.set(v[0],v[1])
        a.append(None)
    if op == "snap":
        rt = snap.snap()
        a.append(rt)
    if op == "get":
        rt = snap.get(v[0], v[1])
        a.append(rt)
print("ans:", a == exp, a)


# arr = [-1,0,0,1]
# i = arr.index(0)
# print(i)
# i = bisect.bisect(arr, 0)
# print(i)
#
# arr = [[-1, 0], [0, 5], [1, 6]]
# i = bisect.bisect(arr, [0])
# print(i)
#
# print([0, float('inf')] < [0,1])