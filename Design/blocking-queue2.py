from threading import Thread, Lock, Condition
import collections
import threading
import time
import random


class MyQueue:
    def __init__(self, cap):
        self.cap = cap
        self.que = collections.deque([])
        self.lock = Lock()
        self.condition = Condition(self.lock)

    def get(self):
        with self.condition:
            while len(self.que) == 0:
                self.condition.wait()
            val = self.que.popleft()
            print(val)
            self.condition.notify()
            return val

    def put(self, item):
        with self.condition:
            while len(self.que) == self.cap:
                self.condition.wait()
            self.que.append(item)
            self.condition.notify()


que = MyQueue(5)
class Produce1(threading.Thread):
    def run(self):
        print('P1 running')
        while True:
            val = random.choice(range(1, 6))
            que.put(val)
            print(self, "put", val)
            time.sleep(random.random())
    def __repr__(self):
        return "P1 "

class Produce2(threading.Thread):
    def run(self):
        print('P2 running')
        while True:
            val = random.choice(range(1, 6))
            que.put(val)
            print(self, "put", val)
            time.sleep(random.random())
    def __repr__(self):
        return "P2 "

class Consume1(threading.Thread):
    def run(self):
        print('C1 {} running'.format(self._ident))
        while True:
            val = que.get()
            print(self, "get", val)
            time.sleep(random.random())
    def __repr__(self):
        return "C1 "

class Consume2(threading.Thread):
    def run(self):
        print('C2 {} running'.format(self._ident))
        while True:
            val = que.get()
            # print(self, "get", val)
            time.sleep(random.random())
    def __repr__(self):
        return "C2 "



Consume1().start()
Consume2().start()

Produce1().start()
Produce2().start()

